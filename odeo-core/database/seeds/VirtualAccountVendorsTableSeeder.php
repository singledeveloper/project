<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class VirtualAccountVendorsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('virtual_account_vendors')->insert([
      'code' => 'doku_bca',
      'name' => 'BCA',
      'prefix' => '74104',
      'is_active' => false,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'cost' => 3300
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'doku_bri',
      'name' => 'BRI',
      'prefix' => '63213',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => false,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 18,
      'min_length' => 8,
      'cost' => 3500
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'doku_mandiri',
      'name' => 'Mandiri',
      'prefix' => '88899043',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'cost' => 3500
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'doku_indomaret',
      'name' => 'Indomaret',
      'prefix' => '88299',
      'is_active' => false,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 7000,
      'cost' => 3500
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'direct_cimb',
      'name' => 'CIMB',
      'prefix' => '8199',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 19,
      'min_length' => 7,
      'cost' => 1000
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'prismalink_bca',
      'name' => 'BCA',
      'prefix' => '74104',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => false,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 23,
      'min_length' => 8,
      'cost' => 1100
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'prismalink_bca',
      'name' => 'BCA',
      'prefix' => '10698',
      'is_active' => false,
      'support_fixed_payment' => false,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 23,
      'min_length' => 8,
      'cost' => 1100
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'doku_alfamart',
      'name' => 'Alfamart',
      'prefix' => '11111067',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 7000,
      'max_length' => 20,
      'min_length' => 11,
      'cost' => 3500
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'direct_artajasa',
      'name' => 'ATM Bersama',
      'prefix' => '500507',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 18,
      'min_length' => 9,
      'cost' => 500
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'direct_permata',
      'name' => 'Permata',
      'prefix' => '3233',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => false,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 16,
      'min_length' => 12,
      'cost' => 1500
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'direct_mandiri',
      'name' => 'Mandiri',
      'prefix' => '89910',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 16,
      'min_length' => 7,
      'cost' => 2000
    ]);
    DB::table('virtual_account_vendors')->insert([
      'code' => 'direct_maybank',
      'name' => 'Maybank',
      'prefix' => '2233',
      'is_active' => true,
      'support_fixed_payment' => true,
      'support_open_payment' => true,
      'support_partial_payment' => false,
      'fee' => 5000,
      'max_length' => 16,
      'min_length' => 7,
      'cost' => 2000
    ]);
  }
}

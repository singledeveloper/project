<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserStoresTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_stores')->insert([
            'user_id'   => 1,
            'store_id'  => 1,
            'type'      => 'owner',
        ]);
        DB::table('user_stores')->insert([
            'user_id'   => 1,
            'store_id'  => 2,
            'type'      => 'owner',
        ]);
        DB::table('user_stores')->insert([
            'user_id'   => 2,
            'store_id'  => 3,
            'type'      => 'owner',
        ]);
    }
}

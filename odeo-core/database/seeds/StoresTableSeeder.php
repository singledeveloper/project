<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Odeo\Domains\Constant\StoreStatus;

class StoresTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('stores')->insert([
      'plan_id' => 5,
      'name' => 'Testong',
      'current_data' => '{"pulsa":4,"bolt":11,"pln":10,"flight":9,"hotel":7}',
      'subdomain_name' => 'testong',
      'renewal_at' => date('Y-m-d H:i:s', strtotime('+1 month')),
      'status' => StoreStatus::OK,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ]);
    DB::table('stores')->insert([
      'plan_id' => 2,
      'name' => 'Store 2',
      'current_data' => '{"pulsa":4,"flight":0,"hotel":0}',
      'subdomain_name' => 'store2',
      'renewal_at' => date('Y-m-d H:i:s', strtotime('+6 month')),
      'status' => StoreStatus::OK,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ]);
    DB::table('stores')->insert([
      'plan_id' => 3,
      'name' => 'Store 3',
      'current_data' => '{"pulsa":4,"flight":0,"hotel":0}',
      'subdomain_name' => 'store3',
      'renewal_at' => date('Y-m-d H:i:s', strtotime('+12 month')),
      'status' => StoreStatus::OK,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ]);
  }
}

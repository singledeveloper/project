<?php

use Illuminate\Database\Seeder;

class PaymentOdeoPaymentChannelInformationSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('payment_odeo_payment_channel_informations')->insert([
      [
        'id' => 1,
        'name' => 'Credit Card',
        'actual_name' => 'Credit Card',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'CC.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 6,
      ],
      [
        'id' => 2,
        'name' => 'Doku Wallet',
        'actual_name' => 'Doku Wallet',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'dokuwallet.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 9
      ],
      [
        'id' => 3,
        'name' => 'ATM Transfer (Virtual Account)',
        'actual_name' => 'ATM Transfer (Virtual Account)',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'va.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 11
      ],
      [
        'id' => 4,
        'name' => 'Sinarmas',
        'actual_name' => 'Sinarmas',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 5,
        'name' => 'Alfa Group',
        'actual_name' => 'Alfa Group',
        'max_transaction' => '3000000',
        'min_transaction' => '10000',
        'logo' => 'alfagroup.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 10
      ],
      [
        'id' => 6,
        'name' => 'Mandiri Clickpay',
        'actual_name' => 'Mandiri Clickpay',
        'max_transaction' => '50000000',
        'min_transaction' => '10000',
        'logo' => 'mandiriclickpay.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 7,
        'name' => 'Kredivo',
        'actual_name' => 'Kredivo',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => 'kredivo.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 5
      ],
      [
        'id' => 8,
        'name' => 'Cimb Clicks',
        'actual_name' => 'Cimb Clicks',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 9,
        'name' => 'Telkomsel Cash',
        'actual_name' => 'Telkomsel Cash',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 10,
        'name' => 'XL Tunai',
        'actual_name' => 'XL Tunai',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 11,
        'name' => 'Mandiri Bill Payment',
        'actual_name' => 'Mandiri Bill Payment',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 12,
        'name' => 'BBM Money',
        'actual_name' => 'BBM Money',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 13,
        'name' => 'Indomaret',
        'actual_name' => 'Indomaret',
        'max_transaction' => '5000000',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 14,
        'name' => 'Indosat Dompetku',
        'actual_name' => 'Indosat Dompetku',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 15,
        'name' => 'Mandiri E Cash',
        'actual_name' => 'Mandiri E Cash',
        'max_transaction' => '1500000',
        'min_transaction' => '10000',
        'logo' => 'mandiriecash.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 8
      ],
      [
        'id' => 16,
        'name' => 'BCA Clickpay',
        'actual_name' => 'BCA Clickpay',
        'max_transaction' => '10000000',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 17,
        'name' => 'BRI Epay',
        'actual_name' => 'BRI Epay',
        'max_transaction' => '5000000',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 18,
        'name' => 'Bank Transfer Mandiri',
        'actual_name' => 'Bank Transfer Mandiri',
        'max_transaction' => '999999999',
        'min_transaction' => '5000',
        'logo' => 'mandiri.png',
        'detail' => json_encode([
          'acc_bank' => 'Mandiri',
          'acc_name' => 'PT. Odeo Teknologi Indonesia',
          'acc_number' => '165-00-88-66-88-77',
          'acc_branch' => 'Grand Slipi Tower - Jakarta Barat'
        ]),
        'is_direct_payment' => false,
        'priority' => 2
      ],
      [
        'id' => 19,
        'name' => 'Bank Transfer BCA',
        'actual_name' => 'Bank Transfer BCA',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'BCA.png',
        'detail' => json_encode([
          'acc_bank' => 'BCA',
          'acc_name' => 'PT. Odeo Teknologi Indonesia',
          'acc_number' => '076-22-999-66',
          'acc_branch' => 'Tanjung Duren - Jakarta Barat'
        ]),
        'is_direct_payment' => false,
        'priority' => 1
      ],
      [
        'id' => 20,
        'name' => 'Bank Transfer BNI',
        'actual_name' => 'Bank Transfer BNI',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'BNI.png',
        'detail' => json_encode([
          'acc_bank' => 'BNI',
          'acc_name' => 'PT. Odeo Teknologi Indonesia',
          'acc_number' => '445-383-134',
          'acc_branch' => 'Grand Slipi Tower - Jakarta Barat'
        ]),
        'is_direct_payment' => false,
        'priority' => 3
      ],
      [
        'id' => 21,
        'name' => 'Cicilan Kartu Kredit',
        'actual_name' => 'Cicilan Kartu Kredit',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'CC.png',
        'detail' => json_encode([
          'mandiri' => [
            'name' => 'Kartu Kredit Mandiri',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 3, 'promo_id' => '003'],
              'pb6' => ['month' => 6, 'mdr' => 5, 'promo_id' => '006'],
              'pb12' => ['month' => 12, 'mdr' => 8, 'promo_id' => '012'],
              'pb18' => ['month' => 18, 'mdr' => 10, 'promo_id' => '018'],
              'pb24' => ['month' => 24, 'mdr' => 11, 'promo_id' => '024'],
            ],
            'installment_acquirer' => 300
          ],
          'danamon' => [
            'name' => 'Kartu Kredit Danamon',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '301'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '601'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '101'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],
          'permata' => [
            'name' => 'Kartu Kredit Permata',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '302'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '602'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '102'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],

          'uob' => [
            'name' => 'Kartu Kredit UOB',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '303'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '603'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '103'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],
          'ocbc' => [
            'name' => 'Kartu Kredit OCBC',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '304'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '604'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '104'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],
          'hsbc' => [
            'name' => 'Kartu Kredit HSBC',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '305'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '605'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '105'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'

          ],
          'stancart' => [
            'name' => 'Kartu Kredit Standard Chartered',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '306'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '606'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '106'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],
          'bri' => [
            'name' => 'Kartu Kredit BRI',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '307'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '607'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '107'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],
          'bukopin' => [
            'name' => 'Kartu Kredit Bukopin',
            'available_installment' => [
              'pb3' => ['month' => 3, 'mdr' => 5, 'promo_id' => '308'],
              'pb6' => ['month' => 6, 'mdr' => 7.5, 'promo_id' => '608'],
              'pb12' => ['month' => 12, 'mdr' => 10, 'promo_id' => '108'],
            ],
            'installment_acquirer' => 100,
            'payment_type' => 'OFFUSINSTALLMENT'
          ],
        ]),
        'is_direct_payment' => false,
        'priority' => 7
      ],
      [
        'id' => 22,
        'name' => 'Bank Transfer BRI',
        'actual_name' => 'Bank Transfer BRI',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'BRI.png',
        'detail' => json_encode([
          'acc_bank' => 'BRI',
          'acc_name' => 'PT. Odeo Teknologi Indonesia',
          'acc_number' => '0417-01-00043930-5',
          'acc_branch' => 'Cabang S. Parman - Jakarta Barat'
        ]),
        'is_direct_payment' => false,
        'priority' => 4
      ],
      [
        'id' => 23,
        'name' => 'Post Paid',
        'actual_name' => 'Post Paid',
        'max_transaction' => '1',
        'min_transaction' => '9999999999',
        'logo' => 'BRI.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 24,
        'name' => 'Credit Card',
        'actual_name' => 'Credit Card Tokenization',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'CC.png',
        'detail' => null,
        'is_direct_payment' => true,
        'priority' => 99
      ],
      [
        'id' => 25,
        'name' => 'Cash on Delivery',
        'actual_name' => 'Cash on Delivery',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 26,
        'name' => 'Pay with oCash',
        'actual_name' => 'Odeo Cash',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => true,
        'priority' => 0
      ],
      [
        'id' => 27,
        'name' => 'Mandiri MPT',
        'actual_name' => 'Mandiri MPT',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 28,
        'name' => 'Internet Banking Danamon',
        'actual_name' => 'internet banking danamon',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 29,
        'name' => 'BNI Debit Online',
        'actual_name' => 'BNI Debit Online',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 30,
        'name' => 'Internet Banking Muamalat',
        'actual_name' => 'Internet Banking Muamalat',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 31,
        'name' => 'Virtual Account BCA',
        'actual_name' => 'Virtual Account BCA',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 32,
        'name' => 'Virtual Account BRI',
        'actual_name' => 'Virtual Account BRI',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 33,
        'name' => 'Virtual Account Mandiri',
        'actual_name' => 'Virtual Account Mandiri',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => 'mandiri.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 34,
        'name' => 'Virtual Account Indomaret',
        'actual_name' => 'Virtual Account Indomaret',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 35,
        'name' => 'Akulaku',
        'actual_name' => 'Akulaku',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 36,
        'name' => 'Virtual Account BNI',
        'actual_name' => 'Virtual Account BNI',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 37,
        'name' => 'Bank Transfer Cimb Niaga',
        'actual_name' => 'Bank Transfer Cimb Niaga',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'CIMB.png',
        'detail' => json_encode([
          'acc_bank' => 'CIMB Niaga',
          'acc_name' => 'PT. Odeo Teknologi Indonesia',
          'acc_number' => '800-099-841-900',
          'acc_branch' => 'Graha Niaga - Sudirman - Jakarta Selatan'
        ]),
        'is_direct_payment' => false,
        'priority' => 3
      ],
      [
        'id' => 38,
        'name' => 'Virtual Account CIMB',
        'actual_name' => 'Virtual Account CIMB',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 39,
        'name' => 'Debit / Credit Card BNI',
        'actual_name' => 'Debit / Credit Card Present (PAX BNI Switching)',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 98
      ],
      [
        'id' => 40,
        'name' => 'Bank Transfer Permata',
        'actual_name' => 'Bank Transfer Permata',
        'max_transaction' => '999999999',
        'min_transaction' => '10000',
        'logo' => 'permata.png',
        'detail' => json_encode([
          'acc_bank' => 'Permata',
          'acc_name' => 'PT. Odeo Teknologi Indonesia',
          'acc_number' => '018-1247-4444',
          'acc_branch' => 'Green Mansion - Jakarta Barat'
        ]),
        'is_direct_payment' => false,
        'priority' => 3
      ],
      [
        'id' => 41,
        'name' => 'Virtual Account ATM Bersama',
        'actual_name' => 'Virtual Account ATM Bersama',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => 'atmb.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 42,
        'name' => 'Debit / Credit Card Lainnya',
        'actual_name' => 'Debit / Credit Card Present (PAX Arta Jasa Switching)',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => 'CC.png',
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 43,
        'name' => 'Virtual Account Permata',
        'actual_name' => 'Virtual Account Permata',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ],
      [
        'id' => 45,
        'name' => 'Virtual Account Maybank',
        'actual_name' => 'Virtual Account Maybank',
        'max_transaction' => '999999999',
        'min_transaction' => '1',
        'logo' => null,
        'detail' => null,
        'is_direct_payment' => false,
        'priority' => 99
      ]
    ]);
  }
}

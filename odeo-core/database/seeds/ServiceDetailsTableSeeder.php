<?php

use Illuminate\Database\Seeder;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\Vendor;

class ServiceDetailsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('service_details')->insert([
      'id' => ServiceDetail::PLAN_ODEO,
      'service_id' => Service::PLAN,
      'vendor_id' => Vendor::ODEO_INTERNAL
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::TOPUP_ODEO,
      'service_id' => Service::OCASH,
      'vendor_id' => Vendor::ODEO_INTERNAL
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::WARRANTY_ODEO,
      'service_id' => Service::WARRANTY,
      'vendor_id' => Vendor::ODEO_INTERNAL
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PULSA_ODEO,
      'service_id' => Service::PULSA,
      'vendor_id' => Vendor::ODEO,
      'margin' => 2,
      'margin_type' => 'percentage',
      'default_discount' => 1.75,
      'default_hustler_margin' => 0.1,
      'mentor_margin' => 0.1,
      'leader_margin' => 0.1
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PULSA_SEPULSA,
      'service_id' => Service::PULSA,
      'vendor_id' => Vendor::SEPULSA,
      'margin' => 2,
      'margin_type' => 'percentage'
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::HOTEL_TRAVELOKA,
      'service_id' => Service::HOTEL,
      'vendor_id' => Vendor::TRAVELOKA,
      'margin' => 10000,
      'margin_type' => 'value'
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::HOTEL_TIKET,
      'service_id' => Service::HOTEL,
      'vendor_id' => Vendor::TIKETCOM,
      'margin' => -6,
      'margin_type' => 'percentage',
      'default_discount' => 5.25
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::FLIGHT_TRAVELOKA,
      'service_id' => Service::FLIGHT,
      'vendor_id' => Vendor::TRAVELOKA,
      'margin' => 10000,
      'margin_type' => 'value'
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::FLIGHT_TIKET,
      'service_id' => Service::FLIGHT,
      'vendor_id' => Vendor::TIKETCOM,
      'margin' => -10000,
      'margin_type' => 'value',
      'default_discount' => 8750
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PLN_ODEO,
      'service_id' => Service::PLN,
      'vendor_id' => Vendor::ODEO,
      'margin' => 2500,
      'margin_type' => 'value',
      'default_discount' => 2188,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 100,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::BOLT_ODEO,
      'service_id' => Service::BOLT,
      'vendor_id' => Vendor::ODEO,
      'margin' => 2,
      'margin_type' => 'percentage',
      'default_discount' => 1.75,
      'default_hustler_margin' => 0.1,
      'mentor_margin' => 0.1,
      'leader_margin' => 0.1
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::DOMAIN_ODEO,
      'service_id' => Service::DOMAIN,
      'vendor_id' => Vendor::ODEO_INTERNAL
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::DEPOSIT_ODEO,
      'service_id' => Service::ODEPOSIT,
      'vendor_id' => Vendor::ODEO_INTERNAL
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::CREDIT_BILL_ODEO,
      'service_id' => Service::CREDIT_BILL,
      'vendor_id' => Vendor::ODEO,
      'margin' => -1000,
      'margin_type' => 'value',
      'default_discount' => 0.00,
      'default_hustler_margin' => 750,
      'mentor_margin' => 150,
      'leader_margin' => 100
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PAKET_DATA_ODEO,
      'service_id' => Service::PAKET_DATA,
      'vendor_id' => Vendor::ODEO,
      'margin' => 2,
      'margin_type' => 'percentage',
      'default_discount' => 1.75,
      'default_hustler_margin' => 0.1,
      'mentor_margin' => 0.1,
      'leader_margin' => 0.1
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PLN_POSTPAID_ODEO,
      'service_id' => Service::PLN_POSTPAID,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PLN_NONTAGLIS_ODEO,
      'service_id' => Service::PLN_NONTAGLIS,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::BPJS_KES_ODEO,
      'service_id' => Service::BPJS_KES,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::BPJS_TK_ODEO,
      'service_id' => Service::BPJS_TK,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PULSA_POSTPAID_ODEO,
      'service_id' => Service::PULSA_POSTPAID,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::BROADBAND_ODEO,
      'service_id' => Service::BROADBAND,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::LANDLINE_ODEO,
      'service_id' => Service::LANDLINE,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PDAM_ODEO,
      'service_id' => Service::PDAM,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::USER_INVOICE_ODEO,
      'service_id' => Service::USER_INVOICE,
      'vendor_id' => Vendor::ODEO_INTERNAL,
      'margin' => 0,
      'margin_type' => 'percentage',
      'default_discount' => 0,
      'default_hustler_margin' => 0,
      'mentor_margin' => 0,
      'leader_margin' => 0,
      'max_margin' => 0
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::GAME_VOUCHER_ODEO,
      'service_id' => Service::GAME_VOUCHER,
      'vendor_id' => Vendor::ODEO,
      'margin' => 2500,
      'margin_type' => 'value',
      'default_discount' => 2188,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 100,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::GOOGLE_PLAY_ODEO,
      'service_id' => Service::GOOGLE_PLAY,
      'vendor_id' => Vendor::ODEO,
      'margin' => 2500,
      'margin_type' => 'value',
      'default_discount' => 2188,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 100,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::TRANSPORTATION_ODEO,
      'service_id' => Service::TRANSPORTATION,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 0,
      'default_hustler_margin' => 50,
      'mentor_margin' => 30,
      'leader_margin' => 20,
      'max_margin' => 100
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::MULTI_FINANCE_ODEO,
      'service_id' => Service::MULTI_FINANCE,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::MD_PLUS_ODEO,
      'service_id' => Service::MD_PLUS,
      'vendor_id' => Vendor::ODEO_INTERNAL,
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PAYMENT_GATEWAY,
      'service_id' => Service::PAYMENT_GATEWAY,
      'vendor_id' => Vendor::ODEO_INTERNAL,
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::DDLENDING_LOAN,
      'service_id' => Service::DDLENDING_LOAN,
      'vendor_id' => Vendor::ODEO_INTERNAL,
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::DDLENDING_PAYMENT,
      'service_id' => Service::DDLENDING_PAYMENT,
      'vendor_id' => Vendor::ODEO_INTERNAL,
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::PGN_ODEO,
      'service_id' => Service::PGN,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);

    DB::table('service_details')->insert([
      'id' => ServiceDetail::EMONEY_ODEO,
      'service_id' => Service::EMONEY,
      'vendor_id' => Vendor::ODEO,
      'margin' => 100,
      'margin_type' => 'value',
      'default_discount' => 87.5,
      'default_hustler_margin' => 100,
      'mentor_margin' => 100,
      'leader_margin' => 50,
      'max_margin' => 2500
    ]);
  }
}

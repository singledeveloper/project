<?php

use Illuminate\Database\Seeder;

class VendorDisbursementsTableSeeder extends Seeder {


  public function run() {

    DB::table('vendor_disbursements')->insert([
      [
        'id' => \Odeo\Domains\Constant\VendorDisbursement::BCA,
        'name' => 'BCA',
        'balance' => 0,
      ],
      [
        'id' => \Odeo\Domains\Constant\VendorDisbursement::FLIP,
        'name' => 'FLIP',
        'balance' => 0,
      ],
      [
        'id' => \Odeo\Domains\Constant\VendorDisbursement::MANDIRI,
        'name' => 'MANDIRI',
        'balance' => 0
      ]
    ]);


  }
}

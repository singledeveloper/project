<?php

use Illuminate\Database\Seeder;
use Odeo\Domains\Constant\Service;

class ServiceTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('services')->insert([
      'id' => Service::PLAN,
      'name' => 'plan',
      'displayed_name' => 'oCommerce'
    ]);
    DB::table('services')->insert([
      'id' => Service::OCASH,
      'name' => 'credit',
      'displayed_name' => 'oCash'
    ]);
    DB::table('services')->insert([
      'id' => Service::WARRANTY,
      'name' => 'warranty',
      'displayed_name' => 'Odeo Safe+'
    ]);
    DB::table('services')->insert([
      'id' => Service::PULSA,
      'name' => 'pulsa',
      'displayed_name' => 'Pulsa',
      'minimum_deposit' => 10000
    ]);
    DB::table('services')->insert([
      'id' => Service::HOTEL,
      'name' => 'hotel',
      'displayed_name' => 'Tiket Hotel',
      'minimum_deposit' => 200000
    ]);
    DB::table('services')->insert([
      'id' => Service::FLIGHT,
      'name' => 'flight',
      'displayed_name' => 'Tiket Pesawat',
      'minimum_deposit' => 400000
    ]);
    DB::table('services')->insert([
      'id' => Service::PLN,
      'name' => 'pln',
      'displayed_name' => 'Token PLN',
      'minimum_deposit' => 100000
    ]);
    DB::table('services')->insert([
      'id' => Service::BOLT,
      'name' => 'bolt',
      'displayed_name' => 'Pulsa BOLT',
      'minimum_deposit' => 100000
    ]);
    DB::table('services')->insert([
      'id' => Service::DOMAIN,
      'name' => 'domain',
      'displayed_name' => 'oCommerce Domain'
    ]);
    DB::table('services')->insert([
      'id' => Service::ODEPOSIT,
      'name' => 'deposit',
      'displayed_name' => 'oDeposit'
    ]);
    DB::table('services')->insert([
      'id' => Service::CREDIT_BILL,
      'name' => 'credit_bill',
      'displayed_name' => 'Credit Bill'
    ]);
    DB::table('services')->insert([
      'id' => Service::PAKET_DATA,
      'name' => 'paket_data',
      'displayed_name' => 'Data'
    ]);
    DB::table('services')->insert([
      'id' => Service::PLN_POSTPAID,
      'name' => 'pln_postpaid',
      'displayed_name' => 'PLN Pascabayar'
    ]);
    DB::table('services')->insert([
      'id' => Service::PLN_NONTAGLIS,
      'name' => 'pln_nontaglis',
      'displayed_name' => 'PLN Nontaglis'
    ]);
    DB::table('services')->insert([
      'id' => Service::BPJS_KES,
      'name' => 'bpjs_kes',
      'displayed_name' => 'BJPS Kesehatan'
    ]);
    DB::table('services')->insert([
      'id' => Service::BPJS_TK,
      'name' => 'bpjs_tk',
      'displayed_name' => 'BJPS Ketenagakerjaan'
    ]);
    DB::table('services')->insert([
      'id' => Service::PULSA_POSTPAID,
      'name' => 'pulsa_postpaid',
      'displayed_name' => 'Pulsa Pascabayar'
    ]);
    DB::table('services')->insert([
      'id' => Service::BROADBAND,
      'name' => 'broadband',
      'displayed_name' => 'Internet'
    ]);
    DB::table('services')->insert([
      'id' => Service::LANDLINE,
      'name' => 'landline',
      'displayed_name' => 'Telepon Rumah'
    ]);
    DB::table('services')->insert([
      'id' => Service::PDAM,
      'name' => 'pdam',
      'displayed_name' => 'PDAM'
    ]);
    DB::table('services')->insert([
      'id' => Service::USER_INVOICE,
      'name' => 'user_invoice',
      'displayed_name' => 'Tagihan Lainnya'
    ]);
    DB::table('services')->insert([
      'id' => Service::GAME_VOUCHER,
      'name' => 'game_voucher',
      'displayed_name' => 'Voucher Game'
    ]);
    DB::table('services')->insert([
      'id' => Service::GOOGLE_PLAY,
      'name' => 'google_play',
      'displayed_name' => 'Kode Voucher Google Play'
    ]);
    DB::table('services')->insert([
      'id' => Service::TRANSPORTATION,
      'name' => 'transport',
      'displayed_name' => 'Transportasi'
    ]);
    DB::table('services')->insert([
      'id' => Service::MULTI_FINANCE,
      'name' => 'multi_finance',
      'displayed_name' => 'Multi Finance'
    ]);
    DB::table('services')->insert([
      'id' => Service::MD_PLUS,
      'name' => 'md_plus',
      'displayed_name' => 'MD Plus'
    ]);
    DB::table('services')->insert([
      'id' => Service::PAYMENT_GATEWAY,
      'name' => 'payment_gateway',
      'displayed_name' => 'Payment Gateway'
    ]);
    DB::table('services')->insert([
      'id' => Service::DDLENDING_LOAN,
      'name' => 'ddlending_loan',
      'displayed_name' => 'Dana Darurat Landing'
    ]);
    DB::table('services')->insert([
      'id' => Service::DDLENDING_PAYMENT,
      'name' => 'ddlending_payment',
      'displayed_name' => 'Dana Darurat Payment'
    ]);
    DB::table('services')->insert([
      'id' => Service::PGN,
      'name' => 'pgn',
      'displayed_name' => 'Perusahaan Gas Negara'
    ]);
    DB::table('services')->insert([
      'id' => Service::EMONEY,
      'name' => 'emoney',
      'displayed_name' => 'eMoney'
    ]);
  }
}

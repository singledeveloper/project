<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PaymentGatewayChannelsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'bca_closed',
      'va_vendor_id' => 6,
      'default_settlement_type' => '2111113',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'bca_open',
      'va_vendor_id' => 7,
      'default_settlement_type' => '2111113',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'alfa',
      'va_vendor_id' => 8,
      'default_settlement_type' => '5777776',
      'default_settlement_fee' => '0.15',
      'default_fee' => '7500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'atm_bersama',
      'va_vendor_id' => 9,
      'default_settlement_type' => '1111132',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'bri',
      'va_vendor_id' => 2,
      'default_settlement_type' => '1111111',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'cimb',
      'va_vendor_id' => 10,
      'default_settlement_type' => '1111111',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'permata',
      'va_vendor_id' => 11,
      'default_settlement_type' => '1111111',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'mandiri',
      'va_vendor_id' => 12,
      'default_settlement_type' => '2111113',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
    DB::table('payment_gateway_channels')->insert([
      'channel_name' => 'maybank',
      'va_vendor_id' => 13,
      'default_settlement_type' => '2111113',
      'default_settlement_fee' => '0.15',
      'default_fee' => '5500',
    ]);
  }
}

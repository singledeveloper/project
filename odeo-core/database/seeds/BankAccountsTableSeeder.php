<?php

use Illuminate\Database\Seeder;

class BankAccountsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('user_bank_accounts')->insert(
      [
        'user_id' => 1,
        'bank_id' => 1,
        'account_name' => 'Brian Japutra',
        'account_number' => '5270123456'
      ]
    );
    
    DB::table('user_bank_accounts')->insert(
      [
        'user_id' => 2,
        'bank_id' => 1,
        'account_name' => 'Brian Japutra',
        'account_number' => '5270123456'
      ]
    );
    
    DB::table('user_bank_accounts')->insert(
      [
        'user_id' => 3,
        'bank_id' => 1,
        'account_name' => 'Brian Japutra',
        'account_number' => '5270123456'
      ]
    );
  }
}

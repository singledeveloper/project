<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlinesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('tiket_airlines', function (Blueprint $table) {
      $table->increments('id');
      $table->char('code', 2)->unique();
      $table->string('name', 50);
      $table->string('logo_path');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('tiket_airlines');
  }
}

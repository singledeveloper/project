<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeoInventoriesAddPulsaOdeoIdRemoveSomeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('pulsa_odeo_inventories', function(Blueprint $table) {
        $table->dropColumn('operator_id');
        $table->integer('pulsa_odeo_id')->unsigned()->after('id')->nullable();
        $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('cascade');
        $table->dropColumn("category");
        $table->dropColumn('nominal');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pulsa_odeo_inventories', function(Blueprint $table) {
        $table->integer('operator_id')->unsigned();
        $table->foreign('operator_id')->references('id')->on('pulsa_operators')->onDelete('cascade');
        $table->dropColumn('pulsa_odeo_id');
        $table->string("category", 50)->default('');
        $table->integer('nominal');
      });
    }
}

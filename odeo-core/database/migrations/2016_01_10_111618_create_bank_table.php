<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('banks', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->integer('priority');
      $table->string('clearing_code', 20)->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('banks');
  }
}

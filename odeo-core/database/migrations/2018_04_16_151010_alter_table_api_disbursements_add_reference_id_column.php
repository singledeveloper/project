<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementsAddReferenceIdColumn extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->string('reference_id', 32)->nullable();
      $table->index(['user_id', 'reference_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->dropIndex(['user_id', 'reference_id']);
      $table->dropColumn('reference_id');
    });
  }
}

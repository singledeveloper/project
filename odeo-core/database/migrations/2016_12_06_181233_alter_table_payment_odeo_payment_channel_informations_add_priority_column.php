<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentOdeoPaymentChannelInformationsAddPriorityColumn extends Migration {

  public function up() {
    Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->integer('priority')->nullable();
    });
  }

  public function down() {
    Schema::table('payment_odeo_payment_channel_informations', function (Blueprint $table) {
      $table->dropColumn('priority');
    });
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBatchDisbursementsAddAmountAndCount extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('batch_disbursements', function (Blueprint $table) {
      $table->decimal('total_amount', 17, 2);
      $table->decimal('total_fee', 17, 2);
      $table->integer('count');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlnInquiries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pln_inquiries', function(Blueprint $table){
          $table->increments('id');
          $table->string('number', 20);
          $table->string('subscriber_id', 20);
          $table->string('name', 50);
          $table->string('tariff', 10);
          $table->string('power', 10);
          $table->string('area', 50);
          $table->text('log');
          $table->timestamp('expired_at');
          $table->timestamps();
          $table->index(['number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandiriBillPaymentInquiryTable extends Migration
{
    public function up()
    {
      Schema::create('payment_mandiri_va_inquiries', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('trx_date_time');
        $table->string('transmission_date_time');
        $table->string('company_code');
        $table->string('channel_id');
        $table->string('bill_key_1');
        $table->string('bill_key_2');
        $table->string('bill_key_3')->nullable();
        $table->string('reference_1')->nullable();
        $table->string('reference_2')->nullable();
        $table->string('reference_3')->nullable();

        $table->string('bill_name')->nullable();
        $table->decimal('bill_amount', 17, 2)->nullable();
        $table->string('bill_info_1')->nullable();
        $table->string('bill_info_2')->nullable();

        $table->index('bill_key_1');
        $table->timestamps();
      });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceCategoriesTable extends Migration {

  public function up() {
    Schema::create('invoice_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->integer('parent_category')->nullable();
    });
  }


  public function down() {
    Schema::drop('invoice_categories');
  }
}

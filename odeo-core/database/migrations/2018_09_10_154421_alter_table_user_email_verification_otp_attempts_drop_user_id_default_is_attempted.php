<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserEmailVerificationOtpAttemptsDropUserIdDefaultIsAttempted extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('user_email_verification_otp_attempts', function (Blueprint $table) {
      $table->dropColumn('user_id');
      $table->boolean('is_attempted')->default(true)->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}

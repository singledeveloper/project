<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentGatewayInvoicePaymentsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_gateway_invoice_payments', function(Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('virtual_account_number');
        $table->string('customer_name');
        $table->string('description');
        $table->string('reference');
        $table->decimal('amount', 17, 2);
        $table->string('vendor');
        $table->bigInteger('order_id')->nullable();
        $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
        $table->timestamp('paid_at')->nullable();

        $table->index(['reference', 'vendor']);

        $table->timestamps();
      });
    }

    public function down()
    {
        //
    }
}

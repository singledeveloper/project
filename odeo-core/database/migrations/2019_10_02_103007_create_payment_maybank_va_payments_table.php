<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMaybankVaPaymentsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_maybank_va_payments', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('bin_number');
        $table->string('va_no');
        $table->decimal('amount', 17, 2);
        $table->string('currency');
        $table->string('delivery_channel_type');
        $table->string('trace_no');
        $table->string('transmission_date_time');
        $table->string('terminal_id');
        $table->string('reference');
        $table->string('status')->nullable();

        $table->bigInteger('inquiry_id');
        $table->foreign('inquiry_id')->references('id')->on('payment_maybank_va_inquiries');

        $table->bigInteger('payment_id')->nullable();
        $table->foreign('payment_id')->references('id')->on('payments');
        $table->timestamps();

        $table->index(['va_no', 'reference']);
      });
    }

    public function down()
    {
        //
    }
}

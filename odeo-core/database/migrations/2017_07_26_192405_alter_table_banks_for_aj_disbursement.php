<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBanksForAjDisbursement extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    DB::beginTransaction();
    Schema::table('banks', function (Blueprint $table) {
      $table->string('aj_bank_code')->nullable();
      $table->string('aj_regency_code')->nullable();
    });

    $this->update();
    DB::commit();
  }

  private function update() {
    $sql =<<<UPDATE
update banks set aj_bank_code='009', aj_regency_code='JKT' where id=2;
update banks set aj_bank_code='002', aj_regency_code='JKT' where id=3;
update banks set aj_bank_code='200', aj_regency_code='JKT' where id=116;
update banks set aj_bank_code='008', aj_regency_code='JKT' where id=4;
update banks set aj_bank_code='088', aj_regency_code='SBY' where id=74;
update banks set aj_bank_code='037', aj_regency_code='JKT' where id=27;
update banks set aj_bank_code='441', aj_regency_code='JKT' where id=5;
update banks set aj_bank_code='076', aj_regency_code='JKT' where id=119;
update banks set aj_bank_code='014', aj_regency_code='JKT' where id=1;
update banks set aj_bank_code='022', aj_regency_code='JKT' where id=6;
update banks set aj_bank_code='011', aj_regency_code='JKT' where id=7;
update banks set aj_bank_code='087', aj_regency_code='JKT' where id=138;
update banks set aj_bank_code='161', aj_regency_code='JKT' where id=92;
update banks set aj_bank_code='484', aj_regency_code='JKT' where id=8;
update banks set aj_bank_code='485', aj_regency_code='JKT' where id=94;
update banks set aj_bank_code='164', aj_regency_code='JKT' where id=132;
update banks set aj_bank_code='555', aj_regency_code='JKT' where id=81;
update banks set aj_bank_code='016', aj_regency_code='JKT' where id=17;
update banks set aj_bank_code='157', aj_regency_code='SBY' where id=77;
update banks set aj_bank_code='097', aj_regency_code='JKT' where id=139;
update banks set aj_bank_code='506', aj_regency_code='JKT' where id=9;
update banks set aj_bank_code='426', aj_regency_code='JKT' where id=9;
update banks set aj_bank_code='151', aj_regency_code='MDN' where id=54;
update banks set aj_bank_code='152', aj_regency_code='JKT' where id=114;
update banks set aj_bank_code='147', aj_regency_code='JKT' where id=20;
update banks set aj_bank_code='095', aj_regency_code='JKT' where id=103;
update banks set aj_bank_code='145', aj_regency_code='BDG' where id=36;
update banks set aj_bank_code='028', aj_regency_code='JKT' where id=10;
update banks set aj_bank_code='146', aj_regency_code='JKT' where id=113;
update banks set aj_bank_code='013', aj_regency_code='JKT' where id=11;
update banks set aj_bank_code='494', aj_regency_code='JKT' where id=131;
update banks set aj_bank_code='498', aj_regency_code='JKT' where id=106;
update banks set aj_bank_code='153', aj_regency_code='JKT' where id=12;
update banks set aj_bank_code='451', aj_regency_code='JKT' where id=13;
update banks set aj_bank_code='023', aj_regency_code='JKT' where id=18;
update banks set aj_bank_code='019', aj_regency_code='JKT' where id=15;
update banks set aj_bank_code='167', aj_regency_code='JKT' where id=115;
update banks set aj_bank_code='531', aj_regency_code='SBY' where id=73;
update banks set aj_bank_code='466', aj_regency_code='JKT' where id=44;
update banks set aj_bank_code='542', aj_regency_code='BDG' where id=34;
update banks set aj_bank_code='536', aj_regency_code='JKT' where id=29;
update banks set aj_bank_code='567', aj_regency_code='BDG' where id=35;
update banks set aj_bank_code='422', aj_regency_code='JKT' where id=22;
update banks set aj_bank_code='526', aj_regency_code='JKT' where id=109;
update banks set aj_bank_code='562', aj_regency_code='BDG' where id=91;
update banks set aj_bank_code='567', aj_regency_code='JKT' where id=142;
update banks set aj_bank_code='513', aj_regency_code='JKT' where id=107;
update banks set aj_bank_code='425', aj_regency_code='BDG' where id=38;
update banks set aj_bank_code='472', aj_regency_code='JKT' where id=80;
update banks set aj_bank_code='535', aj_regency_code='JKT' where id=110;
update banks set aj_bank_code='553', aj_regency_code='JKT' where id=90;
update banks set aj_bank_code='491', aj_regency_code='JKT' where id=82;
update banks set aj_bank_code='548', aj_regency_code='JKT' where id=96;
update banks set aj_bank_code='503', aj_regency_code='JKT' where id=83;
update banks set aj_bank_code='517', aj_regency_code='JKT' where id=108;
update banks set aj_bank_code='558', aj_regency_code='JKT' where id=86;
update banks set aj_bank_code='501', aj_regency_code='JKT' where id=95;
update banks set aj_bank_code='547', aj_regency_code='SMG' where id=68;
update banks set aj_bank_code='523', aj_regency_code='JKT' where id=84;
update banks set aj_bank_code='564', aj_regency_code='DPR' where id=43;
update banks set aj_bank_code='774', aj_regency_code='JKT' where id=5;
update banks set aj_bank_code='213', aj_regency_code='JKT' where id=68;
update banks set aj_bank_code='566', aj_regency_code='JKT' where id=111;
update banks set aj_bank_code='405', aj_regency_code='JKT' where id=93;
update banks set aj_bank_code='490', aj_regency_code='JKT' where id=105;
update banks set aj_bank_code='559', aj_regency_code='SBY' where id=76;
update banks set aj_bank_code='520', aj_regency_code='SBY' where id=78;
update banks set aj_bank_code='135', aj_regency_code='KDI' where id=48;
update banks set aj_bank_code='112', aj_regency_code='YKK' where id=71;
update banks set aj_bank_code='124', aj_regency_code='SMR' where id=67;
update banks set aj_bank_code='111', aj_regency_code='JKT' where id=121;
update banks set aj_bank_code='116', aj_regency_code='BNA' where id=32;
update banks set aj_bank_code='125', aj_regency_code='PLK' where id=59;
update banks set aj_bank_code='115', aj_regency_code='JMB' where id=46;
update banks set aj_bank_code='126', aj_regency_code='MKS' where id=50;
update banks set aj_bank_code='121', aj_regency_code='BDL' where id=33;
update banks set aj_bank_code='119', aj_regency_code='PBR' where id=63;
update banks set aj_bank_code='118', aj_regency_code='PAD' where id=57;
update banks set aj_bank_code='110', aj_regency_code='BDG' where id=37;
update banks set aj_bank_code='131', aj_regency_code='AMB' where id=30;
update banks set aj_bank_code='133', aj_regency_code='BGL' where id=42;
update banks set aj_bank_code='113', aj_regency_code='SMG' where id=70;
update banks set aj_bank_code='114', aj_regency_code='SBY' where id=76;
update banks set aj_bank_code='123', aj_regency_code='PTK' where id=64;
update banks set aj_bank_code='128', aj_regency_code='MTR' where id=53;
update banks set aj_bank_code='130', aj_regency_code='KPG' where id=49;
update banks set aj_bank_code='134', aj_regency_code='PAL' where id=62;
update banks set aj_bank_code='127', aj_regency_code='MND' where id=52;
update banks set aj_bank_code='129', aj_regency_code='DPR' where id=45;
update banks set aj_bank_code='122', aj_regency_code='BJM' where id=41;
update banks set aj_bank_code='132', aj_regency_code='JAP' where id=47;
update banks set aj_bank_code='120', aj_regency_code='PLG' where id=60;
update banks set aj_bank_code='117', aj_regency_code='MDN' where id=56;
update banks set aj_bank_code='950', aj_regency_code='JKT' where id=16;
update banks set aj_bank_code='945', aj_regency_code='JKT' where id=112;
update banks set aj_bank_code='061', aj_regency_code='JKT' where id=14;
update banks set aj_bank_code='057', aj_regency_code='JKT' where id=101;
update banks set aj_bank_code='054', aj_regency_code='JKT' where id=140;
update banks set aj_bank_code='046', aj_regency_code='JKT' where id=122;
update banks set aj_bank_code='777', aj_regency_code='JKT' where id=130;
update banks set aj_bank_code='048', aj_regency_code='JKT' where id=123;
update banks set aj_bank_code='089', aj_regency_code='JKT' where id=120;
update banks set aj_bank_code='047', aj_regency_code='JKT' where id=99;
update banks set aj_bank_code='036', aj_regency_code='JKT' where id=89;
update banks set aj_bank_code='212', aj_regency_code='JKT' where id=39;
update banks set aj_bank_code='949', aj_regency_code='JKT' where id=136;
update banks set aj_bank_code='045', aj_regency_code='JKT' where id=129;
update banks set aj_bank_code='033', aj_regency_code='JKT' where id=126;
update banks set aj_bank_code='069', aj_regency_code='JKT' where id=135;
update banks set aj_bank_code='031', aj_regency_code='JKT' where id=24;
update banks set aj_bank_code='067', aj_regency_code='JKT' where id=102;
update banks set aj_bank_code='032', aj_regency_code='JKT' where id=134;
update banks set aj_bank_code='050', aj_regency_code='JKT' where id=100;
update banks set aj_bank_code='040', aj_regency_code='JKT' where id=127;
update banks set aj_bank_code='042', aj_regency_code='JKT' where id=98;
update banks set aj_bank_code='041', aj_regency_code='JKT' where id=128;
update banks set aj_bank_code='052', aj_regency_code='JKT' where id=124;
UPDATE;

    DB::unprepared($sql);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('banks', function (Blueprint $table) {
      $table->dropColumn('aj_bank_code');
      $table->dropColumn('aj_regency_code');
    });
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSmsCenterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sms_center_users', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('user_id')->nullable();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        $table->string('telephone', 20)->unique();
        $table->char('pin', 60);
        $table->integer('pin_try_counts')->default(0);
        $table->timestamps();
      });

      Schema::create('sms_center_histories', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('order_id')->nullable();
        $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        $table->bigInteger('sms_center_user_id');
        $table->foreign('sms_center_user_id')->references('id')->on('sms_center_users')->onDelete('cascade');
        $table->text('message');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

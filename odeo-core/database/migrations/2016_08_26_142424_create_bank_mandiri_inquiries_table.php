<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankMandiriInquiriesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('bank_mandiri_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->integer('bank_scrape_account_number_id')->unsigned();
      $table->foreign('bank_scrape_account_number_id')
        ->references('id')
        ->on('bank_scrape_account_numbers');

      $table->string('date')->nullable();
      $table->string('date_value')->nullable();
      $table->string('description')->nullable();
      $table->string('reference_number')->nullable();
      $table->string('debit')->nullable();
      $table->string('credit')->nullable();
      $table->string('balance')->nullable();

      $table->string('reference_type')->nullable();
      $table->text('reference')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('bank_mandiri_inquiries');
  }
}

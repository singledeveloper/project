<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOrderTable extends Migration {

  public function up() {

    Schema::create('orders', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('seller_store_id')->unsigned()->nullable();
      $table->foreign('seller_store_id')->references('id')->on('stores')->onDelete('set null');
      $table->tinyInteger('platform_id');
      $table->tinyInteger('gateway_id')->nullable();

      $table->string('salutation')->nullable();
      $table->string('name')->nullable();
      $table->string('email')->nullable();
      $table->string('phone_number')->nullable();

      $table->decimal('subtotal', 17, 2);
      $table->decimal('total', 17, 2);
      $table->char('status', 5)->comment = "10000 = new; 10001 = waiting payment; 20001 = waiting approval; 30001 = processing items; 50000 = completed; 90000 = cancelled";

      $table->timestamp('created_at');
      $table->timestamp('opened_at')->nullable();
      $table->timestamp('paid_at')->nullable();
      $table->timestamp('expired_at')->nullable();
      $table->timestamp('closed_at')->nullable();

    });

  }


  public function down() {
    Schema::drop('orders');
  }
}

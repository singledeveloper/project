<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableJabberUserTelegramUserAddPinTryCounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('jabber_users', function(Blueprint $table){
        $table->integer('pin_try_counts')->default(0);
      });

      Schema::table('telegram_users', function(Blueprint $table){
        $table->integer('pin_try_counts')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAffiliateUserInvoiceVoidLogs extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('affiliate_user_invoice_void_logs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_id');
      $table->text('response');
      $table->integer('response_code');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

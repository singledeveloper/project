<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserReferralCodesAddStoreReferredIdColumn extends Migration {

  public function up() {
    Schema::table('user_referral_codes', function (Blueprint $table) {
      $table->bigInteger('store_referred_id')->unsigned()->nullable();
      $table->foreign('store_referred_id')->references('id')->on('stores')->onDelete('cascade');
    });
  }

  public function down() {
    Schema::table('user_referral_codes', function (Blueprint $table) {
      $table->dropColumn('store_referred_id');
    });
  }
}

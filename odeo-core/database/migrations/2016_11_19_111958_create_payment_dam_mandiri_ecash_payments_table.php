<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDamMandiriEcashPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_dam_mandiri_ecash_payments', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('ecash_id');
          $table->string('finish_redirect_url')->nullable();
          $table->string('unfinish_redirect_url')->nullable();
          $table->string('phone_number')->nullable();
          $table->string('status_id')->nullable()->unique();
          $table->string('status')->nullable();
          $table->string('trace_number')->nullable();
          $table->bigInteger('merchant_trx_id')->nullable();
          $table->timestamp('notified_date_time')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_dam_mandiri_ecash_payments');
    }
}

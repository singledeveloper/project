<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankBcaInquiriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bank_bca_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->integer('bank_scrape_account_number_id')->unsigned();
      $table->foreign('bank_scrape_account_number_id')
        ->references('id')
        ->on('bank_scrape_account_numbers');

      $table->string('date');
      $table->string('description');
      $table->string('branch');
      $table->string('amount');
      $table->string('balance');
      $table->string('pending_scrapped_date')->nullable();

      $table->string('reference_type')->nullable();
      $table->text('reference')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('bank_bca_inquiries');
  }
}

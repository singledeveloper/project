<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAffiliateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_orders', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
          $table->bigInteger('order_id')->nullable();
          $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
          $table->text('request_log');
          $table->string('trx_id', 255)->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

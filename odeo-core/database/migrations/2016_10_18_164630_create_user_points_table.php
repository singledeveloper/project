<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPointsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('user_points', function (Blueprint $table) {
      $table->bigInteger('user_id')->unsigned()->primary();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->integer('point');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('user_points');
  }
}

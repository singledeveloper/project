<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentGatewayInvoicePaymentsAddSettlements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payment_gateway_invoice_payments', function(Blueprint $table){
        $table->bigInteger('invoice_id')->nullable();
        $table->foreign('invoice_id')->references('id')->on('affiliate_user_invoices')->onDelete('set null');
        $table->timestamp('settlement_at')->nullable();
        $table->string('settlement_status')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

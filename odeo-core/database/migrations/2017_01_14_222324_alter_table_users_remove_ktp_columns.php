<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersRemoveKtpColumns extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('ktp_nik');
      $table->dropColumn('ktp_image_url');
    });
  }

  public function down() {
    Schema::table('users', function (Blueprint $table) {
      $table->string('ktp_nik')->nullable();
      $table->string('ktp_image_url')->nullable();
    });
  }
}

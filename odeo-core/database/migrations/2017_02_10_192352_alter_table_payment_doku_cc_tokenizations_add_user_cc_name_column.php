<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTablePaymentDokuCcTokenizationsAddUserCcNameColumn extends Migration {

  public function up() {
    Schema::table('payment_doku_cc_tokenizations', function (Blueprint $table) {
      $table->string('user_cc_name')->nullable();
    });
  }

  public function down() {
    Schema::table('payment_doku_cc_tokenizations', function (Blueprint $table) {
      $table->dropColumn('user_cc_name');
    });
  }
}

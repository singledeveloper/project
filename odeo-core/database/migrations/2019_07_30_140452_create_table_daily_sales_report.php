<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDailySalesReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_sales_reports', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->string('type', 255);
          $table->char('sales_date', 10);
          $table->string('group_type', 255);
          $table->string('value', 255);
          $table->timestamp('created_at');
          $table->index(['type']);
          $table->index(['sales_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserWithdrawsAddReferenceId extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    \Illuminate\Support\Facades\DB::transaction(function () {
      Schema::table('user_withdraws', function (Blueprint $table) {
        $table->string('reference_id', 60)->nullable();
      });

      \Illuminate\Support\Facades\DB::statement("
        CREATE UNIQUE INDEX user_withdraws_user_id_reference_id_unique_index 
        ON user_withdraws (user_id, reference_id)");
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

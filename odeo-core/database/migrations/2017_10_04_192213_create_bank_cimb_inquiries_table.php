<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankCimbInquiriesTable extends Migration {

  public function up() {
    Schema::create('bank_cimb_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->integer('bank_scrape_account_number_id')->unsigned();
      $table->foreign('bank_scrape_account_number_id')
        ->references('id')
        ->on('bank_scrape_account_numbers');

      $table->string('bank_reff_no')->nullable();
      $table->text('description')->nullable();
      $table->dateTime('date')->nullable();
      $table->decimal('debit', 17, 2)->default(0);
      $table->decimal('credit', 17, 2)->default(0);
      $table->string('reference')->nullable();
      $table->text('reference_type')->nullable();

      $table->index(['bank_scrape_account_number_id']);

      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('bank_cimb_inquiries');
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBankAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_bank_accounts', function(Blueprint $table)
      {
        $table->increments('id');
        $table->bigInteger('user_id');
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->integer('bank_id');
        $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
        $table->string('account_name');
        $table->string('account_number');
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('user_bank_accounts');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderReconciliations extends Migration {

  public function up() {
    Schema::create('order_reconciliations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_id');
      $table->foreign('order_id')->references('id')->on('orders');
      $table->index('order_id');
      $table->integer('service_id')->nullable();
      $table->foreign('service_id')->references('id')->on('services');
      $table->integer('platform_id')->nullable();
      $table->index('service_id');
      $table->string('payment_name')->nullable();
      $table->decimal('payment_amount', 17, 2)->default(0);
      $table->decimal('settlement_amount', 17, 2)->default(0);
      $table->decimal('merchant_cost', 17, 2)->default(0);
      $table->text('inventory_detail')->default('');
      $table->char('order_status', 5)->nullable();
      $table->decimal('total_cash_other', 17, 2)->default(0);
      $table->decimal('total_cash', 17, 2)->default(0);
      $table->decimal('total_deposit', 17, 2)->default(0);
      $table->decimal('total_marketing', 17, 2)->default(0);
      $table->decimal('total_mutation', 17, 2)->default(0);
      $table->decimal('total_diff', 17, 2)->default(0);
      $table->string('diff_reason')->nullable();
      $table->boolean('is_reconciled');
      $table->timestamps();
      $table->timestamp('settlement_at')->nullable();
    });
  }


  public function down() {
    //
  }
}

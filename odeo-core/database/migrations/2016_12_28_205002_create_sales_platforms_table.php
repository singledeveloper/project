<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesPlatformsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('sales_platform_reports', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('plan_id')->unsigned()->default(0);
      $table->integer('sales')->unsigned()->default(0);
      $table->integer('sales_target')->unsigned()->default(0);
      $table->boolean('is_locked')->default(0);
      $table->date('date');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('sales_platform_reports');
  }
}

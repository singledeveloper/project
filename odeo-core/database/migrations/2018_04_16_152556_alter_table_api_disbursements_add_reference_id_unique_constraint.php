<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementsAddReferenceIdUniqueConstraint extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      DB::statement('update api_disbursements set reference_id=id where reference_id is null');
      $table->string('reference_id')->nullable(false)->change();
      $table->unique(['user_id', 'reference_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('api_disbursements', function (Blueprint $table) {
      $table->dropUnique(['user_id', 'reference_id']);
    });
  }
}

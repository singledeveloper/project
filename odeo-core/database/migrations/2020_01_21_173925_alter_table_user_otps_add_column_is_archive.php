<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserOtpsAddColumnIsArchive extends Migration {

  public function up() {
    Schema::table('user_otps', function (Blueprint $table) {
      $table->boolean('is_archived')->default(false);
    });
  }

  public function down() {
    // Schema::table('user_otps', function (Blueprint $table) {
    //   $table->dropColumn("is_archived");
    // });
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementPermataDisbursementsAddColumn extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('disbursement_permata_disbursements', function (Blueprint $table) {
      $table->string('request_timestamp')->nullable();
      $table->string('transaction_key')->nullable();
      $table->text('trx_desc')->nullable();
      $table->text('trx_desc2')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}

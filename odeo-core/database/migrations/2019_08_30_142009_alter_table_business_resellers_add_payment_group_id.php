<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBusinessResellersAddPaymentGroupId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('business_resellers', function(Blueprint $table){
        $table->integer('payment_group_id')->nullable();
        $table->index(['payment_group_id']);
      });

      Schema::table('business_reseller_transactions', function(Blueprint $table){
        $table->integer('payment_group_id')->nullable();
        $table->index(['payment_group_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

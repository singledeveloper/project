<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbursementFlipMutationDisbursements extends Migration {

  public function up() {
    Schema::create('disbursement_flip_mutation_disbursements', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('disbursement_flip_disbursement_id')->unsigned()->nullable();
      $table->foreign('disbursement_flip_disbursement_id')->references('id')->on('disbursement_flip_disbursements')->onDelete('set null');

      $table->decimal('amount', 17, 2);
      $table->decimal('fee', 17, 2);

      $table->decimal('balance_before', 17, 2);
      $table->decimal('balance_after', 17, 2);

      $table->string('type');

      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('disbursement_flip_mutation_disbursements');
  }
}

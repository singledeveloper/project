<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuCcTokenizationsTable extends Migration {

  public function up() {
    Schema::create('payment_doku_cc_tokenizations', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('token');
      $table->string('mcn');
      $table->string('bank');
      $table->bigInteger('token_payment_id')->unsigned();
      $table->foreign('token_payment_id')->references('id')->on('payment_doku_cc_tokenization_payments')->onDelete('set null');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('payment_doku_cc_tokenizations');
  }
}

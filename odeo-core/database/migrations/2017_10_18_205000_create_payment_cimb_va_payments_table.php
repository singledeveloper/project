<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentCimbVaPaymentsTable extends Migration
{
    public function up()
    {
      Schema::create('payment_cimb_va_payments', function (Blueprint $table) {
        $table->bigIncrements('id');

        $table->bigInteger('order_id');
        $table->foreign('order_id')->references('id')->on('orders');

        $table->string('transaction_id')->nullable();
        $table->string('transaction_date')->nullable();
        $table->string('company_code')->nullable();
        $table->string('customer_key_1')->nullable();
        $table->double('fee')->nullable();
        $table->double('amount')->nullable();
        $table->string('flag_payment_list')->nullable();
        $table->text('log_inquiry')->nullable();
        $table->text('log_inquiry_xml')->nullable();
        $table->text('log_notify')->nullable();
        $table->text('log_notify_xml')->nullable();

        $table->index(['order_id', 'customer_key_1']);

        $table->timestamps();
      });
    }

    public function down()
    {
      Schema::drop('payment_cimb_va_payments');
    }
}

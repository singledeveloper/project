<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeosAddServiceDetailId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pulsa_odeos', function(Blueprint $table){
          $table->integer('operator_id')->nullable()->change();
          $table->integer('service_detail_id')->nullable();
          $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserPurchasesTable extends Migration {

  public function up() {

    Schema::create('user_purchases', function (Blueprint $table) {

      $table->bigIncrements('id');
      $table->date('date');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('service_id');
      $table->foreign('service_id')->references('id')->on('services')->onDelete('set null');
      $table->bigInteger('service_detail_id');
      $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('set null');
      $table->unsignedInteger('info_id');
      $table->foreign('info_id')->references('id')->on('payment_odeo_payment_channel_informations')->onDelete('set null');
      $table->tinyInteger('gateway_id');
      $table->tinyInteger('platform_id');
      $table->integer('sales_quantity')->unsigned()->default(0);
      $table->decimal('sales_amount', 17, 2)->default(0);
      $table->bigInteger('store_id')->unsigned()->nullable();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');

      $table->index(['date', 'user_id', 'service_id', 'service_detail_id', 'info_id', 'gateway_id', 'platform_id']);

      $table->timestamps();

    });
  }


  public function down() {
    Schema::drop('user_purchases');
  }
}

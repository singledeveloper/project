<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeoInventoryFailuresAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pulsa_odeo_inventory_failures', function(Blueprint $table) {
          $table->text('switcher_query_dumps')->nullable();
          $table->integer('reset_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pulsa_odeo_inventory_failures', function(Blueprint $table) {
        $table->dropColumn('inventory_query_dumps');
        $table->integer('reset_count');
      });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementPermataDisbursementsAddIsReconciled extends Migration
{
    public function up()
    {
      Schema::table('disbursement_permata_disbursements', function (Blueprint $table) {
        $table->boolean('is_reconciled')->default(false);
      });
    }

    public function down()
    {
        //
    }
}

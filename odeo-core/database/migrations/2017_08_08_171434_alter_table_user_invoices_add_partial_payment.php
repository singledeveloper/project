<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserInvoicesAddPartialPayment extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('user_invoices', function (Blueprint $table) {
      $table->boolean('enable_partial_payment')->default(false);
      $table->decimal('minimum_payment', 5, 2)->default(0);
      $table->decimal('total_paid', 17, 2)->default(0);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('user_invoices', function (Blueprint $table) {
      $table->dropColumn('enable_partial_payment');
      $table->dropColumn('minimum_payment');
      $table->dropColumn('total_paid');
    });
  }
}

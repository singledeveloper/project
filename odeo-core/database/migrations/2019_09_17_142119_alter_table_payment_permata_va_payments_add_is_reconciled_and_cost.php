<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentPermataVaPaymentsAddIsReconciledAndCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payment_permata_va_payments', function (Blueprint $table) {
        $table->string('reference_number')->default('');
        $table->boolean('is_reconciled')->default(false);
        $table->decimal('cost', 15, 2)->default(1500);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

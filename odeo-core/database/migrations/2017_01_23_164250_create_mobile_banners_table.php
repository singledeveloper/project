<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileBannersTable extends Migration {

  public function up() {
    Schema::create('mobile_banners', function (Blueprint $table) {
      $table->increments('id');
      $table->string('trigger');
      $table->boolean('active')->default(false);
      $table->unsignedInteger('priority')->default(0);
      $table->text('additional')->nullable();
      $table->string('url');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('mobile_banners');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserDisbursementFees extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('user_disbursement_fees', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users');
      $table->integer('bank_id');
      $table->foreign('bank_id')->references('id')->on('banks');
      $table->decimal('fee', 10, 2);
      $table->timestamps();

      $table->unique(['user_id', 'bank_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeoInventoriesAddIsSeededColumn extends Migration {

  public function up() {
    Schema::table('pulsa_odeo_inventories', function (Blueprint $table) {
      $table->boolean('is_seeded')->default(false)->after('is_active');
    });
  }


  public function down() {
    Schema::table('pulsa_odeo_inventories', function (Blueprint $table) {
      $table->dropColumn('is_seeded');
    });
  }
}

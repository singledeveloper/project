<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailPostpaidSwitchersDropColumnBillRest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_postpaid_switchers', function (Blueprint $table) {
        $table->dropColumn('bill_rest');
      });

      Schema::table('order_detail_postpaid_pln_details', function (Blueprint $table) {
        $table->integer('bill_rest')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

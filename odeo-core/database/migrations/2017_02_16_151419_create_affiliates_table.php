<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesTable extends Migration {

  public function up() {
    Schema::create('affiliates', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('secret_key');
      $table->text('whitelist');
      $table->string('notify_url');
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('affiliates');
  }
}

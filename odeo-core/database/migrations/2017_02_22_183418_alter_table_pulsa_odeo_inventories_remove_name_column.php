<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeoInventoriesRemoveNameColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::statement('update pulsa_odeos po set name = poi.name from pulsa_odeo_inventories poi where poi.pulsa_odeo_id = po.id');
      Schema::table('pulsa_odeo_inventories', function(Blueprint $table) {
        $table->dropColumn("name");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pulsa_odeo_inventories', function(Blueprint $table) {
        $table->string("name", 50);
      });
    }
}

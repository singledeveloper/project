<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermataVaInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_permata_va_inquiries', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->string('instcode');
        $table->string('va_number');
        $table->string('trace_no');
        $table->string('trn_date');
        $table->string('del_channel');
        $table->string('cust_name');
        $table->decimal('bill_amount', 17, 2);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIpLocationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('user_ip_locations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users');
      $table->ipAddress('ip');
      $table->string('country');
      $table->string('country_code');
      $table->string('city');
      $table->float('latitude');
      $table->float('longitude');
      $table->integer('accuracy_radius');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
  }
}

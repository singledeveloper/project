<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryMarginMultiplierTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('inventory_margin_multipliers', function (Blueprint $table) {
      $table->integer('service_detail_id');
      $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('set null');
      $table->decimal('margin', 10, 2);
      $table->decimal('multiplier', 5, 2);
      $table->timestamp('created_at');
      $table->primary(['service_detail_id', 'margin']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('inventory_margin_multipliers');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankAccountInquiriesAddColumnDisbursementVendor extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    \DB::transaction(function () {
      Schema::table('bank_account_inquiries', function (Blueprint $table) {
        $table->string('disbursement_vendor')->nullable();
      });
      \DB::statement("update bank_account_inquiries set disbursement_vendor='bni_api_disbursement' where vendor_disbursement_id=5");
      \DB::statement("update bank_account_inquiries set disbursement_vendor='artajasa_api_disbursement' where vendor_disbursement_id=3");
      Schema::table('bank_account_inquiries', function (Blueprint $table) {
        $table->string('disbursement_vendor')->nullable(false)->change();
      });
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }
}

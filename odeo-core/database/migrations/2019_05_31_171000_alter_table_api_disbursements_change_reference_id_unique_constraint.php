<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementsChangeReferenceIdUniqueConstraint extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    \Illuminate\Support\Facades\DB::transaction(function () {
      Schema::table('api_disbursements', function (Blueprint $table) {
        $table->string('reference_id')->nullable()->change();
        $table->dropUnique(['user_id', 'reference_id']);
      });

      \Illuminate\Support\Facades\DB::statement("
        CREATE UNIQUE INDEX api_disbursements_user_id_reference_id_unique_index 
        ON api_disbursements (user_id, reference_id)");
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBriInquiryBalanceSummaryTable extends Migration {

  public function up() {
    Schema::create('bank_bri_inquiry_balance_summaries', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->decimal('balance_change', 17, 2)->nullable();
      $table->decimal('previous_balance', 17, 2)->nullable();
      $table->decimal('balance', 17, 2)->nullable();
      $table->decimal('scrape_total_debit', 17, 2)->nullable();
      $table->decimal('scrape_total_credit', 17, 2)->nullable();
      $table->decimal('scrape_opening_balance', 17, 2)->nullable();
      $table->decimal('scrape_closing_balance', 17, 2)->nullable();
      $table->date('scrape_start_date');
      $table->date('scrape_end_date');
      $table->unsignedBigInteger('first_distinct_id')->nullable();
      $table->char('status', 5);
      $table->timestamps();
    });
  }

  public function down() {
    //
  }
}

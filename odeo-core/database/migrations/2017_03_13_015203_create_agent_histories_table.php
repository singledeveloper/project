<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentHistoriesTable extends Migration {

  public function up() {
    Schema::create('agent_histories', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('agent_id')->unsigned();
      $table->foreign('agent_id')->references('id')->on('agents')->onDelete('set null');

      $table->bigInteger('user_referred_id')->unsigned();
      $table->foreign('user_referred_id')->references('id')->on('users')->onDelete('set null');

      $table->bigInteger('store_referred_id')->unsigned();
      $table->foreign('store_referred_id')->references('id')->on('stores')->onDelete('set null');

      $table->timestamp('requested_at')->nullable();
      $table->timestamp('joined_at')->nullable();
      $table->string('inputted_master_code')->nullable();

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('agent_histories');
  }
}

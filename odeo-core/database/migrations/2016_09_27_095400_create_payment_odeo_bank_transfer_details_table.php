<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOdeoBankTransferDetailsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('payment_odeo_bank_transfer_details', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('account_number');
      $table->string('account_name');
      $table->decimal('transfer_amount', 17, 2);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('payment_odeo_bank_transfer_details');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserBusinessKyc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('user_business_kycs', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('user_id');
        $table->foreign('user_id')->references('id')->on('users');
        $table->string('business_type', 50);
        $table->char('status', 5);
        $table->timestamp('verified_at')->nullable();
        $table->timestamp('rejected_at')->nullable();
        $table->bigInteger('verified_by_user_id')->nullable();
        $table->bigInteger('rejected_by_user_id')->nullable();
        $table->timestamps();
        $table->index(['user_id']);
        $table->index(['business_type']);
      });

      Schema::create('user_business_kyc_options', function(Blueprint $table){
        $table->increments('id');
        $table->string('business_type', 50);
        $table->string('name', 255);
        $table->boolean('has_string')->default(false);
        $table->boolean('has_number')->default(false);
        $table->boolean('has_date')->default(false);
        $table->boolean('has_file')->default(false);
        $table->timestamps();
        $table->index(['business_type']);
      });

      Schema::create('user_business_kyc_details', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('kyc_id');
        $table->foreign('kyc_id')->references('id')->on('user_business_kycs');
        $table->integer('option_id');
        $table->foreign('option_id')->references('id')->on('user_business_kyc_options');
        $table->string('string_value', 255)->nullable();
        $table->string('number_value', 255)->nullable();
        $table->char('date_value', 10)->nullable();
        $table->string('file_url', 255)->nullable();
        $table->char('kyc_status', 5);
        $table->string('admin_note', 255)->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

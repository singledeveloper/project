<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDirectUrlsTable extends Migration {

  public function up() {
    Schema::create('payment_direct_urls', function (Blueprint $table) {
      $table->increments('id');

      $table->string('reference_id')->unsigned();

      $table->integer('opc_group')->unsigned()->nullable();
      $table->foreign('opc_group')->references('id')->on('payment_odeo_payment_channel_informations')->onDelete('set null');

      $table->text('body');

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('payment_direct_urls');
  }
}

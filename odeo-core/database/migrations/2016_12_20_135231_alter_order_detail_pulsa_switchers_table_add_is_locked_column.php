<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDetailPulsaSwitchersTableAddIsLockedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
            //$table->boolean("is_locked")->default(true)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
            //$table->dropColumn("is_locked");
        });
    }
}

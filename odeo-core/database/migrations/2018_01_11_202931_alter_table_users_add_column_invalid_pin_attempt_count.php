<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddColumnInvalidPinAttemptCount extends Migration {

  public function up() {
    Schema::table('users', function (Blueprint $table) {
      $table->tinyInteger('invalid_pin_attempt_count')->default(0);
    });
  }


  public function down() {
    Schema::table('users', function (Blueprint $table) {
      //
    });
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreInventoryDetailsAddColumnVendorGroupPriceDetailId extends Migration {

  public function up() {
    Schema::table('store_inventory_details', function (Blueprint $table) {
      $table->bigInteger('vendor_group_price_detail_id')->nullable();
      $table->foreign('vendor_group_price_detail_id')->references('id')->on('store_inventory_group_price_details')->onDelete('cascade');

      $table->index(['vendor_group_price_detail_id']);

    });
  }


  public function down() {
    Schema::table('store_inventory_details', function (Blueprint $table) {
      $table->dropColumn('vendor_group_price_detail_id');
    });
  }
}

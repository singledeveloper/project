<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInventoryDetailsTable extends Migration {

  public function up() {

    Schema::create('store_inventory_details', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('store_inventory_id');
      $table->foreign('store_inventory_id')->references('id')->on('store_inventories')->onDelete('cascade');

      $table->bigInteger('store_inventory_vendor_id')->nullable();
      $table->foreign('store_inventory_vendor_id')->references('id')->on('store_inventory_vendors')->onDelete('set null');

      $table->bigInteger('inventory_id');

      $table->index(['store_inventory_id', 'store_inventory_vendor_id', 'inventory_id']);

      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('store_inventory_details');
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserLoginHistoriesAddColumnUseAndroidVersionTable extends Migration {

  public function up() {
    Schema::table('user_login_histories', function (Blueprint $table) {
      $table->string('use_android_version')->nullable();
    });
  }


  public function down() {
    Schema::table('user_login_histories', function (Blueprint $table) {
      $table->dropColumn('use_android_version');
    });
  }
}

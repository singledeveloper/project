<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCimbInquiriesChangeEffPostDate extends Migration
{
  public function up() {
    Schema::table('bank_cimb_inquiries', function (Blueprint $table) {
      $table->dateTime('post_date')->nullable()->change();
      $table->dateTime('eff_date')->nullable()->change();
    });
  }
}

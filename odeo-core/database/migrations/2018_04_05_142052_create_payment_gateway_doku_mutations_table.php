<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentGatewayDokuMutationsTable extends Migration {

  public function up() {
    Schema::create('payment_gateway_doku_mutations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('order_id');

      $table->string('transaction_account')->nullable();
      $table->string('status');
      $table->string('response_code', 4);
      $table->string('response_message');
      $table->string('currency');
      $table->string('payment_channel');
      $table->timestamp('transaction_date_time');
      $table->string('approval_code')->nullable();
      $table->string('issuing_bank')->nullable();
      $table->string('session_id');
      $table->string('void_response_code')->nullable();
      $table->timestamp('void_date_time')->nullable();
      $table->integer('chain_merchant_id')->nullable();
      $table->timestamp('inquiry_date_time')->nullable();
      $table->timestamp('payment_date_time')->nullable();
      $table->timestamp('completion_date')->nullable();
      $table->string('services')->nullable();
      $table->timestamp('redirect_date_time')->nullable();
      $table->timestamp('expiry_date')->nullable();
      $table->string('additional_data')->nullable();
      $table->string('settlement_status')->nullable();
      $table->timestamp('settlement_date')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementBniDisbursementsAddColumnBniResponse extends Migration {

  public function up() {
    Schema::table('disbursement_bni_disbursements', function (Blueprint $table) {
      $table->string('response_code')->nullable();
      $table->text('response_message')->nullable();
    });
  }


  public function down() {
    Schema::table('disbursement_bni_disbursements', function (Blueprint $table) {
      //
    });
  }
}

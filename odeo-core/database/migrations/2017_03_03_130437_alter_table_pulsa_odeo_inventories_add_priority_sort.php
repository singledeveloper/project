<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePulsaOdeoInventoriesAddPrioritySort extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('pulsa_odeo_inventories', function (Blueprint $table) {
        $table->integer('priority_sort')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pulsa_odeo_inventories', function (Blueprint $table) {
        $table->dropColumn('priority_sort');
      });
    }
}

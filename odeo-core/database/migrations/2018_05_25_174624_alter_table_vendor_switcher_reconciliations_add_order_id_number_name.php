<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendorSwitcherReconciliationsAddOrderIdNumberName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_switcher_reconciliations', function(Blueprint $table){
          $table->bigInteger('order_id')->nullable();
          $table->string('vendor_switcher_name', 50)->nullable();
          $table->integer('vendor_switcher_owner_store_id')->nullable();
          $table->integer('inventory_id')->nullable();
          $table->string('inventory_name', 255)->nullable();
          $table->string('number', 50)->nullable();
          $table->index(['order_id', 'vendor_switcher_owner_store_id', 'inventory_id', 'number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

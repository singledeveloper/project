<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreInventoriesTable extends Migration {

  public function up() {
    Schema::create('store_inventories', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
      $table->integer('service_detail_id')->unsigned();
      $table->foreign('service_detail_id')->references('id')->on('service_details')->onDelete('cascade');
      $table->decimal('discount', 10, 2)->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }


  public function down() {
    Schema::drop('store_inventories');
  }
}

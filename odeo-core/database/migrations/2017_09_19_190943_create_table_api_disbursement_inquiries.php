<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableApiDisbursementInquiries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('api_disbursement_inquiries', function (Blueprint $table) {
        $table->bigIncrements('id');

        $table->bigInteger('user_id')->unsigned();
        $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        $table->integer('bank_id')->unsigned();
        $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
        $table->bigInteger('disbursement_artajasa_transfer_inquiry_id')->unsigned()->nullable();
        $table->foreign('disbursement_artajasa_transfer_inquiry_id')
          ->references('id')->on('disbursement_artajasa_transfer_inquiries')
          ->onDelete('set null');

        $table->decimal('fee', 8, 2)->default(0);
        $table->string('account_number');
        $table->string('account_name')->nullable();
        $table->string('status');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('api_disbursement_inquiries');
    }
}

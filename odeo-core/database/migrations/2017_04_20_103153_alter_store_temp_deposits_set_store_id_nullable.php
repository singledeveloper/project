<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStoreTempDepositsSetStoreIdNullable extends Migration {

  public function up() {
    Schema::table('store_temp_deposits', function (Blueprint $table) {
      $table->bigInteger('store_id')->unsigned()->nullable()->change();
    });
  }


  public function down() {
    Schema::table('store_temp_deposits', function (Blueprint $table) {
      $table->bigInteger('store_id')->unsigned()->nullable(false)->change();
    });
  }
}

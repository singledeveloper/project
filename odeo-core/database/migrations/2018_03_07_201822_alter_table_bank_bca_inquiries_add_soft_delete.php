<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankBcaInquiriesAddSoftDelete extends Migration {

  public function up() {
    Schema::table('bank_bca_inquiries', function (Blueprint $table) {
      $table->softDeletes();
    });
  }


  public function down() {
    Schema::table('bank_bca_inquiries', function (Blueprint $table) {
      //
    });
  }
}

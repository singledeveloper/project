<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBniEdcVoids extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (Schema::hasTable('bni_edc_voids')) {
      return;
    }

    Schema::create('bni_edc_voids', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->date('void_date');
      $table->string('mti');
      $table->decimal('amount', 17, 2);
      $table->string('stan');
      $table->string('nii');
      $table->string('pan');
      $table->string('tid');
      $table->string('mid');
      $table->string('auth');
      $table->string('apv');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

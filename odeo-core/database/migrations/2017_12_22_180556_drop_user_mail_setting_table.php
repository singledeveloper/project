<?php

use Illuminate\Database\Migrations\Migration;

class DropUserMailSettingTable extends Migration {
  public function up() {
    Schema::drop('user_mail_settings');
  }

  public function down() {

  }
}

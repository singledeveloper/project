<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayMandiriEcashMutations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_gateway_mandiri_ecash_mutations', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->bigInteger('order_id')->nullable();

          $table->date('transaction_date');
          $table->timestamp('transaction_date_time');
          $table->string('name');
          $table->text('description');
          $table->decimal('amount', 17, 2);
          $table->string('transaction_ref_no');
          $table->string('type');
          $table->bigInteger('transfer_id');
          $table->timestamps();

          // We will always query transaction_date to get the inserted inquiry, so it is better to index this.
          $table->index('transaction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

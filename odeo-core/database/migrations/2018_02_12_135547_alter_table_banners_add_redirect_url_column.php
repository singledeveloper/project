<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBannersAddRedirectUrlColumn extends Migration {

  public function up() {
    Schema::table('banners', function (Blueprint $table) {
      $table->string('redirect_url')->nullable();
    });
  }


  public function down() {
    Schema::table('banners', function (Blueprint $table) {
      //
    });
  }
}

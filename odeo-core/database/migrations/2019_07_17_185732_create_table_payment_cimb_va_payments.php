<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentCimbVaPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::dropIfExists('payment_cimb_va_payments');
      Schema::create('payment_cimb_va_payments', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('transaction_id')->nullable();
        $table->string('channel_id')->nullable();
        $table->string('terminal_id')->nullable();
        $table->string('transaction_date')->nullable();
        $table->string('company_code')->nullable();
        $table->string('customer_key_1')->nullable();
        $table->string('customer_key_2')->nullable();
        $table->string('customer_key_3')->nullable();
        $table->string('language')->nullable();
        $table->string('currency')->nullable();
        $table->decimal('amount', 17, 2)->nullable();
        $table->decimal('fee', 17, 2)->nullable();
        $table->decimal('paid_amount', 17, 2)->nullable();
        $table->string('reference_number_transaction')->nullable();
        $table->string('flag_payment_list')->nullable();
        $table->string('customer_name')->nullable();
        $table->string('additional_data_1')->nullable();
        $table->string('additional_data_2')->nullable();
        $table->string('additional_data_3')->nullable();
        $table->string('additional_data_4')->nullable();

        $table->string('flag_payment')->nullable();
        $table->string('response_code')->nullable();
        $table->string('response_description')->nullable();

        $table->string('virtual_account_number');
        $table->string('status')->nullable();
        $table->unsignedBigInteger('inquiry_id')->nullable();
        $table->unsignedBigInteger('payment_id')->nullable();
        $table->boolean('is_reconciled')->default(false);
        $table->decimal('cost', 15, 2)->nullable();

        $table->timestamps();

        $table->foreign('inquiry_id')->references('id')->on('payment_cimb_va_inquiries')->onDelete('set null');
        $table->foreign('payment_id')->references('id')->on('payments')->onDelete('set null');
        
        $table->index('transaction_id');
        $table->index('transaction_date');
        $table->index('reference_number_transaction');
        $table->index('virtual_account_number');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

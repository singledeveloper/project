<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailPlnPostpaidSwitchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_detail_postpaid_switchers', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('order_detail_id');
        $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('cascade');
        $table->string('subscriber_id', 50);
        $table->string('subscriber_name', 50);
        $table->string('number', 20);
        $table->integer('multiplier');
        $table->integer('first_postpaid_id')->unsigned();
        $table->foreign('first_postpaid_id')->references('id')->on('postpaid_odeos')->onDelete('cascade');
        $table->integer('first_biller_price');
        $table->integer('first_fee');
        $table->integer('current_postpaid_id')->unsigned();
        $table->foreign('current_postpaid_id')->references('id')->on('postpaid_odeos')->onDelete('cascade');
        $table->integer('current_biller_price');
        $table->integer('current_fee');
        $table->string('current_ref_id', 255)->nullable();
        $table->text('additional_data')->nullable();
        $table->text('postpaid_query_dumps')->nullable();
        $table->string('response_number', 255)->nullable();
        $table->string('status', 5)->default('10000');
        $table->timestamp("requested_at")->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('order_detail_postpaid_switchers');
    }
}

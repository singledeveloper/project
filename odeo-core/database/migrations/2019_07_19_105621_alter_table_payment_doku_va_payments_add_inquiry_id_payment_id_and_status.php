<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePaymentDokuVaPaymentsAddInquiryIdPaymentIdAndStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_doku_va_payments', function (Blueprint $table) {
            $table->bigInteger('inquiry_id')->nullable();
            $table->foreign('inquiry_id')->references('id')->on('payment_doku_va_inquiries');
            $table->bigInteger('payment_id')->nullable();
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->integer('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableApiDisbursementInquiriesAddColumnBankInquiryId extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('api_disbursement_inquiries', function (Blueprint $table) {
      $table->bigInteger('bank_account_inquiry_id')->unsigned()->nullable();
      $table->foreign('bank_account_inquiry_id')->references('id')->on('bank_account_inquiries')->onDelete('set null');
      $table->decimal('cost', 17, 2)->default(0);
      $table->string('vendor')->default('');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

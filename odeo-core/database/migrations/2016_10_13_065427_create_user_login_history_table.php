<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginHistoryTable extends Migration {

  public function up() {
    Schema::create('user_login_histories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->string('ip_address', 15)->nullable();
      $table->text('lat_lang')->default('');
      $table->text('user_agent')->default('');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('user_login_histories');
  }
}

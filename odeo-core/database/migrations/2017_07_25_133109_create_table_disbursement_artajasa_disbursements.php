<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbursementArtajasaDisbursements extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('disbursement_artajasa_disbursements', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->integer('transaction_id')->nullable();
      $table->dateTime('transaction_datetime')->nullable();
      $table->integer('inquiry_transaction_id')->nullable();
      $table->dateTime('inquiry_transaction_datetime')->nullable();

      $table->string('status')->nullable();

      $table->string('ref_number')->nullable();
      $table->string('bank_code')->nullable();
      $table->string('regency_code')->nullable();
      $table->string('transfer_to')->nullable();
      $table->string('transfer_to_name')->nullable();
      $table->decimal('amount', 17, 2)->nullable();
      $table->string('purpose_desc')->nullable();

      $table->string('response_code')->nullable();
      $table->text('response_description')->nullable();
      $table->string('inquiry_response_code')->nullable();
      $table->text('inquiry_response_description')->nullable();

      $table->string('transaction_key')->unique()->nullable();
      $table->string('disbursement_type')->nullable();
      $table->unsignedBigInteger('disbursement_reference_id')->nullable();

      $table->dateTime('response_datetime')->nullable();
      $table->dateTime('inquiry_response_datetime')->nullable();
      $table->text('error_exception_message')->nullable();
      $table->integer('error_exception_code')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('disbursement_artajasa_disbursements');
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankScrapeAccountsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bank_scrape_accounts', function (Blueprint $table) {
      $table->integer('id')->unsigned();
      $table->primary('id');

      $table->string('referred_target');
      $table->foreign('referred_target')
        ->references('target')
        ->on('bank_scrape_informations');

      $table->text('account');

      $table->boolean('active');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('bank_scrape_accounts');
  }
}

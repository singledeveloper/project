<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePulsaBulks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulsa_bulks', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->string('name', 255);
          $table->boolean('is_approved')->nullable();
          $table->bigInteger('created_user_id');
          $table->bigInteger('approved_user_id')->nullable();
          $table->timestamps();
          $table->index(['is_approved']);
          $table->index(['created_user_id']);
          $table->index(['approved_user_id']);
        });

        Schema::table('pulsa_bulk_purchases', function (Blueprint $table){
          $table->bigInteger('pulsa_bulk_id')->nullable();
          $table->foreign('pulsa_bulk_id')->references('id')->on('pulsa_bulks')->onDelete('set null');
          $table->index(['pulsa_bulk_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

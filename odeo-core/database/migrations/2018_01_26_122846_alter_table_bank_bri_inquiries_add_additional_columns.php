<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBankBriInquiriesAddAdditionalColumns extends Migration {

  public function up() {
    Schema::table('bank_bri_inquiries', function (Blueprint $table) {
      $table->string('transaction_code')->nullable();
      $table->string('transaction_type')->nullable();
      $table->string('bank_reff')->nullable();
    });
  }


  public function down() {
    Schema::table('bank_bri_inquiries', function (Blueprint $table) {
      //
    });
  }
}

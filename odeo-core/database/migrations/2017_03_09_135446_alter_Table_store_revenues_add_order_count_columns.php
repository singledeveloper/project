<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStoreRevenuesAddOrderCountColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('store_revenues', function (Blueprint $table) {
        $table->bigInteger('order_count')->default(0);
        $table->bigInteger('total_order_count')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('store_revenues', function (Blueprint $table) {
        $table->dropColumn('order_count');
        $table->dropColumn('total_order_count');
      });
    }
}

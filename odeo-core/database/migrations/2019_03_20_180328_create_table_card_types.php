<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCardTypes extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('card_types', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('pan_low');
      $table->bigInteger('pan_high');
      $table->string('name');
      $table->string('type');
      $table->timestamps();

      $table->index(['pan_low', 'pan_high']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
  }
}

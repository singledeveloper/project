<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankBriInquiriesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('bank_bri_inquiries', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->integer('bank_scrape_account_number_id')->unsigned();
      $table->foreign('bank_scrape_account_number_id')
        ->references('id')
        ->on('bank_scrape_account_numbers');

      $table->datetime('date')->nullable();
      $table->string('remark')->nullable();
      $table->decimal('debit', 17, 2)->nullable();
      $table->decimal('credit', 17, 2)->nullable();
      $table->decimal('balance', 17, 2)->nullable();
      $table->string('teller_id')->nullable();

      $table->string('reference_type')->nullable();
      $table->text('reference')->nullable();
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::drop('bank_bri_inquiries');
  }
}

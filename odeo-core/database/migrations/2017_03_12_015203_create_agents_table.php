<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration {

  public function up() {
    Schema::create('agents', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('user_referred_id')->unsigned();
      $table->foreign('user_referred_id')->references('id')->on('users')->onDelete('set null');
      $table->bigInteger('store_referred_id')->unsigned();
      $table->foreign('store_referred_id')->references('id')->on('stores')->onDelete('set null');
      $table->string('status');
      $table->timestamp('requested_at')->nullable();
      $table->timestamp('joined_at')->nullable();
      $table->string('inputted_master_code')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('agents');
  }
}

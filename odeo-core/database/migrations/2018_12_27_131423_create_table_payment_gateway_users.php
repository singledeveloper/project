<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayUsers extends Migration {

  public function up() {
    if (Schema::hasTable('payment_gateway_users')) {
      return;
    }

    Schema::create('payment_gateway_users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id');
      $table->string('virtual_account_prefix')->nullable();
      $table->string('notify_url')->nullable();
      $table->string('email_report')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::dropIfExists('payment_gateway_users');
  }
}

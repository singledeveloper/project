<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderDetailPostpaidMultiFinanceDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_detail_postpaid_multi_finance_details', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('order_detail_pulsa_switcher_id');
        $table->foreign('order_detail_pulsa_switcher_id')->references('id')->on('order_detail_pulsa_switchers')->onDelete('cascade');
        $table->string('subscriber_id', 50);
        $table->string('subscriber_name', 50);
        $table->string('installment', 10);
        $table->string('due_date', 10);
        $table->integer('base_price');
        $table->integer('bill_price');
        $table->integer('fine');
        $table->integer('admin_fee');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

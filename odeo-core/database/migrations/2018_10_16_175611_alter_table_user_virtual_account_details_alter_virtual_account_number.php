<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserVirtualAccountDetailsAlterVirtualAccountNumber extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('user_virtual_account_details', function (Blueprint $table) {
      $table->string('virtual_account_number', 32)->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('user_virtual_account_details', function (Blueprint $table) {
      //
    });
  }
}

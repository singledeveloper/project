<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermataVaPaymentReversalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_permata_va_payment_reversals', function(Blueprint $table){
        $table->bigIncrements('id');
        $table->bigInteger('permata_payment_id');
        $table->foreign('permata_payment_id')->references('id')->on('payment_permata_va_payments');
        $table->string('instcode');
        $table->string('va_number');
        $table->string('trace_no');
        $table->string('trn_date');
        $table->decimal('bill_amount', 17, 2);
        $table->string('del_channel');
        $table->text('ref_info')->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMandiriBillPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payment_mandiri_va_payments', function (Blueprint $table) {
        $table->bigIncrements('id');

        $table->unsignedBigInteger('inquiry_id');
        $table->foreign('inquiry_id')->references('id')->on('payment_mandiri_va_inquiries')->onDelete('set null');

        $table->unsignedBigInteger('payment_id');
        $table->foreign('payment_id')->references('id')->on('payments')->onDelete('set null');

        $table->string('trx_date_time');
        $table->string('transmission_date_time');
        $table->string('company_code');
        $table->string('channel_id');
        $table->string('bill_key_1');
        $table->string('bill_key_2');
        $table->string('bill_key_3')->nullable();
        $table->string('paid_bills');
        $table->decimal('payment_amount', 17, 2);
        $table->string('transaction_id');
        $table->string('reference_1')->nullable();
        $table->string('reference_2')->nullable();
        $table->string('reference_3')->nullable();
        $table->string('status');

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

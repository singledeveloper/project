<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
  public function up()
  {
    Schema::create('cities', function (Blueprint $table) {
      $table->unsignedInteger('id');
      $table->primary('id');
      $table->string('name');
      $table->unsignedInteger('province_id');
      $table->timestamps();
      $table->foreign('province_id')->references('id')->on('provinces');
    });
  }

  public function down()
  {
    Schema::drop('cities');
  }
}

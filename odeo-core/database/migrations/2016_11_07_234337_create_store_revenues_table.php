<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('store_revenues', function(Blueprint $table)
      {
        $table->increments('id');
        $table->bigInteger('store_id')->unsigned();
        $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null');
        $table->bigInteger('service_id');
        $table->foreign('service_id')->references('id')->on('services')->onDelete('set null');
        $table->decimal('amount', 17, 2)->default(0);
        $table->decimal('total', 20, 2)->default(0);
        $table->date('date');
        $table->index('service_id');
        $table->index('date');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('store_revenues');
    }
}

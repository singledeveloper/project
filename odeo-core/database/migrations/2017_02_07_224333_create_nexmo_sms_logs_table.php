<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNexmoSmsLogsTable extends Migration {

  public function up() {
    Schema::create('nexmo_sms_logs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('message_count')->nullable();
      $table->string('from')->nullable();
      $table->string('to')->nullable();
      $table->string('network_code')->nullable();
      $table->string('message_id')->nullable();
      $table->decimal('message_price', 17, 3)->nullable();
      $table->string('request_status')->nullable();
      $table->string('response_status')->nullable();
      $table->decimal('remaining_balance', 12, 8)->nullable();
      $table->string('msisdn')->nullable();
      $table->string('scts')->nullable();
      $table->string('err_code')->nullable();
      $table->text('sms_text')->nullable();
      $table->timestamp('message_timestamp')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('nexmo_sms_logs');
  }
}

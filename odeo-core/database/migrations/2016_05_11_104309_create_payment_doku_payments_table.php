<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDokuPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_payments', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('session_id')->nullable();
            $table->string('mall_id')->nullable();
            $table->string('words')->nullable();

            $table->decimal('amount', 17, 2)->nullable();
            $table->string('currency')->nullable();

            $table->integer('reference_id')->unsigned()->nullable();
            $table->string('request_date_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_doku_payments');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDetailPulsaSwitchersTableAddColumnFailureDetectionCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
        $table->integer("failure_assumption_count")->default(0)->after('tries_is_able_repurchase');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_detail_pulsa_switchers', function (Blueprint $table) {
        $table->dropColumn("failure_assumption_count");
      });
    }
}

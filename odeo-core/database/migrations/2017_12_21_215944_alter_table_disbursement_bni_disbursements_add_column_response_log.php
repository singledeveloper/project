<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDisbursementBniDisbursementsAddColumnResponseLog extends Migration {

  public function up() {
    Schema::table('disbursement_bni_disbursements', function (Blueprint $table) {
      $table->text('response_log')->nullable();
    });
  }


  public function down() {
    Schema::table('disbursement_bni_disbursements', function (Blueprint $table) {
      //
    });
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBillReminders extends Migration {

  public function up() {
    Schema::create('bill_reminders', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name');
      $table->integer('pulsa_odeo_id');
      $table->bigInteger('user_id');
      $table->string('number');
      $table->integer('date');
      $table->date('next_reminder');
      $table->date('expired_at')->nullable();
      $table->timestamp('last_reminded_at')->nullable();
      $table->timestamps();
      $table->softDeletes();

      $table->foreign('pulsa_odeo_id')->references('id')->on('pulsa_odeos')->onDelete('cascade');
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    });
  }

  public function down() {
    //
  }
}

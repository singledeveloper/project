<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresetCashbacksTable extends Migration {

  public function up() {
    Schema::create('preset_cashbacks', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->bigInteger('order_detail_store_deposit_id');
      $table->foreign('order_detail_store_deposit_id')
        ->references('id')
        ->on('order_detail_store_deposits')
        ->onDelete('cascade');

      $table->bigInteger('store_id');
      $table->foreign('store_id')
        ->references('id')
        ->on('stores')
        ->onDelete('cascade');

      $table->integer('preset');
      $table->integer('topup_amount');
      $table->integer('bonus');
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('preset_cashbacks');
  }
}

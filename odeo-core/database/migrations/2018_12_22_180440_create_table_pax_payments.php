<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaxPayments extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('pax_payments', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_id')->nullable();
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
      $table->string('payment_data');
      $table->string('terminal_id');
      $table->string('merchant_id');
      $table->string('card_type');
      $table->string('pan_number');
      $table->string('expiry_date');
      $table->string('entry_mode');
      $table->string('transaction_type');
      $table->string('batch_number');
      $table->string('trace_number');
      $table->string('reference_number');
      $table->string('approval_code');
      $table->string('payment_date');
      $table->string('payment_time');
      $table->decimal('total_amount', 17, 2);
      $table->string('status');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('pax_payments');
  }
}

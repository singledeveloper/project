<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaqsAddColumnGroup extends Migration
{
  public function up() {
    Schema::table('faqs', function (Blueprint $table) {
      $table->string('group')->nullable();
    });
  }

  public function down() {
    Schema::table('faqs', function (Blueprint $table) {
      $table->dropColumn('group');
    });
  }
}

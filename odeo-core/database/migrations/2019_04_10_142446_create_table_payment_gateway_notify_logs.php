<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentGatewayNotifyLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_gateway_notify_logs')) {
            return;
        }

        Schema::create('payment_gateway_notify_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pg_user_id');
            $table->foreign('pg_user_id')->references('id')->on('payment_gateway_users');
            $table->string('notify_type');
            $table->string('request_url');
            $table->text('request_dump')->nullable();
            $table->text('response_dump')->nullable();
            $table->smallInteger('status_code')->nullable();
            $table->text('error_message')->nullable();
            $table->bigInteger('reference_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_gateway_notify_logs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendorSwitcherReconciliationsAddOrderUserIdOrderUserName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_switcher_reconciliations', function(Blueprint $table){
          $table->integer('order_user_id')->nullable();
          $table->string('order_user_name', 255)->nullable();
          $table->index(['order_user_id']);
          $table->index(['order_user_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserReferralCodesUniqueUserId extends Migration {

  public function up() {
    Schema::table('user_referral_codes', function (Blueprint $table) {
      $table->bigInteger('user_id')->unique()->change();
    });
  }


  public function down() {
    //
  }
}

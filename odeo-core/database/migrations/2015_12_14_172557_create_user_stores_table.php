<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStoresTable extends Migration {

  public function up() {
    Schema::create('user_stores', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->bigInteger('store_id')->unsigned();
      $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
      $table->string('type', 10)->default('');
      $table->timestamps();
      $table->softDeletes();
    });
  }


  public function down() {
    Schema::drop('user_stores');
  }
}

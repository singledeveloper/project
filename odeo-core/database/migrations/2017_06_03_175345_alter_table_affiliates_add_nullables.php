<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAffiliatesAddNullables extends Migration
{

    public function up()
    {
        Schema::table('affiliates', function (Blueprint $table) {
          $table->string('secret_key')->nullable()->change();
          $table->text('whitelist')->nullable()->change();
          $table->string('notify_url')->nullable()->change();
        });
    }


    public function down()
    {
        Schema::table('affiliates', function (Blueprint $table) {
            //
        });
    }
}

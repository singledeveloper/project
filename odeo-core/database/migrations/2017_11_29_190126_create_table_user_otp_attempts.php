<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserOtpAttempts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_otp_attempts', function(Blueprint $table){
          $table->bigIncrements('id');
          $table->string('telephone', 20);
          $table->char('otp', 4);
          $table->string('ip_address', 15);
          $table->boolean('is_attempted')->default(true);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentPrismalinkVaPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_prismalink_va_payments')) {
            return;
        }

        Schema::create('payment_prismalink_va_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payment_id')->nullable();
            $table->foreign('payment_id')->references('id')->on('payment_gateway_payments');
            $table->bigInteger('inquiry_id')->nullable();
            $table->foreign('inquiry_id')->references('id')->on('payment_prismalink_va_inquiries');
            $table->string('va_code')->nullable();
            $table->decimal('amount', 17, 2)->nullable();
            $table->decimal('net_amount', 17, 2)->nullable();
            $table->decimal('fee', 17, 2)->nullable();
            $table->char('code_currency', 3)->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->string('trace_number')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_prismalink_va_payments');
    }
}

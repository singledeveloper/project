<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderDetailMdPlus extends Migration {

  public function up() {
    Schema::create('order_detail_md_plus', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('order_detail_id');
      $table->foreign('order_detail_id')->references('id')->on('order_details');
      $table->index('order_detail_id');
      $table->bigInteger('store_id');
      $table->foreign('store_id')->references('id')->on('stores');
      $table->index('store_id');
      $table->integer('md_plus_plan_id');
      $table->foreign('md_plus_plan_id')->references('id')->on('md_plus_plans');
      $table->index('md_plus_plan_id');
      $table->string('type', 20);
      $table->integer('subscription_months');
      $table->boolean('is_auto_renew');
      $table->timestamp('expired_at');
      $table->timestamps();
    });
  }


  public function down() {
    //
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementApiNotifyLogs extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('api_disbursement_notify_logs', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->integer('api_disbursement_id')->unsigned();
      $table->foreign('api_disbursement_id')->references('id')->on('api_disbursements')->onDelete('set null');
      
      $table->integer('response_status_code');
      $table->text('response_reason');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('api_disbursement_notify_logs');
  }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePundiCreditBillsTable extends Migration {

  public function up() {
    Schema::create('pundi_credit_bills', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
      $table->string('phone_number');
      $table->decimal('amount', 17, 2);
      $table->string('status');
      $table->timestamp('settlement_at')->nullable();
      $table->timestamp('should_settlement_at')->nullable();
      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('pundi_credit_bills');
  }
}

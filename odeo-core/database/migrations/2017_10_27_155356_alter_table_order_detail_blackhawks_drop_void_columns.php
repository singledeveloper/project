<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderDetailBlackhawksDropVoidColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('order_detail_blackhawks', function(Blueprint $table){
        $table->dropColumn('log_void_request');
        $table->dropColumn('log_void_response');
        $table->dropColumn('log_reversal_void_request');
        $table->dropColumn('log_reversal_void_response');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTransfersTable extends Migration {

  public function up() {
    Schema::create('user_transfers', function (Blueprint $table) {

      $table->bigIncrements('id');

      $table->bigInteger('sender_user_id')->unsigned()->nullable();
      $table->foreign('sender_user_id')->references('id')->on('users')->onDelete('set null');

      $table->bigInteger('receiver_user_id')->unsigned()->nullable();
      $table->foreign('receiver_user_id')->references('id')->on('users')->onDelete('set null');

      $table->decimal('amount', 17, 2);

      $table->index(['sender_user_id', 'receiver_user_id']);

      $table->timestamps();
    });
  }


  public function down() {
    Schema::drop('user_transfers');
  }
}

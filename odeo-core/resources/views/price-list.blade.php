<style>
    .center {
        font-weight: bold;
        text-align: center;
    }
</style>

<h1>Price List</h1>
<table border="1" cellspacing="0" cellpadding="4">
  @foreach($data as $index => $d)
      <tr class="{{$index == 0 ? 'center': ''}}" >
        <td>{{ $d["category"] }}</td>
        <td>{{ $d["inventory_code"] }}</td>
        <td>{{ $d["name"] }}</td>
        <td>{{ $d["price"] }}</td>
      </tr>
  @endforeach
</table>

<style>
  html, body {margin: 0; padding: 0;}
  body {font-family: monospace; padding: 30px; }
  body > div {border-bottom: 1pt dashed #999; width: 100%; padding-bottom: 15px; margin-bottom: 15px;}
  table {width: 100%;}
  table td {padding: 5px 0; font-size: 1.2em;}
  table td.title {padding: 10px 0;}
  table td.bold {font-weight: bold; text-align: center; }
</style>

@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<body>
  @foreach ($so->plnDetails as $item)
    <div>
      <table> 
        <tr>
          <td colspan="4" class="bold title">STRUK PEMBAYARAN TAGIHAN LISTRIK</td>
        </tr>
        <tr> 
          <td>IDPEL</td> 
          <td>: {{ $item->subscriber_id }} </td> 
          <td>TH/BL</td> 
          <td>: {{ $item->period }}</td> 
        </tr> 
        <tr> 
          <td>NAMA</td> <td>: {{ $item->subscriber_name }}</td> 
          <td>STAND METER</td> <td>: {{ $item->meter_changes }}</td> 
        </tr> 
        <tr> 
          <td>TARIF/DAYA</td> 
          <td colspan="3">: {{ $item->tariff . '/' . $item->power }}</td> 
        </tr> 
        <tr> 
          <td>RP TAG PLN</td> 
          <td colspan="3">: {{ $currencyHelper->formatPrice($item->base_price)['formatted_amount'] }}</td> 
        </tr> 
        <tr> 
          <td>NO REF</td> 
          <td colspan="3">: {{ $so->serial_number }}</td> 
        </tr> 
        <tr>
          <td colspan="4" class="bold">
            PLN menyatakan struk ini sebagai bukti pembayaran yang sah.
          </td>
        </tr>
        @if ($item->fine > 0)
          <tr> 
            <td>DENDA</td> 
            <td colspan="3">: {{ $currencyHelper->formatPrice($item->fine)['formatted_amount'] }}</td> 
          </tr> 
        @endif
        <tr> 
          <td>ADMIN</td> 
          <td colspan="3">: {{ $currencyHelper->formatPrice($item->admin_fee)['formatted_amount'] }}</td> 
        </tr> 
        <tr> 
          <td>TOTAL BAYAR</td> 
          <td colspan="3">: {{ $currencyHelper->formatPrice($item->base_price + $item->fine + $item->admin_fee)['formatted_amount'] }}</td> 
        </tr>
        <tr>
          <td colspan="4" style="text-align: center">
            @if ($item->bill_rest > 0)
              Anda masih memiliki sisa tunggakan {{ $item->bill_rest }} bulan
            @else Terima kasih @endif
            <br/>RINCIAN TAGIHAN DAPAT DIAKSES DI www.pln.co.id ATAU PLN TERDEKAT
          </td>
        </tr>
        <tr> 
          <td>DICETAK DI</td> 
          <td colspan="3">: www.odeo.co.id</td> 
        </tr> 
        <tr> 
          <td>TANGGAL/KODE</td> 
          <td colspan="3">: {{ date('d M Y H:i:s') . ' / SO' . $so->orderDetail->order_id }} / ODEO TEKNOLOGI INDONESIA</td> 
        </tr> 
       </table>
    </div>
  @endforeach
</body>
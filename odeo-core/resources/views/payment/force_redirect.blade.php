<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Redirect</title>
  @if ($success)
    <script>
        window.location.href = '{{$redirect_url}}';
    </script>
  @else

    <style>
      #payment-redirect {
        margin-top: 80px;
        padding: 80px;
        text-align: center;
        font-size: 21px;
      }
      .payment-wraper-error{
        margin-bottom: 10px;
      }
    </style>


    <div id="payment-redirect">
      <div class="payment-wraper-error">
        <label>Failed to process your payment.</label><br>
      </div>
      <div><label>You Will Redirect within 2 seconds...</label></div>
    </div>
    <script>
        setTimeout((function(){
            window.location.href = '{{$redirect_url}}';
        }), 2000);
    </script>
  @endif
</head>
<body>
</body>
</html>
<table style="margin: 20px 20px 5px 20px;" cellpadding="0" cellspacing="0">
  <tr>
    <td width="320">
      <p class="no-margin bold-text text-14 green-text">TOTAL</p>
    </td>
    <td width="270">
      <p class="text-14 gray-text no-margin right-text">ID Transaksi</p>
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <p class="text-36 bold-text no-margin">{{ $data['cart_data']['total']['formatted_amount'] }}</p>
    </td>
    <td>
      <p class="text-18 green-text bold-text no-margin right-text">#{{$data['order_id'] or '-'}}</p>
    </td>
  </tr>
  <tr>
    <td>
      <p class="text-14 no-margin right-text">{{ date("j F Y H:i A", strtotime($data['date_opened'] ))}}</p>
    </td>
  </tr>
</table>
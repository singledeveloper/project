<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<div
        style="background-image: url(<?php echo baseUrl('images/email/bg.png') ?>); position: fixed; top: 0; left: 0; background-size: cover; margin:0; padding: 0; width: 100%; height: 100%;">
    <div style="max-width: 80%; height: 100%;  margin: 0 auto; padding: 0;">
        <div style=" padding: 10px 20px;">

          <?php
          if (isset($data['logo'])) {
            ?>
              <div style="text-align:center;">
                  <img src="<?php echo $data['logo']; ?>" alt="" style="max-width:300px;">
              </div>
            <?php
          } else if ($data['name']) {
            ?>
              <p style="font-size: 32px; text-align:center; color: white; padding: 10px; margin: 10px;"><?php echo $data['name']; ?></p>
            <?php
          }
          ?>
        </div>
        <div style="background-color: white; min-height: 500px; padding: 20px;">
            <div style="width: 80%; margin: 0 auto;">
                <p style="font-size: 40px; text-align: center; color: #6fd0e1; margin-top: 10px; margin-bottom: 0;">
                    Terima Kasih </p>
                <p style="text-align: center; color: #0a2529; margin-top: 10px; margin-bottom: 20px;"> Anda telah
                    melakukan pembelian di
                    <a style="color: #6fd0e1;" href="<?php echo $data['domain']; ?>"><?php echo $data['name']; ?></a>
                </p>
                <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #eee; margin: 1em 0; padding: 0;">
                <table style="color: #0a2529;">
                    <tr style="height: 20px;">
                        <td style="font-weight:bold; width: 150px;"> Tanggal</td>
                        <td><?php echo date("j F Y, H:i", strtotime($data['date_opened'])); ?> WIB
                        </td>
                    </tr>
                    <tr style="height: 20px;">
                        <td style="font-weight:bold;"> Nomor Order</td>
                        <td><?php echo $data['order_id']; ?></td>
                    </tr>
                </table>
                <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #eee; margin: 1em 0; padding: 0;">
                <table style="color: #0a2529; width: 100%;">
                    <tr style="height: 20px; ">
                        <td style="font-weight:bold; width: 50%;">Produk</td>
                        <td style="width: 50%; text-align: right;"></td>
                    </tr>
                  <?php foreach ($data['cart_data']['items'] as $item) { ?>
                      <tr>
                          <td style="width: 50%;">

                            <?php echo ucwords($item['type']) . " " . $item['name'] ?>

                            <?php
                            if ($item['type'] == 'pulsa') {
                              echo '<br>' . $item['item_detail']['operator'] . ' - ' . $item['item_detail']['number'];
                            } else if ($item['type'] == 'bolt') {
                              echo '<br>' . $item['item_detail']['number'];
                            } else if ($item['type'] == 'paket_data') {
                              echo '<br>' . $item['item_detail']['number'];
                            } else if ($item['type'] == 'pln') {
                              echo '<br>' . $item['item_detail']['number'];
                            }
                            ?>
                              <br>
                          </td>
                          <td style="text-align: right;"><?php echo $item['price']['formatted_amount']; ?></td>
                      </tr>
                  <?php } ?>

                  <?php foreach ($data['cart_data']['charges'] as $charge) { ?>
                      <tr style="height: 20px;">
                          <td style="font-weight:bold;"><?php echo $charge['displayed_name']; ?></td>
                          <td style="font-weight:bold; text-align: right;">
                            <?php echo $charge['price']['formatted_amount']; ?>
                          </td>
                      </tr>
                    <?php
                  }
                  ?>
                    <tr style="height: 20px;">
                        <td style="font-weight:bold;">Total Harga</td>
                        <td
                                style="font-weight:bold; text-align: right;"><?php echo $data['cart_data']['total']['formatted_amount']; ?></td>
                    </tr>
                </table>
                <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #eee; margin: 1em 0; padding: 0;">
              <?php echo $data['payment_section']; ?>
            </div>
        </div>
        <div>
            <p style="opacity: 0.7; color: white; text-align:center; margin-bottom: 5px;">Butuh Bantuan? Hubungi Kami
                <b><a style="color:white; text-decoration: none;"
                      href=""><?php echo $data['email_info']['email_support']; ?></a></b></p>
        </div>
      <?php if ($data['name'] != 'Odeo') { ?>
          <div>
              <img src="<?php echo baseUrl('images/email/logo.png'); ?>"
                   style="display: block; margin:0 auto; background-position: center; width:200px;">
          </div>
      <?php } else { ?>
          <br>
      <?php } ?>
    </div>
</div>
</body>
</html>
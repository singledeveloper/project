<table style="color: #0a2529; width: 100%;">
  <tr style="height: 20px; ">
    <td valign="top" style="font-weight:bold; width: 50%;">Metode Pembayaran</td>
    <td style="width: 50%; text-align: right;">
     <?php echo $data['payment_data']['detail']['acc_bank']; ?>
      <br/><?php echo $data['payment_data']['detail']['acc_branch']; ?>
      <br/><?php echo $data['payment_data']['detail']['acc_number']; ?>
      <br/><?php echo $data['payment_data']['detail']['acc_name']; ?>
    </td>
  </tr>
</table>
<hr style="display: block; height: 1px; border: 0; border-top: 1px solid #eee; margin: 1em 0; padding: 0;">

<p style="margin: 0 auto; text-align: center; width: 75%;">Harap segera melakukan pembayaran paling lambat
  <br>
  <strong><?php echo \Carbon\Carbon::parse($data['date_expired'])->format('H:i') . ' WIB, ' . \Carbon\Carbon::parse($data['date_expired'])->format('d F Y') ?></strong>
  <br>
  atau transaksi anda akan secara otomatis kami batalkan.</p>

<?php if ($data['platform_id'] == 2) { ?>
  <a href="<?php echo $data['payment_confirmation_link']; ?>"
     style="text-align: center; color: #6fd0e1; display:block; margin-top: 10px;">
    klik disini untuk melakukan konfirmasi pembayaran
  </a>
<?php } ?>
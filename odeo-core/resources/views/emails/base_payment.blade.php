<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <style type="text/css">
    * {
      font-family: Arial, sans-serif;
    }
    #outer-table {
      background-color: rgb(234, 237, 239);
    }
    #letter-table {
      margin: 0 auto 20px auto; 
      background-color: white;
    }
    #content-box {
      padding: 0px 25px;
    }
    #content-table {
      margin: 0px 10px;
    }
    #ocommerce-image {
      margin: 10px 25px;
    }
    #warning-box {
      background:url(https://s3-ap-southeast-1.amazonaws.com/odeo/mail/warning.png) no-repeat; 
      background-size: cover
    }
    #warning-text {
      color: rgb(97,88,55);
      margin-left:100px; 
      font-size:14px; 
      margin-right:20px;
    }
    #footer-box {
      margin: 10px 20px 10px 20px; 
      background: white;
    }
    .no-margin {
      margin: 0;
    }
    .separator {
      border: 1px rgb(238,238,238);
      background-color: rgb(238,238,238);
      border-style: solid;
    }
    .right-text {
      text-align: right;
    }
    .center-text {
      text-align: center;
    }
    .bold-text {
      font-weight: bold;
    }
    .gray-text {
      color: rgb(144,145,146);
    }
    .green-text {
      color: rgb(0,151,143);
    }
    .link {
      text-decoration: none; 
      color: rgb(1,150,246);
    }
    .text-12 {
      font-size: 12px;
    }
    .text-14 {
      font-size: 14px;
    }
    .text-16 {
      font-size: 16px;
    }
    .text-18 {
      font-size: 18px;
    }
    .text-24 {
      font-size: 24px;
    }
    .text-36 {
      font-size: 36px;
    }
  </style>
  @yield('styles')
</head>


<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
  <table id="outer-table" align="center" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td>
        <table
          id="letter-table"
          class="content" 
          cellspacing="0" 
          cellpadding="0"
          width="600"
        >
          <tr>
            <td width="600">
              @yield('header')
            </td>
          </tr>

          <tr>  
            <td id="content-box">
              @yield('content')
              <p class="center-text text-14" style="margin: 15px 10px; line-height: 1.3;">
                Terima kasih atas pembayaran Anda. Apabila produk Anda tidak terkirim dalam waktu 1x24 jam, harap menghubungi <a href="mailto:cs@odeo.co.id" class="link" target="_blank">customer service</a> kami
              </p>
              <hr class="separator"/>
            </td>
          </tr>

          <tr>
            <td>
              <a href="https://ocommerce.odeo.co.id/" target="_blank">
                <img id="ocommerce-image" src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/iklan_odeo.png" alt="" width="550"/>
              </a>
            </td>
          </tr>

          <tr>
            <td id="warning-box">
              <p id="warning-text">Hati-hati terhadap pihak yang mengaku dari ODEO, membagikan voucher atau meminta data pribadi. ODEO tidak pernah meminta password dan data pribadi melalui email, pesan pribadi, telepon ataupun channel lainnya. Untuk semua email dengan link ODEO, pastikan alamat URL di browser sudah di alamat <a href="https://www.odeo.co.id/" class="link bold-text">www.odeo.co.id</a> bukan alamat lainnya.</p>
            </td>
          </tr>

          <tr>
            <td>
              <table id="footer-box">
                <tr>
                  <td><p class="gray-text bold-text text-12">Download Aplikasi Kami</p></td>
                  <td><p class="gray-text bold-text text-12">Ikuti kami di:</p></td>
                </tr>
                <tr>
                  <td>
                    <a href="https://itunes.apple.com/id/app/odeo/id1311188748?mt=8" target="_blank"><img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/appstore.png" width="140"/></a>
                    <a href="https://play.google.com/store/apps/details?id=com.odeo.odeo" target="_blank"><img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/googleplay.png" width="140"/></a>
                  </td>
                  <td>
                    <a href="https://www.facebook.com/odeo.id" target="_blank"><img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/facebook.png" width="47" height="47"/></a>
                    <a href="https://twitter.com/odeo_id" target="_blank"><img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/twitter.png" width="47" height="47"/></a>
                    <a href="https://www.instagram.com/odeo.id" target="_blank"><img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/instagram.png" width="47" height="47"/></a>
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><p class="gray-text text-12">Hak cipta 2016 PT ODEO Teknologi indonesia - ODEO dan semua logonya merupakan properti kreatif milik PT ODEO Teknologi Indonesia</p></td>
                </tr>
              </table>
            </td>
          </tr>

        </table>
      </td>
    </tr>

  </table>

</body>
</html>

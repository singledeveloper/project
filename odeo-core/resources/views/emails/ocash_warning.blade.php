@extends('emails.base')

@section('styles')
  <style type="text/css">
    .instruction {
      margin-left: 15px;
      margin-right: 15px;
    }
  </style>
@endsection

@section('content')
    <p class="center-text text-24 bold-text no-margin">oCash Warning</p>
    <p class="center-text text-14">Your remaining deposit is <b style="color: maroon; font-size: 1.3em">{{ $amount }}</b></p>
    <p class="center-text text-14">Please topup as soon as possible before you're running out of deposit.</p>
@endsection

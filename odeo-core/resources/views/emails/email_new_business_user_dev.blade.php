@extends('emails.base')

@section('styles')
    <style type="text/css">

    </style>
@endsection

@section('content')
    <p class="center-text text-24 bold-text no-margin">Hi, {{ $name }}</p>
    <p class="center-text text-14" style="margin: 10px">Please find the details for the staging account below:</p>
    <ul style="line-height: 1.5">
        <li>API base URL: https://api.v2.staging.odeo.co.id/</li>
        <li>Dashboard: https://seller.staging.odeo.co.id</li>
        <li>Disbursement Documentation: https://business.odeo.co.id/docs/disbursement</li>
        <li>Payment Gateway Documentation: https://business.odeo.co.id/docs/payment-gateway</li>
        <li>You can simulate Payment Gateway here: https://api.staging.odeo.co.id/v1/test/pg/{base64_encode(client_id)}</li>
        <li>Credentials for dashboard account:
            <ul>
                <li>phone number : {{ $telephone }}</li>
                <li>password : {{ $password }}</li>
                <li>pin : {{ $pin }}</li>
            </ul>
        </li>
        <li>You need to login to Business Dashboard for get your client id, secret key and signing key</li>
        <li>You need to change your password and pin when you login for the first time</li>
        <li>You can also view payment and disbursement history from the side menu</li>
    </ul>
@endsection

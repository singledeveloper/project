@extends('emails.base_payment')

@section('header')
  <img src="https://s3-ap-southeast-1.amazonaws.com/odeo/mail/trx_menunggu.png" style="width: 100%" alt=""/>
@endsection

@section('styles')
  <style type="text/css">
    #title {
      text-align: center;
      font-size: 24px;
      margin: 0;
      margin-top: 20px;
      font-weight: bold;
    }
    #time-description-text {
      text-align: center; 
      margin: 5px; 
      margin-top: 15px;
      font-size: 16px;
    }
    #time-text {
      text-align: center; 
      margin: 5px; 
      margin-bottom: 15px; 
      font-size: 24px;
      font-weight: bold;
    }
    #bank-transfer-table {
      padding: 15px 20px;
    }
    .dashed-separator {
      border-color: rgb(238,238,238); 
      border-style: dashed;
    }
    .bank-details {
        margin: 0px;
        font-weight: bold;
        line-height: 1.5;
    }
  </style>
@endsection

@section('content')
    <p id="title" >Transaksi Menunggu Pembayaran</p>
    <p class="center-text text-14">Selesaikan pembelian Anda di <a href="{{$data['domain']}}" target="_blank" class="link">{{$data['name']}}</a></p>

    @include('emails.order_summary')

    <hr class="separator"/>

    <p id="time-description-text">Mohon selesaikan pembayaran Anda sebelum</p>
    
    <p id="time-text">{{ date("l j F Y H:i", strtotime($data['date_expired'] ))}} WIB</p>

    <hr class="dashed-separator"/>

    <table id="bank-transfer-table" width="550" cellpadding="0" cellspacing="0">
      <tr>
        <td style="vertical-align: top; width: 65px">
          <img src="{{ $data['payment_logo_path'] }}" alt="" width="50" style="margin-right: 10px"/>
        </td>
        <td>
          <p class="bank-details">{{$data['payment_data']['detail']['acc_bank']}}</p>
          <p class="bank-details">{{$data['payment_data']['detail']['acc_name']}}</p>
          <p class="bank-details">{{$data['payment_data']['detail']['acc_branch']}}</p>
        </td>
        <td>
          <p class="no-margin right-text text-12 gray-text">No. Rekening</p>
          <p class="no-margin right-text bold-text text-24" style="line-height: 1.5;">{{$data['payment_data']['detail']['acc_number']}}</p>
        </td>
      </tr>
    </table>

    @include('emails.order_detail_table')

@endsection

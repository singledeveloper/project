<!doctype html>
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>Unsettled BNI EDC Transactions - {{$date}}</h1>

<table>
    <tr>
        <th>Proc Date</th>
        <th>Count</th>
    </tr>
    @foreach($counts as $procDate => $count)
        <tr>
            <td>{{$procDate}}</td>
            <td>{{$count}}</td>
        </tr>
    @endforeach
</table>

</body>
</html>
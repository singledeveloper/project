@extends('emails.base')

@section('styles')
  <style type="text/css">
    #transaction-id-label {
      margin: 0px;
      text-align: right;
    }
    #transaction-id {
      color: rgb(0,151,143);
      font-weight: bold;
      margin: 2px 0px 0px 0px;
      text-align: right;
    }
    #purchase-at {
      margin: 0px;
      text-align: right;
    }
    #transaction-desc-container {
      width: 100%;
      margin-bottom: 10px;
    }
    #transaction-desc {
      margin: 10px 0px 0px 0px;
    }
    .transaction-detail-label {
      margin: 10px 0px;
    }
    .transaction-detail-value {
      margin: 10px 0px;
      font-weight: bold;
    }
    .transaction-detail-colon {
      margin: 10px;
    }
    .separator {
      border-color: rgb(238,238,238);
      border-style: solid;
    }
    #tnc-container {
      text-align: justify;
    }
    .tnc-item {
      line-height: 1.5;
    }
    ol {
      margin: 3px;
    }
  </style>
@endsection

@section('content')
  <table style="margin-bottom: 10px;" cellpadding="0" cellspacing="0">
    <tr>
      <td width="350">
        <p class="text-14">Dear <b>{{$data['name'] or '-'}}</b>,</p>
        <p class="text-14" id="transaction-desc">Transaksi <b>{{$data['item_name']}}</b> Anda telah berhasil, dengan rincian sebagai berikut:</p>
      </td>
      <td width="200" rowspan="2">
        <p class="gray-text text-14" id="transaction-id-label">ID Transaksi</p>
        <p class="text-18" id="transaction-id">#{{$data['order_id'] or '-'}}</p>
        <p class="text-14" id="purchase-at">{{$data['purchase_at']}}</p>
      </td>
    </tr>
  </table>

  <table id="transaction-desc-container">
    <tr>
      <td style="background-color: rgb(248,248,248)" width="500">
      <table style="margin: 20px 30px;" cellpadding="0" cellspacing="0">
        <tr>
          <td width="220">
            <p class="gray-text transaction-detail-label">Kode</p>
          </td>
          <td>
            <p class="transaction-detail-colon">:</p>
          </td>
          <td>
            <p class="transaction-detail-value">{{$data['voucher_code'] or '-'}}</p>
          </td>
        </tr>
        <tr>
          <td width="220">
            <p class="gray-text transaction-detail-label">Serial Number</p>
          </td>
          <td>
            <p class="transaction-detail-colon">:</p>
          </td>
          <td>
            <p class="transaction-detail-value">{{$data['serial_number'] or '-'}}</p>
          </td>
        </tr>
      </table>
      </td>
    </tr>
  </table>

  <table align="center" cellpadding="0" cellspacing="0" width="100%" class="content text-12">
    <tr>
      <td id="tnc-container">
        <strong class="gray-text">Instruksi Penukaran Kode Voucher Google Play</strong>
        <hr class="separator"/>
        {{trans('google_play.redeem_instruction_1')}}
        <ol>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_1_list_1')}}</li>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_1_list_2')}}</li>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_1_list_3')}}</li>
        </ol>
        <br/>
        {{trans('google_play.redeem_instruction_2')}}
        <ol>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_2_list_1')}}</li>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_2_list_2')}}</li>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_2_list_3')}}</li>
          <li class="tnc-item">{{trans('google_play.redeem_instruction_2_list_4')}}</li>
        </ol>
        <br/>
        <strong class="gray-text">Syarat dan Ketentuan</strong>
        <hr class="separator"/>
        <span class="tnc-item">{{trans('google_play.tnc')}}</span>
      </td>
    </tr>
  </table>
@endsection

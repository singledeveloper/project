@extends('emails.base')

@section('styles')
  <style type="text/css">
    #detail-box {
      margin: 20px auto;
      background-color: rgb(248,248,248);
    }
    #inner-detail-box {
      margin: 20px 30px;
    }
    .left-box-text {
      color: rgb(144,145,146); 
      margin: 10px 0px;
      font-size: 14px;
    }
    .middle-box-text {
      color: rgb(144,145,146);
      margin: 10px 10px;
      font-size: 14px;
    }
    .right-box-text {
      margin: 10px 0px;
      font-size: 14px;
    }
  </style>
@endsection

@section('content')
    <table id="content-table" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <p class="no-margin text-14">Dear <b>{{$data['name'] or '-'}}</b>,</p>
        </td>
        <td width="45%">
          <p class="no-margin text-14 gray-text right-text">ID Transaksi</p>
        </td>
      </tr>
      <tr>
        <td rowspan="2">
          <p class="no-margin text-14">{{$data['is_bank_transfer'] ? 'Transfer Bank' : 'Penarikan Uang'}} Anda telah berhasil, dengan rincian sebagai berikut:</p>
        </td>
        <td>
          <p class="text-18 green-text bold-text right-text no-margin">#{{$data['withdraw_id'] or '-'}}</p>
        </td>
      </tr>
      <tr>
        <td>
          <p class="no-margin text-14 right-text">{{$data['verified_at']}}</p>
        </td>
      </tr>
    </table>

    <table id="detail-box">
      <tr>
        <td width="520">
        <table id="inner-detail-box" cellpadding="0" cellspacing="0">
          <tr>
            <td width="220">
              <p class="left-box-text">Jumlah</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['amount'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Biaya administrasi</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['fee'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Nama Pemilik Rekening</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['account_name'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Nomor Rekening</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['account_number'] or '-'}}</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="left-box-text">Bank</p>
            </td>
            <td>
              <p class="middle-box-text">:</p>
            </td>
            <td>
              <p class="right-box-text">{{$data['account_bank'] or '-'}}</p>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
@endsection

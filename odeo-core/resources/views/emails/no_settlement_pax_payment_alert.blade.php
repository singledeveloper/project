<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
<h1>No Settlement Pax Payment</h1>
<table>
    <tr>
        <th>PaxPaymentID</th>
        <th>Order ID</th>
        <th>Trx Date</th>
        <th>Amount</th>
        <th>Card Number</th>
        <th>Approval Code</th>
    </tr>
    @foreach($payments as $payment)
        <tr>
            <td>{{$payment->id}}</td>
            <td>{{$payment->order_id}}</td>
            <td>{{$payment->trx_date}}</td>
            <td>{{$payment->total_amount}}</td>
            <td>{{$payment->pan_number}}</td>
            <td>{{$payment->approval_code}}</td>
        </tr>
    @endforeach
</table>

</body>
</html>
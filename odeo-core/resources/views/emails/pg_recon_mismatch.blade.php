<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">
@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<h2>{{$data['vendor']}} Recon Mismatch for {{$data['date']}}</h2>

Settlement description: {{$data['description']}} <br>
Bank settlement amount: {{$currencyHelper->formatPrice($data['total_amount'])['formatted_amount']}} <br>
Calculated settlement amount: {{$currencyHelper->formatPrice($data['settlement_amount'])['formatted_amount']}} <br>

</body>
</html>
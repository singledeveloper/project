<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
</head>
<body class="container">

<div class="row">
    <div class="col-md-2">
        <small>ORDER ID</small>
        <p class="bold">{{$data['order_id']}}</p>
    </div>
    <div class="col-md-2">
        <small>STATUS</small>
        <p class="bold primary">{{'SUCCESS'}}</p>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-2">
        <small>CREATED AT</small>
        <p class="bold">{{$data['created_at']}}</p>
    </div>
    <div class="col-md-2">
        <small>PAID AT</small>
        <p class="bold">{{$data['paid_at']}}</p>
    </div>
    <div class="col-md-2">
        <small>CLOSED AT</small>
        <p class="bold">{{$data['closed_at']}}</p>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-2">
        <small>PRODUCT</small>
        <p class="bold">{{$data['cart_data']['items']['name']}}</p>
    </div>
    @if(isset($data['cart_data']['items']['item_detail']['number']))
        <div class="col-md-2">
            <small>NUMBER</small>
            <p class="bold">{{$data['cart_data']['items']['item_detail']['number']}}</p>
        </div>
    @endif
    @if(isset($data['cart_data']['items']['item_detail']['serial_number']))
        <div class="col-md-2">
            <small>SERIAL NUMBER</small>
            <p class="bold">{{$data['cart_data']['items']['item_detail']['serial_number']}}</p>
        </div>
    @endif
</div>

<hr>

<div class="row">
    <div class="col-md-2">
        <small>PAYMENT</small>
        <p class="bold">{{$data['payment_data']['name']}}</p>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-2">
        <small>TOTAL</small>
        <p class="bold">{{$data['total']['formatted_amount']}}</p>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
        integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
        crossorigin="anonymous"></script>


</html>

<style>


</style>

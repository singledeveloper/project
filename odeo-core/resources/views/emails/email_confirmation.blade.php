@extends('emails.base')

@section('styles')
    <style type="text/css">
        #button-container {
            margin: 30px 0px;
            text-align: center;
        }
        #button {
            text-decoration: none; 
            color: white; 
            background-color: rgb(0,151,143);
            padding: 15px 30px;
            font-weight: bold;
            border-radius: 5px;
        }   
    </style>
@endsection

@section('content')
    <p class="center-text text-24 bold-text no-margin">Email Confirmation</p>
    <p class="center-text text-14" style="margin: 10px">Mohon konfirmasi email Anda dengan klik tombol dibawah ini:</p>
    <p id="button-container">
        <a class="text-14" id="button" href="{{ $url }}" target="_blank">VERIFIKASI EMAIL</a>
    </p>
@endsection

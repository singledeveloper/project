@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

@if(count($odeoOnly) > 0)
  <p>The following order(s) <strong>only exists on Odeo</strong> and missing from Doku Mutation:</p>
  <ul>
    @foreach($odeoOnly as $item)
      <li>{{ $item['odeo_order_id'] }}</li>
    @endforeach
  </ul>
  <br><br>
@endif

@if(count($dokuOnly) > 0)
  <p>The following order(s) <strong>only exists on Doku mutation</strong> and missing from Odeo:</p>
  <ul>
    @foreach($dokuOnly as $item)
      <li>{{ $item['doku_order_id'] }}</li>
    @endforeach
  </ul>
@endif

@if(!empty($wrongAmounts))
  <p>The following order(s) have <strong>mismatch total amount:</strong></p>
  <table border="1" cellspacing="0" cellpadding="5" class="text-center">
    <tr>
      <th>Order Id</th>
      <th>Odeo Amount</th>
      <th>Doku Amount</th>
    </tr>
    @foreach($wrongAmounts as $item)
      <tr>
        <td>{{ $item['odeo_order_id'] }}</td>
        <td>{{ $currencyHelper->formatPrice($item['odeo_total'])['formatted_amount'] }}</td>
        <td>{{ $currencyHelper->formatPrice($item['doku_total'])['formatted_amount'] }}</td>
      </tr>
    @endforeach
  </table><br><br>
@endif

@if(!empty($statusNotMatch))
  <p>The following order(s) have <strong>mismatch status:</strong></p>
  <table border="1" cellspacing="0" cellpadding="5" class="text-center">
    <tr>
      <th>Order Id</th>
      <th>Odeo Status</th>
      <th>Doku Status</th>
    </tr>
    @foreach($statusNotMatch as $item)
      <tr>
        <td>{{ $item['odeo_order_id'] }}</td>
        <td>{{ $item['odeo_status'] }}</td>
        <td>{{ $item['doku_status'] }}</td>
      </tr>
    @endforeach
  </table>
@endif

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body style="font-family: 'Arial'; font-size: 14px">

  <h2>Attention!</h2>
  Your current <?php echo $data['from']; ?> balance is under <?php echo $data['minimum_balance']['formatted_amount'] ?>
  <br><br>
  Please immediately topup to <?php echo $data['from']; ?> before <?php echo $data['from']; ?> products completely out of stock!<br><br>
  current balance : <?php echo $data['current_balance']['formatted_amount'] ?><br>

</body>
</html>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
  .text-center, .text-center td, .text-center th {
    text-align: left;
  }

  tr.total td {
    font-weight: bold;
  }
</style>

@inject('currencyHelper', 'Odeo\Domains\Transaction\Helper\Currency')

<body style="font-family: 'Arial'; font-size: 14px">

<p>The following settlement(s) of Mandiri E-cash have mismatch amount:</p>
<ul>
  @foreach  ($settlements as $settlementId => $mutations)
    <li>
      <p> Settlement ID: <b>{{ $settlementId }} </b></p>
      <p> Settlement Amount: <b>{{ $currencyHelper->formatPrice($mutations[0]->settlement_amount)['formatted_amount'] }}</b></p>
      <p> Mutation(s):
        <table border="1" cellspacing="0" cellpadding="5" class="text-center">
          <tr>
            <th>Mutation ID</th>
            <th>Amount</th>
          <tr/>
          @foreach ($mutations as $mutationDetail)
            <tr>
              <td>{{ $mutationDetail->id }}</td>
              <td>{{ $currencyHelper->formatPrice($mutationDetail->amount)['formatted_amount'] }}</td>
            </tr>
          @endforeach
          <tr>
            <th>Total</th>
            <th>{{ $currencyHelper->formatPrice($mutations->total_amount)['formatted_amount'] }}</th>
          </tr>
        </table>
      </p>
      <br/>
    </li>
  @endforeach
</ul>

</body>
</html>
<?php
return [
  'user_id_exists' => '用户ID已存在',
  'billed_user_not_found' => '用户不存在',
  'template_outdated' => '您的模板格式已过期。 请下载并使用最新的模板（:version)',
  'template_input_invalid' => '您的输入格式不正确，请检查您的输入',
  'invoice_user_exists' => '用户发票已经存在',
  'invoice_user_not_found' => '找不到用户发票',
];
<?php

return [
  'tnc_secret_key_1' => '将您的秘密密钥保存在安全的地方。',
  'tnc_secret_key_2' => '如果有人知道您的密钥，那么您应该对密钥进行更改。',
  'tnc_secret_key_3' => 'ODEO不会告知，更改，删除所有者的秘密密钥。 所有导致他人使用您的秘密密钥的情况，都是所有者的责任。'
];

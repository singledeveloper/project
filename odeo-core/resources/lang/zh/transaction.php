<?php

use Odeo\Domains\Constant\TransactionType;

return [
  'via' => 'Via',
  'product_name' => 'Product Name',
  'customer_number' => 'Customer Number',
  'bank_name' => 'Bank Name',
  'bank_account_number' => 'Account Number',
  'bank_account_name' => 'Account Name',
  'from' => 'From',
  'to' => 'To',
  'notes' => 'Notes',
  'store_name' => 'Store Name',
  'mutation_id' => 'Mutation ID',

  'trx_type_' . TransactionType::TOPUP => 'Topup',
  'trx_type_' . TransactionType::WITHDRAW => 'Withdraw',
  'trx_type_' . TransactionType::ORDER_CONVERT => 'Sales Order',
  'trx_type_' . TransactionType::REFUND => 'Refund',
  'trx_type_' . TransactionType::SPONSOR_BONUS => 'Invite Bonus',
  'trx_type_' . TransactionType::PAYMENT => 'Payment',
  'trx_type_' . TransactionType::TRANSFER => 'Transfer',
  'trx_type_' . TransactionType::PRESET_CASHBACK => 'Weekdays Restock Cashbacks',
];
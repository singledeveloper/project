<?php

return [
  'amount_under_minimum' => 'Payment is under minimum',
  'amount_above_maximum' => 'Payment is above maximum',
  'order_is_in_pending_payment' => 'Please complete your previous chosen payment to complete your order',
  'order_has_been_paid' => 'Your order has been paid',
  'order_is_cancelled' => 'Your order has been cancelled',
  'payment_channel_inactive' => 'Payment channel is not active',
  'unrecognized_va_number' => 'Unrecognized VA code',
  'va_max_length_exceeded' => 'VA max length exceeded'
];

<?php

return [
  'require_grant' => 'GrantType is required',
  'invalid_grant' => 'Invalid grant',
  'invalid_request' => 'Invalid request',
  'unsupported_grant_type' => 'Unsupported grant type',
  'invalid_scope' => 'Invalid scope',
  'invalid_client' => 'Invalid client',
  'invalid_auth_header' => 'Invalid Authorization header',
  'invalid_data' => 'Invalid data'
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute必须被接受',
    'active_url'           => ':attribute不是有效的网址',
    'after'                => ':attribute必须是:date之后的日期',
    'alpha'                => ':attribute只能包含字母',
    'alpha_dash'           => ':attribute只能包含字母，数字和破折号',
    'alpha_num'            => ':attribute只能包含字母和数字',
    'array'                => ':attribute必须是一个数组',
    'before'               => ':attribute必须是:date之前的日期',
    'between'              => [
        'numeric' => ':attribute必须介于:min和:max',
        'file'    => ':attribute必须介于:min和:max千字节',
        'string'  => ':attribute必须介于:min和:max字符',
        'array'   => ':attribute必须介于:min和:max项目',
    ],
    'boolean'              => ':attribute字段必须为true或false',
    'confirmed'            => ':attribute确认不符',
    'date'                 => ':attribute不是有效日期',
    'date_format'          => ':attribute与:format格式不符',
    'different'            => ':attribute和:other必须是不同的',
    'digits'               => ':attribute必须:digits个数字',
    'digits_between'       => ':attribute必须介于:min和:max个数字',
    'email'                => ':attribute必须是有效的电子邮件地址',
    'exists'               => '选择的:attribute无效',
    'filled'               => ':attribute必填项',
    'image'                => ':attribute必须是图像',
    'in'                   => '选择的:attribute无效',
    'integer'              => ':attribute必须是整数',
    'ip'                   => ':attribute必须是有效的IP地址',
    'json'                 => ':attribute必须是有效的JSON字符串',
    'max'                  => [
        'numeric' => ':attribute不得大于:max',
        'file'    => ':attribute不得大于:max千字节',
        'string'  => ':attribute不得大于:max字符',
        'array'   => ':attribute可能不超过:max个项目',
    ],
    'mimes'                => ':attribute必须是:values类型的文件',
    'mimetypes'            => ':attribute必须是:values类型的文件',
    'min'                  => [
        'numeric' => ':attribute必须至少:min',
        'file'    => ':attribute必须至少:min千字节',
        'string'  => ':attribute必须至少:min字符',
        'array'   => ':attribute必须至少:min项目',
    ],
    'not_in'               => '选择的:attribute是无效的',
    'numeric'              => ':attribute必须是一个数字',
    'regex'                => ':attribute格式无效',
    'required'             => ':attribute是必填项',
    'required_if'          => ':attribute当:other是:value时必填',
    'required_unless'      => ':attribute除非:other是在:values否则必填',
    'required_with'        => ':attribute当:value存在时必填',
    'required_with_all'    => ':attribute当:value存在时必填',
    'required_without'     => ':attribute当:value不存在时必填',
    'required_without_all' => ':attribute当没有:values是存在时必填',
    'same'                 => ':attribute和:other必须匹配',
    'size'                 => [
        'numeric' => ':attribute必须是:size',
        'file'    => ':attribute必须是:size千字节',
        'string'  => ':attribute必须是:size字符',
        'array'   => ':attribute必须包含:size项目',
    ],
    'string'               => ':attribute必须是一个字符串',
    'timezone'             => ':attribute必须是有效区域',
    'unique'               => ':attribute必须是用过的',
    'url'                  => ':attribute格式无效',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];

<?php

return [
  'new_year' => 'New Year :year',
  'chinese_new_year' => 'Chinese New Year :year',
  'isra_miraj' => 'Isra\' Mi\'raj Prophet Muhammad SAW​',
  'nyepi' => 'Nyepi :year',
  'good_friday' => 'Good Friday',
  'international_labor' => 'International Labor Day',
  'vesak' => 'Vesak Day :year',
  'ascension_day' => 'Ascension Day of Jesus Christ',
  'eid_al_fitr' => 'Eid al-Fitr :year H',
  'pancasila_birthday' => 'Birthday of Pancasila',
  'eid_al_adha' => 'Eid al-Adha :year H',
  'independence_day' => '​Independence​ Day of Indonesia​',
  'islamic_new_year' => '​Islamic New Year :year H',
  'maulid' => 'Birthday of Prophet Muhammad SAW',
  'christmas' => 'Christmas'
];
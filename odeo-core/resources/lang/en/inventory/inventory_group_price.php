<?php

return [

  'group_price_type_agent_default' => 'Agent (default)',
  'group_price_description_agent_default' => 'default agent pricing',

  'group_price_type_market_place' => 'Market Place',
  'group_price_description_market_place' => 'Sell price on ODEO market place',

  'group_price_type_self' => 'Self Purchase',
  'group_price_description_self' => 'your own store purchasing price',

  ];

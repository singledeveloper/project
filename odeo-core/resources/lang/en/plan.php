<?php

return [
  'plan_name_1' => 'lite',
  'plan_name_2' => 'Starter',
  'plan_name_3' => 'Business',
  'plan_name_4' => 'Pro',
  'plan_name_5' => 'Enterprise',
  'plan_name_99' => 'Free',

  'description_of_id_1' => 'Everything you need to start an online store and selling pulsa.',
  'description_of_id_2' => 'Connect to your own domain name, sell more pulsa everyday and make a lot more money.',
  'description_of_id_3' => 'Start selling flight tickets and expand your store. You are now unstoppable and making a bunch.',
  'description_of_id_4' => 'Start selling hotel tickets like the pro and expand your store. You are now unstoppable and making a bunch.',
  'description_of_id_5' => 'Be above all and sell everything you\'ve ever wanted. Be the King of the Jungle.',

  'sub_name_of_id_1' => 'Individual',
  'sub_name_of_id_2' => 'Starter Pack',
  'sub_name_of_id_3' => 'Most Popular',
  'sub_name_of_id_4' => 'Recommended',
  'sub_name_of_id_5' => 'Most Profitable',

  'pulsa_inventory' => 'Sell Phone Credit',
  'paket_data_inventory' => 'Sell Data Plan',
  'bolt_inventory' => 'Sell Bolt Credit',
  'pln_inventory' => 'Sell PLN Token',
  'pln_postpaid_inventory' => 'Pay Postpaid PLN',
  'bpjs_kes_inventory' => 'Pay BPJS Kesehatan',
  'bpjs_tk_inventory' => 'Pay BPJS Tenagakerjaan',
  'pulsa_postpaid_inventory' => 'Pay Postpaid Phone Bill',
  'broadband_inventory' => 'Pay Internet Bill',
  'landline_inventory' => 'Pay Telephone Bill',
  'pdam_inventory' => 'Pay PDAM Bill',
  'game_voucher_inventory' => 'Sell Game Voucher',
  'google_play_inventory' => 'Sell Google Play Voucher',
  'transportation_inventory' => 'Top Up Go-Pay / GrabPay / e-Toll',

  'bo_title' => 'Business Opportunity',
  'bo_of_id_1' => [
    "Mentoring Bonus" => "Up to Rp78,000/hustler",
    /*"Team Commission" => "$0",
    "Residual Team Commission" => "$0",
    "Team Earning Potential" => "$0"*/
  ],
  'bo_of_id_2' => [
    "Mentoring Bonus" => "Up to Rp390,000/hustler",
    /*"Team Commission" => "Up to 1x$25/day",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Up to $50/day"*/
  ],
  'bo_of_id_3' => [
    "Mentoring Bonus" => "Up to Rp1,170,000/hustler",
    /*"Team Commission" => "Up to 4x$25/day",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Up to $200/day"*/
  ],
  'bo_of_id_4' => [
    "Mentoring Bonus" => "Up to Rp2,340,000/hustler",
    /*"Team Commission" => "Up to 8x$25/day",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Up to $500/day"*/
  ],
  'bo_of_id_5' => [
    "Mentoring Bonus" => "Up to Rp3,900,000/hustler",
    /*"Team Commission" => "Up to 12x$25/day",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Up to $1,000/day"*/
  ],
  'bo_of_id_99' => [
    "Mentoring Bonus" => "Not available",
    /*"Team Commission" => "Up to 12x$25/day",
    "Residual Team Commission" => "$5",
    "Team Earning Potential" => "Up to $1,000/day"*/
  ],

  'tnc_plan_1' => 'The listed price does not include store deposit.',
  'tnc_plan_2' => 'You will get Rp200,000 deposit only if you have completed the payment process and have inputted referral code from other store.',
  'tnc_plan_3' => 'Payment for plan purchase can not be refunded in any form.',

  'tnc_upgrade_plan_1' => 'The listed price is substracted with prorated price of your current plan remaining days.',
  'tnc_upgrade_plan_2' => 'Store active period will be extended by 6 months, starting from date of upgrade.',
  'tnc_upgrade_plan_3' => 'Payment for plan upgrade can not be refunded in any form.'

];

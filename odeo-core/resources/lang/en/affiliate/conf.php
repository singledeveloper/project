<?php

return [
  'tnc_secret_key_1' => 'Simpan secret key anda di tempat yang aman.',
  'tnc_secret_key_2' => 'Apabila ada yang mengetahui secret key anda, diharapkan untuk melakukan perubahan secret key.',
  'tnc_secret_key_3' => 'Odeo tidak akan menyebar, mengubah, menghapus secret key pemilik. Segala kondisi yang menyebabkan secret key anda diketahui/dipakai oleh pihak lain tanpa sepengetahuan pemilik adalah tanggung jawab pemilik.'
];

<?php

return [
  'name' => 'Disbursement',
  'inquiry_name' => 'Bank Account Verification',
  'not_ready' => 'Batch Disbursement is not ready yet, make sure all validations are successful',
  'template_outdated' => 'Your template is outdated. Please download and use the latest template (:version)',
  'cant_delete_the_only_disbursement' => 'You can not delete the only disbursement',
];
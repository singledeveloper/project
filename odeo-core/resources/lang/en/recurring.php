<?php
use Odeo\Domains\Constant\Recurring;

return [
  Recurring::TYPE_ONCE => 'Once',
  Recurring::TYPE_DAY => 'Everyday',
  Recurring::TYPE_DAY_WORK => 'Everyday (Mon - Fri)',
  Recurring::TYPE_DAY_MON_SAT => 'Everyday (Mon - Sat)',
  Recurring::TYPE_WEEK . '_1' => 'Every Monday',
  Recurring::TYPE_WEEK . '_2' => 'Every Tuesday',
  Recurring::TYPE_WEEK . '_3' => 'Every Wednesday',
  Recurring::TYPE_WEEK . '_4' => 'Every Thursday',
  Recurring::TYPE_WEEK . '_5' => 'Every Friday',
  Recurring::TYPE_WEEK . '_6' => 'Every Saturday',
  Recurring::TYPE_WEEK . '_7' => 'Every Sunday',
  Recurring::TYPE_MONTH => 'Every Month',
  'date' => 'Date',
  'time_format' => '\a\t gA \G\M\T+7'
];

<?php
use Odeo\Domains\Constant\Payment;

return [
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_EMONEY => 'Uang Elektronik',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_INTERNET_BANKING => 'Internet Banking',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_ONLINE_LOAN => 'Pinjaman Online',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_DEBIT_CARD => 'Kartu Debit',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_CREDIT_CARD => 'Kartu Kredit',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_INSTALLMENT => 'Cicilan',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_BANK_TRANSFER => 'Transfer Bank',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_ATM_TRANSFER => 'Transfer ATM',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_GROCERY_STORE => 'Mini Market',
  'payment_group_' . Payment::PAYMENT_METHOD_GROUP_DEBIT_CREDIT_CARD_PRESENT => 'Kartu Debit / Kredit',
];

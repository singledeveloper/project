<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:29 PM
 */
use Odeo\Domains\Constant\OrderStatus;

return [
  'order_charge_unique_code' => 'Kode Unik',
  'order_charge_payment_service_cost' => 'Biaya Layanan Pembayaran',
  'order_charge_ocash' => 'Pembayaran oCash',
  'order_charge_referral_cashback' => 'Referral Cashback',

  'order_status_' . OrderStatus::CREATED => 'Melakukan checkout',
  'order_status_' . OrderStatus::OPENED => 'Menunggu pembayaran',
  'order_status_' . OrderStatus::WAITING_FOR_UPDATE => 'Menunggu perubahan data',
  'order_status_' . OrderStatus::CONFIRMED => 'Menunggu verifikasi admin',
  'order_status_' . OrderStatus::WAITING_SUSPECT => 'Menunggu update operator (1x24 jam)',
  'order_status_' . OrderStatus::PARTIAL_FULFILLED => 'Sedang diproses',
  'order_status_' . OrderStatus::COMPLETED => 'Transaksi selesai',
  'order_status_' . OrderStatus::CANCELLED => 'Transaksi dibatalkan',
  'order_status_' . OrderStatus::REFUNDED => 'Transaksi dibatalkan, dana dikembalikan',

  'product_name' => 'Nama Produk',
  'customer_number' => 'No. Pelanggan',
  'store_name' => 'Nama Toko',
  'store_domain' => 'Domain Toko',
  'name' => 'Nama',
  'customer_id' => 'ID Pelanggan',
  'customer_name' => 'Nama Pelanggan',
  'kwh' => 'KWH',
  'tariff' => 'Tariff/Daya',
  'total_month' => 'Jumlah Bulan',
  'reference_number' => 'No. Referensi',
  'period' => 'Periode',
  'player_id' => 'ID Player',
  'amount_due' => 'Jumlah Tagihan',
  'admin_fee' => 'Biaya Administrasi',
  'discount' => 'Diskon',
  'fine' => 'Denda',
  'amount' => 'jumlah',
  'product_price' => 'Harga Produk',
  'meter_change' => 'Perubahan Meter',
  'serial_number' => 'Serial Number',
  'usage' => 'Pemakaian',
  'due_date' => 'Jatuh Tempo'
];

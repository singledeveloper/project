<?php

return [
  'free' => 'Gratis',
  'chat_notification' => 'Customer service kami buka setiap hari jam 06:00 sampai 24:00 WIB',
  'row_field' => 'row :row\'s :field',
  'or' => ':a atau :b',

  'rp_label' => 'Rp'
];
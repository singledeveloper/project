<?php

return [
  'amount_under_minimum' => 'Pembayaran dibawah minimum',
  'amount_above_maximum' => 'Pembayaran diatas maximum',
  'order_is_in_pending_payment' => 'Harap untuk menyelesaikan pembayaran terakhir anda',
  'order_has_been_paid' => 'Pesanan anda telah dibayar',
  'order_is_cancelled' => 'Pesanan anda telah dibatalkan',
  'payment_channel_inactive' => 'Saluran pembayaran ditutup',
  'unrecognized_va_number' => 'Kode VA tidak ditemukan',
  'va_max_length_exceeded' => 'Kode VA terlalu panjang',
];

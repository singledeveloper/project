<?php

use Odeo\Domains\Constant\TransactionType;

return [
  'via' => 'Via',
  'product_name' => 'Nama Produk',
  'customer_number' => 'Nomor Pelanggan',
  'bank_name' => 'Nama Bank',
  'bank_account_number' => 'Nomor Rekening',
  'bank_account_name' => 'Nama Pemilik Rekening',
  'from' => 'Dari',
  'to' => 'Ke',
  'notes' => 'Keterangan',
  'store_name' => 'Nama Toko',
  'mutation_id' => 'ID Mutasi',

  'trx_type_' . TransactionType::TOPUP => 'Topup',
  'trx_type_' . TransactionType::WITHDRAW => 'Pencairan Dana',
  'trx_type_' . TransactionType::ORDER_CONVERT => 'Sales Order',
  'trx_type_' . TransactionType::REFUND => 'Pengembalian Dana',
  'trx_type_' . TransactionType::SPONSOR_BONUS => 'Bonus Invite',
  'trx_type_' . TransactionType::PAYMENT => 'Pembelian',
  'trx_type_' . TransactionType::TRANSFER => 'Transfer',
  'trx_type_' . TransactionType::PRESET_CASHBACK => 'Cashback Restock Weekdays',
];
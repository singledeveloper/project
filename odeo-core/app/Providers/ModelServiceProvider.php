<?php

namespace Odeo\Providers;

use Illuminate\Support\ServiceProvider;
use Odeo\Domains\Core\ModelObserver;

class ModelServiceProvider extends ServiceProvider {

  public function register() {

    $modelObserver = new ModelObserver;

    \Odeo\Domains\Inventory\Helper\Service\Pulsa\Model\PulsaPrefix::observe($modelObserver);
    \Odeo\Domains\Inventory\Model\ServiceDetail::observe($modelObserver);

  }
}

<?php

namespace Odeo\Providers;

use Carbon\Carbon;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Exception;

class AppServiceProvider extends ServiceProvider {

  public function register() {

    setlocale(LC_TIME, config('app.locale'));
    \Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ApiManager::init();
    \Odeo\Domains\Vendor\Bca\Helper\ApiManager::init();
//    \Odeo\Domains\Vendor\Flip\Helper\FlipApi::init();
    \Odeo\Domains\Vendor\Cimb\Helper\ApiManager::init();
    \Odeo\Domains\Vendor\Bni\Helper\ApiManager::init();
    \Odeo\Domains\Vendor\OdeoV2\Helper\ApiManager::init();
    \Odeo\Domains\Vendor\Permata\Helper\ApiManager::init();
    \Odeo\Domains\Vendor\Bri\Helper\ApiManager::init();
    \Odeo\Domains\Vendor\Mandiri\Helper\ApiManager::init();

    if (env('DB_DEBUG')) {
      \DB::listen(function ($query) {
        clog('sql_debug', $query->sql);
        clog('sql_debug', json_encode($query->bindings));
        clog('sql_debug', $query->time);
      });
    }

    \Queue::failing(function (JobFailed $event) {
      if (app()->environment() == 'production') {
        app('sentry')->captureException(new Exception(json_encode($event)));
      } else {
        \Log::info(\json_encode($event));
      }
    });

    \Queue::looping(function () {
      while (\DB::transactionLevel() > 0) {
        \DB::rollBack();
      }
    });

  }
}

<?php
namespace Odeo\Domains\VirtualAccount\Model;

use Odeo\Domains\Core\Entity;

class UserVirtualAccount extends Entity {

  protected $dates = ['created_at', 'updated_at'];

  public $timestamps = true;

  public function details() {
    return $this->hasMany(UserVirtualAccountDetail::class);
  }
}

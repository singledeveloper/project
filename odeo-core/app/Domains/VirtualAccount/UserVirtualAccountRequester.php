<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/19
 * Time: 16.59
 */

namespace Odeo\Domains\VirtualAccount;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;

class UserVirtualAccountRequester extends VirtualAccountManager {

  private $invoiceUsers, $invoices;

  function __construct() {
    parent::__construct();
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
    $this->invoices = app()->make(AffiliateUserInvoiceRepository::class);
  }

  public function requestVa(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    if (!isset($data['billed_user_id'])) {
      $data['billed_user_id'] = $this->invoiceUsers->findByIdAndCreatedBy($data['id'], $userId)->user_id;
    }

    $userVa = $this->userVirtualAccounts->findByUserIdAndCreatedBy($data['billed_user_id'], $userId);

    $vaNumbers = $this->getUserVirtualAccounts($userVa->id, $userId);

    return $listener->response(200, [
      'virtual_accounts' => $vaNumbers
    ]);
  }

  public function requestInvoiceVa(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $invoice = $this->invoices->findByUserIdAndInvoiceId($data['auth']['user_id'], $data['id']);
    $userVa = $this->userVirtualAccounts->findByUserIdAndCreatedBy($invoice->billed_user_id, $userId);

    $vaNumbers = $this->getUserVirtualAccounts($userVa->id, $userId);

    return $listener->response(200, [
      'virtual_accounts' => $vaNumbers
    ]);
  }

}
<?php
namespace Odeo\Domains\VirtualAccount\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\VirtualAccount\Model\VirtualAccountVendor;

class VirtualAccountVendorRepository extends Repository {

  public function __construct(VirtualAccountVendor $virtualAccountVendors) {
    $this->model = $virtualAccountVendors;
  }

  public function getActive() {
    return $this->findAllByAttributes('is_active', true);
  }

  public function getByCode($code) {
    $query = $this->getCloneModel();
    if (is_array($code)) {
      return $query->whereIn('code', $code)->get();
    }
    return $query->where('code', $code)->get();
  }

  public function findByPrefix($vaCode) {
    return $this->model
      ->whereRaw("left('$vaCode', length(prefix)) = prefix")
      ->where('is_active', true)
      ->first();
  }

  public function findDokuPrefix($vaCode) {
    return $this->model
      ->where('code', 'like', 'doku%')
      ->whereRaw("left('$vaCode', length(prefix)) = prefix")
      ->where('is_active', true)
      ->first();
  }

  public function getAffiliatePaymentVendors() {
    return $this->model->where('support_affiliate_payment', true)->get();
  }

  public function getAffiliatePaymentVendorsByCode($code) {
    $query = $this->getCloneModel();
    $query = $this->model->where('support_affiliate_payment', true);
    if (is_array($code)) {
      return $query->whereIn('code', $code)->get();
    }
    return $query->where('code', $code)->get();
  }

  public function getFeeDataOnUser($userId) {
    return \DB::select(\DB::raw("
      select vav.payment_group_id, pgc.channel_name, popci.name, 
      pgc.default_fee, pgc.starwin_default_fee,
      (
        select fee from payment_gateway_user_payment_channels pgupc
        join payment_gateway_users pgu on pgupc.pg_user_id = pgu.id
        where user_id = ? and pgupc.payment_gateway_channel_id = pgc.id
      )
      from virtual_account_vendors vav
      join payment_gateway_channels pgc on vav.id = pgc.va_vendor_id
      join payment_odeo_payment_channel_informations popci on popci.id = vav.payment_group_id
      where vav.is_active = true and pgc.channel_name <> 'bca_open'
      order by pgc.channel_name asc
    "), [$userId]);
  }

}

<?php
namespace Odeo\Domains\VirtualAccount\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\VirtualAccount\Model\UserVirtualAccountDetail;

class UserVirtualAccountDetailRepository extends Repository {

  public function __construct(UserVirtualAccountDetail $userVirtualAccountDetail) {
    $this->model = $userVirtualAccountDetail;
  }

  public function getByUserVirtualAccountId($userVirtualAccountId) {
    return $this->model->select('*', 'virtual_account_vendors.id as vendor_id', 'virtual_account_vendors.name as vendor_name')
      ->join('virtual_account_vendors', 'virtual_account_vendors.code', '=', 'user_virtual_account_details.vendor')
      ->where('user_virtual_account_id', $userVirtualAccountId)->get();
  }

  public function findByNumber($virtualAccountNumber, $vendorCode = NULL) {
    $query = $this->getCloneModel()
      ->leftJoin('user_virtual_accounts', 'user_virtual_account_details.user_virtual_account_id', '=', 'user_virtual_accounts.id')
      ->leftJoin('virtual_account_vendors', function($join) use ($virtualAccountNumber) {
        $join->on('user_virtual_account_details.vendor', '=', 'virtual_account_vendors.code')
          ->whereRaw("left('$virtualAccountNumber', length(virtual_account_vendors.prefix)) = virtual_account_vendors.prefix");
      })
      ->where('virtual_account_number', $virtualAccountNumber);

    if (isset($vendorCode)) {
      $query = $query->where('vendor', $vendorCode);
    }
    return $query->first();
  }

  public function getVirtualAccountInfo($referenceIds) {
    $query = $this->model->select('service_reference_id', 'virtual_account_number', 'user_virtual_account_details.vendor as vendor_code',
      'fee', 'virtual_account_vendors.name as vendor_name', 'virtual_account_vendors.id as vendor_id')
      ->from('user_virtual_account_details')
      ->join('user_virtual_accounts', 'user_virtual_accounts.id' , '=', 'user_virtual_account_details.user_virtual_account_id')
      ->join('virtual_account_vendors', 'virtual_account_vendors.code', '=', 'user_virtual_account_details.vendor')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'virtual_account_vendors.payment_group_id');
    if(is_array($referenceIds)) return $query->whereIn('service_reference_id', $referenceIds)->get();
    else return $query->where('service_reference_id', $referenceIds)->first();
  }

  public function getVirtualAccountInfoByUserId($userIds) {
    $query = $this->model->select('user_id', 'virtual_account_number', 'user_virtual_account_details.vendor as vendor_code',
      'fee', 'virtual_account_vendors.name as vendor_name', 'virtual_account_vendors.id as vendor_id')
      ->from('user_virtual_account_details')
      ->join('user_virtual_accounts', 'user_virtual_accounts.id' , '=', 'user_virtual_account_details.user_virtual_account_id')
      ->join('virtual_account_vendors', 'virtual_account_vendors.code', '=', 'user_virtual_account_details.vendor')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'virtual_account_vendors.payment_group_id');
    if(is_array($userIds)) return $query->whereIn('user_id', $userIds)->get();
    else return $query->where('user_id', $userIds)->first();
  }

  public function getVirtualAccountInfoByAccountNumber($accountNumbers)
  {
    $query = $this->model->select('user_id', 'virtual_account_number', 'user_virtual_account_details.vendor as vendor_code',
      'fee', 'virtual_account_vendors.name as vendor_name', 'virtual_account_vendors.id as vendor_id', 'virtual_account_vendors.payment_group_id as channel_id')
      ->from('user_virtual_account_details')
      ->join('user_virtual_accounts', 'user_virtual_accounts.id', '=', 'user_virtual_account_details.user_virtual_account_id')
      ->join('virtual_account_vendors', 'virtual_account_vendors.code', '=', 'user_virtual_account_details.vendor')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'virtual_account_vendors.payment_group_id');
    if (is_array($accountNumbers)) return $query->whereIn('virtual_account_number', $accountNumbers)->get();
    else return $query->where('virtual_account_number', $accountNumbers)->first();
  }
}

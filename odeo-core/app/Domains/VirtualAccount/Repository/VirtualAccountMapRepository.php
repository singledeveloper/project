<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/07/19
 * Time: 10.19
 */

namespace Odeo\Domains\VirtualAccount\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\VirtualAccount\Model\VirtualAccountMap;

class VirtualAccountMapRepository extends Repository {

  public function __construct(VirtualAccountMap $vaMap) {
    $this->model = $vaMap;
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/7/16
 * Time: 11:26 PM
 */

namespace Odeo\Domains\Activity;

use Odeo\Domains\Core\PipelineListener;

class ActivitySelector {

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userCashes = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function get(PipelineListener $listener, $data) {

    return $listener->response(200, [
      'order' => $this->orders->getOrderActivity(),
      'store' => $this->stores->getStoreActivity(),
      'user' => $this->users->getUserActivity(),
      'user_cash' => array_map(function($value) { 
        return $this->currency->formatPrice($value); 
      }, $this->userCashes->getUserCashActivity())
    ]);
  }

}

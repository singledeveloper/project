<?php

namespace Odeo\Domains\Activity\Repository;

use Carbon\Carbon;
use Odeo\Domains\Activity\Model\Holiday;
use Odeo\Domains\Core\Repository;

class HolidayRepository extends Repository {

  public function __construct(Holiday $holiday) {
    $this->model = $holiday;
  }

  public function checkDate($date) {
    return $this->getCloneModel()->where('event_date', $date)
      ->where('is_active', true)->first();
  }

  public function getNextMonth($date, $months = 1) {
    $nextMonthDate = Carbon::parse($date)->addMonths($months)->format('Y-m-d');
    return $this->getCloneModel()->whereBetween('event_date', [$date, $nextMonthDate])
      ->where('is_active', true)->orderBy('event_date', 'asc')->get();
  }

  public function getDayCountBetween($startDate, $endDate) {
    return $this->getCloneModel()->whereBetween('event_date', [$startDate, $endDate])
      ->where('is_active', true)->count();
  }

  public function getAllActive() {
    return $this->getCloneModel()->where('is_active', true)->get()->pluck('event_date')->toArray();
  }

}
<?php

namespace Odeo\Domains\Activity\Model;

use Odeo\Domains\Core\Entity;

class DailySalesReport extends Entity {

  public $timestamps = false;

}
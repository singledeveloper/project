<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/6/17
 * Time: 2:56 PM
 */

namespace Odeo\Domains\Activity\Scheduler;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\SendgridApi;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;

class SendgridContactUpdater {

  private $users, $redis;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'handle'));
    $pipeline->execute([]);
  }

  public function handle() {

    $date = Carbon::today()->toDateString();
    if ($cursor = $this->redis->get('sendgrid_current_cursor'));
    else {
      $cursor = Carbon::create(2017, 9, 6)->toDateString();
    }

    if ($cursor == $date) return;

    $userEmailRes = $this->users->getRecentRegisteredUsers($cursor)->get();

    if ($userEmailRes->isEmpty()) return;

    $client = new Client();
    $emails = $userEmailRes->all();

    $response = $client->request('POST', SendgridApi::URL_RECIPIENTS, [
      'headers' => [
        'Authorization' => 'Bearer ' . SendgridApi::API_KEY
      ],
      'json' => $emails
    ]);

    $result = json_decode($response->getBody(), true);
    $response = $client->request('POST', SendgridApi::URL_RECIPIENT_GROUP_LISTS . '/' . SendgridApi::CUSTOMER_GROUP_LIST_ID . '/recipients', [
      'headers' => [
        'Authorization' => 'Bearer ' . SendgridApi::API_KEY
      ],
      'json' => $result['persisted_recipients']
    ]);

    if ($response->getStatusCode() == '201') $this->redis->set('sendgrid_current_cursor', $date);

  }

}
<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoiceVoidLog;

class AffiliateUserInvoiceVoidLogRepository extends Repository {

  public function __construct(AffiliateUserInvoiceVoidLog $model) {
    $this->model = $model;
  }

}

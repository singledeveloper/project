<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Helper;

use Odeo\Domains\Constant\UserInvoice;
use Carbon\Carbon;

class UserInvoiceParser {

  public function parseStatus($invoice) {
    $status = '';

    switch($invoice->status) {
      case UserInvoice::OPENED:
        if($invoice->due_date->lte(Carbon::now())) {
          $status = 'OVERDUE';
        } else {
          $status = 'UNPAID';
        }
        break;
      case UserInvoice::PARTIALLY_PAID:
        $status = 'PARTIAL';
        break;
      case UserInvoice::VERIFIED:
        $status = 'VERIFIED';
        break;
      case UserInvoice::COMPLETED:
      case UserInvoice::COMPLETED_WITH_EXTERNAL_PAYMENT:
        $status = 'PAID';
        break;
      case UserInvoice::MERGED:
        $status = 'MERGED';
        break;
      case UserInvoice::CANCELLED:
      case UserInvoice::VOID:
        $status = 'VOID';
        break;
    }

    return $status;
  }
}

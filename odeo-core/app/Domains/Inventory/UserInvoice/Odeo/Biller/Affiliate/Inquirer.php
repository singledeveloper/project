<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Inventory\UserInvoice\Repository\UserInvoiceBillerRepository;

class Inquirer {

  private $userInvoiceRepo, $userInvoiceBillerRepo;

  public function __construct() {
    $this->userInvoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
    $this->userInvoiceBillerRepo = app()->make(UserInvoiceBillerRepository::class);
  }

  public function getByPhone(PipelineListener $listener, $data) {
    $biller = $this->userInvoiceBillerRepo->findById($data['biller_id']);
    if (!$biller) {
      return $listener->response(200, [
        'invoices' => [],
        'next_row' => 0,
      ]);
    }

    $invoices = $this->userInvoiceRepo->getUnpaidByPhoneNumber([$data['biller_id']], $data['phone_number']);
    $billerUserId = $data['billed_user_id'] ?? '';
    $period = $data['period'] ?? '';
    $response = [];

    foreach ($invoices as $item) {
      $response[] = [
        'invoice_number' => $item->invoice_number,
        'period' => $item->period,
        'invoice_name' => $item->name,
        'billed_user_id' => $item->billed_user_id,
        'billed_user_name' => $item->billed_user_name,
        'descriptions' => [$item->name],
        'total' => +$item->total,
        'biller_id' => $data['biller_id'],
        'biller_name' => $biller->biller_name,
      ];
    }

    $response = array_filter($response, function ($item) use ($billerUserId, $period) {
      if (!!$period && $item['period'] != $period) {
        return false;
      }

      if (!!$billerUserId && stripos($item['billed_user_id'], $billerUserId) !== false) {
        return false;
      }

      return true;
    });

    return $listener->response(200, [
      'invoices' => $response,
      'next_row' => 0,
    ]);
  }
}

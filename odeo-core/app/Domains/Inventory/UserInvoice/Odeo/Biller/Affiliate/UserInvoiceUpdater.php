<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\UserInvoice;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class UserInvoiceUpdater {

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->invoiceDataGenerator = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Helper\UserInvoiceDataGenerator::class);
  }

  public function create(PipelineListener $listener, $data) {

    try {
      $invoice = $this->invoiceDataGenerator->generate($data, $data);
      $invoice['items'] = json_decode($invoice['items']);

      $this->userInvoices->save($invoice);

      return $listener->response(200, [
        'invoice_name' => $invoice['name'],
        'invoice_number' => $invoice['invoice_number'],
        'amount' => $invoice['total'],
        'reference_id' => $invoice['invoice_number'],
        'user_id' => $invoice['billed_user_id'],
        'user_name' => $invoice['billed_user_name']
      ]);
    } catch (\Illuminate\Database\QueryException $e) {
      if ($e->getCode() == 23505) {
        return $listener->response(400, 'Tagihan ' . $e->getBindings()[7] . ' sudah ada.');
      } else {
        return $listener->response(400, 'Data tidak sesuai.');
      }
    }

    return $listener->response(400);
  }

  public function void(PipelineListener $listener, $data) {
    $this->userInvoices->normalizeFilters($data);

    $invoice = $this->userInvoices->getDetail(true);

    if($invoice && $invoice->status < UserInvoice::COMPLETED) {
      $invoice->status = UserInvoice::VOID;
      $this->userInvoices->save($invoice);

      return $listener->response(200);
    }

    return $listener->response(400, 'Tagihan tidak ditemukan atau sudah dibayar');
  }

  public function batchCreate(PipelineListener $listener, $data) {
    $file = $data['invoice'];

    try {
      $reader = ReaderFactory::create($file->extension());

      $reader->open($file->path());

      $columnMap = [
        'name',
        'due_date',
        'period',
        'billed_user_id',
        'billed_user_name',
        'billed_user_email',
        'billed_user_telephone',
      ];
      $dataLength = count($columnMap);

      foreach ($reader->getSheetIterator() as $sheet) {
        $rowIdx = 1;
        $itemAttributeMap = array();
        $invoices = array();

        foreach ($sheet->getRowIterator() as $row) {
          if ($rowIdx == 1) {
            $itemAttributes = array_slice($row, $dataLength);
            $itemAttributeMap = array_map(function($attributes) {
              return explode(';', $attributes);
            }, $itemAttributes);
          } else {
            $invoiceData = array_slice($row, 0, $dataLength);
            $invoiceItems = array_slice($row, $dataLength);

            $invoice = array_combine($columnMap, $invoiceData);
            
            foreach ($invoiceItems as $key => $item) {
              $invoice['items'][] = array_combine($itemAttributeMap[$key], explode(';', $item));
            }

            $invoices[] = $invoice;
          }
          $rowIdx++;
        }
      }

      $newInvoices = array();

      foreach ($invoices as $invoice) {
        $newInvoices[] = $this->invoiceDataGenerator->generate($invoice, $data);
      }

      count($newInvoices) && $this->userInvoices->saveBulk($newInvoices);

      $reader->close();
    } catch (\Illuminate\Database\QueryException $e) {
      if ($e->getCode() == 23505) {
        return $listener->response(400, 'Tagihan ' . $e->getBindings()[9] . ' sudah ada.');
      } else {
        return $listener->response(400, 'Format file tidak sesuai.');
      }
    } catch (\Exception $e) {
      return $listener->response(400, 'Format file tidak sesuai.');
    }

    return $listener->response(200, [
      'invoices' => $newInvoices
    ]);
  }
}

<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\UserInvoice\Model\UserInvoiceBiller;
use Odeo\Domains\Subscription\Model\Store;

class AffiliateUserInvoice extends Entity {

  protected $casts = [
    'items' => 'array',
  ];

  protected $dates = ['due_date'];

  protected $guarded = ['biller_fee', 'payment_gateway_cost'];

  public function invoicePayments() {
    return $this->hasMany(AffiliateUserInvoicePayment::class, 'invoice_id', 'id');
  }

  public function biller() {
    return $this->belongsTo(UserInvoiceBiller::class, 'biller_id', 'id');
  }

  public function store() {
    return $this->belongsTo(Store::class, 'store_id');
  }
}

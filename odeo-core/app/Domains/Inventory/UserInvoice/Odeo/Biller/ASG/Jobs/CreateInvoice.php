<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs;

use DB;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\InvoiceUpdater;
use Odeo\Jobs\Job;

class CreateInvoice extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $pipeline = new Pipeline();
    $pipeline->add(new Task(InvoiceUpdater::class, 'create'));
    $pipeline->enableTransaction();

    $pipeline->execute($this->data);
  }
}

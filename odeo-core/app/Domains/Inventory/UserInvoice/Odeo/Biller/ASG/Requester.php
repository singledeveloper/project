<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG;

use Carbon\Carbon;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\PaxPayment;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs\NotifyInvoicePayment;
use Odeo\Domains\Order\Helper\OrderCharger;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Payment\Helper\FeeGenerator;
use Odeo\Domains\Payment\Pax\Repository\PaxPaymentRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class Requester {

  private $feeGenerator;

  public function completeOrder(PipelineListener $listener, $data) {
    $userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $userInvoicePayments = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoicePaymentRepository::class);
    $orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $tempInquiry = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\InvoiceTempInquiry::class);
    $paxPaymentRepo = app()->make(PaxPaymentRepository::class);
    $orderCharger = app()->make(OrderCharger::class);
    $this->feeGenerator = app()->make(FeeGenerator::class);

    $invoiceNumbers = json_decode($data['invoice_number']);
    if (!$invoiceNumbers) {
      return $listener->response(400);
    }

    if (!is_array($invoiceNumbers)) {
      $invoiceNumbers = [$invoiceNumbers];
    }

    $invoices = $userInvoices->getByInvoiceNumber($invoiceNumbers);
    if (empty($invoices)) {
      return $listener->response(400);
    }

    $orderDetail = $orderDetails->findById($data['order_detail_id']);

    $totalPaid = $orderDetail->sale_price;
    $totalSettled = 0;

    $updatedInvoices = [];
    $invoiceNumbers = [];

    $paxPayment = $paxPaymentRepo->findSuccessByOrderId($orderDetail->order_id);

    $paidAt = $orderDetail->order->paid_at ? Carbon::parse($orderDetail->order->paid_at) : Carbon::now();

    foreach ($invoices as $invoice) {
      if($invoice->status >= UserInvoice::COMPLETED) throw new \Exception('Invalid invoice payment');

      if ($totalPaid - $invoice->total > 0) {
        $paymentAmount = $invoice->total;
      } else {
        $paymentAmount = $totalPaid;
      }

      $userInvoicePayment = $userInvoicePayments->getNew();
      $userInvoicePayment->invoice_id = $invoice->id;
      $userInvoicePayment->order_id = $orderDetail->order_id;
      $userInvoicePayment->amount = $paymentAmount;
      $userInvoicePayment->paid_at = $paidAt;

      $userInvoicePayments->save($userInvoicePayment);
      $invoice->biller_fee = $this->getBillerFee($invoice, $paxPayment);
      $invoice->payment_gateway_cost = $this->getPaymentGatewayCost($invoice, $paxPayment);
      $mdr = $this->getMDR($invoice, $paxPayment);

      $orderCharger->charge($orderDetail->order_id, OrderCharge::BILLER_FEE, $invoice->biller_fee, OrderCharge::GROUP_TYPE_COMPANY_PROFIT);
      if ($mdr != $invoice->mdr) {
        $invoice->mdr = $mdr;
        $orderCharger->charge($orderDetail->order_id, OrderCharge::PAYMENT_SERVICE_COST, $invoice->mdr, OrderCharge::GROUP_TYPE_COMPANY_PROFIT);
      }

      if ($paymentAmount == $invoice->total) {
        $updatedInvoices[] = [
          'id' => $invoice->id,
          'status' => UserInvoice::COMPLETED,
          'total_paid' => $paymentAmount,
          'paid_at' => $paidAt,
          'biller_fee' => $invoice->biller_fee,
          'mdr' => $invoice->mdr,
          'payment_gateway_cost' => $invoice->payment_gateway_cost,
        ];
        $invoiceNumbers[] = $invoice->name;
        $totalSettled += $paymentAmount;
      } else {
        clog('asg_api', $invoice->id . ': ' . $paymentAmount);
      }

      $totalPaid -= $paymentAmount;
    }

    count($updatedInvoices) && $userInvoices->updateBulk($updatedInvoices);

    if ($totalSettled) {
//      $userId = $orderDetail->order->user_id;
//
//      $settlementAt = Carbon::tomorrow()->addHours(18)->toDateTimeString();
//      if ($paxPayment) {
//        $settlementAt = Carbon::today()->addDays(3)->addHours(18)->toDateTimeString();
//      }

//      $cashInserter->add([
//        'user_id' => $userId,
//        'trx_type' => TransactionType::INVOICE_PAYMENT,
//        'cash_type' => CashType::OCASH,
//        'amount' => $totalSettled,
//        'data' => json_encode([
//          'order_id' => $orderDetail->order_id,
//          'invoice_number' => $invoiceNumbers,
//        ]),
//      ]);

//      $cashInserter->add([
//        'user_id' => $userId,
//        'trx_type' => TransactionType::TRANSACTION_FEE,
//        'cash_type' => CashType::OCASH,
//        'amount' => -$invoice->biller_fee,
//        'data' => json_encode([
//          'order_id' => $orderDetail->order_id,
//          'invoice_number' => $invoiceNumbers,
//        ]),
//      ]);

//      if ($invoice->mdr > 0) {
//        $cashInserter->add([
//          'user_id' => $userId,
//          'trx_type' => TransactionType::MDR,
//          'cash_type' => CashType::OCASH,
//          'amount' => -$invoice->mdr,
//          'data' => json_encode([
//            'order_id' => $orderDetail->order_id,
//            'invoice_number' => $invoiceNumbers,
//          ]),
//        ]);
//      }

//      $cashInserter->run();

      $listener->pushQueue(new NotifyInvoicePayment([
        'order_id' => $orderDetail->order_id,
        'payment_date' => $paidAt->toIso8601String(),
        'payment_name' => $data['payment_name'],
        'project_id' => BillerASG::getProjectIdByBiller($data['biller_id']),
        'billed_user_id' => $invoices[0]->billed_user_id,
        'reference_id' => $invoices[0]->invoice_number,
        'payment_amount' => $totalSettled,
        'invoice_numbers' => $invoiceNumbers
      ]));
    }

    $tempInquiry->deleteInvoiceNumber($data['biller_id'], $invoices[0]->billed_user_id);

    return $listener->response(200);
  }

  public function updateCashTransaction(PipelineListener $listener, $data) {
    $invoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
    $orderRepo = app()->make(OrderRepository::class);
    $cashInserter = new CashInserter();
    $orderId = $data['order_id'];

    $order = $orderRepo->findById($orderId);
    $invoice = $invoiceRepo->findByOrderId($orderId);

    if ($order->status != OrderStatus::COMPLETED) {
      return $listener->response(400, "order status is not completed");
    }

    if ($invoice->status != UserInvoice::COMPLETED) {
      return $listener->response(400, "invoice status is not completed");
    }

    $invoiceNumbers = [$invoice->name];
    $paidAt = Carbon::parse($invoice->paid_at)->startOfDay();

    $settlementAt = $paidAt->addDay(1)->addHours(18);
    if (($order->payment->info_id == Payment::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_BNI_SWITCHING) ||
        ($order->payment->info_id == Payment::OPC_GROUP_DEBIT_CREDIT_CARD_PRESENT_PAX_ARTAJASA_SWITCHING)) {
      $settlementAt = $paidAt->addDays(3)->addHours(18);
    }

    if ($settlementAt->lessThan(Carbon::now())) {
      $settlementAt = null;
    } else {
      $settlementAt = $settlementAt->toDateTimeString();
    }

    $cashInserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::INVOICE_PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => $invoice->total_paid,
      'data' => json_encode([
        'order_id' => $orderId,
        'invoice_number' => $invoiceNumbers,
        'settlement_at' => $settlementAt
      ]),
      'settlement_at' => $settlementAt
    ]);

    $cashInserter->add([
      'user_id' => $order->user_id,
      'trx_type' => TransactionType::TRANSACTION_FEE,
      'cash_type' => CashType::OCASH,
      'amount' => -$invoice->biller_fee,
      'data' => json_encode([
        'order_id' => $orderId,
        'invoice_number' => $invoiceNumbers,
        'settlement_at' => $settlementAt,
      ]),
      'settlement_at' => $settlementAt
    ]);

    if ($invoice->mdr > 0) {
      $cashInserter->add([
        'user_id' => $order->user_id,
        'trx_type' => TransactionType::MDR,
        'cash_type' => CashType::OCASH,
        'amount' => -$invoice->mdr,
        'data' => json_encode([
          'order_id' => $orderId,
          'invoice_number' => $invoiceNumbers,
          'settlement_at' => $settlementAt
        ]),
        'settlement_at' => $settlementAt
      ]);
    }

    $cashInserter->run();
  }

  public function sendNotification(PipelineListener $listener, $data) {
    $invoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
    $orderRepo = app()->make(OrderRepository::class);
    $orderId = $data['order_id'];

    $order = $orderRepo->findById($orderId);
    $invoices = $invoiceRepo->listByOrderId($orderId);

    if ($order->status < OrderStatus::VERIFIED || $order->status >= OrderStatus::CANCELLED) {
      return $listener->response(200);
    }

    if (count($invoices) == 0) {
      return $listener->response(400, 'invoice not found');
    }

    $invoice = $invoices[0];

    if ($invoice->status != UserInvoice::COMPLETED) {
      return $listener->response(400, "invoice status is not completed");
    }

    $totalSettled = 0;
    $invoiceNumbers = [];
    foreach ($invoices as $inv) {
      $totalSettled += $inv->total_paid;
      if ($inv->total_paid == 0) {
        return $listener->response(400, 'invoice total paid is zero');
      }

      $invoiceNumbers [] = $inv->name;
    }

    $listener->pushQueue(new NotifyInvoicePayment([
      'order_id' => $orderId,
      'payment_date' => Carbon::parse($invoice->paid_at)->toIso8601String(),
      'payment_name' => $order->payment->information->actual_name,
      'project_id' => BillerASG::getProjectIdByBiller($invoice->biller_id),
      'billed_user_id' => $invoice->billed_user_id,
      'reference_id' => $invoice->invoice_number,
      'payment_amount' => $totalSettled,
      'invoice_numbers' => $invoiceNumbers,
    ]));

    return $listener->response(200);
  }

  public function resendNotification(PipelineListener $listener, $data) {
    $listener->pushQueue(new NotifyInvoicePayment([
      'order_id' => $data['order_id'],
      'payment_date' => $data['payment_date'],
      'payment_name' => $data['payment_name'],
      'project_id' => $data['project_id'],
      'billed_user_id' => $data['billed_user_id'],
      'reference_id' => $data['reference_id'],
      'payment_amount' => $data['payment_amount'],
      'invoice_numbers' => $data['invoice_numbers'],
    ]));

    return $listener->response(200);
  }

  private function getBillerFee($invoice, $paxPayment) {
    if (!$paxPayment) {
      return $invoice->biller_fee;
    }

    switch ($paxPayment->actual_card_type) {
      case PaxPayment::TYPE_CREDIT_ON_US:
      case PaxPayment::TYPE_CREDIT_OFF_US:
        return BillerASG::getCreditBillerFee();
      case PaxPayment::TYPE_DEBIT_ON_US:
      case PaxPayment::TYPE_DEBIT_OFF_US:
        return BillerASG::getDebitBillerFee();
    }

    return $invoice->biller_fee;
  }

  private function getMDR($invoice, $paxPayment) {
    if (!$paxPayment) {
      return 0;
    }

    switch ($paxPayment->actual_card_type) {
      case PaxPayment::TYPE_CREDIT_ON_US:
      case PaxPayment::TYPE_CREDIT_OFF_US:
        return round($invoice->total * BillerASG::getCreditMdrPercentage() / 100);
      case PaxPayment::TYPE_DEBIT_ON_US:
      case PaxPayment::TYPE_DEBIT_OFF_US:
        return round($invoice->total * BillerASG::getDebitMdrPercentage() / 100);
    }

    return 0;
  }

  private function getPaymentGatewayCost($invoice, $paxPayment) {
    if (!$paxPayment) {
      return $invoice->payment_gateway_cost;
    }

    return $paxPayment->mdr + $paxPayment->payment_gateway_cost;
  }
}

<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Inquirer;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceUpdater;
use Odeo\Domains\Payment\PaymentVaDirectSelector;

class InventoryValidator {

  public function __construct() {
    $this->biller = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->api = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager::class);
    $this->tempInquiry = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\InvoiceTempInquiry::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    if ($invoiceNumber = $this->tempInquiry->getInvoiceNumber($data['biller_id'], $data['user_id'])) {
      $listener->addNext(new Task(Inquirer::class, 'getByNumber', [
        'biller_invoice_number' => $invoiceNumber
      ]));
    } else {
      $listener->addNext(new Task(Inquirer::class, 'getUnpaid'));
    }

    $listener->addNext(new Task(__CLASS__, 'validateInvoice'));

    return $listener->response(200);
  }

  public function validateInvoice(PipelineListener $listener, $data) {
    $currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    
    if (isset($data['total_due']) && $data['total_due'] > 0) {
      
      $fee = $data['fee'] ?? 0;
      $cost = $data['cost'] ?? 0;

      $invoiceNumbers = [];
      $invoiceNames = [];

      foreach ($data['invoices'] as $invoice) {
        $userInvoice = $this->userInvoices->findByAttributes('invoice_number', $invoice['invoice_number']);
        $userInvoice->biller_fee = $fee;
        $userInvoice->payment_gateway_cost = $cost;

        $this->userInvoices->save($userInvoice);

        $invoiceNumbers[] = $invoice['invoice_number'];
        $invoiceNames[] = $invoice['name'];
      }

      $this->userInvoices->save($userInvoice);

      return $listener->response(200, [
        'base_price' => $data['total_due'] - $fee,
        'merchant_price' => $data['total_due'],
        'sale_price' => $data['total_due'],
        'margin' => 0,
        'mentor_margin' => 0,
        'leader_margin' => 0,
        'referral_cashback' => 0,
        'name' => $userInvoice->billed_user_id,
        'first_name' => $userInvoice->billed_user_name,
        'invoice_number' => json_encode($invoiceNumbers),
        'item_name' => implode(",", $invoiceNames)
      ]);
    }
    
    return $listener->response(400, 'Tagihan tidak ditemukan atau sudah dibayarkan');
  }
}

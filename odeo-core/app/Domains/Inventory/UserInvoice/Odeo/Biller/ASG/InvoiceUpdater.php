<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceUpdater;
use Odeo\Domains\Payment\PaymentVaDirectSelector;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs\CreateInvoice;

class InvoiceUpdater {

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceUserRepository::class);
    $this->api = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager::class);
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository::class);
  }

  public function create(PipelineListener $listener, $data) {

    try {
      $billedUserId = $data['houseName'];
      $user = $this->users->findByAttributes($billedUserId);
      $invoiceDetail = $this->api->getByInvoiceId($data['invoiceNumber']);

      if (!$user) {
        return $listener->response(400, 'Invalid user id.');
      }

      $billerId = $user->biller_id;
      
      $user->name = $invoiceDetail['name'];
      $user->telephone = $invoiceDetail['phone'];
      $user->address = $data['address'] ?? '';

      $user->isDirty() && $this->users->save($user);

      if (strtolower($invoiceDetail['status']) == BillerASG::UNPAID) {
        $items = []; 
        foreach ($invoiceDetail['invoices'] as $item) {
          $items[] = [
            'name' => $item['name'],
            'price' => $item['remaining'],
            'image' => $item['picture']
          ];
        }
        $dueDate = \Carbon\Carbon::parse($invoiceDetail['dueDate']);

        if (!$this->userInvoices->findByAttributes(['billed_user_id' => $billedUserId, 'period' => $dueDate->format('Y-m')])) {
          $listener->addNext(new Task(UserInvoiceUpdater::class, 'create', [
            'user_id' => BillerASG::getOdeoUserId(),
            'billed_user_id' => $billedUserId,
            'billed_user_name' => $invoiceDetail['name'],
            'billed_user_telephone' => $invoiceDetail['phone'],
            'period' => $dueDate->format('Y-m'),
            'due_date' => $dueDate,
            'name' => $data['invoiceNumber'],
            'items' => $items,
            'biller' => [
              'id' => $billerId
            ],
            'merge_previous' => true,
          ]));
          $listener->addNext(new Task(PaymentVaDirectSelector::class, 'get', [
            'service_detail_id' => ServiceDetail::USER_INVOICE_ODEO,
            'biller' => [
              'id' => $billerId
            ],
            'virtual_account_vendors' => VirtualAccount::VENDOR_DOKU_BCA
          ]));
        } else {
          return $listener->response(400, 'Existing invoice for user id in selected period');
        }
      }
      
    } catch (\Exception $e) {
      clog('asg_invoice', $e->getMessage());
      clog('asg_invoice', serialize($data));
      return $listener->response(400, 'Invalid invoice data.');
    }

    return $listener->response(200);
  }

  public function getUnpaid(PipelineListener $listener, $data) {
    try {
      $billedUserId = $data['houseName'];
      $invoiceData = $this->api->getUnitInvoiceByStatus($billedUserId, BillerASG::UNPAID);
      
      $unpaidInvoices = array_reverse($invoiceData['data']);

      foreach ($unpaidInvoices as $unpaidInvoice) {
        $listener->pushQueue(new CreateInvoice(array_merge($unpaidInvoice, [
          'houseName' => $billedUserId, 
          'invoiceNumber' => $unpaidInvoice['id']
        ])));
      }
    } catch (\Exception $e) {
      clog('asg_invoice', $e->getMessage());
      clog('asg_invoice', serialize($data));
      return $listener->response(400, 'Invalid invoice data.');
    }

    return $listener->response(200);
  }
}

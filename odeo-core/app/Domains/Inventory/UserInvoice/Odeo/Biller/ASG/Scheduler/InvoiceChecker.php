<?php

namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Scheduler;

use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs\CreateInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Jobs\GetUnpaidInvoice;

class InvoiceChecker {

  private $apiManager;

  public function __construct() {
    $this->apiManager = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\Helper\ApiManager::class);
  }

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'check'));
    // $pipeline->add(new Task(__CLASS__, 'insertPreviousInvoice'));
    $pipeline->execute([]);
  }

  public function check(PipelineListener $listener, $data) {
    $invoices = $this->apiManager->getCurrentInvoicesByProject(BillerASG::GLC_PROJECT_ID);
    
    foreach ($invoices as $invoice) {
      if ($invoice['status'] == 'Paid') continue;
      $listener->pushQueue(new CreateInvoice($invoice));
    }
    return $listener->response(200);
  }

  public function insertPreviousInvoice(PipelineListener $listener, $data) {
    $invoices = $this->api->getCurrentInvoicesByProject(BillerASG::GLC_PROJECT_ID);
    
    foreach ($invoices as $invoice) {
      $listener->pushQueue(new GetUnpaidInvoice($invoice));
    }
    return $listener->response(200);
  }
}
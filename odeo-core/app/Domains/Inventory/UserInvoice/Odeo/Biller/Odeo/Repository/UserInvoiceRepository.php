<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Model\UserInvoice;
use Odeo\Domains\Core\Repository;

class UserInvoiceRepository extends Repository {

  public function __construct(UserInvoice $userInvoice) {
    $this->model = $userInvoice;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    $query = $query->select('user_invoices.*', 'users.name as billed_user', 'invoice_categories.name as category_name')
      ->leftJoin('users', 'users.id', '=', 'user_invoices.billed_user_id')
      ->join('invoice_categories', 'invoice_categories.id', '=', 'user_invoices.category_id');

    if(!isAdmin()) {
      if (isset($filters["store_id"])) {
        $query = $query->where('store_id', $filters['store_id']);
      } else if(isset($filters["billed_user_id"])) {
        $query = $query->where('billed_user_id', $filters['billed_user_id']);
        $query = $query->where('user_invoices.status', '<', \Odeo\Domains\Constant\UserInvoice::COMPLETED);
      }
    }

    return $this->getResult($query->orderBy('user_invoices.id', 'desc'));
  }

  public function getDetail($lockForUpdate) {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if($lockForUpdate) $query = $query->lockForUpdate();

    return $query->where('invoice_number', $filters['invoice_number'])->first();
  }

  public function getStat() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    $query = $query->select(\DB::raw("SUM(CASE WHEN status = '10001' AND due_date >= now() THEN total ELSE 0 END) as total_unpaid,
      SUM(CASE WHEN status = '10001' AND due_date < now() THEN total ELSE 0 END) as total_overdue,
      SUM(CASE WHEN status = '50000' THEN total ELSE 0 END) as total_paid"));

    if(!isAdmin()) {
      $query = $query->where('store_id', $filters['store_id']);
    }

    return $query->first();
  }

}

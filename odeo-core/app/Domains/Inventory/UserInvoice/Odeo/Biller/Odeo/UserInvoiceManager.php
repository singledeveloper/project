<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Odeo\Domains\Core\PipelineListener;

class UserInvoiceManager {

  public function validateInventory(PipelineListener $listener, $data) {
    return app()->make(InventoryValidator::class)->validateInventory($listener, $data);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    return app()->make(Requester::class)->completeOrder($listener, $data);
  }

  public function terminalInquiry(PipelineListener $listener, $data) {
    throw new \Exception('Biller does not support terminal inquiry');
  }
}

<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;

class UserInvoiceSelector implements SelectorListener {

  public function __construct() {
    $this->userInvoices = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Repository\UserInvoiceRepository::class);
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->invoiceParser = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Helper\UserInvoiceParser::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function _extends($items, Repository $repository) {
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);

    if ($invoiceNumbers = $repository->beginExtend($items, 'invoice_number')) {
      if ($repository->hasExpand('virtual_accounts')) {
        $invoices = $this->virtualAccounts->getVirtualAccountInfo($invoiceNumbers);
        $results = [];
        foreach($invoices as $invoice) {
          $results[$invoice->service_reference_id][] = $invoice;
        }
        $repository->addExtend('virtual_accounts', $results);
      }
    }

    return $repository->finalizeExtend($items);
  }

  public function _transforms($item, Repository $repository) {
    $data = [];

    $data['invoice_number'] = $item['invoice_number'];
    $data['store_id'] = $item['store_id'];
    $data['name'] = $item['name'];
    $data['category'] = $item['category_name'];
    $data['category_id'] = $item['category_id'];
    $data['notes'] = $item['notes'];
    $data['items'] = $item['items'];
    $data['billed_user'] = $item['billed_user'] ?? $item['billed_user_telephone'];
    $data['billed_user_name'] = $item['billed_user_name'];
    $data['billed_user_telephone'] = $item['billed_user_telephone'];
    $data['total_paid'] = $this->currency->formatPrice($item['total_paid']);
    $data['total_unpaid'] = $this->currency->formatPrice($item['total'] - $item['total_paid']);
    $data['total'] = $this->currency->formatPrice($item['total']);
    $data['status'] = $this->invoiceParser->parseStatus($item);
    $data['created_at'] = Carbon::parse($item['created_at'])->toDateString();
    $data['due_date'] = Carbon::parse($item['due_date'])->toDateString();
    $data['biller_id'] = UserInvoice::BILLER_ODEO;

    if($repository->hasExpand('config')) {
      $data['enable_partial_payment'] = $item['enable_partial_payment'];
      $data['minimum_payment'] = $item['minimum_payment'];
    }

    if($repository->hasExpand('payment')) {
      $data['total_paid'] = $this->currency->getAmountOnly($item->total_paid);
      $data['paid_at'] = Carbon::parse($item->paid_at)->toISO8601String();
      $data['payments'] = [];

      foreach($item->invoicePayments as $invoicePayment) {
        $temp = [];
        $temp['paid_at'] = Carbon::parse($invoicePayment->paid_at)->toDateTimeString();
        $temp['amount'] = $this->currency->getAmountOnly($invoicePayment->amount);
        $temp['payment_method'] = $invoicePayment->payment->information->name;
        $data['payments'][] = $temp;
      }
    }
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->userInvoices->normalizeFilters($data);

    $invoices = [];

    foreach ($this->userInvoices->get() as $invoice) {
      $invoices[] = $this->_transforms($invoice, $this->userInvoices);
    }

    if (count($invoices) > 0) {
      return $listener->response(200, array_merge(
        ['invoices' => $this->_extends($invoices, $this->userInvoices)],
          $this->userInvoices->getPagination()
      ));
    }
    return $listener->response(204, [], false, true);
  }

  public function getDetail(PipelineListener $listener, $data) {
    $this->userInvoices->normalizeFilters($data);
    $invoice = $this->userInvoices->getDetail(true);

    if($invoice) {
      $invoice = $this->_transforms($invoice, $this->userInvoices);
      return $listener->response(200, $this->_extends($invoice, $this->userInvoices));
    } else {
      return $listener->response(400, "Tagihan tidak ditemukan.");
    }
  }
}

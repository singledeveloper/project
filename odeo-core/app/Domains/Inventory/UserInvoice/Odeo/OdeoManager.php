<?php
namespace Odeo\Domains\Inventory\UserInvoice\Odeo;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Inventory\UserInvoice\Contract\UserInvoiceContract;

class OdeoManager implements UserInvoiceContract {

  private $instance;

  public function setBiller($biller) {
    if ($biller == UserInvoice::BILLER_ODEO) {
      $this->instance = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\UserInvoiceManager::class);
    } else if (in_array($biller, UserInvoice::BILLER_GROUP_ASG)) {
      $this->instance = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\ASG\UserInvoiceManager::class);
    } else {
      $this->instance = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\UserInvoiceManager::class);
    }
  }

  public function checkout(PipelineListener $listener, $data) {
    $orderDetails = app()->make(\Odeo\Domains\Inventory\UserInvoice\Repository\OrderDetailUserInvoiceRepository::class);

    $orderDetail = $orderDetails->getNew();
    $orderDetail->invoice_number = $data['item_id'] ?? $data['invoice_number'];
    $orderDetail->order_detail_id = $data['order_detail']['id'];
    $orderDetail->biller_id = $data['biller_id'];

    $orderDetails->save($orderDetail);

    return $listener->response(201);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    $this->instance->validateInventory($listener, $data);
  }

  public function completeOrder(PipelineListener $listener, $data) {
    $this->instance->completeOrder($listener, $data);
  }

  public function initializeInvoiceUsers(PipelineListener $listener, $data) {
    $this->instance->initializeInvoiceUsers($listener, $data);
  }

  public function inquiry(PipelineListener $listener, $data) {
    $this->instance->inquiry($listener, $data);
  }

  public function pay(PipelineListener $listener, $data) {
    $this->instance->pay($listener, $data);
  }

  public function cancelPay(PipelineListener $listener, $data) {
    $this->instance->cancelPay($listener, $data);
  }

  public function terminalInquiry(PipelineListener $listener, $data) {
    $this->instance->terminalInquiry($listener, $data);
  }

  public function refund(PipelineListener $listener, $data) {
    $this->instance->refund($listener, $data);
  }

  public function resendNotification(PipelineListener $listener, $data) {
    $notifyLogRepo = app()->make(\Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceNotifyLogRepository::class);

    $notifyLog = $notifyLogRepo->findByAttributes('invoice_number', $data['invoice_number']);

    $data = array_merge($data, json_decode($notifyLog->request, true));
    
    $this->instance->resendNotification($listener, $data);
  }
}

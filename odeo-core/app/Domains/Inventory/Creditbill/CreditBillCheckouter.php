<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/22/16
 * Time: 12:50 AM
 */

namespace Odeo\Domains\Inventory\Creditbill;


use Odeo\Domains\Core\PipelineListener;

class CreditBillCheckouter {

  public function checkout(PipelineListener $listener, $data) {

    $orderDetails = app()->make(\Odeo\Domains\Inventory\Creditbill\Repository\OrderDetailCreditBillRepository::class);

    $orderDetail = $orderDetails->getNew();
    $orderDetail->credit_bill_id = $data['credit_bill']['id'];
    $orderDetail->order_detail_id = $data['order_detail']['id'];
    $orderDetail->biller = $data['biller'];

    $orderDetails->save($orderDetail);

    return $listener->response(201);
  }

}
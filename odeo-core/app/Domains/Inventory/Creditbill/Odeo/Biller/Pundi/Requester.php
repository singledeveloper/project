<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/21/16
 * Time: 9:10 PM
 */

namespace Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi;


use Odeo\Domains\Constant\CreditBill;
use Odeo\Domains\Constant\BillerPundi;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\PipelineListener;
use Carbon\Carbon;
use Odeo\Domains\Order\Repository\OrderChargeRepository;

class Requester {

  public function request(PipelineListener $listener, $data) {

    $pundiCreditBills = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Repository\PundiCreditBillRepository::class);

    $bill = $pundiCreditBills->getNew();

    $bill->user_id = $data['auth']['user_id'];
    $bill->phone_number = $data['phone_number'];
    $bill->amount = $data['amount']['amount'];
    $bill->status = CreditBill::PENDING;

    if ($pundiCreditBills->save($bill)) {
      return $listener->response(200, [
        'credit_bill' => [
          'id' => $bill->id,
          'amount' => $bill->amount
        ]
      ]);
    }
    return $listener->response(400);
  }


  public function completeOrder(PipelineListener $listener, $data) {

    $pundiCreditBills = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Repository\PundiCreditBillRepository::class);
    $orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $bill = $pundiCreditBills->findById($data["credit_bill_id"]);

    $bill->status = CreditBill::COMPLETED;

    $pundiCreditBills->save($bill);

    $orderDetail = $orderDetails->findById($data['order_detail_id']);
    $settlementAt = Carbon::now()->addDays(BillerPundi::SETTLEMENT_DAY)->toDateTimeString();
    $mdr = 1500;
    $cashInserter->add([
      'user_id' => BillerPundi::ODEO_USER_ID,
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::OCASH,
      'amount' => ($orderDetail->base_price + $orderDetail->margin - $mdr),
      'data' => json_encode([
        'order_id' => $orderDetail->order_id,
        'recent_desc' => $orderDetail->order->sellerStore->name,
        'settlement_at' => $settlementAt
      ]),
      'settlement_at' => $settlementAt
    ]);

    $cashInserter->add([
      'user_id' => NULL,
      'trx_type' => TransactionType::MDR,
      'cash_type' => CashType::OCASH,
      'amount' => $mdr,
      'data' => json_encode([
        'order_id' => $orderDetail->order_id,
        'recent_desc' => $orderDetail->order->sellerStore->name,
      ]),
    ]);

    $cashInserter->run();

    $orderCharges = app()->make(OrderChargeRepository::class);

    $charge = $orderCharges->getNew();

    $charge->type = OrderCharge::PUNDI_FEE;
    $charge->group_type = OrderCharge::GROUP_TYPE_COMPANY_PROFIT;
    $charge->amount = $mdr;
    $charge->order_id = $orderDetail->order_id;

    $orderCharges->save($charge);

    $orderStores = app()->make(\Odeo\Domains\Order\Repository\OrderStoreRepository::class);
    $orderStore = $orderStores->getNew();
    $orderStore->order_id = $orderDetail->order_id;
    $orderStore->store_id = BillerPundi::ODEO_STORE_ID;
    $orderStore->role = 'hustler';

    $orderStores->save($orderStore);

    return $listener->response(200, ['settlement_at' => $settlementAt]);
  }


  public function cancel(PipelineListener $listener, $data) {

    $pundiCreditBills = app()->make(\Odeo\Domains\Inventory\Creditbill\Odeo\Biller\Pundi\Repository\PundiCreditBillRepository::class);

    $bill = $pundiCreditBills->findById($data['credit_bill_id']);

    $bill->status = CreditBill::CANCELLED;

    if ($pundiCreditBills->save($bill)) {
      return $listener->response(200);
    }
    return $listener->response(400);
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/21/16
 * Time: 9:28 PM
 */

namespace Odeo\Domains\Inventory\Creditbill\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Creditbill\Model\OrderDetailCreditBill;

class OrderDetailCreditBillRepository extends Repository {

  public function __construct(OrderDetailCreditBill $orderDetailCreditBill) {
    $this->model = $orderDetailCreditBill;
  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model
      ->where('order_detail_id', $order_detail_id)
      ->first();
  }

}
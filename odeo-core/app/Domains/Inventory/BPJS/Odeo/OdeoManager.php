<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:16 PM
 */

namespace Odeo\Domains\Inventory\BPJS\Odeo;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\BPJS\Contract\BPJSContract;

class OdeoManager implements BPJSContract {

  private $checkouter, $switcher, $refunder;

  public function __construct() {
    $this->checkouter = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PostpaidCheckouter::class);
    $this->switcher = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSwitcher::class);
    $this->refunder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaRefunder::class);
  }

  public function inquiry(PipelineListener $listener, $data) {
    return $this->switcher->inquiryPostpaid($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    return $this->checkouter->checkout($listener, $data);
  }

  public function purchasePostpaid(PipelineListener $listener, $data) {
    if (!isset($data['month_counts'])) $data['month_counts'] = 1; // TO DO BPJS DELETE LATER
    return $this->switcher->purchasePostpaid($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    return $this->switcher->validatePostpaidInventory($listener, $data);
  }
  
  public function refund(PipelineListener $listener, $data) {
    return $this->refunder->refund($listener, $data);
  }
}
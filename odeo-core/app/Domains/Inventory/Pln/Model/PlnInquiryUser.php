<?php

namespace Odeo\Domains\Inventory\Pln\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class PlnInquiryUser extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function inquiry() {
    return $this->belongsTo(PlnInquiry::class, 'pln_inquiry_id');
  }

}
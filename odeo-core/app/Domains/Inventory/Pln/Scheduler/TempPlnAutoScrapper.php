<?php

namespace Odeo\Domains\Inventory\Pln\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Pln\Jobs\ScrapeAutoPln;

class TempPlnAutoScrapper {

  private $pulsaSwitchers, $redis;

  public function __construct() {
    $this->pulsaSwitchers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {

    $switchFlag = $this->redis->get(Pulsa::REDIS_PLN_SWITCH_LOCK);
    if ($switchFlag && $switchFlag >= Pulsa::PLN_SWITCH_FLAG) return;

    if ($this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LOCK))
      return;

    $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LOCK, 1);

    if (!$lastId = $this->redis->hget(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LAST_ID))
      $lastId = 1;

    $x = 0;
    foreach ($this->pulsaSwitchers->getModel()->where('operator_id', Pulsa::OPERATOR_PLN)
               ->where('status', SwitcherConfig::SWITCHER_COMPLETED)
               ->where('id', '>', $lastId)
               ->take(4)->orderBy('id', 'asc')
               ->get() as $item) {

      dispatch((new ScrapeAutoPln($item->id, $item->number))->delay($x * 12));

      $x++;
      $this->redis->hset(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LAST_ID, $item->id);
    }

    $this->redis->hdel(Pulsa::REDIS_H2H_NS, Pulsa::REDIS_H2H_KEY_PLN_AUTO_SCRAPE_LOCK);
  }

}

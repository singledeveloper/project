<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 6:03 PM
 */

namespace Odeo\Domains\Inventory\Pln;

use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class PlnManager extends InventoryManager {

  private static $plnInstance;
  private $switcherOrder, $pulsaInventories, $users;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function setVendor(PipelineListener $listener, $data) {
    self::$plnInstance = app()->make(\Odeo\Domains\Inventory\Pln\Odeo\OdeoManager::class);
    $this->setServiceDetail(isset($data['service_detail_id']) ? $data['service_detail_id'] : ServiceDetail::PLN_ODEO);

    return $listener->response(200, []);

  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$plnInstance, $name], $argument);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $this->setVendor($listener, $data);
    if ($inventory = $this->pulsaInventories->getCheapestPrice($data['item_id'])) {
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      return self::$plnInstance->validateInventory($listener, $data);
    }
    return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    if (!isset($data['service_detail_id']) || (isset($data['service_detail_id']) && $data['service_detail_id'] == '0')) {
      $data['service_detail_id'] = ServiceDetail::PLN_POSTPAID_ODEO;

    } else if ($inventory = $this->pulsaInventories->getCheapestFee($data['service_detail_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);
      self::$plnInstance->validatePostpaidInventory($listener, $data);

    } else {
      return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
    }
  }

}

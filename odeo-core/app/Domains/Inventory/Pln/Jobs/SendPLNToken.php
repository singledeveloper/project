<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/20/17
 * Time: 7:24 PM
 */

namespace Odeo\Domains\Inventory\Pln\Jobs;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Payment\Jobs\EmailHelper;
use Odeo\Jobs\Job;

class SendPLNToken extends Job  {

  use EmailHelper;

  private $order, $data, $orderDetailizer;

  public function __construct($order, $data) {
    parent::__construct();
    $this->order = $order;
    $this->data = $data;
  }

  public function handle() {

    if (!($email = $this->getEmail($this->order, $this->data))) return;

    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $items = $this->orderDetailizer->detailizeItem($this->order);

    $item = $items[0];

    if ($item['service_id'] != \Odeo\Domains\Constant\Service::PLN) return;
    
    Mail::send('emails.pln_token', [
      'data' => [
        'order_id' => $this->order->id,
        'purchase_at' => Carbon::parse($this->order->created_at)->toDateTimeString(),
        'name' => $this->order->name,
        'item_name' => $item['name'],
        'token' => $item['item_detail']['serial_number'],
        'subscriber_id' => isset($item['item_detail']['inquiry']['subscriber_id']) ? $item['item_detail']['inquiry']['subscriber_id'] : '-',
        'subscriber_name' => isset($item['item_detail']['inquiry']['name']) ? $item['item_detail']['inquiry']['name'] : '-',
        'kwh' => $item['item_detail']['inquiry']['kwh'] ?? '-',
        'meter_number' => $item['item_detail']['number'],
      ]
    ], function ($m) use ($email) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to($email)->subject('[ODEO] PLN Token - ' . $this->order->id);

    });

  }

}

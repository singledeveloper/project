<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/20/17
 * Time: 7:24 PM
 */

namespace Odeo\Domains\Inventory\Pln\Jobs;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pln\PlnScrapper;
use Odeo\Jobs\Job;

class ScrapeAutoPln extends Job  {

  private $id, $number;

  public function __construct($id, $number) {
    parent::__construct();
    $this->id = $id;
    $this->number = $number;

  }

  public function handle() {

    $pipeline = new Pipeline();
    $pipeline->add(new Task(PlnScrapper::class, 'getPrepaidInquiry', [
      'switcher_id' => $this->id,
      'number' => $this->number,
      'is_proxy' => true
    ]));
    $pipeline->execute([]);

  }

}

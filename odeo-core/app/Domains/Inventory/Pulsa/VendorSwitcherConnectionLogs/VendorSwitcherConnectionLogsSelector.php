<?php

namespace Odeo\Domains\Inventory\Pulsa\VendorSwitcherConnectionLogs;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class VendorSwitcherConnectionLogsSelector implements SelectorListener {

  private $vendorSwitcherConnectionLogs;

  public function __construct() {
    $this->vendorSwitcherConnectionLogs = app()->make(\Odeo\Domains\Inventory\Pulsa\VendorSwitcherConnectionLogs\Repository\VendorSwitcherConnectionLogsRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $vendorSwitcherConnectionLogs=[];
    $vendorSwitcherConnectionLogs["id"] = $item->id;
    $vendorSwitcherConnectionLogs["vendor_switcher_id"] = $item->vendor_switcher_id;
    $vendorSwitcherConnectionLogs["vendor_switcher_name"] = $item->vendor_switcher_name;
    $vendorSwitcherConnectionLogs["type"] = $item->type;
    $vendorSwitcherConnectionLogs["description"] = $item->description;
    if($item->created_at) $vendorSwitcherConnectionLogs["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if($item->updated_at) $vendorSwitcherConnectionLogs["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    return $vendorSwitcherConnectionLogs;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->vendorSwitcherConnectionLogs->normalizeFilters($data);
    $this->vendorSwitcherConnectionLogs->setSimplePaginate(true);

    if (!isAdmin($data)) {
      if (!isset($data['store_id']))
        return $listener->response(400, 'Anda tidak mempunyai akses.');

      $supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
      list ($isValid, $message) = $supplyValidator->checkStore($data['store_id'], $data);
      if (!$isValid) return $listener->response(400, $message);
    }

    $vendorSwitcherConnectionLogs = [];
    foreach ($this->vendorSwitcherConnectionLogs->gets() as $item) {
      $vendorSwitcherConnectionLogs[] = $this->_transforms($item, $this->vendorSwitcherConnectionLogs);
    }

    if (sizeof($vendorSwitcherConnectionLogs) > 0) {
      return $listener->response(200, array_merge(
        ["vendor_switcher_connection_logs" => $this->_extends($vendorSwitcherConnectionLogs, $this->vendorSwitcherConnectionLogs)],
        $this->vendorSwitcherConnectionLogs->getPagination()
      ));
    }
    return $listener->response(204, ["vendor_switcher_connection_logs" => []]);
  }

}

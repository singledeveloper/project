<?php

namespace Odeo\Domains\Inventory\Pulsa\VendorSwitcherConnectionLogs\Repository;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Pulsa\VendorSwitcherConnectionLogs\Model\VendorSwitcherConnectionLogs;

class VendorSwitcherConnectionLogsRepository extends Repository {

  public function __construct(VendorSwitcherConnectionLogs $vendorSwitcherConnectionLog) {
    $this->model = $vendorSwitcherConnectionLog;
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->model
    ->selectRaw('
      vendor_switcher_connection_logs.id,
      vendor_switcher_id, 
      vendor_switchers.name as vendor_switcher_name, 
      type, 
      description,
      vendor_switcher_connection_logs.created_at,
      vendor_switcher_connection_logs.updated_at
    ')
    ->join('vendor_switchers', 'vendor_switchers.id', '=', 'vendor_switcher_connection_logs.vendor_switcher_id');

    if (isset($filters['search'])) $filters = $filters['search'];

    if (isset($filters['vendor_switcher_id'])) $query->where('vendor_switcher_id', $filters['vendor_switcher_id']);
    if (isset($filters['store_id'])) {
      $query->where('owner_store_id', $filters['store_id'])
        ->where('vendor_switcher_connection_logs.type', Supplier::LOG_UNKNOWN_REPORT);
    }

    return $this->getResult($query->orderBy('vendor_switcher_connection_logs.id', 'desc'));
  }

}

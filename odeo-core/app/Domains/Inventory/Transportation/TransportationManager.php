<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 6:01 PM
 */

namespace Odeo\Domains\Inventory\Transportation;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class TransportationManager extends InventoryManager {

  private static $transportationInstance;
  private $switcherOrder, $pulsaInventories, $userKtpValidator;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
  }

  public function setVendor(PipelineListener $listener, $data) {
    self::$transportationInstance = app()->make(\Odeo\Domains\Inventory\Transportation\Odeo\OdeoManager::class);
    $this->setServiceDetail(isset($data['service_detail_id']) ? $data['service_detail_id'] : ServiceDetail::TRANSPORTATION_ODEO);

    return $listener->response(200, []);

  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$transportationInstance, $name], $argument);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $this->setVendor($listener, $data);

    $redis = Redis::connection();
    if ($queues = $redis->hget(Supplier::REDIS_BILLER, Supplier::REDIS_BILLER_POI_QUEUE . $data['item_id']))
      $queues = explode(',', $queues);
    else $queues = [];

    if ($inventory = $this->pulsaInventories->getCheapestPrice($data['item_id'], '', $queues)) {
      $pulsa = $inventory->pulsa;
      if ($pulsa->category == PulsaCode::GRAB && !$this->userKtpValidator->isVerified($data['auth']['user_id'])) {
        return $listener->response(400, trans('pulsa.cant_buy_without_kyc'));
      }
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      return self::$transportationInstance->validateInventory($listener, $data);
    }
    return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

  public function getTermAndCon(PipelineListener $listener, $data) {

    return $listener->response(200, [
      [
        '1_text' => trans('pulsa.tnc_transportation_right_number'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_transportation_right_number_list_1'),
          ],
          [
            '1_text' => trans('pulsa.tnc_transportation_right_number_list_2'),
          ],
          [
            '1_text' => trans('pulsa.tnc_transportation_right_number_list_3'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_wrong_number'),
      ],
      [
        '1_text' => trans('pulsa.tnc_transportation_1'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_transportation_1_list_1'),
          ],
          [
            '1_text' => trans('pulsa.tnc_transportation_1_list_2'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_transportation_2'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_transportation_2_list_1'),
          ],
          [
            '1_text' => trans('pulsa.tnc_transportation_2_list_2'),
          ],
          [
            '1_text' => trans('pulsa.tnc_transportation_2_list_3'),
          ],
          [
            '1_text' => trans('pulsa.tnc_transportation_2_list_4'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_suspect'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_general_suspect_max_complain'),
          ],
          [
            '1_text' => trans('pulsa.tnc_general_suspect_max_wait'),
          ],
          [
            '1_text' => trans('pulsa.tnc_general_suspect_more_than_24h'),
          ],
          [
            '1_text' => trans('pulsa.tnc_beforehand_refund_risk'),
          ]
        ]
      ],
      [
        '1_text' => trans('pulsa.tnc_general_complain_statement'),
        '2_list' => [
          [
            '1_text' => trans('pulsa.tnc_general_complain_statement_suspect'),
          ]
        ]
      ],
    ]);
  }

}

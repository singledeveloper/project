<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 7:13 PM
 */

namespace Odeo\Domains\Inventory\Hotel\Tiket;


use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\Tiket;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\CartInserter;
use Validator;


class HotelRequester {

  private $connector, $tokenManager, $orderHelper, $tiketOrders,
    $currencyHelper, $marginFormatter;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->orderHelper = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Helper\OrderHelper::class);
    $this->tiketOrders = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\OrderDetailTiketRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
  }

  public function getRoomList(PipelineListener $listener, $data) {


    list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($token['status_code'], $token['message']);
    }


    $query = [
      'token' => $token['data']['token'],
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', $data['business_uri'] . '&' . http_build_query($query));

    $detail = $this->connector->response([
      200 => function ($body) {
        $response = [
          'primary_photo' => isset($body['primaryPhotos']) ? $body['primaryPhotos'] : null,
          'primary_photo_large' => isset($body['primaryPhotos_large']) ? $body['primaryPhotos_large'] : null,
          'breadcrumbs' => isset($body['breadcrumb']) ? $body['breadcrumb'] : null,
          'rooms' => isset($body['results']['result']) ? $body['results']['result'] : null,
          'additional_infos' => isset($body['addinfos']['addinfo']) ? $body['addinfos']['addinfo'] : null,
          'photos' => isset($body['all_photo']['photo']) ? $body['all_photo']['photo'] : null,
          'facilities' => isset($body['avail_facilities']['avail_facilitiy']) ? $body['avail_facilities']['avail_facilitiy'] : null,
          'nearby_attractions' => isset($body['nearby_attractions']['nearby_attraction']) ? $body['nearby_attractions']['nearby_attraction'] : null,
          'general' => isset($body['general']) ? $body['general'] : null,
        ];

        return [
          'data' => $response,
        ];
      },
    ])[0];


    if ($detail['status'] != Tiket::STATUS_SUCCESS) {

      return $listener->response($detail['status_code'], $detail['message']);

    }


    foreach ($detail['data']['rooms'] as &$room) {

      if (isset($room['oldprice'])) {
        list($room['oldprice'], $_) = $this->marginFormatter->formatMargin($room['oldprice']);
        $room['oldprice'] = $this->currencyHelper->formatPrice($room['oldprice']);
      }

      list($room['price'], $_) = $this->marginFormatter->formatMargin($room['price']);
      $room['price'] = $this->currencyHelper->formatPrice($room['price']);
    }


    return $listener->response($detail['status_code'], $detail['data']);

  }


  public function book(PipelineListener $listener, $data) {

    list($token) = $this->tokenManager->getToken(['user_id' => $data['auth']['user_id']]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return $listener->response($token['status_code'], $token['message']);
    }

    $query = [
      'token' => $token['data']['token'],
      'output' => 'json',

    ];


    $this->connector->request('GET', 'tiket', $data['item_detail']['book_uri'] . '&' . http_build_query($query));
    $book = $this->connector->response([
      200 => function ($body) {
        return $body;
      },
    ])[0];


    if ($book['status'] != Tiket::STATUS_SUCCESS) {

      return $listener->response($book['status_code'], $book['message']);
    }

    $data['token'] = $query['token'];


    if ($orderData = $this->orderHelper->getOrder($data)) {

      if (!count($orderData['data']['data'])) return $listener->response(400);

      $cartData = json_decode($data['cart']['cart_data'], true);
      $validateCart = isset($cartData['items']) && count($cartData['items']) > 0;

      foreach ($orderData['data']['data'] as $order) {

        $skip = false;

        if ($validateCart) {
          foreach ($cartData['items'] as $item) {

            if ($item['inventory_id'] == $order['order_detail_id']) {
              $skip = true;
              break;
            }

          }
        }


        if ($skip) continue;

        $salePricePerNight = $order['detail']['price_per_night'] / $order['detail']['rooms'];

        list($salePricePerNight, $basePricePerNight, $margin) = $this->marginFormatter->formatMargin($salePricePerNight);

        $totalPrice = $salePricePerNight * $order['detail']['rooms'] * $order['detail']['nights'];
        $totalCredit = $basePricePerNight * $order['detail']['rooms'] * $order['detail']['nights'];


        $listener->addNext(new Task(CartInserter::class, 'addItem', [
          'sale_price' => $totalPrice,
          'base_price' => $totalCredit,
          'margin' => $margin,
          'name' => $order['order_name'],
          'item_detail' => [
            'order' => array_merge($order, [
              'tiket_order_id' => $orderData['data']['order_id']
            ]),
            'booking_data' => $data['item_detail'],
          ],
          'cart' => $data['cart'],
          'item_id' => $order['order_detail_id'],
          'service_id' => $data['service_id'],
          'service_detail_id' => $data['service_detail_id']
        ]));
      }
    }


    return $listener->response($book['status_code']);

  }

}
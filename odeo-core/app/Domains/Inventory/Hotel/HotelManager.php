<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 7:43 PM
 */

namespace Odeo\Domains\Inventory\Hotel;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class HotelManager extends InventoryManager {

  private static $hotelInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$hotelInstance = app()->make(\Odeo\Domains\Inventory\Hotel\Tiket\TiketManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200, []);

  }


  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$hotelInstance, $name], $argument);
  }

}
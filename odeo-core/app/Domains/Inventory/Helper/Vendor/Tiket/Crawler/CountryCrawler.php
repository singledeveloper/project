<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Crawler;

use Odeo\Domains\Constant\Tiket;


class CountryCrawler {


  private $tokenManager;

  public function __construct() {

    $this->tokenManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\TokenManager::class);
    $this->connector = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Connector::class);
    $this->countries = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository\TiketCountryRepository::class);
  }

  public function getCountries() {
    list($token) = $this->tokenManager->getToken(['user_id' => 0]);

    if ($token['status'] != Tiket::STATUS_SUCCESS) {
      return [$token];
    }

    $query = [
      'token' => $token['data']['token'],
      'output' => 'json',
    ];

    $this->connector->request('GET', 'tiket', '/general_api/listCountry', $query);
    $responses = $this->connector->response([
      200 => function ($body) {
        foreach ($body['listCountry'] as $c) {

          $country = $this->countries->findById($c['country_id']);

          if (!$country) {
            $country = $this->countries->getNew();
          }

          $country->id = $c['country_id'];
          $country->name = $c['country_name'];
          $country->areacode = $c['country_areacode'];

          $this->countries->save($country);
        }

        return [
          'data' => ['countries' => $this->countries->getAll()],
        ];
      },
    ]);

    return $responses;
  }

}
<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class OrderDetailTiket extends Entity {

  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = ['user_id', 'order_id', 'email', 'data'];

  protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


}

<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Commands;

use Illuminate\Console\Command;

use Odeo\Domains\Inventory\Flight\Tiket\Crawler\AirportCrawler;

class GetAirports extends Command
{

    protected $signature = 'get:vendor-tiket-airports';

    protected $description = 'Retrieve list of airports from Tiket.com';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
      (new AirportCrawler)->getAirports();
    }
}

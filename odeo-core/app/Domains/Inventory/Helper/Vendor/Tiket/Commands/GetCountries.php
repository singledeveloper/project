<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\Crawler\CountryCrawler;


class GetCountries extends Command
{

    protected $signature = 'get:vendor-tiket-countries';

    protected $description = 'Retrieve list of countries from Tiket.com';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
      (new CountryCrawler)->getCountries();
    }
}

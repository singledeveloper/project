<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Tiket\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\Model\OrderDetailTiket;


class OrderDetailTiketRepository extends Repository {

  public function __construct(OrderDetailTiket $detailFlightTiket) {
    $this->model = $detailFlightTiket;
  }

  public function getExistingOrder($orderId) {

    return $this->model->where('order_id', $orderId)->first();

  }

  public function findByOrderDetailId($order_detail_id) {
    return $this->model
      ->where('order_detail_id', $order_detail_id)
      ->first();
  }

}

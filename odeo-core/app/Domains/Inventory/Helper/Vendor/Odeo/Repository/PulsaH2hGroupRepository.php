<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaH2hGroup;

class PulsaH2hGroupRepository extends Repository {

  public function __construct(PulsaH2hGroup $pulsaH2hGroup) {
    $this->model = $pulsaH2hGroup;
  }

  public function getAllActive() {
    return $this->model->where('is_active', true)->get();
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\VendorSwitcherMutation;

class VendorSwitcherMutationRepository extends Repository {

  public function __construct(VendorSwitcherMutation $vendorSwitcherMutation) {
    $this->model = $vendorSwitcherMutation;
  }

  public function getActiveMutations() {
    return $this->model->whereHas('vendorSwitcher', function($query){
      $query->where('is_active', true);
    })->get();
  }

  public function findByVendorSwitcherId($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)->first();
  }

  public function findByVendorSwitcherIdLocked($vendorSwitcherId) {
    return $this->model->lockForUpdate()->where('vendor_switcher_id', $vendorSwitcherId)->first();
  }

  public function findAllWithVendorSwitcher() {
    return $this->model->with('vendorSwitcher')->whereHas('vendorSwitcher', function($query){
      $query->whereNull('owner_store_id')->orderBy('name', 'desc');
    })
      ->orderByRaw('last_zero_at desc nulls last')
      ->get();
  }

  public function findByVendorSwitcherIds($vendorSwitcherIds) {
    return $this->model->whereIn('vendor_switcher_id', $vendorSwitcherIds)->get()->keyBy('vendor_switcher_id');
  }

}
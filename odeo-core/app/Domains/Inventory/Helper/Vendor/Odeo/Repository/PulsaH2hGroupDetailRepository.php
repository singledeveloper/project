<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaH2hGroupDetail;

class PulsaH2hGroupDetailRepository extends Repository {

  public function __construct(PulsaH2hGroupDetail $pulsaH2hGroupDetail) {
    $this->model = $pulsaH2hGroupDetail;
  }

  public function findByPulsaOdeoId($pulsaOdeoId, $groupId) {
    return $this->model->where('pulsa_odeo_id', $pulsaOdeoId)->where('pulsa_h2h_group_id', $groupId)->first();
  }

  public function getByUserId($userId, $pulsaOdeoIds = []) {
    $query = $this->model->join('pulsa_h2h_groups', 'pulsa_h2h_groups.id', '=', 'pulsa_h2h_group_details.pulsa_h2h_group_id')
      ->join('pulsa_h2h_users', 'pulsa_h2h_users.pulsa_h2h_group_id', '=', 'pulsa_h2h_groups.id')
      ->where('pulsa_h2h_users.user_id', $userId)
      ->where('pulsa_h2h_groups.is_active', true);

    if (sizeof($pulsaOdeoIds) > 0) {
      $query = $query->whereIn('pulsa_h2h_group_details.pulsa_odeo_id', $pulsaOdeoIds);
    }

    return $query->select(
      'pulsa_h2h_group_details.pulsa_odeo_id',
      'pulsa_h2h_group_details.price'
    )->get()->keyBy('pulsa_odeo_id');
  }

  public function findByUserId($userId, $pulsaOdeoId) {
    return $this->model->join('pulsa_h2h_groups', 'pulsa_h2h_groups.id', '=', 'pulsa_h2h_group_details.pulsa_h2h_group_id')
      ->join('pulsa_h2h_users', 'pulsa_h2h_users.pulsa_h2h_group_id', '=', 'pulsa_h2h_groups.id')
      ->where('pulsa_h2h_users.user_id', $userId)
      ->where('pulsa_h2h_groups.is_active', true)
      ->where('pulsa_h2h_group_details.pulsa_odeo_id', $pulsaOdeoId)
      ->select(
      'pulsa_h2h_group_details.pulsa_odeo_id',
      'pulsa_h2h_group_details.price',
      'pulsa_h2h_group_details.is_enabled'
    )->first();
  }

  public function getByGroupId($groupId) {
    return $this->model->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_h2h_group_details.pulsa_odeo_id')
      ->where('pulsa_h2h_group_details.pulsa_h2h_group_id', $groupId)
      ->select(
        'pulsa_h2h_group_details.id',
        'pulsa_odeos.id as pulsa_odeo_id',
        'pulsa_odeos.name',
        'pulsa_odeos.category',
        'pulsa_odeos.inventory_code',
        'pulsa_odeos.price as pulsa_odeo_price',
        'pulsa_h2h_group_details.price'
      )->orderBy('pulsa_odeos.category', 'asc')
      ->orderBy('pulsa_odeos.price', 'asc')
      ->get();
  }

  public function getByGroupIdNoJoin($groupId) {
    return $this->model->where('pulsa_h2h_group_id', $groupId)->get()->keyBy('pulsa_odeo_id');
  }

}

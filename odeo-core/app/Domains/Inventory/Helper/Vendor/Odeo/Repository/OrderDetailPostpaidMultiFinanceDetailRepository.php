<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPostpaidMultiFinanceDetail;

class OrderDetailPostpaidMultiFinanceDetailRepository extends Repository {

  public function __construct(OrderDetailPostpaidMultiFinanceDetail $orderDetailPostpaidMultiFinanceDetail) {
    $this->model = $orderDetailPostpaidMultiFinanceDetail;
  }

}
<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaH2hUser;

class PulsaH2hUserRepository extends Repository {

  public function __construct(PulsaH2hUser $pulsaH2hUser) {
    $this->model = $pulsaH2hUser;
  }

  public function findByUser($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function getUserIdByGroupId($groupId) {
    return $this->model->where('pulsa_h2h_group_id', $groupId)->select('user_id')->get();
  }

  public function getByGroupId($groupId) {
    return $this->model->join('users', 'users.id', '=', 'pulsa_h2h_users.user_id')
      ->where('pulsa_h2h_group_id', $groupId)
      ->select('pulsa_h2h_users.user_id', 'pulsa_h2h_users.id', 'users.name')
      ->orderBy('users.name', 'asc')->get();
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 8:15 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\VendorSwitcherReconciliation;

class VendorSwitcherReconciliationRepository extends Repository {

  public function __construct(VendorSwitcherReconciliation $vendorSwitcherReconciliation) {
    $this->model = $vendorSwitcherReconciliation;
  }

  public function findByTransactionId($trxId, $vendorSwitcherId) {
    return $this->model->where('transaction_id', $trxId)->where('vendor_switcher_id', $vendorSwitcherId)->first();
  }

  public function findByOrderId($orderId, $vendorSwitcherId = '') {
    $query = $this->model->where('order_id', $orderId);
    if ($vendorSwitcherId != '') $query = $query->where('vendor_switcher_id', $vendorSwitcherId);
    return $query->orderBy('id', 'desc')->first();
  }

  public function findBySwitcherId($switcherId, $vendorSwitcherId) {
    return $this->model->where('order_detail_pulsa_switcher_id', $switcherId)
      ->where('vendor_switcher_id', $vendorSwitcherId)->orderBy('id', 'desc')->first();
  }

  public function findStuckSwitcher($switcherId) {
    return $this->model->where('order_detail_pulsa_switcher_id', $switcherId)->where('status', '00000')->first();
  }

  public function findByBillerTransactionId($trxId, $vendorSwitcherId) {
    return $this->model->where('biller_transaction_id', $trxId)->where('vendor_switcher_id', $vendorSwitcherId)->first();
  }

  public function getLog($detailId) {
    return $this->model->where('order_detail_pulsa_switcher_id', $detailId)->orderBy('id', 'desc')->get();
  }

  public function getCountByStatus($vendorSwitcherIds = [], $statusCodes, $date = '') {
    $query = $this->model->whereIn('status', $statusCodes);
    if ($date != '') $query = $query->where(\DB::raw('date(created_at)'), $date);
    if (count($vendorSwitcherIds) > 0) $query = $query->whereIn('vendor_switcher_id', $vendorSwitcherIds);
    return $query->groupBy('vendor_switcher_id')->select('vendor_switcher_id', \DB::raw('count(id) as total_counts'))->get();
  }

  public function getSNCount($vendorSwitcherIds = [], $date = '') {
    $query = $this->model->where('status', SwitcherConfig::BILLER_SUCCESS)
      ->where('reference_number', '<>', 'N/A')->whereNotNull('reference_number');
    if ($date != '') $query = $query->where(\DB::raw('date(created_at)'), $date);
    if (count($vendorSwitcherIds) > 0) $query = $query->whereIn('vendor_switcher_id', $vendorSwitcherIds);
    return $query->groupBy('vendor_switcher_id')->select('vendor_switcher_id', \DB::raw('count(id) as total_counts'))->get();
  }

  public function getLeadTime($vendorSwitcherIds = [], $date = '') {
    $query = $this->model->where('status', SwitcherConfig::BILLER_SUCCESS);
    if ($date != '') $query = $query->where(\DB::raw('date(created_at)'), $date);
    if (count($vendorSwitcherIds) > 0) $query = $query->whereIn('vendor_switcher_id', $vendorSwitcherIds);
    return $query->groupBy('vendor_switcher_id')->select('vendor_switcher_id', \DB::raw('avg(updated_at - created_at) as average_lead_time'))->get();
  }

  public function getBillerSales($vendorSwitcherId, $start, $end) {

    return $this->model->whereBetween('created_at', [$start, $end])
      ->where('vendor_switcher_id', $vendorSwitcherId)
      ->where('status', SwitcherConfig::BILLER_SUCCESS)
      ->select(\DB::raw('sum(base_price) as sales_total'))
      ->first();
  }

  public function gets() {
    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    $query = $query->where('status', SwitcherConfig::BILLER_SUCCESS);

    if (isset($filters['search'])) $filters = $filters['search'];

    if (isset($filters['vendor_switcher_id']) && $filters['vendor_switcher_id'] != '0') {
      $query = $query->where('vendor_switcher_id', $filters['vendor_switcher_id']);
      $filters['sort_by'] = 'created_at';
    }

    if (isset($filters['date'])) {
      $query = $query->where(\DB::raw('date(created_at)'), date('Y-m-d', strtotime($filters['date'])));
    }

    if (isset($filters['phone_number'])) {
      $query = $query->whereIn('number', [purifyTelephone($filters['phone_number']), $filters['phone_number']]);
      $filters['sort_by'] = 'created_at';
    }

    if (!isset($filters['sort_by'])) $filters['sort_by'] = 'id';
    if (!isset($filters['sort_type'])) $filters['sort_type'] = 'desc';

    return $this->getResult($query->select(
      'transaction_id',
      'biller_transaction_id',
      'vendor_switcher_name',
      'inventory_name',
      'number',
      'base_price',
      'reference_number',
      'created_at'
    )->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function getsForSupply() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->join('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.id', '=', 'vendor_switcher_reconciliations.order_detail_pulsa_switcher_id')
      ->where('vendor_switcher_owner_store_id', $filters['store_id'])
      ->whereRaw('vendor_switcher_reconciliations.vendor_switcher_id = order_detail_pulsa_switchers.vendor_switcher_id');

    if (isset($filters['vendor_switcher_id']) && $filters['vendor_switcher_id'] != '0') {
      $query = $query->where('vendor_switcher_reconciliations.vendor_switcher_id', $filters['vendor_switcher_id']);
    }

    if (isset($filters['order_id'])) {
      $query = $query->where('vendor_switcher_reconciliations.order_id', $filters['order_id']);
    }

    if (isset($filters['date'])) {
      $query = $query->where(\DB::raw('date(vendor_switcher_reconciliations.created_at)'), date('Y-m-d', strtotime($filters['date'])));
    }

    if (isset($filters['number'])) {
      $query = $query->whereIn('vendor_switcher_reconciliations.number', [purifyTelephone($filters['number']), $filters['number']]);
      $filters['sort_by'] = 'vendor_switcher_reconciliations.created_at';
    }

    if (isset($filters['destination'])) {
      $query = $query->where('vendor_switcher_reconciliations.destination', purifyTelephone($filters['destination']));
      $filters['sort_by'] = 'vendor_switcher_reconciliations.created_at';
    }

    if (isset($filters['status'])) {
      $query = $query->where('order_detail_pulsa_switchers.status', $filters['status']);
    }

    if (!isset($filters['sort_by'])) $filters['sort_by'] = 'vendor_switcher_reconciliations.id';
    if (!isset($filters['sort_type'])) $filters['sort_type'] = 'desc';

    return $this->getResult($query
      ->select('vendor_switcher_reconciliations.*', 'order_detail_pulsa_switchers.status as order_status')
      ->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function checkDuplicateDifferentTransaction($id, $switcherId) {
    return $this->model->where('order_detail_pulsa_switcher_id', $switcherId)->where('id', '<>', $id)->first();
  }

  public function checkDuplicateDifferentSwitcher($number, $switcherId, $sn, $checkType) {
    $query = $this->model->where('number', $number)->whereIn('status', [
      SwitcherConfig::BILLER_IN_QUEUE,
      SwitcherConfig::BILLER_SUCCESS,
      SwitcherConfig::BILLER_CREATED
    ])->where('reference_number', $sn)
      ->where('order_detail_pulsa_switcher_id', '<>', $switcherId);

    if ($checkType == SwitcherConfig::DUPLICATE_TYPE_24_HOURS)
      $query = $query->where('created_at', '>', Carbon::now()->subHours(24)->toDateTimeString());
    else $query = $query->where(\DB::raw('date(created_at)'), date('Y-m-d'));

    return $query->first();
  }

  public function getJabberNoReplies() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)
      ->where('format_type', Supplier::TYPE_TEXT)->whereNull('notify')
      ->get();
  }

  public function findByInventoryCodeAndNumber($data) {
    $query = $this->model->whereIn('status', [SwitcherConfig::BILLER_IN_QUEUE, SwitcherConfig::BILLER_SUSPECT])
      ->where(function ($query) use ($data) {
        $query->where('request', 'like', '%' . $data['code'] . '%' . $data['no'] . '%')
          ->orWhere('request', 'like', '%' . $data['no'] . '%' . $data['code'] . '%');
      })->where('vendor_switcher_id', $data['vendor_switcher_id']);

    if (isset($data['format_type'])) $query->where('format_type', $data['format_type']);

    return $query->orderBy('id', 'desc')->first();
  }

  public function getPendingTransactions($vendorSwitcherId) {
    return $this->model->whereIn('status', [SwitcherConfig::BILLER_IN_QUEUE, SwitcherConfig::BILLER_TIMEOUT])
      ->where('vendor_switcher_id', $vendorSwitcherId)
      ->where(\DB::raw('date(created_at)'), date('Y-m-d'))->get();
  }

  public function getPendingTransactionByTemplateIds($templateIds) {
    return $this->model->join('vendor_switcher_connections', 'vendor_switcher_connections.vendor_switcher_id', '=', 'vendor_switcher_reconciliations.vendor_switcher_id')
      ->whereIn('vendor_switcher_reconciliations.status', [SwitcherConfig::BILLER_IN_QUEUE, SwitcherConfig::BILLER_TIMEOUT])
      ->whereIn('vendor_switcher_connections.template_id', $templateIds)
      ->where(\DB::raw('date(vendor_switcher_reconciliations.created_at)'), date('Y-m-d'))
      ->select(
        'vendor_switcher_reconciliations.id',
        'vendor_switcher_connections.id as connection_id',
        'vendor_switcher_connections.template_id',
        'vendor_switcher_connections.server_destination',
        'vendor_switcher_connections.server_user_id',
        'vendor_switcher_connections.server_user_name',
        'vendor_switcher_connections.server_password',
        'vendor_switcher_connections.server_pin',
        'vendor_switcher_connections.server_token'
      )
      ->get();
  }

  public function getPendingFromHiddenInventories() {
    return $this->model
      ->where('status', SwitcherConfig::BILLER_IN_QUEUE)
      ->where('created_at', '<=', Carbon::now()->subMinutes(3)->toDateTimeString())
      ->whereHas('switcher', function ($subquery) {
        $subquery->where('pulsa_status', Pulsa::STATUS_OK_HIDDEN)
          ->where('operator_id', '<>', Pulsa::OPERATOR_PLN)
          ->where('status', SwitcherConfig::SWITCHER_IN_QUEUE);
      })->get();
  }

  public function getByIds($ids) {
    return $this->model->whereIn('status', [SwitcherConfig::BILLER_IN_QUEUE, SwitcherConfig::BILLER_TIMEOUT])
      ->whereIn('id', $ids)->get();
  }

  public function getMonthRecapByStatus($status, $ownerStoreIds = []) {
    $query = $this->getCloneModel()->join('vendor_switchers', 'vendor_switcher_reconciliations.vendor_switcher_id', '=', 'vendor_switchers.id')
      ->where('vendor_switchers.status', $status)
      ->where('vendor_switcher_reconciliations.status', SwitcherConfig::BILLER_SUCCESS)
      ->where('vendor_switcher_reconciliations.trx_month_year', date('Y-m'));
    if (count($ownerStoreIds) > 0) $query = $query->whereIn('vendor_switcher_owner_store_id', $ownerStoreIds);

    return $query->groupBy('destination')
      ->select('destination', \DB::raw('sum(cast(inventory_code as integer)) as total_sales'))
      ->get()->keyBy('destination');
  }

}

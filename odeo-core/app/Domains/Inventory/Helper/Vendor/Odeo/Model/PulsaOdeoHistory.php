<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;

class PulsaOdeoHistory extends Entity
{

  public function pulsa() {
    return $this->belongsTo(PulsaOdeo::class, 'pulsa_odeo_id');
  }

}

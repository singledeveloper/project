<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherReconciliation extends Entity
{
  
  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class, 'order_detail_pulsa_switcher_id');
  }

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }

}

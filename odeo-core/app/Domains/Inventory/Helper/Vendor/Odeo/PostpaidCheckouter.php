<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/26/16
 * Time: 9:30 PM
 */

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Core\PipelineListener;

class PostpaidCheckouter {

  private $switcherOrder, $pulsaInventories, $inquiryFormatter, $orderDetails;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->inquiryFormatter = app()->make(\Odeo\Domains\Supply\Formatter\InquiryManager::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
  }

  public function checkout(PipelineListener $listener, $data) {

    $switcher = $this->switcherOrder->getNew();
    $inventory = $this->pulsaInventories->findById($data['item']['inventory_id']);
    $pulsa = $inventory->pulsa;
    $detail = $data['item']['item_detail'];
    $switcher->order_detail_id = $data['order_detail']['id'];
    $switcher->first_inventory_id = $data['item']['inventory_id'];
    $switcher->first_base_price = $detail['biller_price'] + ($detail['multiplier'] * $inventory->base_price);
    $switcher->subsidy_amount = $inventory->base_price - $pulsa->price;
    $switcher->current_inventory_id = $data['item']['inventory_id'];
    $switcher->current_base_price = $detail['biller_price'] + ($detail['multiplier'] * $inventory->base_price);
    $switcher->number = isset($detail['number']) ? $detail['number'] : $detail['subscriber_id'];
    $switcher->inventory_query_dumps = $data['item']['inventory_id'];
    $switcher->vendor_switcher_id = $inventory->vendor_switcher_id;
    $switcher->pulsa_status = $pulsa->status;

    $this->switcherOrder->save($switcher);

    $orderDetail = $switcher->orderDetail;
    $orderDetail->name = $pulsa->name;
    $this->orderDetails->save($orderDetail);

    if (isset($detail['inquiries'])) {
      $inquiryManager = $this->inquiryFormatter->setPath($pulsa->service_detail_id);
      if ($inquiryManager != '') $inquiryManager->record($switcher->id, $detail);
    }

    return $listener->response(201);
  }

}
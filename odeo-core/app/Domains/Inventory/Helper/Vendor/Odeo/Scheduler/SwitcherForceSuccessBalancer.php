<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo\Scheduler;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Inventory\Helper\Jobs\AutoForceSuccess;

class SwitcherForceSuccessBalancer {

  private $recons;

  public function __construct() {
    $this->recons = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
  }

  public function run() {

    if (Pulsa::IS_MAINTANANCE) return;

    foreach ($this->recons->getPendingFromHiddenInventories() as $item) {
      dispatch(new AutoForceSuccess($item->order_detail_pulsa_switcher_id));
    }

  }

}

<?php

namespace Odeo\Domains\Inventory\Helper\Vendor\Odeo;

use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class PulsaMutationEditor {

  private $switcherOrder, $statuses = [
    SwitcherConfig::ROUTE_DOUBLE_PURCHASE,
    SwitcherConfig::ROUTE_VOID_DOUBLE_PURCHASE,
    SwitcherConfig::ROUTE_BALANCER,
    SwitcherConfig::ROUTE_UNKNOWN_DIFF,
    SwitcherConfig::ROUTE_VOID,
    SwitcherConfig::ROUTE_CASHBACK
  ];

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function insertManualMutation(PipelineListener $listener, $data) {
    if (!in_array($data['type'], $this->statuses))
      return $listener->response(400, 'Type invalid.');

    $insertMutationData = [
      'vendor_switcher_id' => $data['vendor_switcher_id'],
      'mutation_route' => $data['type'],
      'user_id' => $data['auth']['user_id']
    ];

    if (isset($data['order_id']) && $data['order_id'] != '') {
      $switcher = $this->switcherOrder->findByOrderId($data['order_id']);
      if (!$switcher) return $listener->response(400, 'Order ID not exist or not a Pulsa Order.');
      $insertMutationData['order_detail_pulsa_switcher_id'] = $switcher->id;
      $insertMutationData['amount'] = isset($data['amount']) ? $data['amount'] : $switcher->currentInventory->current_base_price;
    }
    else if (!isset($data['amount']) || (isset($data['amount']) && $data['amount'] == ''))
      return $listener->response(400, 'Amount is needed.');
    else $insertMutationData['amount'] = $data['amount'];

    $listener->pushQueue(new InsertMutation($insertMutationData));

    return $listener->response(200);
  }

  public function insertBalanceMutationAll(PipelineListener $listener, $data) {

    foreach($this->switcherOrder->getCompletedSwitcherNotHasMutation($data['date']) as $item) {
      $listener->pushQueue(new InsertMutation([
        'order_detail_pulsa_switcher_id' => $item->id,
        'vendor_switcher_id' => $item->vendor_switcher_id,
        'amount' => -$item->current_base_price,
        'mutation_route' => SwitcherConfig::ROUTE_BALANCER,
        'user_id' => isset($data['auth']) ? $data['auth']['user_id'] : null
      ]));
    }

    return $listener->response(200);
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 2:50 PM
 */

namespace Odeo\Domains\Inventory\Helper;

use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\PulsaCode;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\DDLending\LoanPaymentRequester;
use Odeo\Domains\Inventory\Bolt\BoltManager;
use Odeo\Domains\Inventory\BPJS\BPJSManager;
use Odeo\Domains\Inventory\Broadband\BroadbandManager;
use Odeo\Domains\Inventory\EMoney\EMoneyManager;
use Odeo\Domains\Inventory\MultiFinance\MultiFinanceManager;
use Odeo\Domains\Inventory\PDAM\PDAMManager;
use Odeo\Domains\Inventory\Creditbill\CreditBillCheckouter;
use Odeo\Domains\Inventory\Flight\FlightManager;
use Odeo\Domains\Inventory\GameVoucher\GameVoucherManager;
use Odeo\Domains\Inventory\GooglePlay\GooglePlayManager;
use Odeo\Domains\Inventory\Hotel\HotelManager;
use Odeo\Domains\Inventory\Landline\LandlineManager;
use Odeo\Domains\Inventory\PGN\PGNManager;
use Odeo\Domains\Inventory\Pln\PlnManager;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Inventory\Transportation\TransportationManager;
use Odeo\Domains\Inventory\UserInvoice\UserInvoiceManager;
use Odeo\Domains\Payment\Pax\EdcTransactionRequester;
use Odeo\Domains\Subscription\StoreRequester;
use Odeo\Domains\Subscription\WarrantyRequester;
use Odeo\Domains\Subscription\MdPlus\MdPlusRequester;
use Odeo\Domains\Transaction\DepositRequester;
use Odeo\Domains\Transaction\TopupRequester;

class InventoryManager {

  private static $serviceInstance, $storeInventoryInstance;

  public function setServiceDetail($serviceDetailId) {
    $serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    self::$serviceInstance = $serviceDetail->findById($serviceDetailId);
  }

  public function setStoreInventory() {
    return null;
  }

  public function getServiceDetail() {
    return self::$serviceInstance;
  }

  public function getStoreInventory() {
    return self::$storeInventoryInstance;
  }

  public function getManagerClassName($service_id) {
    switch ($service_id) {
      case Service::PLAN:
        return StoreRequester::class;
        break;
      case Service::OCASH:
        return TopupRequester::class;
        break;
      case Service::ODEPOSIT:
        return DepositRequester::class;
        break;
      case Service::WARRANTY:
        return WarrantyRequester::class;
        break;
      case Service::MD_PLUS:
        return MdPlusRequester::class;
        break;
      case Service::PULSA:
      case Service::PAKET_DATA:
      case Service::PULSA_POSTPAID:
        return PulsaManager::class;
        break;
      case Service::BOLT:
        return BoltManager::class;
        break;
      case Service::PLN:
      case Service::PLN_POSTPAID:
        return PlnManager::class;
        break;
      case Service::BPJS_KES:
      case Service::BPJS_TK:
        return BPJSManager::class;
        break;
      case Service::HOTEL:
        return HotelManager::class;
        break;
      case Service::FLIGHT:
        return FlightManager::class;
        break;
      case Service::CREDIT_BILL:
        return CreditBillCheckouter::class;
        break;
      case Service::BROADBAND:
        return BroadbandManager::class;
        break;
      case Service::LANDLINE:
        return LandlineManager::class;
        break;
      case Service::USER_INVOICE:
        return UserInvoiceManager::class;
        break;
      case Service::GAME_VOUCHER:
        return GameVoucherManager::class;
        break;
      case Service::GOOGLE_PLAY:
        return GooglePlayManager::class;
        break;
      case Service::PDAM:
        return PDAMManager::class;
        break;
      case Service::PGN:
        return PGNManager::class;
        break;
      case Service::EMONEY:
        return EMoneyManager::class;
        break;
      case Service::TRANSPORTATION:
        return TransportationManager::class;
        break;
      case Service::MULTI_FINANCE:
        return MultiFinanceManager::class;
        break;
      case Service::DDLENDING_PAYMENT:
        return LoanPaymentRequester::class;
        break;
      case Service::EDC_TRANSACTION:
        return EdcTransactionRequester::class;
        break;
    }
  }

  public function getServiceDataFromCategory($category) {
    $data = [];

    if (in_array($category, PulsaCode::groupBolt())) {
      $data['service_id'] = Service::BOLT;
      $data['service_detail_id'] = ServiceDetail::BOLT_ODEO;
      $data['category'] = PulsaCode::groupBolt();
      $data['operator_id'] = Pulsa::OPERATOR_BOLT;
    }
    else if (in_array($category, PulsaCode::groupPulsa())) {
      $data['service_id'] = Service::PULSA;
      $data['service_detail_id'] = ServiceDetail::PULSA_ODEO;
      $data['category'] = PulsaCode::groupPulsa();
    }
    else if (in_array($category, PulsaCode::groupData())) {
      $data['service_id'] = Service::PAKET_DATA;
      $data['service_detail_id'] = ServiceDetail::PAKET_DATA_ODEO;
      $data['category'] = PulsaCode::groupData();
    }
    else if (in_array($category, PulsaCode::groupTransportation())) {
      $data['service_id'] = Service::TRANSPORTATION;
      $data['service_detail_id'] = ServiceDetail::TRANSPORTATION_ODEO;
      $data['category'] = PulsaCode::groupTransportation();
    }
    else if (in_array($category, PulsaCode::groupPln())) {
      $data['service_id'] = Service::PLN;
      $data['service_detail_id'] = ServiceDetail::PLN_ODEO;
      $data['category'] = PulsaCode::groupPln();
      $data['operator_id'] = Pulsa::OPERATOR_PLN;
    }
    else if (in_array($category, PulsaCode::groupGameVoucher())) {
      $data['service_id'] = Service::GAME_VOUCHER;
      $data['service_detail_id'] = ServiceDetail::GAME_VOUCHER_ODEO;
      $data['category'] = PulsaCode::groupGameVoucher();
    }
    else if (in_array($category, PulsaCode::groupGooglePlay())) {
      $data['service_id'] = Service::GOOGLE_PLAY;
      $data['service_detail_id'] = ServiceDetail::GOOGLE_PLAY_ODEO;
      $data['category'] = PulsaCode::groupGooglePlay();
    }
    else if (in_array($category, PulsaCode::groupEMoney())) {
      $data['service_id'] = Service::EMONEY;
      $data['service_detail_id'] = ServiceDetail::EMONEY_ODEO;
      $data['category'] = PulsaCode::groupEMoney();
    }
    else if ($category == PostpaidType::BPJS_KES) {
      $data['service_id'] = Service::BPJS_KES;
      $data['service_detail_id'] = ServiceDetail::BPJS_KES_ODEO;
      $data['category'] = [PostpaidType::BPJS_KES];
    }
    else if ($category == PostpaidType::PLN_POSTPAID) {
      $data['service_id'] = Service::PLN_POSTPAID;
      $data['service_detail_id'] = ServiceDetail::PLN_POSTPAID_ODEO;
      $data['category'] = [PostpaidType::PLN_POSTPAID];
    }
    else if ($category == PostpaidType::PDAM) {
      $data['service_id'] = Service::PDAM;
      $data['service_detail_id'] = ServiceDetail::PDAM_ODEO;
      $data['category'] = [PostpaidType::PDAM];
    }
    else if ($category == PostpaidType::PGN) {
      $data['service_id'] = Service::PGN;
      $data['service_detail_id'] = ServiceDetail::PGN_ODEO;
      $data['category'] = [PostpaidType::PGN];
    }
    else if ($category == PostpaidType::MULTIFINANCE) {
      $data['service_id'] = Service::MULTI_FINANCE;
      $data['service_detail_id'] = ServiceDetail::MULTI_FINANCE_ODEO;
      $data['category'] = [PostpaidType::MULTIFINANCE];
    }
    else if ($category == PostpaidType::TELKOM_PSTN) {
      $data['service_id'] = Service::LANDLINE;
      $data['service_detail_id'] = ServiceDetail::LANDLINE_ODEO;
      $data['category'] = [PostpaidType::TELKOM_PSTN];
    }
    else if (in_array($category, PostpaidType::groupPulsaPostpaid())) {
      $data['service_id'] = Service::PULSA_POSTPAID;
      $data['service_detail_id'] = ServiceDetail::PULSA_POSTPAID_ODEO;
      $data['category'] = PostpaidType::groupPulsaPostpaid();
    }
    else if (in_array($category, PostpaidType::groupBroadband())) {
      $data['service_id'] = Service::BROADBAND;
      $data['service_detail_id'] = ServiceDetail::BROADBAND_ODEO;
      $data['category'] = PostpaidType::groupBroadband();
    }

    if (in_array($category, PulsaCode::groupOperatorTelkomsel()))
      $data['operator_id'] = Pulsa::OPERATOR_TELKOMSEL;
    else if (in_array($category, PulsaCode::groupOperatorIndosat()))
      $data['operator_id'] = Pulsa::OPERATOR_INDOSAT;
    else if (in_array($category, PulsaCode::groupOperatorXL()))
      $data['operator_id'] = Pulsa::OPERATOR_XL;
    else if (in_array($category, PulsaCode::groupOperatorTri()))
      $data['operator_id'] = Pulsa::OPERATOR_THREE;
    else if (in_array($category, PulsaCode::groupOperatorAxis()))
      $data['operator_id'] = Pulsa::OPERATOR_AXIS;
    else if (in_array($category, PulsaCode::groupOperatorSmartFren()))
      $data['operator_id'] = Pulsa::OPERATOR_SMARTFREN;

    return $data;
  }
}

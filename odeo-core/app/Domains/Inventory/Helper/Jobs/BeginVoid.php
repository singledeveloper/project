<?php

namespace Odeo\Domains\Inventory\Helper\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;
use Odeo\Domains\Inventory\UserInvoice\UserInvoiceManager;
use Odeo\Domains\Transaction\CashRequester;
use Odeo\Jobs\Job;

class BeginVoid extends Job {

  use SerializesModels;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $orderDetailRepo = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);

    $orderDetail = $orderDetailRepo->findByOrderId($this->data['order_id']);
    
    if ($orderDetail) {
      $serviceId = $orderDetail->serviceDetail->service_id;

      if (in_array($serviceId, Service::PPOB)) {
        $switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  
        if (isset($this->data['order_id'])) {
          $switcher = $switcherOrder->findByOrderId($this->data["order_id"]);
          $this->data["order_detail_pulsa_switcher_id"] = $switcher->id;
        }
        else $switcher = $switcherOrder->findById($this->data["order_detail_pulsa_switcher_id"]);
    
        $switcher->status = SwitcherConfig::SWITCHER_VOID_IN_QUEUE;
        $switcherOrder->save($switcher);
    
        $pipeline = new Pipeline();
        $pipeline->add(new Task(PulsaManager::class, 'void', [
          "order_detail_pulsa_switcher_id" => $this->data["order_detail_pulsa_switcher_id"],
          "vendor_switcher_id" => $switcher->vendor_switcher_id
        ]));
        $pipeline->execute([]);
    
        if ($pipeline->fail()) {
          $switcher->status = SwitcherConfig::SWITCHER_VOID_FAIL;
          $switcherOrder->save($switcher);
        }
      } else if ($serviceId == Service::USER_INVOICE) {
        $invoiceDetailRepo = app()->make(\Odeo\Domains\Inventory\UserInvoice\Repository\OrderDetailUserInvoiceRepository::class);
        $invoiceDetail = $invoiceDetailRepo->findByAttributes('order_detail_id', $orderDetail->id);

        $pipeline = new Pipeline();
        $pipeline->add(new Task(UserInvoiceManager::class, 'refund'));
        $pipeline->add(new Task(CashRequester::class, 'settleOrder'));

        $pipeline->enableTransaction();
        $pipeline->execute([
          'order_id' => $this->data['order_id'],
          'service_detail_id' => $orderDetail->service_detail_id,
          'biller_id' => $invoiceDetail->biller_id
        ]);

        if ($pipeline->fail()) {
          throw new \Exception($pipeline->errorMessage);
        }
      }
    }

    throw new \Exception('order not found');
  }

}

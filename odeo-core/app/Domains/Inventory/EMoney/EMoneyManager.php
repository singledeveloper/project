<?php

namespace Odeo\Domains\Inventory\EMoney;

use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class EMoneyManager extends InventoryManager {

  private static $eMoneyInstance;
  private $switcherOrder, $pulsaInventoriesRepo, $userKtpValidator;

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->pulsaInventoriesRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
  }

  public function setVendor(PipelineListener $listener, $data) {
    self::$eMoneyInstance = app()->make(\Odeo\Domains\Inventory\EMoney\Odeo\OdeoManager::class);
    $this->setServiceDetail(isset($data['service_detail_id']) ? $data['service_detail_id'] : ServiceDetail::EMONEY_ODEO);
    return $listener->response(200, []);
  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$eMoneyInstance, $name], $argument);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    $this->setVendor($listener, $data);
    if ($inventory = $this->pulsaInventoriesRepo->getCheapestPrice($data['item_id'])) {
      if (!$this->userKtpValidator->isVerified($data['auth']['user_id'])) {
        return $listener->response(400, trans('pulsa.cant_buy_without_kyc'));
      }
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      return self::$eMoneyInstance->validateInventory($listener, $data);
    }
    return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

}

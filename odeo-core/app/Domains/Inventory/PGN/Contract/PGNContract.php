<?php

namespace Odeo\Domains\Inventory\PGN\Contract;

use Odeo\Domains\Core\PipelineListener;

interface PGNContract {

  public function inquiry(PipelineListener $listener, $data);

  public function checkout(PipelineListener $listener, $data);

  public function purchasePostpaid(PipelineListener $listener, $data);

  public function validatePostpaidInventory(PipelineListener $listener, $data);

}
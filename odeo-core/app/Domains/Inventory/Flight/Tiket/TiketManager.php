<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/21/16
 * Time: 4:27 PM
 */

namespace Odeo\Domains\Inventory\Flight\Tiket;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Flight\Contract\FlightContract;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\CheckoutRequester;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\OrderRequester;
use Odeo\Domains\Inventory\Helper\Vendor\Tiket\PaymentRequester;

class TiketManager implements FlightContract {


  private $bookingFormRequester, $searcher, $checkoutRequester,
    $orderRequester, $paymentRequester, $tiketAirports;

  public function __construct() {

    $this->bookingFormRequester = app()->make(BookingFormRequester::class);
    $this->searcher = app()->make(Searcher::class);
    $this->checkoutRequester = app()->make(CheckoutRequester::class);
    $this->orderRequester = app()->make(OrderRequester::class);
    $this->paymentRequester = app()->make(PaymentRequester::class);
    $this->tiketAirports = app()->make(AirportSelector::class);

  }

  public function getAirport(PipelineListener $listener, $data) {
    return $this->tiketAirports->getAirport($listener, $data);
  }

  public function searchFlight(PipelineListener $listener, $data) {
    return $this->searcher->searchFlight($listener, $data);
  }

  public function getBookingForm(PipelineListener $listener, $data) {
    return $this->bookingFormRequester->getBookingForm($listener, $data);
  }

  public function addToCart(PipelineListener $listener, $data) {
    return $this->bookingFormRequester->submitForm($listener, $data);
  }

  public function removeCart(PipelineListener $listener, $data) {
    return $this->orderRequester->cancelOrder($listener, $data);
  }

  public function checkout(PipelineListener $listener, $data) {
    return $this->checkoutRequester->checkout($listener, $data);
  }

  public function purchase(PipelineListener $listener, $data) {
    return $this->paymentRequester->pay($listener, $data);
  }

}
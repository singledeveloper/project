<?php

namespace Odeo\Domains\Inventory\GooglePlay;

use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;
use Odeo\Domains\Constant\ServiceDetail;

class GooglePlayManager extends InventoryManager {

  private static $googlePlayInstance;
  private $pulsaInventories, $switcherOrder, $pulsaOdeos;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function setVendor(PipelineListener $listener, $data) {
    self::$googlePlayInstance = app()->make(\Odeo\Domains\Inventory\GooglePlay\Odeo\OdeoManager::class);
    $this->setServiceDetail(isset($data['service_detail_id']) ? $data['service_detail_id'] : ServiceDetail::GOOGLE_PLAY_ODEO);
    return $listener->response(200);
  }

  public function __call($name, $argument) {
    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$googlePlayInstance, $name], $argument);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);
    else if ($inventory = $this->pulsaInventories->getCheapestPrice($data['item_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      return self::$googlePlayInstance->validateInventory($listener, $data);
    }
    return $listener->responseWithErrorStatus(ErrorStatus::EMPTY_STOCK, trans('pulsa.empty_stock'));
  }

  public function getTermAndCon(PipelineListener $listener, $data) {
    return $listener->response(200, [
      [
        '1_text' => trans('google_play.tnc')
      ]
    ]);
  }

  public function getMarketingCopyList(PipelineListener $listener, $data) {
    return $listener->response(200, [
      'marketing_copies' => [[
          'title' => trans('google_play.marketing_copy_title_1'),
          'description' => trans('google_play.marketing_copy_desc_1')
        ], [
          'title' => trans('google_play.marketing_copy_title_2'),
          'description' => trans('google_play.marketing_copy_desc_2')
        ], [
          'title' => trans('google_play.marketing_copy_title_3'),
          'description' => trans('google_play.marketing_copy_desc_3')
      ]]
    ]);
  }

  public function getRedeemInstructionList(PipelineListener $listener, $data) {
    return $listener->response(200, [
      [
        '1_text' => trans('google_play.redeem_instruction_1'),
        '2_list' => [
          [
            '1_text' => trans('google_play.redeem_instruction_1_list_1'),
          ],
          [
            '1_text' => trans('google_play.redeem_instruction_1_list_2'),
          ],
          [
            '1_text' => trans('google_play.redeem_instruction_1_list_3'),
          ]
        ]
      ],
      [
        '1_text' => trans('google_play.redeem_instruction_2'),
        '2_list' => [
          [
            '1_text' => trans('google_play.redeem_instruction_2_list_1'),
          ],
          [
            '1_text' => trans('google_play.redeem_instruction_2_list_2'),
          ],
          [
            '1_text' => trans('google_play.redeem_instruction_2_list_3'),
          ],
          [
            '1_text' => trans('google_play.redeem_instruction_2_list_4'),
          ]
        ]
      ]
    ]);
  }

}

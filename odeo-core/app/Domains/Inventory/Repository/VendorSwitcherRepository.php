<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 3:31 PM
 */

namespace Odeo\Domains\Inventory\Repository;

use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherRepository extends  Repository {

  public function __construct(VendorSwitcher $vendorSwitcher) {
    $this->model = $vendorSwitcher;
  }

  public function gets() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();
    if (isset($filters['is_active'])) $query = $query->where('is_active', $filters['is_active']);
    if (isset($filters['store_id'])) $query = $query->where('owner_store_id', $filters["store_id"]);
    if (isset($filters['is_internal'])) $query = $query->where(function($subquery){
      $subquery->whereNull('owner_store_id');
    });
    if (isset($filters['status'])) $query = $query->where('status', $filters['status']);

    if (!isset($filters['no_active_sort'])) $query = $query->orderBy('is_active', 'desc');
    if (!isset($filters['no_owner_sort'])) $query = $query->orderBy('owner_store_id', 'desc');
    return $this->getResult($query->orderBy('name', 'asc'));
  }

  public function getAutoReplenishBillers() {
    return $this->model->where('is_active', true)->where('is_auto_replenish', true)->get();
  }

  public function getByIds($vendorSwitcherIds) {
    return $this->model->whereIn('id', $vendorSwitcherIds)->orderBy('name', 'asc')->get();
  }

  public function getByIdsWithStoreName($vendorSwitcherIds) {
    return $this->model->join('stores', 'stores.id', '=', 'vendor_switchers.owner_store_id')
      ->whereIn('vendor_switchers.id', $vendorSwitcherIds)
      ->where('vendor_switchers.is_active', true)
      ->orderBy('vendor_switchers.current_balance', 'desc')
      ->select('stores.name', 'vendor_switchers.current_balance')
      ->get();
  }

  public function findByStatus($status, $ownerStoreId) {
    $query = $this->model->where('status', $status);
    if ($ownerStoreId == null) $query = $query->whereNull('owner_store_id');
    else $query = $query->where('owner_store_id', $ownerStoreId);
    return $query->first();
  }

  public function getByStatus($status) {
    return $this->model->where('status', $status)->get();
  }

  public function getByActiveStatus($status) {
    return $this->model->where('status', $status)
      ->where('is_active', true)->get();
  }

  public function findBySlug($slug) {
    return $this->getCloneModel()->where('notify_slug', $slug)->first();
  }

}
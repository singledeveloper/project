<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/11/17
 * Time: 9:17 PM
 */

namespace Odeo\Domains\Inventory\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Inventory\Model\AutoReportVendor;

class AutoReportVendorRepository extends Repository {

  public function __construct(AutoReportVendor $autoReportVendor) {
    $this->model = $autoReportVendor;
  }

  public function getByReferenceIds($referenceIds, $vendor, $type) {
    return $this->model
      ->whereIn('reference_id', $referenceIds)
      ->where('vendor', $vendor)
      ->where('type', $type)
      ->get();
  }

}
<?php

namespace Odeo\Domains\Inventory\Broadband;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\Helper\InventoryManager;

class BroadbandManager extends InventoryManager {


  private static $broadbandInstance;

  public function setVendor(PipelineListener $listener, $data) {

    self::$broadbandInstance = app()->make(\Odeo\Domains\Inventory\Broadband\Odeo\OdeoManager::class);

    $this->setServiceDetail($data['service_detail_id']);

    return $listener->response(200);

  }

  public function __call($name, $argument) {

    $this->setVendor($argument[0], $argument[1]);
    return call_user_func_array([self::$broadbandInstance, $name], $argument);

  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {

    if (Pulsa::IS_MAINTANANCE) return $listener->response(400, Pulsa::MAINTANANCE_MESSAGE);

    if (!isset($data['service_detail_id']) || (isset($data['service_detail_id']) && $data['service_detail_id'] == '0'))
      $data['service_detail_id'] = ServiceDetail::BROADBAND_ODEO;

    $repository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    if ($inventory = $repository->getCheapestFee($data['service_detail_id'])) {
      $this->setVendor($listener, $data);
      $data['inventory_id'] = $inventory->id;
      $data['vendor_switcher_id'] = $inventory->vendor_switcher_id;
      $listener->replaceData($data);

      self::$broadbandInstance->validatePostpaidInventory($listener, $data);
    }
    else return $listener->response(400, trans('pulsa.cant_purchase'));
  }

}

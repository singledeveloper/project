<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/26/16
 * Time: 2:02 PM
 */

namespace Odeo\Domains\Core;


class ModelObserver {

  private $cache;

  protected function clearCacheTags($tags)
  {
    if (!$this->cache) {
      $this->cache = app()->make(\Illuminate\Contracts\Cache\Repository::class);
    }

    $this->cache->tags($tags)->flush();
  }

  public function saved($model)
  {
    $this->clearCacheTags($model->getTable());
  }

  public function deleted($model)
  {
    $this->clearCacheTags($model->getTable());
  }

  public function restored($model)
  {
    $this->clearCacheTags($model->getTable());
  }


}
<?php

namespace Odeo\Domains\Affiliate;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class UserAffiliateSelector implements SelectorListener {

  private $affiliates;

  public function __construct() {
    $this->affiliates = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $output = [];
    $output['id'] = $item->id;
    $output['user_name'] = $item->user->name;
    $output['api_type'] = $item->api_type;
    $output['ip_whitelist'] = $item->whitelist ? json_decode($item->whitelist, true) : [];
    $output['notify_url'] = $item->notify_url;
    $output['notify_url_staging'] = $item->staging_notify_url;
    $output['broadcast_email'] = $item->broadcast_email;
    $output['last_updated_at'] = $item->updated_at->format('Y-m-d H:i:s');
    return $output;

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function gets(PipelineListener $listener, $data) {
    $affiliates = [];

    $this->affiliates->normalizeFilters($data);
    $this->affiliates->setSimplePaginate(true);

    foreach ($this->affiliates->gets() as $item) {
      $affiliates[] = $this->_transforms($item, $this->affiliates);
    }

    if (sizeof($affiliates) > 0)
      return $listener->response(200, array_merge(
        ["affiliates" => $this->_extends($affiliates, $this->affiliates)],
        $this->affiliates->getPagination()
      ));

    return $listener->response(204, ["affiliates" => []]);
  }

}

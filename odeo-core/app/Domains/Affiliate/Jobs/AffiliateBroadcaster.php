<?php

namespace Odeo\Domains\Affiliate\Jobs;

use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Constant\Supplier;
use Odeo\Jobs\Job;

class AffiliateBroadcaster extends Job  {

  private $data, $specificDataPerUser;

  public function __construct($data, $specificDataPerUser = []) {
    parent::__construct();
    $this->data = $data;
    $this->specificDataPerUser = $specificDataPerUser;
  }

  public function handle() {

    $affiliateRepo = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateRepository::class);
    $apiTypeManager = app()->make(\Odeo\Domains\Affiliate\Formatter\ApiTypeManager::class);

    foreach ($affiliateRepo->getAllNotifies() as $item) {
      foreach ($this->specificDataPerUser as $key => $output) {
        if (isset($output[$item->user_id])) {
          $this->data[$key] = $output[$item->user_id];
        } else if (isset($output['default'])) {
          $this->data[$key] = $output['default'];
        }
      }
      $this->data['signature'] = \hash("sha256", $item->user_id . $item->secret_key . $this->data['status']);

      if ($item->api_type != Affiliate::API_TYPE_JSON) {
        $request = ['body' => $apiTypeManager->setPath($item->api_type)
          ->constructNotify($this->data, Supplier::RC_OK_BROADCAST)];
      }
      else $request = ['json' => $this->data];

      $request['timeout'] = 10;

      dispatch(new AffiliateNotifyBroadcaster($item->api_type, $item->notify_url, $request));

    }

  }
}

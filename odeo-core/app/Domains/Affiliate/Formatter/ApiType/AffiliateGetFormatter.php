<?php

namespace Odeo\Domains\Affiliate\Formatter\ApiType;

use Odeo\Domains\Affiliate\Formatter\Contract\ApiTypeContract;
use Odeo\Domains\Constant\Supplier;

class AffiliateGetFormatter implements ApiTypeContract {

  public function deconstructRequest() {
    $typeManager = app()->make(\Odeo\Domains\Supply\Formatter\TypeManager::class);
    $formatter = $typeManager->setPath(Supplier::TYPE_HTTP_QUERY);
    return $formatter->getRequestData(app()->make(\Illuminate\Http\Request::class));
  }

  public function constructNotify($array, $code = Supplier::RC_OK) {
    $array = array_merge([
      'rc' => $code
    ], $array);

    return http_build_query($array);
  }

  public function constructResponse($array, $code = Supplier::RC_OK) {
    if (isset($array['message']))
      return is_array($array['message']) ? implode(';', $array['message']) : $array['message'];
    else {
      $newArray = [];
      foreach ($array as $key => $item) {
        $newArray[] = $key . '=' . $item;
      }
      return implode(';', $newArray);
    }
  }

}

<?php

namespace Odeo\Domains\Affiliate\Formatter\ApiType;

use Odeo\Domains\Affiliate\Formatter\Contract\ApiTypeContract;
use Odeo\Domains\Constant\Supplier;

class AffiliateXmlFormatter implements ApiTypeContract {

  public function deconstructRequest() {
    $typeManager = app()->make(\Odeo\Domains\Supply\Formatter\TypeManager::class);
    $formatter = $typeManager->setPath(Supplier::TYPE_XML);
    return $formatter->deconstructResponse($formatter->getRequestData(app()->make(\Illuminate\Http\Request::class)));
  }

  public function constructNotify($array, $code = Supplier::RC_OK) {
    return $this->constructResponse($array, $code);
  }

  public function constructResponse($array, $code = Supplier::RC_OK) {
    $array = array_merge([
      'rc' => $code
    ], $array);

    return arrayToXml($array, 'odeo')->asXML();
  }

}

<?php

namespace Odeo\Domains\Notification\Jobs;

use Odeo\Jobs\Job;

class SaveNotification extends Job {

  protected $messages, $notification, $push, $pushOnly;

  public function __construct($messages, $pushOnly = false) {
    parent::__construct();
    $this->notification = app()->make(\Odeo\Domains\Notification\Repository\NotificationRepository::class);
    $this->push = app()->make(\Odeo\Domains\Notification\Helper\PushSender::class);
    $this->messages = $messages;
    $this->pushOnly = $pushOnly;
  }

  private function convertKeysToCamelCase($array = []) {
    $arr = [];
    foreach ($array as $key => $value) {
      $key = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));
      if (is_array($value)) {
        $value = $this->convertKeysToCamelCase($value);
      }
      $arr[$key] = $value;
    }
    return $arr;
  }

  private function sendPush($notification) {
    $pushDesc = '';
    $type = null;

    $descriptions = json_decode($notification['description'], true);
    $endText = false;
    $additional = new \stdClass();

    foreach ($descriptions as $desc) {
      if (isset($desc['text']) && !$endText) {
        $pushDesc .= $desc['text'] . ' ';
      }
      if (isset($desc['layer'])) {
        $type = $desc;
        if (isset($desc['id'])) $additional->id = $desc['id'];

        $endText = true;
      }
    }

    $this->push->toUser($notification['user_id'], [
      'type' => $type['layer'],
      'title' => $notification['title'] ?? 'Odeo',
      'message' => trim($pushDesc),
      'additional' => json_encode($additional),
      'data' => $this->convertKeysToCamelCase(json_decode($notification['data']) ?? [])
    ]);
  }


  public function handle() {
    foreach ($this->messages as $data) {
      $data = (array)$data;
      $data["data"] = "";
      if (isset($data["description"]) && $desc = json_decode($data["description"], true)) {
        $data_p = [];
        $new_desc = [];
        foreach ($desc as $key => $item) {
          if ($item["type"] == "convert") {
            list($key, $item) = explode(":", $item["desc"], 2);
            $data_p[$key] = $item;
            if ($key == "team_commission_store_id" || $key == "sponsor_bonus_store_id" || $key == "sponsor_bonus_store_with_plan_id") {
              $this->notification->findLikeAndDelete($key, $item);
            }
          } else $new_desc[] = $item;
        }

        if (sizeof($data_p) > 0) {
          $data["description"] = json_encode($new_desc);
          $data["data"] = json_encode($data_p);
        }
      }

      if (!$this->pushOnly) $this->notification->save($data);
      $this->sendPush($data);
    }
  }
}

<?php

namespace Odeo\Domains\Notification\Model;

use Odeo\Domains\Core\Entity;

class Notification extends Entity {

  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
  protected $fillable = ['user_id', 'type', 'description', 'data', 'group', 'title'];

   /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
  public $timestamps = false;
  
  public static function boot() {
    parent::boot();

    static::creating(function($model) {
      $timestamp = $model->freshTimestamp();
      $model->setCreatedAt($timestamp);
    });
  }
}

<?php

namespace Odeo\Domains\Notification;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\NotificationType;

class AnnouncementRequester {

  public function __construct() {
    $this->announcement = app()->make(\Odeo\Domains\Notification\Repository\AnnouncementRepository::class);
  }
  
  public function create(PipelineListener $listener, $data) {
    $announcement = $this->announcement->getNew();

    $announcement->title = $data['title'] ?? '';
    $announcement->content = $data['content'];
    $announcement->start_date = $data['start_date'];
    $announcement->end_date = $data['end_date'];
    $announcement->priority = $data['priority'];
    $announcement->is_active = true;

    $this->announcement->save($announcement);

    return $listener->response(201);
  }

  public function delete(PipelineListener $listener, $data) {
    $announcement = $this->announcement->findById($data['announcement_id']);

    if ($announcement) {
      $announcement->delete();
    }

    return $listener->response(200);
  }
}

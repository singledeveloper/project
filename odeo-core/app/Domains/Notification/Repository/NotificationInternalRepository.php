<?php

namespace Odeo\Domains\Notification\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Notification\Model\NotificationInternal;

class NotificationInternalRepository extends Repository {

  public function __construct(NotificationInternal $notificationInternal) {
    $this->model = $notificationInternal;
  }

  public function getUnreads() {
    return $this->getCloneModel()
      ->where('is_read', false)
      ->orderBy('updated_at', 'asc')->get();
  }

  public function findByMessageType($messageType, $referenceId) {
    return $this->getCloneModel()
      ->where('message_type', $messageType)
      ->where('reference_id', $referenceId)
      ->where('is_read', false)->orderBy('id', 'desc')->first();
  }

  public function getAllProblemInventories($date = '') {
    $query = $this->getCloneModel()
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'notification_internals.reference_id')
      ->whereNotNull('reference_id');

    if ($date != '') $query = $query->where(\DB::raw('date(notification_internals.updated_at)'), $date);

    return $query->groupBy('message_type', 'pulsa_odeos.name')
      ->orderBy('message_type', 'asc')
      ->orderBy('pulsa_odeos.name', 'asc')
      ->select('message_type', 'pulsa_odeos.name', \DB::raw('sum(notification_internals.multiplier) as icount'))->get();
  }
}

<?php

namespace Odeo\Domains\Biller\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendPurchaseWarning extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    Mail::send('emails.pulsa_purchase_alert', [
      'order_id' => $this->data['order_id'],
      'payment_name' => $this->data['payment_name'],
      'item_name' => $this->data['item_name'],
      'user_id' => $this->data['user_id'],
      'user_name' => $this->data['user_name'],
      'user_email' => $this->data['user_email'],
      'total' => $currencyHelper->formatPrice($this->data['total'])
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('fraud@odeo.co.id')->subject('Purchase Warning - ' . time());
    });

  }

}



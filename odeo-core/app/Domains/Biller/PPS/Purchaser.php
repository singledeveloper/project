<?php

namespace Odeo\Domains\Biller\PPS;

use Odeo\Domains\Constant\BillerPPS;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::PPS);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $number = revertTelephone($this->currentSwitcher->number);

    $pulsaInventory = $this->currentSwitcher->currentInventory;
    $query = [
      'user' => BillerPPS::USER_ID,
      'produk' => $pulsaInventory->code,
      'mdn' => $number,
      'notrx' => $this->billerOrder->id,
      'signature' => \md5($number .
        $pulsaInventory->code .
        $this->billerOrder->id .
        BillerPPS::PASSWORD)
    ];

    $this->billerOrder->log_request = json_encode($query);

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ]
    ]);

    try {
      $response = $client->request('POST', BillerPPS::PROD_SERVER . 'evshop/Sell', [
        'form_params' => $query,
        "timeout" => 100
      ]);
      $this->billerOrder->log_response = $response->getBody()->getContents();

      if ($content = json_decode($this->billerOrder->log_response, true)) {
        if ($content['Status'] == '9' || $content['Status'] == '0')
          $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;
        else if ($content['Status'] == '1') $statusTransaction = SwitcherConfig::BILLER_FAIL;
        else $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
      }
      else {
        $this->timeoutMessage = 'error json not valid status: ' . $response->getStatusCode();
        $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
      }
    }
    catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
    }

    $this->billerOrder->status = $statusTransaction;

    return $this->finalize($listener);
  }
}
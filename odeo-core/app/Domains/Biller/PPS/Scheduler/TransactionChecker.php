<?php

namespace Odeo\Domains\Biller\PPS\Scheduler;

use Odeo\Domains\Constant\BillerPPS;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class TransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::PPS);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }
  
  public function check(PipelineListener $listener, $data) {
    $orders = $this->billerRepository->getPendingTransaction();

    foreach ($orders as $output) {
      $this->load($output);

      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);

      $response = $client->request('POST', BillerPPS::PROD_SERVER . 'evshop/StatusTrx', [
        'form_params' => [
          'user' => BillerPPS::USER_ID,
          'notrx' => $this->billerOrder->id,
          'signature' => \md5($this->billerOrder->id . BillerPPS::PASSWORD)
        ]
      ]);
      $this->billerOrder->log_transaction = $response->getBody()->getContents();

      if ($content = json_decode($this->billerOrder->log_transaction, true)) {
        if ($content['Status'] == '0') {
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;

          try {
            $temp = explode('no ref <', $content['Message']);
            $this->currentSwitcher->serial_number = trim(str_replace('>', '', $temp[1]));
          }
          catch (\Exception $e) {
            try {
              $temp = explode('SN <', $content['Message']);
              $temp = explode('>', $temp[1]);
              $this->currentSwitcher->serial_number = trim($temp[0]);
            }
            catch (\Exception $e) {}
          }
          $this->currentBiller->current_balance -= $this->currentSwitcher->current_base_price;
        }
        else if ($content['Status'] == '1') {
          if (BillerPPS::checkWrongNumberPattern($content['Message']))
            $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        }

        if (isset($content['ServerIDTrx'])) $this->currentRecon->biller_transaction_id = $content['ServerIDTrx'];
      }

      $this->currentRecon->notify = $this->billerOrder->log_transaction;
      $this->finalize($listener, false);

    }
  }
}

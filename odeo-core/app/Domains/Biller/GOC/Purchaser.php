<?php

namespace Odeo\Domains\Biller\GOC;

use Odeo\Domains\Constant\BillerGOC;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Purchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::UNIPIN);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    try {
      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ],
        'timeout' => 20
      ]);

      $notes = '-';
      $request = [
        'ProductCode' => $this->currentPulsaInventory->code,
        'MerchantId' => env('GOC_MID'),
        'MerchantTrxid' => $this->currentRecon->id,
        'notes' => $notes,
        'sign' => strtolower(hash('sha256', $this->currentPulsaInventory->code . env('GOC_MID') . $this->currentRecon->id . $notes . env('GOC_KEY')))
      ];
      $this->currentRecon->request = json_encode($request);

      $response = $client->request('POST', BillerGOC::SERVER . 'h2h/BuyCoupon', [
        'json' => $request
      ]);
      $response = json_decode($response->getBody()->getContents());

      /*
      -101 = error db
      -102 = invalid ProductCode
      -107 = MerchantTrxid already available
      -103 = insufficient balance
      -104 = out of stock
      -501 = required parameter is not supplied
      -502 = invalid sign
      */

      if (isset($response->resultCode)) {
        if ($response->resultCode == '100') {
          $this->currentRecon->status = SwitcherConfig::BILLER_SUCCESS;
          $this->currentRecon->biller_transaction_id = $response->OrderId;
          $this->currentSwitcher->serial_number = $response->data->CouponSerial . '/' . $response->data->CouponPin;
        }
        else if (in_array($response->resultCode, ['-102', '-107', '-103', '-104', '-501', '-502']))
          $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
        else $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
      }
      else $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;

      $this->currentRecon->response = json_encode($response);
    }
    catch (\Exception $e) {
      $this->currentRecon->response = 'Error: ' . $e->getMessage();
      $this->currentRecon->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    try {
      $response = $client->request('POST', BillerGOC::SERVER . 'h2h/GetDeposit', [
        'json' => [
          'MerchantId' => env('GOC_MID'),
          'sign' => hash('sha256', env('GOC_MID') . env('GOC_KEY'))
        ]
      ]);
      $response = json_decode($response->getBody()->getContents());
      if (isset($response->koin)) $this->currentBiller->current_balance = $response->koin;
    }
    catch(\Exception $e) {}

    return $this->finalize($listener);
  }
}
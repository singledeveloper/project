<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 5:25 PM
 */

namespace Odeo\Domains\Biller\Contract;

use Odeo\Domains\Core\PipelineListener;

interface ReplenishmentContract {

  public function request(PipelineListener $listener, $data);

  public function complete(PipelineListener $listener, $data);

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 9:41 PM
 */

namespace Odeo\Domains\Biller\Datacell\Crawler;

use Goutte\Client;
use Odeo\Domains\Constant\BillerDatacell;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaOperatorRepository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository;
use Symfony\Component\BrowserKit\Cookie;

class ProductCrawler {

  private $client, $codes = [];

  public function __construct() {

    $this->client = new Client();
    $this->client->setClient(new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'cookies' => true,
      'headers' => BillerDatacell::WEB_REPORT_HEADER
    ]));

    $this->pulsaOperator = app()->make(PulsaOperatorRepository::class);
    $this->pulsaInventories = app()->make(PulsaOdeoInventoryRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
  }

  public function crawl2() {

    $this->client->request('POST', BillerDatacell::WEB_REPORT_SYSTEM_URL . "verify.asp", [
      'form_params' => [
        "txtUsername" => BillerDatacell::WEB_USER_ID,
        "txtPassword" => BillerDatacell::WEB_TEMP_PASSWORD
      ]
    ]);

    $response = $this->client->request('POST', BillerDatacell::WEB_REPORT_SYSTEM_URL . "price_dealer.asp", [
      "txtDealerId" => "", "txtProductId" => "", "btnGet" => "GET RECORD"
    ]);

    dd($response->html());
  }

  public function crawl() {

    $vendorSwitcher = $this->vendorSwitchers->findById(SwitcherConfig::DATACELL);
    $add = json_decode($vendorSwitcher->additional_data);

    if (!isset($add->datacell_cookie_name)) exit("Please set Datacell's cookie name.\n");
    else if (!isset($add->datacell_cookie_value)) exit("Please set Datacell's cookie value.\n");

    $this->client->getCookieJar()->set(new Cookie($add->datacell_cookie_name, $add->datacell_cookie_value));

    $crawler = $this->client->request('POST', BillerDatacell::WEB_REPORT_SYSTEM_URL . "price_dealer.asp", [
      "txtDealerId" => "", "txtProductId" => "", "btnGet" => "GET RECORD"
    ]);

    $inventories = $this->pulsaInventories->findByVendorSwitcherId(SwitcherConfig::DATACELL);
    $listInventories = $mappings = [];
    foreach ($inventories as $item) {
      $codec = str_replace('.', '', $item->code);
      if (!isset($listInventories[$codec])) $listInventories[$codec] = [];
      $listInventories[$codec][] = $item;
      $mappings[$codec] = $item->code;
    }

    if (count($tr = $crawler->filter('table table tr')) > 0) {
      $i = 1;
      $tr->each(function ($node, $i) use ($listInventories, $mappings) {
        if (count($td = $node->filter('td')) > 0) {
          $td->each(function ($node, $i) use ($listInventories, &$data, $mappings) {
            switch ($i) {
              case 2:
                $data['code'] = preg_replace("/[^a-zA-Z0-9]/", "", trim($node->text()));
                break;
              case 3:
                if (isset($listInventories[$data['code']])) {
                  $this->codes[] = $mappings[$data['code']];
                  foreach ($listInventories[$data['code']] as $item) {
                    $item->base_price = preg_replace("/[^a-zA-Z0-9]/", "", $node->text());
                    $item->is_active = true;
                    $this->pulsaInventories->save($item);
                  }
                }
                break;
            }
          });
        }
      });

      $this->pulsaInventories->finalizeProducts(SwitcherConfig::DATACELL, $this->codes);
    }
    else {
      echo "Looks like there's nothing here.\n";
    }
  }

}
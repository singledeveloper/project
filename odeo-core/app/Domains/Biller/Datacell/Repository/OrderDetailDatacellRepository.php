<?php

namespace Odeo\Domains\Biller\Datacell\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Biller\Datacell\Model\OrderDetailDatacell;

class OrderDetailDatacellRepository extends BillerRepository {

  public function __construct(OrderDetailDatacell $orderDetailDatacell) {
    $this->setModel($orderDetailDatacell);
  }

  public function getPendingTransaction() {
    return $this->model->where('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }
}

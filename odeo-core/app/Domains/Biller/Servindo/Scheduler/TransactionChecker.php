<?php

namespace Odeo\Domains\Biller\Servindo\Scheduler;

use Odeo\Domains\Constant\BillerServindo;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class TransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::SERVINDO);
  }

  public function run() {
    $pipeline = new Pipeline;

    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }
  
  public function check(PipelineListener $listener, $data) {
    $orders = $this->billerRepository->getPendingTransaction();

    foreach ($orders as $output) {
      $this->load($output);

      $client = new \GuzzleHttp\Client([
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false
        ]
      ]);

      $response = $client->request('GET', BillerServindo::getServer(), [
        "query" => json_decode($this->billerOrder->log_request, true),
        "timeout" => 100
      ]);
      $this->billerOrder->log_response_repeat = BillerServindo::hardFixRes($response->getBody()->getContents());

      if ($content = json_decode($this->billerOrder->log_response_repeat)) {
        $isValidSN = isset($content->sn) && trim($content->sn) != '-' && trim($content->sn) != '';

        if ($content->status == '1') {
          if ($isValidSN) $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
        }
        else if ($content->status == '5') {
          if ($isValidSN) $this->billerOrder->status = SwitcherConfig::BILLER_DUPLICATE_SUCCESS;
        }
        else if ($content->status == '2' || $content->status == '7') $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
        else if (strpos($content->message, 'Gagal Server Off Cut') === false) {
          $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
          if (BillerServindo::checkWrongNumberPattern($content->message))
            $this->currentSwitcher->status = SwitcherConfig::SWITCHER_FAIL_WRONG_NUMBER;
        }

        if (in_array($this->billerOrder->status, [
          SwitcherConfig::BILLER_SUCCESS,
          SwitcherConfig::BILLER_DUPLICATE_SUCCESS
        ]) && $this->currentSwitcher->serial_number == null) {
          if ($this->currentSwitcher->operator_id == Pulsa::OPERATOR_PLN) {
            $this->currentSwitcher->serial_number = wordwrap(str_replace('-', '', $content->sn), 4, '-', true);
          }
          else $this->currentSwitcher->serial_number = $content->sn;

          if (isset($content->harga) && trim($content->harga) != '0') $this->currentSwitcher->current_base_price = $content->harga;
          if (isset($content->saldo)) $this->currentBiller->current_balance = $content->saldo;
        }

        if (isset($content->tn) && $content->tn != '0') $this->currentRecon->biller_transaction_id = trim($content->tn);
      }
      else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;

      $this->finalize($listener, false);

    }
  }
}

<?php

namespace Odeo\Domains\Biller\Servindo\Inquirer;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\BillerServindo;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;

class PlnInquirer {

  public function inquiry(PipelineListener $listener, $data) {

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 90
    ]);

    $inquiry = $client->request('GET', BillerServindo::getServer(), ["query" => [
      'regid' => time(),
      'userid' => BillerServindo::USER_ID,
      'passwd' => BillerServindo::PASSWORD,
      'msisd' => $data['number'],
      'denom' => 'CIPLN20'
    ]]);

    $inquiry = json_decode($inquiry->getBody()->getContents());

    if (isset($inquiry->status) && $inquiry->status == '1') {
      return $listener->response(200, ['inquiry' => [
        'number' => $data['number'],
        'subscriber_id' => '-',
        'name' => $inquiry->namapelanggan,
        'tariff' => '-',
        'power' => '-'
      ]]);
    }
    else if (isset($data['bypass'])) {
      return $listener->response(200, ['inquiry' => [
        'number' => $data['number'],
        'subscriber_id' => '-',
        'name' => '-',
        'tariff' => '-',
        'power' => '-'
      ]]);
    }
    return $listener->response(400, 'Invalid PLN number.');
  }

}
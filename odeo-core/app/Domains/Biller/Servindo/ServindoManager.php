<?php

namespace Odeo\Domains\Biller\Servindo;

use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\Contract\BillerContract;

class ServindoManager implements BillerContract {

  private $purchaser, $inventoryValidator, $switcherOrder;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    $switcher = $this->switcherOrder->findById($data["order_detail_pulsa_switcher_id"]);

    switch($switcher->currentInventory->pulsa->operator_id) {
      case Pulsa::OPERATOR_PLN:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Servindo\Purchaser\PlnPurchaser::class);
        break;
      default:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Servindo\Purchaser\PulsaPurchaser::class);
        break;
    }

    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

}
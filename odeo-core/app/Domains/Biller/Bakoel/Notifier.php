<?php

namespace Odeo\Domains\Biller\Bakoel;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class Notifier extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::BAKOEL);
    $this->request = app()->make(\Illuminate\Http\Request::class);
  }

  public function notify(PipelineListener $listener, $data) {
    if (isProduction() && !in_array(getClientIP(), BillerBakoel::IP_WHITELIST)) {
      echo 'IP MISMATCH';
    }
    else {
      try {
        if (!isset($data['msg']) || (isset($data['msg']) && $data['msg'] != 'REVERSAL'))
          echo 'UNKNOWN OPERATION';
        else if (!isset($data['trxID']) || (isset($data['trxID']) && $data['trxID'] == ''))
          echo 'EMPTY REFERENCE NUMBER';
        else if (!isset($data['msisdn']) || (isset($data['msisdn']) && $data['msisdn'] == ''))
          echo 'EMPTY PHONE NUMBER';
        else if (!$transaction = $this->billerRepository->findById($data['trxID']))
          echo 'UNKNOWN REFERENCE NUMBER';
        else {
          $this->load($transaction);

          if ($this->billerOrder->status != SwitcherConfig::BILLER_FAIL) {
            $this->billerOrder->log_refund = json_encode($data);
            $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;

            if ($this->currentSwitcher->vendor_switcher_id == SwitcherConfig::BAKOEL) {
              $this->currentSwitcher->status = SwitcherConfig::SWITCHER_IN_QUEUE;
            }

            $this->currentRecon->notify = $this->billerOrder->log_refund;
            $this->finalize($listener, false);
          }

          echo 'REVERSAL DONE';
        }
      }
      catch (\Exception $e) {
        echo 'REVERSAL FAILED';
      }
    }
  }

}

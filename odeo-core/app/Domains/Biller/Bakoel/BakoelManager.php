<?php

namespace Odeo\Domains\Biller\Bakoel;

use Odeo\Domains\Biller\Bakoel\Inquirer\MultiFinanceInquirer;
use Odeo\Domains\Biller\Bakoel\Inquirer\PDAMInquirer;
use Odeo\Domains\Biller\Bakoel\Inquirer\PGNInquirer;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Biller\Bakoel\Inquirer\BPJSInquirer;
use Odeo\Domains\Biller\Bakoel\Inquirer\PlnInquirer;
use Odeo\Domains\Biller\Bakoel\Inquirer\PulsaPostpaidInquirer;
use Odeo\Domains\Biller\Contract\BillerContract;

class BakoelManager implements BillerContract {

  private $purchaser, $inventoryValidator, $repository,
    $currencyHelper, $switcherOrder;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->repository = app()->make(\Odeo\Domains\Biller\Bakoel\Repository\OrderDetailBakoelRepository::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    $switcher = $this->switcherOrder->findById($data["order_detail_pulsa_switcher_id"]);

    switch($switcher->currentInventory->pulsa->operator_id) {
      case Pulsa::OPERATOR_PLN:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\PlnPurchaser::class);
        break;
      default:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\PulsaPurchaser::class);
        break;
    }

    return $this->purchaser->purchase($listener, $data);
  }

  public function purchasePostpaid(PipelineListener $listener, $data) {
    $switcher = $this->switcherOrder->findById($data["order_detail_pulsa_switcher_id"]);

    switch($switcher->currentInventory->pulsa->service_detail_id) {
      case ServiceDetail::PLN_POSTPAID_ODEO:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\PlnPostpaidPurchaser::class);
        break;
      case ServiceDetail::PULSA_POSTPAID_ODEO:
      case ServiceDetail::BROADBAND_ODEO:
      case ServiceDetail::LANDLINE_ODEO:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\PulsaPostpaidPurchaser::class);
        break;
      case ServiceDetail::BPJS_KES_ODEO:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\BPJSKesPurchaser::class);
        break;
      case ServiceDetail::MULTI_FINANCE_ODEO:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\MultiFinancePurchaser::class);
        break;
      case ServiceDetail::PDAM_ODEO:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\PDAMPurchaser::class);
        break;
      case ServiceDetail::PGN_ODEO:
        $this->purchaser = app()->make(\Odeo\Domains\Biller\Bakoel\Purchaser\PGNPurchaser::class);
        break;
    }

    return $this->purchaser->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validatePostpaidInventory($listener, $data);
  }

  public function inquiryPostpaid(PipelineListener $listener, $data) {
    switch($data['service_detail_id']) {
      case ServiceDetail::PLN_POSTPAID_ODEO:
        return app()->make(PlnInquirer::class)->inquiryPostpaid($listener, $data);
      case ServiceDetail::PULSA_POSTPAID_ODEO:
      case ServiceDetail::BROADBAND_ODEO:
      case ServiceDetail::LANDLINE_ODEO:
        return app()->make(PulsaPostpaidInquirer::class)->inquiry($listener, $data);
      case ServiceDetail::BPJS_KES_ODEO:
        return app()->make(BPJSInquirer::class)->inquiryKes($listener, $data);
      case ServiceDetail::BPJS_TK_ODEO:
        return app()->make(BPJSInquirer::class)->inquiryKtg($listener, $data);
      case ServiceDetail::MULTI_FINANCE_ODEO:
        return app()->make(MultiFinanceInquirer::class)->inquiry($listener, $data);
      case ServiceDetail::PDAM_ODEO:
        return app()->make(PDAMInquirer::class)->inquiry($listener, $data);
      case ServiceDetail::PGN_ODEO:
        return app()->make(PGNInquirer::class)->inquiry($listener, $data);
    }
  }
}
<?php

namespace Odeo\Domains\Biller\Bakoel\Repository;

use Carbon\Carbon;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;
use Odeo\Domains\Biller\Bakoel\Model\OrderDetailBakoel;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\Pulsa;

class OrderDetailBakoelRepository extends BillerRepository {

  public function __construct(OrderDetailBakoel $orderDetailBakoel) {
    $this->setModel($orderDetailBakoel);
  }

  public function findByTransactionId($trxId) {
    return $this->model->where('log_response', 'like', '%"trxID":"' . $trxId . '"%')->first();
  }

  public function getEmptySNAndPendingTransaction() {
    return $this->model->whereHas('switcher', function ($query) {
      $query->whereNull('serial_number')->whereHas('currentInventory', function ($query2) {
        $query2->whereHas('pulsa', function ($query3) {
          $query3->where('operator_id', '<>', Pulsa::OPERATOR_PLN)->whereNotNull('operator_id');
        });
      });
    })->where('status', SwitcherConfig::BILLER_SUCCESS)
      ->orWhere('status', SwitcherConfig::BILLER_IN_QUEUE)->get();
  }

}

<?php

namespace Odeo\Domains\Biller\Bakoel\Inquirer;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Biller\Inquirer;

class MultiFinanceInquirer {

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->inquirer = app()->make(\Odeo\Domains\Supply\Biller\Inquirer::class);
  }

  public function inquiry(PipelineListener $listener, $data) {

    if (isset($data['inventory_code'])) $code = $data['inventory_code'];
    else {
      $inventory = $this->pulsaInventories->findById($data['inventory_id']);
      $code = $inventory->code;
    }

    $data['number'] = isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'];
    $request = [
      'productCode' => $code,
      'idPel' => $data['number'],
      'idPel2' => '',
      'miscData' => ''
    ];

    try {
      $inquiry = BillerBakoel::setClient(BillerBakoel::PP_SERVER)->call(BillerBakoel::CMD_PP_INQUIRY, $request);

      if (isset($inquiry['responseCode'])) {

        if ($inquiry['responseCode'] == '00') {
          $name = 'MULTIFINANCE';
          if (isset($inventory)) $name .= ' ' . strtoupper($inventory->pulsa->name);

          $platform = '-';
          if (isset($inquiry['platform'])) {
            if ($inquiry['platform'] == 'S') $platform = PostpaidType::MULTIFINANCE_TYPE_SYARIAH;
            else if ($inquiry['platform'] == 'K') $platform = PostpaidType::MULTIFINANCE_TYPE_CONVENTIONAL;
          }

          $result[Supplier::SF_PARAM_MF_NAME] = $name;
          $result[Supplier::SF_PARAM_OWNER_NAME] = $inquiry['nama'];
          $result[Supplier::SF_PARAM_SN] = $inquiry['refID'];
          $result[Supplier::SF_PARAM_POSTPAID_PRICE_TOTAL] = $inquiry['totalBayar'];
          $result[Supplier::SF_PARAM_MF_CREDIT_CURRENT] = $inquiry['angsuranKe'];
          if (isset($inquiry['totalAngsuran'])) $result[Supplier::SF_PARAM_MF_CREDIT_TOTAL] = $inquiry['totalAngsuran'];
          if (isset($inquiry['jatuhTempo'])) $result[Supplier::SF_PARAM_MF_CREDIT_DUE_DATE] = $inquiry['jatuhTempo'];
          $result[Supplier::SF_PARAM_MF_PLATFORM] = $platform;
          $result[Supplier::SF_PARAM_POSTPAID_PRICE] = isset($inquiry['angsuran']) ? $inquiry['angsuran'] : $inquiry['tagihan'];
          if (isset($inquiry['biayaTagih'])) $result[Supplier::SF_PARAM_MF_COLL_FEE] = $inquiry['biayaTagih'];
          if (isset($inquiry['denda'])) $result[Supplier::SF_PARAM_POSTPAID_FINE] = $inquiry['denda'];
          $result[Supplier::SF_PARAM_POSTPAID_ADMIN] = $inquiry['admin'];
          $billerStatus = SwitcherConfig::BILLER_SUCCESS;
          $data['result'] = json_encode($result);
        }
        else {
          list($billerStatus, $data['error_message']) = BillerBakoel::translatePostpaidResponse($inquiry['responseCode'],
            isset($inquiry['message']) ? $inquiry['message'] : '');
        }

        $data['response'] = json_encode($inquiry);
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
      else {
        $data['error_message'] = Inquirer::DEFAULT_ERROR;
        $data['status'] = $billerStatus ?? SwitcherConfig::BILLER_FAIL;
      }
    }
    catch(\Exception $e) {
      $data['error_message'] = 'Error timeout. Please try again later.';
      $data['status'] =  SwitcherConfig::BILLER_FAIL;
    }

    $data['request'] = json_encode($request);

    return $this->inquirer->saveManually($listener, $data);
  }

}
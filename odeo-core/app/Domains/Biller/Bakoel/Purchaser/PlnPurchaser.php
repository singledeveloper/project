<?php

namespace Odeo\Domains\Biller\Bakoel\Purchaser;

use Odeo\Domains\Constant\BillerBakoel;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class PlnPurchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::BAKOEL);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    try {
      list($productCode, $denom) = explode(".", $this->currentSwitcher->currentInventory->code);
      $inquiry = BillerBakoel::getPlnPrepaidInquiry($this->currentSwitcher->number, $denom);
      $response['inquiry'] = $inquiry;
      if (count($inquiry) > 0) {
        $request = [
          'productCode' => $productCode,
          'refID' => $inquiry['refID'],
          'nominal' => intval(str_replace('U', '', $denom) . '000') + $inquiry['data']['admin'],
          'miscData' => (strpos($denom, 'U') !== false) ? '1' : ''
        ];
        $payment = BillerBakoel::setClient(BillerBakoel::PP_SERVER)->call(BillerBakoel::CMD_PP_PAYMENT, $request);
        $response['payment'] = $payment;
        if (isset($payment['responseCode']) && $payment['responseCode'] == '00') {
          $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
          $tokenNumber = wordwrap($payment['data']['tokenNumber'], 4, '-', true);
          $this->currentRecon->biller_transaction_id = $payment['data']['ref'];
        }
        else if (isset($payment['responseCode']) && $payment['responseCode'] == '9983') {
          $advice = BillerBakoel::setClient(BillerBakoel::PP_SERVER)->call(BillerBakoel::CMD_PP_OPTION, [
            'cmd' => 'manualAdvicePrepaid',
            'data' => json_encode([
              'productCode' => $productCode,
              'hashID' => $payment['manualAdviceHashID']
            ])
          ]);
          $this->billerOrder->log_refund = json_encode($advice);
          if (isset($advice['responseCode']) && $advice['responseCode'] == '00') {
            $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
            $tokenNumber = wordwrap($advice['data']['tokenNumber'], 4, '-', true);
            $this->currentRecon->biller_transaction_id = $advice['data']['ref'];
          }
          else $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
        }
        else $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
      }
      else {
        $request = $inquiry;
        $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;
      }

      $this->billerOrder->log_request = json_encode($request);
      $this->billerOrder->log_response = json_encode($response);
    }
    catch (\Exception $e) {
      $this->billerOrder->log_response = 'Error: ' . $e->getMessage();
      $this->billerOrder->status = SwitcherConfig::BILLER_TIMEOUT;
    }

    if (isset($tokenNumber)) $this->currentSwitcher->serial_number = $tokenNumber;

    try {
      $info = BillerBakoel::setClient(BillerBakoel::PULSA_SERVER)->call(BillerBakoel::CMD_MITRA_INFO);
      $this->currentBiller->current_balance = $info['quota'];
    }
    catch(\Exception $e) {}

    return $this->finalize($listener);
  }
}
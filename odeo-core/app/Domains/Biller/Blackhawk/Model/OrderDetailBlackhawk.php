<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/19/17
 * Time: 6:02 PM
 */

namespace Odeo\Domains\Biller\Blackhawk\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\OrderDetailPulsaSwitcher;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Model\PulsaOdeoInventory;

class OrderDetailBlackhawk extends Entity {

  public function switcher() {
    return $this->belongsTo(OrderDetailPulsaSwitcher::class, 'switcher_reference_id');
  }

}
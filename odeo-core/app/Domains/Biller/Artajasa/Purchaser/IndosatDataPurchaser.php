<?php

namespace Odeo\Domains\Biller\Artajasa\Purchaser;

use Odeo\Domains\Constant\BillerArtajasa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Biller\BillerManager;

class IndosatDataPurchaser extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::ARTAJASA_NEW);
  }

  public function purchase(PipelineListener $listener, $data) {

    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 20,
      'headers' => [
        'Pass-Key' => env('ARTAJASA_BILLER_PASS_KEY', ''),
        'Client-Id' => env('ARTAJASA_BILLER_CLIENT_ID', '')
      ]
    ]);

    $number = revertTelephone($this->currentSwitcher->number);
    //$authKey = '';
    $amount = '';
    $amountDesc = '';
    $packetAmount = '';
    $acquireId = env('ARTAJASA_BILLER_ACQUIRE_ID');

    try {
      $dateISO = date('Ymd\THis+07');
      $stanId = BillerArtajasa::generateSTAN();
      $request = [
        'billerId' => BillerArtajasa::INDOSAT_DATA_BILLER_ID,
        'signature' => strtoupper(sha1($stanId . $this->currentOrder->id .
          $dateISO . $number . $number . $acquireId . env('ARTAJASA_BILLER_SECRET_KEY', ''))),
        'transId' => [
          'STAN' => $stanId,
          'RRN' => (string)$this->currentOrder->id,
          'Timestamp' => $dateISO,
          'termId' => $number,
          'acquireId' => $acquireId
        ],
        'transData' => [
          'idCustomer' => $number,
          'code' => $this->currentPulsaInventory->code
        ]
      ];
      $this->currentRecon->request = json_encode($request);

      $response = $client->request('POST', BillerArtajasa::INDOSAT_JSON_SERVER . 'api/inquiry', [
        'json' => $request
      ]);
      $response = json_decode($response->getBody()->getContents());

      if (isset($response->transId) && isset($response->transId->respCode)) {
        if ($response->transId->respCode == '00') {
          //$authKey = $response->transId->authKey;
          $amount = $response->transId->Amount;
          $amountDesc = $response->transData->desc;
          $packetAmount = $response->transData->packetAmt;
          //$this->currentSwitcher->current_base_price = $response->transData->packetAmt;
        }
        else $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
      }
      else $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;

      $this->currentRecon->response = json_encode($response);
    }
    catch (\Exception $e) {
      $this->currentRecon->response = 'Error: ' . $e->getMessage() . (isset($response) ? (', res: ' . json_encode($response)) : '');
      $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
    }

    if ($amount != '') {
      sleep(1);
      try {
        $dateISO = date('Ymd\THis+07');
        $stanId = BillerArtajasa::generateSTAN();
        $request = [
          'billerId' => BillerArtajasa::INDOSAT_DATA_BILLER_ID,
          'signature' => strtoupper(sha1($stanId . $this->currentRecon->id .
            $dateISO . $number . $number . $acquireId . env('ARTAJASA_BILLER_SECRET_KEY', ''))),
          'transId' => [
            'STAN' => $stanId,
            'RRN' => (string)$this->currentRecon->id,
            'Timestamp' => $dateISO,
            'termId' => $number,
            'acquireId' => $acquireId,
            //'authKey' => $authKey,
            'Amount' => $amount,
            //'respCode' => '00'
          ],
          'transData' => [
            'idCustomer' => $number,
            'code' => $this->currentPulsaInventory->code,
            'desc' => $amountDesc,
            'packetAmt' => $packetAmount
          ]
        ];
        $this->currentRecon->request = json_encode($request);

        $response = $client->request('POST', BillerArtajasa::INDOSAT_JSON_SERVER . 'api/payment', [
          'json' => $request
        ]);
        $response = json_decode($response->getBody()->getContents());

        if (isset($response->transId) && isset($response->transId->respCode)) {
          if ($response->transId->respCode == '00') {
            $this->currentRecon->status = SwitcherConfig::BILLER_SUCCESS;
            $this->currentSwitcher->serial_number = $response->transData->voucherSN;
            if (isset($response->transData->wdwPeriodDate))
              $this->currentSwitcher->serial_number.= '/' . $response->transData->wdwPeriodDate;
          }
          else if (in_array($response->transId->respCode, ['68']))
            $this->currentRecon->status = SwitcherConfig::BILLER_IN_QUEUE;
          else $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
        }
        else $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;

        $this->currentRecon->response = json_encode($response);
      }
      catch (\Exception $e) {
        $this->currentRecon->response = 'Error: ' . $e->getMessage() . (isset($response) ? (', res: ' . json_encode($response)) : '');
        $this->currentRecon->status = SwitcherConfig::BILLER_TIMEOUT;
      }
    }

    return $this->finalize($listener);
  }
}
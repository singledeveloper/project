<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/15/17
 * Time: 4:04 PM
 */

namespace Odeo\Domains\Biller\JavaH2H;

use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class InventoryValidator {

  private $pulsaInventories, $marginFormatter;

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->marginFormatter = app()->make(\Odeo\Domains\Inventory\Helper\MarginFormatter::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function validateInventory(PipelineListener $listener, $data) {

    if ($inventory = $this->pulsaInventories->findById($data['inventory_id'])) {
      $pulsa = $inventory->pulsa;
      $formatted = $this->marginFormatter->formatMargin($pulsa->price, $data);

      return $listener->response(200, array_merge($formatted, [
        'inventory_id' => $inventory->id,
        'name' => $pulsa->name
      ]));
    }
    else return $listener->response(400, trans('pulsa.cant_purchase_no_backup'));
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/18/17
 * Time: 9:51 PM
 */

namespace Odeo\Domains\Biller\JavaH2H;


use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerJavaH2H;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class Purchaser extends BillerManager {
  public function __construct() {
    parent::__construct(SwitcherConfig::JAVAH2H);
  }

  public function purchase(PipelineListener $listener, $data) {
    if (!$this->initiate($listener, $data)) return $listener->response(200);

    $number = revertTelephone($this->currentSwitcher->number);

    if ($this->currentSwitcher->operator_id == Pulsa::OPERATOR_PLN) {
      $query = [
        'inquiry' => BillerJavaH2H::INQUIRY_PLN,
        'code' => $this->currentSwitcher->currentInventory->code,
        'idcust' => $number,
        'trxid_api' => $this->billerOrder->id
      ];
    } else {
      if ($this->currentSwitcher->operator_id == Pulsa::OPERATOR_BOLT) {
        $number = ltrim($number, '0');
        $this->currentSwitcher->number = $number;
      }
      $query = [
        'inquiry' => BillerJavaH2H::INQUIRY_PULSA,
        'code' => $this->currentSwitcher->currentInventory->code,
        'phone' => $number,
        'trxid_api' => $this->billerOrder->id
      ];
    }

    $this->billerOrder->log_request = json_encode($query);

    $client = new \GuzzleHttp\Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'timeout' => 90
    ]);

    try {
      $response = $client->request('POST', BillerJavaH2H::PATH, [
        "form_params" => $query,
        "headers" => [
          "h2h-userid" => BillerJavaH2H::USER_ID,
          "h2h-key" => BillerJavaH2H::PROD_KEY,
          "h2h-secret" => BillerJavaH2H::PROD_SECRET
        ],
        "timeout" => 100
      ]);

      $this->billerOrder->log_response = $response->getBody()->getContents();
      if ($content = json_decode($this->billerOrder->log_response)) {
        if ($content->result == "success") {
          $statusTransaction = SwitcherConfig::BILLER_IN_QUEUE;
        } else {
          $statusTransaction = SwitcherConfig::BILLER_FAIL;
        }
      } else $statusTransaction = SwitcherConfig::BILLER_SUSPECT;
    } catch (\Exception $e) {
      $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
      $statusTransaction = SwitcherConfig::BILLER_TIMEOUT;
    }

    $this->billerOrder->status = $statusTransaction;

    return $this->finalize($listener);

  }
}
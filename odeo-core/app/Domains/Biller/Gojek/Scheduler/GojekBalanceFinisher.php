<?php

namespace Odeo\Domains\Biller\Gojek\Scheduler;

use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class GojekBalanceFinisher {

  private $gojekUsers;

  public function __construct() {
    $this->gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
  }

  public function run() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }

  public function check(PipelineListener $listener, $data) {
    if ($lowBalanceUser = $this->gojekUsers->findLowBalanceWithMaxMonthCash(5000)) {
      if ($receiverUser = $this->gojekUsers->findDumpAccount($lowBalanceUser->id)) {
        $result = app()->make(\Odeo\Domains\Biller\Gojek\Purchaser::class)->purchase($listener, [
          'gojek_user_id' => $lowBalanceUser->id,
          'amount' => $lowBalanceUser->current_balance,
          'msisdn' => revertTelephone($receiverUser->telephone)
        ]);

        if (isset($result['status']) && $result['status'] == SwitcherConfig::BILLER_SUCCESS) {
          $receiverUser->monthly_cash_in += $lowBalanceUser->current_balance;
          $this->gojekUsers->save($receiverUser);
        }
      }
    }
  }

}

<?php

namespace Odeo\Domains\Biller\Gojek\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Gojek;

class GojekOnHoldChecker {

  private $gojekUsers, $redis;

  public function __construct() {
    $this->gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
    $this->redis = Redis::connection();
  }

  public function run() {
    if (!$this->redis->hget(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_HOLD_FLAG)) return;

    $isDeleteHold = true;
    foreach ($this->gojekUsers->getOnHold() as $item) {
      if (time() - strtotime($item->updated_at) > 5 * 60) {
        $item->status = Gojek::CONNECTED;
        $this->gojekUsers->save($item);
      }
      else $isDeleteHold = false;
    }

    if ($isDeleteHold) $this->redis->hdel(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_HOLD_FLAG);
  }

}

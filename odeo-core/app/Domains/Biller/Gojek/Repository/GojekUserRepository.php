<?php

namespace Odeo\Domains\Biller\Gojek\Repository;

use Carbon\Carbon;
use Odeo\Domains\Biller\Gojek\Model\GojekUser;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Core\Repository;

class GojekUserRepository extends Repository {

  const SECOND_PAR = 2;

  public function __construct(GojekUser $gojekUser) {
    $this->model = $gojekUser;
  }

  public function findByTelephone($telephone) {
    return $this->getCloneModel()->where('telephone', $telephone)->where('is_active', true)->first();
  }

  public function findByIdLocked($gojekUserId) {
    return $this->getCloneModel()->lockForUpdate()->where('id', $gojekUserId)->first();
  }

  public function findBiggestBalance($storeId, $amount, $ids = []) {
    $query = $this->getCloneModel()->lockForUpdate()->where('is_active', true)
      ->where('status', Gojek::CONNECTED);
    if ($storeId == null) $query = $query->whereNull('store_id');
    else $query = $query->where('store_id', $storeId);

    if (sizeof($ids) > 0) $query = $query->whereNotIn('id', $ids);

    return $query->where('current_balance', '>=', $amount)
      ->where('updated_at', '<', Carbon::now()->subSeconds(self::SECOND_PAR)->toDateTimeString())
      ->orderBy('updated_at', 'asc')
      ->first();
  }

  public function findLowBalanceWithMaxMonthCash($amount) {
    return $this->getCloneModel()->where('is_active', true)
      ->where('status', Gojek::CONNECTED)
      ->where('current_balance', '>', 0)
      ->where('current_balance', '<', $amount)
      ->where('monthly_cash_in', Gojek::MAX_MONTH_CASH)
      ->first();
  }

  public function findDumpAccount($storeId) {
    $query = $this->getCloneModel()->where('is_active', true)
      ->where('status', Gojek::CONNECTED);
    if ($storeId == null) $query = $query->whereNull('store_id');
    else $query = $query->where('store_id', $storeId);

    return $query->where('monthly_cash_in', '<', Gojek::MAX_MONTH_CASH)
      ->where('is_dump', true)->first();
  }

  public function findByStoreId($storeId) {
    return $this->getCloneModel()->where('store_id', $storeId)->where('is_active', true)->first();
  }

  public function getTotalBalance($storeId) {
    $query = $this->getCloneModel()->where('is_active', true);
    if ($storeId == null) $query = $query->whereNull('store_id')->where('status', Gojek::CONNECTED);
    else $query = $query->where('store_id', $storeId);

    return $query->select(\DB::raw('sum(current_balance) as total_balance'))->first();
  }

  public function getMonitoringAccounts() {
    return $this->getCloneModel()->where('is_active', true)
      ->whereNull('store_id')->where(function($subquery){
      $subquery->where('current_balance', '>', 0)
        ->orWhere('monthly_cash_in', '<', Gojek::MAX_MONTH_CASH);
    })->orderBy('monthly_cash_in', 'asc')->get();
  }

  public function getAllSuppliers() {
    return $this->getCloneModel()->where('is_active', true)
      ->where('status', Gojek::CONNECTED)
      ->whereNotNull('store_id')->get();
  }

  public function getAccounts($storeId = '') {
    $query = $this->getCloneModel()->where('is_active', true);
    if ($storeId != '') $query = $query->where('store_id', $storeId);
    else $query = $query->with(['store']);

    $filters = $this->getFilters();

    if (!isset($filters['sort_by'])) {
      $filters['sort_by'] = 'id';
    }
    if (!isset($filters['sort_type'])) {
      $filters['sort_type'] = 'desc';
    }
    if (isset($filters['search']) && is_array($filters['search']))
      $filters = array_merge($filters['search'], [
        'sort_by' => $filters['sort_by'],
        'sort_type' => $filters['sort_type']
      ]);

    if (isset($filters['telephone'])) {
      $query = $query->whereIn('telephone', [
        purifyTelephone(trim($filters['telephone'])),
        trim($filters['telephone'])
      ]);
    }

    if (isset($filters['status']) && $filters['status'] != '0')
      $query = $query->where('status', $filters['status']);

    if ($storeId == '' && isset($filters['only_admin_store']))
      $query = $query->whereNull('store_id');

    return $this->getResult($query
      ->orderBy('monthly_cash_in', 'asc')
      ->orderBy($filters['sort_by'], $filters['sort_type']));
  }

  public function getAccountTPS($nominal = 100000) {
    return $this->getCloneModel()->where('current_balance', '>=', $nominal)
      ->where('is_active', true)
      ->where('status', Gojek::CONNECTED)
      ->select('store_id', \DB::raw('(count(id)/' . (self::SECOND_PAR+0.5) . ') as tps'), \DB::raw('sum(current_balance) as total'))
      ->groupBy('store_id')->get();
  }

  public function getOnHold() {
    return $this->getCloneModel()->where('status', Gojek::ON_HOLD)
      ->where('is_active', true)
      ->get();
  }

}

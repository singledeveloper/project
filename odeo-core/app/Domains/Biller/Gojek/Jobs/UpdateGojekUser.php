<?php

namespace Odeo\Domains\Biller\Gojek\Jobs;

use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Jobs\Job;

class UpdateGojekUser extends Job  {

  private $id, $data;

  public function __construct($id, $data) {
    parent::__construct();
    $this->id = $id;
    $this->data = $data;
  }

  public function handle() {

    $gojekUsers = app()->make(\Odeo\Domains\Biller\Gojek\Repository\GojekUserRepository::class);
    if ($gojekUser = $gojekUsers->findById($this->id)) {
      if (isset($this->data['unique_id'])) $gojekUser->unique_id = $this->data['unique_id'];
      if (isset($this->data['token'])) $gojekUser->token = $this->data['token'] != '' ? $this->data['token'] : null;
      if (isset($this->data['refresh_token'])) $gojekUser->refresh_token = $this->data['refresh_token'] != '' ? $this->data['refresh_token'] : null;
      if (isset($this->data['last_error_message'])) $gojekUser->last_error_message = $this->data['last_error_message'];
      if (isset($this->data['proxy_session_name'])) $gojekUser->proxy_session_name = $this->data['proxy_session_name'];
      if (isset($this->data['status'])) {
        if ($this->data['status'] == Gojek::PENDING_OTP && $gojekUser->store_id != null) {
          $userStoreRepo = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
          if ($ownerStore = $userStoreRepo->findSingleOwner($gojekUser->store_id)) {
            $notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
            $notification->setup($ownerStore->user_id, NotificationType::WARNING, NotificationGroup::TRANSACTION);
            $notification->gojekLogout(revertTelephone($gojekUser->telephone));
            dispatch($notification->queue());
          }
        }
        if ($this->data['status'] == Gojek::ON_HOLD) {
          $redis = \Illuminate\Support\Facades\Redis::connection();
          $redis->hset(Gojek::REDIS_GOJEK, Gojek::REDIS_KEY_HOLD_FLAG, 1);
        }
        else if ($this->data['status'] != Gojek::CONNECTED) {
          $internalNoticer = app()->make(\Odeo\Domains\Notification\Helper\InternalNoticer::class);
          $internalNoticer->saveMessage('Akun GoPay ' . revertTelephone($gojekUser->telephone) . ' terputus dari sistem ODEO.');
        }
        $gojekUser->status = $this->data['status'];
      }

      $gojekUsers->save($gojekUser);
    }

  }

}



<?php

namespace Odeo\Domains\Biller\Gojek\Jobs;

use Odeo\Jobs\Job;

class UpdatePurchaserToAdminNote extends Job  {

  private $switcherId, $number;

  public function __construct($switcherId, $number) {
    parent::__construct();
    $this->switcherId = $switcherId;
    $this->number = $number;
  }

  public function handle() {

    $switcherRepo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    if ($switcher = $switcherRepo->findById($this->switcherId)) {
      $switcher->admin_note = 'Using ' . $this->number;
      $switcherRepo->save($switcher);
    }

  }

}



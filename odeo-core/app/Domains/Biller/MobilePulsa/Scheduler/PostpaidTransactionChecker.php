<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 12/12/17
 * Time: 20.25
 */

namespace Odeo\Domains\Biller\MobilePulsa\Scheduler;

use GuzzleHttp\Client;
use Odeo\Domains\Biller\BillerManager;
use Odeo\Domains\Constant\BillerMobilePulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;

class PostpaidTransactionChecker extends BillerManager {

  public function __construct() {
    parent::__construct(SwitcherConfig::MOBILEPULSA);
  }

  public function run() {
    $pipeline = new Pipeline;
    $pipeline->add(new Task(__CLASS__, 'check'));
    $pipeline->execute([]);
  }

  public function check(PipelineListener $listener, $data) {
    if ($transactions = $this->billerRepository->getPendingTransactions()) {

      $client = new Client();

      foreach ($transactions as $transaction) {
        $this->load($transaction);

        $query = [
          'commands' => BillerMobilePulsa::COMMAND_CHECK_STATUS,
          'username' => BillerMobilePulsa::USERNAME,
          'ref_id' => $this->billerOrder->id,
          'sign' => md5(BillerMobilePulsa::USERNAME . BillerMobilePulsa::getKey() . 'cs')
        ];

        $xml = new \SimpleXMLElement('<mp/>');

        array_walk_recursive($query, function ($value, $key) use ($xml) {
          $xml->addChild($key, $value);
        });

        try {

          $response = $client->request('POST', BillerMobilePulsa::getPostpaidUrl(), ['body' => $xml->asXML()]);
          $this->billerOrder->log_notify = $response->getBody();
          $dom = new \DOMDocument();

          if (@$dom->loadXML($this->billerOrder->log_notify)) {
            $status = $dom->getElementsByTagName('response_code')[0]->nodeValue;

            if (array_key_exists($status, BillerMobilePulsa::POSTPAID_STATUS_SUCCESS)) {
              $this->isCheckSN = false;
              $this->billerOrder->status = SwitcherConfig::BILLER_SUCCESS;
              $this->currentRecon->biller_transaction_id = $dom->getElementsByTagName('tr_id')[0]->nodeValue;
              $this->currentBiller->current_balance = $dom->getElementsByTagName('balance')[0]->nodeValue;
            } else if (array_key_exists($status, BillerMobilePulsa::POSTPAID_STATUS_PENDING))
              $this->billerOrder->status = SwitcherConfig::BILLER_IN_QUEUE;
            else $this->billerOrder->status = SwitcherConfig::BILLER_FAIL;

          } else $this->billerOrder->status = SwitcherConfig::BILLER_SUSPECT;

        } catch (\Exception $e) {
          $this->timeoutMessage = 'Error timeout: ' . $e->getMessage();
        }

        $this->finalize($listener, false);
      }

    }
  }

}
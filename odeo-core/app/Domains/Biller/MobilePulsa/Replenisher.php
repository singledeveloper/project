<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/29/17
 * Time: 9:42 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa;

use Goutte\Client;
use Odeo\Domains\Biller\Contract\ReplenishmentContract;
use Odeo\Domains\Constant\BillerMobilePulsa;
use Odeo\Domains\Core\PipelineListener;

class Replenisher implements ReplenishmentContract {

  public function request(PipelineListener $listener, $data) {

    $client = new Client();
    $client->setClient(new \GuzzleHttp\Client(['cookies' => true, 'timeout' => 60]));
    try {
      $crawler = $client->request('GET', BillerMobilePulsa::LOGIN_URL);
      $form = $crawler->selectButton('Login')->form();
      $form['hp'] = BillerMobilePulsa::USERNAME;
      $form['password'] = BillerMobilePulsa::WEB_REPORT_PASSWORD;
      $client->submit($form);

      $crawler = $client->request('GET', BillerMobilePulsa::TOPUP_FORM_URL);
      $data = [
        'berita-isideposit' => '',
        'deposit' => $data['amount'],
        'method' => 'bca',
        'nama-rekening' => 'Odeo Teknologi Indonesia',
        'pin' => BillerMobilePulsa::WEB_REPORT_PASSWORD
      ];
      $token = $crawler->filter('meta[name="_token"]')->attr('content');
      $client->setHeader('X-CSRF-Token', $token);
      $client->setHeader('X-Requested-With', 'XMLHttpRequest');
      $client->request('POST', BillerMobilePulsa::TOPUP_POST_URL, $data);
      $response = json_decode($client->getResponse()->getContent(), true);
      if (isset($response)) {
        $amount = str_replace('.', '', $response['bank_amount']);
        return ['amount' => $amount];
      }
    } catch (\Exception $e) {}

    return ['is_error' => true];
  }

  public function complete(PipelineListener $listener, $data) {
    return [];
  }

}
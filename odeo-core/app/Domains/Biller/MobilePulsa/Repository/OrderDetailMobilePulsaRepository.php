<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/10/17
 * Time: 3:16 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa\Repository;


use Odeo\Domains\Biller\MobilePulsa\Model\OrderDetailMobilePulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\BillerRepository;

class OrderDetailMobilePulsaRepository extends BillerRepository {
  public function __construct(OrderDetailMobilePulsa $orderDetailMobilePulsa) {
    $this->setModel($orderDetailMobilePulsa);
  }

  public function getPendingTransactions() {
    return $this->model
      ->where('status', SwitcherConfig::BILLER_IN_QUEUE)
      ->get();
  }
}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/31/17
 * Time: 12:53 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa\Inquirer;


use GuzzleHttp\Client;
use Odeo\Domains\Constant\BillerMobilePulsa;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;

class BroadbandLandlineInquirer {

  public function __construct() {
    $this->pulsaInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
  }

  public function inquiry(PipelineListener $listener, $data) {

    if (isset($data['inventory_code'])) {
      $inventory = $this->pulsaInventories->findExisting(SwitcherConfig::MOBILEPULSA, $data['inventory_code']);
      $code = $data['inventory_code'];
    } else {
      $inventory = $this->pulsaInventories->findById($data['inventory_id']);
      $code = $inventory->code;
    }

    $refId = isset($data['ref_id']) ? $data['ref_id'] : (new \DateTime())->getTimestamp();
    $query = [
      'commands' => BillerMobilePulsa::COMMAND_POSTPAID_INQUIRY,
      'username' => BillerMobilePulsa::USERNAME,
      'code' => $code,
      'hp' => isset($data['item_detail']) ? $data['item_detail']['number'] : $data['number'],
      'ref_id' => $refId,
      'sign' => md5(BillerMobilePulsa::USERNAME . BillerMobilePulsa::getKey() . $refId)
    ];

    $xml = new \SimpleXMLElement('<mp/>');
    array_walk_recursive($query, function ($value, $key) use ($xml) {
      $xml->addChild($key, $value);
    });

    $client = new Client();
    $response = $client->post(BillerMobilePulsa::getPostpaidUrl(), ['body' => $xml->asXML()]);
    $xmlContent = $response->getBody()->getContents();

    $dom = new \DOMDocument();
    if (@$dom->loadXML($xmlContent)) {
      $inquiryResult = [];
      for ($i = 0; $i < $dom->getElementsByTagName('mp')->item(0)->childNodes->length; $i++) {
        $current = $dom->getElementsByTagName('mp')->item(0);
        if ($current->childNodes->item($i)->nodeName == 'detail') break;

        $key = $current->childNodes->item($i)->nodeName;
        $val = $current->childNodes->item($i)->nodeValue;
        $inquiryResult[$key] = $val;

      }

      if (isset($inquiryResult['response_code']) && $inquiryResult['response_code'] == '00') {

        $inquiryDetails = [];
        $totalMonth = 0;

        $nodeDetails = $dom->getElementsByTagName('detail');

        for ($i = 0; $i < $nodeDetails->length; $i++) {

          $currentDetail = $nodeDetails->item($i);

          list($month, $year) = explode(' ', trim($currentDetail->getElementsByTagName('periode')[0]->nodeValue));
          $month = normalizeMonthID($month);
          $totalMonth++;

          $inquiryDetails[] = [
            'period' => date('Y-m', strtotime($month . ' ' . $year)),
            'base_price' => $currentDetail->getElementsByTagName('nilai_tagihan')[0]->nodeValue,
            'admin_fee' => $currentDetail->getElementsByTagName('admin')[0]->nodeValue,
          ];
        }

        $name = 'PULSA PASCABAYAR';
        if (isset($inventory)) $name = strtoupper($inventory->pulsa->category);

        return [
          'tr_id' => $inquiryResult['tr_id'],
          'name' => $name,
          'subscriber_id' => $inquiryResult['hp'],
          'subscriber_name' => $inquiryResult['tr_name'],
          'multiplier' => count($inquiryDetails),
          'ref_id' => $inquiryResult['ref_id'],
          'biller_price' => $inquiryResult['price'],
          'details' => $inquiryDetails,
          'month_counts' => $totalMonth
        ];

      } else {
        return [
          'status' => SwitcherConfig::BILLER_FAIL,
          'error_message' => BillerMobilePulsa::POSTPAID_ERROR_MAPPINGS[$inquiryResult['response_code']] ?? BillerMobilePulsa::GENERAL_ERROR_MESSAGE,
          'log' => $xmlContent
        ];
      }
    }

    return [
      'status' => SwitcherConfig::BILLER_FAIL,
      'error_message' => BillerMobilePulsa::GENERAL_ERROR_MESSAGE,
      'log' => $xmlContent
    ];
  }

}
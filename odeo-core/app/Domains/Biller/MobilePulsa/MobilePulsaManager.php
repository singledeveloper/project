<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/10/17
 * Time: 3:14 PM
 */

namespace Odeo\Domains\Biller\MobilePulsa;

use Odeo\Domains\Biller\Contract\BillerContract;
use Odeo\Domains\Biller\MobilePulsa\Inquirer\BroadbandLandlineInquirer;
use Odeo\Domains\Biller\MobilePulsa\Inquirer\PlnPostpaidInquirer;
use Odeo\Domains\Biller\MobilePulsa\Inquirer\PulsaPostpaidInquirer;
use Odeo\Domains\Biller\MobilePulsa\Purchaser\PostpaidPurchaser;
use Odeo\Domains\Biller\MobilePulsa\Purchaser\PrepaidPurchaser;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;

class MobilePulsaManager implements BillerContract {

  private $inventoryValidator, $currencyHelper;

  public function __construct() {
    $this->inventoryValidator = app()->make(InventoryValidator::class);
    $this->currencyHelper = app()->make(Currency::class);
  }

  public function purchase(PipelineListener $listener, $data) {
    return app()->make(PrepaidPurchaser::class)->purchase($listener, $data);
  }

  public function purchasePostpaid(PipelineListener $listener, $data) {
    return app()->make(PostpaidPurchaser::class)->purchase($listener, $data);
  }

  public function validateInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validateInventory($listener, $data);
  }

  public function validatePostpaidInventory(PipelineListener $listener, $data) {
    return $this->inventoryValidator->validatePostpaidInventory($listener, $data);
  }

  public function inquiryPostpaid(PipelineListener $listener, $data) {
    switch ($data['service_detail_id']) {
      case ServiceDetail::PLN_POSTPAID_ODEO:
        return app()->make(PlnPostpaidInquirer::class)->inquiryPostpaid($listener, $data);
      case ServiceDetail::BROADBAND_ODEO:
      case ServiceDetail::LANDLINE_ODEO:
        return app()->make(BroadbandLandlineInquirer::class)->inquiry($listener, $data);
      default:
        return app()->make(PulsaPostpaidInquirer::class)->inquiry($listener, $data);
    }
  }

}
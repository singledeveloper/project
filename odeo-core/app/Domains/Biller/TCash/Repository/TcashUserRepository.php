<?php

namespace Odeo\Domains\Biller\TCash\Repository;

use Odeo\Domains\Biller\TCash\Model\TcashUser;
use Odeo\Domains\Constant\TCash;
use Odeo\Domains\Core\Repository;

class TcashUserRepository extends Repository {

  public function __construct(TcashUser $tcashUser) {
    $this->model = $tcashUser;
  }

  public function findByTelephone($telephone) {
    return $this->model->where('telephone', $telephone)
      ->where('is_active', true)->first();
  }

  public function findLeastConnected() {
    return $this->model->where('status', TCash::CONNECTED)
      ->orderBy('updated_at', 'asc')->first();
  }

  public function findBiggestBalance($storeId) {
    $query = $this->model->lockForUpdate()->where('is_active', true)
      ->where('status', TCash::CONNECTED)
      ->where('current_balance', '>=', 10000)
      ->where('purchase_counts', '<', TCash::MAX_PURCHASE_LIMIT);
    if ($storeId == null) $query = $query->whereNull('store_id');
    else $query = $query->where('store_id', $storeId);

    return $query->orderBy('updated_at', 'asc')->first();
  }

  public function findByStoreId($storeId) {
    return $this->model->where('store_id', $storeId)
      ->where('status', TCash::CONNECTED)
      ->where('is_active', true)->first();
  }

  public function getTotalBalance($storeId) {
    $query = $this->getCloneModel();
    if ($storeId == null) $query = $query->whereNull('store_id');
    else $query = $query->where('store_id', $storeId);

    return $query->select(\DB::raw('sum(current_balance) as total_balance'))->first();
  }

  public function resetPurchaseCount($storeId = '') {
    $query = $this->getCloneModel();
    if ($storeId != '') $query = $query->where('store_id', $storeId);
    return $query->where('status', TCash::CONNECTED)
      ->where('is_active', true)->update(['purchase_counts' => 0]);
  }

  public function getAllBasicAccountWithZeroBalance($storeId, $limit = 10) {
    $query = $this->getCloneModel();
    if ($storeId == null) $query = $query->whereNull('store_id');
    else $query = $query->where('store_id', $storeId);

    return $query->where('current_balance', 0)->where('purchase_counts', '<', TCash::MAX_PURCHASE_LIMIT)
      ->where('status', TCash::CONNECTED)->where('is_active', true)->where('account_type', TCash::ACCOUNT_TYPE_BASIC)
      ->skip(0)->limit($limit)->get();
  }

  public function getAllFullAccount($storeId = 0) {
    $query = $this->getCloneModel()->where('account_type', TCash::ACCOUNT_TYPE_FULL)
      ->where('status', TCash::CONNECTED)->where('is_active', true);
    if ($storeId != 0) $query = $query->where('store_id', $storeId);

    return $query->get();
  }

  public function gets() {
    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    if (isset($filters['search']) && is_array($filters['search'])) $filters = $filters['search'];

    if (isset($filters['store_id'])) $query = $query->where('store_id', $filters['store_id']);
    else $query = $query->whereNull('store_id');

    if (isset($filters['telephone'])) {
      $query = $query->whereIn('telephone', [
        purifyTelephone(trim($filters['telephone'])),
        trim($filters['telephone'])
      ]);
    }
    if (isset($filters['status'])) $query = $query->where('status', $filters['status']);

    return $this->getResult($query->orderBy('account_type', 'desc')->orderBy('id', 'desc'));
  }

}

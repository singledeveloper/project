<?php

namespace Odeo\Domains\Biller\TCash\Repository;

use Odeo\Domains\Biller\TCash\Model\TcashTransferHistory;
use Odeo\Domains\Core\Repository;

class TcashTransferHistoryRepository extends Repository {

  public function __construct(TcashTransferHistory $tcashTransferHistory) {
    $this->model = $tcashTransferHistory;
  }

}

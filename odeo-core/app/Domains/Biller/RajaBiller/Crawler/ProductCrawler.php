<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/22/16
 * Time: 9:41 PM
 */

namespace Odeo\Domains\Biller\RajaBiller\Crawler;

use Odeo\Domains\Constant\BillerRaja;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaOperatorRepository;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository;

class ProductCrawler {

  private $client, $codes = [], $mapping = [];

  public function __construct() {

    $this->client = new \GuzzleHttp\Client(['curl' => [CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false]]);

    $this->pulsaOperator = app()->make(PulsaOperatorRepository::class);
    $this->pulsaInventories = app()->make(PulsaOdeoInventoryRepository::class);

    $this->mapping = ["ISAT", "XL", "AXIS", "SMART", "KARTU3", "TELKOMSEL", "BOLT"];
  }

  public function crawl() {

    $inventories = $this->pulsaInventories->findByVendorSwitcherId(SwitcherConfig::RAJABILLER);
    $listInventories = [];
    foreach ($inventories as $item) {
      if (!isset($listInventories[$item->code])) $listInventories[$item->code] = [];
      $listInventories[$item->code][] = $item;
    }

    foreach ($this->mapping as $item) {
      $xml = BillerRaja::formatXML('rajabiller.harga', [$item, BillerRaja::USER_ID, BillerRaja::PASSWORD]);

      try {
        $response = $this->client->request('POST', BillerRaja::PROD_SERVER, ["body" => $xml]);

        $dom = new \DOMDocument;
        if (@$dom->loadXML($response->getBody()->getContents())) {
          $arrays = $dom->getElementsByTagName('string');

          $key = array('id', 'pin', 'sisa_saldo', 'status', 'keterangan');
          $i = 0;
          foreach ($arrays as $array) $a[$key[$i++]] = $array->nodeValue;

          $lists = explode(";", $a['keterangan']);
          foreach ($lists as $output) {
            try {
              list ($id, $name, $price) = explode("\t", $output);
              list ($code, $nominal) = explode(" ", trim($id));

              if (isset($listInventories[$code])) {
                $this->codes[] = $code;
                foreach ($listInventories[$code] as $item) {
                  $item->base_price = str_replace(["Rp.", ","], "", $price);
                  $item->is_active = true;
                  $this->pulsaInventories->save($item);
                }
              }
            }
            catch (\Exception $e) {}
          }
        }
      }
      catch (\Exception $e) {}
    }

    $this->pulsaInventories->finalizeProducts(SwitcherConfig::RAJABILLER, $this->codes);
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 05/01/18
 * Time: 16.07
 */

namespace Odeo\Domains\Account;


use Carbon\Carbon;
use Odeo\Domains\Account\Jobs\SendEmailConfirmation;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Core\PipelineListener;

class UserMailVerificator {

  private $user;

  public function __construct() {
    $this->user = app()->make(UserRepository::class);
  }

  public function sendVerification(PipelineListener $listener, $data) {
    $user = $this->user->findByEmail($data['email']);

    if (!isset($user->email)) {
      return $listener->response(400, trans('errors.invalid_email'));
    }

    if ($user->is_email_verified) {
      return $listener->response(400, 'email anda sudah terverifikasi');
    }

    $ivLen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivLen);
    $cipherTextRaw = openssl_encrypt($user->email . '#' . datenow(), "AES-128-CBC", $this->getMailKey(), $options = OPENSSL_RAW_DATA, $iv);
    $hMac = $this->hashHMac($cipherTextRaw);
    $cipherText = $this->base64UrlEncode($iv . $hMac . $cipherTextRaw);
    $url = baseUrl('v1/user/verify/email/' . $cipherText);

    if (isProduction()) {
      $url = str_replace('api', 'mailver', $url);
    }

    dispatch(new SendEmailConfirmation($url, $user));

    return $listener->response(200);
  }


  public function verifyEmail(PipelineListener $listener, $data) {
    $c = $this->base64UrlDecode($data["token"]);
    $ivLen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = substr($c, 0, $ivLen);
    $hMac = substr($c, $ivLen, $sha2len = 32);
    $cipherTextRaw = substr($c, $ivLen + $sha2len);
    $originalPlaintext = openssl_decrypt($cipherTextRaw, $cipher, $this->getMailKey(), $options = OPENSSL_RAW_DATA, $iv);
    $calcMac = $this->hashHMac($cipherTextRaw);

    if (!hash_equals($hMac, $calcMac)) {
      return $listener->response(200, [
        'status' => 'error'
      ]);
    }

    $explodedData = explode('#', $originalPlaintext);
    $email = $explodedData[0];

    $now = Carbon::now();
    $date = Carbon::parse($explodedData[1]);

    if ($date->diffInWeeks($now) >= 2) {
      return $listener->response(200, [
        'status' => 'expired'
      ]);
    }

    $user = $this->user->findByEmail($email);
    $user->is_email_verified = true;
    $user->mail_verified_at = $now;
    $this->user->save($user);

    return $listener->response(200, [
      'status' => 'success'
    ]);
  }

  private function hashHMac($cipherTextRaw) {
    return hash_hmac('sha256', $cipherTextRaw, $this->getMailKey(), $as_binary = true);
  }

  private function getMailKey() {
    return env('mailKey');
  }

  private function base64UrlEncode($input) {
    return strtr(base64_encode($input), '+/=', '._-');
  }

  private function base64UrlDecode($input) {
    return base64_decode(strtr($input, '._-', '+/='));
  }

}

<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\Email;
use Odeo\Domains\Core\PipelineListener;

class UserValidator {

  private $userRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
  }

  public function validateEmail(PipelineListener $listener, $data) {

    if (Email::isBlacklisted($data['email'])) {
      return $listener->response(400, 'email not allowed, this email is marked as disposable mail.');
    }

    if ($this->userRepo->findByEmail($data["email"])) {
      return $listener->response(400, 'The Email has already been taken.');
    }

    return $listener->response(200);

  }
}

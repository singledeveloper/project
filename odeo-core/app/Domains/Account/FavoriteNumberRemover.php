<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 24-Jan-18
 * Time: 6:15 PM
 */

namespace Odeo\Domains\Account;


use Odeo\Domains\Account\Repository\FavoriteNumberRepository;
use Odeo\Domains\Core\PipelineListener;

class FavoriteNumberRemover {

  private $favoriteNumberRepo;

  public function __construct() {
    $this->favoriteNumberRepo = app()->make(FavoriteNumberRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {

    $userId = $data['auth']['user_id'];

    if (count($data['ids']) > 0) {
      $this->favoriteNumberRepo->deleteByUserId($userId, $data['ids']);
    }

    return $listener->response(200);
  }

}

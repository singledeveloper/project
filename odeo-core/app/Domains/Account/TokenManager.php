<?php

namespace Odeo\Domains\Account;

use Carbon\Carbon;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Odeo\Domains\Constant\Header;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\Jobs\GetUserIpLocation;

class TokenManager {

  private $deviceToken, $user, $userToken, $tokenCreator;

  private static $userData = null;

  public function __construct() {
    $this->deviceToken = app()->make(\Odeo\Domains\Account\Repository\UserFcmTokenRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userToken = app()->make(\Odeo\Domains\Account\Repository\UserTokenRepository::class);
    $this->tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);
  }

  public function _transforms($item) {
    $response = [];
    $response["id"] = $item->id;
    $response["user_id"] = $item->user_id;
    $response["ssid"] = $item->device_ssid;
    if($item->device_system_name) $response["system_name"] = $item->device_system_name;
    if($item->device_system_version) $response["system_version"] = $item->device_system_version;
    if($item->device_name) $response["name"] = $item->device_name;
    if($item->device_model) $response["model"] = $item->device_model;
    if($item->login_ip_address) $response["ip_address"] = $item->login_ip_address;
    if($item->login_country) $response["country"] = $item->login_country;
    $response["last_login_time"] = $item->last_login_time;
    $response["platform_id"] = $item->platform_id;
    $response["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    $response["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");
    return $response;
  }

  private function purifyToken($token) {
    $token = (new Parser())->parse($token);
    return $token;
  }

  public static function getUserData() {
    return self::$userData;
  }

  public function setUserFromAffiliate(PipelineListener $listener, $data) {
    $auth = [
      "user_id" => $data['user_id'],
      "type" => $data['type'],
      "platform_id" => Platform::AFFILIATE,
    ];
    self::$userData = $auth;
    return $listener->response(200, ['auth' => $auth]);
  }

  public function setUserFromDisbursement(PipelineListener $listener, $data) {
    self::$userData = [
      "user_id" => $data['user_id'],
      "type" => $data['type'],
      "platform_id" => Platform::DISBURSEMENT,
    ];
    return $listener->response(200);
  }

  public function setUserFromTerminal(PipelineListener $listener, $data) {
    self::$userData = [
      'user_id' => $data['user_id'],
      'type' => $data['type'],
      'platform_id' => Platform::TERMINAL,
    ];
    return $listener->response(200);
  }

  public function create(PipelineListener $listener, $data) {
    $data['login_ip_address'] = getClientIP();
    $version = null;

    $user = $this->user->findById($data['user_id']);

    if ($data['platform_id'] == Platform::ANDROID) {
      $request = app()->make(\Illuminate\Http\Request::class);
      $version = $request->header(Header::ANDROID_HEADER_VER);

      if (!$user->android_version || $user->android_version != $version) {
        $user->use_android_version = $version;
        $this->user->save($user);
      }
    }

    if (isVersionSatisfy(Platform::ANDROID, '3.1.0') && isset($data["device_ssid"])) {
      $userToken = $this->userToken->findBySsid($data["device_ssid"]);
      if (!$userToken) {
        $userToken = $this->userToken->generate(
          $data["user_id"],
          $data['device_ssid'],
          $data['device_system_name'] ?? null,
          $data['device_system_version'] ?? null,
          $data['device_name'] ?? null,
          $data['device_model'] ?? null,
          $data['login_ip_address'] ?? null,
          $data['platform_id']);
      } else {
        $userToken->user_id = $data["user_id"];
        $userToken->last_login_time = Carbon::now();
        $userToken->expired_at = Carbon::now()->addDays(30);
        $userToken->save();
      }
    } else {
      $userToken = $this->userToken->generate($data["user_id"], null, null, $data['platform_id']);
    }
    
    $deviceToken = $userToken->id;
  
    $token = $this->tokenCreator->createToken(
      $data['user_id'],
      $data['user_type'], 
      $data['platform_id'],
      $version, 
      $deviceToken
    );

    $tokenRefresh = $this->tokenCreator->createTokenRefresh(
      $data['user_id'], 
      $data['user_type'],
      $data['platform_id'],
      $userToken->secret,
      $deviceToken
    );

    if ($data['login_ip_address'] && isProduction()) {
      $listener->pushQueue(new GetUserIpLocation([
        'user_id' => $data['user_id'],
        'ip' => $data['login_ip_address']
      ]));
    }

    return $listener->response(201, [
      "user_id" => $data["user_id"],
      "name" => $user->name,
      "user_type" => $data["user_type"],
      "phone_number" => $user->telephone,
      "email" => $user->email,
      "token" => (string)$token,
      "token_refresh" => (string)$tokenRefresh,
      "requirement" => $data['requirement'] ?? [],
      "has_disbursement" => isset($data["has_disbursement"]) && $data["has_disbursement"],
      "has_payment_gateway" => isset($data["has_payment_gateway"]) && $data["has_payment_gateway"],
      "has_invoice" => isset($data["has_invoice"]) && $data["has_invoice"],
      "has_supply_store" => isset($data["has_supply_store"]) && $data["has_supply_store"],
      "is_email_verified" => $data['is_email_verified'] ?? false,
      "is_ktp_verified" => $data['is_ktp_verified'] ?? false,
      "is_forced_change_password" => $user->status == UserStatus::FORCE_CHANGE_PASSWORD,
      "is_forced_change_pin" => $user->status == UserStatus::FORCE_CHANGE_PIN,
      "is_reseller" => isset($data["is_reseller"]) && $data["is_reseller"],
      "is_approver" => isset($data["is_approver"]) && $data["is_approver"],
      "is_business" => strpos($user->telephone, UserType::BUSINESS_PREFIX) == 0
    ]);
  }

  public function parseBearerAndAuth($roles) {

    $request = app()->make(\Illuminate\Http\Request::class);

    if (!$request->header('Authorization')) $headers = [];
    else $headers = explode(", ", $request->header('Authorization'));

    $data = [];
    foreach ($headers as $header) {
      $auth = explode(" ", $header);
      if (count($auth) == 2) {
        $data[strtolower($auth[0])] = $auth[1];
      }
    }

    try {

      $token = (new Parser())->parse($data["bearer"]);

      if (!$token->verify(new Sha256(), env('APP_KEY'))) return null;

      $allowed_types = explode("|", $roles);

      if ($token->getClaim('uty') != 'admin' && (sizeof($allowed_types) > 0 && !in_array($token->getClaim('uty'), $allowed_types))) return null;
      else {

        self::$userData = [
          "user_id" => $token->getClaim('uid'),
          "type" => $token->getClaim('uty'),
          "platform_id" => $token->getClaim('upf'),
        ];
        return self::$userData;

      }
    } catch (\Exception $e) {
      return null;
    }
  }

  public function check(PipelineListener $listener, $data) {
    //return $listener->response(401, trans('errors.invalid_authorization'));
    try {
      $token = (new Parser())->parse($data["bearer"]);

      if (!$token->verify(new Sha256(), env('APP_KEY'))) {
        return $listener->response(401, trans('errors.invalid_authorization'));
      }

      $allowed_types = explode("|", $data["roles"]);
      
      if ($token->getClaim('uty') != 'admin' && (sizeof($allowed_types) > 0 && !in_array($token->getClaim('uty'), $allowed_types)))
        return $listener->response(400, "You don't have any credential to access this data.");
      else {
        if ($token->getClaim('uty') == 'admin' && !in_array($token->getClaim('uid'), [4350, 4309, 133285])) {
          $request = app()->make(\Illuminate\Http\Request::class);
          //clog('server_test', json_encode($request->server()));
          $ip = $request->server('REMOTE_ADDR');
          if (app()->environment() == 'production' && $ip != '127.0.0.1' && !ipInRange($ip, '172.31.0.0/16')) {
            return $listener->response(401, trans('errors.invalid_authorization'));
          }
        }
        self::$userData = [
          "user_id" => $token->getClaim('uid'),
          "type" => $token->getClaim('uty'),
          "platform_id" => $token->getClaim('upf'),
        ];

        return $listener->response(200, self::$userData);
      }
    } catch (\Exception $e) {
      if (app()->environment() == 'production')
        return $listener->response(401, trans('errors.invalid_authorization'));
      return $listener->response(401, $e->getMessage());
    }
  }

  public function refresh(PipelineListener $listener, $data) {
    try {

      $token = (new Parser())->parse($data["refresh_token"]);
      if (!$token->verify(new Sha256(), env('APP_KEY')))
        return $listener->response(401, trans('errors.invalid_authorization'));
      if ($data["user_id"] != $token->getClaim('uid'))
        return $listener->response(401, trans('errors.invalid_authorization'));
      $userToken = $this->userToken->findById($token->getClaim('dvt'));
      if (!$userToken || ($userToken && $userToken->secret != $token->getClaim('ref')))
        return $listener->response(401, trans('errors.invalid_authorization'));

      $version = null;

      if ($token->getClaim('upf') == Platform::ANDROID) {
        $request = app()->make(\Illuminate\Http\Request::class);
        $version = $request->header(Header::ANDROID_HEADER_VER);
        $user = $this->user->findById($data['user_id']);

        if (!$user->android_version || $user->android_version != $version) {
          $user->use_android_version = $version;
          $this->user->save($user);
        }
      }

      return $listener->response(201, [
        "token" => (string)$this->tokenCreator->createToken(
          $token->getClaim('uid'), $token->getClaim('uty'), $token->getClaim('upf'), $version, $token->getClaim('dvt')),
      ]);
    } catch (\Exception $e) {
      if (app()->environment() == 'production')
        return $listener->response(401, trans('errors.invalid_authorization'));
      return $listener->response(401, $e->getMessage());
    }
  }

  public function registerDevice(PipelineListener $listener, $data) {
    if ($data['auth']['type'] == UserType::ADMIN) {
      return $listener->response(200);
    }

    if (!$this->deviceToken->findDeviceToken($data["auth"]["user_id"], $data['device_token'])) {
      $deviceToken = $this->deviceToken->getNew();
      $deviceToken->user_id = $data["auth"]["user_id"];
      $deviceToken->device_token = $data['device_token'];

      $this->deviceToken->save($deviceToken);

      $user = $deviceToken->user;
      $listener->addNext(new Task(LoginWarner::class, 'warn', [
        'email' => $user->email,
        'phone_number' => $user->telephone,
        'ip_address' => getClientIP(),
        'platform_id' => $data['auth']['platform_id'],
      ]));
      return $listener->response(201);
    }
    return $listener->response(200);

  }

  public function removeDevice(PipelineListener $listener, $data) {
    $this->deviceToken->removeDeviceToken($data['auth']['user_id'], $data['device_token']);

    return $listener->response(200);
  }

  public function removeDeviceById(PipelineListener $listener, $data) {
    $this->userToken->removeDeviceById($data['id']);

    return $listener->response(200);
  }

  public function removeAllDevice(PipelineListener $listener, $data) {
    $token = $this->purifyToken($data['bearer']);
    $userTokenId = $token->getClaim('dvt');

    $this->userToken->removeAllDevice($data['auth']['user_id'], $userTokenId);

    return $listener->response(200);
  }

  public function getDevices(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    $devices = [];
    foreach ($this->userToken->getByUserId($userId) as $item) {
      $devices[] = $this->_transforms($item);
    }

    if (sizeof($devices) > 0) {
      return $listener->response(200, ["devices" => $devices]);
    }
    return $listener->response(204, ["devices" => []]);
  }

}
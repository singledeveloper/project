<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Account\Repository\UserHistoryRepository;
use Odeo\Domains\Account\Repository\UserOtpAttemptRepository;
use Odeo\Domains\Account\Repository\UserOtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\Job\SendSms;

class UserVerificator {

  private $userRepo, $userOtpRepo,
    $userOtpAttemptRepo, $userHistoryRepo, $userHistoryCreator, $userOtpAttemptRecorder;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userOtpRepo = app()->make(UserOtpRepository::class);
    $this->userOtpAttemptRepo = app()->make(UserOtpAttemptRepository::class);
    $this->userHistoryRepo = app()->make(UserHistoryRepository::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
    $this->userOtpAttemptRecorder = app()->make(UserOtpAttemptRecorder::class);
  }

  public function guardTelephone(PipelineListener $listener, $data) {
    if (isset($data["phone_number"]) && $user = $this->userRepo->findByTelephone($data['phone_number'])) {
      return $listener->response(200, ["user_id" => $user->id]);
    }
    return $listener->response(400, "User doesn't exist.");
  }

  public function guardActiveTelephone(PipelineListener $listener, $data) {
    if ($user = $this->userRepo->findByTelephone($data['phone_number'])) {
      return $listener->response(400, "Anda tidak dapat menggunakan nomor telepon ini.");
    }
    return $listener->response(200);
  }

  public function verify(PipelineListener $listener, $data) {
    list($isValid, $message) = $this->userOtpAttemptRecorder->recordPhoneOtp($data['phone_number'], $data['verification_code']);
    if (!$isValid) return $listener->response(400, $message);
    return $listener->response(200);
  }

  public function verifyAfterLogin(PipelineListener $listener, $data) {
    $user = $this->userRepo->findById($data['auth']['user_id']);
    list($isValid, $message) = $this->userOtpAttemptRecorder->recordPhoneOtp($user->telephone, $data['verification_code']);
    if (!$isValid) return $listener->response(400, $message);
    return $listener->response(200);
  }

  public function verifyChangeNumber(PipelineListener $listener, $data) {
    $user = $this->userRepo->findById($data['auth']['user_id']);
    $oldPhoneNumber = $user->telephone;

    list($isValid, $message) = $this->userOtpAttemptRecorder->recordPhoneOtp($data['phone_number'], $data['verification_code']);
    if (!$isValid) return $listener->response(400, $message);

    $user->telephone = $data['phone_number'];

    $this->saveUserHistories($user);

    $this->userRepo->save($user);

    $listener->pushQueue(new SendSms([
      'sms_destination_number' => $oldPhoneNumber,
      'sms_text' => 'Nomor telepon yang terhubung ke akun ODEO Anda telah diubah ke ' . revertTelephone($user->telephone) . '. ' .
        'Jika Anda tidak melakukan perubahan, mohon segera hubungi CS ODEO.'
    ]));

    return $listener->response(200);
  }

  private function saveUserHistories(User $user) {
    $this->userHistoryCreator->create($user, $user->id);
  }
}

<?php

namespace Odeo\Domains\Account;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Account\Helper\UserChecker;
use Odeo\Domains\Account\Repository\UserEmailVerificationOtpAttemptRepository;
use Odeo\Domains\Account\Repository\UserOtpAttemptRepository;
use Odeo\Domains\Account\Repository\UserOtpRepository;
use Odeo\Domains\Account\Repository\UserPasswordHistoryRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\Email;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;

class UserUpdater {

  private $user, $selector, $userOtp, $userEmailVerificationOtpAttemptRepo,
    $userPasswordHistories, $userChecker, $userOtpAttemptRepo, $userHistoryCreator;

  public function __construct() {
    $this->user = app()->make(UserRepository::class);
    $this->selector = app()->make(UserSelector::class);
    $this->userOtp = app()->make(UserOtpRepository::class);
    $this->userPasswordHistories = app()->make(UserPasswordHistoryRepository::class);
    $this->userChecker = app()->make(UserChecker::class);
    $this->userOtpAttemptRepo = app()->make(UserOtpAttemptRepository::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
    $this->userEmailVerificationOtpAttemptRepo = app()->make(UserEmailVerificationOtpAttemptRepository::class);
  }

  public function changeData(PipelineListener $listener, $data) {
    $user = $this->user->findById($data["auth"]["user_id"]);

    $newEmailFlow = isVersionSatisfy(Platform::ANDROID, '3.0.0')
      || isVersionSatisfy(Platform::IOS, '3.0.0');

    if (isset($data["email"]) && !$newEmailFlow) {
      if (Email::isBlacklisted($data['email'])) {
        return $listener->response(400, 'email not allowed, this email is marked as disposable mail');
      }

      $anotherUser = $this->user->findByEmail($data["email"]);
      if ($anotherUser && $anotherUser->id != $user->id) {
        return $listener->response(400, trans('errors.email_taken'));
      }

      if (isset($data['email']) && (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))) {
        return $listener->response(400, 'Your email format is not valid');
      }

      $user->email = $data['email'];
      $user->is_email_verified = false;
    }

    if (isset($data["name"])) {
      $userKtpValidator = app()->make(UserKtpValidator::class);
      if ($userKtpValidator->isVerified($data["auth"]["user_id"]))
        return $listener->response(400, 'You can\'t change your name.');
      $user->name = trim($data['name']);
    }
    if (isset($data["province_id"])) $user->province_id = $data['province_id'];
    if (isset($data["city_id"])) $user->city_id = $data['city_id'];
    if (isset($data["street"])) $user->street = $data['street'];
    if (isset($data["postcode"])) $user->postcode = $data['postcode'];

    $this->userHistoryCreator->create($user, $user->id);

    $this->user->save($user);

    return $listener->response(200, $this->selector->_transforms($user, $this->user));
  }

  public function changePassword(PipelineListener $listener, $data) {
    $user = $this->user->findById($data["auth"]["user_id"]);

    if (isAdmin()) {
      list($isValid, $message) = $this->userChecker->checkPassword($user, $data['old_password']);
      if (!$isValid) return $listener->response(400, $message);
    } else {
      // temporary
      list($isValid, $message) = $this->userChecker->checkPin($user, $data['pin']);
      if (!$isValid) return $listener->response(400, $message);
    }

    $user->password = Hash::make($data['new_password']);

    if ($user->status == UserStatus::FORCE_CHANGE_PASSWORD) {
      $user->status = UserStatus::FORCE_CHANGE_PIN;
    }

    $userPasswordHistory = $this->userPasswordHistories->getNew();
    $userPasswordHistory->user_id = $user->id;
    $userPasswordHistory->created_at = date('Y-m-d H:i:s');
    $this->userPasswordHistories->save($userPasswordHistory);

    $this->user->save($user);

    return $listener->response(200);
  }

  public function changePin(PipelineListener $listener, $data) {
    $user = $this->user->findById($data["auth"]["user_id"]);

    // temporary
    list($isValid, $message) = $this->userChecker->checkPin($user, $data['old_pin']);
    if (!$isValid) return $listener->response(400, $message);

    list($isValid, $message) = checkPinSecure($data['new_pin']);
    if (!$isValid) return $listener->response(400, $message);

    if (isset($data['confirm_pin']) && $data['confirm_pin'] != $data['new_pin'])
      return $listener->response(400, 'Confirm PIN must be same as New PIN');

    $newPin = Hash::make($data['new_pin']);

    if ($user->transaction_pin == $user->password) {
      $user->password = $newPin;
    }
    if ($user->status == UserStatus::RESET_PIN || $user->status == UserStatus::FORCE_CHANGE_PIN) {
      $user->status = UserStatus::OK;
    }

    $this->userHistoryCreator->create($user, $data['auth']['user_id']);
    $user->transaction_pin = $newPin;
    $this->user->save($user);

    return $listener->response(200);
  }

  public function changeTransactionPin(PipelineListener $listener, $data) {
    $user = $this->user->findById($data["auth"]["user_id"]);

    if (!$this->userOtp->findUsedTelephone($user->telephone)) {
      return $listener->response(400, trans('errors.telephone_not_verified'));
    }

    list($isValid, $message) = checkPinSecure($data['transaction_pin']);
    if (!$isValid) return $listener->response(400, $message);

    $user->transaction_pin = Hash::make($data['transaction_pin']);
    $this->user->save($user);

    $this->userOtp->archiveOtp($user->telephone);

    return $listener->response(200);
  }

  public function toggleBlock(PipelineListener $listener, $data) {
    $user = $this->user->findById($data["user_id"]);

    $user->login_counts = $user->login_counts == UserStatus::LOGIN_BLOCKED_COUNT
      ? 0
      : UserStatus::LOGIN_BLOCKED_COUNT;

    $this->user->save($user);

    return $listener->response(200);
  }

  public function updateStatus(PipelineListener $listener, $data) {
    $user = $this->user->findById($data["user_id"]);
    $newStatus = $data['status'];

    if (in_array($user->status, UserStatus::groupBanned()) && $newStatus == UserStatus::OK) {
      $this->userOtpAttemptRepo->disableAttempt($user->telephone);
      $this->userEmailVerificationOtpAttemptRepo->disableAttempt($user->email);
    }

    $user->status = $newStatus;
    $user->invalid_pin_attempt_count = 0;
    $user->login_counts = 0;

    $this->userHistoryCreator->create($user, $data['auth']['user_id']);
    $this->user->save($user);

    return $listener->response(200);
  }
}

<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\SubUser;
use Odeo\Domains\Core\Repository;

class SubUserRepository extends Repository
{

  public function __construct(SubUser $subUser)
  {
    $this->model = $subUser;
  }

  public function getParentUserId($userId)
  {
    return $this->model->where('user_id', $userId)
      ->first(['parent_user_id']);
  }

}

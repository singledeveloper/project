<?php

namespace Odeo\Domains\Account\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserToken;
use Illuminate\Support\Facades\Hash;

class UserTokenRepository extends Repository {

  public function __construct(UserToken $userToken) {
    $this->model = $userToken;
  }

  public function findBySsid($ssid) {
    return $this->model->where('device_ssid', $ssid)->first();
  }

  public function findByUserIdAndSsid($userId, $ssid) {
    return $this->model->where('user_id', $userId)->where('device_ssid', $ssid)->first();
  }

  public function deleteByUserIdAndSsid($userId, $ssid) {
    $this->model->where('user_id', $userId)->where('device_ssid', $ssid)->delete();
  }

  public function deleteExpiredTokens() {
    return $this->model->where('expired_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))->delete();
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function findByUserIdAndIpAddress($userId, $ipAddress) {
    return $this->model->where('user_id', $userId)->where('login_ip_address', $ipAddress)->first();
  }

  public function findByUserIdAndSecretKey($userId, $secretKey) {
    return $this->model->where('user_id', $userId)
      ->where('secret', $secretKey)->first();
  }

  public function getByUserId($userId) {
    return $this->model->where('user_id', $userId)->get();
  }

  public function removeDeviceById($userTokenId) {
    return $this->model
      ->Where('id', $userTokenId)
      ->delete();
  }

  public function removeAllDevice($userId, $userTokenId) {
    return $this->model
      ->where('user_id', $userId)
      ->where('id', '!=' ,$userTokenId)
      ->delete();
  }

  public function generate(
    $userId,
    $ssid = null,
    $systemName = null,
    $systemVersion = null,
    $name = null,
    $model = null,
    $ipAddress = null,
    $platformId = null
  ) {
    $this->model->user_id = $userId;
    $this->model->secret = Hash::make(str_random(64));
    $this->model->device_ssid = $ssid;
    $this->model->device_system_name = $systemName;
    $this->model->device_system_version = $systemVersion;
    $this->model->device_name = $name;
    $this->model->device_model = $model;
    $this->model->login_ip_address = $ipAddress;
    $this->model->last_login_time = Carbon::now();
    $this->model->expired_at = Carbon::now()->addDays(30);
    $this->model->platform_id = $platformId;
    $this->model->save();

    return $this->model;
  }

}

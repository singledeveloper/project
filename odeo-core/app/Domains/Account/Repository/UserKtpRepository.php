<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Account\Model\UserKtp;
use Odeo\Domains\Core\Repository;

class UserKtpRepository extends Repository {

  public function __construct(UserKtp $userKtp) {
    $this->model = $userKtp;
  }

  public function findByUserIdWithoutReject($userId) {
    return $this->model->where('user_id', $userId)
      ->whereNull('rejected_at')
      ->first();
  }

  public function findLastByUserId($userId) {
    return $this->model->where('user_id', $userId)
      ->orderBy('id', 'desc')
      ->first();
  }

  public function findVerificationStatusByUserId($userId) {
    return $this->model->where('user_id', $userId)
      ->select(\DB::raw("
        CASE 
          WHEN verified_at IS NOT NULL THEN 'verified' 
          WHEN rejected_at IS NOT NULL THEN 'rejected'
          ELSE 'waiting for verification'
         END
         as status
      "))
      ->orderBy('created_at', 'desc')
      ->first();
  }

  public function findLastVerificationByUserId($userId) {
    return $this->model->where('user_id', $userId)
      ->select(\DB::raw("
        CASE 
          WHEN verified_at IS NOT NULL THEN 'verified' 
          WHEN rejected_at IS NOT NULL THEN 'rejected'
          ELSE 'waiting for verification'
         END
         as status,
         *
      "))
      ->orderBy('id', 'desc')
      ->first();
  }

  public function findByKtpNik($ktpNik) {
    return $this->model->where('ktp_nik', $ktpNik)->first();
  }

  public function isVerified($userId) {
    return $this->model
      ->where('user_id', '=', $userId)
      ->whereNotNull('verified_at')
      ->first();
  }

  public function gets() {
    $filters = $this->getFilters();

    $query = $this->model->from(\DB::raw('(select 
      distinct on (user_ktps.user_id) 
      user_ktps.*, 
      user_ktps.name as ktp_name,
      users.name,
      users.telephone,
      users.email,
      count(user_ktps.user_id) over (PARTITION BY user_ktps.user_id) as total_attempt,
      verified_user.name as verified_by_user_name,
      rejected_user.name as rejected_by_user_name,
      last_successful_user_ktps.ktp_nik as last_ktp_nik,
      last_successful_user_ktps.name as last_ktp_name,
      last_successful_user_ktps.ktp_image_url as last_ktp_image_url,
      last_successful_user_ktps.ktp_selfie_image_url as last_ktp_selfie_image_url,
      case when user_cashes.amount isnull then 0 else user_cashes.amount end as current_ocash_balance
      from user_ktps
      join users on user_ktps.user_id = users.id
      left join users as verified_user on user_ktps.verified_by_user_id = verified_user.id
      left join users as rejected_user on user_ktps.rejected_by_user_id = rejected_user.id
      left join user_cashes on user_cashes.user_id = user_ktps.user_id and user_cashes.cash_type = \'cash\'
      left join user_ktps as last_successful_user_ktps on last_successful_user_ktps.id = (
        select user_ktps_test.id
        from user_ktps as user_ktps_test
        where 
          user_ktps_test.user_id = user_ktps.user_id and 
          user_ktps_test.verified_at is not null and
          user_ktps_test.id < user_ktps.id
        order by user_ktps_test.verified_at desc
        fetch first 1 rows only
      )
      order by user_ktps.user_id desc, user_ktps.id desc, last_successful_user_ktps.id desc
    ) s'));

    if (isset($filters['search'])) {
      if (isset($filters['search']['user_id'])) {
        $query->where('s.user_id', $filters['search']['user_id']);
      }
      if (isset($filters['search']['ktp_status'])) {
        switch ($filters['search']['ktp_status']) {
          case 1:
            $query->whereNotNull('s.verified_at');
            break;
          case 2:
            $query->whereNotNull('s.rejected_at');
            break;
          case 3:
            $query->whereNull('s.verified_at')
              ->whereNull('s.rejected_at');
            break;
        }
      }
    }
    return $this->getResult($query->orderBy('s.id', 'desc'));
  }

  public function getUserKtpHistory($userId) {
    $filters = $this->getFilters();

    $query = $this->model->select(
      'user_ktps.*',
      'users.name',
      'users.telephone',
      'users.email',
      'verified_user.name as verified_by_user_name',
      'rejected_user.name as rejected_by_user_name',
      'last_successful_user_ktps.ktp_nik as last_ktp_nik',
      'last_successful_user_ktps.name as last_ktp_name',
      'last_successful_user_ktps.ktp_image_url as last_ktp_image_url',
      'last_successful_user_ktps.ktp_selfie_image_url as last_ktp_selfie_image_url'  
    )
      ->join('users', 'users.id', '=', 'user_ktps.user_id')
      ->leftJoin('users as verified_user', 'verified_user.id', '=', 'user_ktps.verified_by_user_id')
      ->leftJoin('users as rejected_user', 'rejected_user.id', '=', 'user_ktps.rejected_by_user_id')
      ->leftJoin('user_ktps as last_successful_user_ktps', 'last_successful_user_ktps.id', '=', \DB::raw('(
        select user_ktps_test.id
        from user_ktps as user_ktps_test
        where 
          user_ktps_test.user_id = user_ktps.user_id and 
          user_ktps_test.verified_at is not null and
          user_ktps_test.id < user_ktps.id
        order by user_ktps_test.verified_at desc
        fetch first 1 rows only
      )'))
      ->where('user_ktps.user_id', $userId);

    if (isset($filters['search'])) {
      if (isset($filters['search']['ktp_status'])) {
        switch ($filters['search']['ktp_status']) {
          case 1:
            $query->whereNotNull('verified_at');
            break;
          case 2:
            $query->whereNotNull('rejected_at');
            break;
          case 3:
            $query->whereNull('verified_at')
              ->whereNull('rejected_at');
            break;
        }
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }
}

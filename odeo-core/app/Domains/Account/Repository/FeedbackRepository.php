<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\Feedback;

class FeedbackRepository extends Repository {

  public function __construct(Feedback $feedback) {
    $this->model = $feedback;
  }
  
  public function gets() {

    $filters = $this->getFilters();

    $query = $this->model;

    if(isset($filters['search'])) {

      if (isset($filters['search']['feedback_id'])) {
        $query = $query->where('id', $filters['search']['feedback_id']);
      }
      if (isset($filters['search']['user_id'])) {
        $query = $query->where('user_id', $filters['search']['user_id']);
      }
      if (isset($filters['search']['feedback_message'])) {
        $query = $query->where('message', 'like', '%' . $filters['search']['feedback_message'] . '%');
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/3/16
 * Time: 10:51 PM
 */

namespace Odeo\Domains\Account\Repository;


use Odeo\Domains\Account\Model\UserHistory;
use Odeo\Domains\Core\Repository;

class UserHistoryRepository extends Repository {

  public function __construct(UserHistory $userHistory) {
    $this->model = $userHistory;
  }

}
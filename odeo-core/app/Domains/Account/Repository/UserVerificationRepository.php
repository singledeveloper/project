<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserVerification;

class UserVerificationRepository extends Repository {

  public function __construct(UserVerification $userVerification) {
    $this->model = $userVerification;
  }
  
}

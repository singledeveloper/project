<?php

namespace Odeo\Domains\Account\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Account\Model\UserBankAccount;

class UserBankAccountRepository extends Repository {

  public function __construct(UserBankAccount $bankAccount) {
    $this->model = $bankAccount;
  }
  
  public function get() {
    $filters = $this->getFilters();

    $query = $this->model;
    
    if ($this->hasExpand('user')) $query = $query->with('user');
    if ($this->hasExpand('bank')) $query = $query->with('bank');

    if (!isAdmin($filters)) $userId = $filters['auth']['user_id'];
    else {
      $userId = $filters['user_id'];
    }

    $query = $query->where('user_id', $userId);

    if (isset($filters['is_own_account'])) {
      $query = $query->where('is_own_account', $filters['is_own_account']);
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getByAccountNumber($accountNumber) {
    return $this->model->where('account_number', $accountNumber)->get();
  }

  public function findAccountNumberInUserId($userId, $accountNumber) {
    return $this->getCloneModel()->where('account_number', $accountNumber)
      ->where('user_id', $userId)
      ->first();
  }
}

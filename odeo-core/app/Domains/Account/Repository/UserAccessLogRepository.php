<?php

namespace Odeo\Domains\Account\Repository;

use Carbon\Carbon;
use Odeo\Domains\Account\Model\UserAccessLog;
use Odeo\Domains\Core\Repository;

class UserAccessLogRepository extends Repository {

  public function __construct(UserAccessLog $userAccessLog) {
    $this->model = $userAccessLog;
  }

  public function getTodayFirstLog($yesterday, $userIds = []) {
    $query = $this->model->where('user_access_logs.created_at', '>', Carbon::now()->subDay($yesterday)->format('Y-m-d') . ' 05:00:00')
      ->join('users', 'users.id', '=', 'user_access_logs.user_id')
      ->select('users.name', \DB::raw('min(user_access_logs.created_at) as first_created_at'))
      ->groupBy('users.name');

    if (count($userIds) > 0) $query = $query->whereIn('user_id', $userIds);
    return $query->get();
  }

}

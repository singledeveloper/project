<?php
/**
 * Created by PhpStorm.
 * User: William
 * Date: 24-Jan-18
 * Time: 2:03 PM
 */

namespace Odeo\Domains\Account;


use Odeo\Domains\Constant\FavoriteNumber;
use Odeo\Domains\Account\Repository\FavoriteNumberRepository;
use Odeo\Domains\Core\PipelineListener;

class FavoriteNumberCreator {
  private $favoriteNumberRepo, $favoriteNumberSelector;

  public function __construct() {
    $this->favoriteNumberRepo = app()->make(FavoriteNumberRepository::class);
    $this->favoriteNumberSelector = app()->make(FavoriteNumberSelector::class);
  }

  public function create(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    $favorite = $this->favoriteNumberRepo->getNew();
    $favorite->user_id = $userId;
    $favorite->name = $data['name'];
    $favorite->number = $data['number'];
    $favorite->service_id = $data['service_id'];
    $favorite->category = FavoriteNumber::getCategory($data['service_id']);
    if (isset($data['is_private'])) {
      $favorite->is_private = $data['is_private'];
    }
    if (isset($data['type'])) {
      $favorite->type = $data['type'];
    }

    if ($this->favoriteNumberRepo->save($favorite)) {
      return $listener->response(201,
        $this->favoriteNumberSelector->_transforms($favorite, $this->favoriteNumberRepo)
      );
    }
    return $listener->response(400, trans('errors.database'));
  }

}
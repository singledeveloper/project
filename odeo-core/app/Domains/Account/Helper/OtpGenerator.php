<?php
/**
 * Created by PhpStorm.
 * User: odeo
 * Date: 12/6/17
 * Time: 5:06 PM
 */

namespace Odeo\Domains\Account\Helper;


use Odeo\Domains\Constant\Platform;

class OtpGenerator {


  public function generateOtp($force4digits = false) {
//
//    $otp6digit =  isVersionSatisfy(Platform::IOS, '1.2.4')
//      || isVersionSatisfy(Platform::WEB_APP, '1.16.20')
//      || isVersionSatisfy(Platform::ANDROID, '2.0.5')
//      || isVersionSatisfy(Platform::WEB_STORE, '2.0.1');

    $otp6digit = !$force4digits && true;

    if (app()->environment() !== 'production') {
      return $otp6digit ? 123456 : 1234;
    }

    return mt_rand(
      $otp6digit ? 100000 : 1000,
      $otp6digit ? 999999 : 9999
    );
  }

}

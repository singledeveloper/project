<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 12:08 AM
 */

namespace Odeo\Domains\Account\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Account\Jobs\SendAccountblockEmail;
use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\ErrorStatus;
use Odeo\Exceptions\FailException;

class UserChecker {

  private $userRepo;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
  }

  public function checkPassword($user, $password) {
    try {
      $this->checkUserStatus($user);
      $this->checkPasswordIsBlocked($user);

      $passwordIsCorrect = $this->comparePassword($password, $user->password);
      $user->login_counts = $passwordIsCorrect ? 0 : $user->login_counts + 1;
      $user->last_attempt_login = Carbon::now();
      $this->userRepo->save($user);

      if ($passwordIsCorrect) {
        return [true, $user, null];
      }

      $this->checkPasswordIsBlocked($user);

      throw new FailException(trans('errors.invalid_password', [
        "count" => 3 - $user->login_counts % 3
      ]));

    } catch (FailException $e) {
      //temporary fix
      if ($user->type == 'admin') {
        return [false, trans('errors.invalid_email_or_password'), $e->getErrorStatus()];
      }

      return [false, $e->getMessage(), $e->getErrorStatus()];
    }
  }

  public function checkPin($user, $pin) {
    try {
      $this->checkUserStatus($user);
      $this->checkPinIsBlocked($user);

      $pinIsCorrect = $this->comparePassword($pin, $user->transaction_pin);
      $user->invalid_pin_attempt_count = $pinIsCorrect ? 0 : $user->invalid_pin_attempt_count + 1;
      $this->userRepo->save($user);

      if ($pinIsCorrect) {
        return [true, $user, null];
      }

      $this->checkPinIsBlocked($user);

      return [false, trans('errors.invalid_transaction_pin', [
        "count" => 3 - $user->invalid_pin_attempt_count
      ]), null];

    } catch (FailException $e) {
      return [false, $e->getMessage(), $e->getErrorStatus()];
    }
  }

  private function comparePassword($password, $comparator) {
    return Hash::check($password, $comparator);
  }

  private function checkUserStatus(User $user) {
    if ($user->status == UserStatus::PENDING) {
      throw new FailException(trans('errors.user_not_verified'));

    } else if (in_array($user->status, UserStatus::groupBanned())) {
      throw new FailException(trans('errors.account_banned'), ErrorStatus::ACCOUNT_BANNED);

    } else if ($user->status == UserStatus::DEACTIVATED) {
      throw new FailException(trans('errors.account_deactivated'));

    } else if ($user->status == UserStatus::RESET_PIN) {
      throw new FailException(trans('errors.account_reset_pin'));

    }
  }

  private function checkPinIsBlocked(User $user) {
    if ($user->invalid_pin_attempt_count >= UserStatus::LOGIN_BLOCKED_COUNT) {
      throw new FailException(trans('errors.pin_attempt_blocked'), ErrorStatus::ACCOUNT_BLOCKED);
    }
  }

  private function checkPasswordIsBlocked(User $user) {
    if ($user->login_counts && $user->login_counts % UserStatus::LOGIN_BLOCKED_COUNT == 0) {
      if ($this->isNearLastAttempt($user)) {
        //temporary fix
        if ($user->type == 'admin') {
          dispatch(new SendAccountblockEmail($user->email, $this->getThreshold($user)));
        }

        throw new FailException(trans('errors.account_blocked_try_again', [
          'count' => $this->getThreshold($user)
        ]), ErrorStatus::ACCOUNT_BLOCKED);
      }
    }
  }

  private function isNearLastAttempt($user) {
    if (!$user->last_attempt_login) {
      return false;
    }

    return Carbon::now()->diffInHours($user->last_attempt_login) < $this->getThreshold($user);
  }

  private function getThreshold($user) {
    $threshold = pow(2, $user->login_counts / 3 - 1);
    return min($threshold, 24);
  }

  public function checkPhoneNumber(PipelineListener $listener, $data) {
    $user = $this->userRepo->findByTelephone($data["phone_number"]);

    $response = [
      "exist" => !empty($user),
    ];

    if ($response['exist']) {
      $response['user_id'] = $user->id;
    }
    /*else {
      return $listener->response(400, "Tidak dapat registrasi nomor baru");
    }*/

    return $listener->response(200, $response);
  }

  public function checkPinExist(PipelineListener $listener, $data) {
    $user = $this->userRepo->findById($data["auth"]["user_id"]);

    $response = [
      "exist" => !empty($user->transaction_pin)
    ];

    return $listener->response(200, $response);
  }

  public function checkUserBanned(PipelineListener $listener, $data) {

    $user = $this->userRepo->findById($data['user_id']);

    if (in_array($user->status, UserStatus::groupBanned())) {
      return $listener->response(200, ['auth' => ['user_id' => $data['user_id']]]);
    }
    
    return $listener->response(400, trans('errors.invalid_authorization'));

  }
}


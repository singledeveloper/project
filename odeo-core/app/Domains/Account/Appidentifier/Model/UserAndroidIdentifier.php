<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/2/17
 * Time: 12:06 AM
 */

namespace Odeo\Domains\Account\Appidentifier\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class UserAndroidIdentifier extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }

}
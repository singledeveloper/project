<?php

namespace Odeo\Domains\Account;

use Illuminate\Support\Facades\Storage;
use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Core\PipelineListener;

class UserKycUpdater {

  private $userKycOptionRepo, $userKycRepo, $userKycDetailRepo;

  public function __construct() {
    $this->userKycOptionRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycOptionRepository::class);
    $this->userKycRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycRepository::class);
    $this->userKycDetailRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycDetailRepository::class);
  }

  public function upload(PipelineListener $listener, $data) {

    $option = $this->userKycOptionRepo->findById($data['option_id']);

    if (!$currentKyc = $this->userKycRepo->checkCurrentKyc($data['auth']['user_id'])) {
      $currentKyc = $this->userKycRepo->getNew();
      $currentKyc->user_id = $data['auth']['user_id'];
      $currentKyc->business_type = $option->business_type;
      $currentKyc->status = UserKycStatus::PENDING;
      $this->userKycRepo->save($currentKyc);
    }

    if ($currentKyc->business_type != $option->business_type) {
      return $listener->response(400, 'No access');
    }

    $isNewData = !isset($data['id']) || (isset($data['id']) && $data['id'] == '0');
    if ($isNewData) {
      $kycDetail = $this->userKycDetailRepo->getNew();
      $kycDetail->kyc_id = $currentKyc->id;
      $kycDetail->option_id = $data['option_id'];
    }
    else {
      $kycDetail = $this->userKycDetailRepo->findById($data['id']);
      if ($kycDetail->kyc_id != $currentKyc->id || $kycDetail->option_id != $data['option_id']) {
        return $listener->response(400, 'No access');
      }
    }

    $kycDetail->admin_note = '';

    if ($option->has_string && !isset($data['string_value'])) {
      $kycDetail->admin_note = 'Need to input name';
    }
    else if (isset($data['string_value'])) $kycDetail->string_value = $data['string_value'];

    if ($option->has_number && !isset($data['number_value'])) {
      $kycDetail->admin_note = 'Need to input document number';
    }
    else if (isset($data['number_value'])) $kycDetail->number_value = $data['number_value'];

    if ($option->has_date && !isset($data['date_value'])) {
      $kycDetail->admin_note = 'Need to input the date';
    }
    else if (isset($data['date_value'])) $kycDetail->date_value = date('Y-m-d', strtotime($data['date_value']));

    if ($option->has_file && !isset($data['file']) && $kycDetail->file_url == null) {
      $kycDetail->admin_note = 'Need to upload the file';
    }
    else if (isset($data['file'])) {
      $url = (isProduction() ? 'business-kyc' : 'temp_kyc') . '/' . $data['auth']['user_id'] . '_' . $data['option_id'] . '_' . time() . '.' . $data['file']->getClientOriginalExtension();
      if (!Storage::disk('s3')->put($url, file_get_contents($data['file']), 'private')) {
        $kycDetail->admin_note = 'Fail to upload the file. Please try again';
      }
      else {
        if (!$isNewData && $kycDetail->file_url != null) {
          $newKycDetail = $kycDetail->replicate();
          $kycDetail->kyc_status = UserKycStatus::CANCELLED;
          $this->userKycDetailRepo->save($kycDetail);
          $kycDetail = $newKycDetail;
        }
        $kycDetail->file_url = $url;
      }
    }

    $kycDetail->kyc_status = $kycDetail->admin_note != '' ? UserKycStatus::NOT_COMPLETE : UserKycStatus::PENDING;
    $this->userKycDetailRepo->save($kycDetail);

    return $listener->response(200, ForBusiness::createDetail($kycDetail, $option));
  }

  public function cancel(PipelineListener $listener, $data) {
    if ($currentKyc = $this->userKycRepo->checkCurrentKyc($data['auth']['user_id'])) {
      $currentKyc->status = UserKycStatus::CANCELLED;
      $this->userKycRepo->save($currentKyc);

      $this->userKycDetailRepo->cancelAll($currentKyc->id);
    }

    return $listener->response(200);
  }

  public function requestVerify(PipelineListener $listener, $data) {
    if (!$currentKyc = $this->userKycRepo->checkCurrentKyc($data['auth']['user_id'])) {
      return $listener->response(400, 'You don\'t fill KYC form yet');
    }

    if ($this->userKycDetailRepo->getStatusCount($currentKyc->id, UserKycStatus::PENDING) !=
      $this->userKycOptionRepo->getPassedOptionCount($currentKyc->business_type))
      return $listener->response(400, 'Verify request is rejected. Please make sure that all forms are checked.');

    $currentKyc->status = UserKycStatus::WAIT_VERIFY;
    $this->userKycRepo->save($currentKyc);

    return $listener->response(200, [
      'status' => $currentKyc->status
    ]);
  }

  public function manageApproval(PipelineListener $listener, $data) {

    if ($kycDetail = $this->userKycDetailRepo->findById($data['kyc_detail_id'])) {
      $kycDetail->kyc_status = $data['status'];
      if (isset($data['admin_note']) && $data['admin_note'] != '') $kycDetail->admin_note = $data['admin_note'];
      else $kycDetail->admin_note = null;

      $this->userKycDetailRepo->save($kycDetail);

      $kycUser = $this->userKycRepo->findById($kycDetail->kyc_id);
      if ($kycDetail->kyc_status == UserKycStatus::REJECTED) {
        $kycUser->rejected_at = date('Y-m-d H:i:s');
        $kycUser->rejected_by_user_id = $data['auth']['user_id'];
        $kycUser->status = UserKycStatus::REJECTED;
        $this->userKycRepo->save($kycUser);
      }
      else if ($kycDetail->kyc_status == UserKycStatus::VERIFIED &&
        $this->userKycDetailRepo->getStatusCount($kycDetail->kyc_id, UserKycStatus::VERIFIED) ==
        $this->userKycOptionRepo->getPassedOptionCount($kycUser->business_type)) {
        $kycUser->status = UserKycStatus::VERIFIED;
        $kycUser->verified_at =  date('Y-m-d H:i:s');
        $kycUser->verified_by_user_id = $data['auth']['user_id'];
        $this->userKycRepo->save($kycUser);
      }
      return $listener->response(200);
    }

    return $listener->response(400, 'No data');
  }

}

<?php

namespace Odeo\Domains\Account;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Helper\Currency;
use Carbon\Carbon;

class UserSelector implements SelectorListener {

  private $users, $withdraws, $currency;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->withdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->revenueManager = app()->make(\Odeo\Domains\Marketing\Helper\RevenueManager::class);
    $this->userKtpValidator = app()->make(\Odeo\Domains\Account\UserKtpValidator::class);
  }

  public function _transforms($item, Repository $repository) {
    $user = [];
    if ($item->id) $user["user_id"] = $item->id;
    $user["name"] = $item->name ?? '';
    if ($item->email) $user["email"] = $item->email;
    if ($item->telephone) $user["phone_number"] = $item->telephone;
    if ($item->type) $user["type"] = $item->type;
    if ($item->status) $user["status"] = $item->status;
    if ($item->created_at) $user["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->province_id) $user["province_id"] = $item->province_id;
    if ($item->city_id) $user["city_id"] = $item->city_id;
    if ($item->street) $user["street"] = $item->street;
    if ($item->postcode) $user["postcode"] = $item->postcode;
    $user["is_email_verified"] = $item->is_email_verified;
    $user["is_ktp_verified"] = $this->userKtpValidator->isVerified($item->id);
    if ($item->login_counts) $user["login_counts"] = $item->login_counts;
    if ($item->invalid_pin_attempt_count) $user["invalid_pin_attempt_count"] = $item->invalid_pin_attempt_count;
    if ($item->mail_verified_at) $user['mail_verified_at'] = $item->mail_verified_at;

    //if ($item->ktp_nik) $user["ktp_nik"] = substr_replace($item->ktp_nik, '************', 4, 12);
    //$user['ktp_image'] = isset($item->ktp_image_url) ? 'uploaded' : false;

    return $user;
  }

  public function _extends($data, Repository $repository) {
    if ($userIds = $repository->beginExtend($data, "user_id")) {
      $cash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
      if ($repository->hasExpand('deposit') || $repository->hasExpand('locked_deposit')) {
        $result = $cash->getUserDepositBalance($userIds, Currency::IDR, $lockeds);
        if ($repository->hasExpand('deposit')) $repository->addExtend('deposit', $result);
        if ($repository->hasExpand('locked_deposit')) $repository->addExtend('locked_deposit', $lockeds);
      }
      if ($repository->hasExpand('cash') || $repository->hasExpand('locked_cash')) {
        $result = $cash->getCashBalance($userIds, Currency::IDR, $lockeds);
        if ($repository->hasExpand('cash')) $repository->addExtend('cash', $result);
        if ($repository->hasExpand('locked_cash')) $repository->addExtend('locked_cash', $lockeds);
      }
      if ($repository->hasExpand('revenue')) {
        $result = $this->revenueManager->getUserRevenue($data['user_id'], Carbon::now(), Carbon::now(), $profits, $orderCounts);
        $repository->addExtend('revenue', $result);
      }
      if ($repository->hasExpand('total_withdrawal')) {
        $repository->addExtend('total_withdrawal', [$data['user_id'] => $this->currency->formatPrice($this->withdraws->getTotalWithdrawalByUserId($data['user_id']))]);
      }
      if ($repository->hasExpand('current_month_topup')) {
        $cashTransaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
        $result = $cashTransaction->getCurrentMonthTopup($userIds);
        $repository->addExtend('current_month_topup', [$data['user_id'] => $this->currency->formatPrice($result->total_amount ?? 0)]);
      }
      if ($repository->hasExpand('ktp_verification')) {
        $userKtp = app()->make(\Odeo\Domains\Account\Repository\UserKtpRepository::class);
        $result = $userKtp->findVerificationStatusByUserId($userIds);
        $repository->addExtend('ktp_verification', [$data['user_id'] => $result]);
      }
      if ($repository->hasExpand('maximum_cash')) {
        $cashLimiter = app()->make(\Odeo\Domains\Transaction\Helper\CashLimiter::class);
        $repository->addExtend('maximum_cash', [$data['user_id'] => $this->currency->formatPrice($cashLimiter->getMaximumUserCashAmount($data['user_id']))]);
      }
    }
    return $repository->finalizeExtend($data);
  }

  public function getUser(PipelineListener $listener, $data) {
    if (isset($data['auth']) && isset($data['auth']['user_id'])) {
      $redis = Redis::connection();
      $redis->hincrby('odeo_core:contact-check', $data['auth']['user_id'], 1);
    }

    $this->users->normalizeFilters($data);

    $users = [];
    foreach ($this->users->getByContact() as $item) {
      $user = $this->_transforms($item, $this->users);
      $user['name'] = maskMessage($user['name']);
      $users[] = $user;
    }

    if (sizeof($users) > 0) {
      return $listener->response(200, array_merge(
        ["users" => $this->_extends($users, $this->users)],
        $this->users->getPagination()
      ));
    }
    return $listener->response(204, ["users" => []]);
  }

  public function getAll(PipelineListener $listener, $data) {

    $this->users->normalizeFilters($data);
    $this->users->setSimplePaginate(true);

    $users = [];
    foreach ($this->users->gets() as $item) {
      $users[] = $this->_transforms($item, $this->users);
    }

    if (sizeof($users) > 0) {
      return $listener->response(200, array_merge(
        ["users" => $this->_extends($users, $this->users)],
        $this->users->getPagination()
      ));
    }

    return $listener->response(204, ["users" => []]);
  }


  public function summariesUser(PipelineListener $listener, $data) {
    $this->users->normalizeFilters($data);

    if ($user = $this->users->findById($data["user_id"])) {
      $user = $this->_transforms($user, $this->users);
      return $listener->response(200, $this->_extends($user, $this->users));
    }
    return $listener->response(204);
  }

  public function getUserDetail(PipelineListener $listener, $data) {
    $this->users->normalizeFilters($data);
    if (isset($data["auth"]) && $user = $this->users->findById($data["auth"]["user_id"])) {
      $user = $this->_transforms($user, $this->users);
      return $listener->response(200, $this->_extends($user, $this->users));
    }
    return $listener->response(204);
  }
}
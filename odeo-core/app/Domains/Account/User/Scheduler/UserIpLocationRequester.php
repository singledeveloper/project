<?php

namespace Odeo\Domains\Account\User\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Jobs\GetUserIpLocation;
use Odeo\Domains\Core\Task;

class UserIpLocationRequester {

  private $user;
  
  public function __construct() {
    $this->userLoginHistory = app()->make(\Odeo\Domains\Account\Repository\UserLoginHistoryRepository::class);
  }

  public function run() {
    $redis = Redis::connection();
    $key = 'odeo_core:user_login_history_offset';
    $limit = 100;

    if (!$redis->get($key)) {
      $startId = 0;
    } else {
      $startId = $redis->get($key);
    }

    $userLoginHistories = $this->userLoginHistory->getCloneModel()->limit($limit)->offset($startId)->get();
    
    foreach ($userLoginHistories as $userLoginHistory) {
      dispatch(new GetUserIpLocation([
        'user_id' => $userLoginHistory->user_id,
        'ip' => $userLoginHistory->ip_address
      ]));
    }

    $redis->set($key, $startId + $limit);
  }

}
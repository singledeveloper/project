<?php

namespace Odeo\Domains\Account\Scheduler;

use Odeo\Domains\Account\Device\DeviceRemover;
use Odeo\Domains\Account\Repository\UserDebtRepository;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Domains\Transaction\Repository\UserCashRepository;

class DebtChecker {

  private $userDebtRepo, $userCashRepo, $cashInserter;
  
  public function __construct() {
    $this->userDebtRepo = app()->make(UserDebtRepository::class);
    $this->userCashRepo = app()->make(UserCashRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
  }

  public function run() {
    foreach ($this->userDebtRepo->getUnprocessed() as $item) {
      $cash = $this->userCashRepo->findByUserId($item->user_id, CashType::OCASH);
      if ($cash->amount >= $item->amount) {
        $this->cashInserter->clear();
        $this->cashInserter->add([
          'user_id' => $item->user_id,
          'trx_type' => $item->trx_type,
          'cash_type' => CashType::OCASH,
          'amount' => -$item->amount,
          'data' => $item->order_id ? json_encode([
            "order_id" => $item->order_id
          ]) : '',
          'reference_type' => $item->reference_type,
          'reference_id' => $item->reference_id
        ]);

        try {
          $this->cashInserter->run();
          $item->is_processed = true;
          $this->userDebtRepo->save($item);
        }
        catch (InsufficientFundException $e) {}
      }
    }
  }

}
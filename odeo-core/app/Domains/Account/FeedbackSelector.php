<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/15/16
 * Time: 8:35 PM
 */

namespace Odeo\Domains\Account;


use Odeo\Domains\Account\Repository\FeedbackRepository;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class FeedbackSelector implements SelectorListener {

  private $feedbacks;

  public function __construct() {
    $this->feedbacks = app()->make(FeedbackRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output = [];

    if ($item->id) $output['id'] = $item->id;
    if ($item->user_id) $output['user_id'] = $item->user_id;
    if ($item->telephone) $output['phone_number'] = $item->telephone;
    if ($item->email) $output['email'] = $item->email;
    if ($item->message) $output['message'] = $item->message;
    if ($item->created_at) $output['created_at'] = $item->created_at->format("Y-m-d H:i:s");

    return $output;
  }

  public function getAll(PipelineListener $listener, $data) {

    $this->feedbacks->normalizeFilters($data);

    $orders = [];

    foreach ($this->feedbacks->gets() as $order) {
      $orders[] = $this->_transforms($order, $this->feedbacks);
    }

    if (sizeof($orders) > 0) {
      return $listener->response(200, array_merge(
        ["feedbacks" => $this->_extends($orders, $this->feedbacks)],
        $this->feedbacks->getPagination()
      ));
    }

    return $listener->response(204, ["feedbacks" => []]);

  }


}
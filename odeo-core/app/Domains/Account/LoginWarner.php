<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2019-01-16
 * Time: 19:25
 */

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Account\Jobs\SendNewLoginAlert;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\SMS\Job\SendSms;

class LoginWarner {

  public function warn(PipelineListener $listener, $data) {
    if (isOcashApp()) {
      $listener->pushQueue($this->produceJobEmailLoginAlert($data));
      $listener->pushQueue(new SendSms($this->produceSmsData($data)));
    } else {
      $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
        'email' => $data['email'],
        'job' => $this->produceJobEmailLoginAlert($data)
      ]));
    }
  }

  private function produceSmsData($data) {
    return [
      'sms_destination_number' => $data['phone_number'],
      'sms_text' => trans('user.new_login'),
      'sms_path' => SMS::NEXMO
    ];
  }

  private function produceJobEmailLoginAlert($data) {
    $ua = parse_user_agent();
    return new SendNewLoginAlert([
      'browser' => $this->getBrowser($ua, $data),
      'platform' => $ua['platform'],
      'to' => $data['email'],
      'number' => revertTelephone($data['phone_number']),
      'ip_address' => $data['ip_address'],
    ]);
  }

  private function getBrowser($ua, $data) {
    return in_array($data['platform_id'], Platform::getBrowserPlatform()) ? $ua['platform'] : '';
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/28/17
 * Time: 6:52 PM
 */

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Repository\CityRepository;
use Odeo\Domains\Core\PipelineListener;

class CitySelector {

  private $city;

  public function __construct() {
    $this->city = app()->make(CityRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    if($city = $this->city->getByProvinceId($data['province_id'])) return $listener->response(200, ['cities' => $city]);
    return $listener->response(204, ['cities' => []]);
  }

}
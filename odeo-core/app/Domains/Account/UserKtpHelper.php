<?php

namespace Odeo\Domains\Account;

use Validator;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Core\PipelineListener;

class UserKtpHelper {

  private $userKtpRepo;

  public function __construct() {
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
  }

  public function fillEmptyData(PipelineListener $listener, $data) {
    $userId = $data['user_id'] ?? $data['auth']['user_id'];
    $userKtp = $this->userKtpRepo->findLastByUserId($userId);

    if ($userKtp && $userKtp->rejected_at) {
      if (!isset($data['name'])) {
        $data['name'] = $userKtp->name;
      }
      if (!isset($data['ktp_nik'])) {
        $data['ktp_nik'] = $userKtp->ktp_nik;
      }
      if (!isset($data['ktp_image'])) {
        $data['string_ktp_image'] = true;
        $data['ktp_image'] = $userKtp->ktp_image_url;
      }
      if (!isset($data['ktp_selfie_image'])) {
        $data['string_ktp_selfie_image'] = true;
        $data['ktp_selfie_image'] = $userKtp->ktp_selfie_image_url;
      }
      if (!isset($data['last_ocash_balance'])) {
        $data['last_ocash_balance'] = $userKtp->last_ocash_balance;
      }
      if (!isset($data['last_transaction_description'])) {
        $data['last_transaction_description'] = $userKtp->last_transaction_description;
      }
      return $listener->response(200, $data);
    } else {
      $rules = [
        'ktp_nik' => 'size:16|required',
        'ktp_image' => 'image|required',
        'ktp_selfie_image' => 'image|required',
        'name' => 'required'
      ];
      if ($userKtp && $userKtp->last_ocash_balance) {
        $rules['last_ocash_balance'] = 'required';
        $rules['last_transaction_description'] = 'required';
      }

      $validator = Validator::make($data, $rules);

      if ($validator->fails()) {
        $errors = $validator->errors()->all();
        return $listener->response(400, $errors);
      }
    }
  }
}
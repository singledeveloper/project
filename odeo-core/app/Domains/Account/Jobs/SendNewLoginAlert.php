<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendNewLoginAlert extends Job  {

  protected $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    Mail::send('emails.new_device_alert', $this->data, function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to($this->data['to'])->subject('[ODEO] Login Baru dari ' . $this->data['browser'] . ' ' . $this->data['platform']);
    });
  }
}

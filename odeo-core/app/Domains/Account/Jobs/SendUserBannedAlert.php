<?php

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendUserBannedAlert extends Job {

  protected $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }


  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.user_banned_alert', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to('security@odeo.co.id', 'odeo')->subject('New User Banned - ' . time());
    });
  }
}

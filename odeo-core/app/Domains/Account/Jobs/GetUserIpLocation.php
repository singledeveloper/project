<?php

namespace Odeo\Domains\Account\Jobs;

use Odeo\Jobs\Job;

class GetUserIpLocation extends Job  {

  private $userIpLocations, $users, $userToken;
  protected $data;

  const BASE_URL = 'http://api.ipapi.com/api/';
  const API_KEY = 'e2fbbcb76caf3fe0920e4b40fef64044';

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->userIpLocations = app()->make(\Odeo\Domains\Account\Repository\UserIpLocationRepository::class);
    $this->userToken = app()->make(\Odeo\Domains\Account\Repository\UserTokenRepository::class);
  }

  public function handle() {
    $userIpLocation = $this->userIpLocations->findLatestByIp($this->data['ip']);
    $userToken = $this->userToken->findByUserIdAndIpAddress($this->data['user_id'], $this->data['ip']);

    if (!$userIpLocation || $userIpLocation->created_at->diffInDays(\Carbon\Carbon::now()) >= 90) {
      try {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', self::BASE_URL . $this->data['ip'], [
          'timeout' => 10,
          'query' => [
            'access_key' => self::API_KEY
          ]
        ]);

        if ($response->getStatusCode() == 200) {
          $result = $response->getBody()->getContents();
          
          $location = json_decode($result);

          if (isset($location->error)) {
            clog('ip_location', json_encode($location->error));
          } 

          $userIpLocations = $this->userIpLocations->getNew();
          $userIpLocations->user_id = $this->data['user_id'];
          $userIpLocations->ip = $this->data['ip'];
          $userIpLocations->city = $location->city ?? '';
          $userIpLocations->country = $location->country_name ?? '';
          $userIpLocations->country_code = $location->country_code ?? '';
          $userIpLocations->latitude = $location->latitude ?? 0;
          $userIpLocations->longitude = $location->longitude ?? 0;
          $userIpLocations->accuracy_radius = 0;

          $this->userIpLocations->save($userIpLocations);

          if ($userToken) {
            $userToken->login_country = $location->country_name;
            $this->userToken->save($userToken);
          }
        }

      } catch (\Exception $exception) {
        throw $exception;
      }
    } else if ($userIpLocation->user_id != $this->data['user_id']) {
      $userIpLocations = $userIpLocation->replicate();

      $userIpLocations->user_id = $this->data['user_id'];
      $userIpLocations->ip = $this->data['ip'];

      $this->userIpLocations->save($userIpLocations);
    }
    if ($userToken && $userIpLocation) {
      $userToken->login_country = $userIpLocation->country;
      $this->userToken->save($userToken);
    }
  }
}

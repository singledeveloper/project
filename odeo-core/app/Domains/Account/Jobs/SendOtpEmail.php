<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 10/05/18
 * Time: 15.47
 */

namespace Odeo\Domains\Account\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendOtpEmail extends Job{

  private $data, $email;

  public function __construct($email, $data) {
    parent::__construct();
    $this->email = $email;
    $this->data = $data;
  }

  public function handle() {
    Mail::send('emails.send_otp', $this->data, function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id');
      $m->to($this->email)->subject('[ODEO] Email Confirmation');
    });
  }
}

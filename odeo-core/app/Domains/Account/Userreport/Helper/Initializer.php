<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/19/17
 * Time: 9:59 PM
 */

namespace Odeo\Domains\Account\Userreport\Helper;

class Initializer {

  const DAY_NUM = 7;
  const YESTERDAY = 1;

  const DEFAULT_REGISTRATION_TARGET = 0;

}
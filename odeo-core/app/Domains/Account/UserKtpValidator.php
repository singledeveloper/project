<?php

namespace Odeo\Domains\Account;

use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Constant\UserType;

class UserKtpValidator {

  private $userKtpRepo, $userGroupRepo, $kycRepo;

  public function __construct() {
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
    $this->userGroupRepo = app()->make(UserGroupRepository::class);
    $this->kycRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycRepository::class);
  }

  public function isVerified($userId) {

    if ($this->userGroupRepo->findUserWithinGroup($userId, UserType::GROUP_BYPASS_KYC))
      return true;

    if ($kyc = $this->kycRepo->checkCurrentKyc($userId)) {
      if ($kyc->status == UserKycStatus::VERIFIED) return true;
    }

    $lastAttempt = $this->userKtpRepo->isVerified($userId);
    return !!$lastAttempt;
  }
}
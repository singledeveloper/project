<?php

namespace Odeo\Domains\Account\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Account\Model\User;

class UserBankAccount extends Entity {
  
  use SoftDeletes;
  
  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['bank_id', 'account_name', 'account_number', 'is_own_account', 'alias'];
  
  public $timestamps = true;
  
  /**
   * Get the bank of the account
   */
  public function bank()
  {
    return $this->belongsTo(Bank::class);
  }
  
  public function user()
  {
    return $this->belongsTo(User::class);
  }
}

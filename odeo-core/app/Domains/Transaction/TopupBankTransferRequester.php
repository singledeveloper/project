<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Account\Jobs\SendUserBannedAlert;
use Odeo\Domains\Account\UserHistoryCreator;
use Odeo\Domains\Constant\Gojek;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\TopupStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\Jobs\NotifyTopupSuccess;
use Odeo\Domains\Transaction\Jobs\SendTopupOverlimitAlert;

class TopupBankTransferRequester {

  private $topupBankTransfers;

  public function __construct() {
    $this->topupBankTransfers = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupBankTransferRepository::class);
  }

  public function toggle(PipelineListener $listener, $data) {
    if ($detail = $this->topupBankTransfers->findById($data['topup_bank_transfer_id'])) {
      $detail->is_suspected = !$detail->is_suspected;
      $this->topupBankTransfers->save($detail);
      return $listener->response(200);
    }

    return $listener->response(400, 'Data not exist');
  }

}
<?php

namespace Odeo\Domains\Transaction\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\ExportQueue;

class ExportQueueRepository extends Repository {

  public function __construct(ExportQueue $exportQueue) {
    $this->model = $exportQueue;
  }

  public function getExpired() {
    return $this->getCloneModel()->where('is_active', true)
      ->where('updated_at', '<=', Carbon::now()->subHour()->toDateTimeString())
      ->get();
  }

}

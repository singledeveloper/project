<?php

namespace Odeo\Domains\Transaction\Repository;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Model\CashRecurring;

class CashRecurringRepository extends Repository {

  public function __construct(CashRecurring $cashRecurring) {
    $this->model = $cashRecurring;
  }

  public function getLatest($userId, $data, $take = 5) {
    $query = $this->getCloneModel()->with(['bank'])
      ->where('user_id', $userId)
      ->where('is_active', true)
      ->where('is_enabled', true);

    if (isset($data['bank_id'])) $query->where('bank_id', $data['bank_id']);
    if (isset($data['bank_id_not_included'])) $query->where('bank_id', '<>', $data['bank_id_not_included']);

    return $query->skip(0)->take($take)->orderBy('id', 'desc')->get();
  }

  public function gets($userId) {
    return $this->getResult($this->getCloneModel()->with(['bank'])
      ->where('user_id', $userId)
      ->where('is_active', true)
      ->orderBy('id', 'desc'));
  }

  public function getRecurringQueues($take = 10) {
    return $this->model->where(function ($query) {

      $query->where('type', Recurring::TYPE_ONCE . '_' . date('Y-m-d') . '_' . date('G'))
        ->orWhere('type', Recurring::TYPE_DAY . '_' . date('G'))
        ->orWhere('type', Recurring::TYPE_WEEK . '_' . date('N') . '_' . date('G'))
        ->orWhere('type', Recurring::TYPE_MONTH . '_' . date('j') . '_' . date('G'));

      if (date('t') == date('j'))
        $query->orWhere('type', Recurring::TYPE_MONTH . '_end_' . date('G'));
      if (date('N') != 7)
        $query->orWhere('type', Recurring::TYPE_DAY_MON_SAT . '_' . date('G'));
      if (date('N') != 6 && date('N') != 7)
        $query->orWhere('type', Recurring::TYPE_DAY_WORK . '_' . date('G'));

    })->where(function($query3){
      $query3->where(\DB::raw('date(created_at)'), '<>', date('Y-m-d'))
        ->orWhere('type', 'like', Recurring::TYPE_ONCE . '_%');
    })
      ->where(function($query2){
        $query2->whereNull('last_executed_at')
          ->orWhere(\DB::raw('date(last_executed_at)'), '<>', date('Y-m-d'));
      })
      ->where('is_enabled', true)
      ->where('is_active', true)
      ->take($take)->select('id', 'type', 'created_at')->get();
  }

}

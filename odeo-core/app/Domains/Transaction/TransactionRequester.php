<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Biller\IDBiller\TransactionChecker;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Illuminate\Support\Facades\Redis;

class TransactionRequester {

  public function __construct() {
    $this->tempCash = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
    $this->cashTransactions = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->revenueInserter = app()->make(\Odeo\Domains\Marketing\Helper\RevenueInserter::class);
    $this->cashRevenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
  }

  public function refundOrder(PipelineListener $listener, $data) {
    $orderTransactions = $this->cashTransactions->findAllByAttributes('order_id', $data['order_id']);
    $this->cashInserter->clear();
    $this->revenueInserter->clear();
    $redis = Redis::connection();
    $cashRevenues = array();

    foreach($orderTransactions as $orderTransaction) {
      if(in_array($orderTransaction->trx_type, [
        TransactionType::ORDER_REFUND,
        TransactionType::PAYMENT,
        TransactionType::MANUAL_VOID,
        TransactionType::UNIQUE_CODE_REFUND
      ])) continue;
      if($trxData = json_decode($orderTransaction->data, true)) {
        unset($trxData['settlement_at']);
      }

      $this->cashInserter->add([
        'user_id' => $orderTransaction->user_id,
        'store_id' => $orderTransaction->store_id,
        'trx_type' => $orderTransaction->trx_type == TransactionType::SUPPLIER_CONVERT ?
          TransactionType::SUPPLIER_REFUND : TransactionType::REFUND,
        'cash_type' => $orderTransaction->cash_type,
        'amount' => -$orderTransaction->amount,
        'data' => $trxData ? json_encode($trxData) : '',
      ]);

      $storeId = $orderTransaction->store_id ?? ($trxData['store_id'] ?? false);
      if(!$storeId) continue;

      if(!isset($cashRevenues[$storeId])) {
        $cashRevenues[$storeId] = [
          'user_id' => $orderTransaction->user_id,
          'role' => $trxData['role'] ?? (isset($trxData['item_from_store_id']) ? 'agent' : ''),
          'is_community' => $trxData['is_community'] ?? false,
          'is_rush' => false
        ];
      }

      if($orderTransaction->trx_type == TransactionType::ORDER_CONVERT && $orderTransaction->cash_type == CashType::ORUSH) {
        $cashRevenues[$storeId]['is_rush'] = true;
      }

      if($orderTransaction->trx_type == TransactionType::ORDER_CONVERT && $orderTransaction->cash_type != CashType::ORUSH) {
        $cashRevenues[$storeId]['order_bonus'] = ($cashRevenues[$storeId]['order_bonus'] ?? 0) + $orderTransaction->amount;
        $cashRevenues[$storeId]['order_bonus_count'] = ($cashRevenues[$storeId]['order_bonus_count'] ?? 0) + 1;
      }

      if($orderTransaction->cash_type == CashType::OCASH && $orderTransaction->amount > 0) {
        $cashRevenues[$storeId]['order_revenue'] = $orderTransaction->amount;
      }

      if($orderTransaction->cash_type == CashType::OCASH && in_array($orderTransaction->trx_type, [
        TransactionType::RUSH_BONUS,
        TransactionType::MENTOR_RUSH_BONUS,
        TransactionType::LEADER_RUSH_BONUS
      ])) {
        $cashRevenues[$storeId]['rush_bonus'] = $orderTransaction->amount;
      }
    }

    foreach($cashRevenues as $storeId => $revenueData) {
      $commonData = [
        'user_id' => $revenueData['user_id'],
        'store_id' => $storeId,
        'order_id' => $data['order_id']
      ];

      foreach(['order_revenue', 'order_bonus', 'rush_bonus'] as $trxType) {
        if(isset($revenueData[$trxType])) {
          if($trxType == 'order_bonus' && $revenueData['order_bonus_count'] <= 1) continue;
          $this->cashRevenue->save(array_merge($commonData, [
            'amount' => -$revenueData[$trxType],
            'trx_type' => constant('\Odeo\Domains\Constant\TransactionType::' .
              strtoupper($revenueData['role'] . ($revenueData['role'] ? '_' : '') . $trxType)),
            'data' => '',
          ]));
        }
      }

      $this->revenueInserter->add([
        'store_id' => $storeId,
        'service_id' => $data['service_id'],
        'amount' => -($revenueData['order_revenue'] ?? 0),
        'margin' => -($revenueData['order_bonus'] ?? 0),
        'rush' => -($revenueData['rush_bonus'] ?? 0),
        'count' => -1,
        'is_free' => $revenueData['is_community']
      ]);

      if ($revenueData['is_community'] && !$revenueData['is_rush']) {
        $profitQueue = 'odeo_core:store_profit_queue:' . $data['service_id'];
        $key = $storeId . ':' . $data['service_detail_id'];
        $score = $redis->zscore($profitQueue, $key);
        $incAmount = -($revenueData['order_bonus'] ?? 0);
        if (isset($score)) {
          if ($score < 0) $incAmount += abs($score);
          $redis->zincrby($profitQueue, $incAmount, $key);
        }
        else {
          if (($val = $redis->hget('odeo_core:store_profit_temp', $key)) !== null) {
            if ($val < 0) $incAmount += abs($val);
            $redis->hincrby('odeo_core:store_profit_temp', $key, (int)$incAmount);
          }
        }
      }
    }
    $this->revenueInserter->run();
    $this->cashInserter->run();

    return $listener->response(200);
  }

  public function patch(PipelineListener $listener, $data) {
    $patchLogRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionPatchLogRepository::class);
    $referenceTrx = $this->cashTransactions->findById($data['id']);
    
    if ($referenceTrx && $referenceTrx->amount) {
      $patchedAmount = -$referenceTrx->amount;
      $trxData = json_decode($referenceTrx->data, true);
      unset($trxData['settlement_at']);
      $trxData['patched_by'] = $data['auth']['user_id'];

      if ($patchedAmount > 0) {
        $trxType = TransactionType::REFUND;
      } else {
        $trxType = TransactionType::MANUAL_VOID;

      }
      $this->cashInserter->clear();
      $this->cashInserter->add([
        'user_id' => $referenceTrx->user_id,
        'store_id' => $referenceTrx->store_id,
        'trx_type' => $trxType,
        'cash_type' => $referenceTrx->cash_type,
        'amount' => $patchedAmount,
        'data' => json_encode($trxData),
      ]);
  
      try {
        $ids = $this->cashInserter->run();

        $patchLog = $patchLogRepo->getNew();
        $patchLog->transaction_id = $ids[0];
        $patchLog->referred_transaction_id = $data['id'];
        $patchLog->patch_by_user_id = $data['auth']['user_id'];

        $patchLog->save();
      } catch(\Exception $e) {
        clog('patch_cash', $e->getMessage());
        return $listener->response(400);
      }

      return $listener->response(200);
    }

    return $listener->response(400, 'Transaction not found');


  }
}

<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Account\Jobs\SendUserBannedAlert;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Account\Repository\UserKtpRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Account\UserHistoryCreator;
use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\CashConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\TopupStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserStatus;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Notification\Helper\Builder;
use Odeo\Domains\Order\Jobs\SendSmsTransfer;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Transaction\Exception\InsufficientFundException;
use Odeo\Domains\Transaction\Helper\CashInserter;
use Odeo\Domains\Transaction\Helper\CashLimiter;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\CashRecurringManager;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\Jobs\SendTransferOcashComplete;
use Odeo\Domains\Transaction\Repository\CashTransactionRepository;
use Odeo\Domains\Transaction\Repository\UserTopupBankTransferRepository;
use Odeo\Domains\Transaction\Repository\UserTransferRepository;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;

class CashTransfer {

  private $userRepo, $userKtpRepo, $cashManager, $currency,
    $cashTransactionRepo, $cashLimiter, $userTopUpBankTransfers,
    $cashInserter, $notification, $orderRepo, $userTransferRepo, $userWithdrawRepo,
    $userHistoryCreator, $userKtpValidator, $cashRecurringManager;

  public function __construct() {
    $this->userRepo = app()->make(UserRepository::class);
    $this->userKtpRepo = app()->make(UserKtpRepository::class);
    $this->cashManager = app()->make(CashManager::class);
    $this->currency = app()->make(Currency::class);
    $this->cashTransactionRepo = app()->make(CashTransactionRepository::class);
    $this->cashLimiter = app()->make(CashLimiter::class);
    $this->userTopUpBankTransfers = app()->make(UserTopupBankTransferRepository::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->notification = app()->make(Builder::class);
    $this->orderRepo = app()->make(OrderRepository::class);
    $this->userTransferRepo = app()->make(UserTransferRepository::class);
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->userHistoryCreator = app()->make(UserHistoryCreator::class);
    $this->userKtpValidator = app()->make(UserKtpValidator::class);
    $this->cashRecurringManager = app()->make(CashRecurringManager::class);
  }


  public function transfer(PipelineListener $listener, $data) {
    try {
      $senderUser = $this->userRepo->findById($data['auth']['user_id']);
      $receiverUser = $this->getReceiverUser($data);
      $notes = $data['notes'] ?? '';

      $this->validateSelfTransfer($senderUser->id, $receiverUser->id);
      $this->validateHasKyc($senderUser->id);
      $this->validateMaximumCashout($senderUser->id, $data['amount']);
      $this->validateIfUserHasNotConfigureAgent($receiverUser->id, $data['amount']);

      if ($bannedReason = $this->validateIsInSuspectList($senderUser->id, $receiverUser)) {
        $listener->pushQueue(new SendUserBannedAlert([
          'user_id' => $receiverUser->id,
          'banned_reason' => $bannedReason
        ]));
      }

      $continue = $this->validateRecurringData($data);
      if (!$continue) return $listener->response(200, ['recurring_created' => true]);

      $this->validateIsEnoughBalance($senderUser->id, $data['amount']);

      $this->cashInserter->clear();

      $this->cashInserter->add($this->produceCashOutData($data, $senderUser, $receiverUser, $notes));
      $this->cashInserter->add($this->produceCashInData($data, $senderUser, $receiverUser, $notes));
      list($transferOutId, $transferInId) = $this->cashInserter->run();

      $this->notification->setup($senderUser->id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
      $this->notification->transfer_out($transferOutId, $data['amount'], $notes);

      $this->notification->setup($receiverUser->id, NotificationType::CHECK, NotificationGroup::TRANSACTION);
      $this->notification->transfer_in($transferInId, $data['amount'], $notes);

      $userTransfer = $this->userTransferRepo->getNew();

      $userTransfer->sender_user_id = $senderUser->id;
      $userTransfer->receiver_user_id = $receiverUser->id;
      $userTransfer->amount = $data['amount'];

      $this->userTransferRepo->save($userTransfer);

      if (isset($data['password'])) {
        isProduction() &&
        $listener->pushQueue($this->getSmsTransferJob($data, $senderUser->name));
      }

      $listener->pushQueue($this->notification->queue());

      $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
        'email' => $senderUser->email,
        'job' => new SendTransferOcashComplete($userTransfer, $senderUser, $receiverUser, $notes)
      ]));

    } catch (InsufficientFundException $e) {
      return $listener->response(400, trans('errors.cash.insufficient_cash', ['cash' => 'oCash']));
    } catch (CashTransferException $e) {
      return $listener->response(400, $e->getMessage());
    }

    return $listener->response(200, isset($transferOutId) ? ['id' => $transferOutId] : []);

  }

  private function validateIsEnoughBalance($userId, $transferAmount) {
    $balance = $this->cashManager->getCashBalance($userId);
    if (isset($balance[$userId]) && $balance[$userId] < $transferAmount) {
      throw new CashTransferException(trans('errors.cash.insufficient_cash', ['cash' => 'oCash']));
    }
  }

  private function getOverSettlementTransactionData($data, $receiverUser, $notes) {
    $temp = explode('#', $notes);

    if (isset($temp[1])) {

      $orderId = trim($temp[1]);

      $overSettlementRecord = $this->cashTransactionRepo->findByOrderId(
        $orderId,
        TransactionType::OVER_SETTLEMENT_REFUND
      );

      if ($overSettlementRecord && $overSettlementRecord->user_id != UserStatus::INTERNAL_OVER_SETTLEMENT_USER_ID) {
        throw new CashTransferException("Can't transfer this, order id not recorded.");
      }

      $order = $this->orderRepo->findById($orderId);

      if ($receiverUser->id != $order->user_id) {
        throw new CashTransferException('order ID bukan milik nomor telephone ' . $data['phone_number']);
      }
      if ($data['amount'] != $order->subtotal) {
        throw new CashTransferException('Total transfer tidak sama dengan harga barang');
      }

      return [
        'transaction_type' => TransactionType::OVER_SETTLEMENT_REFUND,
        'transaction_data' => [
          'notes' => $notes,
          'to' => $receiverUser->name ?? $receiverUser->telephone,
          'order_id' => $orderId
        ]
      ];
    }

    return [];
  }

  private function produceCashOutData($data, $senderUser, $receiverUser, $notes) {
    $transactionData = [];
    $transactionType = TransactionType::TRANSFER;

    if ($notes != '' && $senderUser->id == UserStatus::INTERNAL_OVER_SETTLEMENT_USER_ID) {
      $overSettlementData = $this->getOverSettlementTransactionData($data, $receiverUser, $notes);
      if ($overSettlementData) {
        $transactionType = $overSettlementData['transaction_type'];
        $transactionData = $overSettlementData['transaction_data'];
      }
    }

    if (empty($transactionData)) {
      $transactionData = [
        'notes' => $notes,
        'to' => $receiverUser->name ?? $receiverUser->telephone,
        'to_user_id' => $receiverUser->id
      ];
    }

    return [
      'user_id' => $senderUser->id,
      'trx_type' => $transactionType,
      'cash_type' => CashType::OCASH,
      'amount' => -$data['amount'],
      'data' => json_encode($transactionData)
    ];
  }

  private function produceCashInData($data, $senderUser, $receiverUser, $notes) {
    $transactionData = [
      'notes' => $notes,
      'from' => $senderUser->name,
      'from_user_id' => $senderUser->id
    ];

    if ($notes != '' && $senderUser->id == UserStatus::INTERNAL_OVER_SETTLEMENT_USER_ID) {
      $temp = explode('#', $notes);

      if (isset($temp[1])) {
        $orderId = trim($temp[1]);
        $transactionData['order_id'] = $orderId;
      }
    }

    return [
      'user_id' => $receiverUser->id,
      'trx_type' => TransactionType::TRANSFER,
      'cash_type' => CashType::OCASH,
      'amount' => $data['amount'],
      'data' => json_encode($transactionData)
    ];
  }

  private function getSmsTransferJob($data, $senderUserName) {
    $formattedAmount = $this->currency->formatPrice($data['amount']);

    return new SendSmsTransfer(
      $data['phone_number'], $data['password'], $formattedAmount,
      "transfer", $senderUserName
    );
  }

  private function validateIsInSuspectList($senderUserId, $receiverUser) {
    $suspectLists = $this->userTopUpBankTransfers->getSuspectList($senderUserId);
    foreach ($suspectLists as $item) {
      $userTopUpBankTransfer = $this->userTopUpBankTransfers
        ->findSame($receiverUser->id,
          $item->from_bank_id,
          $item->from_bank_account_name
        );

      if (!$userTopUpBankTransfer) {
        $userTopUpBankTransfer = $this->userTopUpBankTransfers->getNew();

        $userTopUpBankTransfer->user_id = $receiverUser->id;
        $userTopUpBankTransfer->from_bank_id = $item->from_bank_id;
        $userTopUpBankTransfer->from_bank_account_name = $item->from_bank_account_name;
        $userTopUpBankTransfer->route = 'cash_transfer';

        $this->userTopUpBankTransfers->save($userTopUpBankTransfer);
      }
    }

    if ($this->userTopUpBankTransfers->getUserIdSuspectCount($receiverUser->id) >= TopupStatus::TOPUP_SUSPECT_LIMIT) {
      $receiverUser->status = UserStatus::BANNED_RECEIVE_TRANSFER_FROM_BANK_SUSPECT;

      $this->userHistoryCreator->create($receiverUser);
      $this->userRepo->save($receiverUser);

      return 'User (' . $receiverUser->email . ') got transfer from another account that has bank transfer suspect 
          and because of it, has ' . TopupStatus::TOPUP_SUSPECT_LIMIT . ' bank transfer suspects or more within 5 days. 
          This user might be an agent and have connected to H2H but I just can\'t ignore this.';
    }

    return null;
  }

  private function validateMaximumCashout($userId, $cashOutAmount) {
    $userGroupRepo = app()->make(UserGroupRepository::class);

    if ($userGroupRepo->findUserWithinGroup($userId, [
      UserType::GROUP_BYPASS_WITHDRAW_LIMIT,
      UserType::GROUP_BYPASS_WITHDRAW_FEE
    ])) return;

    $sumTransfer = $this->userTransferRepo->sumTodayTransfer($userId);
    $sumWithdraw = $this->userWithdrawRepo->getTodayLimit($userId);

    $total = $sumTransfer + $sumWithdraw + $cashOutAmount;

    if ($total > CashConfig::MAXIMUM_CASH_OUT_PER_DAY) {
      throw new CashTransferException("Maximum cash out for today.");
    }
  }

  private function validateIfUserHasNotConfigureAgent($userId, $cashOutAmount) {
    if ($this->cashLimiter->userHasNotConfigureAgent($userId)) {

      list($isValid, $message) = $this->cashLimiter->check($userId, $cashOutAmount, 'Penerima');
      if (!$isValid) {
        throw new CashTransferException($message);
      }
    }
  }

  private function validateSelfTransfer($senderUserId, $receiverUserId) {
    if ($senderUserId == $receiverUserId) {
      throw new CashTransferException('Cannot transfer to yourself.');
    }
  }


  private function validateHasKyc($userId) {
    if (!$this->userKtpValidator->isVerified($userId)) {
      throw new CashTransferException('Anda tidak dapat melakukan transfer sebelum melakukan verifikasi KTP.');
    }
  }

  private function getReceiverUser($data) {
    $user = isset($data['user_id'])
      ? $this->userRepo->findById($data['user_id'])
      : $this->userRepo->findByTelephone($data['to_phone']);

    if (!$user) {
      throw new CashTransferException('User not found');
    }

    return $user;
  }

  private function validateRecurringData($data) {
    list($isError, $isContinue, $recData) = $this->cashRecurringManager->check($data);
    if ($isError) throw new CashTransferException($recData);
    if (!$isContinue) {
      $cashRecurringRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringRepository::class);
      $recurring = $cashRecurringRepo->getNew();
      $recurring->user_id = $data['auth']['user_id'];
      $recurring->transaction_type = TransactionType::TRANSFER;
      $recurring->bank_id = Bank::ODEO;
      $recurring->destination_number = $data['to_phone'];
      $recurring->type = $recData;
      $recurring->amount = $data['amount'];
      $recurring->template_notes = $data['notes'] ?? '';
      $recurring->is_enabled = true;
      $cashRecurringRepo->save($recurring);
      return false;
    }
    return true;
  }
}

class CashTransferException extends \Exception {

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/15/17
 * Time: 5:59 PM
 */

namespace Odeo\Domains\Transaction\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SuspectedWithdrawAlert extends Job  {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    Mail::send('emails.suspected_withdraw_alert', [
      'data' => $this->data
    ], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to('vincent.wu.vt@gmail.com')->subject('Recon Withdraw Suspected - ' . time());

    });

  }


}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Transaction\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendTopupOverlimitAlert extends Job  {

  use SerializesModels;

  private $data, $users, $currency, $push;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->push = app()->make(\Odeo\Domains\Notification\Helper\PushSender::class);
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    $user = $this->users->findById($this->data['user_id']);

    $topupAmount = $this->currency->formatPrice($this->data['amount']);

    $this->push->toUser([4350, 5193, 6096, 25551, 90051], [
      'messages' => [$user->name . ' has added ' . $topupAmount['formatted_amount'] . ' using ' . $this->data['payment_method']]
    ]);

    Mail::send('emails.topup_overlimit_alert', [
      'data' => [
        'user' => $user->toArray(),
        'topup_amount' => $topupAmount,
        'payment_method' => $this->data['payment_method']
      ]
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('replenishment@odeo.co.id')->subject('Priority Topup - ' . time());
    });

  }

}

<?php

namespace Odeo\Domains\Transaction\Jobs;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\PulsaRecurring\Helper\PulsaRecurringHelper;
use Odeo\Domains\Transaction\CashTransfer;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Jobs\Job;

class ExecuteCashRecurring extends Job  {

  use SerializesModels;

  private $recurringId;

  public function  __construct($recurringId) {
    parent::__construct();
    $this->recurringId = $recurringId;
  }

  public function handle() {

    $cashRecurringRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringRepository::class);
    $cashRecurringHistoryRepo = app()->make(\Odeo\Domains\Transaction\Repository\CashRecurringHistoryRepository::class);
    if ($recurring = $cashRecurringRepo->findById($this->recurringId)) {
      $recurringHistory = $cashRecurringHistoryRepo->getNew();
      $recurringHistory->cash_recurring_id = $recurring->id;
      $recurringHistory->reference_type = $recurring->transaction_type;
      if ($recurring->is_enabled) {
        $status = '';
        $message = null;
        $pipeline = new Pipeline;
        $executingData = [];
        switch ($recurring->transaction_type) {
          case TransactionType::TRANSFER: {
            if ($recurring->bank_id == Bank::ODEO) {
              $pipeline->add(new Task(CashTransfer::class, 'transfer'));
              $executingData = [
                'auth' => ['user_id' => $recurring->user_id],
                'amount' => $recurring->amount,
                'to_phone' => purifyTelephone($recurring->destination_number),
                'currency' => 'IDR',
                'notes' => $recurring->template_notes
              ];
            }
            else {
              $status = Recurring::FAILED;
              $message = 'Not valid transfer';
            }
            break;
          }
          case TransactionType::BANK_TRANSFER: {
            $pipeline->add(new Task(WithdrawRequester::class, 'request'));
            list($accNumber, $accName) = explode('::', $recurring->destination_number);
            $executingData = [
              'auth' => ['user_id' => $recurring->user_id],
              'cash' => [
                'amount' => $recurring->amount,
                'currency' => 'IDR'
              ],
              'bank_id' => $recurring->bank_id,
              'account_name' => $accName,
              'account_number' => $accNumber,
              'currency' => 'IDR',
              'description' => $recurring->template_notes,
              'is_bank_transfer' => true
            ];
            break;
          }
          default: {
            $status = Recurring::FAILED;
            $message = 'Not valid data';
          };
        }

        if (count($executingData) > 0) {
          $pipeline->execute($executingData);
          if ($pipeline->fail()) {
            $status = Recurring::FAILED;
            $message = $pipeline->errorMessage;
          }
          else {
            if (isset($pipeline->data['id'])) {
              $status = Recurring::COMPLETED;
              $recurringHistory->reference_id = $pipeline->data['id'];
            }
            else if (isset($pipeline->data['withdraw_id'])) {
              $status = Recurring::PENDING;
              $recurringHistory->reference_id = $pipeline->data['withdraw_id'];
            }
          }
        }
      }
      else {
        $status = Recurring::FAILED;
        $message = 'Scheduled not enabled/not active';
      }

      $recurringHistory->status = $status;
      $recurringHistory->error_message = $message;

      $cashRecurringHistoryRepo->save($recurringHistory);

      if (strpos($recurring->type, Recurring::TYPE_ONCE) === false) {
        $recurringHelper = app()->make(PulsaRecurringHelper::class);
        $recurring->next_execution_at = $recurringHelper->getNextPayment($recurring->type,
          $recurring->next_execution_at ? $recurring->next_execution_at : Carbon::now()->toDateTimeString());
        $cashRecurringRepo->save($recurring);
      }
    }
  }
}

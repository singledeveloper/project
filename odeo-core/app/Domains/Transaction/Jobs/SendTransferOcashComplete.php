<?php
namespace Odeo\Domains\Transaction\Jobs;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendTransferOcashComplete extends Job {

  use SerializesModels;

  private $userTransfer, $senderUser, $receiverUser, $notes, $currencyHelper;

  public function __construct($userTransfer, $senderUser, $receiverUser, $notes) {
    parent::__construct();
    $this->userTransfer = $userTransfer;
    $this->senderUser = $senderUser;
    $this->receiverUser = $receiverUser;
    $this->notes = $notes;
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function handle() {
    $data = [];
    $data['name'] = $this->senderUser['name'];
    $data['user_transfer_id'] = $this->userTransfer['id'];
    $data['amount'] = $this->currencyHelper->formatPrice($this->userTransfer['amount'])['formatted_amount'];
    $data['created_at'] = $this->userTransfer['created_at'];  
    $data['to'] = $this->receiverUser['name'];
    $data['to_phone'] = $this->receiverUser['telephone'];
    $data['notes'] = $this->notes;

    $email = $this->senderUser->email;
    $userTransferId = $this->userTransfer['id'];

    Mail::send('emails.transfer_ocash_complete', ['data' => $data], function ($m) use ($email, $userTransferId) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to($email)->subject('[ODEO] Transfer Ocash Complete - ' . $userTransferId);
    });
  }

}

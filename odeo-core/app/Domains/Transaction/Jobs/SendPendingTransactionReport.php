<?php

namespace Odeo\Domains\Transaction\Jobs;

use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendPendingTransactionReport extends Job {
  private $filters, $email, $selector;

  public function __construct($filters, $email) {
    parent::__construct();
    $this->filters = $filters;
    $this->email = $email;
  }

  public function handle() {
    
    $this->selector = app()->make(\Odeo\Domains\Transaction\RecentSelector::class);
    
    $transactions = app()->make(\Odeo\Domains\Transaction\Repository\UserTempCashRepository::class);
    $writer = WriterFactory::create($this->filters['file_type']);
    ob_start();

    $writer->openToFile('php://output');
    $transactions->normalizeFilters($this->filters);
    $report = $transactions->getPendingTransactionReport();

    if ($this->filters['type'] == 'deposit') {

      $writer->addRow([
        'Tanggal Transaksi',
        'Via',
        'Deskripsi',
        'Dari',
        'Ke',
        'Catatan',
        'Toko',
        'Kredit',
        'Debit',
        'ID Referensi'
      ]);
    } else {
      $writer->addRow([
        'Tanggal Transaksi',
        'Via',
        'Deskripsi',
        'Dari',
        'Ke',
        'Catatan',
        'Kredit',
        'Debit',
        'ID Referensi'
      ]);
    }

    while ($row = $report->fetch(\PDO::FETCH_OBJ)) {
      $transform = $this->selector->_transforms($row, $transactions);

      if ($this->filters['type'] == 'deposit') {
        $writer->addRow([
          $row->created_at,
          $transform['via'] ?? '-',
          $transform['description'],
          $transform['from'] ?? '-',
          $transform['to'] ?? '-',
          $transform['notes'] ?? '-',
          $row->store_name,
          $row->amount > 0 ? $row->amount : 0,
          $row->amount < 0 ? abs($row->amount) : 0,
          $row->reference_id ? $row->reference_id : '-'
        ]);
      } else {
        $writer->addRow([
          $row->created_at,
          $transform['via'] ?? '-',
          $transform['description'],
          $transform['from'] ?? '-',
          $transform['to'] ?? '-',
          $transform['notes'] ?? '-',
          $row->amount > 0 ? $row->amount : 0,
          $row->amount < 0 ? abs($row->amount) : 0,
          $row->reference_id ? $row->reference_id : '-'
        ]);
      }
    }

    $writer->close();

    $file = ob_get_clean();

    Mail::send('emails.email_transaction_report', [
      'data' => $this->filters,
      'title' => 'Laporan Mutasi Pending ' . ucwords($this->filters['type'])
    ], function ($m) use ($file) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->attachData($file, 'Laporan Mutasi Pending ' . ucwords($this->filters['type']) . ' ' . $this->filters['start_date'] . ' - ' . $this->filters['end_date'] . '.' . $this->filters['file_type']);

      $m->to($this->email)->subject('[ODEO] Laporan Mutasi Pending ' . ucwords($this->filters['type']));
    });
  }
}

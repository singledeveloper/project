<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/10/17
 * Time: 19:23
 */

namespace Odeo\Domains\Transaction\Helper;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\StoreTempDeposit;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Subscription\Model\Store;

class TempDepositHelper {


  public function __construct() {
    $this->storeTempDeposits = app()->make(\Odeo\Domains\Transaction\Repository\StoreTempDepositRepository::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  }

  public function refundAgentDeposit($orderId) {

    $tempDeposit = $this->storeTempDeposits->findByOrderId($orderId, StoreTempDeposit::AGENT_SERVER_ORDER);

    $this->cashInserter->clear();
    
    foreach ($tempDeposit as $tmp) {

      $desc = json_decode($tmp->description);

      $trxData = [
        'order_id' => $orderId,
        'recent_desc' => $desc->store_name,
      ];

      if (isset($desc->item_from_store_id)) {
        $trxData['item_from_store_id'] = $desc->item_from_store_id;
      }

      $this->cashInserter->add([
        'user_id' => $desc->store_user_id,
        'store_id' => $tmp->store_id,
        'trx_type' => TransactionType::ORDER_REFUND,
        'cash_type' => CashType::ODEPOSIT,
        'amount' => $tmp->amount,
        'data' => json_encode($trxData),
      ]);

    }

    $this->cashInserter->run();
  }

  public function deleteDeposit($orderId) {
    $this->storeTempDeposits->getModel()
      ->where('reference_id', $orderId)
      ->delete();
  }

  public function deleteOnlyAgentAndServerDeposit($orderId) {
    $this->storeTempDeposits->getModel()
      ->where('reference_id', $orderId)
      ->whereIn('type',  [
        StoreTempDeposit::AGENT_ORDER,
        StoreTempDeposit::AGENT_SERVER_ORDER,
        StoreTempDeposit::SUPPLIER_CASH
      ])
      ->delete();
  }

}
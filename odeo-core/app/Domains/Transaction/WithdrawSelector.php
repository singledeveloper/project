<?php

namespace Odeo\Domains\Transaction;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Transaction\Helper\Currency;

class WithdrawSelector implements SelectorListener {

  private $withdrawTnc, $withdraw;

  public function __construct() {
    $this->withdraw = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->withdrawTnc = app()->make(\Odeo\Domains\Transaction\Helper\WithdrawTnc::class);
  }

  public function _transforms($item, Repository $repository) {
    $withdraw = [];
    if ($item->id) $withdraw["withdraw_id"] = $item->id;
    if ($repository->hasExpand('user') && $user = $item->user) {
      $withdraw["user"] = [
        "id" => $item->user->id,
        "name" => $item->user->name,
        "email" => $item->user->email,
        "phone_number" => $item->user->telephone
      ];

      if ($repository->hasExpand('cash') && $cash = $this->cashManager->getCashBalance($user->id, Currency::IDR)) {
        $withdraw["cash"] = $cash[$user->id];
      }
    }

    if ($repository->hasExpand('clearing') && $bank = $item->bank) {
      $withdraw['clearing_code'] = $bank->clearing_code;
    }

    $withdraw['fee'] = $this->currency->formatPrice($item->fee);

    $withdraw["amount"] = $this->currency->formatPrice($item->amount);
    $withdraw['grand_total'] = $this->currency->formatPrice($item->amount - $item->fee);

    if ($item->account_bank_logo) $withdraw["account_bank_logo"] = baseUrl('images/payment/' . $item->account_bank_logo);
    if ($item->account_bank) $withdraw["account_bank"] = $item->account_bank;
    if ($item->account_name) $withdraw["account_name"] = $item->account_name;
    if ($item->account_number) $withdraw["account_number"] = $item->account_number;
    if ($item->description) $withdraw["description"] = $item->description;
    if ($item->status) {
      if ($item->status == Withdraw::PENDING) $status = "PENDING";
      else if ($item->status == Withdraw::COMPLETED) $status = "COMPLETED";
      else if ($item->status == Withdraw::CANCELLED) $status = "CANCELLED";
      else {
        $status = "PENDING";

        if (isAdmin()) {
          if ($item->status == Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
            $status = 'ON PROGRESS AUTO TRANSFER';
          } else if ($item->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER) {
            $status = 'PENDING PROGRESS AUTO TRANSFER';
          } else if ($item->status == Withdraw::FAIL_PROCESS_AUTO_TRANSFER) {
            $status = 'FAIL PROCESS AUTO TRANSFER';
          } else if ($item->status == Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER) {
            $status = 'CANCELLED FOR WRONG ACCOUNT NUMBER';
          } else if ($item->status == Withdraw::CANCELLED_FOR_CLOSED_BANK_ACCOUNT) {
            $status = 'CANCELLED FOR CLOSED BANK ACCOUNT';
          } else if ($item->status == Withdraw::CANCELLED_FOR_BANK_REJECTION) {
            $status = 'CANCELLED FOR BANK REJECTION';
          } else if ($item->status == Withdraw::WAIT_FOR_NOTIFY) {
            $status = 'WAIT FOR NOTIFY';
          } else if ($item->status == Withdraw::PENDING_PROGRESS_AUTO_TRANSFER_NOT_ENOUGH_DEPOSIT) {
            $status = 'PENDING PROGRESS AUTO TRANSFER NOT ENOUGH DEPOSIT';
          } else if ($item->status == Withdraw::CANCELLED_FOR_INVALID_ACCOUNT_INFORMATION) {
            $status = 'CANCELLED FOR INVALID ACCOUNT INFORMATION';
          } else if ($item->status == Withdraw::SUSPECT_CON) {
            $status = 'SUSPECT FOR CON';
          }
        }
      }
      $withdraw["status"] = $status;
    }
    if ($item->created_at) $withdraw["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $withdraw["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    if (isAdmin()) {

      $withdraw['process_by'] = $item->proccess_by ?? 'machine';
      $withdraw['auto_disbursement_vendor'] = $item->auto_disbursement_vendor;
      $withdraw['auto_disbursement_reference_id'] = $item->auto_disbursement_reference_id;

      $withdraw["verified_at"] = $item->verified_at;
      $withdraw["cancelled_at"] = $item->cancelled_at;

    }

    return $withdraw;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {
    $this->withdraw->normalizeFilters($data);
    $this->withdraw->setSimplePaginate(true);

    $withdraws = [];
    foreach ($this->withdraw->get() as $item) {
      $withdraws[] = $this->_transforms($item, $this->withdraw);
    }

    if (sizeof($withdraws) > 0) {

      return $listener->response(200, array_merge([
        "withdraws" => $this->_extends($withdraws, $this->withdraw),
      ],
        $this->withdraw->getPagination()
      ));
    }

    return $listener->response(204, ["withdraws" => []]);
  }

  public function getDetail(PipelineListener $listener, $data) {
    if ($withdraw = $this->withdraw->findLastWithdraw($data["auth"]["user_id"])) {
      $withdraw = $this->_transforms($withdraw, $this->withdraw);
      $withdraw['tnc'] = $this->withdrawTnc->getTnc();
      return $listener->response(200, $this->_extends($withdraw, $this->withdraw));
    }

    return $listener->response(204);
  }

  public function find(PipelineListener $listener, $data) {
    $this->withdraw->normalizeFilters($data);
  
    if ($withdraw = $this->withdraw->findById($data['withdraw_id'])) {
      $withdraw = $this->_transforms($withdraw, $this->withdraw);
      $withdraw['tnc'] = $this->withdrawTnc->getTnc();
      return $listener->response(200, $this->_extends($withdraw, $this->withdraw));
    }

    return $listener->response(204);
  }

  public function exportUserWithdrawReport($data) {
    $this->withdraw->normalizeFilters($data);
    $withdraws = $this->withdraw->getByUserPhones($data['phone_no']);

    $writer = WriterFactory::create(Type::CSV);
    $writer->openToBrowser('withdraws_' . implode('-', $data['phone_no']) . ($data['start_date'] ?? '0000-00-00') . ' to ' . ($data['end_date'] ?? Carbon::now()->toDateString()) . '.csv');

    $headers = ['telephone', 'id', 'amount', 'fee', 'account_bank', 'account_name', 'account_number', 'status', 'created_at', 'updated_at'];

    $writer->addRow($headers);

    foreach ($withdraws as $wd) {

      $temp = [];

      foreach ($headers as $h) {
        if ($h == 'status') {
          $statusCode = $wd[$h];
          if ($statusCode < Withdraw::SUSPECT_CON) {
            $status = 'PENDING';
          } else if ($statusCode == Withdraw::COMPLETED) {
            $status = 'SUCCESS';
          } else if ($statusCode > Withdraw::COMPLETED && $statusCode != Withdraw::SUSPECT) {
            $status = 'FAILED';
          } else {
            $status = 'SUSPECT';
          }
          $temp[] = $status;
        } else {
          $temp[] = $wd[$h];
        }
      }

      $writer->addRow($temp);
    }

    $writer->close();
  }
}

<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class CashUpdater {

  public function __construct() {
    $this->channel = app()->make(\Odeo\Domains\Channel\Repository\StoreChannelRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function pay(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    $balance = $this->cashManager->getCashBalance($userId);
    $amount = $this->currency->formatPrice($data['amount'], $data['currency'])['amount'];

    if (isset($balance[$userId]) && $balance[$userId] < $amount) {
      return $listener->response(400, trans('errors.cash.insufficient_cash', ['cash' => 'oCash']));
    }

    $this->revenue->save([
      'user_id' => $data['user_id'],
      'store_id' => $data['store_id'],
      'order_id' => NULL,
      'amount' => $amount,
      'data' => json_encode(["channel_id"=>$data['channel_id']]),
      'trx_type' => TransactionType::PAYMENT
    ]);

    $this->cashInserter->clear();
    $this->cashInserter->add([
      'user_id' => $userId,
      'trx_type' => TransactionType::PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => -$amount,
      'data' => json_encode([
        'channel_id' => $data['channel_id'],
        'recent_desc' => $data['channel_name'],
      ])
    ]);
    $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
    $this->notification->payment_out($amount, $data['channel_name']);

    $this->cashInserter->add([
      'user_id' => $data['user_id'],
      'trx_type' => TransactionType::PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => $amount,
      'data' => json_encode([
        'channel_id' => $data['channel_id'],
        'recent_desc' => $data['channel_name'],
      ])
    ]);
    $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
    $this->notification->payment_in($amount, $data['channel_name']);

    $this->cashInserter->run();
    $listener->pushQueue($this->notification->queue());

    return $listener->response(200);
  }

  public function deduct(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    $balance = $this->cashManager->getCashBalance($userId);
    $amount = $this->currency->formatPrice($data['amount'], $data['currency'])['amount'];

    if (isset($balance[$userId]) && $balance[$userId] < $amount) {
      return $listener->response(400, trans('errors.cash.insufficient_cash', ['cash' => 'oCash']));
    }

    $cashData = [];
    $cashData['order_id'] = $data['order_id'];
    if(isset($data['channel_id'])) {
      $cashData['channel_id'] = $data['channel_id'];
      $cashData['recent_desc'] = $data['channel_name'];
    }
    $this->cashInserter->clear();
    $this->cashInserter->add([
      'user_id' => $userId,
      'trx_type' => TransactionType::PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => -$amount,
      'data' => json_encode($cashData)
    ]);
    if(isset($data['channel_id'])) {
      $this->notification->setup($userId, NotificationType::ALPHABET, NotificationGroup::TRANSACTION);
      $this->notification->payment_out($amount, $data['channel_name']);
      $listener->pushQueue($this->notification->queue());
    }

    $this->cashInserter->run();

    return $listener->response(200);
  }

  public function cod(PipelineListener $listener, $data) {
    $order = $this->orders->findById($data['order_id']);
    $userId = $order->user->id;
    $sellerStore = $order->sellerStore;

    $this->cashInserter->clear();
    $this->cashInserter->add([
      'user_id' => $userId,
      'store_id' => $sellerStore->id,
      'trx_type' => TransactionType::ORDER_CONVERT,
      'cash_type' => CashType::ODEPOSIT,
      'amount' => -$data['total_deposit'],
      'data' => json_encode([
        'order_id' => $order->id,
        'recent_desc' => $sellerStore->name
      ]),
    ]);
    $this->cashInserter->add([
      'user_id' => $userId,
      'store_id' => $sellerStore->id,
      'trx_type' => TransactionType::PAYMENT,
      'cash_type' => CashType::OCOD,
      'amount' => isset($data['paid_amount']) ? $data['paid_amount'] : $data['total_revenue'],
      'data' => json_encode([
        'order_id' => $order->id,
        'recent_desc' => $sellerStore->name,
      ])
    ]);
    $this->cashInserter->run();

    $this->revenue->save([
      'user_id' => $userId,
      'store_id' => $sellerStore->id,
      'order_id' => $order->id,
      'amount' => isset($data['paid_amount']) ? $data['paid_amount'] : $data['total_revenue'],
      'trx_type' => TransactionType::PAYMENT,
      'data' => json_encode([
        'type' => CashType::OCOD
      ]),
    ]);

    return $listener->response(200);
  }

}

<?php

namespace Odeo\Domains\Transaction;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;

class TopupBankTransferSelector implements SelectorListener {

  private $topupBankTransfers;

  public function __construct() {
    $this->topupBankTransfers = app()->make(\Odeo\Domains\Transaction\Repository\UserTopupBankTransferRepository::class);
  }
  
  public function _transforms($item, Repository $repository) {
    return $item;
  }
  
  public function _extends($data, Repository $repository) {
    return $data;
  }
  
  public function get(PipelineListener $listener, $data) {
    $this->topupBankTransfers->setSimplePaginate(true);
    $this->topupBankTransfers->normalizeFilters($data);

    $temps = [];
    foreach($this->topupBankTransfers->gets() as $item) {
      $temps[] = [
        'id' => $item->id,
        'user_id' => $item->user_id,
        'user_name' => $item->user_name,
        'phone_number' => revertTelephone($item->telephone),
        'bank_name' => $item->bank_name,
        'bank_account_name' => $item->from_bank_account_name,
        'is_suspected' => $item->is_suspected,
        'created_at' => $item->created_at->format('Y-m-d H:i:s')
      ];
    }

    if (sizeof($temps) > 0)
      return $listener->response(200, array_merge(
        ["topup_bank_transfers" => $this->_extends($temps, $this->topupBankTransfers)],
        $this->topupBankTransfers->getPagination()
      ));
    
    return $listener->response(204, ["topup_bank_transfers" => []]);
  }
}

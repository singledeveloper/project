<?php
namespace Odeo\Domains\Transaction\Scheduler;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Transaction\Jobs\ExecuteCashRecurring;
use Odeo\Domains\Transaction\Repository\CashRecurringRepository;

class CashRecurringScheduler {

  private $cashRecurringRepo;

  public function __construct() {
    $this->cashRecurringRepo = app()->make(CashRecurringRepository::class);
  }

  public function run() {
    if (date('H') >= 7 && date('H') < 21) {
      foreach($this->cashRecurringRepo->getRecurringQueues() as $item) {
        $item->last_executed_at = date('Y-m-d H:i:s');
        if (strpos($item->type, Recurring::TYPE_ONCE) !== false) {
          $item->is_active = false;
        }
        $this->cashRecurringRepo->save($item);
        dispatch(new ExecuteCashRecurring($item->id));
      }
    }
  }

}

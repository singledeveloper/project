<?php

namespace Odeo\Domains\Transaction\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;

class StoreTempDeposit extends Entity
{

  public $fillable = [
    'store_id',
    'order_id',
    'amount'
  ];

  public function order() {
    return $this->belongsTo(Order::class, 'reference_id');
  }
}

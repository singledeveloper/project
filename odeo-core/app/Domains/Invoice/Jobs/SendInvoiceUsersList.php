<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 16/12/19
 * Time: 16.04
 */

namespace Odeo\Domains\Invoice\Jobs;


use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Invoice\Helper\InvoiceExporter;
use Odeo\Domains\Transaction\Helper\ExportDownloader;
use Odeo\Jobs\Job;

class SendInvoiceUsersList extends Job {

  protected $userId, $email, $exportId, $fileType;

  public function __construct($userId, $email, $exportId, $fileType) {
    parent::__construct();
    $this->userId = $userId;
    $this->email = $email;
    $this->exportId = $exportId;
    $this->fileType = $fileType;
  }

  public function handle() {
    $exporter = app()->make(InvoiceExporter::class);
    $exportQueueManager = app()->make(ExportDownloader::class);
    $file = $exporter->exportUsers(VirtualAccount::BILLER_ODEO, $this->userId);
    $dateNow = Carbon::now()->format('Y-m-d');

    $fileName = "Invoice User List $dateNow.$this->fileType";
    $exportQueueManager->update($this->exportId, $file, $fileName);

    Mail::send('emails.user-invoice.export_invoice_users_mail', [
      'date' => $dateNow
    ], function ($m) use ($file, $fileName) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->attachData($file, $fileName);
      $m->to($this->email)->subject('List User Invoice');
    });
  }

}
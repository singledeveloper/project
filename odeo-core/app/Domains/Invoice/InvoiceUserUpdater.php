<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/11/19
 * Time: 11.43
 */

namespace Odeo\Domains\Invoice;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;

class InvoiceUserUpdater {

  function __construct() {
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  function updateInvoiceUser(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    if (!$invoiceUser = $this->invoiceUsers->findByIdAndCreatedBy($data['id'], $userId)) {
      return $listener->response(400, trans("invoice/error.billed_user_not_found"));
    }

    $invoiceUser->name = $data['billed_user_name'] ?? '';
    $invoiceUser->email = $data['billed_user_email'] ?? '';
    $invoiceUser->telephone = $data['billed_user_telephone'] ?? '';
    $invoiceUser->address = $data['billed_user_address'] ?? '';
    $invoiceUser->created_by = $userId;
    $this->invoiceUsers->save($invoiceUser);

    return $listener->response(200, [
      'id' => $invoiceUser->id,
      'billed_user_id' => $invoiceUser->user_id,
      'billed_user_name' => $invoiceUser->name,
      'billed_user_email' => $invoiceUser->email,
      'billed_user_telephone' => $invoiceUser->telephone,
      'billed_user_address' => $invoiceUser->address,
    ]);
  }

}
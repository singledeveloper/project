<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 27/12/19
 * Time: 14.22
 */

namespace Odeo\Domains\Invoice\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayChannel;

class BusinessInvoiceUserSetting extends Entity {

  public function channel() {
    return $this->belongsTo(PaymentGatewayChannel::class, 'payment_gateway_channel_id');
  }

}
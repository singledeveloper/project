<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/11/19
 * Time: 15.46
 */

namespace Odeo\Domains\Invoice\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\UserInvoice\Model\UserInvoiceBiller;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Model\AffiliateUserInvoicePayment;

class AffiliateUserInvoice extends Entity {

  protected $casts = [
    'items' => 'array',
  ];

  protected $dates = ['due_date', 'created_at', 'updated_at', 'paid_at'];

  protected $guarded = ['biller_fee', 'payment_gateway_cost'];

  public function invoicePayments() {
    return $this->hasMany(AffiliateUserInvoicePayment::class, 'invoice_id', 'id');
  }

  public function biller() {
    return $this->belongsTo(UserInvoiceBiller::class, 'biller_id', 'id');
  }

}
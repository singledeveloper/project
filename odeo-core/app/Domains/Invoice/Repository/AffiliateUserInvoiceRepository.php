<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/11/19
 * Time: 15.46
 */

namespace Odeo\Domains\Invoice\Repository;


use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Invoice\Model\AffiliateUserInvoice;

class AffiliateUserInvoiceRepository extends Repository {

  function __construct(AffiliateUserInvoice $invoice) {
    $this->model = $invoice;
  }

  public function getAllByUser($billerId, $userId) {
    $filter = $this->getFilters();
    $q = $this->buildAndSelectQuery()
      ->where('user_id', $userId)
      ->where('biller_id', $billerId);

    if (isset($filter['id']) && $filter['id'] != '') {
      $q = $q->where('affiliate_user_invoices.id', $filter['id']);
    }

    if (isset($filter['billed_user_id']) && $filter['billed_user_id'] != '') {
      $q = $q->where('affiliate_user_invoices.billed_user_id', $filter['billed_user_id']);
    }

    if (isset($filter['billed_user_name']) && $filter['billed_user_name'] != '') {
      $q = $q->where('affiliate_user_invoices.billed_user_name', 'like', '%' . $filter['billed_user_name'] . '%');
    }

    if (isset($filter['status']) && $filter['status'] != '') {
      if ($filter['status'] == UserInvoice::OVERDUE) {
        $q = $q->where('affiliate_user_invoices.status', UserInvoice::OPENED)
          ->whereNotNull('due_date')->where('due_date', '<=', date('Y-m-d'));
      }
      else if ($filter['status'] == UserInvoice::VOID) {
        $q = $q->whereIn('affiliate_user_invoices.status', [UserInvoice::VOID, UserInvoice::CANCELLED]);
      }
      else if ($filter['status'] == UserInvoice::COMPLETED) {
        $q = $q->whereIn('affiliate_user_invoices.status', [UserInvoice::COMPLETED, UserInvoice::MERGED, UserInvoice::COMPLETED_WITH_EXTERNAL_PAYMENT]);
      }
      else $q = $q->where('affiliate_user_invoices.status', $filter['status']);
    }

    if (isset($filter['name']) && $filter['name'] != '') {
      $q = $q->where('affiliate_user_invoices.name', $filter['name']);
    }

    if (isset($filter['due_date']) && $filter['due_date'] != '') {
      $q = $q->where('affiliate_user_invoices.due_date', $filter['due_date']);
    }

    return $this->getResult($q->orderBy('id', 'desc'));
  }

  public function findOpenInvoiceUserInvoice($billedUserId, $userId) {
    return $this->getCloneModel()
      ->where('user_id', $userId)
      ->where('billed_user_id', $billedUserId)
      ->where('biller_id', UserInvoice::BILLER_ODEO)
      ->where('status', UserInvoice::OPENED)
      ->first();
  }

  public function getByBilledUserId($userId, $billedUserId) {
    return $this->buildAndSelectQuery()
      ->where('affiliate_user_invoices.user_id', $userId)
      ->where('affiliate_user_invoices.billed_user_id', $billedUserId)
      ->where('affiliate_user_invoices.biller_id', UserInvoice::BILLER_ODEO)
      ->get();
  }

  public function findByUserIdAndInvoiceId($userId, $id) {
    return $this->buildAndSelectQuery()
      ->where('affiliate_user_invoices.id', $id)
      ->where('affiliate_user_invoices.user_id', $userId)
      ->first();
  }

  public function findByInvoiceNumber($invoiceNumber) {
    return $this->getCloneModel()
      ->where('invoice_number',  $invoiceNumber)
      ->first();
  }

  public function getByInvoiceNameAndCustomerName($invoiceNames, $billedUserName) {
    return $this->getCloneModel()
      ->whereIn('name', $invoiceNames)
      ->whereRaw('trim(billed_user_name) = ?', [$billedUserName])
      ->orderBy('id')
      ->get();
  }

  private function buildAndSelectQuery() {
    $q = $this->getCloneModel()
      ->leftJoin('payment_gateway_invoice_payments', function($join) {
        $join->on('payment_gateway_invoice_payments.invoice_id', '=', 'affiliate_user_invoices.id')
          ->whereNotNull('payment_gateway_invoice_payments.paid_at');
      });

    $q = $q->select('affiliate_user_invoices.*', 'payment_gateway_invoice_payments.settlement_at', 'payment_gateway_invoice_payments.settlement_status');
    return $q;
  }

}
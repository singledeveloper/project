<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/11/19
 * Time: 14.25
 */

namespace Odeo\Domains\Invoice;


use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceUserRepository;
use Odeo\Domains\VirtualAccount\VirtualAccountManager;

class InvoiceUserRequester extends VirtualAccountManager {

  private $invoiceUsers;

  function __construct() {
    parent::__construct();
    $this->invoiceUsers = app()->make(AffiliateUserInvoiceUserRepository::class);
  }

  public function addInvoiceUser(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];
    if (isset($data['billed_user_id']) && $this->invoiceUsers->findUserByCreatedBy(UserInvoice::BILLER_ODEO, $data['billed_user_id'], $userId)) {
      return $listener->response(400, trans('invoice/error.user_id_exists'));
    }

    $invoiceUser = $this->invoiceUsers->getNew();
    $invoiceUser->biller_id = UserInvoice::BILLER_ODEO;
    $invoiceUser->user_id = $data['billed_user_id'] ?? time();
    $invoiceUser->name = $data['billed_user_name'] ?? '';
    $invoiceUser->email = $data['billed_user_email'] ?? '';
    $invoiceUser->telephone = $data['billed_user_telephone'] ?? '';
    $invoiceUser->address = $data['billed_user_address'] ?? '';
    $invoiceUser->created_by = $userId;
    $this->invoiceUsers->save($invoiceUser);

    $this->createVaUser($invoiceUser->user_id, $userId);

    return $listener->response(200, [
      'id' => $invoiceUser->id,
      'billed_user_id' => $invoiceUser->user_id,
      'billed_user_name' => $invoiceUser->name,
      'billed_user_email' => $invoiceUser->email,
      'billed_user_telephone' => $invoiceUser->telephone,
      'billed_user_address' => $invoiceUser->address,
    ]);
  }

}
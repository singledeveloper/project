<?php

namespace Odeo\Domains\Supply;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;

class TemplateResponseGroupMaker {

  private $vendorSwitcherTemplateResponses, $vendorSwitcherTemplates, $supplyValidator, $vendorSwitchers, $templateCache;

  public function __construct() {
    $this->vendorSwitcherTemplates = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateRepository::class);
    $this->vendorSwitcherTemplateResponses = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->templateCache = app()->make(\Odeo\Domains\Supply\Helper\CacheManager::class);
  }

  public function setResponseGroup(PipelineListener $listener, $data) {

    if (!isAdmin() && $data['vendor_switcher_id'] == '0')
      return $listener->response(400, 'Invalid data.');

    if ($data['vendor_switcher_id'] != '0') {
      $biller = $this->vendorSwitchers->findById($data['vendor_switcher_id']);
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);
    }

    if (!in_array($data['format_type'], Supplier::getFormats()))
      return $listener->response(400, 'Format tidak valid.');
    if (!in_array($data['route_type'], Supplier::getRoutes()))
      return $listener->response(400, 'Route tidak valid.');

    $template = $this->vendorSwitcherTemplates->findById($data['template_id']);
    if ($template->format_type == Supplier::TYPE_TEXT)
      return $listener->response(400, 'Anda tidak dapat menambahkan grup di template dengan format ' . Supplier::TYPE_TEXT);

    $templateResponse = $this->vendorSwitcherTemplateResponses->getNew();
    $templateResponse->template_id = $data['template_id'];
    $templateResponse->predefined_vendor_switcher_id = $data['vendor_switcher_id'] == '0' ? null : $data['vendor_switcher_id'];
    $templateResponse->format_type = $data['format_type'];
    $templateResponse->route_type = $data['route_type'];

    $this->vendorSwitcherTemplateResponses->save($templateResponse);

    return $listener->response(200);
  }

  public function removeGroup(PipelineListener $listener, $data) {
    if ($templateResponse = $this->vendorSwitcherTemplateResponses->findById($data['template_response_id'])) {

      if (!isAdmin()) {
        list ($isValid, $message) = $this->supplyValidator->checkStore($templateResponse->vendorSwitcher->owner_store_id, $data);
        if (!$isValid) return $listener->response(400, $message);
      }

      if ($templateResponse->template->format_type == Supplier::TYPE_TEXT)
        return $listener->response(400, 'Anda tidak dapat menghapus grup ini.');

      $templateResponse->is_active = false;
      $this->vendorSwitcherTemplateResponses->save($templateResponse);
      $this->templateCache->resetResponse($templateResponse->template_id, $templateResponse->predefined_vendor_switcher_id, $templateResponse->route_type);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

}

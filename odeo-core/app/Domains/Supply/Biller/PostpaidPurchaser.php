<?php

namespace Odeo\Domains\Supply\Biller;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Supply\Helper\BillerStatusUpdater;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class PostpaidPurchaser extends BillerStatusUpdater {

  private $client, $inquiryHistories, $inquirer;

  public function __construct() {
    parent::__construct();
    $this->client = app()->make(\Odeo\Domains\Supply\Formatter\ClientParser::class);
    $this->inquiryHistories = app()->make(\Odeo\Domains\Supply\Repository\PostpaidInquiryHistoryRepository::class);
    $this->inquirer = app()->make(Inquirer::class);
  }

  public function purchase(PipelineListener $listener, $data) {

    $this->initiate($listener, $data);

    $continue = false;
    if (!$inquiry = $this->inquiryHistories->findRecentInquiry($this->currentSwitcher->number, $this->currentOrder->user_id,
      $this->currentSwitcher->current_inventory_id, Supplier::POSTPAID_INQ_TYPE_PRE_PURCHASE)) {
      $data['vendor_switcher_id'] = $this->currentBiller->id;
      $data['user_id'] = $this->currentOrder->user_id;
      if ($inquiry = $this->inquirer->inquiry($listener, $data)) {
        if ($inquiry->status == SwitcherConfig::BILLER_IN_QUEUE)
          $this->currentRecon->response = 'Wait for inquiry re-check';
        else if ($inquiry->status == SwitcherConfig::BILLER_SUCCESS) $continue = true;
        else $this->currentRecon->response = $inquiry->response;
        $this->currentRecon->status = $inquiry->status;
      }
      else {
        $this->currentRecon->response = 'Inquiry re-check error';
        $this->currentRecon->response = SwitcherConfig::BILLER_FAIL;
      }
    }
    else $continue = true;

    if ($continue) {
      $result = json_decode($inquiry->result, true);
      $basePrice = $this->currentPulsa->owner_store_id ? $this->currentPulsa->price : $this->currentPulsaInventory->base_price;
      if ($this->currentSwitcher->current_base_price != Supplier::checkPostpaidTotal($result) + $basePrice) {
        $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
        $this->currentRecon->response = 'No match price';
      }
      $this->currentRecon->base_price = $this->currentSwitcher->current_base_price;
      $this->reconRepository->save($this->currentRecon);

      if ($connection = $this->vendorSwitcherConnections->findCacheByVendorSwitcherId($this->currentPulsaInventory->vendor_switcher_id)) {
        $template = $connection->template;

        $this->currentRecon->destination = $connection->server_destination;
        $this->currentRecon->format_type = $template->format_type;

        $temp = explode('.', $this->currentPulsaInventory->code);

        $dataToTranslate = [
          'user_id' => $connection->server_user_id,
          'password' => $connection->server_password ? Crypt::decrypt($connection->server_password) : null,
          'pin' => $connection->server_pin ? Crypt::decrypt($connection->server_pin) : null,
          'token' => $connection->server_token ? Crypt::decrypt($connection->server_token) : null,
          'denom' => isset($temp[1]) ? $temp[1] : $temp[0],
          'base_price' => $this->currentPulsaInventory->base_price,
          'service_detail_id' => $this->currentPulsa->service_detail_id
        ];
        $encryptedBody = [];

        $body = json_decode($template->body_details, true);
        $headers = json_decode($template->headers, true);
        if (!$headers) $headers = [];

        foreach ($body as $key => $value) {
          $body[$key] = $this->translateRequest($value, $dataToTranslate);
          $encryptedBody[$key] = $this->translateRequest($value, array_merge($dataToTranslate, ['encrypt' => true]));
        }
        foreach ($headers as $key => $value) $headers[$key] = $this->translateRequest($value, $dataToTranslate);

        $body = $this->typeFormatter->setPath($template->format_type)->constructRequest($body, $template->body_title);
        $encryptedBody = $this->typeFormatter->setPath($template->format_type)->constructRequest($encryptedBody, $template->body_title);

        $this->currentRecon->request = is_array($encryptedBody) ? http_build_query($encryptedBody) : $encryptedBody;

        list($isValid, $response) = $this->client->parse($template->format_type, $connection->server_destination, $body, $headers);

        if (!$isValid) {
          $this->currentRecon->response = $response;
          if (strpos($this->currentRecon->response, 'Failed to connect to') !== false ||
              strpos($this->currentRecon->response, 'Could not resolve host') !== false)
            $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
          else $this->currentRecon->status = SwitcherConfig::BILLER_TIMEOUT;
        } else if ($response) {
          $this->currentRecon->response = $response;
          foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($template->id, $this->currentBiller->id, Supplier::ROUTE_RESPONSE) as $item) {
            if ($this->translateResponse($item, $response)) break;
          }

          if (!isset($this->translateResult[Supplier::SF_PARAM_STATUS]))
            $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
        } else {
          $this->currentRecon->response = 'No response from biller';
          $this->currentRecon->status = SwitcherConfig::BILLER_SUSPECT;
        }
      } else {
        $this->currentRecon->response = 'No connection available';
        $this->currentRecon->status = SwitcherConfig::BILLER_FAIL;
      }
    }

    return $this->supplyFinalize($listener);

  }

}

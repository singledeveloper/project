<?php

namespace Odeo\Domains\Supply;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;

class TemplateResponseMaker {

  private $vendorSwitcherTemplateResponses, $vendorSwitcherTemplateResponseDetails, $supplyValidator, $vendorSwitchers, $templateCache;

  public function __construct() {
    $this->vendorSwitcherTemplateResponses = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseRepository::class);
    $this->vendorSwitcherTemplateResponseDetails = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->templateCache = app()->make(\Odeo\Domains\Supply\Helper\CacheManager::class);
  }

  public function setResponse(PipelineListener $listener, $data) {

    if (!isAdmin() && $data['vendor_switcher_id'] == '0')
      return $listener->response(400, 'Invalid data.');

    if ($data['vendor_switcher_id'] != '0') {
      $biller = $this->vendorSwitchers->findById($data['vendor_switcher_id']);
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);
    }

    if (!array_key_exists($data['response_status_type'], Supplier::getStatuses()))
      return $listener->response(400, 'Status respon tidak valid.');

    if (isset($data['template_response_id']))
      $templateResponse = $this->vendorSwitcherTemplateResponses->findById($data['template_response_id']);
    else {
      if (!$templateResponse = $this->vendorSwitcherTemplateResponses->findByTemplateId($data['template_id'], $data['vendor_switcher_id'], Supplier::ROUTE_BOTH)) {
        $templateResponse = $this->vendorSwitcherTemplateResponses->getNew();
        $templateResponse->template_id = $data['template_id'];
        $templateResponse->predefined_vendor_switcher_id = $biller->id;
        $templateResponse->format_type = Supplier::TYPE_TEXT;
        $templateResponse->route_type = Supplier::ROUTE_BOTH;
        $this->vendorSwitcherTemplateResponses->save($templateResponse);
      }
      $data['template_response_id'] = $templateResponse->id;
    }

    $specialOutputs = $newResponses = [];

    if ($templateResponse->format_type == Supplier::TYPE_TEXT) {
      if (!is_string($data['response_details'])) return $listener->response(400, 'Detail Kondisi harus tipe String jika grup menggunakan format ' . Supplier::TYPE_TEXT . '.');
      try {
        preg_match_all(Supplier::SF_PATTERN, trim($data['response_details']), $matches);
        foreach ($matches[1] as $output) {
          if (!in_array(strtolower($output), Supplier::SF_RESPONSES))
            return $listener->response(400, 'Format ' . Supplier::addPattern($output) . ' tidak valid. Mohon gunakan format yang terdaftar di ODEO.');

          $data['response_details'] = str_replace(Supplier::addPattern($output), Supplier::addPattern(strtolower($output)), $data['response_details']);

          if (!in_array( strtolower($output), $specialOutputs))
            $specialOutputs[] = strtolower($output);
        }
      }
      catch (\Exception $e) {}

      $newResponses = $data['response_details'];
    }
    else {
      foreach ($data['response_details'] as $item) {
        if (!isset($item['param']) || !isset($item['value']))
          return $listener->response(400, 'Data Response tidak lengkap.');

        try {
          preg_match_all(Supplier::SF_PATTERN, trim($item['value']), $matches);
          foreach ($matches[1] as $output) {
            if (!in_array(strtolower($output), Supplier::SF_RESPONSES))
              return $listener->response(400, 'Format ' . Supplier::addPattern($output) . ' tidak valid. Mohon gunakan format yang terdaftar di ODEO.');

            $item['value'] = str_replace(Supplier::addPattern($output), Supplier::addPattern(strtolower($output)), $item['value']);

            if (!in_array( strtolower($output), $specialOutputs))
              $specialOutputs[] = strtolower($output);
          }
        }
        catch (\Exception $e) {}

        $newResponses[trim($item['param'])] = trim($item['value']);
      }
    }

    if ($templateResponse->route_type == Supplier::ROUTE_REPORT) {
      if (!in_array(Supplier::SF_PARAM_REFID, $specialOutputs) && !in_array(Supplier::SF_PARAM_TRXID, $specialOutputs)) {
        if ($templateResponse->format_type == Supplier::TYPE_TEXT) {
          if (!in_array(Supplier::SF_PARAM_CODE, $specialOutputs) && !in_array(Supplier::SF_PARAM_MSISDN, $specialOutputs)) {
            return $listener->response(400, 'Reply dalam bentuk ' . Supplier::TYPE_TEXT . ' harus mempunyai format ' .
              Supplier::addPattern(Supplier::SF_PARAM_CODE) . ' dan ' . Supplier::addPattern(Supplier::SF_PARAM_MSISDN) .
              ' jika tidak mempunyai referensi ' . Supplier::addPattern(Supplier::SF_PARAM_TRXID));
          }
        }
        else return $listener->response(400, 'Reply harus mempunyai format minimal salah satu dari ' .
          Supplier::addPattern(Supplier::SF_PARAM_REFID) . ' atau ' . Supplier::addPattern(Supplier::SF_PARAM_TRXID));
      }
      else if (in_array(Supplier::SF_PARAM_REFID, $specialOutputs) && !in_array(Supplier::SF_PARAM_TRXID, $specialOutputs)) {
        if ($templateResponse->format_type == Supplier::TYPE_TEXT)
          return $listener->response(400, 'Format ' . Supplier::TYPE_TEXT . ' tidak dapat menggunakan ' . Supplier::addPattern(Supplier::SF_PARAM_REFID) . ' sebagai referensi reply.');
        foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($templateResponse->template_id, $data['vendor_switcher_id'], Supplier::ROUTE_RESPONSE) as $item) {
          if ($details = json_decode($item->response_details)) {
            $responseSpecialOutputs = [];
            foreach ($details as $output) {
              try {
                preg_match_all(Supplier::SF_PATTERN, $output, $matches);
                foreach ($matches[1] as $value) {
                  if (!in_array(strtolower($value), $responseSpecialOutputs))
                    $responseSpecialOutputs[] = strtolower($value);
                }
              }
              catch (\Exception $e) {}
            }
            if (!in_array(Supplier::SF_PARAM_REFID, $responseSpecialOutputs))
              return $listener->response(400, 'Anda tidak dapat mengkondisikan ' . Supplier::addPattern(Supplier::SF_PARAM_REFID) . ' tanpa ' .
                Supplier::addPattern(Supplier::SF_PARAM_TRXID) . ' karena template yang Anda gunakan mempunyai satu kondisi atau lebih di mapping response yang tidak menyimpan ' .
                Supplier::addPattern(Supplier::SF_PARAM_REFID) . ' sehingga dapat terjadi suspect yang tidak diinginkan.');
          }
        }
      }
    }

    if ($data['response_status_type'] == Supplier::STATUS_SUCCESS) {
      if (!in_array(Supplier::SF_PARAM_BALANCE, $specialOutputs) && !in_array(Supplier::SF_PARAM_UNIT_BALANCE, $specialOutputs))
        return $listener->response(400, 'Reply sukses harus mempunyai format ' . Supplier::addPattern(Supplier::SF_PARAM_BALANCE) . ' agar sistem dapat mengetahui sisa saldo Anda di biller tersebut.');
      //else if (!in_array(Supplier::SF_PARAM_PRICE, $specialOutputs) && !in_array(Supplier::SF_PARAM_UNIT_BALANCE, $specialOutputs))
      //  return $listener->response(400, 'Reply sukses harus mempunyai format ' . Supplier::addPattern(Supplier::SF_PARAM_PRICE) . ' agar sistem dapat mengetahui berapa harga barang Anda di biller tersebut.');
      else if (!in_array(Supplier::SF_PARAM_SN, $specialOutputs))
        return $listener->response(400, 'Reply sukses harus mempunyai format ' . Supplier::addPattern(Supplier::SF_PARAM_SN));
    }

    $templateResponseDetail = $this->vendorSwitcherTemplateResponseDetails->getNew();
    $templateResponseDetail->template_response_id = $data['template_response_id'];
    $templateResponseDetail->predefined_vendor_switcher_id = $data['vendor_switcher_id'] == '0' ? null : $data['vendor_switcher_id'];
    $templateResponseDetail->response_status_type = $data['response_status_type'];
    $templateResponseDetail->response_details = is_array($newResponses) ? json_encode($newResponses) : $newResponses;

    $this->vendorSwitcherTemplateResponseDetails->save($templateResponseDetail);
    $this->templateCache->resetResponse($templateResponse->template_id, $templateResponseDetail->predefined_vendor_switcher_id, $templateResponse->route_type);

    return $listener->response(200, [
      'template_response_detail_id' => $templateResponseDetail->id,
      'predefined_vendor_switcher_id' => $templateResponseDetail->predefined_vendor_switcher_id,
      'response_status_type' => $templateResponseDetail->response_status_type,
      'conditions' => $newResponses
    ]);
  }

  public function remove(PipelineListener $listener, $data) {
    if ($templateResponseDetail = $this->vendorSwitcherTemplateResponseDetails->findById($data['template_response_detail_id'])) {

      if (!isAdmin()) {
        list ($isValid, $message) = $this->supplyValidator->checkStore($templateResponseDetail->vendorSwitcher->owner_store_id, $data);
        if (!$isValid) return $listener->response(400, $message);
      }

      $templateResponseDetail->is_active = false;
      $this->vendorSwitcherTemplateResponseDetails->save($templateResponseDetail);
      $templateResponse = $templateResponseDetail->response;
      $this->templateCache->resetResponse($templateResponse->template_id, $templateResponseDetail->predefined_vendor_switcher_id, $templateResponse->route_type);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

}

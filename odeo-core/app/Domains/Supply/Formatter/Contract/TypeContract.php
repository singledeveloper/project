<?php

namespace Odeo\Domains\Supply\Formatter\Contract;

use Illuminate\Http\Request;

interface TypeContract {

  public function constructRequest($array, $title = null);

  public function constructRequestPreview($array, $title = null);

  public function deconstructResponse($string);

  public function getRequestData(Request $request);

}
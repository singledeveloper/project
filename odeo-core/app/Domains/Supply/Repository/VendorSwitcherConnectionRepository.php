<?php

namespace Odeo\Domains\Supply\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Supply\Model\VendorSwitcherConnection;

class VendorSwitcherConnectionRepository extends Repository {

  public function __construct(VendorSwitcherConnection $vendorSwitcherConnection) {
    $this->model = $vendorSwitcherConnection;
  }

  public function findActiveBillerUseTemplate($templateId) {
    return $this->model->whereHas('vendorSwitcher', function($query){
      $query->where('is_active', true);
    })->where('template_id', $templateId)->first();
  }

  public function findByVendorSwitcherId($vendorSwitcherId) {
    return $this->model->where('vendor_switcher_id', $vendorSwitcherId)->first();
  }

  public function findCacheByVendorSwitcherId($vendorSwitcherId) {
    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.biller.' . $vendorSwitcherId, 60 * 60 * 24 * 30,
      function () use ($vendorSwitcherId) {
        return $this->model->with('template')
          ->where('vendor_switcher_id', $vendorSwitcherId)->first();
    });
  }

  public function getByVendorSwitcherIds($vendorSwitcherIds) {
    return $this->model->with('template')
      ->whereIn('vendor_switcher_id', $vendorSwitcherIds)->get();
  }

  public function findByVendorSwitcherNotifySlug($slug) {
    $key = $this->model->getTable();

    return $this->runCache($key, $key . '.biller-slug.' . $slug, 60 * 60 * 24 * 30,
      function() use ($slug) {
        return $this->model->with('template')->whereHas('vendorSwitcher', function ($query) use ($slug) {
          $query->where('notify_slug', $slug);
        })->first();
      });
  }

  public function findByServerDestination($string, $ownerStoreId = '') {
    $query = $this->model->where('server_destination', $string);
    if ($ownerStoreId != '') $query = $query->whereHas('vendorSwitcher', function($subquery) use ($ownerStoreId) {
      $subquery->where('owner_store_id', $ownerStoreId);
    });

    return $query->first();
  }

  public function getByTemplateId($templateId) {
    return $this->model->where('template_id', $templateId)->get();
  }

}

<?php

namespace Odeo\Domains\Supply;

use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class TemplateSelector implements SelectorListener {

  private $vendorSwitcherTemplates, $vendorSwitcherTemplateResponses, $vendorSwitcherTemplateResponseDetails, $supplyValidator, $vendorSwitchers;

  public function __construct() {
    $this->vendorSwitcherTemplates = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->vendorSwitcherTemplateResponses = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseRepository::class);
    $this->vendorSwitcherTemplateResponseDetails = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output = [];

    $output['id'] = $item->id;
    $output['name'] = $item->name;
    $output['format_type'] = $item->format_type;
    $output['predefined_store_id'] = $item->predefined_store_id;
    $output['headers'] = $item->headers ? json_decode($item->headers, true) : [];
    $output['body_title'] = $item->body_title;
    $decode = json_decode($item->body_details, true);
    $output['body_details'] = $decode ? $decode : $item->body_details;

    return $output;
  }

  public function get(PipelineListener $listener, $data) {

    $templates = [];

    if (isset($data['only_store_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($data['only_store_id'], $data);
      if (!$isValid) return $listener->response(400, $message);
      $billerTemplates = $this->vendorSwitcherTemplates->getByStoreId($data['only_store_id']);
    }
    else {
      $storeId = isset($data['store_id']) ? $data['store_id'] : '';
      if ($storeId != '') {
        list ($isValid, $message) = $this->supplyValidator->checkStore($storeId, $data);
        if (!$isValid) return $listener->response(400, $message);
      }
      $billerTemplates = $this->vendorSwitcherTemplates->gets($storeId);
    }
    foreach ($billerTemplates as $template) {
      $templates[] = [
        'id' => $template->id,
        'name' => $template->name,
        'format_type' => $template->format_type,
        'predefined_store_id' => $template->predefined_store_id,
        'user_id_alias' => $template->user_id_alias,
        'user_name_alias' => $template->user_name_alias,
        'pin_alias' => $template->pin_alias,
        'password_alias' => $template->password_alias,
        'token_alias' => $template->token_alias
      ];
    }

    if (sizeof($templates) > 0)
      return $listener->response(200, [
        "templates" => $this->_extends($templates, $this->vendorSwitcherTemplates)
      ]);
    return $listener->response(204, ["templates" => []]);
  }

  public function getDetail(PipelineListener $listener, $data) {

    if ($template = $this->vendorSwitcherTemplates->findById($data['template_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($template->predefined_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      return $listener->response(200, $this->_transforms($template, $this->vendorSwitcherTemplates));
    }

    return $listener->response(204);
  }

  public function getResponses(PipelineListener $listener, $data) {

    $vendorSwitcherId = isset($data['vendor_switcher_id']) ? $data['vendor_switcher_id'] : '';
    if ($vendorSwitcherId != '') {
      $biller = $this->vendorSwitchers->findById($vendorSwitcherId);
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);
    }

    $responses = [];

    foreach ($this->vendorSwitcherTemplateResponses->getByTemplateId($data['template_id'], $vendorSwitcherId) as $item) {
      if (!isset($responses[$item->route_type])) $responses[$item->route_type] = [];
      $response = [
        'template_response_id' => $item->id,
        'format_type' => $item->format_type,
        'predefined_vendor_switcher_id' => $item->predefined_vendor_switcher_id,
        'details' => []
      ];

      $responseStatuses = Supplier::getStatuses();
      foreach ($this->vendorSwitcherTemplateResponseDetails->getByResponseId($item->id, $vendorSwitcherId) as $output) {
        $decode = json_decode($output->response_details, true);
        $response['details'][] = [
          'template_response_detail_id' => $output->id,
          'predefined_vendor_switcher_id' => $output->predefined_vendor_switcher_id,
          'response_status_type' => $output->response_status_type,
          'response_status_name' => $responseStatuses[$output->response_status_type],
          'conditions' => $decode ? $decode : $output->response_details
        ];
      }

      $responses[$item->route_type][] = $response;
    }

    if (sizeof($responses) > 0) return $listener->response(200, ['template_responses' => $responses]);
    return $listener->response(204);

  }

  public function getBasicResponses(PipelineListener $listener, $data) {

    $vendorSwitcherId = isset($data['vendor_switcher_id']) ? $data['vendor_switcher_id'] : '';
    if ($vendorSwitcherId != '') {
      $biller = $this->vendorSwitchers->findById($vendorSwitcherId);
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);
    }

    $responses = [];

    $responseStatuses = Supplier::getStatuses();
    foreach ($this->vendorSwitcherTemplateResponseDetails->getBasicResponseList($data['template_id'], $vendorSwitcherId) as $output) {
      $decode = json_decode($output->response_details, true);
      $responses[] = [
        'template_response_detail_id' => $output->id,
        'predefined_vendor_switcher_id' => $output->predefined_vendor_switcher_id,
        'response_status_type' => $output->response_status_type,
        'response_status_name' => $responseStatuses[$output->response_status_type],
        'conditions' => $decode ? $decode : $output->response_details
      ];
    }

    if (sizeof($responses) > 0) return $listener->response(200, ['template_responses' => $responses]);
    return $listener->response(204);

  }

}

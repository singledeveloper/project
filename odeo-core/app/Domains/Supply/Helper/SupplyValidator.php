<?php

namespace Odeo\Domains\Supply\Helper;

use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\Supplier;

class SupplyValidator {

  public function checkStore($storeId, $data) {
    if (!isAdmin($data)) {
      $userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
      if ($storeId == null || ($storeId != null && !$userStores->checkCredibility($storeId, $data['auth']['user_id'])))
        return [false, 'Anda tidak mempunyai akses mengubah data ini.'];
      $stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
      $store = $stores->findById($storeId);
      if (!$store->can_supply)
        return [false, 'Anda tidak mempunyai akses untuk men-supply barang.'];
    }
    return [true, ''];
  }

  public function checkTemplateConnection($data, $checkParam = []) {
    $vendorSwitcherTemplates = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateRepository::class);

    if ($template = $vendorSwitcherTemplates->findById($data['biller_template_id'])) {

      $storeId = isset($data['store_id']) && $data['store_id'] != StoreStatus::STORE_DEFAULT ? $data['store_id'] : null;
      if (!isAdmin($data) && $storeId != null && $template->format_type == Supplier::TYPE_TEXT) {
        $jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
        if (!$jabberStore = $jabberStores->findByStoreId($storeId)) return [false, 'Anda belum melakukan setup Jabber Sender di toko Anda'];
        else if ($jabberStore->socket_path == null) return [false, 'Jabber Sender anda belum aktif. Mohon menunggu 1x24 jam.'];

        if (isset($data['server_destination'])) {
          $vendorSwitcherConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
          $connection = $vendorSwitcherConnections->findByServerDestination($data['server_destination'], $storeId);
          if ($connection && ((isset($data['vendor_switcher_id']) && $data['vendor_switcher_id'] != $connection->vendor_switcher_id) ||
            !isset($data['vendor_switcher_id']))) return [false, 'Akun Jabber sebagai Destinasi sudah pernah digunakan di biller lain'];
        }
      }

      if (!$body = json_decode($template->body_details, true)) $body = ['body' => $template->body_details];
      if (!$header = json_decode($template->headers, true)) $header = ['header' => $template->headers];

      $request = array_merge($header, $body);
      $specialOutputs = [];

      foreach ($request as $item) {
        preg_match_all(Supplier::SF_PATTERN, $item, $matches);
        foreach ($matches[1] as $output) $specialOutputs[] = $output;
      }

      if ((count($checkParam) == 0 || in_array(Supplier::SF_PARAM_USERID, $checkParam))
        && ($data['server_user_id'] == null && in_array(Supplier::SF_PARAM_USERID, $specialOutputs)))
        return [false, 'Template yang Anda pilih mengharuskan Anda meng-input User ID.'];
      if ((count($checkParam) == 0 || in_array(Supplier::SF_PARAM_PASSWORD, $checkParam))
        && ($data['server_password'] == null && in_array(Supplier::SF_PARAM_PASSWORD, $specialOutputs)))
        return [false, 'Template yang Anda pilih mengharuskan Anda meng-input Password.'];
      if ((count($checkParam) == 0 || in_array(Supplier::SF_PARAM_PIN, $checkParam))
        && ($data['server_pin'] == null && in_array(Supplier::SF_PARAM_PIN, $specialOutputs)))
        return [false, 'Template yang Anda pilih mengharuskan Anda meng-input Pin.'];
      if ((count($checkParam) == 0 || in_array(Supplier::SF_PARAM_TOKEN, $checkParam))
        && ($data['server_token'] == null && in_array(Supplier::SF_PARAM_TOKEN, $specialOutputs)))
        return [false, 'Template yang Anda pilih mengharuskan Anda meng-input Token.'];

      return [true, ''];
    }
    return [false, 'Template tidak ditemukan'];
  }

  public function checkHeader($headers) {
    $newHeaders = [];

    foreach ($headers as $item) {
      if (!isset($item['param']) || !isset($item['value']))
        return [false, 'Data Header tidak lengkap.'];
      if (!in_array(trim($item['param']), Supplier::ALLOW_HEADERS))
        return [false, 'Parameter Header tidak valid.'];
      $newHeaders[trim($item['param'])] = trim($item['value']);
    }

    return [true, $newHeaders];
  }

  public function checkBody($bodies, $formatType) {
    $newBodies = [];

    if ($formatType == Supplier::TYPE_TEXT) {
      if (!is_string($bodies)) return [false, 'Detail Body harus tipe String jika menggunakan format ' . Supplier::TYPE_TEXT . '.'];
      try {
        preg_match_all(Supplier::SF_PATTERN, trim($bodies), $matches);
        foreach ($matches[1] as $output) {
          if (!in_array(strtolower($output), Supplier::SF_REQUESTS))
            return [false, 'Format ' . Supplier::addPattern($output) . ' tidak valid. Mohon gunakan format yang terdaftar di ODEO.'];
          $bodies = str_replace(Supplier::addPattern($output), Supplier::addPattern(strtolower($output)), $bodies);
        }
      }
      catch (\Exception $e) {}

      $newBodies = $bodies;
    }
    else {
      foreach ($bodies as $item) {
        if (!isset($item['param']) || !isset($item['value']))
          return [false, 'Data Body tidak lengkap.'];

        try {
          preg_match_all(Supplier::SF_PATTERN, trim($item['value']), $matches);
          foreach ($matches[1] as $output) {
            if (!in_array(strtolower($output), Supplier::SF_REQUESTS))
              return [false, 'Format ' . Supplier::addPattern($output) . ' tidak valid. Mohon gunakan format yang terdaftar di ODEO.'];
            $item['value'] = str_replace(Supplier::addPattern($output), Supplier::addPattern(strtolower($output)), $item['value']);
          }
        }
        catch (\Exception $e) {}

        if (substr(trim($item['value']), 0, strlen(Supplier::SIGN_HEADER)) == Supplier::SIGN_HEADER) {
          $noHeader = substr(trim($item['value']), strlen(Supplier::SIGN_HEADER), strlen(trim($item['value'])) - 1);
          $hashFunction = substr($noHeader, 0, strpos($noHeader, '#'));

          if ($hashFunction == Supplier::SIGN_TYPE_CUSTOM) {
            $hashValue = substr($noHeader, strpos($noHeader, '#') + 1, strlen($noHeader) - 1);
            $path = app()->make(\Odeo\Domains\Supply\Formatter\SignatureManager::class)->setPath($hashValue);
            if ($path == '') return [false, 'Signature "' . $hashValue . '" tidak tersedia. Kontak @odeo_it di Telegram untuk request Custom Signature.'];
          }
          else {
            foreach (explode('&', $hashFunction) as $output)
              if (!in_array($output, Supplier::getSignTypes()))
                return [false, 'Fungsi ' . $output . ' belum mendukung pembuatan Signature. Kontak @odeo_it di Telegram untuk keterangan lebih lanjut.'];
          }
        }

        $newBodies[trim($item['param'])] = trim($item['value']);
      }
    }

    return [true, $newBodies];
  }

}

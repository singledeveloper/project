<?php

namespace Odeo\Domains\Supply\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Jobs\Job;

class SaveLog extends Job {

  use SerializesModels;

  private $vendorSwitcherId, $type, $description;

  public function __construct($vendorSwitcherId, $type, $description) {
    parent::__construct();
    $this->vendorSwitcherId = $vendorSwitcherId;
    $this->type = $type;
    $this->description = $description;
  }

  public function handle() {

    if ($this->description == null) return;

    $logs = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionLogRepository::class);

    $log = $logs->getNew();
    $log->vendor_switcher_id = $this->vendorSwitcherId;
    $log->type = $this->type;
    $log->description = $this->description;
    $logs->save($log);

  }

}

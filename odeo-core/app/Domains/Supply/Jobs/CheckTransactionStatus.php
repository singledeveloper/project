<?php

namespace Odeo\Domains\Supply\Jobs;

use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\SupplierTemplate;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Jobs\Job;

class CheckTransactionStatus extends Job {

  use SerializesModels;

  private $templateId, $data;

  public function __construct($templateId, $data) {
    parent::__construct();
    $this->templateId = $templateId;
    $this->data = $data;
  }

  public function parsePath() {
    switch ($this->templateId) {
      case SupplierTemplate::ERATEL:
        return \Odeo\Domains\Biller\Eratel\TransactionChecker::class;
      case SupplierTemplate::IDBILLER:
        return \Odeo\Domains\Biller\IDBiller\TransactionChecker::class;
      default:
        return '';
    }
  }

  public function handle() {

    $path = $this->parsePath();
    if ($path != '') {
      $pipeline = new Pipeline();
      $pipeline->add(new Task($path, 'check'));
      $pipeline->execute($this->data);
    }

  }

}

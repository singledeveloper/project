<?php

namespace Odeo\Domains\Supply;

use Illuminate\Support\Facades\Crypt;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;

class BillerConnector {

  private $vendorSwitcherConnections, $vendorSwitchers, $supplyValidator, $jabberUsers, $pulsaOdeoInventories, $jabberStores,
    $responseDetails, $vendorSwitcherTemplateResponses, $vendorSwitcherTemplateResponseDetails, $templateCache;

  public function __construct() {
    $this->vendorSwitcherConnections = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherConnectionRepository::class);
    $this->vendorSwitcherTemplateResponses = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->supplyValidator = app()->make(\Odeo\Domains\Supply\Helper\SupplyValidator::class);
    $this->responseDetails = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
    $this->vendorSwitcherTemplateResponseDetails = app()->make(\Odeo\Domains\Supply\Repository\VendorSwitcherTemplateResponseDetailRepository::class);
    $this->jabberUsers = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberUserRepository::class);
    $this->templateCache = app()->make(\Odeo\Domains\Supply\Helper\CacheManager::class);
    $this->pulsaOdeoInventories = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoInventoryRepository::class);
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->jabberStores = app()->make(\Odeo\Domains\Vendor\Jabber\Repository\JabberStoreRepository::class);
  }

  public function setup(PipelineListener $listener, $data) {

    list ($isValid, $message) = $this->supplyValidator->checkStore($data['store_id'], $data);
    if (!$isValid) return $listener->response(400, $message);

    $data['server_user_id'] = isset($data['server_user_id']) && $data['server_user_id'] != '' ? $data['server_user_id'] : null;
    $data['server_user_name'] = isset($data['server_user_name']) && $data['server_user_name'] != '' ? $data['server_user_name'] : null;
    $data['server_password'] = isset($data['server_password']) && $data['server_password'] != '' ? $data['server_password'] : null;
    $data['server_pin'] = isset($data['server_pin']) && $data['server_pin'] != '' ? $data['server_pin'] : null;
    $data['server_token'] = isset($data['server_token']) && $data['server_token'] != '' ? $data['server_token'] : null;

    list ($isValid, $message) = $this->supplyValidator->checkTemplateConnection($data);
    if (!$isValid) return $listener->response(400, $message);

    $slug = '';
    $storeId = $data['store_id'] != StoreStatus::STORE_DEFAULT ? $data['store_id'] : null;
    if (isAdmin() && (isset($data['vendor_switcher_id']) && $data['vendor_switcher_id'] != '')) {
      if (!$biller = $this->vendorSwitchers->findById($data['vendor_switcher_id']))
        return $listener->response(400, 'Biller tidak ditemukan');
      $slug = str_replace(' ', '-', strtolower($biller->name));
    }
    else {
      $biller = $this->vendorSwitchers->getNew();
      $biller->vendor_id = 1;
      $biller->name = $data['name'];
      $biller->is_active = true;
      $biller->minimum_topup = BillerReplenishment::DEFAULT_MINIMAL_DEPOSIT;
      $biller->status = SwitcherConfig::BILLER_STATUS_OK;
      $biller->owner_store_id = $storeId;
      $biller->is_auto_replenish = false;
    }

    $biller->queue_limit = isset($data['queue_limit']) ? $data['queue_limit'] : 20;
    $biller->info_to_client = isset($data['info']) && $data['info'] != '' ? $data['info'] : null;

    if ((isset($data['recon_start']) && $data['recon_start'] != '') &&
      isset($data['recon_end']) && $data['recon_end'] != '') {
      list($reconStartHour, ) = explode(':', $data['recon_start']);
      list($reconEndHour, ) = explode(':', $data['recon_end']);

      $biller->recon_started_at = $data['recon_start'];
      $biller->recon_ended_at = $data['recon_end'];
      $biller->recon_interval = $reconStartHour > $reconEndHour;
    }

    $this->vendorSwitchers->save($biller);

    $biller->notify_slug = $slug != '' ? $slug : (trim(preg_replace('/-+/', '-',
        preg_replace("/[^a-zA-Z0-9]/", "-", strtolower($data['name']))), '-') . '-' . $biller->id);

    $this->vendorSwitchers->save($biller);

    $connection = $this->vendorSwitcherConnections->getNew();
    $connection->vendor_switcher_id = $biller->id;
    $connection->template_id = $data['biller_template_id'];
    $connection->server_destination = $data['server_destination'];
    if ($data['server_user_id'] != null) $connection->server_user_id = $data['server_user_id'];
    if ($data['server_user_name'] != null) $connection->server_user_name = $data['server_user_name'];
    if ($data['server_password'] != null) $connection->server_password = Crypt::encrypt($data['server_password']);
    if ($data['server_pin'] != null) $connection->server_pin = Crypt::encrypt($data['server_pin']);
    if ($data['server_token'] != null) $connection->server_token = Crypt::encrypt($data['server_token']);

    $this->vendorSwitcherConnections->save($connection);

    if ($connection->template->format_type == Supplier::TYPE_TEXT && !$jabberUser = $this->jabberUsers->findByEmail($data['server_destination'], $storeId)) {
      $jabberUser = $this->jabberUsers->getNew();
      $jabberUser->email = $data['server_destination'];
      $jabberUser->account_type = InlineJabber::ACCOUNT_TYPE_BILLER;
      $jabberUser->owner_store_id = $storeId;
      $this->jabberUsers->save($jabberUser);

      dispatch(new ReplySocket(InlineJabber::JAXL_SUBSCRIBE, [
        'to' => $jabberUser->email
      ], $storeId != null ? $this->jabberStores->findByStoreId($storeId)->socket_path : ''));
    }

    if (!isAdmin($data)) {
      $templateResponse = $this->vendorSwitcherTemplateResponses->getNew();
      $templateResponse->template_id = $data['biller_template_id'];
      $templateResponse->predefined_vendor_switcher_id = $biller->id;
      $templateResponse->format_type = Supplier::TYPE_TEXT;
      $templateResponse->route_type = Supplier::ROUTE_BOTH;
      $this->vendorSwitcherTemplateResponses->save($templateResponse);
    }

    return $listener->response(200, [
      'server_destination' => $connection->server_destination,
      'server_user_id' => $connection->server_user_id,
      'server_user_name' => $data['server_user_name'],
      'server_pin' => $data['server_pin'],
      'server_password' => $data['server_password'],
      'server_token' => $data['server_token'],
      'biller' => [
        'id' => $biller->id,
        'name' => $biller->name
      ],
      'template' => [
        'id' => $data['biller_template_id']
      ]
    ]);
  }

  public function edit(PipelineListener $listener, $data) {
    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      //if (isAdmin() && $biller->is_active) return $listener->response(400, 'Anda harus men-disable biller Anda terlebih dahulu.');

      $data['server_user_id'] = isset($data['server_user_id']) && $data['server_user_id'] != '' ? $data['server_user_id'] : null;
      $data['server_user_name'] = isset($data['server_user_name']) && $data['server_user_name'] != '' ? $data['server_user_name'] : null;
      if ($biller->owner_store_id != null) $data['store_id'] = $biller->owner_store_id;
      list ($isValid, $message) = $this->supplyValidator->checkTemplateConnection($data, [Supplier::SF_PARAM_USERID]);
      if (!$isValid) return $listener->response(400, $message);

      $biller->name = $data['name'];
      if ((isset($data['recon_start']) && $data['recon_start'] != '') &&
        isset($data['recon_end']) && $data['recon_end'] != '') {
        list($reconStartHour, ) = explode(':', $data['recon_start']);
        list($reconEndHour, ) = explode(':', $data['recon_end']);

        $biller->recon_started_at = $data['recon_start'];
        $biller->recon_ended_at = $data['recon_end'];
        $biller->recon_interval = $reconStartHour > $reconEndHour;
      }
      $biller->queue_limit = isset($data['queue_limit']) ? $data['queue_limit'] : 20;
      $biller->info_to_client = isset($data['info']) && $data['info'] != '' ? $data['info'] : null;
      $this->vendorSwitchers->save($biller);

      $connection = $this->vendorSwitcherConnections->findByVendorSwitcherId($biller->id);
      $connection->template_id = $data['biller_template_id'];
      $connection->server_destination = $data['server_destination'];
      $connection->server_user_id = $data['server_user_id'];
      $connection->server_user_name = $data['server_user_name'];

      $data['server_password'] = isset($data['server_password']) && $data['server_password'] != '' ? $data['server_password'] : null;
      $data['server_pin'] = isset($data['server_pin']) && $data['server_pin'] != '' ? $data['server_pin'] : null;
      $data['server_token'] = isset($data['server_token']) && $data['server_token'] != '' ? $data['server_token'] : null;

      if ($data['server_password'] != null) $connection->server_password = Crypt::encrypt($data['server_password']);
      if ($data['server_pin'] != null) $connection->server_pin = Crypt::encrypt($data['server_pin']);
      if ($data['server_token'] != null) $connection->server_token = Crypt::encrypt($data['server_token']);

      $this->vendorSwitcherConnections->save($connection);

      if ($connection->template->format_type == Supplier::TYPE_TEXT && !$jabberUser = $this->jabberUsers->findByEmail($data['server_destination'], $biller->owner_store_id)) {
        $jabberUser = $this->jabberUsers->getNew();
        $jabberUser->email = $data['server_destination'];
        $jabberUser->account_type = InlineJabber::ACCOUNT_TYPE_BILLER;
        $jabberUser->owner_store_id = $biller->owner_store_id;
        $this->jabberUsers->save($jabberUser);

        dispatch(new ReplySocket(InlineJabber::JAXL_SUBSCRIBE, [
          'to' => $jabberUser->email
        ], $biller->owner_store_id != null ? $this->jabberStores->findByStoreId($biller->owner_store_id)->socket_path : ''));
      }

      $this->templateCache->resetConnection($biller->id);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

  public function editPassword(PipelineListener $listener, $data) {
    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $connection = $this->vendorSwitcherConnections->findByVendorSwitcherId($biller->id);
      $data['biller_template_id'] = $connection->template_id;

      $data['server_password'] = isset($data['server_password']) && $data['server_password'] != '' ? Crypt::encrypt($data['server_password']) : null;
      list ($isValid, $message) = $this->supplyValidator->checkTemplateConnection($data, [Supplier::SF_PARAM_PASSWORD]);
      if (!$isValid) return $listener->response(400, $message);

      $connection->server_password = $data['server_password'];

      $this->vendorSwitcherConnections->save($connection);

      $this->templateCache->resetConnection($biller->id);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

  public function editPin(PipelineListener $listener, $data) {
    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $connection = $this->vendorSwitcherConnections->findByVendorSwitcherId($biller->id);
      $data['biller_template_id'] = $connection->template_id;

      $data['server_pin'] = isset($data['server_pin']) && $data['server_pin'] != '' ? Crypt::encrypt($data['server_pin']) : null;
      list ($isValid, $message) = $this->supplyValidator->checkTemplateConnection($data, [Supplier::SF_PARAM_PIN]);
      if (!$isValid) return $listener->response(400, $message);

      $connection->server_pin = $data['server_pin'];

      $this->vendorSwitcherConnections->save($connection);

      $this->templateCache->resetConnection($biller->id);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

  public function editToken(PipelineListener $listener, $data) {
    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $connection = $this->vendorSwitcherConnections->findByVendorSwitcherId($biller->id);
      $data['biller_template_id'] = $connection->template_id;

      $data['server_token'] = isset($data['server_token']) && $data['server_token'] != '' ? Crypt::encrypt($data['server_token']) : null;
      list ($isValid, $message) = $this->supplyValidator->checkTemplateConnection($data, [Supplier::SF_PARAM_TOKEN]);
      if (!$isValid) return $listener->response(400, $message);

      $connection->server_token = $data['server_token'];

      $this->vendorSwitcherConnections->save($connection);

      $this->templateCache->resetConnection($biller->id);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia.');
  }

  public function toggle(PipelineListener $listener, $data) {

    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      list ($isValid, $message) = $this->supplyValidator->checkStore($biller->owner_store_id, $data);
      if (!$isValid) return $listener->response(400, $message);

      $biller->is_active = !$biller->is_active;

      if ($biller->is_active) {
        $connection = $this->vendorSwitcherConnections->findByVendorSwitcherId($biller->id);
        if (!$connection) {
          if (SwitcherConfig::getVendorSwitcherManagerById($biller->id) != '') {
            $this->vendorSwitchers->save($biller);
            return $listener->response(200);
          }
          //return $listener->response(400, 'Anda harus membuat koneksi terlebih dahulu.');
        }

        if ($biller->status == SwitcherConfig::BILLER_STATUS_OK && $connection->template->predefined_store_id) {
          if (!$this->responseDetails->getResponseCount($connection->template_id, Supplier::STATUS_SUCCESS))
            return $listener->response(400, 'Response template harus mempunyai minimal 1 kondisi sukses.');
          if (!$this->responseDetails->getResponseCount($connection->template_id, Supplier::STATUS_IN_QUEUE))
            return $listener->response(400, 'Response template harus mempunyai minimal 1 kondisi pending.');
          if (!$this->responseDetails->getResponseCount($connection->template_id, Supplier::STATUS_FAIL))
            return $listener->response(400, 'Response template harus mempunyai minimal 1 kondisi gagal.');
        }

        if (!isAdmin($data)) {
          foreach ($this->pulsaOdeoInventories->findByVendorSwitcherId($data['vendor_switcher_id']) as $item) {
            $item->is_active = true;
            $this->pulsaOdeoInventories->save($item);

            if ($pulsa = $item->pulsa) {
              $pulsa->is_active = true;
              $this->pulsaOdeo->save($pulsa);
            }
          }
        }
      }
      else $this->pulsaOdeoInventories->disableInventory($data['vendor_switcher_id']);

      $this->vendorSwitchers->save($biller);

      $this->templateCache->resetConnection($biller->id);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data tidak tersedia');
  }

  public function testConnection(PipelineListener $listener, $data) {

    if (!in_array($data['route_type'], Supplier::getRoutes()))
      return $listener->response(400, 'Route tidak valid.');

    $billerStatusUpdater = app()->make(\Odeo\Domains\Supply\Helper\BillerStatusUpdater::class);

    foreach ($this->vendorSwitcherTemplateResponseDetails->getResponseList($data['template_id'], $data['vendor_switcher_id'], $data['route_type']) as $item) {
      if ($billerStatusUpdater->translateResponse($item, $data['response_example'])) break;
    }

    $result = $billerStatusUpdater->translateResult;
    if (sizeof($result) <= 0)
      return $listener->response(200, [
        'test_result' => ['is_matched' => false]
      ]);

    $finalResult['is_matched'] = true;
    if ($result['status'] == SwitcherConfig::BILLER_IN_QUEUE) $finalResult['status'] = 'PENDING';
    else if ($result['status'] == SwitcherConfig::BILLER_FAIL) $finalResult['status'] =  'GAGAL';
    else if ($result['status'] == SwitcherConfig::BILLER_SUCCESS) $finalResult['status'] = 'SUKSES';
    else if ($result['status'] == SwitcherConfig::BILLER_DUPLICATE_SUCCESS) $finalResult['status'] = 'SUDAH PERNAH TERJADI';
    else $finalResult['status'] = 'UNKNOWN';

    unset($result['status']);
    if (isset($result['passed'])) unset($result['passed']);

    $finalResult['detail'] = [];

    foreach ($result as $key => $item) $finalResult['detail'][Supplier::addPattern($key)] = $item;

    return $listener->response(200, ['test_result' => $finalResult]);

  }

}

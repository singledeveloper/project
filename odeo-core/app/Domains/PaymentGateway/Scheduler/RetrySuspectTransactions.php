<?php

namespace Odeo\Domains\PaymentGateway\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewayVendor;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Domains\PaymentGateway\PaymentGatewayPaymentUpdater;

class RetrySuspectTransactions {

  private $paymentGatewayPaymentRepo;

  public function __construct() {
    $this->paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
  }

  public function run() {
    $suspectTransactions = $this->paymentGatewayPaymentRepo->getSuspectTransactions();
    $paymentGatewayPaymentRepo = app()->make(PaymentGatewayPaymentUpdater::class);

    foreach ($suspectTransactions as $transaction) {
      clog('pg_transaction', $transaction->vendor_id);
      $vendorPaymentRepo = PaymentGatewayVendor::getVendorPaymentRepo($transaction->vendor_type);
      
      $vendorPayment = $vendorPaymentRepo->findById($transaction->vendor_id);
      
      $vendorPayment->status = PaymentGateway::ACCEPTED;
      $vendorPaymentRepo->save($vendorPayment);
    }
  }

}

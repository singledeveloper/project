<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 27/11/18
 * Time: 19.46
 */

namespace Odeo\Domains\PaymentGateway\Model;

use Odeo\Domains\Core\Entity;

class PaymentGatewayUser extends Entity {

  public function __construct() {
    parent::__construct();
  }

  public function channels() {
    return $this->hasMany(PaymentGatewayUserPaymentChannel::class, 'pg_user_id');
  }

}

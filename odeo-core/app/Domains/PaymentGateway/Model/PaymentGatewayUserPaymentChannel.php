<?php

namespace Odeo\Domains\PaymentGateway\Model;

use Odeo\Domains\Core\Entity;

class PaymentGatewayUserPaymentChannel extends Entity {

  public function __construct() {
    parent::__construct();
  }

  public function channel() {
    return $this->belongsTo(PaymentGatewayChannel::class, 'payment_gateway_channel_id');
  }
}

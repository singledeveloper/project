<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/09/19
 * Time: 18.07
 */

namespace Odeo\Domains\PaymentGateway;


use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewayVendor;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;

class PaymentGatewayPaymentUpdater {

  function __construct() {
    $this->paymentRepo = app()->make(PaymentRepository::class);
    $this->pgPaymentRepo = app()->make(PaymentGatewayPaymentRepository::class);
  }

  public function markPaymentAsAccepted(PipelineListener $listener, $data) {
    if ($data['password'] != env('PG_MARK_AS_ACCEPTED_PASSWORD')) {
      return $listener->response(400, trans('errors.invalid_password'));
    }
    if (!$pgPayment = $this->pgPaymentRepo->findById($data['payment_id'])) {
      return $listener->response(400, trans('errors.payment_gateway.not_found'));
    }
    if (!$payment = $this->paymentRepo->findByPgPaymentId($pgPayment->id)) {
      return $listener->response(400, trans('errors.payment_gateway.payment_not_found'));
    }
    if (!$vendorPaymentRepo = PaymentGatewayVendor::getVendorPaymentRepo($payment->vendor_type)) {
      return $listener->response(400, trans('errors.payment_gateway.invalid_vendor'));
    }
    if (!$vendorPayment = $vendorPaymentRepo->findById($payment->vendor_id)) {
      return $listener->response(400, trans('errors.payment_gateway.vendor_payment_not_found'));
    }

    if (!$this->validateStatus($pgPayment->status, $vendorPayment->status)) {
      return $listener->response(400, trans('errors.payment_gateway.error_update'));
    }

    $vendorPayment->status = PaymentGateway::ACCEPTED;
    $vendorPaymentRepo->save($vendorPayment);
    $pgPayment->status = PaymentGateway::ACCEPTED;
    $this->pgPaymentRepo->save($pgPayment);

    return $listener->response(200);
  }

  private function validateStatus($pgStatus, $vendorStatus) {
    return in_array($pgStatus, [
      PaymentGateway::SUCCESS,
      PaymentGateway::SUSPECT,
      PaymentGateway::SUSPECT_TIMEOUT_PAYMENT_ACCEPTANCE,
      PaymentGateway::WAITING_FOR_ACCEPTANCE,
      PaymentGateway::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE
    ]) || in_array($vendorStatus, [
      PaymentGateway::SUCCESS,
      PaymentGateway::FAILED_PAYMENT_MALFORMED_MERCHANT_RESPONSE
    ]);
  }

}
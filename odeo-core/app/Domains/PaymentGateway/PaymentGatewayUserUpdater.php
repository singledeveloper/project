<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 03/07/19
 * Time: 17.43
 */

namespace Odeo\Domains\PaymentGateway;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Marketing\Repository\BusinessResellerRepository;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewaySettings;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserRepository;
use Odeo\Domains\VirtualAccount\Repository\VirtualAccountMapRepository;

class PaymentGatewayUserUpdater {

  private $pgUserRepo, $pgUserPaymentChannelRepo, $vaMapRepo, $businessResellerRepo;

  public function __construct() {
    $this->pgUserRepo = app()->make(PaymentGatewayUserRepository::class);
    $this->pgChannelRepo = app()->make(PaymentGatewayChannelRepository::class);
    $this->pgUserPaymentChannelRepo = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->vaMapRepo = app()->make(VirtualAccountMapRepository::class);
    $this->businessResellerRepo = app()->make(BusinessResellerRepository::class);
  }

  public function createPaymentGatewayUser(PipelineListener $listener, $data) {
    $userId = $data['auth']['user_id'];

    if ($pgUser = $this->pgUserRepo->findByUserId($userId)) {
      return $listener->response(400, trans('api_users.client_exists'));
    }

    $pgUser = $this->pgUserRepo->getNew();
    $pgUser->user_id = $userId;
    $pgUser->notify_url = '';
    $pgUser->email_report = '';
    $this->pgUserRepo->save($pgUser);

    return $listener->response(201, [
      'va_prefix' => '',
      'notify_url' => $pgUser->notify_url,
      'email_report' => $pgUser->email_report,
      'fee' => 0,
      'settlement_type' => '-',
      'settlement_fee_pct' => 0
    ]);
  }

  public function updateCallback(PipelineListener $listener, $data) {
    if (!$pgUser = $this->pgUserRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $pgUser->notify_url = $data['url'];
    $this->pgUserRepo->save($pgUser);

    return $listener->response(200, [
      'notify_url' => $pgUser->notify_url
    ]);
  }

  public function togglePaymentChannel(PipelineListener $listener, $data) {
    if (!$pgUser = $this->pgUserRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    if (!$pgChannel = $this->pgChannelRepo->findById($data['channel_id'])) {
      return $listener->response(400, trans('api_users.invalid_pg_channel'));
    }

    if ($userPaymentChannel = $this->pgUserPaymentChannelRepo->findByPgUserIdAndPgChannelId($pgUser->id, $data['channel_id'])) {
      $userPaymentChannel->active = !$userPaymentChannel->active;
      $this->pgUserPaymentChannelRepo->save($userPaymentChannel);
    } else {
      $prefix = ($this->pgUserPaymentChannelRepo->findLatestPrefix($pgChannel->vaVendor->prefix)->prefix ?? PaymentGatewaySettings::getChannelPrefix($pgChannel->vaVendor->prefix)) + 1;

      $userPaymentChannel = $this->pgUserPaymentChannelRepo->getNew();
      $userPaymentChannel->pg_user_id = $pgUser->id;
      $userPaymentChannel->active = true;
      $userPaymentChannel->fee = $this->businessResellerRepo->isStarwinAgent($data['auth']['user_id'])
        && $pgChannel->starwin_default_fee ? $pgChannel->starwin_default_fee : $pgChannel->default_fee;
      $userPaymentChannel->settlement_fee = $pgChannel->default_settlement_fee;
      $userPaymentChannel->settlement_type = $pgChannel->default_settlement_type;
      $userPaymentChannel->prefix = $prefix;
      $userPaymentChannel->payment_gateway_channel_id = $pgChannel->id;
      $this->pgUserPaymentChannelRepo->save($userPaymentChannel);

      $vaMap = $this->vaMapRepo->getNew();
      $vaMap->user_id = $data['auth']['user_id'];
      $vaMap->prefix = $userPaymentChannel->prefix;
      $vaMap->channel_type = PaymentGatewaySettings::CHANNEL_TYPE_PG;
      $vaMap->va_vendor_id = $pgChannel->vaVendor->id;
      $this->vaMapRepo->save($vaMap);
    }

    return $listener->response(200, [
      'channel_id' => $pgChannel->id,
      'channel_fee' => $userPaymentChannel->fee,
      'prefix' => $userPaymentChannel->prefix,
      'active' => $userPaymentChannel->active
    ]);
  }

  public function updateEmailReport(PipelineListener $listener, $data) {
    if (!$pgUser = $this->pgUserRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $emails = str_replace(' ', '', $data['email']);
    foreach (explode(';', $emails) as $email) {
      if (!$this->isValidEmailFormat($email)) {
        return $listener->response(400, trans('api_users.invalid_email_format'));
      }
    }

    $pgUser->email_report = strtolower($emails);
    $this->pgUserRepo->save($pgUser);

    return $listener->response(200, [
      'email_report' => $pgUser->email_report
    ]);
  }

  public function updateSettlement(PipelineListener $listener, $data) {
    if (!$pgUser = $this->pgUserRepo->findByUserId($data['auth']['user_id'])) {
      return $listener->response(400, trans('api_users.unauthorized'));
    }

    $userPaymentChannel = $this->pgUserPaymentChannelRepo->findByPgUserIdAndPgChannelId($pgUser->id, $data['channel_id']);

    if (!$userPaymentChannel || !$userPaymentChannel->active) {
      return $listener->response(400, trans('api_users.invalid_pg_channel'));
    }

    $defaultSettlementType = $userPaymentChannel->channel->default_settlement_type;

    if (strlen($data['settlement_type']) != strlen($defaultSettlementType)) {
      return $listener->response(400, trans('api_users.invalid_settlement_type'));
    }

    foreach(str_split($data['settlement_type']) as $idx => $settlementDay) {
      if ($settlementDay > $defaultSettlementType[$idx]) {
        return $listener->response(400, trans('api_users.invalid_settlement_type'));
      }
    }

    $userPaymentChannel->settlement_type = $data['settlement_type'];
    $this->pgUserPaymentChannelRepo->save($userPaymentChannel);

    return $listener->response(200, [
      'channel_id' => $data['channel_id'],
      'settlement_type' => $userPaymentChannel->settlement_type
    ]);
  }

  private function isValidEmailFormat($email) {
    return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$email);
  }

}
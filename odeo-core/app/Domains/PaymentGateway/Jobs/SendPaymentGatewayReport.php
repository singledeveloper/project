<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\PaymentGateway\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewayExporter;
use Odeo\Jobs\Job;

class SendPaymentGatewayReport extends Job {
  protected $filters, $email, $exportId;

  public function __construct($filters, $email, $exportId) {
    parent::__construct();
    $this->filters = $filters;
    $this->email = $email;
    $this->exportId = $exportId;
  }

  public function handle() {
    $filters = $this->filters;
    $exporter = app()->make(PaymentGatewayExporter::class);
    $exportQueueManager = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class);
    $file = $exporter->exportPaymentGatewayPayment($filters);

    $fileName = 'Laporan Payment Gateway ' . $filters['start_date'] . ' - ' . $filters['end_date'] . '.' . $filters['file_type'];
    $exportQueueManager->update($this->exportId, $file, $fileName);

    Mail::send('emails.payment_gateway_payment_export_mail', [
      'data' => $filters
    ], function ($m) use ($file, $fileName, $filters) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->attachData($file, $fileName);
      $m->to($this->email)->subject('Laporan Payment Gateway');
    });
  }
}

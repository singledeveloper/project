<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/7/17
 * Time: 5:35 PM
 */

namespace Odeo\Domains\PaymentGateway\Jobs;


use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewayExporter;
use Odeo\Jobs\Job;

class SendPaymentGatewayUsageReport extends Job {
  protected $filter, $email;

  public function __construct($filter, $email) {
    parent::__construct();
    $this->filter = $filter;
    $this->email = $email;
  }

  public function handle() {
    $filter = $this->filter;
    $exporter = app()->make(PaymentGatewayExporter::class);
    $file = $exporter->exportPaymentGatewayPayment($filter);
    $dateString = Carbon::parse($filter['start_date'])->toDateString();

    Mail::send('emails.payment_gateway_usage_report_mail', [
      'data' => [
        'start_date' => $dateString
      ]
    ], function ($m) use ($file, $filter, $dateString) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->attachData($file, "Laporan Payment Gateway-{$dateString}.{$filter['file_type']}");
      $m->to($this->email)->subject("Laporan Payment Gateway {$dateString}");
    });
  }
}

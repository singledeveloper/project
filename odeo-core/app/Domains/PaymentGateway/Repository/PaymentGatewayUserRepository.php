<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 27/11/18
 * Time: 19.47
 */

namespace Odeo\Domains\PaymentGateway\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\PaymentGateway\Helper\PaymentGatewaySettings;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayUser;

class PaymentGatewayUserRepository extends Repository {

  public function __construct(PaymentGatewayUser $pgUser) {
    $this->model = $pgUser;
  }

  public function findByClientId($clientId) {
    return $this->model
      ->join('oauth2_clients', 'payment_gateway_users.user_id', '=', 'oauth2_clients.user_id')
      ->where('client_id', '=', $clientId)
      ->select('oauth2_clients.*', 'payment_gateway_users.*')
      ->first();
  }

  public function getByClientId($clientId) {
    return $this->model
      ->join('oauth2_clients', 'payment_gateway_users.user_id', '=', 'oauth2_clients.user_id')
      ->where('client_id', $clientId)
      ->get();
  }

  public function findByUserId($userId) {
    return $this->model->where('user_id', $userId)->first();
  }

  public function getActivePrefixesByClientId($clientId) {
    return $this->model
      ->join('oauth2_clients', 'payment_gateway_users.user_id', '=', 'oauth2_clients.user_id')
      ->join('payment_gateway_user_payment_channels', 'payment_gateway_users.id', 'payment_gateway_user_payment_channels.pg_user_id')
      ->where('client_id', $clientId)
      ->where('payment_gateway_user_payment_channels.active', true)
      ->select('payment_gateway_user_payment_channels.prefix')
      ->get();
  }
}

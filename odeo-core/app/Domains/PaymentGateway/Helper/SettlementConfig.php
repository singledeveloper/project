<?php

namespace Odeo\Domains\PaymentGateway\Helper;

use Carbon\Carbon;
use Odeo\Domains\Constant\Payment;

class SettlementConfig {

  public function getSettlementAt($paymentGroupId, $settlementType, $notifyAt) {
    $settlementAt = $notifyAt->copy();
    $dayOfWeek = $settlementAt->dayOfWeek;

    switch ($paymentGroupId) {
      case Payment::OPC_GROUP_VA_BCA:
        if ($dayOfWeek === 5 && $settlementAt->hour >= 21) { 
          $dayOfWeek = 6;
        }
        break;
    }

    $settlementAt->addDays(intval($settlementType[$dayOfWeek]));
    return $settlementAt;
  }

  public function getConstantSettlementDay($paymentGroupId) {
    switch ($paymentGroupId) {
      case Payment::OPC_GROUP_ALFA:
        return 4;
      default:
        return 1;
    }
  }
}

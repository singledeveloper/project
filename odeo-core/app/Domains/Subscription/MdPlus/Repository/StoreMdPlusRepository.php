<?php

namespace Odeo\Domains\Subscription\MdPlus\Repository;

use Odeo\Domains\Constant\MdPlusStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\MdPlus\Model\StoreMdPlus;

class StoreMdPlusRepository extends Repository {

  public function __construct(StoreMdPlus $storeMdPlus) {
    $this->model = $storeMdPlus;
  }

  public function getRenewable() {
    return $this->model->where('status', MdPlusStatus::OK)->where('is_auto_renew', true)
      ->whereRaw("date(expired_at) <= date(NOW() + interval '3 days')")->get();
  }

  public function getExpired() {
    return $this->model->where('status', MdPlusStatus::OK)
      ->whereRaw("date(expired_at) < date(NOW()')")->get();
  }
}
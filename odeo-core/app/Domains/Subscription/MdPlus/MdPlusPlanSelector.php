<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;

class MdPlusPlanSelector {

  private $mdPlus, $store;

  public function __construct() {
    $this->mdPlusPlans = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\MdPlusPlanRepository::class);
    $this->serviceDetail = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }
  
  public function _transforms($item, Repository $repository) {
    $mdPlus = [];
    
    $mdPlus['id'] = $item->id;
    $mdPlus['name'] = trans('md_plus.plan_month', ['month' => $item->subscription_months]);
    $mdPlus['color'] = $item->color;
    $mdPlus['price'] = $this->currency->formatPrice($item->price);
    $mdPlus['discounted_price'] = $this->currency->formatPrice($item->discounted_price);
    $mdPlus['referral_bonus'] = $this->currency->formatPrice($item->referral_bonus);
    $mdPlus['subscription_months'] = $item->subscription_months;

    return $mdPlus;
  }
  
  public function get(PipelineListener $listener, $data) {
    $this->mdPlusPlans->normalizeFilters($data);

    $mdPlusPlans = [];

    foreach ($this->mdPlusPlans->gets() as $item) {
      $mdPlusPlans[] = $this->_transforms($item, $this->mdPlusPlans);
    }

    if (sizeof($mdPlusPlans) > 0) {
      $result['md_plus_plans'] = $mdPlusPlans;
      return $listener->response(200, array_merge($result, $this->mdPlusPlans->getPagination()));
    }
    return $listener->response(204, ["md_plus_plans" => []]);
  }

  public function getInformation(PipelineListener $listener, $data) {

    foreach (Plan::getAvailableServices(Plan::FREE) as $serviceId) {
      $key = Service::getConstKeyByValue($serviceId);
      $serviceDetail = $this->serviceDetail->findById(constant('Odeo\Domains\Constant\ServiceDetail::'.$key.'_ODEO'));
      
      $serviceName = $serviceDetail->service->displayed_name;
      $bonuses[$serviceName] = $this->currency->getFormattedOnly($serviceDetail->referral_cashback);
    }

    $bonusDescriptions = array(
      [
        trans('md_plus.bonus_header_1'),
        trans('md_plus.bonus_header_2'),
      ]
    );

    foreach ($bonuses as $serviceName => $bonusAmount) {
      $bonusDescriptions[] = [
        $serviceName,
        $bonusAmount
      ];
    }

    return $listener->response(200, [
      [
        '1_text' => trans('md_plus.description_1'),
      ],
      [
        '1_text' => trans('md_plus.description_2'),
      ],
      [
        '1_text' => trans('md_plus.description_3'),
        '3_table' => $bonusDescriptions,
      ],
      [
        '1_text' => trans('md_plus.description_4'),
      ],
      [
        '1_text' => trans('md_plus.description_5'),
      ],
    ]);
  }
  
}
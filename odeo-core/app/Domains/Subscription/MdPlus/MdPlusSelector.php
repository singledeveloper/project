<?php

namespace Odeo\Domains\Subscription\MdPlus;

use Odeo\Domains\Account\UserKtpValidator;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\MdPlusStatus;

class MdPlusSelector {

  private $mdPlus, $store, $currency;

  public function __construct() {
    $this->mdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }
  
  public function _transforms($item, Repository $repository) {
    $mdPlus = [];
    
    $mdPlus['store_id'] = $item->store_id;
    $mdPlus['is_auto_renew'] = $item->is_auto_renew;
    $mdPlus['plan_name'] = $item->plan->name;
    $mdPlus['created_at'] = $item->created_at->format('Y-m-d');
    $mdPlus['expired_at'] = $item->expired_at ? $item->expired_at->format('Y-m-d') : '';
    $mdPlus['is_free'] = $item->status == MdPlusStatus::OK_FREE;

    switch ($item->status) {
      case MdPlusStatus::OK:
      case MdPlusStatus::OK_FREE:
        $mdPlus['status'] = trans('md_plus.status_md_plus_active');
        break;
      default:
        $mdPlus['status'] = trans('md_plus.status_md_plus_inactive');
        break;
    }

    return $mdPlus;
  }
  
  public function _extends($data, Repository $repository) {
    if ($storeIds = $repository->beginExtend($data, 'store_id')) {
      if ($repository->hasExpand('order')) {
        $order = app()->make(\Odeo\Domains\Order\Helper\OrderManager::class);
        $repository->addExtend('order', $order->getMdPlusOrderByStoreId($storeIds));
      }
    }
    return $repository->finalizeExtend($data);
  }
  
  public function getDashboard(PipelineListener $listener, $data) {
    $lang = app('translator');
    
    $this->mdPlus->normalizeFilters($data);

    $storeOwner = $this->store->findOwner($data['store_id']); 

    if (!app()->make(UserKtpValidator::class)->isVerified($storeOwner->user_id)) {
      return $listener->response(200, [ 
        'referral_code' => '-', 
        'md_plus' => null,
        'description' => trans('md_plus.description_enable_md_plus_feature'), 
        'allow' => false 
      ], false, true); 
    } 

    $mdPlus = $this->mdPlus->findByAttributes('store_id', $data['store_id']);

    if ($mdPlus && $mdPlus->status != MdPlusStatus::PENDING) {
      $result['allow'] = true;
      $result['referral_code'] = strtoupper($this->store->findById($data['store_id'])->subdomain_name);
      $result['description'] = trans('md_plus.description_md_plus_feature');
      $result['md_plus'] = $this->_extends($this->_transforms($mdPlus, $this->mdPlus), $this->mdPlus);
      
      return $listener->response(200, $result);
    }

    return $listener->response(200, [
      'referral_code' => '-', 
      'md_plus' => null,
      'description' => trans('md_plus.description_md_plus_feature'), 
      'allow' => true 
    ], false, true);
  }
  
}
<?php

namespace Odeo\Domains\Subscription\MdPlus\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Jobs\Job;

class RenewMdPlus extends Job implements ShouldQueue {

  use SerializesModels;

  private $userId, $storeId, $mdPlusPlanId;

  public function  __construct($userId, $storeId, $mdPlusPlanId) {
    parent::__construct();
    $this->userId = $userId;
    $this->storeId = $storeId;
    $this->mdPlusPlanId = $mdPlusPlanId;
  }

  public function handle() {
    $data = [
      'info_id' => Payment::OPC_GROUP_OCASH,
      'service_detail_id' => ServiceDetail::MD_PLUS_ODEO,
      'gateway_id' => Payment::AUTO_RECURRING,
      'platform_id' => Platform::AUTO_RECURRING,
      'opc' => 375,
    ];

    $data['auth'] = [
      'user_id' => $this->userId,
      'type' => UserType::SELLER,
      'platform_id' => Platform::AUTO_RECURRING
    ];

    $pipeline = new Pipeline;

    $pipeline->add(new Task(CartRemover::class, 'clear'));
    $pipeline->add(new Task(CartInserter::class, 'addToCart', [
      'service_detail_id' => ServiceDetail::MD_PLUS_ODEO,
      'store_id' => $this->storeId,
      'md_plus_plan_id' => $this->mdPlusPlanId,
      'is_auto_renew' => true,
    ]));
    $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
    $pipeline->add(new Task(PaymentRequester::class, 'request'));
    $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
      'with_password' => true
    ]));

    $pipeline->enableTransaction();
    $pipeline->execute($data);
  }

}

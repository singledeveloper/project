<?php

namespace Odeo\Domains\Subscription\MdPlus\Scheduler;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\PipelineListener;
use Carbon\Carbon;

class UnsubscribeScheduler {

  private $mdPlus;

  public function __construct() {
    $this->mdPlus = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\StoreMdPlusRepository::class);
  }

  public function run() {
    foreach($this->mdPlus->getExpired() as $mdPlus) {
      $mdPlus->status = MdPlusStatus::EXPIRED;

      $this->mdPlus->save($mdPlus);
    }
  }

}

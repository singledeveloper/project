<?php

namespace Odeo\Domains\Subscription\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\Model\StoreAccount;

class StoreAccountRepository extends Repository {

  public function __construct(StoreAccount $storeAccount) {
    $this->model = $storeAccount;
  }
  
  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->first();
  }
}

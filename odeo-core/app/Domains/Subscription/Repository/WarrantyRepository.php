<?php

namespace Odeo\Domains\Subscription\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\WarrantyStatus;
use Odeo\Domains\Subscription\Model\Warranty;

class WarrantyRepository extends Repository {

  public function __construct(Warranty $warranty) {
    $this->model = $warranty;
  }
  
  public function findExists($storeId) {
    return $this->model->where(function($where){
      $where->where('status', WarrantyStatus::OK)->orWhere('status', WarrantyStatus::PENDING);
    })->where("store_id", $storeId)->first();
  }
  
  public function findPending($storeId) {
    return $this->model->where('status', WarrantyStatus::PENDING)->where("store_id", $storeId)->first();
  }
}

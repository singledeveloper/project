<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/3/17
 * Time: 16:22
 */

namespace Odeo\Domains\Subscription\DistributionChannel\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\DistributionChannel\Model\StoreDistributionChannel;

class StoreDistributionChannelRepository extends Repository {

  public function __construct(StoreDistributionChannel $storeDistributionChannel) {
    $this->model = $storeDistributionChannel;
  }

}
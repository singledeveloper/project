<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\PipelineListener;

class DomainLauncher {

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->cash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->selector = app()->make(\Odeo\Domains\Subscription\StoreSelector::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->banners = app()->make(\Odeo\Domains\Banner\Webstore\Repository\BannerRepository::class);
    $this->bannerHelper = app()->make(\Odeo\Domains\Banner\Webstore\Helper\BannerHelper::class);
    $this->services = app()->make(\Odeo\Domains\Inventory\Repository\ServiceRepository::class);
  }

  public function launchDomainSwitcher(PipelineListener $listener, $data) {

    $store = [];
    $bannerList = [];

    foreach ($this->banners->getActive() as $banner) {
      $bannerList[$banner->owner_type][$banner->owner_id][] = [
        'desktop' => AwsConfig::S3_BASE_URL . $banner->desktop_url,
        'mobile' => AwsConfig::S3_BASE_URL . $banner->mobile_url,
        'redirect' => $banner->redirect_url
      ];
    }

    $serviceDetailIds = [
      ServiceDetail::PULSA_ODEO,
      ServiceDetail::PAKET_DATA_ODEO,
      //ServiceDetail::BOLT_ODEO,
      ServiceDetail::PLN_ODEO,
      ServiceDetail::PULSA_POSTPAID_ODEO,
      ServiceDetail::PLN_POSTPAID_ODEO,
      ServiceDetail::BPJS_KES_ODEO,
      ServiceDetail::LANDLINE_ODEO,
      ServiceDetail::BROADBAND_ODEO,
      ServiceDetail::GOOGLE_PLAY_ODEO,
      ServiceDetail::GAME_VOUCHER_ODEO,
      ServiceDetail::TRANSPORTATION_ODEO,
      ServiceDetail::MULTI_FINANCE_ODEO,
      ServiceDetail::PDAM_ODEO,
      ServiceDetail::PGN_ODEO
    ];

    $serviceDetails = $this->serviceDetails->getDetails($serviceDetailIds);

    $queueSelectors = app()->make(\Odeo\Domains\Marketing\QueueSelector::class);

    $inventories = [];
    $storeIds = [];

    foreach ($serviceDetails as $detail) {
      $inventory = [];
      $inventory["type"] = $detail->service->name;
      $inventory['banner'] = isset($bannerList['service'][$detail->service_id]) ? $bannerList['service'][$detail->service_id] : [];
      $inventory['name'] = isset($detail->service->displayed_name) ? $detail->service->displayed_name : ucwords($detail->service->name);
      $inventory["service_detail_id"] = $detail->id;
      $inventory["service_id"] = $detail->service_id;
      $inventory['stock_available'] = true;

      $inventory['store_id'] = $queueSelectors->getFree($listener, [
        'service_id' => $detail->service_id,
        'skip_pipeline_response' => true
      ]);

      $storeIds[] = $inventory['store_id'];
      $inventories[] = $inventory;
    }

    $this->stores->normalizeFilters(['store_ids' => $storeIds, 'fields' => 'id,logo_path,name']);
    $stores = $this->stores->get();
    foreach ($inventories as &$inventory) {
      foreach ($stores as $s) {
        if ($s->id == $inventory['store_id']) {
          $inventory['provider_image_icon'] = parseFilePath($s->logo_path, "logo")['logo_image_hires'];
          $inventory['provider_name'] = $s->name;
        }
      }
    }
    $store["inventories"] = $inventories;

    unset($bannerList['service']);
    $store['banner_payment'] = $bannerList['payment'] ?? [];
    $store['banner_adsv_bottom_store'] = $bannerList['adsv'][1] ?? [];
    $store['version'] = env('WEB_STORE_LATEST_VERSION');
    $store['version_clear'] = env('WEB_STORE_LATEST_VERSION_CLEAR');
    return $listener->response(200, $store);
  }

  public function launch(PipelineListener $listener, $data) {

    $this->stores->normalizeFilters($data);

    $bannerList = [];

    if ($store = $this->stores->findByDomain($data["domain"])) {

      if (!$store->distribution->webstore) {
        return $listener->response(204);
      }

      $store = $this->selector->_transforms($store, $this->stores);
      $store = $this->selector->_extends($store, $this->stores);

      if (isset($store["plan_id"])) unset($store["plan_id"]);
      if (isset($store["current_data"])) {
        $serviceDetailIds = [];
        foreach ($store["current_data"] as $serviceDetailId) {
          if ($serviceDetailId != "0" && !in_array($serviceDetailId, $serviceDetailIds))
            $serviceDetailIds[] = $serviceDetailId;
        }

        foreach ($this->banners->getActive() as $banner) {
          $bannerList[$banner->owner_type][$banner->owner_id][] = [
            'desktop' => AwsConfig::S3_BASE_URL . $banner->desktop_url,
            'mobile' => AwsConfig::S3_BASE_URL . $banner->mobile_url,
            'redirect' => $banner->redirect_url
          ];
        }

        $services = $this->serviceDetails->getDetails($serviceDetailIds);
        $servicesTemp = [];
        foreach ($services as $item) {
          $servicesTemp[$item->id] = $item;
        }

        $inventories = [];
        foreach ($store["current_data"] as $serviceName => $serviceDetailId) {
          if ($serviceDetailId != "0" && isset($servicesTemp[$serviceDetailId])) {
            $inventory = [];
            $inven = $servicesTemp[$serviceDetailId];
            $inventory["type"] = $serviceName;
            $inventory['banner'] = isset($bannerList['service'][$inven->service_id]) ? $bannerList['service'][$inven->service_id] : [];
            $inventory['name'] = isset($inven->service->displayed_name) ? $inven->service->displayed_name : ucwords($serviceName);
            $inventory["service_detail_id"] = $inven->id;
            $inventory["service_id"] = $inven->service_id;
            $inventory['stock_available'] = (isset($store["deposit"]) && isset($store["deposit"]["amount"])) && $store["deposit"]["amount"] > $inven->service->minimum_deposit;
            if (file_exists(public_path('images/icon_powered_' . $inven->vendor->id . '.png'))) {
              $inventory["provider_image_icon"] = baseUrl('images/odeo-logo-1.png');
            }
            $inventories[] = $inventory;
          }
        }
        $store["inventories"] = $inventories;
        if (isset($store['deposit'])) unset($store['deposit']);
        unset($store["current_data"]);
      }

      $store['banner_payment'] = $bannerList['payment'] ?? [];
      $store['banner_adsv_bottom_store'] = $bannerList['adsv'][1] ?? [];
      $store['version'] = env('WEB_STORE_LATEST_VERSION');
      //ALL_EXCEPT_AUTH
      //NOTHING
      $store['version_clear'] = env('WEB_STORE_LATEST_VERSION_CLEAR');
      return $listener->response(200, $store);
    }
    return $listener->response(204);
  }

  public function getDomainDescription(PipelineListener $listener, $data) {

    $this->stores->normalizeFilters($data);

    $store = $this->stores->findByDomain($data["domain"]);

    $services = $store ? $this->services->findByIds(Plan::getAvailableServices($store->plan_id)) : $this->services->getOcommerceProduct();
    $servicesNames = [];
    foreach ($services as $item) {
      if ($item->id == Service::CREDIT_BILL) continue;
      $servicesNames[] = $item->displayed_name;
    }

    return $listener->response(200, [
      'title' => $store ? $store->name : 'ODEO - Server Pulsa H2H Termurah',
      'favicon' => $store && $store->favicon_path ? parseFilePath($store->favicon_path, "favicon") : (function () {
        return [
          'favicon_image_hires' => 'https://www.odeo.co.id/assets/img/favicon32.ico',
          'favicon_image_thumb' => 'https://www.odeo.co.id/assets/img/favicon32.ico',
          'favicon_image_normal' => 'https://www.odeo.co.id/assets/img/favicon32.ico',
        ];
      })(),
      'description' => 'Transaksi H2H, API, Jabber, Telegram ' . implode(', ', $servicesNames) . ' murah hanya di ' . ($store ? $store->name : 'ODEO') . '.',
      'logo' => $store && $store->logo_path ? parseFilePath($store->logo_path, "logo") : (function () {
        return [
          'logo_image_hires' => baseUrl('images/odeo-logo-1.png'),
          'logo_image_thumb' => baseUrl('images/odeo-logo-1.png'),
          'logo_image_normal' => baseUrl('images/odeo-logo-1.png'),
        ];
      })(),
      'is_google_crawlable' => !$store || ($store && $store->domain_name != null && strpos($data['domain'], $store->domain_name) !== false)
    ]);
  }

}

<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Transaction\Helper\Currency;

class DepositSelector implements SelectorListener {


  private $cashTransaction, $currency, $cashManager;

  public function __construct() {

    $this->cashTransaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
  }

  public function _transforms($item, Repository $repository) {

    $item->amount = $this->currency->formatPrice($item->amount);
    $item->balance_after = $this->currency->formatPrice($item->balance_after);
    $item->balance_before = $this->currency->formatPrice($item->balance_before);

    return $item;

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->cashTransaction->normalizeFilters($data);

    $deposits = [];

    foreach ($this->cashTransaction->getTransactionBy(TransactionType::CONVERT, CashType::ODEPOSIT, $data['store_id']) as $item) {
      $deposits[] = $this->_transforms($item, $this->cashTransaction);
    }

    if (sizeof($deposits) > 0) {

      $response['deposits'] = $deposits;

      if ($this->cashTransaction->hasExpand('total_deposit')) {
        $response['total_deposit'] = $this->cashManager->getStoreDepositBalance($data['store_id'], Currency::IDR)[$data['store_id']];
      }

      return $listener->response(200, array_merge($response,
        $this->cashTransaction->getPagination()
      ));
    }
    return $listener->response(204);

  }
}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/31/17
 * Time: 16:58
 */

namespace Odeo\Domains\Subscription\ManageInventory\Helper;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\Platform;

class PriceGetter {

  const FLAG_DISABLE_BASE_PRICE = null;

  public function __construct() {
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
  }

  public function getPriceForInventory($storeId, $data = [], $flag = null) {

    $query = $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_users', 'store_inventory_group_price_users.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id')
      ->join('users', 'users.id', '=', 'store_inventory_group_price_users.user_id')
      ->join('store_inventory_group_price_details as my_details', function ($join) {
        $join->on('my_details.store_inventory_detail_id', '=', 'store_inventory_details.id');
        $join->on('my_details.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id');
      });

    // \Log::info(json_encode($query->where('inventory_id', 1)->where('store_id', $storeId)->get()));

    if (isset($data['category']) || isset($data['operator_id']))
      $query->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id');
    if (isset($data['category'])) {
      if (!is_array($data['category'])) $data['category'] = explode(',', $data['category']);
      $query->whereIn('pulsa_odeos.category', $data['category']);
    }
    if (isset($data['operator_id']))
      $query->where('pulsa_odeos.operator_id', $data['operator_id']);
    if (isset($data['service_detail_id']))
      $query->where('store_inventories.service_detail_id', $data['service_detail_id']);
    if (isset($data['service_id']))
      $query->where('store_inventories.service_id', $data['service_id']);
    if (isset($data['inventory_ids']))
      $query->whereIn('store_inventory_details.inventory_id', $data['inventory_ids']);

    //temporary
    $query->where('my_details.sell_price', '>', 1000);

    $query->where('store_id', $storeId)
      ->where('store_inventory_group_price_users.user_id', $data['user_id'])
      ->select(
        'store_inventory_details.id as inventory_detail_id',
        'my_details.sell_price',
        'store_inventory_details.inventory_id'
      );

    if (!isVersionSatisfy(Platform::ANDROID, '3.1.4')) {
      $query->where('users.purchase_preferred_store_id', $storeId);
    }


    if ($flag != self::FLAG_DISABLE_BASE_PRICE) {
      $query->leftJoin('store_inventory_group_price_details as vendor_details', 'vendor_details.id', '=', 'store_inventory_details.vendor_group_price_detail_id');
      $query->addSelect('vendor_details.sell_price as base_price');
    }

    return $query
      ->get()
      ->keyBy('inventory_id');
  }

  public function checkSupplyChainExistFromInventoryCode($data) {
    return $this->storeInventories->getModel()
      ->join('store_inventory_details', 'store_inventory_details.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_users', 'store_inventory_group_price_users.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id')
      ->join('users', 'users.id', '=', 'store_inventory_group_price_users.user_id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->where('store_id', $data['store_id'])
      ->where('users.purchase_preferred_store_id', $data['store_id'])
      ->where('store_inventory_group_price_users.user_id', $data['user_id'])
      ->where('pulsa_odeos.inventory_code', $data['inventory_code'])
      ->whereNotNull('pulsa_odeos.owner_store_id')
      ->select('pulsa_odeos.id', 'pulsa_odeos.category', 'pulsa_odeos.status')
      ->first();
  }

  public function applyAgentPrice(PipelineListener $listener, $data) {

    $h2hManager = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Helper\PulsaH2HManager::class);
    $userId = $data['user_id'] ?? $data['auth']['user_id'];

    if (!$h2hManager->isUserH2H($userId) && $price = $this->getPriceForInventory($data['store_id'], [
      'store_id' => $data['store_id'],
      'service_detail_id' => $data['service_detail_id'],
      'inventory_ids' => [$data['inventory_id']],
      'user_id' => $userId
    ])->first()
    ) {

      return $listener->response(200, [
        'agent_base_price' => $price->base_price ?? $data['merchant_price'],
        'sale_price' => $price->sell_price,
        'store_inventory_detail_id' => $price->inventory_detail_id
      ]);
    }

    return $listener->response(200);

  }


}

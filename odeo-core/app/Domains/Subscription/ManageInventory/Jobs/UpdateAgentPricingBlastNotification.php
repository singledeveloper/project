<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/17/17
 * Time: 21:20
 */

namespace Odeo\Domains\Subscription\ManageInventory\Jobs;

use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\Service;
use Odeo\Jobs\Job;

class UpdateAgentPricingBlastNotification extends Job {

  private $data;
  private $notifications, $storeInventories, $stores;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  public function handle() {

    $data = $this->data;

    $subscribed = $this->getSubscribedUser($data);
    $userSubscribedVendors = $this->getUserSubscribedVendor($data, $subscribed->pluck('user_id'));
    $vendorStore = $this->stores->findById($data['vendor_id']);
    $serviceNames = [];

    if ($subscribed->isEmpty()) return;

    //suatu hari harus ganti ke topic, ini user banyak bisa mampos

    foreach ($subscribed as $sub) {

      $serviceName = $serviceNames[$sub->service_id] ?? (
        $serviceNames[$sub->service_id] = strtolower(str_replace('_', ' ', Service::getConstKeyByValue($sub->service_id)))
        );

      $this->notifications->setup($sub->user_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);

      if (isset($userSubscribedVendors[$sub->user_id])) {
        $vendor = $userSubscribedVendors[$sub->user_id];
        $this->notifications->inventoryPricingUpdate(
          $vendor->name, $serviceName, $vendor->store_id, $data['reflected_date']
        );
      } else {
        $this->notifications->agentPricingUpdate(
          $vendorStore->name, $serviceName, $data['reflected_date']
        );
      }


    }

    dispatch($this->notifications->queue());

  }


  public function getUserSubscribedVendor($data, $userIds) {
    return $this->storeInventories->getModel()
      ->join('store_inventory_vendors', 'store_inventory_vendors.store_inventory_id', '=', 'store_inventories.id')
      ->join('user_stores', 'user_stores.store_id', '=', 'store_inventories.store_id')
      ->join('stores', 'stores.id', '=', 'user_stores.store_id')
      ->where('store_inventory_vendors.vendor_id', $data['vendor_id'])
      ->where('user_stores.type', 'owner')
      ->whereIn('user_stores.user_id', $userIds)
      ->select(
        'user_stores.user_id',
        'store_inventories.store_id',
        'stores.name'
      )
      ->get()
      ->keyBy('user_id');
  }


  public function getSubscribedUser($data) {

    $query = $this->storeInventories->getModel()
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.store_inventory_id', '=', 'store_inventories.id')
      ->join('store_inventory_group_price_users', 'store_inventory_group_price_users.store_inventory_group_price_id', '=', 'store_inventory_group_prices.id')
      ->where('store_inventories.store_id', '=', $data['vendor_id'])
      ->whereIn('store_inventory_group_price_users.store_inventory_group_price_id', is_array($data['group_price_id']) ? $data['group_price_id'] : [$data['group_price_id']]);

    if (isset($data['agent_user_id'])) {
      $query->whereIn('store_inventory_group_price_users.user_id', is_array($data['agent_user_id']) ? $data['agent_user_id'] : [$data['agent_user_id']]);
    }

    return $query
      ->select(
        'store_inventories.service_id',
        'store_inventory_group_price_users.user_id'
      )
      ->get();
  }

}

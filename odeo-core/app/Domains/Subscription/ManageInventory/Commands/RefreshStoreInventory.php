<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 6:16 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Subscription\ManageInventory\Jobs\AddNewProductToStoreInventory;
use Odeo\Domains\Subscription\ManageInventory\Jobs\RemoveDeletedProductInStoreInventory;

class RefreshStoreInventory extends Command {

  protected $signature = 'store-inventory:refresh';

  protected $description = 'Refresh Store Inventory';

  private $redis, $pulsaOdeos;

  public function __construct() {
    parent::__construct();
    $this->redis = Redis::connection();
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
  }

  public function handle() {

    $normalizedCaches = $rawCaches = null;
    $toBeAddedCache = $toBeAddedStoreInventory = [];
    $toBeRemovedCache = $toBeRemovedStoreInventory = [];

    if ($this->redis->get('odeo_core:store_inventory:refresh_inventory_cache_lock') ||
      $this->redis->get('odeo_core:add_store_inventory_lock')
    ) {
      $this->error('Store inventory is still refreshing');
      return;
    }

    $inventories = $this->pulsaOdeos->getCloneModel()
      ->select('id', 'service_detail_id')
      ->get();

    $rawCaches = $this->redis->hmget('odeo_core:store_inventory:inventory_cache', (function () use ($inventories) {
      $temp = [];
      foreach ($inventories as $inv) {
        $temp[] = 'service_detail_' . $inv->service_detail_id . '_inventory_' . $inv->id;
        $normalizedCaches[] = [$inv->service_detail_id . '-' . $inv->id];
      }
      return $temp;
    }));

    $normalizedCaches = array_combine($normalizedCaches, $rawCaches);

    foreach ($inventories as $inv) {

      if ($inv->deleted_at && $normalizedCaches[$inv->service_detail_id . '-' . $inv->id]) {
        $toBeRemovedCache[] = [
          'service_detail_' . $inv->service_detail_id . '_inventory_' . $inv->id
        ];
        $toBeRemovedStoreInventory[$inv->service_detail_id][] = $inv->id;
      } else {
        $toBeAddedCache[] = [
          'service_detail_' . $inv->service_detail_id . '_inventory_' . $inv->id
        ];
        $toBeAddedStoreInventory[$inv->service_detail_id][] = $inv->id;
      }
    }

    $this->redis->pipeline(function ($pipeline) use ($toBeAddedCache, $toBeRemovedCache) {

      $needAdd = !empty($toBeAddedCache);
      $needRemove = !empty($toBeRemovedCache);

      ($needAdd || $needRemove) && $pipeline->multi();

      $needRemove && $pipeline->hdel('odeo_core:store_inventory:inventory_cache', $toBeRemovedCache);
      $needAdd && $pipeline->hmset('odeo_core:store_inventory:inventory_cache', $toBeAddedCache);

      ($needAdd || $needRemove) && $pipeline->exec();

    });

    foreach ($toBeAddedStoreInventory as $serviceDetailId => $iventoryIds) {
      dispatch(new AddNewProductToStoreInventory([
        'service_detail_id' => $serviceDetailId,
        'inventory_ids' => $iventoryIds
      ]));
    }
    foreach ($toBeRemovedStoreInventory as $serviceDetailId => $iventoryIds) {
      dispatch(new RemoveDeletedProductInStoreInventory([
        'service_detail_id' => $serviceDetailId,
        'inventory_ids' => $iventoryIds
      ]));
    }

    $this->redis->get('odeo_core:store_inventory:inventory_cache');

    $this->info('Success dispatching store inventories');

  }
}

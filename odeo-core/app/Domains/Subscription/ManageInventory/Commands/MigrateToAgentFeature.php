<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 6:16 PM
 */

namespace Odeo\Domains\Subscription\ManageInventory\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\InventoryGroupPrice;
use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateStoreInventory;
use Odeo\Domains\Subscription\ManageInventory\Jobs\PopulateUserGroupPrice;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryGroupPriceUserRepository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository;

class MigrateToAgentFeature extends Command {

  protected $signature = 'store-inventory:migrate {flag}';

  protected $description = 'Migrate to agent feature';

  private $stores;

  public function __construct() {
    parent::__construct();
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->groupPriceUsers = app()->make(StoreInventoryGroupPriceUserRepository::class);
    $this->groupPrices = app()->make(StoreInventoryGroupPriceRepository::class);
    $this->storeInventories = app()->make(StoreInventoryRepository::class);
    $this->cashes = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
  }

  public function handle() {

    $flag = $this->argument('flag');

//    if (app()->environment() != 'production') {
//
//      if ($flag == 1) {
//        foreach ($this->stores->getModel()
//                   ->whereIn('status', StoreStatus::ACTIVE)
//                    ->whereIn('stores.id', [
//                      15, 22, 18, 10, 16, 6, 13, 12,
//                      20, 8, 21, 14, 9, 19, 796, 797
//                    ])
//                   ->get() as $s) {
//          dispatch(new PopulateStoreInventory([
//            'store_id' => $s->id
//          ]));
//        }
//      }
//
//      if ($flag == 2) {
//        foreach (
//          $this->agents->getModel()
//            ->join('stores', 'stores.id', '=', 'agents.store_referred_id')
//            ->where('agents.status', AgentConfig::ACCEPTED)
//            ->whereIn('agents.user_id', [
//              66631,58202,62025,71298,63374,11437,67150,64598,13,62110,67314,70498,59955,34447,55421,44958,1,58743,67829,27605,52273
//            ])
//            ->select(
//              'agents.user_id',
//              'store_referred_id',
//              'stores.plan_id'
//            )
//            ->get() as $s) {
//
//          $owner = $this->stores->findOwner($s->store_referred_id);
//
//          dispatch(new PopulateUserGroupPrice([
//            'plan_id' => $s->plan_id,
//            'user_id' => $s->user_id,
//            'store_id' => $s->store_referred_id,
//            'group_price_type' => $owner->user_id == $s->user_id ? InventoryGroupPrice::SELF : InventoryGroupPrice::AGENT_DEFAULT
//          ]));
//
//        }
//      }


//
//    if ($flag == 3) {
//      app()->make(StoreInventoryGroupPriceRepository::class)->getModel()
//        ->whereIn('type', [
//          InventoryGroupPrice::MARKET_PLACE,
//        ])
//        ->update([
//          'keep_profit_consistent' => false
//        ]);
//    }
//
//
//    if ($flag == 4) {
//
//      $inventories = \DB::select(\DB::raw("
//        SELECT
//          store_inventories.store_id,
//          store_inventory_vendors.vendor_id,
//          stores.plan_id,
//          vendor.plan_id vendor_plan_id,
//          user_stores.user_id,
//          user_vendor.user_id AS vendor_user_id
//        FROM user_stores
//          JOIN stores ON user_stores.store_id = stores.id
//          JOIN store_inventories ON stores.id = store_inventories.store_id
//          JOIN store_inventory_vendors ON store_inventories.id = store_inventory_vendors.store_inventory_id
//          LEFT JOIN stores vendor ON vendor.id = store_inventory_vendors.vendor_id
//          LEFT JOIN user_stores user_vendor ON user_vendor.store_id = vendor.id
//        WHERE stores.status = '50000' AND user_vendor.user_id = user_stores.user_id
//      "));
//
//      foreach ($inventories as $inv) {
//
//        $currGroupPrice = $this->groupPriceUsers->getModel()
//          ->join('store_inventory_group_prices', 'store_inventory_group_prices.id', '=', 'store_inventory_group_price_users.store_inventory_group_price_id')
//          ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
//          ->where('user_id', $inv->user_id)
//          ->where('store_id', $inv->vendor_id)
//          ->select(
//            'store_inventory_group_price_users.id',
//            'service_detail_id'
//          )
//          ->get();
//
//
//        foreach ($currGroupPrice as $groupPrice) {
//
//          $p = $this->groupPriceUsers->findById($groupPrice->id);
//
//          if (!$p) {
//            throw new \Exception();
//          }
//
//          $p->store_inventory_group_price_id = $this->groupPrices->getModel()
//            ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
//            ->where('store_inventories.service_detail_id', $groupPrice->service_detail_id)
//            ->where('store_inventories.store_id', $inv->vendor_id)
//            ->where('store_inventory_group_prices.type', InventoryGroupPrice::SELF)
//            ->select('store_inventory_group_prices.id')->first()->id;
//
//          if (!$p->store_inventory_group_price_id) {
//            throw new \Exception('tidak ketemu');
//          }
//
//          $this->groupPriceUsers->save($p);
//
//        }
//
//
//      }
//
//    }
//
//    if ($flag == 5) {
//
//      $inventories = \DB::select(\DB::raw("
//        SELECT
//          store_inventories.store_id,
//          store_inventory_vendors.vendor_id,
//          stores.plan_id,
//          vendor.plan_id vendor_plan_id,
//          user_stores.user_id,
//          user_vendor.user_id AS vendor_user_id,
//          store_inventories.service_detail_id
//        FROM user_stores
//          JOIN stores ON user_stores.store_id = stores.id
//          JOIN store_inventories ON stores.id = store_inventories.store_id
//          JOIN store_inventory_vendors ON store_inventories.id = store_inventory_vendors.store_inventory_id
//          LEFT JOIN stores vendor ON vendor.id = store_inventory_vendors.vendor_id
//          LEFT JOIN user_stores user_vendor ON user_vendor.store_id = vendor.id
//        WHERE stores.status = '50000' AND user_vendor.user_id = user_stores.user_id
//      "));
//
//
//      foreach ($inventories as $inv) {
//
//        $userGroupPrice = $this->groupPriceUsers->getUserGroupPrice($inv->user_id, $inv->vendor_id, $inv->service_detail_id)->first();
//
//        if (!$userGroupPrice) {
//          throw new \Exception();
//        }
//
//        $vendorInventories = $this->storeInventories->getStorePricing([
//          'store_id' => $inv->vendor_id,
//          'service_detail_id' => $inv->service_detail_id,
//          'group_price_id' => $userGroupPrice->store_inventory_group_price_id
//        ], [
//          'store_inventory_details.inventory_id as inventory_id',
//          'store_inventory_group_price_details.id as store_inventory_group_price_detail_id'
//        ], false);
//
//        $tobeUpdate = [];
//
//        foreach ($vendorInventories as $vInv) {
//          $temp = [
//            'inventory_id' => $vInv->inventory_id,
//            'vendor_group_price_detail_id' => $vInv->store_inventory_group_price_detail_id
//          ];
//
//          $tobeUpdate[] = '(' . implode(',', $temp) . ')';
//        }
//
//
//        \DB::statement("
//          WITH inventories AS (
//            SELECT d.vendor_group_price_detail_id, store_inventory_details.id
//            FROM
//              store_inventories
//              JOIN store_inventory_details ON store_inventories.id = store_inventory_details.store_inventory_id
//              JOIN
//              (VALUES
//                " . implode(',', $tobeUpdate) . "
//              ) AS D(inventory_id, vendor_group_price_detail_id) ON D.inventory_id = store_inventory_details.inventory_id
//            WHERE
//              D.inventory_id = store_inventory_details.inventory_id AND
//              store_inventories.store_id = " . $inv->store_id . " AND
//              store_inventories.service_detail_id = " . $inv->service_detail_id . "
//          )
//          UPDATE store_inventory_details AS invd
//          SET
//            vendor_group_price_detail_id = d.vendor_group_price_detail_id
//          FROM
//            inventories AS d
//          WHERE d.id = invd.id
//        ");
//
//      }
//    }
//
//    if ($flag == 6) {
//
//      $stores = $this->stores->getModel()
//        ->where('plan_id', Plan::FREE)
//        ->whereIn('status', StoreStatus::ACTIVE)
//        ->get();
//
//      $storeDeposits = $this->cashes->findStoreBalance($stores->pluck('id')->toArray(), CashType::ODEPOSIT);
//
//      foreach ($stores as $store) {
//        if (isset($storeDeposits[$store->id]) && $storeDeposits[$store->id] > 0) {
//          $userId = $store->owner->first()->id;
//          $storeId = $store->id;
//          $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
//          $inserter->add([
//            'user_id' => $userId,
//            'store_id' => $storeId,
//            'trx_type' => \Odeo\Domains\Constant\TransactionType::RETURN_DEPOSIT,
//            'cash_type' => \Odeo\Domains\Constant\CashType::ODEPOSIT,
//            'amount' => -$storeDeposits[$storeId],
//            'data' => json_encode([
//              'notes' => 'remove oDeposit on store FREE'
//            ])
//          ]);
//          $inserter->add([
//            'user_id' => $userId,
//            'trx_type' => \Odeo\Domains\Constant\TransactionType::RETURN_DEPOSIT,
//            'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
//            'amount' => $storeDeposits[$storeId],
//            'data' => json_encode([
//              'notes' => 'remove oDeposit on store FREE'
//            ])
//          ]);
//          $inserter->run();
//        }
//      }
//    }

//
//    $stores = $this->stores->getModel()
//      ->where('plan_id', Plan::FREE)
//      ->whereIn('status', StoreStatus::ACTIVE)
//      ->whereIn('id', [
//        3665, 3733, 3860, 3919, 3990
//      ])
//      ->get();
//
//    $storeDeposits = $this->cashes->findStoreBalance($stores->pluck('id')->toArray(), CashType::ODEPOSIT);
//
//    foreach ($stores as $store) {
//      if (isset($storeDeposits[$store->id]) && $storeDeposits[$store->id] > 0) {
//        $userId = $store->owner->first()->id;
//        $storeId = $store->id;
//        $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
//        $inserter->add([
//          'user_id' => $userId,
//          'store_id' => $storeId,
//          'trx_type' => \Odeo\Domains\Constant\TransactionType::RETURN_DEPOSIT,
//          'cash_type' => \Odeo\Domains\Constant\CashType::ODEPOSIT,
//          'amount' => -$storeDeposits[$storeId],
//          'data' => json_encode([
//            'notes' => 'remove oDeposit on store FREE'
//          ])
//        ]);
//        $inserter->add([
//          'user_id' => $userId,
//          'trx_type' => \Odeo\Domains\Constant\TransactionType::RETURN_DEPOSIT,
//          'cash_type' => \Odeo\Domains\Constant\CashType::OCASH,
//          'amount' => $storeDeposits[$storeId],
//          'data' => json_encode([
//            'notes' => 'remove oDeposit on store FREE'
//          ])
//        ]);
//        $inserter->run();
//      }
//    }

  }
}

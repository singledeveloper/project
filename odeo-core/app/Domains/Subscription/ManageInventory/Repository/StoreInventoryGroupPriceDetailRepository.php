<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/1/17
 * Time: 15:57
 */

namespace Odeo\Domains\Subscription\ManageInventory\Repository;


use Odeo\Domains\Constant\InventoryStore;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventoryGroupPriceDetail;

class StoreInventoryGroupPriceDetailRepository extends Repository {

  public function __construct(StoreInventoryGroupPriceDetail $storeInventoryGroupPriceDetail) {
    $this->model = $storeInventoryGroupPriceDetail;
  }

  public function getAllDetails($groupIds) {
    return $this->model
      ->join('store_inventory_details', 'store_inventory_details.id', '=', 'store_inventory_group_price_details.store_inventory_detail_id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->join('pulsa_odeo_inventories', 'pulsa_odeo_inventories.pulsa_odeo_id', '=', 'pulsa_odeos.id')
      ->join('vendor_switchers', 'vendor_switchers.id', '=', 'pulsa_odeo_inventories.vendor_switcher_id')
      ->whereIn('store_inventory_group_price_details.store_inventory_group_price_id', $groupIds)
      ->where('pulsa_odeo_inventories.is_active', true)
      ->select(
        'store_inventory_group_price_details.id',
        'pulsa_odeos.id as pulsa_id',
        'pulsa_odeos.name as pulsa_name',
        'vendor_switchers.id as vendor_switcher_id',
        'vendor_switchers.name as vendor_switcher_name',
        'pulsa_odeos.inventory_code as supply_code',
        'pulsa_odeo_inventories.code as biller_code',
        'pulsa_odeos.price as store_base_price',
        'store_inventory_group_price_details.sell_price'
      )
      ->orderBy('pulsa_odeos.name', 'asc')
      ->orderBy('vendor_switchers.name', 'asc')->get();
  }

  public function getAllInventoryIdsInGroup($supplyGroupId) {
    return $this->model
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.id', '=', 'store_inventory_group_price_details.store_inventory_group_price_id')
      ->join('store_inventory_details', 'store_inventory_details.id', '=', 'store_inventory_group_price_details.store_inventory_detail_id')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'store_inventory_details.inventory_id')
      ->where('store_inventory_group_prices.supply_group_id', $supplyGroupId)
      ->where('pulsa_odeos.is_active', true)
      ->select('inventory_id')->get();
  }

}
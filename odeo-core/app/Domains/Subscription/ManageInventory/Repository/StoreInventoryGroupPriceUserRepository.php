<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/1/17
 * Time: 15:59
 */

namespace Odeo\Domains\Subscription\ManageInventory\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Model\StoreInventoryGroupPriceUser;

class StoreInventoryGroupPriceUserRepository extends Repository {

  public function __construct(StoreInventoryGroupPriceUser $groupPriceUser) {
    $this->model = $groupPriceUser;
  }

  public function getUserGroupPrice($userId, $storeId, $serviceDetailId) {

    return $this->model
      ->join('store_inventory_group_prices', 'store_inventory_group_prices.id', '=', 'store_inventory_group_price_users.store_inventory_group_price_id')
      ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
      ->where('user_id', $userId)
      ->whereIn('store_inventories.store_id', (is_array($storeId) ? $storeId : [$storeId]))
      ->where('store_inventories.service_detail_id', (int)$serviceDetailId)
      ->select(
        'store_inventory_group_price_users.store_inventory_group_price_id',
        'store_inventory_group_price_users.new_store_inventory_group_price_id',
        'group_price_apply_at'
      )
      ->get();
  }

  public function getGroupPricePricingToApply() {
      return $this->model
        ->join('store_inventory_group_prices', 'store_inventory_group_prices.id', '=', 'store_inventory_group_price_users.store_inventory_group_price_id')
        ->join('store_inventories', 'store_inventories.id', '=', 'store_inventory_group_prices.store_inventory_id')
        ->where('group_price_apply_at', '<=', Carbon::now()->toDateTimeString())
        ->select(
          'store_inventory_group_price_users.id as group_price_user_id',
          'user_id',
          'store_id',
          'service_detail_id',
          'new_store_inventory_group_price_id'
        )
        ->get();
  }

  public function checkIsGroupPriceHasUserOnProgressUpdate($groupPriceId) {
    return $this->model
      ->where('new_store_inventory_group_price_id', $groupPriceId)
      ->whereNotNull('group_price_apply_at')
      ->select('id')
      ->count();
  }

  public function findSupplyAgent($userId, $storeInventoryId) {

    return $this->model->whereHas('storeInventoryGroupPrice', function($query) use ($storeInventoryId) {
      $query->where('store_inventory_id', $storeInventoryId);
    })->where('user_id', $userId)->first();
  }

  public function getByGroupPriceId($groupPriceId) {
    return $this->model->where('store_inventory_group_price_id', $groupPriceId)->get();
  }
  
}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/1/17
 * Time: 15:57
 */

namespace Odeo\Domains\Subscription\ManageInventory\Model;


use Odeo\Domains\Core\Entity;

class StoreInventoryGroupPrice extends Entity {

  public function storeInventory() {
    return $this->belongsTo(StoreInventory::class, 'store_inventory_id');
  }

}
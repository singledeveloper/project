<?php

namespace Odeo\Domains\Subscription;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Core\PipelineListener;

class DomainVerificator {

  public function __construct() {
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->treeParent = app()->make(\Odeo\Domains\Network\Repository\TreeParentRepository::class);
  }

  public function checkSubdomain(PipelineListener $listener, $data) {
    $data["subdomain_name"] = strtolower($data['subdomain_name']);
    if (isset($data["subdomain_name"]) && $this->store->findBySubdomain($data["subdomain_name"])) {
      return $listener->response(400, trans('errors.subdomain_name_taken'));
    } else if (Plan::isReservedKeyword($data['subdomain_name'])) {
      return $listener->response(400, "Domain name is contained reserved or banned keyword.");
    }
    return $listener->response(200);
  }

  public function checkDomain(PipelineListener $listener, $data) {
    if (isset($data["domain_name"]) && $this->store->findByDomain($data["domain_name"])) {
      return $listener->response(400, "Domain was already taken.");
    }
    return $listener->response(200);
  }

  public function checkInvitationCode(PipelineListener $listener, $data) {
    if (isset($data["invitation_code"])) {

      $referredStore = $this->store->findBySubdomain(strtolower($data["invitation_code"]), [
        'with' => [
          'network'
        ]
      ]);

      if (!$referredStore) {
        return $listener->response(400, "Invitation Code is not valid.");
      } else if ($referredStore->plan_id === Plan::FREE) {
        return $listener->response(400, "You can't use ivitation code from FREE store.");
      } else if (isset($data['store_id'])) {

        $store = $this->store->findById($data['store_id']);

        if ($data["store_id"] == $referredStore->id) {
          return $listener->response(400, "You can't use your own invitation code.");

        } else if (($storeNework = $store->network) && $storeNework->referred_store_id != NULL) {
          return $listener->response(400, "You can't re-refer this store with another invitation code.");

        } else if (in_array($data['store_id'], (function () use ($referredStore) {
          return array_merge(
            explode(",", $referredStore->network->referred_store_ids),
            explode(",", $referredStore->network->hustler_store_ids),
            explode(",", $referredStore->network->grand_hustler_store_ids),
            explode(",", $referredStore->network->team_store_ids)
          );

        })())) {

          return $listener->response(400, "You can't use an invitation code from your downline(s).");
        }
      }

      return $listener->response(200, ["referred_store_id" => $referredStore->id]);
    }
    return $listener->response(200);
  }
}

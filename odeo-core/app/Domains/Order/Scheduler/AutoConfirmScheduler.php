<?php
namespace Odeo\Domains\Order\Scheduler;

use Carbon\Carbon;

class AutoConfirmScheduler {

  private $orders;

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function run() {
    foreach ($this->orders->getUnconfirmedSuccessOrder() as $order) {
      $order->confirm_receipt = true;
      $order->confirmed_at = Carbon::now();

      $this->orders->save($order);
    }
  }
}

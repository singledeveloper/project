<?php
namespace Odeo\Domains\Order\Scheduler;

class PulsaRecurringDeactivateScheduler {

  private $pulsaRecurring;

  public function __construct() {
    $this->pulsaRecurring = app()->make(\Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository::class);
  }

  public function run() {
    if (date('H') >= 7 && date('H') < 23) {
      $this->pulsaRecurring->deactivateExpiredRecurring();
    }
  }
}

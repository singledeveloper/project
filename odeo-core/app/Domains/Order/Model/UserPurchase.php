<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 11:46 PM
 */

namespace Odeo\Domains\Order\Model;

use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Inventory\Model\Service;
use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\PaymentOdeoPaymentChannelInformation;

class UserPurchase extends Entity {

  public function service() {
    return $this->belongsTo(Service::class);
  }

  public function information() {
    return $this->belongsTo(PaymentOdeoPaymentChannelInformation::class, 'info_id');
  }

  public function user() {
    return $this->belongsTo(User::class);
  }
}
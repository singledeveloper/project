<?php

namespace Odeo\Domains\Order\Vendor\SMS;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\CenterManager;

class OrderSelector extends CenterManager {

  private $switcherOrder, $status;

  public function __construct() {
    parent::__construct();
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $this->saveHistory($data);

    list($command, $number, $pin) = explode('.', $data['sms_message']);

    if ($switcher = $this->switcherOrder->findByNumber(purifyTelephone($number), $data['auth']['user_id'])) {
      $orderDetail = $switcher->orderDetail;
      $order = $orderDetail->order;

      $this->reply("#" . $order->id . ' status: ' . $this->status->parse($order)['status_message'] .
        ', ' . $number . ' - ' . $orderDetail->name . ' - SN: ' . ($switcher->serial_number != null ? $switcher->serial_number : 'N/A') .
        ' @' . date('d/m/Y H:i', strtotime($switcher->requested_at)), $data['sms_to']);
    }
    else $this->reply('Nomor ' . $number . ' tidak ditemukan', $data['sms_to']);

    return $listener->response(200);
  }

}
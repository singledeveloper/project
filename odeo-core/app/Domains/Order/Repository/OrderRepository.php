<?php

namespace Odeo\Domains\Order\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\Order;


class OrderRepository extends Repository {

  public function __construct(Order $order) {
    $this->model = $order;
  }

  public function getWithAdminPrivilege() {

    $filters = $this->getFilters();

    $query = $this->model
      ->select(\DB::raw('
        orders.*,
        stores.name as store_name,
        service_details.service_id,
        services.displayed_name as service_name,
        order_details.name as item_name,
        order_detail_pulsa_switchers.number,
        payments.opc,
        payments.info_id,
        payment_odeo_payment_channel_informations.actual_name as payment_name
      '))
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->join('payments', 'payments.order_id', '=', 'orders.id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->join('services', 'service_details.service_id', '=', 'services.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->leftJoin('stores', 'stores.id', '=', 'orders.actual_store_id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id')
      ->with(['charges'])
      ->orderBy($filters['sort_by'], $filters['sort_type']);

    if (isset($filters['search'])) {

      if (isset($filters['search']['seller_store_id'])) {
        if (isset($filters['search']['additional_store_ids'])) {
          $query->whereIn('actual_store_id', array_merge([$filters['search']['seller_store_id']], $filters['search']['additional_store_ids']));
        } else {
          $query->where('actual_store_id', $filters['search']['seller_store_id']);
        }
      }
    }

    if (isset($filters['search']['date_type'])) {
      if (isset($filters['search']['start_date'])) {
        $query->where('orders.' . $filters['search']['date_type'], '>=', Carbon::parse($filters['search']['start_date'])->startOfDay());
      }
      if (isset($filters['search']['end_date'])) {
        $query->where('orders.' . $filters['search']['date_type'], '<=', Carbon::parse($filters['search']['end_date'])->endOfDay());
      }
    }

    if (isset($filters['search']['order_id'])) {
      $query->where('orders.id', $filters['search']['order_id']);
    }

    if (isset($filters['search']['user_id'])) {
      $query->where('orders.user_id', $filters['search']['user_id']);
    }

    if (isset($filters['search']['platform_id'])) {
      $query->where('orders.platform_id', $filters['search']['platform_id']);
    }

    if (isset($filters['search']['service_id'])) {
      $serviceId = $filters['search']['service_id'];
      $query->where('service_details.service_id', $serviceId);
    }

    if (isset($filters['search']['payment_method'])) {
      $id = $filters['search']['payment_method'];
      $query->where('payments.info_id', $id);
    }

    if (isset($filters['search']['status'])) {
      if (is_array($filters['search']['status'])) {
        $query->whereIn('orders.status', $filters['search']['status']);
      } else {
        $query->where('orders.status', $filters['search']['status']);
      }
    }

    if (isset($filters['search']['not_status'])) {
      $query->where('orders.status', '<>', $filters['search']['not_status']);
    }

    return $this->getResult($query);

  }


  public function getUserOrders() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel()
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->join('payments', 'payments.order_id', '=', 'orders.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->leftJoin('stores', 'stores.id', '=', 'orders.actual_store_id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id')
      ->where('orders.status', '>=', OrderStatus::OPENED)->where('orders.user_id', getUserId());

    if (isset($filters['order_id']) && $filters['order_id'] != '') $query->where('orders.id', '=', $filters['order_id']);
    if (isset($filters['service_id']) && $filters['service_id'] != '') $query->where('service_details.service_id', '=', $filters['service_id']);
    if (isset($filters['payment_id']) && $filters['payment_id'] != '') $query->where('payment_odeo_payment_channel_informations.id', '=', $filters['payment_id']);
    if (isset($filters['tokenization_id']) && $filters['tokenization_id'] != '') 
      $query->leftJoin('payment_doku_payments', 'payment_doku_payments.id', '=', 'payments.reference_id')
        ->leftJoin('payment_doku_cc_payments', 'payment_doku_payments.reference_id', '=', 'payment_doku_cc_payments.id')
        ->where('payment_doku_cc_payments.tokenization_id', $filters['tokenization_id']); 
    if (isset($filters['status_code']) && $filters['status_code'] != '') $query->where('orders.status', '=', $filters['status_code']);
    if (isset($filters['start_date']) && $filters['start_date'] != '' && isset($filters['end_date']) && $filters['end_date'] != '') $query->whereBetween('orders.created_at', [date($filters['start_date'] . ' 00:00:00', time()), date($filters['end_date'] . ' 23:59:59', time())]);

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type'])
      ->select('orders.id as id', 'orders.rating', 'orders.confirm_receipt', 'order_details.id as order_detail_id',
        'stores.id as store_id', 'stores.name as store_name', 'stores.logo_no_odeo_path as store_logo',
        'orders.created_at as created_at', 'paid_at', 'expired_at', 'closed_at', 'total', 'subtotal', 'service_id',
        'service_details.id as service_detail_id', 'orders.status', 'payments.opc as payment_opc',
        'payments.info_id as payment_info_id', 'user_id', 'orders.name as user_name', 'orders.email as user_email',
        'orders.phone_number', 'payment_odeo_payment_channel_informations.name as payment_name',
        'order_details.name as item_name', 'order_detail_pulsa_switchers.number as item_number',
        'order_detail_pulsa_switchers.serial_number as item_serial_number',
        'order_detail_pulsa_switchers.current_response_status_code as item_response_code',
        'order_details.sale_price as item_price',
        'refunded_at'));
  }

  public function getUserLastThreeOrders() {

    return $this->model
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->where('orders.status', '=', OrderStatus::COMPLETED)->where('orders.user_id', getUserId())
      ->whereIn('service_details.service_id', Service::PPOB)->orderBy('orders.created_at', 'DESC')
      ->select(
        'paid_at',
        'subtotal',
        'order_details.name as item_name'
      )
      ->take(3)->get();
  }

  public function getSixMonthOrderTotal() {
    return $this->model
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->where('orders.status', '=', OrderStatus::COMPLETED)
      ->where('orders.created_at', '>', Carbon::now()->subMonth(6))
      ->whereIn('service_details.service_id', Service::PPOB)
      ->sum('total');
  }

  public function findUserFirstOrder() {
    return $this->model
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->where('orders.status', '=', OrderStatus::COMPLETED)
      ->whereIn('service_details.service_id', Service::PPOB)
      ->orderBy('orders.paid_at', 'asc')
      ->first();
  }

  public function getUserPurchaseReport() {
    $filters = $this->getFilters();

    $query = $this->model->select('orders.id as id', 'order_details.id as order_detail_id', 'stores.name as store_name', 'stores.id as store_id',
      'orders.created_at as created_at', 'paid_at', 'expired_at', 'closed_at', 'total', 'subtotal', 'service_id',
      'service_details.id as service_detail_id', 'orders.status', 'payments.opc as payment_opc',
      'payments.info_id as payment_info_id', 'user_id', 'orders.name as user_name', 'orders.email as user_email',
      'orders.phone_number', 'payment_odeo_payment_channel_informations.name as payment_name',
      'order_details.name as item_name', 'order_detail_pulsa_switchers.number as item_number',
      'order_detail_pulsa_switchers.serial_number as item_serial_number',
      'order_details.sale_price as item_price')
      ->from('orders')
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->join('service_details', 'service_details.id', '=', 'order_details.service_detail_id')
      ->join('payments', 'payments.order_id', '=', 'orders.id')
      ->join('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'payments.info_id')
      ->leftJoin('stores', 'stores.id', '=', 'orders.actual_store_id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id')
      ->where('orders.status', '>=', OrderStatus::OPENED)
      ->where('orders.user_id', $filters['user_id']);

    if (isset($filters['start_date']) && $filters['start_date'] != '') {
      $query->whereDate('orders.created_at', '>=', $filters['start_date']);
    }

    if (isset($filters['end_date']) && $filters['end_date'] != '') {
      $query->whereDate('orders.created_at', '<=', $filters['end_date']);
    }

    if (isset($filters['order_id']) && $filters['order_id'] != '') {
      $query->where('orders.id', '=', $filters['order_id']);
    }

    if (isset($filters['service_id']) && $filters['service_id'] != '') {
      $query->where('service_details.service_id', '=', $filters['service_id']);
    }

    if (isset($filters['payment_id']) && $filters['payment_id'] != '') {
      $query->where('payment_odeo_payment_channel_informations.id', '=', $filters['payment_id']);
    }

    if (isset($filters['status_code']) && $filters['status_code'] != '') {
      $query->where('orders.status', '=', $filters['status_code']);
    }

    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query->toSql());
    $result->execute($query->getBindings());

    return $result;
  }

  public function findByUserId($userId, $take = 20, $skip = 0) {
    if (($take != 0 && intval($take) == 0) || ($skip != 0 && intval($skip) == 0)) return [];
    else if ($take < 0 || $take > 20) $take = 20;
    return $this->model->where('user_id', $userId)->take($take)->skip($skip)->orderBy('id', 'desc')->get();
  }

  public function getPaginatedFinancialOrders() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if (isset($filters['search'])) {

      if (isset($filters['search']['start_date'])) {
        $query = $query->where('closed_at', '>=', $filters['search']['start_date']);
      }
      if (isset($filters['search']['end_date'])) {
        $query = $query->where('closed_at', '<=', $filters['search']['end_date']);
      }

      if (isset($filters['search']['order_id'])) {
        $query = $query->where('order_id', $filters['search']['order_id']);
      }

      if (isset($filters['search']['service_id'])) {
        $service_id = $filters['search']['service_id'];
        $query = $query->whereHas('details.serviceDetail', function ($query) use ($service_id) {
          $query->where('service_details.service_id', $service_id);
        });
      }
      if (isset($filters['search']['payment_method'])) {
        $id = $filters['search']['payment_method'];
        $query = $query->whereHas('payment', function ($query) use ($id) {
          $query->where('payments.info_id', $id);
        });
      }
    }

    $query = $query
      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
      ->whereIn('orders.status', [
        OrderStatus::COMPLETED,
        OrderStatus::MANUAL_FULFILLED
      ])
      ->with(['payment', 'user', 'sellerStore'])
      ->whereHas('payment', function ($q) {
        $q->where('info_id', '!=', Payment::OPC_GROUP_POST_PAID);
      })
      ->orderBy('orders.id', 'desc');

    if (!isset($filters['option']['ocash'])) {
      $query = $query->where('order_details.service_detail_id', '!=', ServiceDetail::TOPUP_ODEO);
    }
    if (!isset($filters['option']['odeposit'])) {
      $query = $query->where('order_details.service_detail_id', '!=', ServiceDetail::DEPOSIT_ODEO);
    }

    return $this->getResult($query);
  }


  public function getAllFinancialOrders() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    $query = $query->select(
      \DB::raw("orders.paid_at::date as date"),
      \DB::raw("orders.paid_at::time as time"),
      "orders.id as order_id",
      "orders.gateway_id",
      "order_details.name as product_name",
      "services.id as service_id",
      "services.name as service_name",
      "stores.subdomain_name as subdomain_name",
      "stores.domain_name as domain_name",
      "order_detail_pulsa_switchers.number as number_from_order_detail_pulsa_switcher",
      "users.telephone as user_phone_number",
      "users.id as user_id",
      "orders.actual_store_id as seller_store_id",
      "payment_odeo_payment_channel_informations.actual_name as payment_method",
      "payment_prismalink_va_direct_payments.reference_number as payment_reference",
      "order_detail_pulsa_switchers.current_base_price as vendor_price",
      "order_details.base_price as merchant_cost",
      "order_details.sale_price as price",
      "order_details.sale_price as net_sale",
      "orders.total as total",
      "orders.status as order_status"
    )->from('orders')
      ->join('order_details', 'orders.id', '=', 'order_details.order_id')
      ->join('users', 'orders.user_id', '=', 'users.id')
      ->join('payments', 'orders.id', '=', 'payments.order_id')
      ->join('payment_odeo_payment_channel_informations', 'info_id', '=', 'payment_odeo_payment_channel_informations.id')
      ->join('service_details', 'order_details.service_detail_id', '=', 'service_details.id')
      ->join('services', 'service_details.service_id', '=', 'services.id')
      ->leftJoin('order_detail_pulsa_switchers', 'order_detail_pulsa_switchers.order_detail_id', '=', 'order_details.id')
      ->leftJoin('payment_prismalink_va_direct_payments', function ($join) {
        $join->on('payment_prismalink_va_direct_payments.id', '=', 'payments.reference_id')
             ->where('payments.info_id', '=', 31)
             ->where('payments.id', '>', 4741770);
      })
      ->leftJoin('order_detail_plans', 'order_details.id', '=', 'order_detail_plans.order_detail_id')
      ->leftJoin('stores', function ($join) {
        $join->on('orders.seller_store_id', '=', 'stores.id');
        $join->orOn('order_detail_plans.store_id', '=', 'stores.id');
      })
//      ->leftJoin('order_charges', 'orders.id', '=', 'order_charges.order_id')
      ->whereIn('orders.status', [
        OrderStatus::WAITING_FOR_UPDATE,
        OrderStatus::WAITING_SUSPECT,
        OrderStatus::COMPLETED,
        OrderStatus::REFUNDED,
        OrderStatus::REFUNDED_AFTER_SETTLEMENT,
        OrderStatus::TEMPORARY_FAILED,
        OrderStatus::MANUAL_FULFILLED
      ]);
//      ->where(function ($query) {
//        $query->whereNull('order_charges.group_type')
//          ->orWhere('order_c2harges.group_type', '=', 'charge_to_customer');
//      });

    if (isset($filters['search'])) {
      if (isset($filters['search']['start_date'])) {
        $query = $query->whereDate('orders.paid_at', '>=', $filters['search']['start_date']);
      }
      if (isset($filters['search']['end_date'])) {
        $query = $query->whereDate('orders.paid_at', '<=', $filters['search']['end_date']);
      }
    }

    $query = $query->orderBy('orders.id', 'asc');

    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query->toSql());
    $result->execute($query->getBindings());

    return $result;

  }

  public function findTopupOrder($topupIds) {
    $model = app()->make('Odeo\Domains\Transaction\Model\OrderDetailTopup');
    $query = $model->join('order_details', 'order_details.id', '=', 'order_detail_topups.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id');

    if (is_array($topupIds)) $query = $query->whereIn("topup_id", $topupIds);
    else $query = $query->where("topup_id", $topupIds);

    return $query->get();
  }

  public function findStoreDepositOrder($storeDepositIds) {
    $model = app()->make(\Odeo\Domains\Transaction\Model\OrderDetailStoreDeposit::class);
    $query = $model->join('order_details', 'order_details.id', '=', 'order_detail_store_deposits.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id');

    if (is_array($storeDepositIds)) $query = $query->whereIn("store_deposit_id", $storeDepositIds);
    else $query = $query->where("store_deposit_id", $storeDepositIds);

    return $query->get();
  }

  public function findStoreOrder($storeIds) {
    $model = app()->make('Odeo\Domains\Subscription\Model\OrderDetailPlan');
    $query = $model->join('order_details', 'order_details.id', '=', 'order_detail_plans.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->where('status', '<>', OrderStatus::CANCELLED);

    if (is_array($storeIds)) $query = $query->whereIn("store_id", $storeIds);
    else $query = $query->where("store_id", $storeIds);

    return $query->get();
  }

  public function findWarrantyOrder($warrantyIds) {
    $model = app()->make('Odeo\Domains\Subscription\Model\OrderDetailWarranty');
    $query = $model->join('order_details', 'order_details.id', '=', 'order_detail_warranties.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id');

    if (is_array($warrantyIds)) $query = $query->whereIn("warranty_id", $warrantyIds);
    else $query = $query->where("warranty_id", $warrantyIds);

    return $query->get();
  }

  public function findMdPlusOrder($storeIds) {
    $model = app()->make('Odeo\Domains\Subscription\MdPlus\Model\OrderDetailMdPlus');
    $query = $model->join('order_details', 'order_details.id', '=', 'order_detail_md_plus.order_detail_id')
      ->join('orders', 'orders.id', '=', 'order_details.order_id');

    if (is_array($storeIds)) $query = $query->whereIn("store_id", $storeIds);
    else $query = $query->where("store_id", $storeIds);

    return $query->get();
  }


  public function getExpiredActiveOrder() {
    return $this->model
      ->where('expired_at', '<=', Carbon::now()->toDateTimeString())
      ->where('status', '<=', OrderStatus::FAKE_CONFIRMED)
      ->take(5)
      ->get();
  }

  public function getUnconfirmedSuccessOrder() {
    return $this->model
      ->where('confirm_receipt', '=', false)
      ->where('settlement_at', '<=', Carbon::now()->toDateTimeString())
      ->where('status', '=', OrderStatus::COMPLETED)
      ->get();
  }

  public function getOrderActivity() {
    return $this->runCache('activity', 'activity.order_activity.', 10, function () {
      return [
        'completed' => $this->model
          ->whereIn('status', [
            OrderStatus::COMPLETED,
            OrderStatus::MANUAL_FULFILLED
          ])->count(),
        'partial_fulfilled' => $this->model->where('status', '=', OrderStatus::PARTIAL_FULFILLED)->count(),
        'processing_order' => $this->model->where('status', '=', OrderStatus::VERIFIED)->count(),
        'confirmed' => $this->model->where('status', '=', OrderStatus::CONFIRMED)->count(),
      ];
    });
  }

  public function getFailedOrderAlert() {
    return $this->model
      ->whereIn('status', [
        OrderStatus::PARTIAL_FULFILLED,
        OrderStatus::VERIFIED
      ])
      ->where('paid_at', '<', Carbon::now()->subMinutes(15))
      ->get();
  }

  public function getConfirmedAlert() {
    return $this->model
      ->where(function ($query) {
        $query
          ->where('status', '=', OrderStatus::CONFIRMED);
      })
      ->where('paid_at', '<', Carbon::now()->subMinutes(15))
      ->get();
  }

  public function getOrderValue($day, $platforms = null) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    $query = $this->getCloneModel()->join('users', 'users.id', '=', 'orders.user_id')
      ->whereNotNull('actual_store_id')
      ->where('orders.status', OrderStatus::COMPLETED)
      ->where(\DB::raw('date(closed_at)'), $date)
      ->select('user_id', 'users.name', 'users.telephone', 'users.email',
        \DB::raw('sum(total) as order_total'), \DB::raw('count(orders.id) as order_counts'))
      ->groupBy('user_id', 'users.name', 'users.telephone', 'users.email')
      ->orderBy(\DB::raw('count(orders.id)'), 'desc');

    if (isset($platforms)) {
      $query = $query->whereIn('platform_id', $platforms);
    }

    return $query->get();
  }

  public function getNotCompletedOrderStatusCount($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where('status', '<>', OrderStatus::COMPLETED)
      ->where(function ($query) use ($date) {
        $query->where(\DB::raw('date(created_at)'), $date)
          ->orWhere(\DB::raw('date(opened_at)'), $date)
          ->orWhere(\DB::raw('date(paid_at)'), $date);
      })
      ->select('status', \DB::raw('count(status) as status_count'))
      ->orderBy('status', 'asc')
      ->groupBy('status')
      ->get();
  }

  public function countUserTransactionBetweenDate($userId, $start, $end) {
    return $this->model
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->whereIn('service_detail_id', ServiceDetail::groupPpob())
      ->where('user_id', $userId)
      ->whereDate('created_at', '>=', $start)
      ->whereDate('created_at', '<=', $end)
      ->where('status', OrderStatus::COMPLETED)
      ->count();
  }

  public function countUserTransaction($userId) {
    return $this->model
      ->join('order_details', 'order_details.order_id', '=', 'orders.id')
      ->whereIn('service_detail_id', ServiceDetail::groupPpob())
      ->where('user_id', $userId)
      ->where('status', OrderStatus::COMPLETED)
      ->count();
  }

  public function listCancelledByTotalAndDate($total, $date) {
    return $this->model
      ->where('total', $total)
      ->whereDate('created_at', $date)
      ->where('status', OrderStatus::CANCELLED)
      ->orderBy('id')
      ->get();
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:23 PM
 */

namespace Odeo\Domains\Order\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Model\Cart;

class CartRepository extends Repository {

  public function __construct(Cart $cart) {
    $this->model = $cart;
  }


  public function getUserCart($user_id) {
    return $this->model->where('user_id', $user_id)->first();
  }

  public function getUserCartByPhone($phone) {
    return $this->model->join('users', 'users.id', '=', 'user_carts.user_id')
      ->where('users.telephone', '=', purifyTelephone($phone))
      ->first();
  }

}
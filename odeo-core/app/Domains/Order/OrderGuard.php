<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/21/17
 * Time: 7:20 PM
 */

namespace Odeo\Domains\Order;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Repository\OrderRepository;

class OrderGuard {

  private $orderRepo;

  public function __construct() {
    $this->orderRepo = app()->make(OrderRepository::class);
  }

  public function validateStatus(PipelineListener $listener, $data) {
    $status = is_array($data['status']) ? $data['status'] : [$data['status']];
    $order = $this->orderRepo->findById($data['order_id']);

    if ($order && in_array($order->status, $status)) {
      return $listener->response(200);
    }

    return $listener->response(400);
  }

  public function validateUser(PipelineListener $listener, $data) {
    if (!isAdmin()) {
      $order = $this->orderRepo->findById($data['order_id']);
      if ($order->user_id != getUserId($data)) {
        return $listener->response(400, "You don't have access to this order.");
      }
    }
    return $listener->response(200);
  }

}

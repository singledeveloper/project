<?php

namespace Odeo\Domains\Order\Affiliate;

use Odeo\Domains\Affiliate\Jobs\AffiliateNotifier;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\QueueSelector;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\TempDepositRequester;
use Odeo\Domains\Core\Pipeline;

class OrderPurchaser {

  private $pulsaOdeo, $pulsaPrefix, $switcherOrder, $status, $inventoryManager, $affiliateOrder, $orderDetails, $priceGetter, $users;

  public function __construct() {
    $this->pulsaOdeo = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
    $this->status = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->inventoryManager = app()->make(\Odeo\Domains\Inventory\Helper\InventoryManager::class);
    $this->affiliateOrder = app()->make(\Odeo\Domains\Affiliate\Repository\AffiliateOrderRepository::class);
    $this->priceGetter = app()->make(\Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {

    $loop = 1;
    $firstData = $data;
    $pulsaOdeo = false;
    if (strpos($data['number'], '/') !== false) {
      list($data['number'], $trxId) = explode('/', $data['number']);
      if ($affiliateOrder = $this->affiliateOrder->findByTrxId($trxId, $data['auth']['user_id'])) {
        $orderDetail = $this->orderDetails->findByOrderId($affiliateOrder->order_id);
        $switcher = $orderDetail->switcher;
        $order = $orderDetail->order;
        return $listener->response(200, [
          'order_id' => $order->id,
          'name' => $orderDetail->name,
          'number' => revertTelephone($switcher->number),
          'serial_number' => $switcher->serial_number != null ? $switcher->serial_number : 'N/A',
          'status' => $this->status->parse($order)['status'],
          'price' => intval($order->total),
          'message' => $this->status->parseMessageForSameTransaction($orderDetail->switcher)
        ]);
      }
    }
    else if (strpos($data['number'], '#') !== false) {
      list($data['number'], $loop) = explode('#', $data['number']);
      $loop = intval($loop);
      if ($loop == 0) $loop = 1;
    }

    if (intval($data['denom'])) {
      $temp = substr($data['number'], 0, 3);
      if ($temp != '999') $temp = substr(purifyTelephone($data['number']), 0, 5);
      if ($pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => $temp])) {
        if ($pulsaOdeo = $this->pulsaOdeo->findByNominal($data['denom'] . '000', $pulsaPrefix->operator_id)) {
          $data['pulsa_odeo_id'] = $pulsaOdeo->id;
        }
      }
    }
    else if (isset($data['pulsa_odeo_id']))
      $pulsaOdeo = $this->pulsaOdeo->findById($data['pulsa_odeo_id']);
    else {
      $user = $this->users->findById($data['auth']['user_id']);
      if ($user->purchase_preferred_store_id) {
        $pulsaOdeo = $this->priceGetter->checkSupplyChainExistFromInventoryCode([
          'inventory_code' => strtoupper($data['denom']),
          'user_id' => $data['auth']['user_id'],
          'store_id' => $user->purchase_preferred_store_id
        ]);
      }
      if (!$pulsaOdeo) {
        $userStores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
        if ($suppStore = $userStores->findSupplyStore($data['auth']['user_id'])) {
          $pulsaOdeo = $this->pulsaOdeo->findSameCodeFromStore(strtoupper($data['denom']), $suppStore->id);
          if ($pulsaOdeo) {
            $data['store_id'] = $suppStore->id;
            $data['is_agent'] = true;
            $data['agent_base_price'] = $pulsaOdeo->price;
            $data['is_h2h'] = true;
          }
        }
      }
      if (!$pulsaOdeo) $pulsaOdeo = $this->pulsaOdeo->findByInventoryCode(strtoupper($data['denom']));
    }

    $listener->replaceData($data);

    if ($pulsaOdeo) {
      if (!isset($trxId)) {
        $same = $this->switcherOrder->getSamePurchaseCount($data['number'], $pulsaOdeo->id);
        if ($loop <= count($same)) return $listener->response(400, $this->status->parseMessageForSameTransaction($same[$loop - 1]));
      }

      $data = array_merge($data, $this->inventoryManager->getServiceDataFromCategory($pulsaOdeo->category));

      $affiliateOrder = $this->affiliateOrder->getNew();
      $affiliateOrder->user_id = $data['auth']['user_id'];
      $affiliateOrder->request_log = json_encode($firstData);
      $affiliateOrder->trx_id = isset($trxId) ? $trxId : null;

      if (!isset($data['item_id']))
        $data['item_id'] = $pulsaOdeo->id;

      $data['item_detail'] = [
        'number' => $data['number'],
        'operator' => $pulsaOdeo->category
      ];

      if ($pulsaOdeo->service_detail_id == ServiceDetail::PULSA_POSTPAID_ODEO)
        $data['item_detail']['postpaid_type'] = PostpaidType::getConstKeyByValue($pulsaOdeo->category);

      $data['info_id'] = Payment::OPC_GROUP_OCASH;
      $data['gateway_id'] = Payment::AFFILIATE;
      $data['platform_id'] = Platform::AFFILIATE;
      $data['opc'] = 228;

      $pipeline = new Pipeline();

      if (!isset($data['store_id'])) {
        $usePreferredStoreId = isset($data['agent_base_price']) ||
          (!isset($data['agent_base_price']) && $pulsaOdeo->status != Pulsa::STATUS_OK_HIDDEN);
        $pipeline->add(new Task(QueueSelector::class, 'get', ['use_preferred_store' => $usePreferredStoreId]));
      }

      $pipeline->add(new Task(CartRemover::class, 'clear'));
      $pipeline->add(new Task(CartInserter::class, 'addToCart'));
      $pipeline->add(new Task(CartCheckouter::class, 'checkout'));
      $pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
      $pipeline->add(new Task(PaymentRequester::class, 'request'));
      $pipeline->add(new Task(PaymentOpenRequester::class, 'open', [
        'with_password' => true
      ]));

      $pipeline->enableTransaction();
      $pipeline->execute($data);

      if ($pipeline->fail()) return $listener->response(400, $pipeline->errorMessage);
      else $affiliateOrder->order_id = $pipeline->data['order_id'];

      $this->affiliateOrder->save($affiliateOrder);

      $currencyHelper = app()->make(Currency::class);
      return $listener->response(200, [
        'rc' => Supplier::RC_PENDING,
        'order_id' => $pipeline->data['order_id'],
        'price' => $pipeline->data['sale_price'],
        'message' => 'Trx ke ' . revertTelephone($data['number']) . ' sedang diproses. Harga ' .
          $currencyHelper->formatPrice($pipeline->data['sale_price'])['formatted_amount'] . ' REFF#' . $pipeline->data['order_id']
      ]);
    }

    return $listener->response(400, 'Kode ' . $data['denom'] . ' tidak valid');
  }

  public function checkOrder(PipelineListener $listener, $data) {
    if ($switcher = $this->switcherOrder->findByOrderId($data['order_id'])) {
      $orderDetail = $switcher->orderDetail;
      $order = $orderDetail->order;
      if ($order->user_id != $data['auth']['user_id']) return $listener->response(400, 'Unauthorized.');

      $status = $this->status->parse($order)['status'];
      if ($status == 'PROCESSING ORDER') $status = 'PENDING';

      if (isset($data['notify'])) {
        $orderStatus = app()->make(\Odeo\Domains\Order\Helper\StatusParser::class);
        $listener->pushQueue(new AffiliateNotifier([
          'order_id' => $order->id,
          'user_id' => $order->user_id,
          'status' => $orderStatus->parse($order)['status'],
          'message' => $switcher->h2h_message,
          'response_code' => $switcher->current_response_status_code
        ]));
      }

      return $listener->response(200, [
        'order_id' => $order->id,
        'name' => $orderDetail->name,
        'msisdn' => revertTelephone($switcher->number),
        'serial_number' => $switcher->serial_number != null ? $switcher->serial_number : 'N/A',
        'status' => $status,
        'price' => intval($order->total)
      ]);
    }
    return $listener->response(204);
  }
}
<?php

namespace Odeo\Domains\Order\Reconciliation;

use DB;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\OrderReconciliation;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Reconciliation\Jobs\ReconcileOrder;
use Odeo\Domains\Order\Reconciliation\Jobs\UpdateReconcileData;

class OrderReconciler {

  public function __construct() {
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderRecon = app()->make(\Odeo\Domains\Order\Reconciliation\Repository\OrderReconciliationRepository::class);
    $this->cashTransaction = app()->make(\Odeo\Domains\Transaction\Repository\CashTransactionRepository::class);
  }

  public function updateRecon(PipelineListener $listener, $data) {

    DB::beginTransaction();
    try {
      $prevId = 0;
      $orderRecon = null;
      foreach ($data['recons'] as $row) {
        if ($prevId != $row['order_id']) {
          if ($orderRecon) {
            $orderRecon->is_reconciled = false;
            $orderRecon->payment_name = NULL;
            $orderRecon->total_diff = 0;
            $orderRecon->diff_reason = NULL;
            $this->orderRecon->save($orderRecon);
          }

          $orderRecon = $this->orderRecon->getCloneModel()->lockForUpdate()->where('order_id', $row['order_id'])->first();
          $prevId = $row['order_id'];
        }

        if (!$orderRecon) {
          $orderRecon = $this->orderRecon->getNew();
          $orderRecon->order_id = $row['order_id'];
          $orderRecon->transaction_date = $row['transaction_date'];
          $orderRecon->trx_month = \Carbon\Carbon::parse($row['transaction_date'])->format('n');
          $orderRecon->trx_year = \Carbon\Carbon::parse($row['transaction_date'])->format('Y');
        }

        switch ($row['cash_type']) {
          case CashType::OCASH :
            if ($row['is_other']) {
              $orderRecon->total_cash_other += $row['amount'];
            } else {
              $orderRecon->total_cash += $row['amount'];
            }
            break;
          case CashType::ODEPOSIT :
            $orderRecon->total_deposit += $row['amount'];
            break;
          case CashType::ORUSH :
          case CashType::OCASHBACK :
          case CashType::OADS :
            $orderRecon->total_marketing += $row['amount'];
            break;
        }

        $orderRecon->total_mutation += $row['amount'];
        $orderRecon->trx_count++;
      }

      $orderRecon->is_reconciled = false;
      $orderRecon->payment_name = NULL;
      $orderRecon->total_diff = 0;
      $orderRecon->diff_reason = NULL;

      $this->orderRecon->save($orderRecon);
    } catch (\Illuminate\Database\QueryException $e) {
      DB::rollback();
      clog('order_recon', $e->getMessage());
      $listener->pushQueue(new UpdateReconcileData($data['recons']));
    } catch (\Exception $e) {
      DB::rollback();
      throw $e;
    }
    DB::commit();

    return $listener->response(200);
  }

  public function reconcileOrder(PipelineListener $listener, $data) {

    $orderRecons = $this->orderRecon->getCloneModel()->where('is_reconciled', false)->orderBy('id', 'asc');

    if (isset($data['trx_month'])) $orderRecons = $orderRecons->where('trx_month', $data['trx_month']);
    if (isset($data['trx_year'])) $orderRecons = $orderRecons->where('trx_year', $data['trx_year']);
    if (isset($data['new_data_only'])) {
      if ($data['new_data_only']) $orderRecons = $orderRecons->whereNull('payment_name');
      else $orderRecons = $orderRecons->whereNotNull('payment_name');
    }
    if (isset($data['limit'])) $orderRecons = $orderRecons->limit($data['limit']);
    if (isset($data['begin_from_id'])) $orderRecons = $orderRecons->where('id', '>=', $data['begin_from_id']);

    $orderRecons = $orderRecons->get();

    if (!isset($data['new_data_only']) || $data['new_data_only']) {
      $orderRecons->load(
        'order.details.switcher.currentInventory.vendorSwitcher',
        'order.details.orderDetailPlans',
        'order.charges',
        'order.payment.information'
      );
    } else {
      $orderRecons->load('order');
    }

    $updatedRecon = array();

    foreach ($orderRecons as $orderRecon) {
      $order = $orderRecon->order;

      $orderRecon->order_status = $order->status;

      if (!isset($orderRecon->payment_name)) {
        $orderRecon->payment_name = $order->payment ? $order->payment->information->actual_name : '';

        if ($order->payment) {

          $inquiry = Payment::getBankInquiryRepository($order->payment->info_id);

          if ($inquiry) {
            $bankInquiries = $inquiry->findAllByAttributes([
              'reference_type' => 'order_id',
              'reference' => '["' . $order->id . '"]'
            ]);

            if ((!$bankInquiries || $bankInquiries->isEmpty()) && $order->payment->info_id == Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI) {
              $inquiry = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository::class);
              $bankInquiries = $inquiry->findAllByAttributes([
                'reference_type' => 'order_id',
                'reference' => '["' . $order->id . '"]'
              ]);
            }

            if (!$bankInquiries || $bankInquiries->isEmpty()) {
              $orderRecon->payment_amount = 0;
            } else {
              $paymentAmount = 0;
              foreach ($bankInquiries as $bankInquiry) {
                $paymentAmount += $bankInquiry->credit;
              }
              $orderRecon->payment_amount = $paymentAmount;
            }
          } else {
            $orderRecon->payment_amount = $order->subtotal;
          }
          $orderRecon->payment_info_id = $order->payment->info_id;
        } else {
          $orderRecon->payment_amount = 0;
          $orderRecon->payment_info_id = 0;
        }

        $inventoryDetails = array();
        $orderRecon->merchant_cost = 0;

        foreach ($order->details as $orderDetail) {
          $orderRecon->merchant_cost += $orderDetail->base_price;
          if ($orderDetail->switcher) {
            $vendorStoreId = $orderDetail->switcher->currentInventory->vendorSwitcher->owner_store_id;

            $isMarketplace = isset($vendorStoreId) && 
              isset($orderDetail->switcher->currentInventory->supply_pulsa_id) && 
              $orderDetail->service_detail_id != ServiceDetail::TRANSPORTATION_ODEO;
              
            if (isset($vendorStoreId)) {
              if (!$isMarketplace) { // supply
                
                $orderRecon->merchant_cost = 0;

                if ($orderDetail->switcher->first_inventory_id != $orderDetail->switcher->current_inventory_id) {
                  $orderRecon->merchant_cost = $orderDetail->switcher->current_base_price - $orderDetail->switcher->first_base_price;
                }

                $orderRecon->merchant_cost += $orderDetail->switcher->subsidy_amount;
              } else { // marketplace from supply
                $orderRecon->merchant_cost = $orderDetail->switcher->current_base_price - $orderDetail->base_price;
              }
            }

            $inventoryDetails[] = [
              'vendor_switcher_id' => $orderDetail->switcher->currentInventory->vendor_switcher_id,
              'vendor_price' => $orderDetail->switcher->current_base_price,
              'pulsa_odeo_id' => $orderDetail->switcher->currentInventory->pulsa_odeo_id,
              'commission' => $orderDetail->base_price - $orderDetail->switcher->current_base_price
            ];
          }

          $orderRecon->is_marketplace = $isMarketplace ?? false;

          if ($orderDetail->orderDetailPlans) {
            $inventoryDetails[] = [
              'plan_id' => $orderDetail->orderDetailPlans->plan_id
            ];
          }
        }

        $orderRecon->inventory_detail = json_encode($inventoryDetails);
        $orderRecon->service_id = ServiceDetail::getServiceid($order->details[0]->service_detail_id);
        $orderRecon->platform_id = $order->platform_id;

        $orderCharges = array();

        foreach ($order->charges as $charge) {
          $orderCharges[] = [
            'amount' => $charge->amount,
            'type' => $charge->type,
            'group_type' => $charge->group_type
          ];
        }

        $orderRecon->order_charges = json_encode($orderCharges);
      }

      $orderCharges = json_decode($orderRecon->order_charges, true);

      $total = $orderRecon->total_cash_other + $orderRecon->total_cash + $orderRecon->total_deposit + $orderRecon->total_marketing;
      $orderRecon->total_mutation = sprintf("%.2f", $total);

      $serviceId = $orderRecon->service_id;
      $isReconciled = true;
      $totalDiff = 0;
      $diffReason = NULL;
      if ($orderRecon->payment_amount <= 3000) {
        $acceptableDiff = $orderRecon->payment_amount * 0.1;
      } else {
        $acceptableDiff = $orderRecon->payment_amount * 0.03;
      }

      if (OrderStatus::isRefunded($orderRecon->order_status) || $orderRecon->order_status == OrderStatus::CANCELLED) {
        $isReconciled = $total == 0;
        $totalDiff = $total;

        if (!in_array($orderRecon->payment_info_id, [Payment::OPC_GROUP_OCASH, Payment::OPC_GROUP_CASH_ON_DELIVERY])) {
          $totalDiff -= $orderRecon->payment_amount;
        }

        list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff, NULL, OrderReconciliation::COMPENSATION_FOR_CUSTOMERS);
      } else {
        $opc = $orderRecon->payment_info_id;

        if (in_array($serviceId, Service::PPOB)) {
          if ($orderCharges) {
            $referralBonus = array_reduce($orderCharges, function($total, $item) {
              return $item['type'] == OrderCharge::REFERRAL_CASHBACK && $item['group_type'] == OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY ? 
                $total + $item['amount'] : $total;
            }, 0);
          } else {
            $referralBonus = 0;
          }
        } else {
          $referralBonus = 0;
        }

        switch ($opc) {
          case Payment::OPC_GROUP_OCASH:
            if (in_array($serviceId, Service::PPOB)) {
              if ($orderRecon->is_marketplace && $orderRecon->merchant_cost > 0) {
                $totalDiff = $total - $orderRecon->merchant_cost - $referralBonus;
              } else {
                $totalDiff = abs($orderRecon->merchant_cost) + $total - $referralBonus;
              }

              list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff, NULL, NULL, NULL, OrderReconciliation::SYSTEM_FAULT);
            } else if ($serviceId == Service::ODEPOSIT) {
              $isReconciled = $total == 0;
              $totalDiff = $total;
            } else if ($serviceId == Service::PLAN) {
              $isReconciled = abs($total) == $orderRecon->merchant_cost;
              $totalDiff = abs($total) - $orderRecon->merchant_cost;
            } else if ($serviceId == Service::USER_INVOICE) {
              $isReconciled = $total == 0;
              $totalDiff = $total;
            } else if ($serviceId == Service::DDLENDING_LOAN) {
              if ($orderCharges) {
                $billerFee = array_reduce($orderCharges, function($total, $item) {
                  return $item['type'] == OrderCharge::API_FEE ? 
                    $total + $item['amount'] : $total;
                }, 0);
              } else {
                $billerFee = 0;
              }

              $totalDiff = $total + $orderRecon->merchant_cost + $billerFee;
              list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff);
            }
            break;
          case Payment::OPC_GROUP_CASH_ON_DELIVERY:
            $isReconciled = true;
            $totalDiff = $orderRecon->total_cash + $orderRecon->total_marketing;
            if ($totalDiff > $orderRecon->merchant_cost) {
              $totalDiff = $totalDiff + $orderRecon->total_deposit + $orderRecon->merchant_cost;
            }

            list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff);
            break;
          default:
            if ($serviceId == Service::OCASH || $serviceId == Service::ODEPOSIT) {
              $isReconciled = $total == $orderRecon->payment_amount;
              $totalDiff = $total - $orderRecon->payment_amount;

              if (Payment::getBankId($opc)) {
                list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, 1000, OrderReconciliation::UNIQUE_CODE, OrderReconciliation::OTHER_EXPENSE);
              }
            } else if (in_array($serviceId, Service::PPOB)) {
              if ($orderRecon->merchant_cost < 0) {
                $totalDiff = $total - ($orderRecon->payment_amount + $orderRecon->merchant_cost) - $referralBonus;
              } else {
                $totalDiff = $total - ($orderRecon->payment_amount - $orderRecon->merchant_cost) - $referralBonus;
              }

              list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff);
            } else if ($serviceId == Service::CREDIT_BILL) {
              $isReconciled = $total == $orderRecon->payment_amount - 250;
              $totalDiff = $total - $orderRecon->payment_amount + (1500 - $orderRecon->total_cash_other);

              list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff);
            } else if ($serviceId == Service::USER_INVOICE || $serviceId == Service::DDLENDING_PAYMENT) {
              if ($orderCharges) {
                $billerFee = array_reduce($orderCharges, function($total, $item) {
                  return $item['type'] == OrderCharge::BILLER_FEE || $item['type'] == OrderCharge::PAYMENT_SERVICE_COST ? 
                    $total + $item['amount'] : $total;
                }, 0);
              } else {
                $billerFee = 0;
              }
              $totalDiff = $total - $orderRecon->payment_amount + $billerFee;

              list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff);
            } else {
              $totalDiff = $total - ($orderRecon->payment_amount - $orderRecon->merchant_cost);

              list($isReconciled, $diffReason) = $this->checkDiff($totalDiff, $acceptableDiff, NULL, NULL, NULL, OrderReconciliation::SYSTEM_FAULT);
            }
            break;
        }
      }

      if ($order->status < OrderStatus::COMPLETED) $isReconciled = false;

      $orderRecon->is_reconciled = $isReconciled;
      $orderRecon->total_diff = sprintf("%.2f", $totalDiff);
      $orderRecon->diff_reason = $diffReason;

      if ($orderRecon->isDirty()) {
        $attributes = $orderRecon->getAttributes();
        $this->sanitize($attributes);

        $updatedRecon[] = $attributes;
      }
    }
    count($updatedRecon) && $this->orderRecon->updateBulk($updatedRecon);

    return $listener->response(200);
  }

  public function manualRecon(PipelineListener $listener, $data) {
    $orderRecon = $this->orderRecon->findByAttributes('order_id', $data['order_id']);

    $orderRecon->diff_reason = $data['diff_reason'];
    $orderRecon->is_reconciled = true;

    $this->orderRecon->save($orderRecon);

    return $listener->response(200);
  }

  public function recalculateData(PipelineListener $listener, $data) {
    $this->orderRecon->updateData($data['order_id']);

    return $listener->response(200);
  }

  public function dispatchRecon(PipelineListener $listener, $data) {
    $where = 'is_reconciled = ?';
    $bindings = [0];

    if (isset($data['trx_month'])) {
      $where .= ' and trx_month = ?';
      $bindings[] = $data['trx_month'];
    }
    if (isset($data['trx_year'])) {
      $where .= ' and trx_year = ?';
      $bindings[] = $data['trx_year'];
    }
    if (isset($data['new_data_only'])) {
      if ($data['new_data_only']) $where .= ' and payment_name is null';
      else $where .= ' and payment_name is not null';
    }

    $query = 'select id from (select id, row_number() over (order by id asc) as sequence
      from order_reconciliations where ' . $where . ' order by id asc) temp where temp.sequence % 100 = 1';

    $db = \DB::connection()->getPdo();

    $result = $db->prepare($query);
    $result->execute($bindings);

    $limit = 100;

    while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
      $listener->pushQueue(new ReconcileOrder([
        'begin_from_id' => $row['id'],
        'limit' => $limit,
        'new_data_only' => $data['new_data_only'] ?? false,
        'trx_month' => $data['trx_month'] ?? null,
        'trx_year' => $data['trx_year'] ?? null
      ]));
    }

    return $listener->response(200);
  }

  private function sanitize(&$arr) {
    foreach ($arr as $key => $value) {
      if (!isset($arr[$key])) {
        $arr[$key] = null;
      }
      if ($key == 'settlement_at') {
        unset($arr[$key]);
      }
    }
  }

  private function checkDiff(
    $diffAmount,
    $threshold,
    $reasonNegative = NULL,
    $reasonPositive = NULL,
    $reasonNegativeOverThreshold = NULL,
    $reasonPositiveOverThreshold = NULL
  ) {
    if ($diffAmount < 0) {
      if (abs($diffAmount) < $threshold) {
        return [true, $reasonNegative ?? OrderReconciliation::OTHER_INCOME];
      } else {
        return [isset($reasonNegativeOverThreshold), $reasonNegativeOverThreshold];
      }
    } else if ($diffAmount > 0) {
      if ($diffAmount < $threshold) {
        return [true, $reasonPositive ?? OrderReconciliation::RUSH_BONUS];
      } else {
        return [isset($reasonPositiveOverThreshold), $reasonPositiveOverThreshold];
      }
    } else {
      return [true, NULL];
    }
  }
}

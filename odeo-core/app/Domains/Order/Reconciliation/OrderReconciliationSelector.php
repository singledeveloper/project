<?php

namespace Odeo\Domains\Order\Reconciliation;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Carbon\Carbon;

class OrderReconciliationSelector implements SelectorListener {

  private $orderRecons;

  public function __construct() {

    $this->orderRecons = app()->make(\Odeo\Domains\Order\Reconciliation\Repository\OrderReconciliationRepository::class);
    $this->orderReconSummaries = app()->make(\Odeo\Domains\Order\Reconciliation\Repository\OrderReconciliationSummaryRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

  }

  public function _extends($data, Repository $repository) {
    return $data;
  }


  public function _transforms($orderRecon, Repository $repository) {

    $output = [];

    $output['transaction_date'] = $orderRecon->transaction_date;
    $output['order_id'] = $orderRecon->order_id;
    $output['order_status'] = ucwords(str_replace('_', ' ', OrderStatus::getConstKeyByValue($orderRecon->order_status)));
    $output['service_name'] = $orderRecon->service->displayed_name;
    $output['payment_name'] = $orderRecon->payment_name;
    $output['payment_amount'] = $this->currency->formatPrice($orderRecon->payment_amount);
    $output['settlement_amount'] = $this->currency->formatPrice($orderRecon->settlement_amount);
    $output['settlement_at'] = $orderRecon->settlement_at;
    $output['merchant_cost'] = $this->currency->formatPrice($orderRecon->merchant_cost);
    $output['total_cash'] = $this->currency->formatPrice($orderRecon->total_cash);
    $output['total_cash_other'] = $this->currency->formatPrice($orderRecon->total_cash_other);
    $output['total_deposit'] = $this->currency->formatPrice($orderRecon->total_deposit);
    $output['total_marketing'] = $this->currency->formatPrice($orderRecon->total_marketing);
    $output['total_diff'] = $this->currency->formatPrice($orderRecon->total_diff);
    $output['diff_reason'] = $orderRecon->diff_reason;
    $output['is_reconciled'] = $orderRecon->is_reconciled;

    return $output;
  }

  public function get(PipelineListener $listener, $data) {

    $this->orderRecons->normalizeFilters($data);
    $this->orderRecons->setSimplePaginate(true);

    $orderRecons = [];

    foreach ($this->orderRecons->gets() as $orderRecon) {
      $orderRecons[] = $this->_transforms($orderRecon, $this->orderRecons);
    }

    if (sizeof($orderRecons) > 0)
      return $listener->response(200, array_merge(
        ["order_recons" => $this->_extends($orderRecons, $this->orderRecons)],
        $this->orderRecons->getPagination()
      ));
    return $listener->response(204, ["orders" => []]);

  }

  public function getSummary(PipelineListener $listener, $data) {
    $diffReasonMap = [
      'other_income' => 'Other Income',
      'unique_code' => 'Unique Code',
      'other_expense' => 'Other Expense',
      'compensation_for_customers' => 'Compensation for Customers',
      'promotion_expense' => 'Promotion Expense',
      'rush_bonus' => 'Rush Bonus',
      'system_fault' => 'System Fault',
    ];

    $dateStart = Carbon::today()->subDays(6);
    $dateEnd = Carbon::today();

    $summaries = $this->orderReconSummaries->getByDate($dateStart->toDateString(), $dateEnd->toDateString());
    
    $reconSummaries = [];
    while ($dateStart->lte($dateEnd)) {
      $date = $dateStart->toDateString();
      foreach ($diffReasonMap as $key => $value) {
        $reconSummaries[$date][$value] = $this->currency->getFormattedOnly(0);
      }
      $dateStart->addDay();
    }
    foreach ($summaries as $summary) {
      $reconSummaries[$summary['date']][$diffReasonMap[$summary['diff_reason']]] = 
        $this->currency->getFormattedOnly(-$summary['total_diff']);
    }
    return $listener->response(200, ['recon_summary' => $reconSummaries]);
  }

}

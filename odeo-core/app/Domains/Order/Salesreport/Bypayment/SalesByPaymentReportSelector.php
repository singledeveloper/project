<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/2/17
 * Time: 1:18 PM
 */

namespace Odeo\Domains\Order\Salesreport\Bypayment;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;

class SalesByPaymentReportSelector {

  private $salesByPayments;

  public function __construct() {

    $this->salesByPayments = app()->make(\Odeo\Domains\Order\Salesreport\Bypayment\Repository\SalesByPaymentRepository::class);
  }

  public function _transforms($data, Repository $repository) {

    $output= [];

    $output['sales'] = $data->sales;
    $output['sales_target'] = $data->sales_target;
    $output['info_id'] = $data->info_id;

    return $output;

  }

  public function getSalesByPaymentReport(PipelineListener $listener, $data) {

    $this->salesByPayments->normalizeFilters($data);
    $dayNum = $data['day_num'];

    $response = [];

    foreach ($this->salesByPayments->getLastNDay($dayNum) as $salesByPayment) {
      $date = $salesByPayment->date;
      $type = $salesByPayment->info_id;

      $response[$date][$type] = $this->_transforms($salesByPayment, $this->salesByPayments);
    }

    return $listener->response(200, ['by_payments' => $response]);

  }

}
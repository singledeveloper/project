<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/2/17
 * Time: 1:44 PM
 */

namespace Odeo\Domains\Order\Salesreport\Ocommerce;

use Carbon\Carbon;
use Odeo\Domains\Order\Salesreport\Helper\Initializer;

class SalesOcommerceReportUpdater {

  private $ocommerceReport, $orderDetails;

  public function __construct() {

    $this->ocommerceReport = app()->make(\Odeo\Domains\Order\Salesreport\Ocommerce\Repository\SalesOcommerceRepository::class);
    $this->orderDetails = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);

  }

  public function updateOcommerceReport($services, $day = Initializer::YESTERDAY) {

    $salesLastNDayOcommerce = $this->ocommerceReport->findSalesLastNDayByServiceId($day, $services['id']);

    if (!$salesLastNDayOcommerce) {

      $sales = $this->orderDetails->findSalesComponentLastNDayByServiceId($day, $services['id']);
      $salesOcommerceReport = $this->ocommerceReport->getNew();
      $salesOcommerceReport->service_id = $services['id'];
      $salesOcommerceReport->date = Carbon::now()->subDays($day)->format('Y-m-d');
      $salesOcommerceReport->sales = $sales->total;
      $salesOcommerceReport->total_sale_amount = $sales->total_sale_amount ? $sales->total_sale_amount : 0;
      $salesOcommerceReport->total_base_amount = $sales->total_base_amount ? $sales->total_base_amount : 0;
      $salesOcommerceReport->total_profit = $sales->total_profit ? $sales->total_profit : 0;
      $salesOcommerceReport->is_locked = FALSE;
      $salesOcommerceReport->sales_target = Initializer::DEFAULT_TARGET_OCOMMERCE;

      $this->ocommerceReport->save($salesOcommerceReport);

    } else if (!$salesLastNDayOcommerce['is_locked']) {

      $sales = $this->orderDetails->findSalesComponentLastNDayByServiceId($day, $services['id']);
      $salesLastNDayOcommerce->sales = $sales->total;
      $salesLastNDayOcommerce->total_sale_amount = $sales->total_sale_amount ? $sales->total_sale_amount : 0;
      $salesLastNDayOcommerce->total_base_amount = $sales->total_base_amount ? $sales->total_base_amount : 0;
      $salesLastNDayOcommerce->total_profit = $sales->total_profit ? $sales->total_profit : 0;
      $salesLastNDayOcommerce->is_locked = TRUE;

      $this->ocommerceReport->save($salesLastNDayOcommerce);
    }

    $salesOcommerceReport = $this->ocommerceReport->findSalesTodayByServiceId($services['id']);

    if (!$salesOcommerceReport) {

      $salesOcommerceReport = $this->ocommerceReport->getNew();
      $salesOcommerceReport->service_id = $services['id'];
      $salesOcommerceReport->date = Carbon::now()->format('Y-m-d');
      $salesOcommerceReport->sales_target = Initializer::DEFAULT_TARGET_OCOMMERCE;
    }

    $sales = $this->orderDetails->findSalesComponentTodayByServiceId($services['id']);
    $salesOcommerceReport->sales = $sales->total;
    $salesOcommerceReport->total_sale_amount = $sales->total_sale_amount ? $sales->total_sale_amount : 0;
    $salesOcommerceReport->total_base_amount = $sales->total_base_amount ? $sales->total_base_amount : 0;
    $salesOcommerceReport->total_profit = $sales->total_profit ? $sales->total_profit : 0;

    $this->ocommerceReport->save($salesOcommerceReport);

  }

  public function recalculateOcommerceReport($services) {
    $sales = $this->orderDetails->findSalesComponentByServiceId($services['id'], '2016-07-01', Carbon::yesterday());

    $salesOcommerceReports = [];

    foreach ($sales as $sale) {
      $salesOcommerceReports[] = [
        'service_id' => $services['id'],
        'date' => $sale->date,
        'sales' => $sale->total,
        'sales_target' => Initializer::DEFAULT_TARGET_OCOMMERCE,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'total_sale_amount' => $sale->total_sale_amount ? $sale->total_sale_amount : 0,
        'total_base_amount' => $sale->total_base_amount ? $sale->total_base_amount : 0,
        'total_profit' => $sale->total_profit ? $sale->total_profit : 0,
        'is_locked' => TRUE,
      ];
    }
    $this->ocommerceReport->saveBulk($salesOcommerceReports);
  }
}
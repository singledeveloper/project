<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/28/16
 * Time: 9:06 PM
 */

namespace Odeo\Domains\Order\Salesreport\Platform\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\Salesreport\Platform\Model\SalesPlatformReport;

class SalesPlatformRepository extends Repository {

  public function __construct(SalesPlatformReport $salesPlatformReport) {
    $this->model = $salesPlatformReport;
  }

  public function getLockedReport($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->with('plan')->where('date', '=', $date)->where('is_locked', true)->get();
  }

  public function findSalesTodayByPlanId($id) {
    $date = Carbon::now()->format('Y-m-d');

    return $this->model
      ->where('plan_id', $id)
      ->where('date', '=', $date)->first();
  }

  public function findSalesLastNDayByPlanId($dayNum, $id) {
    $date = Carbon::today()->subDays($dayNum)->format('Y-m-d');

    return $this->model
      ->where('plan_id', $id)
      ->where('date', '=', $date)->first();
  }

  public function getLastNDay($dayNum) {

    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    if (isset($filters['search']['start_date'])) {
      $date = Carbon::parse($filters['search']['start_date']);
    } else {
      $date = Carbon::today();
    }

    if (isset($filters['search']['cursor'])) {
      if($filters['search']['cursor'] < 0) $date->addDays($filters['search']['cursor'] * 7);
      else $date->addDays($filters['search']['cursor'] * 7);
    }

    $dateEnd = clone $date;
    $dateStart = clone $date->subDays($dayNum);

    return $query->with('plan')->where('date', '>', $dateStart)->where('date', '<=', $dateEnd)->orderBy('date', 'asc')->get();

  }

}
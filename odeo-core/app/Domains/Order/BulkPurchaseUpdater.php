<?php

namespace Odeo\Domains\Order;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\UserGroupRepository;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Helper\Currency;

class BulkPurchaseUpdater {

  private $bulkPurchaseRepo, $userGroupRepo, $bulkRepo, $bulkId, $userId, $redis,
    $bulkNotCompleteAmount = 0, $bulkDisperseChain = [],
    $users, $pulsaOdeos, $bulks = [], $repeats = [], $codes = [], $prefixes = [], $approvalPendingRequestRepo;

  public function __construct() {
    $this->bulkPurchaseRepo = app()->make(\Odeo\Domains\Order\Repository\PulsaBulkPurchaseRepository::class);
    $this->bulkRepo = app()->make(\Odeo\Domains\Order\Repository\PulsaBulkRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->pulsaOdeos = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository::class);
    $this->redis = Redis::connection();
    $this->userGroupRepo = app()->make(\Odeo\Domains\Account\Repository\UserGroupRepository::class);
    $this->approvalPendingRequestRepo = app()->make(ApprovalPendingRequestRepository::class);
  }

  private function addBulk($key, $amount, $number) {
    if (!isset($this->repeats[$amount . '_' . $number]))
      $this->repeats[$amount . '_' . $number] = 0;
    $this->repeats[$amount . '_' . $number]++;
    if (is_numeric($amount)) $nominal = $amount . '000';
    else $nominal = $this->codes[$amount]->name;
    $this->bulks[] = [
      'user_id' => trim($this->userId),
      'pulsa_bulk_id' => $this->bulkId,
      'batch_flag' => $key,
      'nominal' => $nominal,
      'number' => $number,
      'request_message' => $amount . '.' . $number . '#' . $this->repeats[$amount . '_' . $number] . '.XXX',
      'is_bulk_processed' => false,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
  }

  private function fetchBulk($key, $amount, $number) {
    $this->bulkDisperseChain = [];
    $prefix = substr(purifyTelephone($number), 0, 5);
    if (isset($this->prefixes[$prefix])) {
      $amountAllowed = $this->prefixes[$prefix];
    }
    else $amountAllowed = [1000, 500, 200, 100, 50, 25, 20, 10, 5, 3, 2, 1];
    $amount = $amount / 1000;
    do {
      if (!in_array($amount, $amountAllowed)) {
        $newAmount = 0;
        foreach ($amountAllowed as $type) {
          if ($amount > $type) {
            $this->addBulk($key, $type, $number);
            $newAmount = $amount - $type;
            $this->bulkDisperseChain[] = $type;
            break;
          }
        }
        if ($newAmount == 0) {
          $this->bulkNotCompleteAmount = $amount;
          break;
        }
        else $amount = $newAmount;
      } else {
        $this->addBulk($key, $amount, $number);
        break;
      }

    } while (true);
  }

  public function mapPrefixes() {
    $pulsaPrefixes = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);

    $this->prefixes = [];
    $operatorNums = [];
    foreach ($this->pulsaOdeos->getAllOdeoPulsaWithOperator() as $item) {
      $nominal = $item->nominal / 1000;
      if ($nominal < 1) continue;
      if (!isset($operatorNums[$item->operator_id])) $operatorNums[$item->operator_id] = [];
      if (!in_array($nominal, $operatorNums[$item->operator_id]))
        $operatorNums[$item->operator_id][] = $nominal;
    }

    foreach ($pulsaPrefixes->getAll() as $item) {
      if (!isset($operatorNums[$item->operator_id])) continue;
      $this->prefixes[$item->prefix] = $operatorNums[$item->operator_id];
    }
  }

  public function insert(PipelineListener $listener, $data) {

    $this->bulkId = $data['pulsa_bulk_id'];
    $bulk = $this->bulkRepo->findById($this->bulkId);
    if ($bulk->status != BulkPurchase::PENDING) return $listener->response(400, "You can't add if the group already not in pending status");

    $redisKey = 'pulsa_bulk_purchases_group_lock_' . $this->bulkId;
    if ($this->redis->setnx($redisKey, 0)) $this->redis->expire($redisKey, 5 * 60);
    else return $listener->response(400, "You can't execute again under 5 minutes.");

    $users = [];
    try {
      $lines = preg_split('/\n|\r\n?/', trim($data['bulks']));

      $this->mapPrefixes();

      foreach ($lines as $key => $item) {
        $row = explode(',', $item);
        if (count($row) != 3) return $listener->response(400, 'Data at row ' . ($key + 1) . ' don\'t have 3 data needed. Bulk aborted.');
        list($userId, $number, $amount) = $row;
        $userId = trim($userId);
        if (!isset($users[$userId])) {
          $users[$userId] = $this->users->findById($userId);
          if (!$users[$userId]) return $listener->response(400, 'Account not exist: ' . $userId . '. Bulk aborted');
          if (!$this->userGroupRepo->findUserWithinGroup($userId, UserType::GROUP_CAN_ACCESS_PULSA_BULK))
            return $listener->response(400, 'You can\'t use this user for pulsa bulk: ' . $userId . '. Please check again and make sure you also input in H2H group if necessary. Bulk aborted');
        }
        $this->userId = $userId;
        $amount = str_replace('.', '', trim($amount));
        if (is_numeric($amount)) {
          $number = '0' . ltrim(str_replace('-', '', revertTelephone(trim($number))), '0');
          $this->fetchBulk($key, $amount, $number);
          if ($this->bulkNotCompleteAmount != 0) {
            $currency = app()->make(Currency::class);
            return $listener->response(400, 'Can\'t purchase amount ' .
              $currency->formatPrice($this->bulkNotCompleteAmount . '000')['formatted_amount'] . ' for row ' . ($key + 1) . ' (' . $number . '), ' .
              'Chaining from ' . $currency->formatPrice($amount)['formatted_amount'] . ': ' . implode('-', $this->bulkDisperseChain) . '-' . $this->bulkNotCompleteAmount .
              '. Bulk aborted.');
          }
        }
        else {
          if (!isset($this->codes[$amount])) {
            $this->codes[$amount] = $this->pulsaOdeos->findByInventoryCode($amount);
            if (!$this->codes[$amount]) return $listener->response(400, 'Inventory not exist: ' . $amount . '. Bulk aborted');
          }
          $this->addBulk($key, $amount, $number);
        }
      }
    }
    catch (\Exception $e) {
      return $listener->response(400, 'Error bulk: ' . $e->getMessage());
    }

    $this->bulkPurchaseRepo->saveBulk($this->bulks);

    if (count($users) == 1) {
      $request = $this->approvalPendingRequestRepo->getNew();
      $request->raw_data = json_encode([
        'description' => $bulk->name
      ]);
      $request->path = Approval::PATH_BULK_PULSA;
      $request->requested_by = $data['auth']['user_id'];
      $request->need_approver_from = $this->userId;
      $request->reference_id = $this->bulkId;
      $request->status = Approval::CREATED;
      $this->approvalPendingRequestRepo->save($request);
    }

    return $listener->response(200);
  }

  public function createGroup(PipelineListener $listener, $data) {
    $bulk = $this->bulkRepo->getNew();

    $bulk->name = $data['bulk_name'];
    $bulk->created_user_id = $data['auth']['user_id'];
    $bulk->status = BulkPurchase::PENDING;

    $this->bulkRepo->save($bulk);

    return $listener->response(200);
  }

  public function removeGroup(PipelineListener $listener, $data) {

    if ($bulk = $this->bulkRepo->findById($data['pulsa_bulk_id'])) {
      if ($bulk->status != BulkPurchase::PENDING)
        return $listener->response(400, 'Anda tidak dapat melakukan removal di grup ini.');

      if ($this->bulkPurchaseRepo->findProcessedInGroup($data['pulsa_bulk_id']))
        return $listener->response(400, 'Anda tidak dapat melakukan removal saat retry attempt.');

      $bulk->status = BulkPurchase::REMOVED;

      $this->bulkRepo->save($bulk);

      if ($pending = $this->approvalPendingRequestRepo->findPathReferenceId(Approval::PATH_BULK_PULSA, $bulk->id)) {
        if ($pending->status == Approval::CREATED) {
          $this->approvalPendingRequestRepo->delete($pending);
        }
        else $listener->response(400, "Can't remove, approval is in progress");
      }

      return $listener->response(200);
    }

    return $listener->response(400, 'Data not exist');
  }

  public function approveOrRejectGroup(PipelineListener $listener, $data) {

    $userGroupRepo = app()->make(UserGroupRepository::class);

    if (isAdmin($data) && !$userGroupRepo->findUserWithinGroup($data['auth']['user_id'], UserType::GROUP_BULK_PURCHASE_APPROVER))
      return $listener->response(400, 'Anda tidak memiliki akses.');

    if ($bulk = $this->bulkRepo->findById($data['pulsa_bulk_id'])) {
      if ($bulk->status != BulkPurchase::PENDING)
        return $listener->response(400, 'Anda tidak dapat melakukan perubahan.');

      if (!in_array($data['status'], [BulkPurchase::APPROVED, BulkPurchase::REJECTED]))
        return $listener->response(400, 'Status not valid');

      if ($data['status'] == BulkPurchase::APPROVED) {
        if (!$lastRow = $this->redis->hget(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LAST_ROW))
          $lastRow = 0;

        if ($this->bulkPurchaseRepo->findProcessedInGroup($data['pulsa_bulk_id'])) { // true means it want to approve a retry attempt
          if ($lastUnprocessedGlobal = $this->bulkPurchaseRepo->findLastUnprocessed($data['pulsa_bulk_id'])) {
            if ($lastRow != 0 && $lastUnprocessedGlobal->id > $lastRow)
              return $listener->response(400, "Tidak bisa melakukan retry sekarang, masih ada purchase yang belum terproses.");

            $this->redis->hdel(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LAST_ROW);
          }
          else return $listener->response(400, 'No need to retry this bulk.');
        }
        else {
          $lastUnprocessed = $this->bulkPurchaseRepo->findLastUnprocessed($data['pulsa_bulk_id'], true);
          if ($lastUnprocessed && $lastUnprocessed->id < $lastRow)
            $this->redis->hdel(BulkPurchase::REDIS_TEMP_BULK, BulkPurchase::REDIS_TEMP_BULK_KEY_LAST_ROW);
        }
      }

      $bulk->status = $data['status'];
      $bulk->approved_user_id = $data['auth']['user_id'];

      $this->bulkRepo->save($bulk);

      return $listener->response(200);
    }

    return $listener->response(400, 'Data not exist');
  }

  public function toggleDetail(PipelineListener $listener, $data) {
    if ($bulkDetail = $this->bulkPurchaseRepo->findById($data['pulsa_bulk_detail_id'])) {
      if (!$bulkDetail->is_bulk_processed) return $listener->response(400, 'Detail sudah terproses.');

      if ($bulkDetail->order_id != null) {
        $orderDetailPulsaSwitchers = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
        if ($switcher = $orderDetailPulsaSwitchers->findByOrderId($bulkDetail->order_id)) {
          if ($switcher->status != SwitcherConfig::SWITCHER_REFUNDED)
            return $listener->response(400, 'Anda tidak dapat melakukan retry di order yang belum di-refund');
        }
        else return $listener->response(400, 'Data error');
      }

      $bulkDetail->is_bulk_processed = false;
      $this->bulkPurchaseRepo->save($bulkDetail);

      $bulk = $this->bulkRepo->findById($bulkDetail->pulsa_bulk_id);
      $bulk->status = BulkPurchase::PENDING;
      $this->bulkRepo->save($bulk);

      if ($pending = $this->approvalPendingRequestRepo->findPathReferenceId(Approval::PATH_BULK_PULSA, $bulk->id)) {
        if (in_array($pending->status, [Approval::PROCESSED, Approval::FAILED])) {
          $pending->status = Approval::CREATED;
          $this->approvalPendingRequestRepo->save($pending);
        }
        else return $listener->response(400, "Can't retry, approval is in progress or rejected");
      }

      return $listener->response(200);
    }
    return $listener->response(400, 'Data not exist');
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 5:33 PM
 */

namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\ServiceDetail;

class CartChecker {


  private $currencyHelper;

  public function __construct() {
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
  }

  public function isDuplicate($cartData, $item) {

    ksort_recursive($item['item_detail']);

    $comparator['item_detail'] = \json_encode($item['item_detail']);
    $comparator['inventory_id'] = $item['item_id'];
    $comparator['service_detail_id'] = $item['service_detail_id'];

    foreach ($cartData['items'] as $cartItem) {

      if (
        $comparator['item_detail'] == \json_encode($cartItem['item_detail']) &&
        $comparator['inventory_id'] == $cartItem['inventory_id'] &&
        $comparator['service_detail_id'] == $cartItem['service_detail_id']
      ) {
        return true;
      }
    }

    return false;
  }

  public function hasUid($cartData, $uid) {
    return array_search($uid, array_column($cartData['items'], 'uid'));
  }

  public function hasCharge($cartData, $type) {
    return array_search($type, array_column($cartData['charges'], 'type'));
  }

  public function isFreePurchase($item) {

    switch ($item['service_id']) {
      case Service::PLAN:
        if ($item['inventory_id'] == Plan::FREE) {
          return true;
        }
        break;
    }

    return false;

  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/11/17
 * Time: 4:53 PM
 */

namespace Odeo\Domains\Order\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class UserPurchasesRetriever {

  private $redis;

  public function __construct() {
    $this->redis = Redis::connection();
  }


  public function getPurchaseInformationByDate($userIds, $dates, $serviceIds = []) {

    $userIds = is_array($userIds) ? $userIds : [$userIds];
    $dates = is_array($dates) ? $dates : [$dates];

    return $this->redis->pipeline(function ($pipe) use ($userIds, $dates, $serviceIds) {

      $toBeRetrieve = [
        'total_sales_quantity', 'total_sales_amount'
      ];

      foreach ($serviceIds as $serviceId) {
        $toBeRetrieve[] = 'total_sales_quantity_on_service_id_' . $serviceId;
      }

      foreach ($userIds as $userId) {

        foreach ($dates as $date) {

          $namespace = 'odeo_core:user:' . $userId . ':purchase_information_on_' . $date;

          $pipe->hmget($namespace, $toBeRetrieve);

        }
      }

    });

  }


}
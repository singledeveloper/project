<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 9:16 PM
 */

namespace Odeo\Domains\Order\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Service;

class AgentMasterSalesManager {

  private $agentMasterSales, $redis;

  const MAX_CACHE_DAY = 2;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->agentMasterSales = app()->make(\Odeo\Domains\Order\Repository\AgentMasterSaleRepository::class);
  }

  public function getPurchaseInformationByDate($userIds, $dates, $serviceIds = []) {

    $userIds = is_array($userIds) ? $userIds : [$userIds];
    $dates = is_array($dates) ? $dates : [$dates];

    return $this->redis->pipeline(function ($pipe) use ($userIds, $dates, $serviceIds) {

      $toBeRetrieve = [
        'total_sales_amount', 'total_sales_quantity'
      ];

      foreach ($serviceIds as $serviceId) {
        $toBeRetrieve[] = 'total_sales_quantity_on_service_id_' . $serviceId;
      }

      foreach ($userIds as $userId) {

        $prefix = 'user_id_' . $userId . '_';

        foreach ($dates as $date) {

          $namespace = 'odeo_core:agent_master_sales_purchase_information_on_' . $date;

          $pipe->hmget($namespace, array_map(function ($item) use ($prefix) {
            return $prefix . $item;
          }, $toBeRetrieve));

        }
      }

    });

  }


  public function update($data) {

    $date = Carbon::parse($data['date']);

    if ($sales = $this->agentMasterSales->getExisting(
      [
        'date' => $date->toDateString(),
        'user_id' => $data['user_id'],
        'store_id' => $data['store_id'],
        'service_id' => $data['service_id'],
      ])
    ) ;
    else {

      $sales = $this->agentMasterSales->getNew();

      $sales->date = $date->toDateString();
      $sales->service_id = $data['service_id'];
      $sales->user_id = $data['user_id'];
      $sales->store_id = $data['store_id'];
      $sales->sales_quantity = 0;
      $sales->sales_amount = 0;

    }

    $sales->sales_quantity += $data['quantity'];
    $sales->sales_amount += $data['sale_amount'];
    $sales->base_sales_amount += $data['base_sale_amount'];

    $this->agentMasterSales->save($sales);

    $this->_validateMaximumCacheDay($date) &&

    $this->redis->pipeline(function ($pipe) use ($sales, $data, $date) {

      $namespace = 'odeo_core:agent_master_sales_purchase_information_on_' . $date->toDateString();
      $prefix = 'user_id_' . $data['user_id'] . '_';

      if (!in_array($data['service_id'], [
        Service::DOMAIN, Service::WARRANTY, Service::PLAN, Service::OCASH, Service::ODEPOSIT
      ])
      ) {
        $pipe->hincrby($namespace, $prefix . 'total_sales_quantity', $data['quantity']);
        $pipe->hincrbyfloat($namespace, $prefix . 'total_sales_amount', $data['sale_amount']);
        $pipe->hincrbyfloat($namespace, $prefix . 'total_base_sales_amount', $data['base_sale_amount']);
      }

      $pipe->expireat($namespace, Carbon::create($date->year, $date->month, $date->day + self::MAX_CACHE_DAY, 0, 0, 0)->timestamp);

    });

  }

  private function _validateMaximumCacheDay(Carbon $date) {

    $now = Carbon::now();

    $date = Carbon::createFromDate($date->year, $date->month, $date->day);
    $now = Carbon::createFromDate($now->year, $now->month, $now->day);

    return abs($date->diffInDays($now)) <= (self::MAX_CACHE_DAY - 1);

  }


}
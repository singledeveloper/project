<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/16/17
 * Time: 10:39 PM
 */

namespace Odeo\Domains\Order\Helper;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\TransactionType;

class StatusParser {

  public function parse($order, $serviceId = null) {
    if (isNewDesign()) {
      return $this->parseForNewDesign($order, $serviceId);
    }
    return $this->parseForLegacy($order, $serviceId);
  }

  public function parseForNewDesign($order, $serviceId = null) {
    switch ($order['status']) {
      case OrderStatus::CREATED:
        return $this->transform('#5b6c9d', 'waiting', '#fff', 'Menunggu');

      case OrderStatus::OPENED:
        return $this->transform('#5b6c9d', 'payment_pending', '#fff', 'Menunggu pembayaran');

      case OrderStatus::CONFIRMED:
      case OrderStatus::FAKE_CONFIRMED:
        return $this->transform('#d2d6de', 'confirmed', '#000', 'Menunggu verifikasi');

      case OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL:
        return $this->transform('#d2d6de', 'waiting_approval', '#fff', 'Menunggu update vendor');

      case OrderStatus::WAITING_FOR_UPDATE:
        return $this->transform('#a94442', 'waiting_for_update', '#fff', 'Ralat nomor');

      case OrderStatus::WAITING_SUSPECT:
        return $this->transform('#f39c12', 'waiting_suspect', '#fff', 'Menunggu update operator');

      case OrderStatus::VERIFIED:
      case OrderStatus::PARTIAL_FULFILLED:
        return $this->transform('#ffb300', 'processing_order', '#fff', 'Diproses');

      case OrderStatus::COMPLETED:
      case OrderStatus::TEMPORARY_FAILED:
      case OrderStatus::MANUAL_FULFILLED:
      case OrderStatus::FREE_ORDER_COMPLETED:
        return $this->transform('#5aa520', 'completed', '#fff', 'Selesai');

      case OrderStatus::CANCELLED:
        return $this->transform('#909192', 'cancelled', '#fff', 'Dibatalkan');

      case OrderStatus::REFUNDED:
      case OrderStatus::REFUNDED_AFTER_SETTLEMENT:
        return $this->transform('#ea1616', 'refunded', '#fff', 'Dikembalikan');
    }
    return [];
  }

  private function parseForLegacy($order, $serviceId = null) {
    switch ($order['status']) {
      case OrderStatus::CREATED:
        return $this->transform('#d2d6de', 'CHECKOUT', '#000', 'Pending', $order['created_at']);

      case OrderStatus::OPENED:
        return $this->transform('#d2d6de', 'PAYMENT PENDING', '#000', 'Menunggu pembayaran', $order['opened_at'] ?? $order['created_at']);

      case OrderStatus::CONFIRMED:
      case OrderStatus::FAKE_CONFIRMED:
        return $this->transform('#d2d6de', 'CONFIRMED', '#000', 'Menunggu verifikasi admin', $order['paid_at']);

      case OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL:
        return $this->transform('#d2d6de', 'WAITING APPROVAL', '#000', 'Menunggu konfirmasi pembayaran dari vendor payment', $order['opened_at']  ?? $order['created_at']);

      case OrderStatus::WAITING_FOR_UPDATE:
        return $this->transform('#a94442', $this->getWaitingForUpdateStatus($serviceId), '#000', 'Menunggu ralat nomor', $order['opened_at']);

      case OrderStatus::WAITING_SUSPECT:
        return $this->transform('#f39c12', 'SUSPECT', '#000', 'Menunggu update operator (1x24 jam)', $order['paid_at']);
      case OrderStatus::VERIFIED:
      case OrderStatus::PARTIAL_FULFILLED:
        return $this->transform('#f39c12', 'PROCESSING ORDER', '#000', 'Sedang diproses', $order['paid_at']);

      case OrderStatus::COMPLETED:
      case OrderStatus::TEMPORARY_FAILED:
      case OrderStatus::MANUAL_FULFILLED:
      case OrderStatus::FREE_ORDER_COMPLETED:
        return $this->transform('#00a65a', 'COMPLETED', '#000', 'Transaksi selesai', $order['closed_at'] ? $order['closed_at'] : $order['paid_at']);

      case OrderStatus::CANCELLED:
        return $this->transform('#dd4b39', 'CANCELLED', '#000', 'Transaksi dibatalkan', $order['closed_at']);

      case OrderStatus::REFUNDED:
      case OrderStatus::REFUNDED_AFTER_SETTLEMENT:
        return $this->transform('#dd4b39', 'REFUNDED', '#000', 'Transaksi dibatalkan, dana dikembalikan', $order['closed_at']);
    }
    return [];
  }

  private function transform($statusColor, $status, $textColor, $message = null, $timestamp = null) {
    $output = [
      'status' => $status,
      'status_color' => $statusColor,
      'status_text_color' => $textColor
    ];
    if ($timestamp) $output['status_timestamp'] = $timestamp;
    if ($message) $output['status_message'] = $message;
    return $output;
  }

  private function getWaitingForUpdateStatus($serviceId) {
    switch ($serviceId) {
      case Service::PULSA:
      case Service::PAKET_DATA:
      case  Service::BOLT:
        return 'INVALID PHONE NUMBER';
      case Service::PLN:
        return 'INVALID NO METER';
      default:
        return 'INVALID DATA';
    }
  }

  public function normalize($status) {
    switch ($status) {
      case OrderStatus::CONFIRMED:
      case OrderStatus::FAKE_CONFIRMED:
        return [
          OrderStatus::CONFIRMED,
          OrderStatus::FAKE_CONFIRMED,
        ];
        break;
      case OrderStatus::VERIFIED:
      case OrderStatus::PARTIAL_FULFILLED:
        return [
          OrderStatus::VERIFIED,
          OrderStatus::PARTIAL_FULFILLED,
        ];
        break;
      case OrderStatus::COMPLETED:
      case OrderStatus::TEMPORARY_FAILED:
      case OrderStatus::MANUAL_FULFILLED:
        return [
          OrderStatus::COMPLETED,
          OrderStatus::TEMPORARY_FAILED,
          OrderStatus::MANUAL_FULFILLED,
        ];
        break;
    }
    return $status;
  }

  public function parseMessageForSameTransaction($switcher) {
    $orderDetail = $switcher->orderDetail;
    $order = $orderDetail->order;

    $temp = explode('/', $orderDetail->name);
    $cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);

    if ($order->status == OrderStatus::COMPLETED) {
      $message = trans('pulsa.inline.success', [
        'order_id' => $order->id,
        'item_name' => $temp[0],
        'number' => revertTelephone($switcher->number),
        'sn' => $switcher->serial_number != null ? $switcher->serial_number : 'N / A',
        'sisa_saldo' => $cashManager->findOrderHistory($order, TransactionType::PAYMENT)['message']
      ]);
    } else if (OrderStatus::isRefunded($order->status)) {
      $message = trans('pulsa.inline.refund', [
        'order_id' => $order->id,
        'item_name' => $temp[0],
        'number' => revertTelephone($switcher->number),
        'sisa_saldo' => $cashManager->findOrderHistory($order, TransactionType::REFUND)['message']
      ]);
    } else $message = trans('pulsa.inline.pending', [
      'order_id' => $order->id,
      'item_name' => $temp[0],
      'number' => revertTelephone($switcher->number)
    ]);

    return trans('pulsa.inline.same_transaction', [
      'date' => date('d/m/Y H:i:s', strtotime($order->paid_at != null ? $order->paid_at : $order->created_at)),
      'message' => $message
    ]);
  }

}

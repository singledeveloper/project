<?php
namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Service;

class OrderTypeParser {

  public function parse($order) {
    $output = ['tags'=>[]];
    $tag = [];

    if(isset($order->role)) {
      $role = $order->role == 'rush' ? 'hustler' : $order->role;
      $tag['text'] = strtoupper($role);

      switch ($role) {
        case 'hustler':
          $tag['color'] = '#F5B80E';
          $tag['text_color'] = '#092529';
          break;
        case 'mentor':
          $tag['color'] = '#7CE020';
          $tag['text_color'] = '#092529';
          break;
        case 'leader':
          $tag['color'] = '#13CBAB';
          $tag['text_color'] = '#092529';
          break;
        default:
          break;
      }
      // $output['tags'][] = $tag;
    }

    if((isset($order->is_agent) && $order->is_agent) || (isset($order->role) && $order->role == 'agent')) {
      $tag['text'] = 'AGENT';
      $tag['color'] = '#FFD700';
      $tag['text_color'] = '#092529';
      $output['tags'][] = $tag;
    } else if(isset($order->is_community) && $order->is_community) {
      $tag['text'] = 'MARKETPLACE';
      $tag['color'] = '#C0C0C0';
      $tag['text_color'] = '#092529';
      $output['tags'][] = $tag;
    }

    return $output;
  }
}

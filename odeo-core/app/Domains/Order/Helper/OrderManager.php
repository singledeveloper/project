<?php

namespace Odeo\Domains\Order\Helper;

class OrderManager {
  
  public function __construct() {
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetail = app()->make(\Odeo\Domains\Order\Repository\OrderDetailRepository::class);
    $this->parser = app()->make(\Odeo\Domains\Order\Helper\OrderParser::class);
  }
  
  public function getByTopupId($topupIds) {
    $topups = [];
    
    if ($data = $this->order->findTopupOrder($topupIds)) {
      foreach ($data as $item) {
        $order = [];
        $order["id"] = $item->id;
        $order["status"] = $this->parser->status($item);
        $topups[$item->topup_id] = $order;
      }
    }
    return $topups;
  }

  public function getByStoreDepositId($depositIds) {
    $storeDeposits = [];

    if ($data = $this->order->findStoreDepositOrder($depositIds)) {
      foreach ($data as $item) {
        $order = [];
        $order["id"] = $item->id;
        $order["status"] = $this->parser->status($item);
        $storeDeposits[$item->store_deposit_id] = $order;
      }
    }
    return $storeDeposits;
  }
  
  public function getByStoreId($storeIds) {
    $stores = [];
    
    if ($data = $this->order->findStoreOrder($storeIds)) {
      foreach ($data as $item) {

        $order = [];
        $order["id"] = $item->id;
        $order["status"] = $this->parser->status($item);
        $stores[$item->store_id] = $order;
      }
    }
    
    return $stores;
  }
  
  public function getByWarrantyId($warrantyIds) {
    $warranties = [];
    
    if ($data = $this->order->findWarrantyOrder($warrantyIds)) {
      foreach ($data as $item) {
        $order = [];
        $order["id"] = $item->id;
        $order["status"] = $this->parser->status($item);
        $warranties[$item->warranty_id] = $order;
      }
    }
    
    return $warranties;
  }

  public function getMdPlusOrderByStoreId($storeIds) {
    $stores = [];
    
    if ($data = $this->order->findMdPlusOrder($storeIds)) {
      foreach ($data as $item) {

        $order = [];
        $order["id"] = $item->id;
        $order["status"] = $this->parser->status($item);
        $stores[$item->store_id] = $order;
      }
    }
    
    return $stores;
  }
}

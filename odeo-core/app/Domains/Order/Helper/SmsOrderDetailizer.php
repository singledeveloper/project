<?php

namespace Odeo\Domains\Order\Helper;

use Odeo\Domains\Constant\ServiceDetail;

class SmsOrderDetailizer {

  public function __construct() {
    $this->switcherOrder = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\OrderDetailPulsaSwitcherRepository::class);
  }

  public function detailizeItem($order, $expand) {
    $expand = array_flip($expand);
    $detail = $order->details[0];

    $item = null;

    switch ($detail->service_detail_id) {
      case ServiceDetail::PULSA_ODEO:
      case ServiceDetail::PLN_ODEO:
      case ServiceDetail::BOLT_ODEO:
      case ServiceDetail::PAKET_DATA_ODEO:
      case ServiceDetail::TRANSPORTATION_ODEO:
        $item = $this->switcherOrder->findByOrderDetailId($detail->id);

        $temp['number'] = ($item) ? $item->number : "";

        if (isset($expand['mask_phone_number']) && $length = strlen($temp['number'])) {
          $temp['number'] = str_repeat('*', max($length - 4, 0)) . substr($temp['number'], max($length - 4, 0));
        }
        break;
    }

    $temp['name'] = $detail->name;

    return $temp;
  }

}

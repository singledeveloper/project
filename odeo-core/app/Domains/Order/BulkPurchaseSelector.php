<?php

namespace Odeo\Domains\Order;

use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Jobs\BulkPurchaseExport;
use Odeo\Domains\Transaction\Helper\Currency;

class BulkPurchaseSelector implements SelectorListener {

  private $bulkPurchaseRepo, $bulkRepo, $currencyHelper, $approvalPendingRequestRepo;

  public function __construct() {
    $this->bulkPurchaseRepo = app()->make(\Odeo\Domains\Order\Repository\PulsaBulkPurchaseRepository::class);
    $this->bulkRepo = app()->make(\Odeo\Domains\Order\Repository\PulsaBulkRepository::class);
    $this->approvalPendingRequestRepo = app()->make(ApprovalPendingRequestRepository::class);
    $this->currencyHelper = app()->make(Currency::class);
  }

  private $groupStatus = '';

  public function _transforms($item, Repository $repository) {
    $status = $item->status;
    if ($item->status == SwitcherConfig::SWITCHER_COMPLETED) $statusMessage = "SUCCESS";
    else if ($item->status == SwitcherConfig::SWITCHER_REFUNDED) {
      $statusMessage = !$item->is_bulk_processed ?
        ($this->groupStatus == BulkPurchase::PENDING ? "WAITING APPROVAL" : "RETRY IN QUEUE") : "FAIL";
      if (!$item->is_bulk_processed) $status = SwitcherConfig::SWITCHER_IN_QUEUE;
    }
    else if (!$item->is_bulk_processed) {
      $status = SwitcherConfig::SWITCHER_IN_QUEUE;
      $statusMessage = $this->groupStatus == BulkPurchase::PENDING ? "WAITING APPROVAL" : "IN QUEUE";
    }
    else if ($item->status != null) $statusMessage = "PENDING";
    else $statusMessage = "FAIL";

    return [
      'id' => $item->id,
      'order_id' => $item->order_id,
      'user_id' => $item->user_id,
      'user_name' => $item->user_name,
      'number' => $item->number,
      'requested_at' => $item->requested_at,
      'response' => $item->response_message,
      'nominal' => is_numeric($item->nominal) ? number_format($item->nominal) : $item->nominal,
      'total' => $item->total ? $this->currencyHelper->formatPrice(
        $item->status == SwitcherConfig::SWITCHER_COMPLETED ? $item->total : 0) : null,
      'serial_number' => $item->serial_number,
      'status' => $status,
      'status_message' => $statusMessage
    ];
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getGroups(PipelineListener $listener, $data) {
    $this->bulkRepo->normalizeFilters($data);

    $bulkGroups = [];

    foreach ($this->bulkRepo->gets() as $item) {
      if ($item->status == BulkPurchase::PENDING) $status = "PENDING";
      else if ($item->status == BulkPurchase::APPROVED) $status = "APPROVED";
      else if ($item->status == BulkPurchase::REJECTED) $status = "REJECTED";
      else $status = "UNKNOWN";

      $bulkGroups[] = [
        'id' => $item->id,
        'name' => $item->name,
        'status' => $status,
        'created_at' => $item->created_at->format('Y-m-d H:i:s')
      ];
    }

    if (count($bulkGroups) > 0) return $listener->response(200, array_merge([
      'bulk_groups' => $bulkGroups
    ], $this->bulkRepo->getPagination()));

    return $listener->response(204, ['bulk_groups' => []]);
  }

  public function getGroupSummary(PipelineListener $listener, $data) {
    if (!isAdmin($data)) {
      $pending = $this->approvalPendingRequestRepo->findPathReferenceId(Approval::PATH_BULK_PULSA, $data['pulsa_bulk_id']);
      if (!$pending) return $listener->response(400, 'Data not exist');
      if ($pending->need_approver_from != $data['auth']['user_id']) {
        return $listener->response(400, 'Invalid data');
      }
    }
    if ($bulk = $this->bulkRepo->findById($data['pulsa_bulk_id'])) {
      $totalNominal = 0;
      $usingCodes = [];
      foreach ($this->bulkPurchaseRepo->getsLite($data['pulsa_bulk_id']) as $item) {
        if (is_numeric($item->nominal)) $totalNominal+= $item->nominal;
        else {
          if (!isset($usingCodes[$item->nominal])) $usingCodes[$item->nominal] = 0;
          $usingCodes[$item->nominal]++;
        }
      }
      $temp = [];
      foreach ($usingCodes as $key => $item) {
        $temp[] = [
          'name' => $key,
          'count' => $item
        ];
      }
      $usingCodes = $temp;
      return $listener->response(200, [
        'pulsa_bulk_id' => $data['pulsa_bulk_id'],
        'name' => $bulk->name,
        'nominals' => [
          'total' => $this->currencyHelper->formatPrice($totalNominal),
          'using_codes' => $usingCodes
        ]
      ]);
    }
    return $listener->response(400, 'Data not exist');
  }

  public function getGroupDetails(PipelineListener $listener, $data) {
    if (!isAdmin($data)) {
      $pending = $this->approvalPendingRequestRepo->findPathReferenceId(Approval::PATH_BULK_PULSA, $data['pulsa_bulk_id']);
      if (!$pending) return $listener->response(400, 'Data not exist');
      if ($pending->need_approver_from != $data['auth']['user_id']) {
        return $listener->response(400, 'Invalid data');
      }
    }

    $this->bulkPurchaseRepo->normalizeFilters($data);

    $groupDetails = [];
    if ($bulk = $this->bulkRepo->findById($data['pulsa_bulk_id'])) {
      $this->groupStatus = $bulk->status;
    }

    foreach ($this->bulkPurchaseRepo->gets(isset($data['all'])) as $item) {
      $groupDetails[] = $this->_transforms($item, $this->bulkPurchaseRepo);
    }

    if (count($groupDetails) > 0) return $listener->response(200, array_merge([
      'bulk_group_details' => $groupDetails
    ], $this->bulkPurchaseRepo->getPagination()));

    return $listener->response(204, ['bulk_group_details' => []]);
  }

  public function export(PipelineListener $listener, $data) {
    if (!isAdmin($data)) {
      $pending = $this->approvalPendingRequestRepo->findPathReferenceId(Approval::PATH_BULK_PULSA, $data['pulsa_bulk_id']);
      if (!$pending) return $listener->response(400, 'Data not exist');
      if ($pending->need_approver_from != $data['auth']['user_id']) {
        return $listener->response(400, 'Invalid data');
      }
    }

    $filters = [
      'file_type' => $data['file_type'] ?? 'xlsx',
      'pulsa_bulk_id' => $data['pulsa_bulk_id']
    ];
    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue(new BulkPurchaseExport($filters, $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

}
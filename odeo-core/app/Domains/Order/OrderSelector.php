<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:31 PM
 */

namespace Odeo\Domains\Order;

use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Order\Helper\OrderDetailizer;
use Odeo\Domains\Order\Helper\OrderParser;
use Odeo\Domains\Order\Helper\StatusParser;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Helper\BankTransferTnc;
use Odeo\Domains\Payment\Odeo\BankTransfer\Repository\PaymentOdeoBankTransferDetailRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelInformationRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Subscription\StoreSelector;

class OrderSelector implements SelectorListener {

  private $orders, $currencyHelper, $orderDetailizer, $payments, $paymentOdeoBankTransferDetails, $dokuVaPayments, $dokuPayments, $statusParser, $storeSelector;

  public function __construct() {
    $this->orders = app()->make(OrderRepository::class);
    $this->orderParser = app()->make(OrderParser::class);
    $this->currencyHelper = app()->make(Currency::class);
    $this->orderDetailizer = app()->make(OrderDetailizer::class);
    $this->payments = app()->make(PaymentRepository::class);
    $this->paymentInformations = app()->make(PaymentOdeoPaymentChannelInformationRepository::class);
    $this->statusParser = app()->make(StatusParser::class);
    $this->storeSelector = app()->make(StoreSelector::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }


  public function _transforms($order, Repository $repository) {
    $output = [];

    $output['order_id'] = $order->id;
    $output['status'] = $order->status;
    $output['created_at'] = $order->created_at;
    $output['opened_at'] = $order->opened_at;
    $output['paid_at'] = $order->paid_at;
    $output['expired_at'] = $order->expired_at;
    $output['closed_at'] = $order->closed_at;
    $output['cart_data']['total'] = $this->currencyHelper->formatPrice($order->total);
    $output['cart_data']['subtotal'] = $this->currencyHelper->formatPrice($order->subtotal);
    $output['gateway'] = Payment::getGatewayName($order->gateway_id);

    if ($repository->hasExpand('user')) {
      $user = $order->user;

      $output['user']['id'] = $user->id;

      if (isset($user->telephone)) $output['user']['telephone'] = $user->telephone;
      else if (isset($order->phone_number)) $output['user']['telephone'] = $order->phone_number;

      if (isset($user->email)) $output['user']['email'] = $user->email;
      else if (isset($order->email)) $output['user']['email'] = $order->email;

      if (isset($user->name)) $output['user']['name'] = $user->name;
      else if (isset($order->name)) $output['user']['name'] = $order->name;

      if (isset($order->salutation)) $output['user']['salutation'] = $order->salutation;
    }

    if (isset($order->seller_store_id)) $output['cart_data']['seller_store_id'] = isset($order->seller_store_id) ? $order->seller_store_id : '';
    if (isset($order->actual_store_id)) $output['cart_data']['actual_store_id'] = isset($order->actual_store_id) ? $order->actual_store_id : '';

    $output['cart_data']['charges'] = $this->orderDetailizer->detailizeCharge($order->id, (function () {
      if (isAdmin()) {
        return [OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY];
      }
      return [];
    })());
    $output['cart_data']['items'] = $this->orderDetailizer->detailizeItem($order);
    $output = array_merge($output, $this->orderDetailizer->detailizeReceipt($output));

    if ($repository->hasExpand('seller')) {
      $output['cart_data']['seller_store_name'] = $order->actual_store_id ? $order->actualStore->name : 'ODEO';

      $logoPath = AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::DEFAULT_STORE_LOGO);
      $output['cart_data']['seller_store_logo'] = $logoPath;

      $output['cart_data']['is_default_store'] = !$order->actual_store_id ||
        $order->actual_store_id !== $this->storeSelector->getUserMasterId($output['cart_data']['items'][0]['service_id']);

      $output['cart_data']['seller_store_user_id'] = $this->storeSelector->getSellerUserId($order->actual_store_id);
      $output['cart_data']['is_supplier'] = $this->storeSelector->isStoreCanSupply($order->actual_store_id);
    }

    if ($payment = $this->payments->findByOrderId($order->id)) {

      $output['payment']['opc'] = $payment->opc;
      $output['payment']['opc_group'] = $payment->info_id;

      if ($repository->hasExpand('payment')) {

        $paymentInformation = $this->paymentInformations->findById($payment->info_id);

        $output['payment']['name'] = $paymentInformation->name;

        if ($repository->hasExpand('payment_logo')) {
          if (isNewDesign()) {
            $output['payment']['logo'] = AssetAws::getAssetPath(AssetAws::PAYMENT, $paymentInformation->logo);
          } else {
            $output['payment']['logo'] = baseUrl('images/payment/' . $paymentInformation->logo);
          }
        }

        if ($repository->hasExpand('payment_fee')) {
          if (!isset($paymentDetailizer)) $paymentDetailizer = app()->make(\Odeo\Domains\Payment\Helper\PaymentDetailizer::class);
          $output['payment'] = $paymentDetailizer->detailize($order, $output['payment']);
        }

        switch ($payment->info_id) {
          case Payment::OPC_GROUP_BANK_TRANSFER_BCA:
          case Payment::OPC_GROUP_BANK_TRANSFER_BRI:
          case Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI:
          case Payment::OPC_GROUP_BANK_TRANSFER_BNI:
          case Payment::OPC_GROUP_BANK_TRANSFER_CIMB:
          case Payment::OPC_GROUP_BANK_TRANSFER_PERMATA:

            $bankTransferTnc = app()->make(BankTransferTnc::class);
            $output['payment']['tnc'] = $bankTransferTnc->getTnc($paymentInformation->id);
            if ($detail = \json_decode($paymentInformation->detail)) {
              $output['payment']['detail'] = $detail;
            }
            if (!$payment->reference_id) break;
            if (!$this->paymentOdeoBankTransferDetails) $this->paymentOdeoBankTransferDetails = app()->make(PaymentOdeoBankTransferDetailRepository::class);

            $bankTransferDetail = $this->paymentOdeoBankTransferDetails->findById($payment->reference_id);

            $output['payment']['confirmation_account_number'] = $bankTransferDetail->account_number;
            $output['payment']['confirmation_account_name'] = $bankTransferDetail->account_name;
            $output['payment']['confirmation_transfer_amount'] = $this->currencyHelper->formatPrice($bankTransferDetail->transfer_amount);
            $output['payment']['is_auto_confirm_transfer'] = $bankTransferDetail->is_auto_confirm_transfer;

            break;

          case Payment::OPC_GROUP_ATM_TRANSFER_VA:
          case Payment::OPC_GROUP_ALFA:
            if (!$this->dokuPayments) $this->dokuPayments = app()->make(\Odeo\Domains\Payment\Doku\Repository\PaymentDokuPaymentRepository::class);
            if (!$this->dokuVaPayments) $this->dokuVaPayments = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository::class);

            $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

            $vaPayment = $this->dokuVaPayments->findById($dokuPayment->reference_id);

            $output['payment']['pay_code'] = $vaPayment->pay_code;

            break;
        }
      }
    }

    return $output;
  }

  public function find(PipelineListener $listener, $data) {
    $this->orders->normalizeFilters($data);

    $order = $this->orders->findById($data['order_id']);

    $output = $this->_transforms($order, $this->orders);

    if (isNewDesign()) {
      $output = array_merge($output, $this->statusParser->parse($order, $order->service_id));
    } else {
      $output['status'] = $this->orderParser->status($order);
    }

    return $listener->response(200, $this->_extends($output, $this->orders));

  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 6:05 PM
 */

namespace Odeo\Domains\Order;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;

class OrderCanceller {

  public function __construct() {
    $this->order = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderDetailizer = app()->make(\Odeo\Domains\Order\Helper\OrderDetailizer::class);
    $this->remover = app()->make(\Odeo\Domains\Order\CartRemover::class);
    $this->tempDepositHelpers = app()->make(\Odeo\Domains\Transaction\Helper\TempDepositHelper::class);
  }

  public function cancel(PipelineListener $listener, $data) {

    $order = $this->order->findById($data['order_id']);
    if (!$order) return $listener->response(400, "Order not found.");

    if (!isAdmin($data) && $order->user_id != $data["auth"]["user_id"])
      return $listener->response(400, "You don't have credential to access this data.");

    if ($order->status <= OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL) {
      $order->status = OrderStatus::CANCELLED;
      $order->closed_at = date('Y-m-d H:i:s');
      $this->order->save($order);

      $details = array_first($this->orderDetailizer->detailizeItem($order, ['order_detail_id', 'base_price']));

      $this->remover->removeItem($listener, [
        "cart_item" => $details
      ]);

      if (isset($details['agent_base_price']) && $details['agent_base_price'] != null) {
        $this->tempDepositHelpers->refundAgentDeposit($order->id);
      }
      $this->tempDepositHelpers->deleteDeposit($order->id);

      return $listener->response(200);
    }
    return $listener->response(400, "Can't cancel this order.");
  }

  public function cancelConfirm(PipelineListener $listener, $data) {

    $order = $this->order->findById($data['order_id']);
    if (!$order) return $listener->response(400, "Order not found.");

    if (!isAdmin($data) && $order->user_id != $data["auth"]["user_id"])
      return $listener->response(400, "You don't have credential to access this data.");

    if ($order->status == OrderStatus::CONFIRMED || $order->status == OrderStatus::FAKE_CONFIRMED) {
      $order->status = OrderStatus::OPENED;
      $this->order->save($order);

      return $listener->response(200);
    }
    return $listener->response(400, "Can't cancel confirmation of this order.");
  }
}

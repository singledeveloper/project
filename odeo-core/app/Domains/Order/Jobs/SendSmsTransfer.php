<?php

namespace Odeo\Domains\Order\Jobs;

use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Vendor\SMS\Job\SendSms;
use Odeo\Jobs\Job;

class SendSmsTransfer extends Job  {

  protected $telephone;
  protected $within_password;
  protected $amount;
  protected $sender;
  protected $action;


  public function __construct($telephone, $within_password, $amount = "", $action = "", $sender = "Odeo") {
    parent::__construct();
    $this->telephone = $telephone;
    $this->within_password = $within_password;
    $this->amount = ($amount != "") ? $amount["formatted_amount"] : "money";
    $this->sender = explode(" ", $sender)[0];
    $this->action = $action;
  }


  public function handle() {
    //bit.ly/2iETGQn >> link to m.odeo.co.id
    //bit.ly/2ihOgKF >> link to m.staging.odeo.co.id
    //bit.ly/2htsKRV >> link to android playstore

    if (app()->environment() == 'production') $link = "bit.ly/2iETGQn";
    else $link = "bit.ly/2ihOgKF";

    dispatch(new SendSms([
      'sms_destination_number' => $this->telephone,
      'sms_text' => 'Anda mendapatkan ' . $this->action . ' ' . $this->amount . ' dari ' . $this->sender . '. Silahkan login di ' . $link . ' dengan menggunakan no HP ' . $this->telephone .' & PIN ' . $this->within_password,
      'sms_path' => SMS::NEXMO
    ]));

  }
}

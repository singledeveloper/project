<?php

namespace Odeo\Domains\Order\PulsaRecurring\Repository;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Order\PulsaRecurring\Model\PulsaRecurring;

class PulsaRecurringRepository extends Repository {

  public function __construct(PulsaRecurring $pulsaRecurring) {
    $this->model = $pulsaRecurring;
  }

  public function getRecurringQueues($take = 10) {
    return $this->model->where(function ($query) {
      $query->where('type', Recurring::TYPE_ONCE . '_' . date('Y-m-d'))
        ->orWhere('type', Recurring::TYPE_DAY)
        ->orWhere('type', Recurring::TYPE_WEEK . '_' . date('N'))
        ->orWhere('type', Recurring::TYPE_MONTH . '_' . date('j'));
    })->where(\DB::raw('date(created_at)'), '<>', date('Y-m-d'))
      ->where(function ($query2) {
        $query2->whereNull('last_executed_at')
          ->orWhere(\DB::raw('date(last_executed_at)'), '<>', date('Y-m-d'));
      })
      ->where('is_enabled', true)
      ->where('is_active', true)
      ->take($take)->select('id', 'type', 'created_at')->get();
  }

  public function deactivateExpiredRecurring() {
    return $this->model->where(\DB::raw('date(expired_at)'), '<=', date('Y-m-d'))
      ->where('is_active', true)
      ->update(['is_active' => false]);
  }

  public function getByUserId($userId) {
    return $this->getResult($this->model
      ->select(\DB::raw('
        distinct on (pulsa_recurrings.id)
        pulsa_recurrings.*,
        pulsa_recurring_histories.error_message,
        pulsa_odeos.name as pulsa_odeos_name,
        pulsa_odeos.service_detail_id as pulsa_odeos_service_detail_id,
        stores.name as stores_name
      '))
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_recurrings.pulsa_odeo_id')
      ->leftJoin('stores', 'stores.id', '=', 'pulsa_recurrings.store_id')
      ->leftJoin('pulsa_recurring_histories', 'pulsa_recurring_histories.pulsa_recurring_id', '=', 'pulsa_recurrings.id')
      ->where('user_id', $userId)
      ->where('pulsa_recurrings.is_active', true)
      ->orderBy('pulsa_recurrings.id', 'desc')
      ->orderBy('pulsa_recurring_histories.updated_at', 'desc'));
  }

  public function findDetailById($id) {
    return $this->model
      ->select(\DB::raw('
        pulsa_recurrings.*,
        pulsa_odeos.name as pulsa_odeos_name,
        pulsa_odeos.service_detail_id as pulsa_odeos_service_detail_id,
        stores.name as stores_name
      '))
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_recurrings.pulsa_odeo_id')
      ->leftJoin('stores', 'stores.id', '=', 'pulsa_recurrings.store_id')
      ->where('pulsa_recurrings.id', $id)
      ->first();
  }

  public function getScheduledByUserId($userId) {
    return $this->getResult($this->model
      ->select(\DB::raw('
        pulsa_recurrings.*,
        pulsa_odeos.id as pulsa_odeos_id,
        pulsa_odeos.name as pulsa_odeos_name,
        pulsa_odeos.service_detail_id as pulsa_odeos_service_detail_id,
        stores.name as stores_name
      '))
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'pulsa_recurrings.pulsa_odeo_id')
      ->leftJoin('stores', 'stores.id', '=', 'pulsa_recurrings.store_id')
      ->where('user_id', $userId)
      ->where('pulsa_recurrings.is_active', true)
      ->where('is_enabled', true)
      ->orderBy('next_payment', 'asc')
    );
  }

  public function getAllActive() {
    return $this->model->where('is_active', true)->get();
  }

}

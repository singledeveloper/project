<?php

namespace Odeo\Domains\Order\PulsaRecurring\Jobs;
use Odeo\Domains\Order\PulsaRecurring\Helper\PulsaRecurringHelper;
use Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository;
use Odeo\Jobs\Job;

class PulsaRecurringUpdatePrice extends Job {

  private $pulsaRecurring, $pulsaRecurringHelper;

  public function __construct() {
    parent::__construct();
  }

  public function handle() {
    $this->pulsaRecurring = app()->make(PulsaRecurringRepository::class);
    $this->pulsaRecurringHelper = app()->make(PulsaRecurringHelper::class);

    $pulsaRecurringUpdated = [];
    foreach($this->pulsaRecurring->getAllActive() as $pulsaRecurring) {
      $pulsaRecurringUpdated[] = [
        'id' => $pulsaRecurring->id,
        'price' => $this->pulsaRecurringHelper->getEstimatedPrice($pulsaRecurring->pulsa_odeo_id, $pulsaRecurring->store_id, $pulsaRecurring->user_id)
      ];
    }

    $this->pulsaRecurring->updateBulk($pulsaRecurringUpdated);
  }
}

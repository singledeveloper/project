<?php

namespace Odeo\Domains\Order\PulsaRecurring\Helper;

use Carbon\Carbon;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Inventory\Helper\MarginFormatter;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\PulsaOdeoRepository;
use Odeo\Domains\Inventory\Repository\ServiceDetailRepository;
use Odeo\Domains\Subscription\ManageInventory\Helper\PriceGetter;

class PulsaRecurringHelper {

  private $pulsaOdeo;
  private $marginFormatter;
  private $priceGetter;
  private $serviceDetail;

  public function __construct() {
    $this->pulsaOdeo = app()->make(PulsaOdeoRepository::class);
    $this->marginFormatter = app()->make(MarginFormatter::class);
    $this->serviceDetail = app()->make(ServiceDetailRepository::class);
    $this->priceGetter = app()->make(PriceGetter::class);
  }

  public function getNextPayment($pulsaRecurringType, $lastDate) {
    $date = Carbon::now()->startOfDay();
    $withTime = false;
    $temp = explode('_', $pulsaRecurringType);
    if (strpos($pulsaRecurringType, Recurring::TYPE_ONCE) !== false) {
      $date = Carbon::parse($temp[1]);
      if (isset($temp[2])) {
        $date->addHours($temp[2]);
        $withTime = true;
      }
    } else if (strpos($pulsaRecurringType, Recurring::TYPE_DAY) !== false) {
      if (Carbon::parse($lastDate)->startOfDay()->diffInDays($date) === 0) {
        $date = $date->tomorrow();
      }
      if (isset($temp[2])) {
        $date->addHours($temp[2]);
        $withTime = true;
      }
    }
    else if (strpos($pulsaRecurringType, Recurring::TYPE_DAY_WORK) !== false) {
      if (Carbon::parse($lastDate)->startOfDay()->diffInDays($date) === 0) {
        $date = $date->tomorrow();
        if ($date->format('N') == 6) $date->addDays(2);
        else if ($date->format('N') == 7) $date->addDay();
      }
      if (isset($temp[2])) {
        $date->addHours($temp[2]);
        $withTime = true;
      }
    } else if (strpos($pulsaRecurringType, Recurring::TYPE_DAY_MON_SAT) !== false) {
      if (Carbon::parse($lastDate)->startOfDay()->diffInDays($date) === 0) {
        $date = $date->tomorrow();
        if ($date->format('N') == 7) $date->addDay();
      }
      if (isset($temp[2])) {
        $date->addHours($temp[2]);
        $withTime = true;
      }
    } else if (strpos($pulsaRecurringType, Recurring::TYPE_WEEK) !== false) {
      $addDays = $temp[2] - 1;
      $date = $date->startOfWeek()->addDays($addDays);
      if (Carbon::now()->diffInDays($date, false) < 0 || Carbon::parse($lastDate)->startOfDay()->diffInDays($date) === 0) {
        $date->addDays(7);
      }
      if (isset($temp[3])) {
        $date->addHours($temp[3]);
        $withTime = true;
      }
    } else if (strpos($pulsaRecurringType, Recurring::TYPE_MONTH) !== false) {
      $date = $date->day($temp[2] == 'end' ? date('t') : $temp[2]);
      if (Carbon::now()->diffInDays($date, false) < 0 || Carbon::parse($lastDate)->startOfDay()->diffInDays($date) === 0) {
        $date->addMonthNoOverflow();
        if ($temp[2] == 'end') {
          $date->endOfMonth()->startOfDay();
        }
      }
      if (isset($temp[3])) {
        $date->addHours($temp[3]);
        $withTime = true;
      }
    }
    return $withTime ? $date->toDateTimeString() : $date->toDateString();
  }

  public function getEstimatedPrice($itemId, $storeId, $userId) {
    $price = 0;
    $pulsaOdeo = $this->pulsaOdeo->findById($itemId);

    $getMarketplacePrice = true;
    if ($storeId) {
      $prices = $this->priceGetter->getPriceForInventory($storeId, [
        'service_detail_id' => $pulsaOdeo->service_detail_id,
        'user_id' => $userId
      ]);
      if (!empty($prices) && isset($prices[$pulsaOdeo->id])) {
        $price = $prices[$pulsaOdeo->id]['sell_price'];
        $getMarketplacePrice = false;
      }
    }

    if ($getMarketplacePrice) {
      $serviceDetail = $this->serviceDetail->findById($pulsaOdeo->service_detail_id);
      $formatted = $this->marginFormatter->formatMargin($pulsaOdeo->price, ['service_detail' => $serviceDetail]);
      $price = $formatted['sale_price'];
    }

    return (int)$price;
  }

}

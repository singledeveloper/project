<?php

namespace Odeo\Domains\Order\PulsaRecurring;

use Carbon\Carbon;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class PulsaRecurringSelector implements SelectorListener {

  private $pulsaRecurring;

  public function __construct() {
    $this->pulsaRecurring = app()->make(\Odeo\Domains\Order\PulsaRecurring\Repository\PulsaRecurringRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output['pulsa_recurring_id'] = $item->id;
    $output['number'] = revertTelephone($item->destination_number);
    $output['name'] = $item->pulsa_odeos_name;
    $output['service_detail_id'] = $item->pulsa_odeos_service_detail_id;
    $output['type'] = Recurring::getTypeName($item->type);
    $output['store_name'] = isset($item->stores_name) ? $item->stores_name : 'Marketplace';
    $output['is_enabled'] = $item->is_enabled;
    $output['next_payment'] = $item->next_payment;
    $output['error_message'] = $item->error_message;
    $output['created_at'] = Carbon::parse($item->created_at)->toDateTimeString();
    $output['last_executed_at'] = isset($item->last_executed_at) ? Carbon::parse($item->last_executed_at)->toDateTimeString() : null;
    $output['expired_at'] = isset($item->expired_at) ? Carbon::parse($item->expired_at)->toDateString() : null;

    return $output;
  }

  public function get(PipelineListener $listener, $data) {

    $this->pulsaRecurring->normalizeFilters($data);

    $recurrings = [];

    foreach ($this->pulsaRecurring->getByUserId($data['auth']['user_id']) as $recurring) {
      $recurrings[] = $this->_transforms($recurring, $this->pulsaRecurring);
    }

    if (sizeof($recurrings) > 0) {
      return $listener->response(200, array_merge([
        "pulsa_recurrings" => $this->_extends($recurrings, $this->pulsaRecurring)
      ], $this->pulsaRecurring->getPagination()));
    }

    return $listener->response(204);
  }

  public function findById(PipelineListener $listener, $data) {
    $this->pulsaRecurring->normalizeFilters($data);

    $recurring = $this->pulsaRecurring->findDetailById($data['id']);

    if ($recurring)
      return $listener->response(200,
        $this->_transforms($recurring, $this->pulsaRecurring)
      );

    return $listener->response(204);

  }

}

<?php

namespace Odeo\Domains\Reminder\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Odeo\Domains\Core\Entity;

class BillReminder extends Entity {

  use SoftDeletes;

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}

<?php

namespace Odeo\Domains\Reminder;

use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\PostpaidType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Reminder\Repository\BillReminderRepository;

class BillReminderSelector implements SelectorListener {

  private $billReminders;

  public function __construct() {
    $this->billReminders = app()->make(BillReminderRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $billReminder = [];
    if ($item->id) $billReminder["id"] = $item->id;
    if ($item->name) $billReminder["name"] = $item->name;
    if ($item->number) $billReminder["number"] = $item->number;
    if ($item->date) $billReminder["date"] = $item->date;
    if ($item->service_detail_id) $billReminder["service_detail_id"] = $item->service_detail_id;
    if ($item->pulsa_odeo_id) $billReminder["pulsa_odeo_id"] = $item->pulsa_odeo_id;
    if ($item->pulsa_odeo_name) $billReminder["pulsa_odeo_name"] = $item->pulsa_odeo_name;
    if ($item->pulsa_odeo_type) $billReminder["pulsa_odeo_type"] = strtolower(PostpaidType::getConstKeyByValue($item->pulsa_odeo_type));
    if ($item->pulsa_odeo_logo) $billReminder["pulsa_odeo_logo"] = AssetAws::getAssetPath(AssetAws::PPOB_LOGO, $item->pulsa_odeo_logo);
    if ($item->next_reminder) $billReminder["next_reminder"] = $item->next_reminder;
    if ($item->expired_at) $billReminder["expired_at"] = $item->expired_at;
    if ($item->created_at) $billReminder["created_at"] = $item->created_at->format("Y-m-d H:i:s");
    if ($item->updated_at) $billReminder["updated_at"] = $item->updated_at->format("Y-m-d H:i:s");

    return $billReminder;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function getByUserId(PipelineListener $listener, $data) {
    $this->billReminders->normalizeFilters($data);

    $billReminders = [];
    foreach ($this->billReminders->getByUserId($data['auth']['user_id']) as $billReminder) {
      $billReminders[] = $this->_transforms($billReminder, $this->billReminders);
    }

    if (sizeof($billReminders) > 0) {
      return $listener->response(200, array_merge(
        ["billReminders" => $this->_extends($billReminders, $this->billReminders)],
        $this->billReminders->getPagination()
      ));
    }
    return $listener->response(204, ["billReminders" => []]);
  }

}

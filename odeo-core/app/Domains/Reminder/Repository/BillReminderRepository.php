<?php

namespace Odeo\Domains\Reminder\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Reminder\Model\BillReminder;

class BillReminderRepository extends Repository {

  public function __construct(BillReminder $billReminder) {
    $this->model = $billReminder;
  }

  function getByUserId($userId) {
    return $this->getResult($this->model
      ->select('bill_reminders.*', 'service_detail_id', 'pulsa_odeos.category as pulsa_odeo_type', 'pulsa_odeos.logo as pulsa_odeo_logo', 'pulsa_odeos.name as pulsa_odeo_name')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'bill_reminders.pulsa_odeo_id')
      ->where('user_id', $userId)
      ->orderBy('next_reminder'));
  }

  public function getBillReminderQueues($take = 10) {
    return $this->model
      ->select('bill_reminders.*', 'service_detail_id', 'pulsa_odeos.category as pulsa_odeo_type', 'pulsa_odeos.logo as pulsa_odeo_logo', 'pulsa_odeos.name as pulsa_odeo_name')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'bill_reminders.pulsa_odeo_id')
      ->where('next_reminder', '<=', date('Y-m-d'))
      ->take($take)
      ->get();
  }

  public function removeExpired() {
    return $this->model
      ->where('expired_at', '<=', date('Y-m-d'))
      ->delete();
  }
}

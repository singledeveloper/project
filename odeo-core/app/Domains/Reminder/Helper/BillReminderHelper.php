<?php

namespace Odeo\Domains\Reminder\Helper;

use Carbon\Carbon;

class BillReminderHelper {

  public function getNextReminder($dateSelected) {
    $lastDate = Carbon::now();

    $date = Carbon::now()->startOfDay()->day($dateSelected);
    if (Carbon::now()->diffInDays($date, false) < 0 || Carbon::parse($lastDate)->startOfDay()->diffInDays($date) === 0) {
      $date->addMonths(1);
    }

    return $date->toDateString();
  }

}

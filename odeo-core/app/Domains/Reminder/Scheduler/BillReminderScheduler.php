<?php
namespace Odeo\Domains\Reminder\Scheduler;

use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;

class BillReminderScheduler {

  private $billReminders;

  public function __construct() {
    $this->billReminders = app()->make(\Odeo\Domains\Reminder\Repository\BillReminderRepository::class);
    $this->billReminderHelper = app()->make(\Odeo\Domains\Reminder\Helper\BillReminderHelper::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
  }

  public function run() {
    if (date('H') >= 8 && date('H') < 23) {
      foreach($this->billReminders->getBillReminderQueues() as $billReminder) {
        $billReminder->last_reminded_at = date('Y-m-d H:i:s');
        $billReminder->next_reminder = $this->billReminderHelper->getNextReminder($billReminder->date);
        $this->billReminders->save($billReminder);

        $this->notifications->setup($billReminder->user_id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
        $this->notifications->billReminder($billReminder);

        dispatch($this->notifications->queue());
      }
    }
  }

}
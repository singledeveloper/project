<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 15/07/19
 * Time: 12.45
 */

namespace Odeo\Domains\Payment\Cimb\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Cimb\Model\PaymentCimbVaInquiry;

class PaymentCimbVaInquiryRepository extends Repository {

  function __construct(PaymentCimbVaInquiry $cimbVaInquiry) {
    $this->model = $cimbVaInquiry;
  }

  function findByVaCode($vaNumber) {
    return $this->model
      ->where('virtual_account_number', $vaNumber)
      ->whereDate('created_at', date('Y-m-d'))
      ->orderBy('id', 'DESC')
      ->first();
  }

}
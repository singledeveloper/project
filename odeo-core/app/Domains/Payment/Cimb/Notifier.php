<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/18/17
 * Time: 4:42 PM
 */

namespace Odeo\Domains\Payment\Cimb;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Cimb\Helper\CimbHelper;
use Odeo\Domains\Payment\Cimb\Helper\CimbManager;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;

class Notifier extends CimbManager {

  private $redis, $paymentChannel;
  public function __construct() {
    parent::__construct();
    $this->redis = Redis::connection();
    $this->paymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
  }

  public function vaPayment(PipelineListener $listener, $data) {

    $responseCode = CimbHelper::validatePaymentData($data);

    if ($responseCode != '00') {
      return $listener->response(400, ['cimb_error_code' => $responseCode]);
    }

    $vaNumber = $data['CompanyCode'] . $data['CustomerKey1'];

    if (!$inquiry = $this->cimbVaInquiry->findByVaCode($vaNumber)) {
      return $listener->response(400, ['cimb_error_code' => 32]);
    }

    if ($this->cimbVaPayments->findByVaCodeAndInquiryId($data['CompanyCode'], $data['CustomerKey1'], $inquiry->id)) {
      return $listener->response(400, ['cimb_error_code' => 41]);
    }

    if ($inquiry->amount != $data['Amount']) {
      return $listener->response(400, ['cimb_error_code' => 46]);
    }

    if ($this->isDuplicateTransaction($inquiry->id)) {
      return $listener->response(400, 'duplicate transaction');
    }

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['CompanyCode'] . $data['CustomerKey1']);
    if (!$pgUserChannel) {
      return $listener->response(400, 'payment.unrecognized_va_number');
    }

    $channel = $this->paymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, 'channel not found');
    }

    $vaPayment = $this->cimbVaPayments->getNew();
    $vaPayment->transaction_id = $data['TransactionID'];
    $vaPayment->channel_id = $data['ChannelID'];
    $vaPayment->terminal_id = $data['TerminalID'];
    $vaPayment->transaction_date = $data['TransactionDate'];
    $vaPayment->company_code = $data['CompanyCode'];
    $vaPayment->customer_key_1 = $data['CustomerKey1'];
    $vaPayment->customer_key_2 = $data['CustomerKey2'];
    $vaPayment->customer_key_3 = $data['CustomerKey3'];
    $vaPayment->language = $data['Language'];
    $vaPayment->currency = $data['Currency'];
    $vaPayment->amount = $data['Amount'];
    $vaPayment->fee = $data['Fee'];
    $vaPayment->paid_amount = $data['PaidAmount'];
    $vaPayment->reference_number_transaction = $data['ReferenceNumberTransaction'];
    $vaPayment->flag_payment_list = $data['FlagPaymentList'];
    $vaPayment->customer_name = $data['CustomerName'];
    $vaPayment->additional_data_1 = $data['AdditionalData1'] ?? '';
    $vaPayment->additional_data_2 = $data['AdditionalData2'] ?? '';
    $vaPayment->additional_data_3 = $data['AdditionalData3'] ?? '';
    $vaPayment->additional_data_4 = $data['AdditionalData4'] ?? '';

    $vaPayment->flag_payment = CimbHelper::FLAG_OPEN_PAYMENT;
    $vaPayment->response_code = '90';
    $vaPayment->response_description = 'Pending';

    $vaPayment->virtual_account_number = $vaNumber;
    $vaPayment->status = PaymentGateway::PENDING;
    $vaPayment->inquiry_id = $inquiry->id;
    $vaPayment->cost = $pgUserChannel->cost;
    
    $this->cimbVaPayments->save($vaPayment);

    $payment = $this->payments->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
    $payment->info_id = $pgUserChannel->payment_group_id;
    $payment->opc = $channel->code;
    $payment->vendor_id = $vaPayment->id;
    $payment->vendor_type = 'va_cimb';
    $this->payments->save($payment);

    return $listener->response(200, [
      'cimb_payment_id' => $vaPayment->id,
      'user_id' => $pgUserChannel->user_id,
      'payment_id' => $payment->id,
      'va_code' => $vaPayment->virtual_account_number,
      'va_prefix' => substr($vaNumber, 0, 6),
      'billed_amount' => $vaPayment->paid_amount,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'customer_name' => $vaPayment->customer_name,
      'vendor_reference_id' => $vaPayment->reference_number_transaction
    ]);

  }

  public function updatePayment(PipelineListener $listener, $data) {

    $vaPayment = $this->cimbVaPayments->findById($data['cimb_payment_id']);
    $vaPayment->response_code = '00';
    $vaPayment->response_description = 'Success';

    $xmlResponse = CimbHelper::generateResponseXml([
      'TransactionID' => $vaPayment->tranaction_id,
      'ChannelID' => $vaPayment->channel_id,
      'TerminalID' => $vaPayment->terminal_id,
      'TransactionDate' => $vaPayment->transaction_date,
      'CompanyCode' => $vaPayment->company_code,
      'CustomerKey1' => $vaPayment->customer_key_1,
      'CustomerKey2' => $vaPayment->customer_key_2,
      'CustomerKey3' => $vaPayment->customer_key_3,
      'PaymentFlag' => $vaPayment->flag_payment_list,
      'CustomerName' => $vaPayment->customer_name,
      'Currency' => $vaPayment->currency,
      'Amount' => $vaPayment->amount,
      'Fee' => $vaPayment->fee,
      'PaidAmount' => $vaPayment->paid_amount,
      'ReferenceNumberTransaction' => $vaPayment->reference_number_transaction,
      'AdditionalData1' => $vaPayment->additional_data_1,
      'AdditionalData2' => $vaPayment->additional_data_2,
      'AdditionalData3' => $vaPayment->additional_data_3,
      'AdditionalData4' => $vaPayment->additional_data_4,
      'ResponseCode' => $vaPayment->response_code,
      'ResponseDescription' => 'Success'
    ], CimbHelper::SERVICE_PAYMENT);

    $vaPayment->payment_id = $data['payment_id'];
    $vaPayment->status = $data['pg_payment_status'];
    $this->cimbVaPayments->save($vaPayment);

    return $listener->response(200, ['xml_response' => $xmlResponse]);
  }

  private function getRedisNamespace($date) {
    return "odeo_core:cimb_va_used_inquiry_id_$date";
  }

  private function isDuplicateTransaction($inquiryId) {
    $currDate = Carbon::now()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($currDate);

    if (!$this->redis->hsetnx($namespace, $inquiryId, 1)) {
      return true;
    }

    $this->redis->expire($namespace, 2 * 24 * 60 * 60);
    $tomorrowDate = Carbon::tomorrow()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($tomorrowDate);
    if (!$this->redis->hset($namespace, $inquiryId, 1)) {
      return true;
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    return false;
  }
}
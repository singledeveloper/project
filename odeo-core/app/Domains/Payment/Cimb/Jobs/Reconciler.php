<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/19
 * Time: 14.09
 */

namespace Odeo\Domains\Payment\Cimb\Jobs;


use Carbon\Carbon;
use Odeo\Domains\Payment\Cimb\Repository\PaymentCimbVaPaymentRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;
use Odeo\Jobs\Job;

class Reconciler extends Job {

  private $data, $vaPayment, $pgPayment, $cimbInquiry;

  function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->vaPayment = app()->make(PaymentCimbVaPaymentRepository::class);
    $this->pgPayment = app()->make(PaymentGatewayPaymentRepository::class);
    $this->cimbInquiry = app()->make(BankCimbInquiryRepository::class);
  }

  public function handle() {
    \DB::transaction(function() {
      $bankInquiry = $this->cimbInquiry->findById($this->data['bank_inquiry_id']);

      if (isset($bankInquiry->reference_type) && $bankInquiry->reference_type != 'note') return;

      $bankInquiry->reference_type = 'payment_gateway_payment_id';
      $bankInquiry->reference = json_encode(["" . $this->data['pg_payment_id']]);
      $this->cimbInquiry->save($bankInquiry);

      $pgPayment = $this->pgPayment->findById($this->data['pg_payment_id']);
      $pgPayment->reconciled_at = Carbon::now();
      $this->pgPayment->save($pgPayment);

      $vaPayment = $this->vaPayment->findById($this->data['va_payment_id']);
      $vaPayment->is_reconciled = true;
      $this->vaPayment->save($vaPayment);
    });
  }

}
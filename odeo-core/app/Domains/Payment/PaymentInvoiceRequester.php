<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/10/19
 * Time: 16.26
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;

class PaymentInvoiceRequester {

  public function __construct() {
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
    $this->orderChargeHelper = app()->make(\Odeo\Domains\Order\Helper\OrderCharger::class);
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository::class);
    $this->payments = app()->make(PaymentRepository::class);
    $this->paymentInformations = app()->make(PaymentOdeoPaymentChannelRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    clog('invoice_va', 'request payment invoice');
    $virtualAccount = $this->virtualAccounts->findById($data['virtual_account_id']);
    $order = $this->orders->findById($data['order_id']);

    $virtualAccount->order_id = $order->id;
    $this->virtualAccounts->save($virtualAccount);

    if ($order->status == OrderStatus::CREATED) {

      if (!$paymentInformation = $this->paymentInformations->findById($data['opc'])) {
        return $listener->response(400, "OPC is not valid.");
      }

      if ($payment = $this->payments->findByOrderId($order->id)) {
        $payment->reference_id = null;
      } else {
        $payment = $this->payments->getNew();
        $payment->order_id = $order->id;
      }

      $payment->opc = $paymentInformation->code;
      $payment->info_id = $paymentInformation->info_id;

      $this->payments->save($payment);

      if ($data['charge_fee_to_customer']) {
        $order->total = $order->total + $data['fee'];
      }
      $this->orderChargeHelper->charge($order->id, OrderCharge::BILLER_FEE, $data['fee'], OrderCharge::GROUP_TYPE_COMPANY_PROFIT);

      $order->status = OrderStatus::OPENED;
      $order->opened_at = date('Y-m-d H:i:s');

      $this->orders->save($order);
    }

    return $listener->response(200, [
      'customer_name' => $order->name,
      'total' => $order->total
    ]);
  }

}
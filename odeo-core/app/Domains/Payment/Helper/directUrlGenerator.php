<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 3:32 PM
 */

namespace Odeo\Domains\Payment\Helper;


trait directUrlGenerator {

  function decodeIdt($idt) {
    $paymentDirectUrls = app()->make(\Odeo\Domains\Payment\Repository\PaymentDirectUrlRepository::class);

    return json_decode($paymentDirectUrls->findById(base64_decode($idt))->body, true);
  }

  function generateUrl($body) {

    $paymentDirectUrl = app()->make(\Odeo\Domains\Payment\Repository\PaymentDirectUrlRepository::class);

    $secured = app()->environment() != 'local' ? true : false;

    if ($directUrl = $paymentDirectUrl->getExisting([
      'reference_id' => $body['reference_id'],
      'opc_group' => $body['opc_group']
    ])->first()
    ) {
      return baseUrl('v1/payment/direct/pay' . '?idt=' . base64_encode($directUrl->id));
    }

    $directUrl = $paymentDirectUrl->getNew();

    $directUrl->reference_id = $body['reference_id'];
    $directUrl->opc_group = $body['opc_group'];
    $directUrl->body = json_encode($body);

    $paymentDirectUrl->save($directUrl);

    return baseUrl('v1/payment/direct/pay?idt=' . base64_encode($directUrl->id));

  }


  public function getUrlData($referenceId, $opc) {

    $paymentUrls = app()->make(\Odeo\Domains\Payment\Repository\PaymentDirectUrlRepository::class);

    return json_decode($paymentUrls->getExisting([
      'reference_id' => $referenceId,
      'opc_group' => $opc
    ])->first()->body, true);

  }

}
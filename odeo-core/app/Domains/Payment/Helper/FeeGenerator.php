<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 6:20 PM
 */

namespace Odeo\Domains\Payment\Helper;


class FeeGenerator {

  const FLOAT_UP_VAL = .004;

  public function getFee($percentage, $total) {
    return ceil(rtrim(rtrim(sprintf('%f', (100 / (100 - $percentage) * $total) - $total), '0'), '.'));
  }

  public function getFeeWithPrecision($percentage, $total, $precision = 2) {
    return round(rtrim(rtrim(sprintf('%f', (100 / (100 - $percentage) * $total) - $total) + self::FLOAT_UP_VAL, '0'), '.'), $precision, PHP_ROUND_HALF_UP);
  }

}
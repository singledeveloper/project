<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/10/19
 * Time: 16.53
 */

namespace Odeo\Domains\Payment\Helper;


class PaymentInvoiceHelper {

  const INVOICE_NOT_FOUND = 9999;
  const INVOICE_INVALID = 3002;
  const INVOICE_INVALID_AMOUNT = 3003;
  const INVOICE_ALREADY_PAID = 3001;
  const INVALID_SIGNATURE = 3004;

}
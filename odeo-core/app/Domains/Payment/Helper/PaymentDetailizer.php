<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/29/16
 * Time: 10:05 PM
 */

namespace Odeo\Domains\Payment\Helper;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoTnc;

class PaymentDetailizer {

  private $feeGenerator, $currencyHelper, $paymentValidator;

  public function __construct() {

    $this->feeGenerator = app()->make(FeeGenerator::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->paymentValidator = app()->make(PaymentValidator::class);
  }


  public function detailize($order, $payment) {

    if (isset($payment['detail'])) {
      $payment['detail'] = \json_decode($payment['detail']);
    }

    if ($this->paymentValidator->isCcInstallment($payment['opc_group'])) {

      foreach ($payment['detail'] as $c) {

        foreach ($c->available_installment as &$atl) {

          $fee = $this->feeGenerator->getFee($atl->mdr, $order->total);

          $cost = $this->currencyHelper->formatPrice(($fee + $order->total) / $atl->month);

          $atl->formatted_text = $cost['formatted_amount'] . " x " . $atl->month;
          $atl->total = $this->currencyHelper->formatPrice($fee + $order->total);
          unset($atl->month);
          unset($atl->promo_id);
          unset($atl->mdr);
        }
        unset($c->payment_type);
        unset($c->installment_acquirer);
      }

    } else if ($this->paymentValidator->isCc($payment['opc_group'])) {

      $fee = $this->feeGenerator->getFee(2.6, $order->total) + 1500;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isAtmTransferVa($payment['opc_group'])) {
      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice(4500),
        'total' => $this->currencyHelper->formatPrice($order->total + 4500)
      ];
    } else if ($this->paymentValidator->isAlfa($payment['opc_group'])) {
      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice(5000),
        'total' => $this->currencyHelper->formatPrice($order->total + 5000)
      ];
    } else if ($this->paymentValidator->isDokuWallet($payment['opc_group'])) {

      $fee = $this->feeGenerator->getFee(2, $order->total);

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isKredivo($payment['opc_group'])) {

      $mdr = 2.3;

      if (in_array($order->gateway_id, [
        Payment::ANDROID_OCASH, Payment::WEB_APP_OCASH,
        Payment::ANDROID_ODEPOSIT, Payment::WEB_APP_ODEPOSIT
      ])) {
        $mdr = 2.9;
      }

      $fee = $this->feeGenerator->getFee($mdr, $order->total);

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee),
        'tnc' => app()
          ->make(KredivoTnc::class)
          ->getTnc()
      ];
    } else if ($this->paymentValidator->isMandiriECash($payment['opc_group'])) {

      if ($order->total > 250000) $fee = $this->feeGenerator->getFee(1, $order->total);
      else {
        $fee = 2500;
      }

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isMandiriClickPay($payment['opc_group'])) {

      $fee = 5000 + 1500;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isMandiriMpt($payment['opc_group'])) {

      $fee = 2000;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isDebitOnlineBni($payment['opc_group'])) {

      $fee = $this->feeGenerator->getFee(2.6, $order->total);

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isMuamalatInternetBanking($payment['opc_group'])) {

      $fee = 4500;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isBriEpay($payment['opc_group'])) {

      $fee = 5000 + 1500;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isDanamonInternetBanking($payment['opc_group'])) {

      $fee = 4500;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isCimbClick($payment['opc_group'])) {

      $fee = 5000;

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    } else if ($this->paymentValidator->isAkulaku($payment['opc_group'])) {

      $fee = $this->feeGenerator->getFee(1.5, $order->total);

      $payment['detail'] = [
        'fee' => $this->currencyHelper->formatPrice($fee),
        'total' => $this->currencyHelper->formatPrice($order->total + $fee)
      ];

    }


    return $payment;

  }
}

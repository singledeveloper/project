<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:08 PM
 */

namespace Odeo\Domains\Payment;

use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class PaymentListSelector implements SelectorListener {


  private $paymentChannels, $paymentDetailizer, $orders;

  public function __construct() {
    $this->paymentChannels = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->paymentDetailizer = app()->make(\Odeo\Domains\Payment\Helper\PaymentDetailizer::class);
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $output = [
      'opc' => $item['code'],
      'opc_group' => $item['id'],
      'name' => $item['name']
    ];
    if (isset($item['logo'])) $output['logo'] = AssetAws::getAssetPath(AssetAws::PAYMENT, $item['logo']);
    if (isset($item['detail'])) $output['detail'] = $item['detail'];

    return $output;
  }

  public function getEnabled(PipelineListener $listener, $data) {

    $this->paymentChannels->normalizeFilters($data);

    $paymentList = [];

    if (!isset($data['gateway_id'])) {
      $order = $this->orders->findById($data['order_id']);
  
      foreach ($this->paymentChannels->getEnabled($order->gateway_id) as $item) {
        $temp = $this->_transforms($item, $this->paymentChannels);
  
        $paymentList[] = $this->paymentDetailizer->detailize($order, $temp);
      }
    } else {
      foreach ($this->paymentChannels->getEnabled($data['gateway_id']) as $item) {
        $paymentList['channelList'][] = $this->_transforms($item, $this->paymentChannels);
      }
    }


    if (count($paymentList) > 0) {
      return $listener->response(200,
        $this->_extends($paymentList, $this->paymentChannels));
    }
    return $listener->response(204);

  }

  public function getGroupedEnabled(PipelineListener $listener, $data) {

    $this->paymentChannels->normalizeFilters($data);

    $order = $this->orders->findById($data['order_id']);

    $paymentList = [
      "defaults" => [],
      "wallet" => [],
    ];

    foreach ($this->paymentChannels->getEnabled($order->gateway_id) as $paymentChannel) {

      // Get the group and detailize payment channel
      $temp = $this->_transforms($paymentChannel, $this->paymentChannels);
      $paymentMethodGroup = Payment::getPaymentMethodGroup($temp['opc_group']);
      $paymentMethodDetailed = $this->paymentDetailizer->detailize($order, $temp);

      // Set default payment channel to kredivo and bank transfer bri
      if ($temp['opc_group'] === Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI
         || $temp['opc_group'] === Payment::OPC_GROUP_BANK_TRANSFER_BCA
      ) {
        $paymentList["defaults"][] = $paymentMethodDetailed;
      }

      // Separate between wallet and non-wallet payment
      if ($paymentMethodGroup['id'] === Payment::PAYMENT_METHOD_GROUP_EMONEY) {
        $paymentList['wallet'][] = $paymentMethodDetailed;
      } else {
        // Store the non-wallet payment channel to the corresponding group
        $paymentMethodGroupName = $paymentMethodGroup['name'];
        if (!isset($paymentList[$paymentMethodGroupName])) {
          $paymentList[$paymentMethodGroupName] = [];
        }
        $paymentList[$paymentMethodGroupName][] = $paymentMethodDetailed;
      }
    }

    if (count($paymentList) > 0) {
      return $listener->response(200,
        $this->_extends($paymentList, $this->paymentChannels));
    }
    return $listener->response(204);

  }

}

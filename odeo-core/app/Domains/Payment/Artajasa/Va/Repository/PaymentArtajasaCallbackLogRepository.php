<?php

namespace Odeo\Domains\Payment\Artajasa\Va\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Artajasa\Va\Model\PaymentArtajasaCallbackLog;

class PaymentArtajasaCallbackLogRepository extends Repository {

  function __construct(PaymentArtajasaCallbackLog $paymentArtajasaCallbackLog) {
    $this->model = $paymentArtajasaCallbackLog;
  }

}

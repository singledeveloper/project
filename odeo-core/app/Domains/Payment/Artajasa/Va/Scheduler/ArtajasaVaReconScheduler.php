<?php

namespace Odeo\Domains\Payment\Artajasa\Va\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Activity\Helper\HolidayHelper;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaPaymentRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementDetailRepository;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayReconMismatchAlert;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;

class ArtajasaVaReconScheduler {

  private $mandiriRepo, $vaPayment;

  private function init() {
    $this->mandiriRepo = app()->make(BankMandiriGiroInquiryRepository::class);
    $this->vaPayment = app()->make(PaymentArtajasaVaPaymentRepository::class);
    $this->paymentSettlement = app()->make(PaymentSettlementRepository::class);
    $this->paymentSettlementDetail = app()->make(PaymentSettlementDetailRepository::class);
    $this->paymentGatewayPayment = app()->make(PaymentGatewayPaymentRepository::class);
    $this->holidayHelper = app()->make(HolidayHelper::class);
  }

  public function run() {
    $this->init();
    
    $this->reconSettlement();
  }

  private function reconSettlement() {
    $settlements = $this->mandiriRepo->getUnreconciledArtajasaSettlement();
    $payments = $this->vaPayment->getUnsettled();

    $updatedAt = Carbon::now();
    
    $paymentSettlementDetails = [];
    $paymentUpdate = [];
    $paymentGatewayPaymentUpdate = [];
    $settlementUpdates = [];

    $totalPayment = [];
    $groupedPayment = [];

    foreach ($payments as $payment) {
      if (!array_key_exists($payment->date, $totalPayment)) {
        $totalPayment[$payment->date] = 0;
        $groupedPayment[$payment->date] = [];
      }
      $totalPayment[$payment->date] += $payment->amount - $payment->cost;
      $groupedPayment[$payment->date][] = $payment;
    }

    $lastReconDate = '';
    $lastFailedReconDate = '';

    foreach ($settlements as $settlement) {  
      clog('artajasa_recon', 'processing settlement ' . $settlement->date . ' ' . $settlement->credit);
      
      if ($settlement->date == $lastReconDate) {
        clog('artajasa_recon', 'previous recon for date ' . $settlement->date . ' successful');
        continue;
      }

      if ($settlement->date != $lastFailedReconDate) {
        if ($lastReconDate != $settlement->date && !empty($lastFailedReconDate) && $settledAmount > 0) {
          dispatch(new SendPaymentGatewayReconMismatchAlert([
            'date' => $lastFailedReconDate,
            'vendor' => 'Artajasa VA',
            'description' => '',
            'settlement_amount' => $settledAmount,
            'total_amount' => $totalAmount
          ]));
        }

        $settledAmount = 0;
        $tempPaymentDate = [];
  
        foreach ($this->getSettlementDates(Carbon::parse($settlement->date), 1) as $date) {
          if (isset($totalPayment[$date])) {
            $settledAmount += $totalPayment[$date];
          }
          $tempPaymentDate[] = $date;
        }
      }

      $totalAmount = $settlement->credit;

      if ($settledAmount != $totalAmount) {
        clog('artajasa_recon', $settlement->date . ' settled amount is: ' . $totalAmount . ' but got: ' . $settledAmount);
        $lastFailedReconDate = $settlement->date;
        continue;
      }
      
      $lastReconDate = $settlement->date;
      $lastFailedReconDate = '';

      $tempPaymentSettlementDetail = [];
      $tempPaymentUpdate = [];
      $tempPaymentGatewayPaymentUpdate = [];

      clog('artajasa_recon', $settlement->date . ' settled amount is: ' . $totalAmount);

      foreach ($tempPaymentDate as $date) {
        if (!isset($groupedPayment[$date])) continue;
        
        foreach ($groupedPayment[$date] as $payment) {
          $tempPaymentSettlementDetail[] = [
            'source_id' => $payment->payment->source_id,
            'source_type' => PaymentChannel::PAYMENT_GATEWAY,
            'vendor_id' => $payment->id,
            'vendor_type' => 'artajasa_va_close',
            'amount' => $payment->amount - $payment->cost
          ];
          $tempPaymentUpdate[] = [
            'id' => $payment->id,
            'is_reconciled' => true,
            'updated_at' => $updatedAt
          ];
          $tempPaymentGatewayPaymentUpdate[] = [
            'id' => $payment->payment->source_id,
            'reconciled_at' => Carbon::now()
          ];
        }
      }

      $paymentSettlement = $this->paymentSettlement->findByAttributes([
        'bank_id' => Bank::MANDIRI,
        'bank_inquiry_id' => $settlement->id
      ]);

      if (!$paymentSettlement) {
        $paymentSettlement = $this->paymentSettlement->getNew();
        $paymentSettlement->bank_id = Bank::MANDIRI;
        $paymentSettlement->bank_inquiry_id = $settlement->id;
        $paymentSettlement->settlement_amount = $totalAmount;
        $paymentSettlement->settlement_date = $settlement->date_value;
        $paymentSettlement->settlement_description = $settlement->description;
        $paymentSettlement->save();
      }

      array_walk($tempPaymentSettlementDetail, function(&$detail, $key, $paymentSettlement) {
        $detail['payment_settlement_id'] = $paymentSettlement->id;
      }, $paymentSettlement);
      $paymentSettlementDetails = array_merge($tempPaymentSettlementDetail, $paymentSettlementDetails);
      $paymentUpdate = array_merge($tempPaymentUpdate, $paymentUpdate);
      $paymentGatewayPaymentUpdate = array_merge($tempPaymentGatewayPaymentUpdate, $paymentGatewayPaymentUpdate);

      $settlementUpdates[] = [
        'id' => $settlement->id,
        'reference_type' => BankTransferInquiries::REFERENCE_TYPE_SETTLEMENT_ID,
        'reference' => json_encode(["" . $paymentSettlement->id]),
        'updated_at' => $updatedAt
      ];
    }

    count($paymentSettlementDetails) && $this->paymentSettlementDetail->saveBulk($paymentSettlementDetails);
    count($settlementUpdates) && $this->mandiriRepo->updateBulk($settlementUpdates);
    count($paymentUpdate) && $this->vaPayment->updateBulk($paymentUpdate);
    count($paymentGatewayPaymentUpdate) && $this->paymentGatewayPayment->updateBulk($paymentGatewayPaymentUpdate);
  }

  private function getSettlementDates($startDate, $settlementDays) {
    $settlementDates = array();

    $endDate = $this->holidayHelper->subWorkingDays($startDate->copy(), $settlementDays);
    $settlementDates[] = $endDate->format('Y-m-d');
    
    $dateDiff = $endDate->diffInDays($startDate);
    if ($dateDiff > $settlementDays) {
      for ($i = 1; $i < $dateDiff; $i++) {
        $date = $endDate->copy()->addDays($i);

        if ($this->holidayHelper->addWorkingDays($date, $settlementDays)->format('Y-m-d') == $startDate->format('Y-m-d')) {
          $settlementDates[] = $date->format('Y-m-d');
        } else {
          break;
        }
      }
    }
    
    return $settlementDates;
  }
}
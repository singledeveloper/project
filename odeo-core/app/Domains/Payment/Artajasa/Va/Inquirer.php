<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/08/19
 * Time: 18.22
 */

namespace Odeo\Domains\Payment\Artajasa\Va;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Artajasa\Helper\ArtajasaApi;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Inquirer {

  private $pgUserPaymentChannel, $odeoPaymentChannel, $ajVaInquiry;

  function __construct() {
    $this->ajVaInquiry = app()->make(PaymentArtajasaVaInquiryRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->odeoPaymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
  }

  public function vaInquiry(PipelineListener $listener, $data) {
    try {
      ArtajasaApi::validateRequest($data, ArtajasaApi::REQ_INQUIRY);
    } catch (\Exception $e) {
      clog('artajasa_va', $e->getMessage());
      return $listener->response(400, [
        'message' => $e->getMessage(),
        'code' => '01'
      ]);
    }

    try {
      if (!$pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['vaid'])) {
        clog('artajasa_va', trans('payment.unrecognized_va_number'));
        return $listener->response(400, [
          'message' => trans('payment.unrecognized_va_number'),
          'code' => '76'
        ]);
      }

      if (PaymentGateway::exceedVaCodeLength($data['vaid'], $pgUserChannel->max_length)) {
        return $listener->response(400, [
          'message' => trans('payment.va_max_length_exceeded'),
          'code' => '76'
        ]);
      }

      if (!$channel = $this->odeoPaymentChannel->findByGatewayidAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id)) {
        clog('artajasa_va', 'channel not found');
        return $listener->response(400, [
          'message' => 'channel not found',
          'code' => '76'
        ]);
      }

    } catch (\Exception $e) {
      return $listener->response(400, [
        'message' => $e->getMessage(),
        'code' => '91'
      ]);
    }

    $refNumber = is_array($data['reference_number']) ? $data['reference_number'][0] : $data['reference_number'];

    return $listener->response(200, [
      'va_code' => $data['vaid'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'vendor_reference_id' => $this->generateBookingId(),
    ]);
  }

  public function createInquiry(PipelineListener $listener, $data) {
    $refNumber = is_array($data['reference_number']) ? $data['reference_number'][0] : $data['reference_number'];

    $inquiry = $this->ajVaInquiry->getNew();
    $inquiry->va_id = $data['vaid'];
    $inquiry->booking_datetime = $data['booking_datetime'];
    $inquiry->reference_number = empty(str_replace(' ', '', $refNumber)) ? NULL : str_replace(' ', '', $refNumber);
    $inquiry->username = $data['username'];
    $inquiry->booking_id = $data['vendor_reference_id'];
    $inquiry->customer_name = $data['customer_name'];
    $inquiry->display_text = $data['display_text'];
    $inquiry->product_id = $data['item_name'];
    $inquiry->min_amount = $data['amount'];
    $inquiry->max_amount = $data['amount'];
    $this->ajVaInquiry->save($inquiry);

    return $listener->response(200, [
      'type' => ArtajasaApi::RES_INQUIRY,
      'ack' => '00',
      'bookingid' => $inquiry->booking_id,
      'customer_name' => $data['customer_name'],
      'min_amount' => $data['amount'],
      'max_amount' => $data['amount'],
      'productid' => $data['item_name'],
      'signature' => ArtajasaApi::getSignature()
    ]);
  }

  private function generateBookingId() {
    return time() . rand(10000, 99999);
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 15/08/19
 * Time: 12.57
 */

namespace Odeo\Domains\Payment\Artajasa\Va\Job;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaVaPaymentStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Artajasa\Helper\ArtajasaApi;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaVaStatusRepository;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Jobs\Job;

class InquiryStatus extends Job {

  private $vaPayment, $redis, $ajVaStatus;

  public function __construct($vaPayment) {
    parent::__construct();
    $this->vaPayment = $vaPayment;
  }

  private function init() {
    $this->ajVaStatus = app()->make(PaymentArtajasaVaStatusRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $this->init();
    $key = 'odeo_core:artajasa_change_status_' . $this->vaPayment->id;

    if (!$this->redis->setnx($key, 1)) {
      \Log::notice('Inquiry Status locked aj va payment ' . $this->vaPayment->id);
      return null;
    }
    $this->redis->expire($key, 5 * 60);

    $data = [
      'type' => ArtajasaApi::REQ_TRX_STATUS,
      'item01' => [
        'vaid' => $this->vaPayment->inquiry->va_id,
        'bookingid' => $this->vaPayment->booking_id,
        'booking_datetime' => $this->vaPayment->inquiry->booking_datetime->format('Y-m-d H:i:s'),
        'username' => ArtajasaApi::getUserName(),
        'signature' => ArtajasaApi::getSignature()
      ]
    ];

    $xml = new \SimpleXMLElement('<notification/>');
    $this->parseXml($data, $xml);
    $body = $xml->asXML();

    $client = new Client();
    $request = new Request('POST', env('AJ_VIRTUAL_ACCOUNT_CHECK_STATUS_URL'), [
      'Content-Type' => 'application/xml'
    ], $body);

    try {
      $response = $client->send($request);
      $responseContent = $response->getBody()->getContents();
    } catch (RequestException $e) {
      $errorMessage = $e->getMessage();
      clog('artajasa_va', $errorMessage);
      $response = $e->getResponse();
      $responseContent = '';
    }

    $requestDump = $this->getDump($request->getHeaders(), $body);
    $responseDump = $this->getDump($response->getHeaders(), $responseContent);

    $responseData = simplexml_load_string($responseContent);
    $responseData = json_decode(json_encode($responseData), true);

    $status = $responseData['item01']['status'];

    $vaStatus = $this->ajVaStatus->getNew();
    $vaStatus->artajasa_payment_id = $this->vaPayment->id;
    $vaStatus->request_dump = $requestDump;
    $vaStatus->response_dump = $responseDump;
    $vaStatus->status_code = $response->getStatusCode();
    $this->ajVaStatus->save($vaStatus);

    switch ($status) {
      case '00':
        $newStatus = ArtajasaVaPaymentStatus::SUCCESS;
        break;
      case '05':
        $newStatus = ArtajasaVaPaymentStatus::FAILED;
        break;
      case '300':
        $newStatus = ArtajasaVaPaymentStatus::SUSPECT_NOT_FOUND_ON_AJ;
        break;
      case '101':
      default:
        $newStatus = ArtajasaVaPaymentStatus::SUSPECT;
    }

    $oldStatus = $this->vaPayment->status;

    // temp fix for unsync booking date
    if ($newStatus == ArtajasaVaPaymentStatus::SUSPECT_NOT_FOUND_ON_AJ) {
      $inquiry = $this->vaPayment->inquiry;
      $inquiry->booking_datetime = $inquiry->booking_datetime->addSecond();
      $inquiry->save();
    }
    
    if ($newStatus != $oldStatus) {
      $this->vaPayment->status = $newStatus;
      $this->vaPayment->save();

      $pipeline = new Pipeline;
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
      $pipeline->execute([
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'pg_payment_status' => ArtajasaVaPaymentStatus::getPaymentGatewayStatus($newStatus),
        'receive_notify_time' => Carbon::now(),
        'vendor_reference_id' => $this->vaPayment->booking_id
      ]);

      if ($pipeline->fail()) {
        \Log::info('Inquiry Status Fail Notify: ' . $pipeline->errorMessage);
        return null;
      }
    }
    
    $this->redis->del($key);
  }

  public function parseXml($data, &$xml) {
    foreach ($data as $key => $val) {
      if (is_array($val)) {
        $this->parseXml($val, $xml->$key);
        continue;
      }
      $xml->$key = $val;
    }
  }

  private function getDump($headers, $stringBody) {
    $dump = '';
    foreach ($headers as $key => $val) {
      $dump = $dump.$key.': '.$val[0]."\n";
    }
    return $dump."\n".$stringBody;
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:37 PM
 */

namespace Odeo\Domains\Payment\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\Payment;
use Odeo\Domains\Constant\OrderStatus;

class PaymentRepository extends Repository {

  public function __construct(Payment $payment) {
    $this->model = $payment;
  }

  public function findByOrderId($order_id) {
    return $this->model->where('order_id', $order_id)->first();
  }

  public function findIdentical($data) {
    $query = $this->getCloneModel();

    if (isset($data['order_id'])) $query = $query->where('order_id', $data['order_id']);
    if (isset($data['opc'])) $query = $query->where('opc', $data['opc']);

    return $query;
  }

  public function findByReferenceId($id, $infoId) {
    return $this->model->where('reference_id', $id)
      ->where('info_id', $infoId)
      ->first();

  }

  public function salesTodayByOpcId($opcId) {
    return $this->salesLastNDayByOpcId(0, $opcId);
  }

  public function salesLastNDayByOpcId($day, $opcId) {

    $query = $this->getCloneModel();

    $dateFrom = Carbon::today()->subDays($day);
    $dateTo = $dateFrom->copy()->endOfDay();

    return $query
      ->join('orders', 'orders.id', '=', 'payments.order_id')
      ->where('info_id', $opcId)
      ->where('payments.created_at', '>=', $dateFrom)
      ->where('payments.created_at', '<=', $dateTo)
      ->where('orders.status', OrderStatus::COMPLETED)
      ->select(
        \DB::raw('count(payments.id) as total_count'),
        \DB::raw('sum(orders.total) as total_order')
      )->first();

  }

  public function findByPgPaymentId($pgPaymentId) {
    return $this->model
      ->where('source_type', PaymentChannel::PAYMENT_GATEWAY)
      ->where('source_id', $pgPaymentId)
      ->first();
  }

}

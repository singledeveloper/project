<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 9:37 PM
 */

namespace Odeo\Domains\Payment\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Model\PaymentUserDirectPayment;


class PaymentUserDirectPaymentRepository extends Repository {

  public function __construct(PaymentUserDirectPayment $directPayment) {
    $this->model = $directPayment;
  }

  public function findByUserId($userId) {
    return $this->model
      ->where('user_id', $userId)
      ->get();
  }

  public function getSelectedDirectPayment($userId, $type) {
    return $this->model
      ->join('users', 'users.id', '=', 'payment_user_direct_payments.user_id')
      ->where('users.direct_payment_reference_id', '=', \DB::raw('payment_user_direct_payments.id'))
      ->where('direct_payment_reference_type', $type)
      ->where('users.id', $userId)
      ->first();
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/04/19
 * Time: 17.55
 */

namespace Odeo\Domains\Payment\Mandiri\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class PaymentMandiriVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/08/19
 * Time: 17.23
 */

namespace Odeo\Domains\Payment\Mandiri\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Mandiri\Model\PaymentMandiriCallbackLog;

class PaymentMandiriCallbackLogRepository extends Repository {

  function __construct(PaymentMandiriCallbackLog $callbackLog) {
    $this->model = $callbackLog;
  }

}

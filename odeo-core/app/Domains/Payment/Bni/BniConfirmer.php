<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/18/17
 * Time: 5:28 PM
 */

namespace Odeo\Domains\Payment\Bni;


use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Bni\Helper\BniManager;

class BniConfirmer extends BniManager {

  private $userVirtualAccounts;

  public function __construct() {
    parent::__construct();
    $this->userVirtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountRepository::class);
  }

  public function confirm(PipelineListener $listener, $data) {

    if (!($order = $this->orders->findById($data['order_id']))) {
      return $listener->response(400);
    }

    $userVirtualAccount = $this->userVirtualAccounts->findById($data['virtual_account_id']);
    $userVirtualAccount->order_id = $order->id;
    $this->userVirtualAccounts->save($userVirtualAccount);

    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $data['fee'], OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    $order->total = $order->total + $data['fee'];

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');
    $this->orders->save($order);

    $bniPayment = $this->bniPayments->findById($data['trx_id']);
    $bniPayment->order_id = $order->id;
    $this->bniPayments->save($bniPayment);

    $payment = $this->payments->getNew();
    $payment->order_id = $order->id;
    $payment->opc = $data['opc'];
    $payment->info_id = $data['info_id'];
    $payment->reference_id = $data['trx_id'];
    $this->payments->save($payment);
    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200, ['status' => '000']);


  }

}
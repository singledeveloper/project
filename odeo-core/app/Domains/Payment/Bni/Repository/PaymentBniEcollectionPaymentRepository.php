<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/15/17
 * Time: 7:25 PM
 */

namespace Odeo\Domains\Payment\Bni\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Bni\Model\PaymentBniEcollectionPayment;

class PaymentBniEcollectionPaymentRepository extends Repository {

  public function __construct(PaymentBniEcollectionPayment $paymentBniEcollectionPayment) {
    $this->setModel($paymentBniEcollectionPayment);
  }

  public function findPreviousOpenTransaction($userVirtualAccountId) {
    return $this->model->whereNull('order_id')
      ->where('user_virtual_account_id', '=', $userVirtualAccountId)
      ->where('expired_at', '>', Carbon::now()->toDateTimeString())
      ->first();
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/15/17
 * Time: 2:34 PM
 */

namespace Odeo\Domains\Payment\Bni;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Payment\Bni\Helper\BniApi;
use Odeo\Domains\Payment\Bni\Helper\BniLibrary;
use Odeo\Jobs\Job;

class BniRequester extends Job {
  private $data, $va, $bniPayments;

  public function __construct($data, $va) {
    parent::__construct();
    $this->data = $data;
    $this->va = $va;
    $this->bniPayments = app()->make(\Odeo\Domains\Payment\Bni\Repository\PaymentBniEcollectionPaymentRepository::class);
  }

  public function handle() {

    $va = $this->va;
    $data = $this->data;

    $client = new Client();

    if ($payment = $this->bniPayments->findPreviousOpenTransaction($va['user_virtual_account_id'])) {
      $payload = [
        'type' => BniApi::BILL_TYPE_UPDATE,
      ];

    } else {

      $payment = $this->bniPayments->getNew();
      $payment->user_virtual_account_id = $va['user_virtual_account_id'];
      $this->bniPayments->save($payment);

      $payload = [
        'type' => BniApi::BILL_TYPE_CREATE,
      ];

    }

    if (isset($data['due_date'])) {
      $expiredAt = Carbon::parse($data['due_date'])->toDateTimeString();
      $payment->expired_at = $expiredAt;
    }

    $payload = array_merge($payload, [
      'client_id' => BniApi::getCLientId(),
      'trx_amount' => $data['amount'],
      'customer_name' => $data['user_name'],
      'virtual_account' => $va['virtual_account_number'],
      'trx_id' => $payment->id,
      'billing_type' => BniApi::PAYMENT_TYPE_CLOSE,
      'datetime_expired' => $expiredAt ?? null
    ]);

    $response = $client->request('POST', BniApi::getUrl(), [
      'json' => [
        'client_id' => BniApi::getCLientId(),
        'data' => BniLibrary::hashData($payload, BniApi::getCLientId(), BniApi::getSecret())
      ]
    ]);

    $result = json_decode($response->getBody()->getContents(), true);

    $payment->log_request = json_encode($payload);
    $payment->log_response = isset($result['data']) ? json_encode(BniLibrary::parseData($result['data'], BniApi::getCLientId(), BniApi::getSecret())) : json_encode($result);
    $this->bniPayments->save($payment);
  }

}

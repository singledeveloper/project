<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/15/17
 * Time: 2:36 PM
 */

namespace Odeo\Domains\Payment\Bni\Helper;


use Odeo\Domains\Payment\Helper\PaymentManager;

class BniManager extends PaymentManager {

  protected $library, $api, $bniPayments, $virtualAccounts, $payments;

  public function __construct() {
    parent::__construct();
    $this->library = app()->make(BniLibrary::class);
    $this->api = app()->make(BniApi::class);
    $this->bniPayments = app()->make(\Odeo\Domains\Payment\Bni\Repository\PaymentBniEcollectionPaymentRepository::class);
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);
    $this->payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
  }

}
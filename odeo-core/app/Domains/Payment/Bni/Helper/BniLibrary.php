<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/14/17
 * Time: 9:07 PM
 */

namespace Odeo\Domains\Payment\Bni\Helper;


class BniLibrary {

  const TIME_DIFF_LIMIT = 300;

  public static function hashData($data, $clientId, $secret) {
    return self::doubleEncrypt(strrev(time()) . '.' . json_encode($data), $clientId, $secret);
  }

  public static function parseData($hashedString, $clientId, $secret) {
    $parsedString = self::doubleDecrypt($hashedString, $clientId, $secret);
    list($timestamp, $data) = array_pad(explode('.', $parsedString, 2), 2, null);
    if (self::tsDiff(strrev($timestamp))) return json_decode($data, true);
    return null;
  }

  private static function tsDiff($timestamp) {
    return abs($timestamp - time()) <= self::TIME_DIFF_LIMIT;
  }

  private static function doubleEncrypt($string, $clientId, $secret) {
    $result = self::encrypt($string, $clientId);
    $result = self::encrypt($result, $secret);
    return strtr(rtrim(base64_encode($result), '='), '+/', '-_');
  }

  private static function encrypt($string, $key) {
    $result = '';
    $keyLength = strlen($key);

    for ($i = 0; $i < strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keyChar = substr($key, ($i % $keyLength) - 1, 1);
      $char = chr((ord($char) + ord($keyChar)) % 128);
      $result .= $char;
    }

    return $result;
  }

  private static function doubleDecrypt($string, $clientId, $secret) {
    $result = base64_decode(strtr(str_pad($string, ceil(strlen($string) / 4) * 4, '=', STR_PAD_RIGHT), '-_', '+/'));
    $result = self::decrypt($result, $clientId);
    $result = self::decrypt($result, $secret);
    return $result;
  }

  private static function decrypt($string, $key) {
    $result = '';
    $keyLength = strlen($key);

    for ($i = 0; $i < strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keyChar = substr($key, ($i % $keyLength) - 1, 1);
      $char = chr(((ord($char) - ord($keyChar)) + 256) % 128);
      $result .= $char;
    }

    return $result;
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/22/17
 * Time: 11:10 AM
 */

namespace Odeo\Domains\Payment\Akulaku\Test;


use GuzzleHttp\Client;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuApi;
use Odeo\Domains\Payment\Akulaku\Repository\PaymentAkulakuPaymentRepository;

class TransactionStatusUpdater {

  private $paymentAkulaku;

  public function __construct() {
    $this->paymentAkulaku = app()->make(PaymentAkulakuPaymentRepository::class);
  }

  public function updateStatus(PipelineListener $listener, $data) {

    if (isset($data['refNo']) && $paymentAkulaku = $this->paymentAkulaku->findByRefNo($data['refNo'])) {
      $status = decbin(isset($data['status']) ? $data['status'] : 100);

      $data = [
        'appId' => AkulakuApi::$APP_ID,
        'refNo' => $data['refNo'],
        'status' => $status,
        'sign' => AkulakuApi::generateSign($data['refNo'] . $status)
      ];

      $client = new Client(['verify' => false]);

      try {
        $response = $client->request('POST', AkulakuApi::$DEV_ONLY_CHANGE_STATUS_URL, ['form_params' => $data]);
        $res = json_decode($response->getBody()->getContents(), true);
        return $listener->response(200, $res);
      } catch (\Exception $e) {
        clog('akulaku_error', 'transactionUpdater', $e->getMessage());
      }
    }

    return $listener->response(200);
  }

}
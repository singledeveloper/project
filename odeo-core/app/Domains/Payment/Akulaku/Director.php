<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 8/8/17
 * Time: 7:42 PM
 */

namespace Odeo\Domains\Payment\Akulaku;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Akulaku\Helper\AkulakuManager;

class Director extends AkulakuManager {


  public function __construct() {
    parent::__construct();
    $this->orders = app()->make(\Odeo\Domains\Order\Repository\OrderRepository::class);
  }

  public function redirect(PipelineListener $listener, $data) {
    if ($url = $this->paymentUrls->getExisting($data)->first()) {

      $data = json_decode($url->body);

      $order = $this->orders->findById($url->order_id);

      if ($order->status < OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL) {
        $order->status = OrderStatus::WAITING_FOR_PAYMENT_VENDOR_APPROVAL;
        $this->orders->save($order);
      }

      return $listener->response(200, ['redirect_to' => $data->finish_redirect_url]);
    }
    return $listener->response(400);
  }

}
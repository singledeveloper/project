<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 7/21/17
 * Time: 8:36 PM
 */

namespace Odeo\Domains\Payment\Akulaku\Helper;


class AkulakuApi {

  public static $APP_ID = '272059673';
  public static $SECRET_KEY = 'sLI1-bHsS9sWiOhUEFsS-L8AV8dD8d23PZ5LgdyonFE';
  public static $GENERATE_ORDER_URL = 'https://app.akulaku.com/api/json/public/openpay/new.do';
  public static $PAYMENT_ENTRY_URL = 'https://mall.akulaku.com/v2/openPay.html';
  public static $INQUIRY_STATUS_URL = 'https://app.akulaku.com/api/json/public/openpay/status.do';
  public static $CANCEL_ORDER_URL = 'https://app.akulaku.com/api/json/public/openpay/cancel.do';
  public static $CONFIRM_RECEIPT_URL = 'https://app.akulaku.com/api/json/public/openpay/order/receipt.do';
  public static $INSTALLMENT_INFO_URL = 'https://app.akulaku.com/api/json/public/openpay/installment/info/get.do';
  public static $DEV_ONLY_CHANGE_STATUS_URL = 'https://test.app.akulaku.com/api/json/public/openpay/test/status/change.do';
  public static $PAYMENT_STATUS_PENDING = '1';
  public static $PAYMENT_STATUS_FAILED = '90';
  public static $PAYMENT_STATUS_REFUND = '91';
  public static $PAYMENT_STATUS_CANCELLED = '92';
  public static $PAYMENT_STATUS_SUCCESS = '100';
  public static $PAYMENT_STATUS_RECEIPTED = '101';

  static function init() {
    if (app()->environment() != 'production') {
      self::$APP_ID = '35352192';
      self::$SECRET_KEY = 'H3FBlTTYSKmgfvYTI2yY1fnNTNevMznDxXXwc3CGUV4';
      self::$GENERATE_ORDER_URL = 'https://test.app.akulaku.com/api/json/public/openpay/new.do';
      self::$PAYMENT_ENTRY_URL = 'https://test.mall.akulaku.com/v2/openPay.html';
      self::$INQUIRY_STATUS_URL = 'https://test.app.akulaku.com/api/json/public/openpay/status.do';
      self::$CANCEL_ORDER_URL = 'https://test.app.akulaku.com/api/json/public/openpay/cancel.do';
      self::$CONFIRM_RECEIPT_URL = 'https://test.app.akulaku.com/api/json/public/openpay/order/receipt.do';
      self::$INSTALLMENT_INFO_URL = 'https://test.app.akulaku.com/api/json/public/openpay/installment/info/get.do';
    }
  }

  static function generateSign($content) {

    $content = self::$APP_ID . self::$SECRET_KEY . $content;

    $sign = base64_encode(hash('sha512', $content, true));

    return str_replace(array('+', '/', '='), array('-', '_', ''), $sign);

  }

}
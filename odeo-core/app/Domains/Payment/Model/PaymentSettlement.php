<?php

namespace Odeo\Domains\Payment\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Account\Model\Bank;

class PaymentSettlement extends Entity {

  public function settlementDetails() {
    return $this->hasMany(PaymentSettlementDetail::class, 'id');
  }

  public function bank() {
    return $this->belongsTo(Bank::class);
  }

}
<?php

namespace Odeo\Domains\Payment\Pax\Bni\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendReconBniEdcSettlementResult extends Job {

  private $results, $date;

  public function __construct($results, $date) {
    parent::__construct();
    $this->results = $results;
    $this->date = $date;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.recon_bni_edc_settlement_result', [
      'results' => $this->results,
      'date' => $this->date,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('Recon BNI EDC Settlement Result - ' . $this->date);
    });
  }

}
<?php

namespace Odeo\Domains\Payment\Pax\Bni\Jobs;

use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendNewBniEdcVoidAlert extends Job {

  private $voids, $date;

  public function __construct($voids, $date) {
    $this->voids = $voids;
    $this->date = $date;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.new_bni_edc_void_alert', [
      'voids' => $this->voids,
      'date' => $this->date,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('New BNI EDC Void - ' . $this->date);
    });
  }

}
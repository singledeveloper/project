<?php


namespace Odeo\Domains\Payment\Pax\Helper;


use Odeo\Domains\Constant\PaxPayment;
use Odeo\Domains\Payment\Pax\Repository\CardTypeRepository;

class CardTypeHelper {

  private $cardTypeRepo;

  public function __construct() {
    $this->cardTypeRepo = app()->make(CardTypeRepository::class);
  }

  public function getCardType($cardNumber) {
    $cardNumber = substr($cardNumber, 0, 10);
    $cardNumber = str_replace('X', '0', $cardNumber);
    $cardNumber = str_replace('*', '0', $cardNumber);
    if (!is_numeric($cardNumber) || strlen($cardNumber) == 0) {
      return PaxPayment::TYPE_CREDIT_OFF_US;
    }

    $cardType = $this->cardTypeRepo->findByCardNumber($cardNumber);
    if (!$cardType) {
      return PaxPayment::TYPE_CREDIT_OFF_US;
    }
    return $cardType->type;
  }

}
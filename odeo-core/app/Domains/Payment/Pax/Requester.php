<?php


namespace Odeo\Domains\Payment\Pax;


use Illuminate\Support\Facades\Validator;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Pax\Helper\Signer;
use Odeo\Domains\Payment\Pax\Repository\PaxPaymentRepository;

class Requester {

  private $paxPaymentRepo, $signer;

  public function __construct() {
    $this->paxPaymentRepo = app()->make(PaxPaymentRepository::class);
    $this->signer = app()->make(Signer::class);
  }

  public function request(PipelineListener $listener, $order, $data) {
    $validator = Validator::make($data, [
      "payment_data" => "required",
    ]);

    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());

    if (!isset($data['skip_signature']) && $data['signature'] != $this->getSignature($data)) {
      clog('pax', "signature mismatch, data=" . json_encode($data));
      return $listener->response(400);
    }

    return $listener->response(200);
  }

  private function getSignature($data) {
    return $this->signer->sign($data['order_id'], $data['terminal_id'], $data['payment_data']);
  }

}
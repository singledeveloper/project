<?php


namespace Odeo\Domains\Payment\Pax\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendAutoSuccessUnknownBniTransactionResult extends Job {

  private $result, $date;

  public function __construct($result, $date) {
    parent::__construct();
    $this->result = $result;
    $this->date = $date;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.auto_success_unknown_bni_transaction_result', [
      'result' => $this->result,
      'date' => $this->date,
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo.co.id')
        ->to('pg@odeo.co.id')
        ->subject('Auto Success Unknown BNI EDC Transaction Result - ' . $this->date);
    });
  }
}
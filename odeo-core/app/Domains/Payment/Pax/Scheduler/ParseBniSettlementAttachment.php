<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/08/19
 * Time: 18.12
 */

namespace Odeo\Domains\Payment\Pax\Scheduler;


use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcParsedAttachmentRepository;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;
use Odeo\Domains\Payment\Pax\Jobs\ParseBniEdcSettlement;

class ParseBniSettlementAttachment {

  private $parseAttachmentRepo;
  /**
   * @var \ZipArchive
   */
  private $zip;

  private function init() {
    $this->parseAttachmentRepo = app()->make(BniEdcParsedAttachmentRepository::class);
    $this->zip = new \ZipArchive();
  }

  public function run() {
    $this->init();

    if ($parseData = $this->parseAttachmentRepo->getBniEdcSettlementParseList()) {

      foreach ($parseData as $data) {
        $target = str_replace('.zip', '', $data->filename);
        $filePath = ParseHelper::getAttachmentDir() . '/' . $data->filename;
        $targetPath = ParseHelper::getAttachmentDir() . '/' . $target;

        $this->zip->open($filePath);
        $this->zip->extractTo($targetPath);
        $this->zip->close();

        dispatch(new ParseBniEdcSettlement([
          'parse_id' => $data->id,
          'path' => $targetPath,
          'trx_date' => $data->date
        ]));
      }
    }
  }

}
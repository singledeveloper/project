<?php

namespace Odeo\Domains\Payment\Pax\Scheduler;

use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcTransactionRepository;
use Odeo\Domains\Payment\Pax\Jobs\SendNotSettledPaxPaymentAlert;
use Odeo\Domains\Payment\Pax\Repository\PaxPaymentRepository;

class ReconSettledBniPaxPayments {

  private $bniEdcTransactionRepo, $paxPaymentRepo;

  public function __construct() {
    $this->bniEdcTransactionRepo = app()->make(BniEdcTransactionRepository::class);
    $this->paxPaymentRepo = app()->make(PaxPaymentRepository::class);
  }

  public function run() {
    $res = $this->bniEdcTransactionRepo->findLastSettledDate();
    if (!$res) {
      return;
    }

    $this->paxPaymentRepo->updateBniEdcTransactionReference($res->trx_date);

    dispatch(new SendNotSettledPaxPaymentAlert($res->trx_date));
  }

}
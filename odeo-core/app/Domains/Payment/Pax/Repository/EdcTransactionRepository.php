<?php


namespace Odeo\Domains\Payment\Pax\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Pax\Model\EdcTransaction;

class EdcTransactionRepository extends Repository {

  public function __construct(EdcTransaction $m) {
    $this->setModel($m);
  }

  public function findByOrderId($orderId) {
    return $this->getCloneModel()
      ->where('order_id', $orderId)
      ->first();
  }

  public function findByReferenceTypeAndReferenceId($referenceType, $referenceId) {
    return $this->getCloneModel()
      ->where('reference_type', $referenceType)
      ->where('reference_id', $referenceId)
      ->first();
  }
}
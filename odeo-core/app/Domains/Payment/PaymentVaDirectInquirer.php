<?php
namespace Odeo\Domains\Payment;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerASG;
use Odeo\Domains\Constant\BillerQluster;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserInvoice;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\DDLending\Repository\LoanRepository;
use Odeo\Domains\Invoice\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Invoice\Repository\BusinessInvoiceUserRepository;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceRequester;

class PaymentVaDirectInquirer extends PaymentManager {

  private $virtualAccounts, $virtualAccountValidator, $loanRepo, $redis, $userInvoices, $businessInvoiceUsers;

  public function __construct() {
    parent::__construct();
    $this->loanRepo = app()->make(LoanRepository::class);
    $this->virtualAccounts = app()->make(\Odeo\Domains\VirtualAccount\Repository\UserVirtualAccountDetailRepository::class);
    $this->virtualAccountValidator = app()->make(\Odeo\Domains\Payment\Helper\VirtualAccountValidator::class);
    $this->userInvoices = app()->make(AffiliateUserInvoiceRepository::class);
    $this->odeoPaymentChannel = app()->make(\Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository::class);
    $this->businessInvoiceUsers = app()->make(BusinessInvoiceUserRepository::class);
    $this->redis = Redis::connection();
  }

  public function inquiry(PipelineListener $listener, $data) {
    $namespace = $this->getRedisNamespace($data['vendor_code'], $data['virtual_account_number']);
    if ($this->redis->get($namespace)) {
      return $listener->response(400, ['error_code' => PaymentInvoiceHelper::INVOICE_ALREADY_PAID]);
    }

    $va = $this->virtualAccounts->findByNumber($data['virtual_account_number'], $data['vendor_code'] ?? NULL);
    if(!$va) return $listener->response(400, ['error_code' => PaymentInvoiceHelper::INVOICE_NOT_FOUND]);

    $fee = $va->fee;

    if (isset($va->virtualAccount) && isset($va->virtualAccount->created_by)) {
      $businessInvoiceUser = $this->businessInvoiceUsers->findByUserId($va->virtualAccount->created_by);
      $isFeeChargedToCustomer = $businessInvoiceUser->charge_to_customer;
    }

    if ($va->service_reference_id != null && $va->service_reference_id != '') {
      $invoice = $this->userInvoices->findByInvoiceNumber($va->service_reference_id);
      if ($invoice->status != UserInvoice::OPENED) return $listener->response(400, ['error_code' => PaymentInvoiceHelper::INVOICE_ALREADY_PAID]);
      $invoiceId = $invoice->id;
    }

    if ($va->service_detail_id == ServiceDetail::USER_INVOICE_ODEO) {
      $userInvoiceBillerRepo = app()->make(\Odeo\Domains\Inventory\UserInvoice\Repository\UserInvoiceBillerRepository::class);

      $biller = $userInvoiceBillerRepo->findById($va->biller);
      if(!$biller) return $listener->response(400, ['error_code' => PaymentInvoiceHelper::INVOICE_NOT_FOUND]);

      $userId = $biller->user_id;

      if ($userId == BillerASG::getOdeoUserId()) {
        $fee = BillerASG::VIRTUAL_ACCOUNT_VENDOR_FEE[$va->vendor];
        $isFeeChargedToCustomer = false;
        $userId = $biller->biller_user_id;
      }
      else {
        $userId = $va->virtualAccount->created_by;
      }
    } else {
      $userId = $va->user_id;
    }

    $channel = $this->odeoPaymentChannel->findByGatewayIdAndGroupId(Payment::VIRTUAL_ACCOUNT, $va->payment_group_id);
    $reference = '' . ($data['reference'] ?? round(microtime(true) * 10000));

    $vaData = [
      'opc' => $channel->code,
      'fee' => $fee,
      'cost' => $va->cost,
      'virtual_account_id' => $va->user_virtual_account_id,
      'platform_id' => Platform::VIRTUAL_ACCOUNT,
      'gateway_id' => Payment::VIRTUAL_ACCOUNT,
      'store_id' => $va->store_id,
      'service_detail_id' => $va->service_detail_id,
      'biller_id' => $va->biller,
      'item_id' => $va->service_reference_id,
      'user_id' => $va->user_id,
      'auth' => [
        'user_id' => $userId
      ],
      'first_name' => $va->display_name,
      'vendor_code' => $va->vendor,
      'reference' => $reference,
      'charge_fee_to_customer' => $isFeeChargedToCustomer ?? true,
      'invoice_id' => $invoiceId ?? null
    ];

    $listener->addNext(new Task(CartInserter::class, 'addToCart'));
    $listener->addNext(new Task(CartCheckouter::class, 'checkout'));
    $listener->addNext(new Task(PaymentInvoiceRequester::class, 'request'));

    if ($va->support_affiliate_payment) {
      $listener->addNext(new Task(PaymentGatewayInvoiceRequester::class, 'request'));
    }

    return $listener->response(200, $vaData);
  }

  private function getLoanUserId($loanId) {
    $loan = $this->loanRepo->findById($loanId);
    return $loan->user_id;
  }

  private function getRedisNamespace($vendor, $vaNo) {
    return "odeo_core:invoice_payment_{$vendor}_{$vaNo}";
  }

}

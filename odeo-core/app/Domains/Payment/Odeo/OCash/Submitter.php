<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 12/9/16
 * Time: 6:23 PM
 */

namespace Odeo\Domains\Payment\Odeo\OCash;


use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Helper\PaymentManager;

class Submitter extends PaymentManager {

  public function __construct() {
    parent::__construct();
  }

  public function submit(PipelineListener $listener, $order, $paymentInformation, $data) {
    if (!isset($data['with_password']) || !$data['with_password']) {
      return $listener->response(400, "Password required");
    }

    $inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $inserter->add([
      'user_id' => $data['auth']['user_id'],
      'trx_type' => TransactionType::PAYMENT,
      'cash_type' => CashType::OCASH,
      'amount' => -$order->total,
      'data' => json_encode([
        "order_id" => $order->id
      ])
    ]);

    try {
      $inserter->run();
    } catch(\Exception $e) {
      return $listener->response(400, "insufficient oCash");
    }

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $this->orders->save($order);

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200, ['order_id' => $order->id]);
  }
}

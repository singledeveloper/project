<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Repository\PaymentGatewayMandiriEcashMutationRepository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\MandiriEcash\Jobs\SendEmailMandiriEcashReconciliationAlert;

class ReconcileMandiriEcashScheduler {

  private $mandiriEcashMutationRepo;

  public function __construct() {
    $this->mandiriEcashMutationRepo = app()->make(PaymentGatewayMandiriEcashMutationRepository::class);
  }

  public function run() {
    $startDate = Carbon::now()->subDays(14)->format("Y-m-d");
    $orders = $this->mandiriEcashMutationRepo->getReconciliationDataFromDate($startDate);

    $odeoOnly = $orders->where('mandiri_order_id', null);
    $mandiriOnly = $orders->where('order_id', null);
    $missingCharge = $this->mandiriEcashMutationRepo->getMissingChargeOrdersByDate($startDate);

    $res = $orders->reduce(function ($prev, $curr) {

      if ($this->hasMissingOrder($curr)) {
        return $prev;
      }

      if ($this->statusNotMatch($curr)) {
        $prev['status_not_match'][] = $curr;
      }

      if ($this->amountNotMatch($curr)) {
        $prev['wrong_amount'][] = $curr;
      }

      return $prev;

    }, []);

    if (!empty($odeoOnly) || !empty($mandiriOnly) || !empty($res) || !empty($missingCharge)) {
      dispatch(new SendEmailMandiriEcashReconciliationAlert([
        'odeoOnly' => $odeoOnly ?? [],
        'mandiriOnly' => $mandiriOnly ?? [],
        'missingCharges' => $missingCharge ?? [],
        'wrongAmounts' => $res['wrong_amount'] ?? [],
        'statusNotMatch' => $res['status_not_match'] ?? [],
      ]));
    }
    return '';
  }

  private function hasMissingOrder($curr) {
    return !isset($curr['mandiri_order_id']) || !isset($curr['odeo_order_id']);
  }

  private function statusNotMatch($curr) {
    return $curr->status > OrderStatus::COMPLETED;
  }

  private function amountNotMatch($curr) {
    return abs(($curr->subtotal + $curr->charge_amount) - ($curr->amount + $curr->merchant_discount_rate)) > 0;
  }

}

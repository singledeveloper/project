<?php

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Repository;

use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Model\PaymentGatewayDokuMutation;

class PaymentGatewayDokuMutationsRepository extends Repository {

  public function __construct(PaymentGatewayDokuMutation $paymentGatewayDokuMutation) {
    $this->model = $paymentGatewayDokuMutation;
  }

  function getAllFromDate($date) {
    return $this->model->where('transaction_date_time', '>=', $date)->get();
  }

  function getReconciliationFromDate($date) {
    $paymentInfoIds = implode(',', [
      Payment::OPC_GROUP_CC,
      Payment::OPC_GROUP_DOKU_WALLET,
      Payment::OPC_GROUP_ATM_TRANSFER_VA,
      Payment::OPC_GROUP_ALFA,
      Payment::OPC_GROUP_CC_TOKENIZATION
    ]);

    return $this->model->selectRaw('
      orders.id AS odeo_order_id,
      orders.subtotal + order_charges.amount AS odeo_total,
      orders.status AS odeo_status,
      payment_gateway_doku_mutations.order_id AS doku_order_id,
      payment_gateway_doku_mutations.total_payment AS doku_total,
      payment_gateway_doku_mutations.status AS doku_status
    ')->from(\DB::raw('
        orders
        JOIN order_charges ON orders.id = order_charges.order_id AND order_charges.type = \'' . OrderCharge::PAYMENT_SERVICE_COST . '\'
        JOIN payments ON orders.id = payments.order_id AND payments.info_id IN (' . $paymentInfoIds .')
        FULL OUTER JOIN payment_gateway_doku_mutations ON payment_gateway_doku_mutations.order_id = orders.id 
      '))
      ->whereDate('orders.created_at', '>=', $date)
      ->get();
  }

}

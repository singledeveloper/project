<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/10/18
 * Time: 19.09
 */

namespace Odeo\Domains\Payment\Odeo\PaymentGateway\Scraper\Doku\Jobs;


use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendEmailDokuReconciliationAlert extends Job {

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    Mail::send('emails.email_doku_reconciliation_alert', $this->data, function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('vincent.wu.vt@gmail.com')->subject('Reconciliation Alert Doku');
    });
  }

}
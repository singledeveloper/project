<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/17/16
 * Time: 5:51 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri;

use Facebook\WebDriver\WebDriverBy;

class MandiriBalanceInquiryScrapper {

  private $driver;

  private function _goToBalanceInquiry() {

    $this->driver = MandiriScrapperManager::$driver;

    $this->driver->switchTo()->defaultContent();

    $this->driver->switchTo()->frame($this->driver->findElement(WebDriverBy::cssSelector('html > frameset:nth-child(2) > frameset:nth-child(2) > frame:nth-child(1)')));

    $this->driver->findElement(WebDriverBy::cssSelector('div.silverheader:nth-child(4) > a:nth-child(1)'))
      ->click();

    sleep(1);

    $this->driver->findElement(WebDriverBy::id('subs6'))
      ->click();

    $this->driver->switchTo()->defaultContent();
  }


  public function balanceHistory() {
    $this->_goToBalanceInquiry();

    $this->driver->switchTo()->frame($this->driver->findElement(WebDriverBy::cssSelector('html > frameset:nth-child(2) > frameset:nth-child(2) > frame:nth-child(2)')));

    $this->driver->findElement(WebDriverBy::cssSelector('.clsButton > td:nth-child(1) > input:nth-child(1)'))
      ->click();

    sleep(3);

    $this->driver->findElement(WebDriverBy::cssSelector('a[href="/corp/front/accountbalancehistory.do?action=doSearchRequest"]'))
      ->click();

    sleep(3);

    $this->driver->findElement(WebDriverBy::cssSelector('tr.clsEven:nth-child(1) > td:nth-child(2) > a:nth-child(7) > img:nth-child(1)'))
      ->click()
      ->sendKeys('20072016');

    $this->driver->findElement(WebDriverBy::cssSelector('tr.clsEven:nth-child(2) > td:nth-child(2) > a:nth-child(7) > img:nth-child(1)'))
      ->click()
      ->sendKeys('16082016');

    $this->driver->findElement(WebDriverBy::cssSelector('tr.clsEven:nth-child(4) > td:nth-child(3) > input:nth-child(2)'))
      ->sendKeys('1650088664421');

    sleep(1);

    $this->driver->findElement(WebDriverBy::cssSelector('.clsButton > td:nth-child(1) > input:nth-child(1)'))
      ->click();
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/27/16
 * Time: 8:22 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Repository;

use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Model\BankMandiriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankMandiriInquiryRepository extends BankInquiryRepository {

  public function __construct(BankMandiriInquiry $mandiriInquiry) {
    $this->setModel($mandiriInquiry);
  }

  public function getBlankBalance($accountNumberId, $sortBy = 'id') {
    return $this->model->where(function ($query) {
      $query->whereNull('balance')
        ->orWhere('balance', 0);
    })
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy($sortBy, 'asc')->get();
  }

  public function getFirstBlankBalance($accountNumberId) {
    return $this->model->where(function ($query) {
      $query->whereNull('balance')
        ->orWhere('balance', 0);
    })
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy('date', 'asc')->first();
  }

  public function getLastRecordedBalance($accountNumberId) {
    return $this->model->whereNotNull('balance')->where('balance', '<>', 0)
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy('date', 'desc')->first();
  }

  public function getInquiryBeforeLimit($accountNumberId, $id, $limit) {
    return $this->model
      ->where('id', '<', $id)
      ->where('bank_scrape_account_number_id', $accountNumberId)
      ->orderBy('id', 'desc')
      ->take($limit)
      ->get();
  }

  public function findIdentical(array $inquiry) {
    return $this->model
      ->where('bank_scrape_account_number_id', $inquiry['bank_scrape_account_number_id'])
      ->where('date', $inquiry['date'])
      ->where('reference_number', $inquiry['reference_number'])
      ->where('debit', $inquiry['debit'])
      ->where('credit', $inquiry['credit'])
      ->where('balance', $inquiry['balance'])
      ->first();
  }

  function getInquiries($from, $to) {
    return $this->model->getmodel()
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->whereDate('date', '>=', $from)
      ->whereDate('date', '<=', $to)
      ->get();
  }

}

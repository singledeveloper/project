<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/17/16
 * Time: 5:49 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri;

use Carbon\Carbon;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Helper\ActivatorMarker;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccountNumber;
use DB;


class MandiriTransactionInquiryScrapper {

  use ActivatorMarker;

  private $mandiriInquiryRepository, $bankScrapeAccountNumberRepository;

  public function __construct() {
    $this->mandiriInquiryRepository = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Repository\BankMandiriInquiryRepository::class);
    $this->bankScrapeAccountNumberRepository = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository::class);
  }

  private function _goToTransactionInquiry(RemoteWebDriver $driver, BankScrapeAccountNumber $acc) {

    $driver->switchTo()->defaultContent();

    $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('html > frameset:nth-child(2) > frameset:nth-child(2) > frame:nth-child(1)')));

    $isNotVisible = null;

    do {

      $driver->findElement(WebDriverBy::cssSelector('div.silverheader:nth-child(4) > a:nth-child(1)'))
        ->click();

      sleep(1);

      try {

        $driver->findElement(WebDriverBy::id('subs11'))
          ->click();

        $isNotVisible = false;

      } catch (ElementNotVisibleException $exception) {
        $isNotVisible = true;
      }

    } while ($isNotVisible);

    $driver->switchTo()->defaultContent();
  }

  public function _fillTransactionInquiryForm(RemoteWebDriver $driver, BankScrapeAccountNumber $acc, $lastScrapedDate, $targetMaxScrapedDate, $firstBlankBalance) {

    $this->markAsActive('mandiri');

    $driver->switchTo()->defaultContent();

    $driver->switchTo()->frame($driver->findElement(WebDriverBy::cssSelector('html > frameset > frameset > frame:nth-child(2)')));

    $driver->findElement(WebDriverBy::cssSelector('body > form > table.clsForm > tbody > tr:nth-child(1) > td:nth-child(2) > a:nth-child(7) > img'))
      ->click();

    $driver->findElement(WebDriverBy::cssSelector('body > form > table.clsForm > tbody > tr:nth-child(2) > td:nth-child(2) > a:nth-child(7) > img'))
      ->click();

    sleep(1);

    $isLoop = false;

    do {

      $driver->executeScript("document.querySelector('body > form > table.clsForm > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=\"hidden\"]:nth-child(1)').value = '" . sprintf('%02d', $lastScrapedDate->day) . "'", array());
      $driver->executeScript("document.querySelector('body > form > table.clsForm > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=\"hidden\"]:nth-child(2)').value = '" . sprintf('%02d', $lastScrapedDate->month) . "'", array());
      $driver->executeScript("document.querySelector('body > form > table.clsForm > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=\"hidden\"]:nth-child(3)').value = '" . $lastScrapedDate->year . "'", array());

      $driver->executeScript("document.querySelector('body > form > table.clsForm > tbody > tr:nth-child(2) > td:nth-child(2) > input[type=\"hidden\"]:nth-child(1)').value = '" . sprintf('%02d', $targetMaxScrapedDate->day) . "'", array());
      $driver->executeScript("document.querySelector('body > form > table.clsForm > tbody > tr:nth-child(2) > td:nth-child(2) > input[type=\"hidden\"]:nth-child(2)').value = '" . sprintf('%02d', $targetMaxScrapedDate->month) . "'", array());
      $driver->executeScript("document.querySelector('body > form > table.clsForm > tbody > tr:nth-child(2) > td:nth-child(2) > input[type=\"hidden\"]:nth-child(3)').value = '" . $targetMaxScrapedDate->year . "'", array());

      $driver->findElement(WebDriverBy::cssSelector('body > form > table.clsForm > tbody > tr:nth-child(5) > td:nth-child(2) > input[type="text"]:nth-child(2)'))
        ->sendKeys($acc->number);

      $acc->last_time_scrapped = Carbon::now();
      $this->bankScrapeAccountNumberRepository->save($acc);

      $driver->findElement(WebDriverBy::cssSelector("input[name=\"show1\"]"))
        ->click();

      sleep(1);

      try {


        $driver = $driver->findElement(WebDriverBy::cssSelector('body > form > table.clsForm > tbody > tr:nth-child(1) > td > b > font > li'));


        if ($driver->getText() == ' APR-1110077 - Transaction Inquiry Record not found') {

          $acc->last_scrapped_date = Carbon::now();
          $this->bankScrapeAccountNumberRepository->save($acc);

          list($dayDifference, $lastScrapedDate, $targetMaxScrapedDate) = $this->_getScrapDate($acc, $firstBlankBalance);
          if ($dayDifference > 0) $isLoop = true;
          else {
            return -1;
          }
        } else if ($driver->getText() == ' ???en_US.MW - DSP0003#MW - DSP0003 - Transaction Time Out. Please Contact Helpdesk=^=MW - DSP0003 - Transaction Time Out. Harap menghubungi Helpdesk???') {
          $isLoop = true;
        }

      } catch (\Exception $exception) {
        $isLoop = false;
      }

    } while ($isLoop);

    return 1;
  }


  public function validateWithTheSameOrderAlgorithm($pastInquiries, $scrappedData) {

    $shouldBePassedNumber = min(10, count($scrappedData));
    $passed = 0;

    $pastInquiriesCount = count($pastInquiries);

    $shouldBePassedNumber = min($pastInquiriesCount, $shouldBePassedNumber);

    $scrappedDataIndex = count($scrappedData) - 1;

    for ($x = 0; $x < $shouldBePassedNumber; $x++, $scrappedDataIndex--) {

      $pastInquiry = $pastInquiries[$x];
      $scrappedInquiry = $scrappedData[$scrappedDataIndex];

      if (Carbon::parse($pastInquiry->date)->ne($scrappedInquiry['date']) ||
        $pastInquiry['reference_number'] != $scrappedInquiry['reference_number'] ||
        $pastInquiry['debit'] != $scrappedInquiry['debit'] ||
        $pastInquiry['credit'] != $scrappedInquiry['credit']
      ) {
        break;
      }

      $passed++;
    }
    if ($passed == $shouldBePassedNumber) {
      return true;
    }
    return false;
  }

  public function validateWithContainsAlgorithm($pastInquiries, $scrappedData) {

    $shouldBePassedNumber = min(10, count($scrappedData));
    $passed = 0;

    $pastInquiriesCount = count($pastInquiries);
    $shouldBePassedNumber = min($pastInquiriesCount, $shouldBePassedNumber);

    $scrappedDataIndex = count($scrappedData) - 1;

    for ($i = 0; $i < $shouldBePassedNumber; $i++) {

      $pastInquiry = $pastInquiries[$i];

      for ($j = $scrappedDataIndex; $j >= 0; $j--) {

        $scrappedInquiry = $scrappedData[$j];

        if (Carbon::parse($pastInquiry['date'])->eq($scrappedInquiry['date']) ||
          $pastInquiry['reference_number'] == $scrappedInquiry['reference_number'] ||
          $pastInquiry['debit'] == $scrappedInquiry['debit'] ||
          $pastInquiry['credit'] == $scrappedInquiry['credit']
        ) {
          $passed++;
          break;
        }
      }
    }

    if ($passed == $shouldBePassedNumber) {
      return true;
    }
    return false;

  }

  public function _scrapTransactionInquiry(RemoteWebDriver $driver, BankScrapeAccountNumber $acc, $targetMaxScrapedDate) {

    \Log::info('is happy scraping');

    $isLoop = null;
    $scrappedData = [];
    $scrappedDataPointer = 0;
    $scrappedDataUnusuedIndex = [];
    $forLogging = [];

    $now = Carbon::now();

    $pendingInquiries = $this->mandiriInquiryRepository->getBlankBalance($acc->id);

    $shouldCheckInquiriesCount = $pendingInquiries->count();
    $shouldCheckInquiries = [];

    $lastInquiry = null;

    $allPastInquiries = [];

    if ($shouldCheckInquiriesCount) {
      $shouldCheckInquiries = $pendingInquiries;

      $allPastInquiries = $this->mandiriInquiryRepository->getInquiryBeforeLimit($acc->id, $pendingInquiries[0]->id, $shouldCheckInquiriesCount + 10)->toArray();

    } else if ($acc->mandiriInquiries()->count()) {

      $temp = $acc->mandiriInquiries()
        ->orderBy('id', 'desc')
        ->orderBy('date', 'desc')
        ->first();

      $lastInquiry = $temp;
      $shouldCheckInquiries[] = $temp;

      $shouldCheckInquiriesCount++;

      $allPastInquiries = $this->mandiriInquiryRepository->getInquiryBeforeLimit($acc->id, $lastInquiry->id, $shouldCheckInquiriesCount + 10)->toArray();

    }


    do {
      $tableRow = $driver->findElements(WebDriverBy::cssSelector('body > form > table.clsFormTrxStatus tr'));


      for ($i = 1, $length = count($tableRow); $i < $length - 3; $i++) {

        $tableCell = $tableRow[$i]->findElements(WebDriverBy::cssSelector('td'));

        $currentDate = Carbon::createFromFormat('d/m/Y H:i:s', $tableCell[0]->getText());

        if (($currentDate->hour == 0 || $currentDate->hour === '00' || $currentDate->hour === '0') && Carbon::now()->hour == 12) {
          $currentDate->hour = 12;
        }

        $debit = $this->parseNumber($tableCell[4]->getText());
        $credit = $this->parseNumber($tableCell[5]->getText());

        $balance = $tableCell[6]->getText() == ''
          ? null
          : $this->parseNumber($tableCell[6]->getText());

        $forLogging[] = [
          'bank_scrape_account_number_id' => $acc->id,
          'date' => $currentDate,
          'date_value' => $tableCell[1]->getText(),
          'description' => $tableCell[2]->getText(),
          'reference_number' => $tableCell[3]->getText(),
          'debit' => $debit,
          'credit' => $credit,
          'balance' => $balance,
          "created_at" => $now,
          "updated_at" => $now,
        ];

        if ($shouldCheckInquiriesCount) {

          for ($a = 0; $a < $shouldCheckInquiriesCount; $a++) {

            $shouldCheckInquiry = $shouldCheckInquiries[$a];

            if (isset($shouldCheckInquiry->stop)) {
              continue;
            }

            if ($currentDate->lt($shouldCheckInquiry->date)) {

              $scrappedDataUnusuedIndex[] = $scrappedDataPointer;

            } else if ($shouldCheckInquiry->date->eq($currentDate) &&
              $shouldCheckInquiry->reference_number == $tableCell[3]->getText() &&
              $shouldCheckInquiry->debit == $debit &&
              $shouldCheckInquiry->credit == $credit
            ) {

              if ($shouldCheckInquiry->description == $tableCell[2]->getText() &&
                $shouldCheckInquiry->balance == $balance
              ) {

                $scrappedDataUnusuedIndex[] = $scrappedDataPointer;

              } else if ($balance) {

                $pastInquiries = array_slice($allPastInquiries, $a, 10);

                if ($this->validateWithTheSameOrderAlgorithm($pastInquiries, $scrappedData) || $this->validateWithContainsAlgorithm($pastInquiries, $scrappedData)) {
                  $shouldCheckInquiry->description = $tableCell[2]->getText();
                  $shouldCheckInquiry->balance = $balance;
                  $shouldCheckInquiry->save();

                  $shouldCheckInquiry->stop = true;

                  $scrappedDataUnusuedIndex[] = $scrappedDataPointer;
                }
              }
            }

          }

        }

        if (in_array($scrappedDataPointer, $scrappedDataUnusuedIndex)) {
          $scrappedDataPointer++;
          continue;
        }

        if ($lastInquiry && ($currentDate->lt($lastInquiry->date) ||
            ($lastInquiry->date->eq($currentDate) &&
              $lastInquiry->description == $tableCell[2]->getText() &&
              $lastInquiry->reference_number == $tableCell[3]->getText() &&
              $lastInquiry->debit == $debit &&
              $lastInquiry->credit == $credit &&
              $lastInquiry->balance == $balance))
        ) {

          $scrappedDataUnusuedIndex[] = $scrappedDataPointer;
        } else if ($this->mandiriInquiryRepository->findIdentical([
          'bank_scrape_account_number_id' => $acc->id,
          'date' => $currentDate,
          'date_value' => $tableCell[1]->getText(),
          'reference_number' => $tableCell[3]->getText(),
          'debit' => $debit,
          'credit' => $credit,
          'balance' => $balance,
        ])
        ) {
          $scrappedDataUnusuedIndex[] = $scrappedDataPointer;
        }

        $temp = [
          'bank_scrape_account_number_id' => $acc->id,
          'date' => $currentDate,
          'date_value' => $tableCell[1]->getText(),
          'description' => $tableCell[2]->getText(),
          'reference_number' => $tableCell[3]->getText(),
          'debit' => $debit,
          'credit' => $credit,
          'balance' => $balance,
          "created_at" => $now,
          "updated_at" => $now,
        ];

        \Log::info($currentDate->toDateTimeString());
        \Log::info($now->toDateTimeString());

        $scrappedData[$scrappedDataPointer] = $temp;


        $scrappedDataPointer++;

      }


      try {
        // $page = $driver->findElement(WebDriverBy::cssSelector('body > form > table.clsFormTrxStatus > tbody > tr.clsBtnBar > td > a:nth-child(4)'));
        $page = $driver->findElement(WebDriverBy::cssSelector('body > form > table.clsFormTrxStatus > tbody > tr.clsBtnBar > td > a[onclick="onClickNext()"]'));

        $page->click();

        sleep(1);

        $isLoop = true;

      } catch (NoSuchElementException $exception) {
        $isLoop = false;
      }



    } while ($isLoop);

    if (count($scrappedData)) {

      $scrappedData = array_except($scrappedData, $scrappedDataUnusuedIndex);
      \Log::info('logging');
      \Log::info(\json_encode($forLogging));
      \Log::info('logging beres');
      $this->mandiriInquiryRepository->saveBulk($scrappedData);

    }

    $acc->last_scrapped_date = $targetMaxScrapedDate->toDateTimeString();
    $this->bankScrapeAccountNumberRepository->save($acc);

  }


  public function transactionInquiry(RemoteWebDriver $driver, BankScrapeAccount $account) {

    for ($i = 0, $length = $account->numbers->count(); $i < $length; $i++) {

     // do {


        $pendingInquiries = $this->mandiriInquiryRepository->getBlankBalance($account->numbers[$i]->id, 'date');


        list($dayDifference, $lastScrapedDate, $targetMaxScrapedDate) = $this->_getScrapDate($account->numbers[$i], count($pendingInquiries) ? $pendingInquiries[0] : null);

        DB::beginTransaction();

        $this->_goToTransactionInquiry($driver, $account->numbers[$i]);
        \Log::info('has go to transaction inquiry');

        if ($this->_fillTransactionInquiryForm($driver, $account->numbers[$i], $lastScrapedDate, $targetMaxScrapedDate, count($pendingInquiries) ? $pendingInquiries[0] : null) == 1) {
          \Log::info('has filled form');


          $this->_scrapTransactionInquiry($driver, $account->numbers[$i], $targetMaxScrapedDate);
        }


        DB::commit();


      //} while ($dayDifference > 0);
    }
  }


  public function _getScrapDate(BankScrapeAccountNumber $acc, $firstBlankBalance) {


    $lastScrapedDate = $acc->last_scrapped_date;

    if ($firstBlankBalance) {
      $lastScrapedDate = $firstBlankBalance->date->subDay(1);
    }

    $maxScrapedDate = $lastScrapedDate->copy();

    $purifiedLastScrapedDate = Carbon::parse($lastScrapedDate->toDateString());

    $dayDifference = $purifiedLastScrapedDate->diffInDays(Carbon::now());

    return [
      $dayDifference,
      $lastScrapedDate,
      $maxScrapedDate->addDay(min($dayDifference, 30))
    ];
  }

  private function parseNumber($str) {
    return intval(str_replace(',', '', str_replace('.00', '', trim($str))));
  }

}

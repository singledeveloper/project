<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model;


use Odeo\Domains\Core\Entity;

class BankScrapeAccount extends Entity
{

  protected $hidden = ['created_at', 'updated_at', 'account', 'active'];

  public function numbers()
  {
    return $this->hasMany(BankScrapeAccountNumber::class);
  }
}

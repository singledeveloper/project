<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/08/18
 * Time: 14.53
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankBriInquiry extends Model {

  use SoftDeletes;

  protected $dates = ['date', 'created_at', 'updated_at', 'deleted_at'];

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/10/18
 * Time: 19.18
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2\Job\ReconBriAccountStatementJob;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class ReconBriAccountStatementScheduler {

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $bankScrapeAccountNo;

  private function initialize() {
    $this->bankScrapeAccountNo = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function run() {
    $this->initialize();
    $number = $this->bankScrapeAccountNo->findByName('bri');
    $startDate = $this->getStartDate($number->last_scrapped_date);
    $endDate = $this->getEndDate($startDate);
//    $startDate = Carbon::createFromFormat('Y-m-d', '2018-05-01');
//    $endDate = $startDate->copy()->endOfMonth();
    dispatch(new ReconBriAccountStatementJob($startDate, $endDate, $endDate->isSameDay(Carbon::now())));
  }

  private function isDayGreaterThan(Carbon $a, Carbon $b) {
    return $a->startOfDay() > $b->startOfDay();
  }

  private function getStartDate(Carbon $lastScrapeDate) {
    $endDateOfYear = $lastScrapeDate->copy()->endOfYear();
    $now = Carbon::now();

    if ($lastScrapeDate->isSameDay($endDateOfYear) && $this->isDayGreaterThan($now, $lastScrapeDate)) {
      return $lastScrapeDate->addDay(1);
    }

    return $lastScrapeDate->subDay(1);
  }

  private function getEndDate(Carbon $startDate) {
    $now = Carbon::now();
    $endDate = $startDate->copy()->addDay(28);

    if ($startDate->year != $endDate->year) {
      return $startDate->copy()->endOfMonth();
    }
    if ($this->isDayGreaterThan($endDate, $now)) {
      return $now;
    }

    return $endDate;
  }
}

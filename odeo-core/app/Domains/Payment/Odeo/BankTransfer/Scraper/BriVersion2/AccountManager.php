<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/08/18
 * Time: 14.56
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BriVersion2;


use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class AccountManager {

  public function login($account) {
    $client = new Client([
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'cookies' => true, 
      'timeout' => 60,
    ]);

    $response = $client->request('GET', 'https://ibank.bri.co.id/cms/Logon.aspx');
    $domCrawler = new Crawler($response->getBody()->getContents(), 'https://ibank.bri.co.id/cms/Logon.aspx');
    $form = $domCrawler->filter('form')->form();
    $payload = [
      '__EVENTTARGET' => 'btnLogin',
      '__VIEWSTATE' => $form['__VIEWSTATE']->getValue(),
      '__VIEWSTATEGENERATOR' => $form['__VIEWSTATEGENERATOR']->getValue(),
      '__EVENTVALIDATION' => $form['__EVENTVALIDATION']->getValue(),
      'ClientID' => $account->cooperate_id,
      'UserID' => $account->user_id,
      'Password' => $account->password,
      '__EVENTARGUMENT' => '',
      'token' => '',
      'isAdaPengumunan' => ''
    ];

    $client->request('POST', 'https://ibank.bri.co.id/cms/Logon.aspx', ['form_params' => $payload]);

    clog('recon_bri', 'login');
    return $client;
  }

  public function logout(Client $client) {
    $header = $client->request('GET', 'https://ibank.bri.co.id/cms/master/Header.aspx');
    $domCrawler = new Crawler($header->getBody()->getContents(), 'https://ibank.bri.co.id/cms/master/Header.aspx');
    $form = $domCrawler->filter('form')->form();

    $client->request('POST', 'https://ibank.bri.co.id/cms/master/Header.aspx', [
      'form_params' => [
        '__EVENTTARGET' => 'btnLogout',
        '__VIEWSTATE' => $form['__VIEWSTATE']->getValue(),
        '__VIEWSTATEGENERATOR' => $form['__VIEWSTATEGENERATOR']->getValue(),
        '__EVENTVALIDATION' => $form['__EVENTVALIDATION']->getValue(),
      ]
    ]);
    clog('recon_bri', 'logout');
  }

}

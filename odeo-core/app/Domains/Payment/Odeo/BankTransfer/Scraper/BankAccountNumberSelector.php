<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 11/1/16
 * Time: 10:50 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class BankAccountNumberSelector implements SelectorListener {


  public function __construct() {
    $this->scrapeAccountNumbers = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    return $item;
  }


  public function get(PipelineListener $listener, $data) {

    $this->scrapeAccountNumbers->normalizeFilters($data);
    $this->scrapeAccountNumbers->setSimplePaginate(true);

    $accountNumbers = [];

    foreach ($this->scrapeAccountNumbers->gets() as $item) {
      $accountNumbers[] = $this->_transforms($item, $this->scrapeAccountNumbers);
    }

    if (count($accountNumbers) > 0) {
      return $listener->response(200, array_merge(['account_numbers' => $this->_extends($accountNumbers, $this->scrapeAccountNumbers)],
        $this->scrapeAccountNumbers->getPagination()
      ));
    }
    return $listener->response(204);
  }

}

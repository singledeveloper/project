<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/29/17
 * Time: 7:31 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository;


use Carbon\Carbon;
use Illuminate\Support\Collection;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Model\BankCimbInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankInquiryRepository;

class BankCimbInquiryRepository extends BankInquiryRepository {

  public function __construct(BankCimbInquiry $bankCimbInquiry) {
    $this->setModel($bankCimbInquiry);
  }

  /**
   * @return Collection
   */
  public function getInquiryBetweenDate($dateFrom, $dateTo) {
    return $this->model
      ->where('date', '>=', Carbon::parse($dateFrom)->startOfDay())
      ->where('date', '<=', Carbon::parse($dateTo)->endOfDay())
      ->get();
  }

  /**
   * @return Collection
   */
  function getInquiries($from, $to) {
    return $this->model
      ->orderBy('date', 'asc')
      ->orderBy('id', 'asc')
      ->where('date', '>=', Carbon::parse($from)->startOfDay())
      ->where('date', '<=', Carbon::parse($to)->endOfDay())
      ->get();
  }

  function findFirstUnsignedSequenceInquiry() {
    return $this->model
      ->whereNotNull('balance')
      ->whereNull('sequence_number')
      ->orderBy('date', 'asc')
      ->first();
  }

  /**
   * @return Collection
   */
  function getInquiryByDate($date) {
    return $this->model
      ->where('date', '>=', Carbon::parse($date)->startOfDay())
      ->get();
  }

  function findLatestSequenceByDate($date) {
    return $this->model
      ->where('date', '<=', Carbon::parse($date)->endOfDay())
      ->orderBy('sequence_number', 'desc')
      ->first();
  }

  public function getUnreconciledVaPayment($reference) {
    return $this->model
      ->where('bank_reff_no', $reference)
      ->where('credit', '>', 0)
      ->where(function($subquery){
        $subquery->whereNull('reference_type')
          ->orWhere('reference_type', 'note');
      })
      ->orderBy('id', 'asc')
      ->first();
  }

}

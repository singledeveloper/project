<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 24/09/18
 * Time: 11.21
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Helper\CimbHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Model\BankCimbInquiryBalanceSummary;

class BankCimbInquiryBalanceSummaryRepository extends Repository {

  function __construct(BankCimbInquiryBalanceSummary $balanceSummary) {
    $this->setModel($balanceSummary);
  }

  function findLatestSummary() {
    return $this->model->orderBy('created_at', 'desc')->first();
  }

  function findDistinctRow() {
    return $this->model
      ->where('status', '=', CimbHelper::STATUS_DIFF_BALANCE)
      ->orderBy('scrape_start_date', 'asc')
      ->first();
  }

  function updateUnbalanceStatus() {
    $this->model
      ->where('status', '=', CimbHelper::STATUS_DIFF_BALANCE)
      ->update(['status' => CimbHelper::STATUS_OK]);
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/10/18
 * Time: 13.26
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Job;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\CimbAccountInquiryScraper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\CimbAccountManager;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Helper\CimbHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Repository\BankCimbInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job\SendMismatchMutationAlert;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeInformation;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository;
use Odeo\Jobs\Job;

class ScrapeReconCimbInquiryJob extends Job {

  const BANK_TARGET = 'cimb';

  /**
   * @var BankCimbInquiryRepository
   */
  private $cimbInquiryRepo;

  /**
   * @var BankCimbInquiryBalanceSummaryRepository
   */
  private $cimbInquirySummary;

  /**
   * @var CimbAccountManager
   */
  private $cimbAccountManager;

  /**
   * @var CimbAccountInquiryScraper
   */
  private $scraper;

  /**
   * @var BankScrapeInformationRepository
   */
  private $bankScrapeInformationRepo;

  /**
   * @var BankScrapeInformation
   */
  private $information;

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $bankScrapeAccountNumber;

  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var Carbon
   */
  private $startDate, $endDate, $shouldRecon;

  function __construct($startDate, $endDate, $shouldRecon = false) {
    parent::__construct();
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->shouldRecon = $shouldRecon;
  }

  private function initialize() {
    $this->cimbInquiryRepo = app()->make(BankCimbInquiryRepository::class);
    $this->cimbInquirySummary = app()->make(BankCimbInquiryBalanceSummaryRepository::class);
    $this->cimbAccountManager = app()->make(CimbAccountManager::class);
    $this->scraper = app()->make(CimbAccountInquiryScraper::class);
    $this->bankScrapeInformationRepo = app()->make(BankScrapeInformationRepository::class);
    $this->bankScrapeAccountNumber = app()->make(BankScrapeAccountNumberRepository::class);
    $this->redis = Redis::connection();

    $this->information = $this->bankScrapeInformationRepo->getActiveAccount(self::BANK_TARGET);
  }

  public function handle() {
    $this->initialize();

    if (!$this->redis->setnx(CimbHelper::REDIS_RECON_MUTATION_LOCK, 1)) {
      return;
    }
    $this->redis->expire(CimbHelper::REDIS_RECON_MUTATION_LOCK, 5 * 60);

    $client = new Client(['cookies' => true, 'timeout' => 60]);

    foreach ($this->information->accounts as $account) {
      $latestSummary = $this->cimbInquirySummary->findLatestSummary();
      $scrapeAccountNumber = $this->bankScrapeAccountNumber->findById(BankAccount::CIMB_ODEO);

      $scrapeAccount = json_decode($account->account, true);
      $errorCount = $this->redis->get(CimbHelper::REDIS_RECON_ERROR_COUNT);
      try {
        $this->cimbAccountManager->login($client, $scrapeAccount);
        list($scrapedInquiries, $scrapeLedgerBalance, $scrapeYesterdayBalance) = $this->scraper->scrapeAccountStatement($client, $this->startDate, $this->endDate);

        if (empty($scrapedInquiries)) {
          continue;
        }

        $existingInquiries = $this->cimbInquiryRepo->getInquiryBetweenDate($this->startDate, $this->endDate)
          ->keyBy(function ($inquiry) {
            return CimbHelper::getInquiryKey($inquiry);
          });

        $scrapedInquiries = collect($scrapedInquiries);
        $scrapedInquiries = $scrapedInquiries->keyBy(function ($inquiry) {
          return CimbHelper::getInquiryKey($inquiry);
        });

        $netMovement = 0;
        $updates = $newInquiries = [];

        // Find new inquires that has not been inserted to db and inquiries to be updated
        foreach ($scrapedInquiries as $key => $inquiry) {
          if ($existingInquiries->has($key)) {
            $existingInquiry = $existingInquiries[$key];

            $hasChanges = $inquiry['balance'] !== $existingInquiry['balance']
              || $inquiry['description'] !== $existingInquiry['description'];

            if ($hasChanges) {
              $updates[] = [
                'id' => $existingInquiry['id'],
                'balance' => $inquiry['balance'],
                'description' => $inquiry['description'],
                'post_date' => $inquiry['post_date'],
                'eff_date' => $inquiry['eff_date'],
                'cheque_no' => $inquiry['cheque_no'],
                'transaction_code' => $inquiry['transaction_code'],
                'updated_at' => Carbon::now()
              ];
            }
          } else {
            $netMovement += $inquiry['credit'] - $inquiry['debit'];
            $newInquiries[] = $inquiry;
          }
        };

        // Find the existing inquiries that has been removed by the payment gateway
        $removeIds = $existingInquiries->diffKeys($scrapedInquiries)
          ->map(function ($inquiry) use (&$netMovement) {
            $netMovement -= $inquiry['credit'] - $inquiry['debit'];
            return $inquiry['id'];
          })
          ->values()
          ->all();

        $newSummary = $this->cimbInquirySummary->getNew();
        $newSummary->balance_change = empty($latestSummary) ? $scrapeLedgerBalance : $netMovement;
        $newSummary->previous_balance = $latestSummary->balance ?? 0;
        $newSummary->balance = empty($latestSummary) ? $scrapeLedgerBalance : $latestSummary->balance + $netMovement;
        $newSummary->scrape_ledger_balance = $scrapeLedgerBalance;
        $newSummary->scrape_yesterday_balance = $scrapeYesterdayBalance;
        $newSummary->scrape_start_date = $this->startDate;
        $newSummary->scrape_end_date = $this->endDate;
        list($newSummary->status, $newSummary->first_distinct_id) = $this->getDiffStatus($latestSummary, $newSummary);

        $scrapeAccountNumber->last_scrapped_date = Carbon::now();
        $scrapeAccountNumber->last_time_scrapped = Carbon::now();

        if (count($updates)) {
          $this->cimbInquiryRepo->updateBulk($updates);
        }
        if (count($removeIds)) {
          $this->cimbInquiryRepo->deleteById($removeIds);
        }
        if (count($newInquiries)) {
          $this->cimbInquiryRepo->saveBulk(array_reverse($newInquiries));
        }

        if ($newSummary->status == CimbHelper::STATUS_OK) {
          $this->cimbInquirySummary->updateUnbalanceStatus();
        }

        $this->bankScrapeAccountNumber->save($scrapeAccountNumber);
        if ($this->shouldRecon) {
          $this->cimbInquirySummary->save($newSummary);
        }

        $errorCount && $this->resetErrorCount();
      } catch (\Exception $e) {
        $this->handleInternalNotice($errorCount, $scrapeAccount);
        clog('recon_cimb', 'err msg: ' . $e->getMessage());
        clog('recon_cimb', 'err trace: ' . $e->getTraceAsString());
      } finally {
        $this->cimbAccountManager->logout($client);
      }
    }

    $this->redis->del(CimbHelper::REDIS_RECON_MUTATION_LOCK);

    if ($this->shouldRecon && $distinctSummary = $this->cimbInquirySummary->findDistinctRow()) {
      if ($this->startDate->startOfDay() > Carbon::now()->subDay(28)) {
     //   dispatch(new self($this->startDate->subDay(2), $this->endDate->subDay(2), true));
      } else {
        dispatch(new SendMismatchMutationAlert(self::BANK_TARGET, $this->startDate));
      }
    }
  }

  private function getDiffStatus($previousSummary, $newSummary) {
    if ($newSummary->balance != $newSummary->scrape_ledger_balance) {
      if ($previousSummary->status != CimbHelper::STATUS_OK) {
        return [CimbHelper::STATUS_DIFF_BALANCE, $previousSummary->first_distinct_id ?? $previousSummary->id];
      }
      return [CimbHelper::STATUS_DIFF_BALANCE, null];
    }
    return [CimbHelper::STATUS_OK, null];
  }

  private function resetErrorCount($count = 0) {
    $this->redis->set(CimbHelper::REDIS_RECON_ERROR_COUNT, $count);
  }

  private function handleInternalNotice($count, $scrapeAccount) {
    if ($count == 14) {
      $noticer = app()->make(InternalNoticer::class);
      $noticer->saveMessage('CIMB Scrape Failed: ' . $scrapeAccount['username'] . ' failed to scrape in the last 15 minutes.');
      $this->resetErrorCount();
    } else {
      $count += 1;
      $this->resetErrorCount($count);
    }
  }
}

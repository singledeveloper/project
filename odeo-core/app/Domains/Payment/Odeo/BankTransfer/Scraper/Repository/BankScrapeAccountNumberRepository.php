<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 8/27/16
 * Time: 8:28 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository;


use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Model\BankBcaInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bni\Model\BankBniInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bri\Model\BankBriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Cimb\Model\BankCimbInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Mandiri\Model\BankMandiriInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Model\BankMandiriGiroInquiry;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccountNumber;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Model\BankPermataInquiry;

class BankScrapeAccountNumberRepository extends Repository {

  public function __construct(BankScrapeAccountNumber $bankScrapeAccountNumber) {
    $this->model = $bankScrapeAccountNumber;
  }

  public function gets() {

    $query = $this->getCloneModel();

    $query = $query->join('bank_scrape_accounts', 'bank_scrape_accounts.id', '=', 'bank_scrape_account_numbers.bank_scrape_account_id')
      ->select('bank_scrape_account_numbers.id', 'referred_target as bank', 'number', 'last_time_scrapped')
      ->where('bank_scrape_account_numbers.active', true);

    return $this->getResult($query);

  }

  public function inquiries($target, $id) {

    $filters = $this->getFilters();
    $targetInquiries = 'bank_' . $target . '_inquiries.';

    switch ($target) {
      case 'permata' :
        $dateCol = 'post_date';
        break;
      default:
        $dateCol = 'date';
        break;

    }

    $accountNumber = $this->model->find($id);

    $query = call_user_func([$accountNumber, $target . 'Inquiries']);

    $query->leftJoin('bank_inquiry_additional_informations',
      function ($join) use ($targetInquiries, $target) {
        $join->on('bank_inquiry_additional_informations.reference_id', '=', $targetInquiries . 'id')
          ->where('bank_inquiry_additional_informations.bank_reference', '=', $target);
      });

    if (isset($filters['start_date'])) $query->where($dateCol, '>=', $filters['start_date']);
    if (isset($filters['end_date'])) $query->where($dateCol, '<=', $filters['end_date']);

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query->where($targetInquiries . 'id', $filters['search']['id']);
      }
      if (isset($filters['search']['reference_type'])) {
        if ($filters['search']['reference_type'] == '-') {
          $query->where(function($subquery) use ($targetInquiries) {
            $subquery->whereNull($targetInquiries . 'reference_type')
              ->orWhere($targetInquiries . 'reference_type', 'note');
          });
        } else {
          $query->where($targetInquiries . 'reference_type', $filters['search']['reference_type']);
        }
      }
      if (isset($filters['search']['reference'])) {

        $filters['search']['reference'] =
          isset($filters['search']['reference_type']) && $filters['search']['reference_type'] == 'note' ?
            ('%' . $filters['search']['reference'] . '%') :
            ('%"' . $filters['search']['reference'] . '"%');

        $query->where($targetInquiries . 'reference', 'ilike', $filters['search']['reference']);

      }
      if (isset($filters['search']['description'])) {

        $query->where($targetInquiries . 'description', 'ilike', '%' . $filters['search']['description'] . '%');

      }
    }

    $result = $this->getResult(
      $query->select(
        $targetInquiries . '*',
        'bank_inquiry_additional_informations.description as info_desc',
        'bank_inquiry_additional_informations.pdf_url'
      )
        ->orderBy($targetInquiries . $dateCol, 'desc')
        ->orderBy($targetInquiries . 'id', 'desc')
    );

    return [
      'content' => $result,
      'last_time_scrapped' => $accountNumber->last_time_scrapped
    ];

  }


  public function getUnreconciledInquiries() {
    return [
      'mandiri' => BankMandiriInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get(),

      'mandiri_giro' => BankMandiriGiroInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get(),

      'bca' => BankBcaInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get(),

      'bri' => BankBriInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get(),

      'cimb' => BankCimbInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get(),

      'bni' => BankBniInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get(),

      'permata' => BankPermataInquiry::query()
        ->where(function ($query) {
          $query->where('reference', null)
            ->orWhere('reference', '');
        })
        ->where('credit', '>', 0)
        ->get()
    ];
  }


  public function getWithdrawUnreconciledInquiries() {
    $briCost = BriDisbursement::COST;
    return [
      'bca' => BankBcaInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%.00WDOCASH 1%')
        ->get(),

      'cimb' => BankCimbInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%.WDOCASH %')
        ->get(),

      'bni' => BankBniInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%WDOCASH 1%')
        ->get(),

      'permata' => BankPermataInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', 'PB KE%')
        ->get(),

      'bri' => BankBriInquiry::query()
        ->where(function($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('description', 'like', 'WDOCASH 1%')
        ->where('debit', '>', 0)
        ->selectRaw("id, debit - $briCost AS debit, description")
        ->get(),

      'mandiri_giro' => BankMandiriGiroInquiry::query()
        ->where(function($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('reference_number', '<>', '')
        ->get()
    ];
  }

  public function getTransferUnreconciledInquiries() {
    return [
      'bca' => BankBcaInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->get(),

      'bri' => BankBriInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', 'WDOCASH 3%')
        ->get()
    ];
  }

  public function getBillerReplenishmentUnreconciledInquiries() {
    return [
      'bca' => BankBcaInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%.00VSREPL %')
        ->get()
    ];
  }

  public function getApiDisbursementUnreconciledInquiries() {
    return [
      'bca' => BankBcaInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%.00WDOCASH 2%')
        ->orderBy('id')
        ->get(),
      'bni' => BankBniInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%WDOCASH 2%')
        ->orderBy('id')
        ->get(),
      'cimb' => BankCimbInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', '%: ADIS%')
        ->get(),
      'permata' => BankPermataInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', 'PB KE%')
        ->get(),
      'bri' => BankBriInquiry::query()
        ->where(function ($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', 'WDOCASH 2%')
        ->get(),
      'mandiri_giro' => BankMandiriGiroInquiry::query()
        ->where(function($query) {
          $query->where('reference_type', null)
            ->orWhere('reference_type', '');
        })
        ->where('debit', '>', 0)
        ->where('description', 'like', 'WDOCASH 2%')
        ->get()
    ];
  }

  public function findByName($name) {
    $query = $this->getCloneModel();

    $query = $query->join('bank_scrape_accounts', 'bank_scrape_accounts.id', '=', 'bank_scrape_account_numbers.bank_scrape_account_id')
      ->where('bank_scrape_accounts.referred_target', $name);

    return $query->select('bank_scrape_accounts.*', 'bank_scrape_account_numbers.*', 'bank_scrape_account_numbers.id')->first();
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/09/18
 * Time: 16.12
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeAccount;

class BankScrapeAccountRepository extends Repository {

  function __construct(BankScrapeAccount $scrapeAccount) {
    $this->setModel($scrapeAccount);
  }

}
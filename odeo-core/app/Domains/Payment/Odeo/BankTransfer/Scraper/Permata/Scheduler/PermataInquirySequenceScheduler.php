<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 02/04/19
 * Time: 15.15
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;

class PermataInquirySequenceScheduler {

  /**
   * @var BankPermataInquiryRepository
   */
  private $inquiryRepo;

  /**
   * @var BankPermataInquiryBalanceSummaryRepository
   */
  private $summaryRepo;
  private $currSequence;
  private $currBalance;
  private $date;

  private function init() {
    $this->inquiryRepo = app()->make(BankPermataInquiryRepository::class);
    $this->summaryRepo = app()->make(BankPermataInquiryBalanceSummaryRepository::class);
    $this->currSequence = 0;
    $this->currBalance = 0;
    $this->date = Carbon::createFromDate(2019, 2, 1);
  }

  public function run() {
    $this->init();

//    if ($this->summaryRepo->findDistinctSummary()) {
//      return;
//    }

    if ($unassignedIq = $this->inquiryRepo->findFirstUnsignedSequenceInquiry()) {
      $this->date = $unassignedIq->post_date;

      if ($prevSequence = $this->inquiryRepo->findLatestSequenceByDate($unassignedIq->post_date->subday(1))) {
        $this->currSequence = $prevSequence->sequence_number;
        $this->currBalance = $prevSequence->balance;
      }
    }

    $inquiries = $this->inquiryRepo->getInquiryByDate($this->date);
    $hasUnsequenced = $inquiries->containsStrict(function($inquiry) {
      return $inquiry->sequence_number === null;
    });

    if ($hasUnsequenced) {

      $groupedInquiries = $this->groupInquiries($inquiries);
      $inquiriesCount = count($inquiries);
      $updates = [];

      for ($i = 0; $i < $inquiriesCount; $i++) {
        $selectedInquiry = $this->getInquiryByBalance($groupedInquiries, number_format($this->currBalance));
        if (!$selectedInquiry) {
          break;
        }

        $this->currSequence ++;
        $this->currBalance = $selectedInquiry['balance'];

        if ($selectedInquiry['sequence_number'] == $this->currSequence) {
          continue;
        }

        $updates[] = [
          'id' => $selectedInquiry['id'],
          'sequence_number' => $this->currSequence
        ];
      }

      if (count($updates)) {
        $this->inquiryRepo->updateBulk($updates);
      }

    }
  }

  private function groupInquiries($inquiries) {
    $groupInquiries = $inquiries->groupBy(function($inquiry) {
      return number_format($inquiry['balance'] - $inquiry['credit'] + $inquiry['debit']);
    });

    return $groupInquiries->transform(function($inquiries) {
      return $inquiries->sortBy('post_date');
    });
  }

  private function getInquiryByBalance(&$groupedInquiries, $balance) {
    if (!isset($groupedInquiries[$balance])) {
      return null;
    }

    $inquiries = $groupedInquiries[$balance];
    $selectedInquiry = $inquiries->shift();

    if (count($inquiries) == 0) {
      unset($groupedInquiries[$balance]);
    }
    return $selectedInquiry;
  }

}
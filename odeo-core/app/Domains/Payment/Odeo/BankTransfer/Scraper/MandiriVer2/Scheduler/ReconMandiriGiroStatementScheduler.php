<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/07/19
 * Time: 15.40
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Job\ReconMandiriAccountStatementJob;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;

class ReconMandiriGiroStatementScheduler {
  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $scrapeAccountNo;

  private function initialize() {
    $this->scrapeAccountNo = app()->make(BankScrapeAccountNumberRepository::class);
  }

  public function run() {
    $this->initialize();

    $number = $this->scrapeAccountNo->findByName('mandiri_giro');
    $startDate = $number->last_scrapped_date->subDay(1);
    $endDate = $this->getEndDate($startDate);

    dispatch(new ReconMandiriAccountStatementJob($startDate, $endDate, MandiriHelper::$Account_Giro, false));
  }

  private function getEndDate(Carbon $startDate) {
    if ($startDate->diffInDays(Carbon::now()) > 30) {
      return $startDate->copy()->addDays(30);
    }
    return Carbon::now();
  }
}
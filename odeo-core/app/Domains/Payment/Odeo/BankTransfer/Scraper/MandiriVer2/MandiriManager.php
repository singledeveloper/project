<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/09/18
 * Time: 17.57
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2;


use GuzzleHttp\Client;
use Odeo\Exceptions\FailException;
use Symfony\Component\DomCrawler\Crawler;

class MandiriManager {

  public function login(Client $client, $account) {
    $res = $client->get('https://mcm.bankmandiri.co.id/corp/common/login.do?action=logout');
    $res = $client->get('https://mcm.bankmandiri.co.id/corp/common/login.do?action=logout');
    $crawler = new Crawler($res->getBody()->getContents(),'https://mcm.bankmandiri.co.id/corp/common/login.do?action=logout');
    $form = $crawler->filter('form')->form();

    $res = $client->request('POST', 'https://mcm.bankmandiri.co.id/corp/common/login.do', [
      'query' => [
        'action' => 'login'
      ],
      'form_params' => [
        'org.apache.struts.taglib.html.TOKEN' => $form['org.apache.struts.taglib.html.TOKEN']->getValue(),
        'corpId' => $account->cooperate_id,
        'userName' => $account->user_id,
        'password' => sha1(md5($account->password)),
        'language' => 'en_US',
        'eTax' => $form['eTax']->getValue(),
      ]
    ]);

    $html = $res->getBody()->getContents();

    preg_match('/errorMessage=(.*)/', $html, $matches);
    if (isset($matches[1])) {
      throw new FailException($matches[1]);
    }

    $client->request('GET', 'https://mcm.bankmandiri.co.id/corp/common/login.do?action=menuRequest');
    $client->request('GET', 'https://mcm.bankmandiri.co.id/corp/common/login.do?action=mainRequest');
  }

  public function logout(Client $client) {
    
    $res = $client->request('GET', 'https://mcm.bankmandiri.co.id/corp/common/login.do', [
      'query' => [
        'action' => 'logout'
      ]
    ]);
    return $res->getBody()->getContents();
  }

}

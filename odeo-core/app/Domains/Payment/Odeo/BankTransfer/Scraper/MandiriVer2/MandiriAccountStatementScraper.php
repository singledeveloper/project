<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/09/18
 * Time: 18.13
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Symfony\Component\DomCrawler\Crawler;

class MandiriAccountStatementScraper {

  public function scrapeAccountStatement(Client $client, $startDate, $endDate, $accountNo) {
    $startDate = Carbon::parse($startDate);
    $endDate = Carbon::parse($endDate);

    $client->request('GET', 'https://mcm.bankmandiri.co.id/corp/front/balanceinquiry.do', [
      'query' => [
        'action' => 'balanceRequest',
        'menuCode' => 'MNU_GCME_040100'
      ]
    ]);

    $res = $client->request('POST', 'https://mcm.bankmandiri.co.id/corp/front/balanceinquiry.do', [
      'query' => [
        'action' => 'showSingleAccountNoShowDate',
        'accountPick' => 'single',
        'accountNumber' => $accountNo,
        'menu' => 'search',
      ],
    ]); // get balance

    $crawler = new Crawler($res->getBody()->getContents());
    $currentBalanceHtml = $crawler->filter('tr.clsEven > table.clsForm > tr.clsEven')->first()->filter('td')->getNode(6);
    
    if (!$currentBalanceHtml) {
      return [[], 0];
    }

    $scrapeCurrentBalance = str_replace(',', '', $currentBalanceHtml->textContent);

    $client->request('GET', 'https://mcm.bankmandiri.co.id/corp/front/transactioninquiry.do', [
      'query' => [
        'action' => 'transactionByDateRequest'
      ]
    ]);

    $scrapedInquiries = [];
    while (true) {
      for ($i = 1;; $i++) {
        $res = $client->request('POST', 'https://mcm.bankmandiri.co.id/corp/front/transactioninquiry.do', [
          'query' => [
            'action' => 'doCheckValidityAndShow',
            'day1' => $startDate->day,
            'mon1' => $startDate->month,
            'year1' => $startDate->year,
            'day2' => $startDate->day,
            'mon2' => $startDate->month,
            'year2' => $startDate->year,
            'accountNumber' => $accountNo,
            'type' => 'show',
            'pagingFlag' => 'Y'
          ],
          'form_params' => [
            'screenState' => 'TRX_DATE',
            'checkDate' => 'Y',
            'currentPage' => $i
          ]
        ]);

        $crawler = new Crawler($res->getBody()->getContents());
    
        $maxScrape = $crawler->filter('table.clsFormTrxStatus tr.clsEven')->count() - 2;
        $scrapeAccId = $this->getScrapeAccountNoId($accountNo);

        $crawler->filter('table.clsFormTrxStatus tr.clsEven')->each(function (Crawler $node, $i) use (&$scrapedInquiries, $maxScrape, $scrapeAccId) {
          if ($i < $maxScrape) {
            $elements = $node->filter('td');
            $balance = str_replace(',', '', $elements->eq(6)->text());
            $inquiry = [
              'date' => Carbon::createFromFormat('d/m/Y H:i:s', $elements->eq(0)->text()),
              'date_value' => Carbon::createFromFormat('d/m/Y', $elements->eq(1)->text())->startOfDay(),
              'bank_scrape_account_number_id' => $scrapeAccId,
              'description' => trim($elements->eq(2)->text()),
              'reference_number' => trim($elements->eq(3)->text()),
              'debit' => str_replace(',', '', $elements->eq(4)->text()),
              'credit' => str_replace(',', '', $elements->eq(5)->text()),
              'balance' => $balance == '' ? null : $balance,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
            ];
            $scrapedInquiries[] = $inquiry;
          }
        });

        $btnBar = $crawler->filter('table.clsFormTrxStatus tr:nth-last-child(3)')->first();

        // if ($startDate->startOfDay() < $endDate->startOfDay() && !isset($btnBar)) break;

        preg_match('!\d+!', $btnBar->text(), $maxPage);
        if ($i == $maxPage[0]) break;
      }

      if ($startDate->startOfDay() == $endDate->startOfDay()) break;
      $startDate->addDay(1);
    }

    return [$scrapedInquiries, $scrapeCurrentBalance];
  }

  private function getScrapeAccountNoId($accountNo) {
    switch ($accountNo) {
      case MandiriHelper::$Account_Default:
        return BankAccount::MANDIRI_ODEO;
      case MandiriHelper::$Account_Giro:
        return BankAccount::MANDIRI_GIRO;
    }
  }


}

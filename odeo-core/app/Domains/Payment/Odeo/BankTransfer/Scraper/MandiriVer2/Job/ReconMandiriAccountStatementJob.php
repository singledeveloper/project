<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/10/18
 * Time: 13.55
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Job;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Notification\Helper\InternalNoticer;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Job\SendMismatchMutationAlert;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper\MandiriHelper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\MandiriAccountStatementScraper;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\MandiriManager;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriInquiryBalanceSummaryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Model\BankScrapeInformation;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeInformationRepository;
use Odeo\Jobs\Job;

class ReconMandiriAccountStatementJob extends Job {

  const MAX_FAILED = 3;

  /**
   * The number of seconds the job can run before timing out.
   *
   * @var int
   */
  public $timeout = 300;

  /**
   * @var BankMandiriInquiryRepository
   */
  private $mandiriInquiryRepo;

  /**
   * @var BankMandiriInquiryBalanceSummaryRepository
   */
  private $mandiriInquirySummary;

  /**
   * @var MandiriManager
   */
  private $mandiriManager;

  /**
   * @var  MandiriAccountStatementScraper
   */
  private $scraper;

  /**
   * @var BankScrapeInformationRepository
   */
  private $bankScrapeInformationRepo;

  /**
   * @var BankScrapeAccountNumberRepository
   */
  private $bankScrapeAccountNumber;

  /**
   * @var BankScrapeInformation
   */
  private $information;

  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var Carbon
   */
  private $startDate, $endDate;

  private $shouldRecon, $accountNo, $bankName;

  public function __construct($startDate, $endDate, $accountNo, $shouldRecon = false) {
    parent::__construct();
    $this->startDate = $startDate;
    $this->endDate = $endDate;
    $this->shouldRecon = $shouldRecon;
    $this->accountNo = $accountNo;
    $this->bankName = $this->getActiveBankName($this->accountNo);
  }

  private function initialize() {
    $this->mandiriInquiryRepo = $this->getBankInquiryRepo($this->accountNo);
    $this->mandiriInquirySummary = $this->getBankSummaryRepo($this->accountNo);
    $this->mandiriManager = app()->make(MandiriManager::class);
    $this->scraper = app()->make(MandiriAccountStatementScraper::class);
    $this->bankScrapeInformationRepo = app()->make(BankScrapeInformationRepository::class);
    $this->bankScrapeAccountNumber = app()->make(BankScrapeAccountNumberRepository::class);
    $this->redis = Redis::connection();

    $this->information = $this->bankScrapeInformationRepo->getActiveAccount($this->bankName);
  }

  public function handle() {
    $this->initialize();
    $this->handleInternalNotice();

    if ($backoff = $this->backoff()) {
      clog('recon_mandiri', 'backing off ' . $this->bankName . ', backoff=' . $backoff);
      return;
    }

    $this->switchAccount();
    $this->run();
  }

  private function switchAccount() {
    $count = $this->redis->get('odeo_core:mandiri_scraper_active_account:' . $this->bankName);
    $numAccount = count($this->information->accounts);
    $nextAccountIdx = ($count + 1) % $numAccount;

    $this->redis->set('odeo_core:mandiri_scraper_active_account:' . $this->bankName, $nextAccountIdx);

    clog('recon_mandiri', 'switching mandiri account, account=' . $nextAccountIdx);
  }

  private function getActiveAccount() {
    return $this->information->accounts[$this->redis->get('odeo_core:mandiri_scraper_active_account:' . $this->bankName)];
  }

  private function backoff()
  {
    $backoff = $this->redis->get('odeo_core:mandiri_scraper_failed_backoff:' . $this->bankName);
    if (!$backoff) {
      $this->redis->set('odeo_core:mandiri_scraper_failed_backoff:' . $this->bankName, 2);
      return 0;
    }

    $this->redis->set('odeo_core:mandiri_scraper_failed_backoff:' . $this->bankName, $backoff - 1);
    return $backoff;
  }

  private function clearBackoff()
  {
    $this->redis->set('odeo_core:mandiri_scraper_failed_backoff:' . $this->bankName, 0);
  }

  private function clearFailed()
  {
    $this->redis->set('odeo_core:mandiri_scraper_failed_count:' . $this->bankName, 0);
  }

  private function incrementFailed()
  {
    $failed = $this->redis->get('odeo_core:mandiri_scraper_failed_count:' . $this->bankName);
    $failed = min(self::MAX_FAILED, $failed + 1);

    if ($failed == self::MAX_FAILED) {
      $this->clearFailed();
      $this->clearBackoff();
    } else {
      $backoff = pow(2, $failed);
      
      clog('recon_mandiri', 'starting back off ' . $this->bankName . ', backoff=' . $backoff);
      $this->redis->set('odeo_core:mandiri_scraper_failed_count:' . $this->bankName, $failed);
      $this->redis->set('odeo_core:mandiri_scraper_failed_backoff:' . $this->bankName, $backoff);
    }
  }

  private function run() {
    if (!$this->redis->setnx(MandiriHelper::MANDIRI_REDIS_KEY . ':' . $this->bankName, 1)) {
      return;
    }
    $this->redis->expire(MandiriHelper::MANDIRI_REDIS_KEY . ':' . $this->bankName, 5 * 60);

    $client = new Client([
      'headers' => [
        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding' => 'gzip, deflate, br',
        'Accept-Language' => 'en-US,en;q=0.5',
        'Cache-Control' => 'no-cache',
        'Connection' => 'keep-alive',
        'Pragma' => 'no-cache',
        'DNT' => 1,
        'Upgrade-Insecure-Requests' => 1,
        'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:68.0) Gecko/20100101 Firefox/68.0'
      ],
      'curl' => [
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false
      ],
      'cookies' => true, 
      'timeout' => 60
    ]);

    $scrapeAccount = json_decode($this->getActiveAccount()->account);
    clog('recon_mandiri', json_encode($scrapeAccount));
    
    try {
      $this->mandiriManager->login($client, $scrapeAccount);
      list($scrapeInquiries, $scrapeCurrBalance) = $this->scraper->scrapeAccountStatement($client, $this->startDate, $this->endDate, $this->accountNo);

      if ($this->isScrapeSuccessful($scrapeInquiries, $scrapeCurrBalance)) {
        $existingInquiries = $this->mandiriInquiryRepo->getInquiries($this->startDate, $this->endDate);

        $groupedScrapeInq = $this->groupInquiries($scrapeInquiries);
        $groupedExistingInq = $this->groupInquiries($existingInquiries);
  
        $newInquiries = $updates = $removeIds = [];
        $netMovement = 0;
  
        foreach ($groupedScrapeInq as $key => $scrapeInquiries) {
          foreach ($scrapeInquiries as $inquiry) {
            // Update existing inquiry
            if ($existingInquiry = $this->getInquiryByKey($groupedExistingInq, $key)) {
              $hasUpdates = (!is_null($existingInquiry['balance']) &&
                  MandiriHelper::formatNumber($existingInquiry['balance']) != $inquiry['balance']) ||
                $this->removeWhitespace($existingInquiry['description']) != $this->removeWhitespace($inquiry['description']);
              if ($hasUpdates) {
                $updates[] = [
                  'id' => $existingInquiry['id'],
                  'credit' => MandiriHelper::formatNumber($inquiry['credit']),
                  'debit' => MandiriHelper::formatNumber($inquiry['debit']),
                  'balance' => $inquiry['balance'],
                  'description' => $inquiry['description'],
                  'updated_at' => Carbon::now()
                ];
              }
              continue;
            }
            $newInquiries[] = $inquiry;
            $netMovement += $inquiry['credit'] - $inquiry['debit'];
          }
        }
  
        foreach ($groupedExistingInq as $key => $existingInquiries) {
          foreach ($existingInquiries as $inquiry) {
            $removeIds[] = $inquiry['id'];
            $netMovement -= $inquiry['credit'] - $inquiry['debit'];
          }
        }
  
        $latestSummary = $this->mandiriInquirySummary->findLatestSummary();
  
        $newSummary = $this->mandiriInquirySummary->getNew();
        $newSummary->balance_change = $netMovement;
        $newSummary->previous_balance = $latestSummary->balance ?? 0;
        $newSummary->scrape_current_balance = $scrapeCurrBalance;
        $newSummary->scrape_start_date = $this->startDate;
        $newSummary->scrape_end_date = $this->endDate;
        $newSummary->balance = isset($latestSummary) ? $latestSummary->balance + $netMovement : $scrapeCurrBalance;
        list($newSummary->status, $newSummary->first_distinct_id) = $this->getSummaryStatus($latestSummary, $newSummary);
  
        if ($newSummary->status == MandiriHelper::DIFF_OK && $this->mandiriInquirySummary->findDistinctSummary()) {
          $this->mandiriInquirySummary->updateDistinctRows();
        }
  
        if (count($updates)) {
          $this->mandiriInquiryRepo->updateBulk($updates);
        }
        if (count($newInquiries)) {
          $this->mandiriInquiryRepo->saveBulk($newInquiries);
        }
        if (count($removeIds)) {
          $this->mandiriInquiryRepo->deleteById($removeIds);
        }

        if ($this->shouldRecon) {
          $this->mandiriInquirySummary->save($newSummary);
        }

        $account = $this->bankScrapeAccountNumber->findByName($this->bankName);
        clog('recon_mandiri', json_encode($account));
        clog('recon_mandiri', $this->bankName);
        $account->last_scrapped_date = Carbon::now();
        // $this->endDate; // only for rescraping, change to Carbon::now()
        $account->last_time_scrapped = Carbon::now();
        $this->bankScrapeAccountNumber->save($account);
      }

      $this->clearFailed();
      $this->resetErrorCount();
    } catch(RequestException $e) {
      clog('recon_mandiri', Psr7\str($e->getRequest()));
      if ($e->hasResponse()) {
        clog('recon_mandiri', Psr7\str($e->getResponse()));
      }
    } catch (\Exception $e) {
      $this->incrementFailed();
      clog('recon_mandiri', 'err msg: ' . $e->getMessage());
      clog('recon_mandiri', 'err trace: ' . $e->getTraceAsString());
    } finally {
      $this->mandiriManager->logout($client);
      $this->redis->del(MandiriHelper::MANDIRI_REDIS_KEY . ':' . $this->bankName);
      clog('recon_mandiri', 'logout ' . $this->bankName);
    }

    if ($this->shouldRecon && $this->mandiriInquirySummary->findDistinctSummary()) {
      if ($this->startDate >= Carbon::now()->subDay(28)) {
        dispatch(new self($this->startDate->subDay(2), $this->endDate->subDay(2), $this->accountNo, true));
      } else {
        dispatch(new SendMismatchMutationAlert($this->bankName, $this->startDate));
      }
    }
  }

  private function getSummaryStatus($prevSummary, $summary) {
    if ($summary->balance != $summary->scrape_current_balance) {
      if ($prevSummary->status != MandiriHelper::DIFF_OK) {
        return [MandiriHelper::DIFF_UNKNOWN, $prevSummary->first_distinct_id ?? $prevSummary->id];
      }
      return [MandiriHelper::DIFF_UNKNOWN, null];
    }
    return [MandiriHelper::DIFF_OK, null];
  }

  private function groupInquiries($inquiries) {
    $inquiries = collect($inquiries);
    $groupInquiries = $inquiries->groupBy(function ($inquiry) {
      return str_replace(' ', '', $inquiry['date'] . $inquiry['reference_number'] . MandiriHelper::formatNumber($inquiry['debit']) . MandiriHelper::formatnumber($inquiry['credit']));
    });

    return $groupInquiries->transform(function ($inquiries) {
      return $inquiries->sortBy('date');
    });
  }

  private function getInquiryByKey(&$groupedInquiries, $key) {
    if (!isset($groupedInquiries[$key])) {
      return null;
    }

    $inquiries = $groupedInquiries[$key];
    $selectedInquiry = $inquiries->shift();

    if (count($inquiries) == 0) {
      unset($groupedInquiries[$key]);
    }

    return $selectedInquiry;
  }

  private function removeWhitespace($string) {
    return preg_replace('/\s*/m', '', $string);
  }

  private function resetErrorCount($count = 0) {
    $this->redis->set(MandiriHelper::MANDIRI_REDIS_ERROR_COUNT . ':' . $this->bankName, $count);
  }

  private function handleInternalNotice() {
    $count = $this->redis->get(MandiriHelper::MANDIRI_REDIS_ERROR_COUNT . ':' . $this->bankName) ?? 0;
    $scrapeAccount = json_decode($this->getActiveAccount()->account);

    if ($count == 14) {
      $noticer = app()->make(InternalNoticer::class);
      $noticer->saveMessage(strtoupper($this->bankName) . ' Scrape Failed: ' . $scrapeAccount->user_id . ' failed to scrape in the last 15 minutes.');
      $this->resetErrorCount();
    } else {
      $count += 1;
      $this->resetErrorCount($count);
    }
  }

  private function isScrapeSuccessful($scrapeInquiries, $scrapeBalance) {
    return !empty($scrapeInquiries) && $scrapeBalance;
  }

  private function getBankInquiryRepo($accountNo) {
    switch ($accountNo) {
      case MandiriHelper::$Account_Giro:
        return app()->make(BankMandiriGiroInquiryRepository::class);
      case MandiriHelper::$Account_Default:
      default:
        return app()->make(BankMandiriInquiryRepository::class);
    }
  }

  private function getBankSummaryRepo($accountNo) {
    switch ($accountNo) {
      case MandiriHelper::$Account_Giro:
        return app()->make(BankMandiriGiroInquiryBalanceSummaryRepository::class);
      case MandiriHelper::$Account_Default:
      default:
        return app()->make(BankMandiriInquiryBalanceSummaryRepository::class);
    }
  }

  private function getActiveBankName($accountNo) {
    switch ($accountNo) {
      case MandiriHelper::$Account_Giro:
        return 'mandiri_giro';
      case MandiriHelper::$Account_Default:
      default:
        return 'mandiri';
    }
  }

}

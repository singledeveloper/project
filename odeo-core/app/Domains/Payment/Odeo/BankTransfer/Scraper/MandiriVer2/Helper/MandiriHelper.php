<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/09/18
 * Time: 19.17
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Helper;


class MandiriHelper {

  static $Account_Default = '1650088664421';
  static $Account_Giro = '1650088668877';
  const DIFF_OK = '50000';
  const DIFF_UNKNOWN = '90000';
  const MANDIRI_REDIS_KEY = 'odeo_core:mandiri_recon_account_statement';
  const MANDIRI_REDIS_ERROR_COUNT = 'odeo_core:mandiri_recon_error_count';

  static function formatNumber($number) {
    return number_format($number, 2, '.', '');
  }

}
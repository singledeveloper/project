<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:41 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Payment\Jobs\SendPaymentPendingBankTransfer;
use Odeo\Domains\Payment\Jobs\EmailHelper;
use Odeo\Domains\Core\Task;

class PaymentOpenRequester extends PaymentManager {
  use EmailHelper;


  public function __construct() {
    parent::__construct();

  }

  public function open(PipelineListener $listener, $order, $paymentInformation, $data) {

    if ($order->status != OrderStatus::CREATED) return $listener->response(400, 'the payment has been opened');

    $order->status = OrderStatus::OPENED;
    $order->opened_at = date('Y-m-d H:i:s');

    $this->orders->save($order);
//    $email = $this->getEmail($order, $data);
//    $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
//      'email' => $email,
//      'job' => new SendPaymentInvoice($order, $paymentInformation, $data)
//    ]));

    $email = $this->getEmail($order, $data);
    $data['opc'] = $order->payment->opc;
    $listener->addNext(new Task(CustomerEmailManager::class, 'pushJobIfEmailVerified', [
      'email' => $email,
      'job' => new SendPaymentPendingBankTransfer($order, $data)
    ]));

    return $listener->response(200);
  }
}

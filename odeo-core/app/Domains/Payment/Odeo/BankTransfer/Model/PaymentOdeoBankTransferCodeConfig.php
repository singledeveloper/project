<?php

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Model;


use Odeo\Domains\Core\Entity;

class PaymentOdeoBankTransferCodeConfig extends Entity
{

  public function codes()
  {
    return $this->hasMany(PaymentOdeoBankTransferCode::class, 'config_id', 'id');
  }
}

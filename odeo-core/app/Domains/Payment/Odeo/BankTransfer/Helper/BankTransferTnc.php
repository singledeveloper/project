<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/6/17
 * Time: 8:22 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer\Helper;

use Odeo\Domains\Constant\Payment;

class BankTransferTnc {

  public function getTnc($infoId) {

    switch ($infoId) {

      case Payment::OPC_GROUP_BANK_TRANSFER_BCA:
        return [
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_bca_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_2'),
          ]
        ];
        break;


      case Payment::OPC_GROUP_BANK_TRANSFER_MANDIRI:
        return [
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_mandiri_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_2'),
          ]
        ];
        break;

      case Payment::OPC_GROUP_BANK_TRANSFER_BRI:
        return [
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_bri_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_2'),
          ]
        ];
        break;

      case Payment::OPC_GROUP_BANK_TRANSFER_CIMB:
        return [
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_cimb_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_2'),
          ]
        ];
        break;

      case Payment::OPC_GROUP_BANK_TRANSFER_BNI:
        return [
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_2'),
          ]
        ];
        break;

      case Payment::OPC_GROUP_BANK_TRANSFER_PERMATA:
        return [
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_1'),
          ],
          [
            '1_text' => trans('payment/bank_transfer.tnc_bank_transfer_2'),
          ]
        ];
        break;

      
    }

    return [];

  }

}

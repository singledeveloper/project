<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:41 PM
 */

namespace Odeo\Domains\Payment\Odeo\BankTransfer;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;
use Odeo\Domains\Payment\Odeo\BankTransfer\Repository\PaymentOdeoBankTransferDetailRepository;

class PaymentConfirmator extends PaymentManager {


  private $details;

  public function __construct() {
    parent::__construct();

    $this->details = app()->make(PaymentOdeoBankTransferDetailRepository::class);

  }


  public function confirm(PipelineListener $listener, $order, $paymentInformation, $data) {

    if ($order->status == OrderStatus::OPENED) {

      $order->status = OrderStatus::CONFIRMED;
      $order->paid_at = date('Y-m-d H:i:s');

      $this->orders->save($order);

    }

    $detail = $this->details->getNew();

    $detail->account_number = $data['confirmation_number'] ?? null;
    $detail->account_name = $data['confirmation_name'] ?? null;
    $detail->transfer_amount = $data['confirmation_amount'];
    $detail->is_auto_confirm_transfer = $data['is_auto_confirm_transfer'] ?? false;

    if (isset($data['confirmation_description'])) {
      $detail->confirmation_description = $data['confirmation_description'];
    }

    $this->details->save($detail);

    $payment = $this->payments->findByOrderId($order->id);

    $payment->reference_id = $detail->id;

    $this->payments->save($payment);

    return $listener->response(200);
  }
}
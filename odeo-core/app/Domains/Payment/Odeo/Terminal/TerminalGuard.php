<?php

namespace Odeo\Domains\Payment\Odeo\Terminal;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Affiliate\Repository\AffiliateUserInvoiceRepository;
use Odeo\Domains\Order\Repository\OrderDetailRepository;
use Odeo\Domains\Order\Repository\OrderRepository;
use Odeo\Domains\Payment\Odeo\Terminal\Repository\UserTerminalRepository;

class TerminalGuard {

  private $userTerminalRepo;
  private $orderRepo;
  private $orderDetailRepo;
  private $affiliateUserInvoiceRepo;

  public function __construct() {
    $this->userTerminalRepo = app()->make(UserTerminalRepository::class);
    $this->orderRepo = app()->make(OrderRepository::class);
    $this->orderDetailRepo = app()->make(OrderDetailRepository::class);
    $this->affiliateUserInvoiceRepo = app()->make(AffiliateUserInvoiceRepository::class);
  }

  public function guard(PipelineListener $listener, $data) {
    $terminalUser = $this->userTerminalRepo->findActiveByTerminalId($data['terminal_id']);
    if (!$terminalUser) {
      return $listener->response(401, 'Terminal is not registered yet.');
    }

    return $listener->response(200, [
      'user_id' => $terminalUser->user_id,
      'type' => UserType::SELLER,
      'platform_id' => Platform::TERMINAL,
      'user_invoice_biller_id' => $terminalUser->user_invoice_biller_id,
    ]);
  }

  public function validateOrderUser(PipelineListener $listener, $data) {
    $order = $this->orderRepo->findById($data['order_id']);
    if (!$order || !$this->canAccess($order, $data)) {
      return $listener->response(400, "You don't have access to this order.");
    }
    return $listener->response(200);
  }

  private function canAccess($order, $data) {
    if ($order->user_id != getUserId($data)) {
      return false;
    }

    $detail = $this->orderDetailRepo->findByOrderId($data['order_id']);
    if (!$detail) {
      return false;
    }

    switch ($detail->service_detail_id) {
      case ServiceDetail::USER_INVOICE_ODEO:
        if ($order->status != OrderStatus::COMPLETED) {
          break;
        }

        $inv = $this->affiliateUserInvoiceRepo->findByOrderId($data['order_id']);
        return $inv && preg_replace('/^0/', '62', $inv->billed_user_telephone) == $data['phone_number'];
    }

    return $order->phone_number == $data['phone_number'];
  }
}
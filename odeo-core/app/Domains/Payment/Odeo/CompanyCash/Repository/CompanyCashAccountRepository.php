<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/06/18
 * Time: 15.35
 */

namespace Odeo\Domains\Payment\Odeo\CompanyCash\Repository;


use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Odeo\CompanyCash\Model\CompanyCashAccount;

class CompanyCashAccountRepository extends Repository {

  public function __construct(CompanyCashAccount $companyCashAccount) {
    $this->model = $companyCashAccount;
  }

  public function gets() {
    return $this->getResult($this->getCloneModel()
      ->join('users', 'users.id', '=', 'company_cash_accounts.user_id')
      ->select('company_cash_accounts.*', 'users.telephone as user_telephone', 'users.name as user_name')
      ->orderBy('users.telephone', 'asc')
    );
  }

  public function transactions($id) {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();
    $query = $query->where('company_cash_accounts.id', '=', $id)
      ->where('cash_type', '=', CashType::OCASH)
      ->join('cash_transactions', 'cash_transactions.user_id', '=', 'company_cash_accounts.user_id');

    if (isset($filters['start_date'])) $query->where('created_at', '>=', $filters['start_date']);
    if (isset($filters['end_date'])) $query->where('created_at', '<=', $filters['end_date']);

    if (isset($filters['search'])) {
      if (isset($filters['search']['id'])) {
        $query->where('cash_transactions.id', $filters['search']['id']);
      }

      if (isset($filters['search']['data'])) {
        $query->where('data', 'ilike', '%' . $filters['search']['data'] . '%');
      }

      if (isset($filters['search']['trx_type'])) {
        $query->where('trx_type', 'ilike', '%' . $filters['search']['trx_type'] . '%');
      }
    }

    $result = $this->getResult(
      $query->select('cash_transactions.id', 'trx_type',
        \DB::raw('CASE WHEN amount > 0 THEN amount ELSE 0 END AS credit'),
        \DB::raw('CASE WHEN amount < 0 THEN abs(amount) ELSE 0 END AS debit'),
        'balance_after',
        'data',
        'order_id',
        'cash_transactions.created_at :: date as date')
      ->orderBy('id', 'desc')
    );

    return $result;
  }

  public function findByTransactionId($transactionId) {
    return $this->getCloneModel()
      ->join('company_cash_account_transactions', 'company_cash_account_transactions.company_cash_account_id', '=', 'company_cash_accounts.id')
      ->where('company_cash_account_transactions.id', '=', $transactionId)
      ->select('company_cash_accounts.*')
      ->first();

  }

  public function findByUserId($userId) {
    return $this->getCloneModel()->where('user_id', $userId)->first();
  }

}
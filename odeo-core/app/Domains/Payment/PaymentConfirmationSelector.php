<?php

namespace Odeo\Domains\Payment;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\PaymentManager;

class PaymentConfirmationSelector extends PaymentManager {

  private $bankTransferSelector;

  public function __construct() {
    parent::__construct();
    $this->bankTransferSelector = app()->make(\Odeo\Domains\Payment\Odeo\BankTransfer\PaymentConfirmationSelector::class);
  }

  public function getDetail(PipelineListener $listener, $data) {
    $payment = $this->payments->findByOrderId($data['order_id']);

    if ($this->paymentValidator->isBankTransfer($payment->info_id)) {
      return $this->bankTransferSelector->getDetail($listener, $data, $payment->reference_id);
    }

    return $listener->response(400);
  }
}

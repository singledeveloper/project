<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.53
 */

namespace Odeo\Domains\Payment\Maybank\Va\Model;


use Odeo\Domains\Core\Entity;
use Odeo\Domains\Payment\Model\Payment;

class PaymentMaybankVaPayment extends Entity {

  public function payment() {
    return $this->belongsTo(Payment::class);
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/10/19
 * Time: 10.53
 */

namespace Odeo\Domains\Payment\Maybank\Va\Repository;


use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Maybank\Va\Model\PaymentMaybankVaInquiry;

class PaymentMaybankVaInquiryRepository extends Repository {

  function __construct(PaymentMaybankVaInquiry $vaInquiry) {
    $this->model = $vaInquiry;
  }

  function findByVaNo($vaNo) {
    return $this->model
      ->where('va_no', $vaNo)
      ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
      ->orderBy('id', 'DESC')
      ->first();
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 20/08/19
 * Time: 13.17
 */

namespace Odeo\Domains\Payment\Permata\Va\Repository;


use Odeo\Domains\Constant\PaymentGateway;
use Odeo\Domains\Constant\PermataVaPaymentStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Permata\Va\Model\PaymentPermataVaPayment;

class PaymentPermataVaPaymentRepository extends Repository {

  function __construct(PaymentPermataVaPayment $vaPayment) {
    $this->model = $vaPayment;
  }

  public function get() {
    $filters = $this->getFilters();
    $q = $this->model
      ->join('payments', 'payment_permata_va_payments.payment_id', '=', 'payments.id');

    if (isset($filters['search'])) {
      $search = $filters['search'];

      if (isset($search['id'])) {
        $q = $q->where('payment_permata_va_payments.id', $search['id']);
      }
      if (isset($search['inquiry_id'])) {
        $q = $q->where('payment_permata_va_payments.inquiry_id', $search['inquiry_id']);
      }
      if (isset($search['va_number'])) {
        $q = $q->where('payment_permata_va_payments.va_number', $search['va_number']);
      }
      if (isset($search['status'])) {
        $q = $q->where('payment_permata_va_payments.status', $search['status']);
      }
      if (isset($search['reference_number'])) {
        $q = $q->where('payment_permata_va_payments.reference_number', $search['reference_number']);
      }
    }

    $q = $q->select(
      'payment_permata_va_payments.id',
      'payment_permata_va_payments.inquiry_id',
      'payment_permata_va_payments.instcode',
      'payment_permata_va_payments.va_number',
      'payment_permata_va_payments.trace_no',
      'payment_permata_va_payments.trn_date',
      'payment_permata_va_payments.bill_amount',
      'payment_permata_va_payments.del_channel',
      'payment_permata_va_payments.status',
      'payment_permata_va_payments.reference_number',
      'payments.source_id',
      'payments.source_type'
    );

    return $this->getResult($q->orderBy('payment_permata_va_payments.id', 'DESC'));
  }

  function findByPaidVaCode($vaCode) {
    return $this->model
      ->where('va_number', $vaCode)
      ->whereDate('created_at', date('Y-m-d'))
      ->whereIn('status', [
        PaymentGateway::PENDING,
        PaymentGateway::WAITING_FOR_ACCEPTANCE,
        PaymentGateway::ACCEPTED,
        PaymentGateway::SUCCESS
      ])
      ->orderBy('id', 'DESC')
      ->first();
  }

  function findDuplicateRef($referenceNumber) {
    return $this->model
      ->where('reference_number', $referenceNumber)
      ->first();
  }

  function findPaymentByVaNumberAndTraceNo($vaNumber, $traceNo) {
    return $this->model
      ->where('trace_no', $traceNo)
      ->where('va_number', $vaNumber)
      ->first();
  }

  function getPendingPayment() {
    return $this->model->whereIn('status', [
      PermataVaPaymentStatus::ACCEPTED,
      PermataVaPaymentStatus::PENDING,
      PermataVaPaymentStatus::FAILED_NEED_UPDATE,
      PermataVaPaymentStatus::SUSPECT_FOR_TESTING,
      PermataVaPaymentStatus::SUSPECT_TIMEOUT,
      PermataVaPaymentStatus::SUSPECT_NEED_STATUS_RECHECK,
      PermataVaPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK
    ])->get();
  }

}
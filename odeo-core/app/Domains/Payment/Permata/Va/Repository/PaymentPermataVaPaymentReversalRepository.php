<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 21/08/19
 * Time: 17.24
 */

namespace Odeo\Domains\Payment\Permata\Va\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Permata\Va\Model\PaymentPermataVaPaymentReversal;

class PaymentPermataVaPaymentReversalRepository extends Repository {

  function __construct(PaymentPermataVaPaymentReversal $vaReversal) {
    $this->model = $vaReversal;
  }

}
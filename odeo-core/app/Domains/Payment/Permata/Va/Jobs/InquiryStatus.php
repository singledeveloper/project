<?php

namespace Odeo\Domains\Payment\Permata\Va\Jobs;

use Carbon\Carbon;
use Odeo\Jobs\Job;
use Odeo\Domains\Constant\PermataVaPaymentStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Permata\Repository\BankPermataInquiryRepository;
use Odeo\Domains\Payment\Permata\Va\Jobs\Reconciler;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Illuminate\Support\Facades\Redis;

class InquiryStatus extends Job {

  private $vaPayment, $permataInquiry, $inquiry, $payment, $redis;

  public function __construct($vaPayment) {
    parent::__construct();
    $this->vaPayment = $vaPayment;
  }

  private function init() {
    $this->permataInquiry = app()->make(BankPermataInquiryRepository::class);
    $this->inquiry = app()->make(PaymentPermataVaInquiryRepository::class);
    $this->payment = app()->make(PaymentRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $this->init();

    $key = 'odeo_core:permata_change_status_' . $this->vaPayment->id;

    if (!$this->redis->setnx($key, 1)) {
      clog('permata_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Status locked');
      return null;
    };
    $this->redis->expire($key, 5 * 60);

    $inquiry = $this->inquiry->findById($this->vaPayment->inquiry_id);
    if (!$inquiry) {
      clog('permata_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Not found');
      return null;
    }

    $payment = $this->payment->findById($this->vaPayment->payment_id);
    if (!$payment) {
      clog('permata_va', 'ID: ' . $this->vaPayment->id . ' Payment Not found');
      return null;
    }
    
    $bankInquiry = $this->permataInquiry->getUnreconciledVaPayment($this->vaPayment->va_number);

    if ($bankInquiry) {
      $this->vaPayment->status = PermataVaPaymentStatus::SUCCESS;
      $this->vaPayment->save();
  
      $pipeline = new Pipeline;
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
      $pipeline->execute([
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'pg_payment_status' => PermataVaPaymentStatus::getPaymentGatewayStatus(PermataVaPaymentStatus::SUCCESS),
        'receive_notify_time' => Carbon::now(),
        'vendor_reference_id' => $this->vaPayment->reference_number,
      ]);
      
      if ($pipeline->fail()) {
        clog('permata_va', 'ID: ' . $this->vaPayment->id . ' Inquiry Status Fail Notify: ' . $pipeline->errorMessage);
        return null;
      }

      dispatch(new Reconciler([
        'bank_inquiry_id' => $bankInquiry->id,
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'va_payment_id' => $this->vaPayment->id
      ]));
    }

    $this->redis->del($key);
  }
}
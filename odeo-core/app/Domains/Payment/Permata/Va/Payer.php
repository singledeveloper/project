<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 20/08/19
 * Time: 13.28
 */

namespace Odeo\Domains\Payment\Permata\Va;


use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Constant\PermataVaPaymentStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaInquiryRepository;
use Odeo\Domains\Payment\Permata\Va\Repository\PaymentPermataVaPaymentRepository;
use Odeo\Domains\Payment\Repository\PaymentOdeoPaymentChannelRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class Payer {

  function __construct() {
    $this->payment = app()->make(PaymentRepository::class);
    $this->paymentChannel = app()->make(PaymentOdeoPaymentChannelRepository::class);
    $this->permataVaPayment = app()->make(PaymentPermataVaPaymentRepository::class);
    $this->permataVaInquiry = app()->make(PaymentPermataVaInquiryRepository::class);
    $this->pgUserPaymentChannel = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
    $this->redis = Redis::connection();
  }

  public function vaPayment(PipelineListener $listener, $data) {
    if (!isset($data['PayBillRq'])) {
      return $listener->response(400, ['message' => 'Unknown data format', 'code'=> '01']);
    }
    $data = $data['PayBillRq'];

    $inquiry = $this->permataVaInquiry->findByVaCode($data['VI_VANUMBER']);
    if (!$inquiry || $inquiry->created_at->diffInMinutes(Carbon::now()) > 10) {
      return $listener->response(400, ['message' => 'inquiry not found', 'code' => '14']);
    }

    if ($inquiry->bill_amount != $data['BILL_AMOUNT']) {
      return $listener->response(400, ['message' => 'invalid payment amount', 'code' => '13']);
    }

    $vaPayment = $this->permataVaPayment->findDuplicateRef($inquiry->reference_number);
    if ($vaPayment) {
      return $listener->response(400, ['message' => 'duplicate transaction', 'code' => '88']);
    }

    $currentDate = Carbon::now()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($currentDate);
    if (!$this->redis->hsetnx($namespace, $inquiry->id, 1)) {
      return $listener->response(400, ['message' => 'duplicate transaction', 'code' => '88']);
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    $tomorrowDate = Carbon::tomorrow()->format('Y-m-d');
    $namespace = $this->getRedisNamespace($tomorrowDate);
    if (!$this->redis->hset($namespace, $inquiry->id, 1)) {
      return $listener->response(400, ['message' => 'duplicate transaction', 'code' => '88']);
    }
    $this->redis->expire($namespace, 2 * 24 * 60 * 60);

    $pgUserChannel = $this->pgUserPaymentChannel->findByActivePrefix($data['VI_VANUMBER']);
    if (!$pgUserChannel) {
      return $listener->response(400, ['message' => trans('payment.unrecognized_va_number'), 'code' => '14']);
    }

    $channel = $this->paymentChannel->findByGatewayIdAndGroupId(Payment::PAYMENT_GATEWAY, $pgUserChannel->payment_group_id);
    if (!$channel) {
      return $listener->response(400, ['message' => 'channel not found', 'code' => '14']);
    }

    $vaPayment = $this->permataVaPayment->getNew();
    $vaPayment->inquiry_id = $inquiry->id;
    $vaPayment->instcode = $data['INSTCODE'];
    $vaPayment->va_number = $data['VI_VANUMBER'];
    $vaPayment->trace_no = $data['VI_TRACENO'];
    $vaPayment->trn_date = $data['VI_TRNDATE'];
    $vaPayment->bill_amount = $data['BILL_AMOUNT'];
    $vaPayment->del_channel = $data['VI_DELCHANNEL'];
    $vaPayment->ref_info = isset($data['RefInfo']) ? json_encode($data['RefInfo']) : '';
    $vaPayment->cost = $pgUserChannel->cost;
    $vaPayment->reference_number = $inquiry->reference_number;
    $vaPayment->status = PermataVaPaymentStatus::PENDING;
    $this->permataVaPayment->save($vaPayment);

    $payment = $this->payment->getNew();
    $payment->source_type = PaymentChannel::PAYMENT_GATEWAY;
    $payment->info_id = $pgUserChannel->payment_group_id;
    $payment->opc = $channel->code;
    $payment->vendor_id = $vaPayment->id;
    $payment->vendor_type = 'va_permata';
    $this->payment->save($payment);

    return $listener->response(200, [
      'permata_payment_id' => $vaPayment->id,
      'payment_id' => $payment->id,
      'va_code' => $data['VI_VANUMBER'],
      'va_prefix' => $pgUserChannel->prefix,
      'user_id' => $pgUserChannel->user_id,
      'billed_amount' => $data['BILL_AMOUNT'],
      'payment_group_id' => $pgUserChannel->payment_group_id,
      'customer_name' => $inquiry->cust_name,
      'vendor_reference_id' => $vaPayment->reference_number
    ]);

  }

  public function getRedisNamespace($date) {
    return "odeo_core:permata_used_inquiry_id_$date";
  }

  public function updatePayment(PipelineListener $listener, $data) {
    $vaPayment = $this->permataVaPayment->findById($data['permata_payment_id']);
    $vaPayment->payment_id = $data['payment_id'];
    $vaPayment->status = $data['pg_payment_status'];
    $this->permataVaPayment->save($vaPayment);

    return $listener->response(200);
  }

}
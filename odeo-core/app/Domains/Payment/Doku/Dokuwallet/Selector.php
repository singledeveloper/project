<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 10/31/16
 * Time: 11:59 PM
 */

namespace Odeo\Domains\Payment\Doku\Dokuwallet;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;

class Selector {

  private $dokuWalet;

  public function __construct() {
    $this->dokuWalet = app()->make(\Odeo\Domains\Payment\Doku\Dokuwallet\Repository\PaymentDokuWalletPaymentRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->dokuWalet->normalizeFilters($data);

    $dokuWalets = [];

    foreach ($this->dokuWalet->gets() as $dokuWalet) {
      $dokuWalets[] = $this->_transforms($dokuWalet, $this->dokuWalet);
    }

    if (sizeof($dokuWalets) > 0) {
      return $listener->response(200, array_merge(
        ["payments" => $this->_extends($dokuWalets, $this->dokuWalet)],
        $this->dokuWalet->getPagination()
      ));
    }

    return $listener->response(204, ["payments" => []]);

  }

}
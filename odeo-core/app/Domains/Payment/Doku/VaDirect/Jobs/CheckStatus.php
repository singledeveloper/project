<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Odeo\Jobs\Job;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\VaDirect\Payer;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuCallbackLogRepository;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaStatusRepository;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class CheckStatus extends Job {

  private $vaInquiry, $initializer, $dokuVaStatus;

  public function __construct($vaInquiry) {
    parent::__construct();
    $this->vaInquiry = $vaInquiry;
    $this->initializer = app()->make(DokuInitializer::class);
    $this->dokuVaStatus = app()->make(PaymentDokuVaStatusRepository::class);
  }

  public function handle() {
    $inquiryRepo = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaInquiryRepository::class);
    $this->initializer->switchAccount($this->getOpcGroup($this->vaInquiry->payment_channel), ['is_payment_gateway' => true]);

    clog('doku_va', 'Processing: ' . $this->vaInquiry->id);

    $words = sha1(
      DokuInitializer::$mallId. 
      DokuInitializer::$sharedKey. 
      $this->vaInquiry->id
    );
    $payload = [
      'MALLID' => DokuInitializer::$mallId,
      'CHAINMERCHANT' => $this->vaInquiry->chain_merchant,
      'TRANSIDMERCHANT' => $this->vaInquiry->id,
      'SESSIONID' => $this->vaInquiry->session_id,
      'WORDS' => $words,
    ];
    $client = new Client();
    $request = new Request('POST', env("DOKU_CHECK_STATUS_URL"),
    [
      'Content-Type' => 'application/x-www-form-urlencoded',
    ], http_build_query($payload));

    try {
      $response = $client->send($request);  
      $responseContent = $response->getBody()->getContents(); 
    }
    catch (\GuzzleHttp\Exception\RequestException $e) {
      $errorMessage = $e->getMessage();
      clog('doku_va', $errorMessage);
      $response = $e->getResponse();
      $responseContent = '';
    }

    $requestDump = $this->getDump($request->getHeaders(), json_encode($payload));
    $responseDump = $this->getDump($response->getHeaders(), $responseContent);

    $responseBody = $responseContent;
    $responseContent = json_decode(json_encode(simplexml_load_string($responseContent)), true);
    if (!$responseContent || !isset($responseContent['RESPONSECODE'])) {
      \Log::notice('Inquiry Status Invalid response');
      return null;
    }

    $vaStatus = $this->dokuVaStatus->getNew();
    $vaStatus->request_dump = $requestDump;
    $vaStatus->response_dump = $responseDump;
    $vaStatus->status_code = $response->getStatusCode();
    $this->dokuVaStatus->save($vaStatus);

    $responseCode = $responseContent['RESPONSECODE'];
    $responseStatus = $responseContent['RESULTMSG'];

    if ($responseCode == '0000' && $responseStatus == 'SUCCESS') {
      foreach($responseContent as $key => $value) {
        if ($value === []) {
          $responseContent[$key] = '';
        }
      }
      clog('doku_va', $this->vaInquiry->id . ' success');
      $dokuCallbackLogRepo = app()->make(PaymentDokuCallbackLogRepository::class);
      $log = $dokuCallbackLogRepo->getNew();
      $log->type = 'va_payment_renotify';
      $log->body = $responseBody;
      $dokuCallbackLogRepo->save($log);
      $responseContent['virtual_account_number'] = $responseContent['PAYMENTCODE'];

      $pipeline = new Pipeline;
      $pipeline->add(new Task(Payer::class, 'vaPayment'));
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
      $pipeline->add(new Task(Payer::class, 'updatePayment'));
      $pipeline->execute($responseContent);

      if ($pipeline->fail()) {
        $status = 'fail';
        clog('doku_va', $pipeline->errorMessage);
      } else {
        $status = 'success';
      }

      dispatch(new SendAutoRenotifyStatus($this->vaInquiry->id, $status));
    }

    $this->vaInquiry->doku_status = $responseCode;
    $inquiryRepo->save($this->vaInquiry);
  }

  private function getDump($headers, $stringBody) {
    $dump = '';
    foreach ($headers as $key => $val) {
      $dump = $dump.$key.': '.$val[0]."\n";
    }
    return $dump."\n".$stringBody;
  }

  private function getOpcGroup($paymentChannel) {
    switch ($paymentChannel) {
      case 34:
        return Payment::OPC_GROUP_VA_BRI;
      case 35:
        return Payment::OPC_GROUP_ALFA;
        break;
      default:
        return null;
        break;
    }
  }
}
<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Odeo\Jobs\Job;
use Odeo\Domains\Constant\DokuPaymentStatus;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Domains\Payment\Doku\Helper\DokuLibrary;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaInquiryRepository;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaStatusRepository;
use Odeo\Domains\Payment\Repository\PaymentRepository;
use Illuminate\Support\Facades\Redis;

class InquiryStatus extends Job {

  private $vaPayment, $inquiry, $payment, $library, $initializer, $dokuVaStatus, $redis;

  public function __construct($vaPayment) {
    parent::__construct();
    $this->vaPayment = $vaPayment;
    $this->inquiry = app()->make(PaymentDokuVaInquiryRepository::class);
    $this->payment = app()->make(PaymentRepository::class);
    $this->library = app()->make(DokuLibrary::class);
    $this->initializer = app()->make(DokuInitializer::class);
    $this->dokuVaStatus = app()->make(PaymentDokuVaStatusRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $key = 'odeo_core:doku_change_status_' . $this->vaPayment->id;
    if (!$this->redis->setnx($key, 1)) {
      \Log::notice('Inquiry Status locked');
      return null;
    };
    $this->redis->expire($key, 5 * 60);

    $inquiry = $this->inquiry->findById($this->vaPayment->inquiry_id);
    if (!$inquiry) {
      \Log::notice('Inquiry Not found');
      return null;
    }
    $payment = $this->payment->findById($this->vaPayment->payment_id);
    if (!$payment) {
      \Log::notice('Payment Not found');
      return null;
    }
    $this->initializer->switchAccount($payment->info_id, ['is_payment_gateway' => true]);

    $words = sha1(
      DokuInitializer::$mallId. 
      DokuInitializer::$sharedKey. 
      $inquiry->id
    );
    $payload = [
      'MALLID' => DokuInitializer::$mallId,
      'CHAINMERCHANT' => $inquiry->chain_merchant,
      'TRANSIDMERCHANT' => $inquiry->id,
      'SESSIONID' => $this->vaPayment->session_id,
      'WORDS' => $words,
    ];
    $client = new Client();
    $request = new Request('POST', env("DOKU_CHECK_STATUS_URL"),
    [
      'Content-Type' => 'application/x-www-form-urlencoded',
    ], http_build_query($payload));

    try {
      $response = $client->send($request);  
      $responseContent = $response->getBody()->getContents(); 
    }
    catch (\GuzzleHttp\Exception\RequestException $e) {
      $errorMessage = $e->getMessage();
      clog('doku_va', $errorMessage);
      $response = $e->getResponse();
      $responseContent = '';
    }
    
    $requestDump = $this->getDump($request->getHeaders(), json_encode($payload));
    $responseDump = $this->getDump($response->getHeaders(), $responseContent);

    $responseContent = json_decode(json_encode(simplexml_load_string($responseContent)), true);
    if (!$responseContent || !isset($responseContent['RESPONSECODE'])) {
      \Log::notice('Inquiry Status Invalid response');
      return null;
    }
    $vaStatus = $this->dokuVaStatus->getNew();
    $vaStatus->doku_payment_id = $this->vaPayment->id;
    $vaStatus->request_dump = $requestDump;
    $vaStatus->response_dump = $responseDump;
    $vaStatus->status_code = $response->getStatusCode();
    $this->dokuVaStatus->save($vaStatus);

    $responseCode = $responseContent['RESPONSECODE'];
    $responseStatus = $responseContent['RESULTMSG'];
    if ($responseCode == '0000' && $responseStatus == 'SUCCESS') {
      $newStatus = DokuPaymentStatus::SUCCESS;
    } else {
      $newStatus = DokuPaymentStatus::SUSPECT_NEED_STATUS_RECHECK;
    }

    $inquiry->doku_status = $responseCode;
    $this->inquiry->save($inquiry);
    
    $oldStatus = $this->vaPayment->status;

    if ($newStatus != $oldStatus) {
      $this->vaPayment->status = $newStatus;
      $this->vaPayment->save();
  
      $pipeline = new Pipeline;
      $pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyPaymentStatus'));
      $pipeline->execute([
        'pg_payment_id' => $this->vaPayment->payment->source_id,
        'pg_payment_status' => DokuPaymentStatus::getPaymentGatewayStatus($newStatus),
        'receive_notify_time' => Carbon::now(),
        'vendor_reference_id' => $this->vaPayment->session_id
      ]);
      
      if ($pipeline->fail()) {
        \Log::notice('Inquiry Status Fail Notify: ' . $pipeline->errorMessage);
        return null;
      }
    }

    $this->redis->del($key);
  }

  private function getDump($headers, $stringBody) {
    $dump = '';
    foreach ($headers as $key => $val) {
      $dump = $dump.$key.': '.$val[0]."\n";
    }
    return $dump."\n".$stringBody;
  }
}
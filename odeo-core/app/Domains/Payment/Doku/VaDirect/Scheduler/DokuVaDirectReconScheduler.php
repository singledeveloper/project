<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementDetailRepository;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaDirectPaymentRepository;

class DokuVaDirectReconScheduler {

  private $bcaRepo, $vaPayment;

  private $SETTLEMENT_MAP = [
    '74104' => 2
  ];

  private function init() {
    $this->bcaRepo = app()->make(BankBcaInquiryRepository::class);
    $this->vaPayment = app()->make(PaymentDokuVaDirectPaymentRepository::class);
    $this->paymentSettlement = app()->make(PaymentSettlementRepository::class);
    $this->paymentSettlementDetail = app()->make(PaymentSettlementDetailRepository::class);
  }

  public function run() {
    $this->init();
    
    $this->reconSettlement('74104');
  }

  private function reconSettlement($prefix) {
    $settlements = $this->bcaRepo->getUnknownVaSettlement($this->SETTLEMENT_MAP[$prefix]);
    $payments = $this->vaPayment->getUnsettled($prefix);

    $updatedAt = Carbon::now();

    $paymentSettlementDetails = [];
    $paymentUpdate = [];
    $settlementUpdates = [];
    $payment = $payments->shift();

    foreach ($settlements as $settlement) {
      $totalAmount = $settlement->credit;
      $settledAmount = 0;

      $referenceType = '';
      $refIds = [];
      $tempPaymentSettlementDetail = [];
      $tempPaymentUpdate = [];
      $isSettled = false;

      !isProduction() && clog('doku_recon', 'total settlement: ' . $settlement->credit);
      while (!$isSettled && $settledAmount < $totalAmount) {

        if (!$payment) break;
        
        !isProduction() && clog('doku_recon', 'processing id: ' . $payment->id . ' payment: ' . ($settledAmount + $payment->amount) . '/' . $settlement->credit);
        
        $paymentDate = Carbon::createFromFormat('YmdHis', $payment->payment_date_time);

        if (Carbon::parse($settlement->date)->diffInHours($paymentDate, false) > 22) {
          $isSettled = true;
          !isProduction() && clog('doku_recon', 'mismatch date, expected ' . $settlement->date . ' but payment date is '. $paymentDate);
          break;
        }
        $tempPaymentSettlementDetail[] = [
          'source_id' => $payment->order_id,
          'source_type' => PaymentChannel::ORDER,
          'vendor_id' => $payment->id,
          'vendor_type' => 'doku_va_close',
          'amount' => $payment->amount
        ];
        $tempPaymentUpdate[] = [
          'id' => $payment->id,
          'is_reconciled' => true,
          'updated_at' => $updatedAt
        ];

        $settledAmount += $payment->amount;
        $isSettled = $settledAmount == $totalAmount;
        
        $payment = $payments->shift();
      }

      if (!$isSettled || $settledAmount > $totalAmount) {
        break;
      }

      $paymentSettlement = $this->paymentSettlement->getNew();
      $paymentSettlement->bank_id = Bank::BCA;
      $paymentSettlement->bank_inquiry_id = $settlement->id;
      $paymentSettlement->settlement_amount = $totalAmount;
      $paymentSettlement->settlement_date = $settlement->date;
      $paymentSettlement->settlement_description = $settlement->description;
      $paymentSettlement->save();

      array_walk($tempPaymentSettlementDetail, function(&$detail, $key, $paymentSettlement) {
        $detail['payment_settlement_id'] = $paymentSettlement->id;
      }, $paymentSettlement);
      $paymentSettlementDetails = array_merge($tempPaymentSettlementDetail, $paymentSettlementDetails);
      $paymentUpdate = array_merge($tempPaymentUpdate, $paymentUpdate);

      $settlementUpdates[] = [
        'id' => $settlement->id,
        'reference_type' => BankTransferInquiries::REFERENCE_TYPE_SETTLEMENT_ID,
        'reference' => json_encode(["" . $paymentSettlement->id]),
        'updated_at' => $updatedAt
      ];
    }
    // dd($paymentSettlementDetails, $settlementUpdates, $paymentUpdate);
    count($paymentSettlementDetails) && $this->paymentSettlementDetail->saveBulk($paymentSettlementDetails);
    count($settlementUpdates) && $this->bcaRepo->updateBulk($settlementUpdates);
    count($paymentUpdate) && $this->vaPayment->updateBulk($paymentUpdate);
  }

}
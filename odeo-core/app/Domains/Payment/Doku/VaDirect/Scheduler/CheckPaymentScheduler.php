<?php

namespace Odeo\Domains\Payment\Doku\VaDirect\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaInquiryRepository;
use Odeo\Domains\Payment\Doku\VaDirect\Jobs\CheckStatus;

class CheckPaymentScheduler {

  private $dokuVaPayment;

  public function __construct() {
    $this->dokuVaInquiry = app()->make(PaymentDokuVaInquiryRepository::class);
  }

  public function run() {
    $noPayments = $this->dokuVaInquiry->getNoPayment();

    foreach($noPayments as $noPayment) {
      dispatch(new CheckStatus($noPayment));
    }
  }

}

<?php
namespace Odeo\Domains\Payment\Doku\VaDirect\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\VaDirect\Model\PaymentDokuVaStatus;

class PaymentDokuVaStatusRepository extends Repository {

  public function __construct(PaymentDokuVaStatus $dokuVaStatus) {
    $this->model = $dokuVaStatus;
  }

}

<?php
namespace Odeo\Domains\Payment\Doku\VaDirect;

use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Notifier extends DokuManager {

  private $dokuVaDirectPayments;

  public function __construct() {
    parent::__construct();
    $this->dokuVaDirectPayments = app()->make(\Odeo\Domains\Payment\Doku\VaDirect\Repository\PaymentDokuVaDirectPaymentRepository::class);
  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $dokuVaDirectPayment = $this->dokuVaDirectPayments->findById($payment->reference_id);

    $wordsGenerated = sha1($data['AMOUNT'] .
      $dokuVaDirectPayment->mall_id .
      $this->library->getSharedKey($dokuVaDirectPayment->mall_id) .
      $order->id .
      $data['RESULTMSG'] .
      $data['VERIFYSTATUS']);

    if ($data['WORDS'] == $wordsGenerated && $dokuVaDirectPayment->session_id == $data['SESSIONID']) {

      $dokuVaDirectPayment->payment_date_time = $data['PAYMENTDATETIME'];
      $dokuVaDirectPayment->liability = $data['LIABILITY'];
      $dokuVaDirectPayment->notify_words = $data['WORDS'];
      $dokuVaDirectPayment->result_msg = $data['RESULTMSG'];
      $dokuVaDirectPayment->verify_id = $data['VERIFYID'];
      $dokuVaDirectPayment->bank = $data['BANK'];
      $dokuVaDirectPayment->status_type = $data['STATUSTYPE'];
      $dokuVaDirectPayment->approval_code = $data['APPROVALCODE'];
      $dokuVaDirectPayment->edu_status = $data['EDUSTATUS'];
      $dokuVaDirectPayment->secured_3ds_status = $data['THREEDSECURESTATUS'];
      $dokuVaDirectPayment->verify_score = $data['VERIFYSCORE'];
      $dokuVaDirectPayment->notify_response_code = $data['RESPONSECODE'];
      $dokuVaDirectPayment->verify_status = $data['VERIFYSTATUS'];

      $this->dokuVaDirectPayments->save($dokuVaDirectPayment);

      $order->status = OrderStatus::VERIFIED;
      $order->paid_at = date('Y-m-d H:i:s');

      $this->orders->save($order);

      $listener->pushQueue(new VerifyOrder($order->id));

      return $listener->response(200);
    } else {
      return $listener->response(400);
    }
  }
}

<?php

namespace Odeo\Domains\Payment\Doku\Briepay\Repository;


use Odeo\Domains\Payment\Doku\Briepay\Model\PaymentDokuBriEpayPayment;
use Odeo\Domains\Core\Repository;

class PaymentDokuBriEpayPaymentRepository extends Repository {

  public function __construct(PaymentDokuBriEpayPayment $paymentDokuBriEpayPayment) {
    $this->model = $paymentDokuBriEpayPayment;
  }

}
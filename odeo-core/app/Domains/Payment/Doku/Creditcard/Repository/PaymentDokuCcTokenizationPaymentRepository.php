<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/12/16
 * Time: 3:01 PM
 */

namespace Odeo\Domains\Payment\Doku\Creditcard\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Creditcard\Model\PaymentDokuCcTokenizationPayment;

class PaymentDokuCcTokenizationPaymentRepository extends Repository {

  public function __construct(PaymentDokuCcTokenizationPayment $ccTokenizationPayment) {
    $this->model = $ccTokenizationPayment;
  }

  public function findByUserId($userId) {

    return $this->model->where('user_id', $userId)->first();

  }

}
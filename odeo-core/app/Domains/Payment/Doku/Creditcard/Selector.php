<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 10/29/16
 * Time: 9:04 PM
 */

namespace Odeo\Domains\Payment\Doku\Creditcard;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class Selector implements SelectorListener {

  private $dokuCcPayment;

  public function __construct() {
    $this->dokuCcPayment = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcPaymentRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->dokuCcPayment->normalizeFilters($data);

    $dokuCcPayments = [];

    foreach ($this->dokuCcPayment->gets() as $dokuCcPayment) {
      $dokuCcPayments[] = $this->_transforms($dokuCcPayment, $this->dokuCcPayment);
    }

    if (sizeof($dokuCcPayments) > 0) {
      return $listener->response(200, array_merge(
        ["payments" => $this->_extends($dokuCcPayments, $this->dokuCcPayment)],
        $this->dokuCcPayment->getPagination()
      ));
    }

    return $listener->response(204);

  }
}
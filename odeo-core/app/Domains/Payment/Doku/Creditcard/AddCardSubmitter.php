<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 5:19 PM
 */

namespace Odeo\Domains\Payment\Doku\Creditcard;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Odeo\Domains\Payment\Jobs\SendPaymentVoidFail;
use Odeo\Domains\Payment\Repository\PaymentUserDirectPaymentRepository;

class AddCardSubmitter extends DokuManager {

  private $ccTokenPayments, $dokuCcTokenizations, $directPayments;

  public function __construct() {
    parent::__construct();
    $this->ccTokenPayments = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcTokenizationPaymentRepository::class);
    $this->dokuCcTokenizations = app()->make(\Odeo\Domains\Payment\Doku\Creditcard\Repository\PaymentDokuCcTokenizationRepository::class);
    $this->directPayments = app()->make(PaymentUserDirectPaymentRepository::class);
  }

  public function submit(PipelineListener $listener, $data) {

    $ccToken = $this->ccTokenPayments->findByid($data['reference_id']);

    $this->initializer->switchAccount($ccToken->opc_group);

    $params = [
      'amount' => $data['doku_amount'],
      'invoice' => $data['doku_invoice_no'],
      'currency' => $data['doku_currency'],
      'pairing_code' => $data['doku_pairing_code'],
      'token' => $data['doku_token']
    ];

    $words = $this->library->doCreateWords($params);

    $basket = 'appl_cc,1000,1,1000';

    $customer = array(
      'name' => 'Odeo',
      'data_phone' => '08',
      'data_email' => 'doku@odeo.co.id',
      'data_address' => 'address'
    );

    $responsePrePayment = $this->api->doPrePayment([
      'req_token_id' => $data['doku_token'],
      'req_pairing_code' => $data['doku_pairing_code'],
      'req_customer' => $customer,
      'req_basket' => $basket,
      'req_words' => $words
    ]);

    if ($responsePrePayment->res_response_code != '0000') { //prepayment success

      return $listener->response(400);
    }

    $requestDateTimeTimeStamp = strtotime("now");

    $requestDateTime = date('YmdHis', $requestDateTimeTimeStamp);
    $sessionId = $this->library->createSessionId($ccToken->id);
    $currency = '360';

    $paymentData = array(
      'req_mall_id' => DokuInitializer::$mallId,
      'req_chain_merchant' => 'NA',
      'req_amount' => $data['doku_amount'],
      'req_words' => $words,
      'req_words_raw' => $this->library->doCreateWordsRaw($params),
      'req_purchase_amount' => $data['doku_amount'],
      'req_trans_id_merchant' => $data['doku_invoice_no'],
      'req_request_date_time' => $requestDateTime,
      'req_currency' => $currency,
      'req_purchase_currency' => $currency,
      'req_session_id' => $sessionId,
      'req_name' => $customer['name'],
      'req_payment_channel' => DokuInitializer::CC_CODE,
      'req_basket' => $basket,
      'req_email' => $customer['data_email'],
      'req_mobile_phone' => $customer['data_phone'],
      'req_address' => $customer['data_address'],
      'req_token_id' => $data['doku_token']
    );

    $result = $this->api->doPayment($paymentData);

    if ($result->res_response_code != '0000') {
      return $listener->response(400);
    }

    $ccToken->amount = $data['doku_amount'];
    $ccToken->response_code = $result->res_response_code;
    $ccToken->response_msg = $result->res_response_msg;
    $ccToken->approval_code = $result->res_approval_code;
    $ccToken->mcn = $result->res_mcn;
    $ccToken->bank = $result->res_bank;
    $ccToken->mid = $result->res_mid;
    $ccToken->payment_channel = $result->res_payment_channel;
    $ccToken->payment_date_time = $result->res_payment_date_time;
    $ccToken->session_id = $result->res_session_id;
    $ccToken->verify_id = $result->res_verify_id;
    $ccToken->verify_score = $result->res_verify_score;
    $ccToken->verify_status = $result->res_verify_status;
    $ccToken->words = $result->res_words;
    $ccToken->mall_id = DokuInitializer::$mallId;
    $ccToken->currency = $currency;

    $this->ccTokenPayments->save($ccToken);

    $tokenization = $this->dokuCcTokenizations->getNew();

    $bundle_token = json_decode($result->res_bundle_token);

    $tokenization->mcn = $result->res_mcn;
    $tokenization->user_id = $ccToken->user_id;
    $tokenization->token = $bundle_token->res_token_payment;
    $tokenization->bank = $result->res_bank;
    $tokenization->user_cc_name = $data['cc_name'];
    $tokenization->token_payment_id = $ccToken->id;

    $this->dokuCcTokenizations->save($tokenization);

    $directPayment = $this->directPayments->getNew();
    $directPayment->user_id = $ccToken->user_id;
    $directPayment->reference_id = $tokenization->id;
    $directPayment->info_id = \Odeo\Domains\Constant\Payment::OPC_GROUP_CC_TOKENIZATION;

    $this->directPayments->save($directPayment);

    $voidRequester = app()->make(\Odeo\Domains\Payment\Doku\VoidRequester::class);

    if (($reason = $voidRequester->void([
      'order_id' => $data['doku_invoice_no'],
      'reference_id' => $data['reference_id'],
      'payment_channel_code' => DokuInitializer::CC_CODE,
      'session_id' => $sessionId,
      'void_type' => 'appl_cc'
    ])) !== true) {
//      $listener->pushQueue(new SendPaymentVoidFail([
//        'order_id' => $data['doku_invoice_no'],
//        'description' => 'Fail to void add Credit Card feature, Please contact Doku to manually void'
//      ]));
    }

    $result->res_redirect_url = $data['finish_redirect_url'];
    $result->res_show_doku_page = false;

    return $listener->response(200, [
      'order_id' => $ccToken->id,
      'additional' => $result
    ]);

  }


}
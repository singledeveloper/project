<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 6:01 PM
 */

namespace Odeo\Domains\Payment\Doku\DebitOnlineBni\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\DebitOnlineBni\Model\PaymentDokuDebitOnlineBniPayment;

class PaymentDokuDebitOnlineBniPaymentRepository extends Repository {

  public function __construct(PaymentDokuDebitOnlineBniPayment $debitOnlineBniPayment) {
    $this->model = $debitOnlineBniPayment;
  }

  public function gets() {

    $payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $query = $payment->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['order_id'])) {
        $id = (int)$filters['search']['order_id'];
        $query = $query->where('order_id', $id);
      }
      if (isset($filters['search']['payment_id'])) {
        $query = $query->where('payment_id', $filters['search']['user_id']);
      }

    }

    $query = $query->with('channel')->whereHas('channel', function ($query) {
      $query->where('info_id', '=', \Odeo\Domains\Constant\Payment::OPC_GROUP_DEBIT_ONLINE_BNI);
    })->join('payment_doku_payments', 'payments.reference_id', '=', 'payment_doku_payments.id')
      ->join('payment_doku_debit_online_bni_payments', 'payment_doku_payments.reference_id', '=', 'payment_doku_debit_online_bni_payments.id');

    return $this->getResult($query);
  }

}
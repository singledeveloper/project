<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 5:56 PM
 */

namespace Odeo\Domains\Payment\Doku\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Model\PaymentDokuPayment;

class PaymentDokuPaymentRepository extends Repository {


  public function __construct(PaymentDokuPayment $paymentDokuPayment) {
    $this->model = $paymentDokuPayment;
  }


}
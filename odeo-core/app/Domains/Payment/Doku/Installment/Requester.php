<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 9:40 PM
 */

namespace Odeo\Domains\Payment\Doku\Installment;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Validator;

class Requester extends DokuManager {

  private $tokenCreator;

  public function __construct() {
    parent::__construct();

    $this->tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $data) {
    
    $validator = Validator::make($data, [
      "finish_redirect_url" => "required",
      "unfinish_redirect_url" => "required",
      "tenure" => "required",
      "installment_bank" => "required",
    ]);
    
    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());

    $this->initializer->switchAccount($paymentInformation->info_id, [
      'bank' => $data['installment_bank']
    ]);

    if (!isset($data['idt'])) {

      $url = $this->generateUrl([
        'opc' => $paymentInformation->code,
        'order_id' => $order->id,
        'bearer' => (string) $this->tokenCreator->createToken($data['auth']['user_id'], $data['auth']['type'], $data['auth']['platform_id']),
        'finish_redirect_url' => $data['finish_redirect_url'],
        'unfinish_redirect_url' => $data['unfinish_redirect_url'],
        'tenure' => $data['tenure'],
        'installment_bank' => $data['installment_bank']
      ]);

      return $listener->response(200, [
        'type' => \Odeo\Domains\Constant\Payment::REQUEST_PAYMENT_TYPE_VIEW,
        'content' => [
          'link' => $url
        ]
      ]);
    }

    $detail = \json_decode($paymentInformation->information->detail, true);

    $mdr = $detail[$data['installment_bank']]['available_installment'][$data['tenure']]['mdr'];

    $total = $this->feeGenerator->getFee($mdr, $order->total) + $order->total;

    $formattedTotal = number_format($total, 2, '.', '');

    $currency = '360';
    $sessionId = $this->library->createSessionId($order->id);

    $words = sha1($formattedTotal . DokuInitializer::$mallId . DokuInitializer::$sharedKey .  $order->id);

    $requestDateTime = date('YmdHis');

    $month = $detail[$data['installment_bank']]['available_installment'][$data['tenure']]['month'];

    $promoID = $detail[$data['installment_bank']]['available_installment'][$data['tenure']]['promo_id'];

    if (env("APP_ENV") != 'production') {

      switch ($month) {
        case 3:
          $promoID = "003";
          break;
        case 6:
          $promoID = "006";
          break;
        case 12:
          $promoID = "012";
          break;
        case 18:
          $promoID = "018";
          break;
        case 24:
          $promoID = "024";
          break;
      }
    }

    $payment = $this->payments->findByOrderId($order->id);

    if ($payment->reference_id) {
      $dokuPayment = $this->dokuPayments->findById($payment->reference_id);
    } else {
      $dokuPayment = $this->dokuPayments->getNew();
    }


    $dokuPayment->request_date_time = date('Y-m-d H:i:s', $requestDateTime);
    $dokuPayment->session_id = $sessionId;
    $dokuPayment->words = $words;
    $dokuPayment->amount = $total;
    $dokuPayment->currency = $currency;
    $dokuPayment->mall_id = DokuInitializer::$mallId;

    $this->dokuPayments->save($dokuPayment);

    $payment->reference_id = $dokuPayment->id;

    $this->payments->save($payment);

    $response = [
      'URL' => DokuInitializer::$redirectPaymentUrl,
      'MALLID' => DokuInitializer::$mallId,
      'CHAINMERCHANT' => 'NA',
      'AMOUNT' => $formattedTotal,
      'PURCHASEAMOUNT' => $formattedTotal,
      'TRANSIDMERCHANT' => $order->id,
      'WORDS' => $words,
      'REQUESTDATETIME' => $requestDateTime,
      'CURRENCY' => '360',
      'TENOR' => sprintf("%02d", $month),
      'PROMOID' => $promoID,
      'INSTALLMENT_ACQUIRER' => $detail[$data['installment_bank']]['installment_acquirer'],
      'PURCHASECURRENCY' => '360',
      'SESSIONID' => $sessionId,
      'NAME' => 'Your Name',
      'PAYMENTCHANNEL' => '15',
      'EMAIL' => 'example@domain.com',
      'BASKET' => $this->library->formatBasket($order)
    ];

    if (isset($detail[$data['installment_bank']]['payment_type'])) {
      $response['PAYMENTTYPE'] = $detail[$data['installment_bank']]['payment_type'];
    }

    return $listener->response(200, [
        'view_page' => 'payment.doku-v2',
        'view_data' => $response
      ]
    );

  }

}
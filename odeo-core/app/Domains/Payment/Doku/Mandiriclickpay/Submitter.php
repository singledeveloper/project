<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 11/29/16
 * Time: 10:06 PM
 */

namespace Odeo\Domains\Payment\Doku\Mandiriclickpay;

use DateTime;
use Odeo\Domains\Constant\OrderCharge;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Submitter extends DokuManager {

  private $dokuMandiriclickpay;

  public function __construct() {
    parent::__construct();
    $this->dokuMandiriclickpay = app()->make(\Odeo\Domains\Payment\Doku\Mandiriclickpay\Repository\PaymentDokuMandiriClickpayPaymentRepository::class);
  }

  public function submit(PipelineListener $listener, $order, $paymentInformation, $data) {

    $this->initializer->switchAccount($paymentInformation->info_id);

    $params = [
      'amount' => $data['doku_amount'],
      'invoice' => $order->id,
      'currency' => $data['doku_currency']
    ];

    $words = $this->library->doCreateWords($params);
    $basket = $this->library->formatBasket($order);

    $customer = array(
      'name' => 'Odeo',
      'data_phone' => '08',
      'data_email' => 'doku@odeo.co.id',
      'data_address' => 'address',
      'cc' => str_replace(" - ", "", $data['cc_number'])
    );

    $requestDateTime = date('YmdHis');
    $sessionId = sha1($requestDateTime);
    $currency = $data['doku_currency'];

    $paymentData = array(
      'req_mall_id' => DokuInitializer::$mallId,
      'req_chain_merchant' => 'NA',
      'req_amount' => $data['doku_amount'],
      'req_words' => $words,
      'req_purchase_amount' => $data['doku_amount'],
      'req_trans_id_merchant' => $order->id,
      'req_request_date_time' => $requestDateTime,
      'req_currency' => $currency,
      'req_purchase_currency' => $currency,
      'req_session_id' => $sessionId,
      'req_name' => $customer['name'],
      'req_payment_channel' => DokuInitializer::MANDIRI_CLICKPAY_CODE,
      'req_email' => $customer['data_email'],
      'req_card_number' => $customer['cc'],
      'req_basket' => $basket,
      'req_challenge_code_1' => $data['CHALLENGE_CODE_1'],
      'req_challenge_code_2' => $data['CHALLENGE_CODE_2'],
      'req_challenge_code_3' => $data['CHALLENGE_CODE_3'],
      'req_response_token' => $data['response_token'],
      'req_mobile_phone' => $customer['data_phone'],
      'req_address' => $customer['data_address']
    );

    $result = $this->api->doDirectPayment($paymentData);

    if ($result->res_response_code != '0000') {
      return $listener->response(400, [
        'additional' => $result
      ]);

    }

    $payment = $this->payments->findByOrderId($order->id);

    $dokuPayment = $this->dokuPayments->getNew();
    $dokuPayment->request_date_time = date('Y-m-d H:i:s', $requestDateTime);
    $dokuPayment->session_id = $sessionId;
    $dokuPayment->words = $words;
    $dokuPayment->amount = $data['doku_amount'];
    $dokuPayment->currency = $currency;
    $dokuPayment->mall_id = DokuInitializer::$mallId;

    $dokuMandiriclickpay = $this->dokuMandiriclickpay->getNew();
    $dokuMandiriclickpay->response_msg = $result->res_response_msg;
    $dokuMandiriclickpay->transaction_code = $result->res_transaction_code;
    $dokuMandiriclickpay->mcn = $result->res_mcn;
    $dokuMandiriclickpay->approval_code = $result->res_approval_code;
    $dokuMandiriclickpay->payment_date = DateTime::createFromFormat('YmdHis', $result->res_payment_date)->format('Y-m-d H:i:s');
    $dokuMandiriclickpay->request_code = $result->res_request_code;
    $dokuMandiriclickpay->bank = $result->res_bank;
    $dokuMandiriclickpay->response_code = $result->res_response_code;
    $this->dokuMandiriclickpay->save($dokuMandiriclickpay);

    $dokuPayment->reference_id = $dokuMandiriclickpay->id;
    $this->dokuPayments->save($dokuPayment);

    $payment->reference_id = $dokuPayment->id;
    $this->payments->save($payment);

    $order->status = OrderStatus::VERIFIED;
    $order->paid_at = date('Y-m-d H:i:s');

    $fee = 5000 + 1500;
    $this->charge($order->id, OrderCharge::PAYMENT_SERVICE_COST, $fee, OrderCharge::GROUP_TYPE_CHARGE_TO_CUSTOMER);

    //$this->charge($order->id, OrderCharge::DOKU_FEE, 1500, OrderCharge::GROUP_TYPE_CHARGE_TO_COMPANY);

    $order->total = $order->total + $fee;

    $this->orders->save($order);

    $result->res_show_doku_page = false;

    $listener->pushQueue(new VerifyOrder($order->id));

    return $listener->response(200, [
      'order_id' => $order->id,
      'opc' => $paymentInformation->code,
      'additional' => $result
    ]);

  }

}
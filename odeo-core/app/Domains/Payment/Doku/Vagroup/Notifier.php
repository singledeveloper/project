<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 9:03 PM
 */

namespace Odeo\Domains\Payment\Doku\Vagroup;


use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;

class Notifier extends DokuManager {


  private $dokuVaPayments;

  public function __construct() {
    parent::__construct();
    $this->dokuVaPayments = app()->make(\Odeo\Domains\Payment\Doku\Vagroup\Repository\PaymentDokuVaPaymentRepository::class);
  }

  public function notified(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $dokuPayment = $this->dokuPayments->findById($payment->reference_id);

    $WORDSGENERATED = sha1($data['AMOUNT'] .
      $dokuPayment->mall_id .
      $this->library->getSharedKey($dokuPayment->mall_id) .
      $order->id .
      $data['RESULTMSG'] .
      $data['VERIFYSTATUS']);

    if ($data['WORDS'] == $WORDSGENERATED && $dokuPayment->amount == $data['AMOUNT']) {

      $vaPayment = $this->dokuVaPayments->findById($dokuPayment->reference_id);

      $vaPayment->payment_date_time = $data['PAYMENTDATETIME'];
      $vaPayment->purchase_currency = $data['PURCHASECURRENCY'];
      $vaPayment->liability = $data['LIABILITY'];
      $vaPayment->amount = $data['AMOUNT'];
      $vaPayment->mcn = $data['MCN'];
      $vaPayment->payment_code = $data['PAYMENTCODE'];
      $vaPayment->words = $data['WORDS'];
      $vaPayment->notify_response_msg = $data['RESULTMSG'];
      $vaPayment->verify_id = $data['VERIFYID'];
      $vaPayment->bank = $data['BANK'];
      $vaPayment->status_type = $data['STATUSTYPE'];
      $vaPayment->approval_code = $data['APPROVALCODE'];
      $vaPayment->edu_status = $data['EDUSTATUS'];
      $vaPayment->secured_3ds_status = $data['THREEDSECURESTATUS'];
      $vaPayment->verify_score = $data['VERIFYSCORE'];
      $vaPayment->currency = $data['CURRENCY'];
      $vaPayment->notify_response_code = $data['RESPONSECODE'];
      $vaPayment->verify_status = $data['VERIFYSTATUS'];
      $vaPayment->session_id = $data['SESSIONID'];

      $this->dokuVaPayments->save($vaPayment);

      $order->status = OrderStatus::VERIFIED;
      $order->paid_at = date('Y-m-d H:i:s');

      $this->orders->save($order);

      $listener->pushQueue(new VerifyOrder($order->id));

      return $listener->response(200);

    } else {

      return $listener->response(400);

    }

  }
}
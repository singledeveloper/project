<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 8:34 PM
 */

namespace Odeo\Domains\Payment\Doku\Vagroup\Repository;

use Odeo\Domains\Constant\DokuPaymentStatus;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Vagroup\Model\PaymentDokuVaPayment;

class PaymentDokuVaPaymentRepository extends Repository {

  public function __construct(PaymentDokuVaPayment $dokuVaPayment) {
    $this->model = $dokuVaPayment;
  }

  public function gets() {

    $payment = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);

    $query = $payment->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['order_id'])) {
        $id = (int)$filters['search']['order_id'];
        $query = $query->where('order_id', $id);
      }
      if (isset($filters['search']['payment_id'])) {
        $query = $query->where('payment_id', $filters['search']['user_id']);
      }

    }

    $query = $query->with('channel')->whereHas('channel', function ($query) {
      $query->where('info_id', '=', \Odeo\Domains\Constant\Payment::OPC_GROUP_ALFA);
    })->join('payment_doku_payments', 'payments.reference_id', '=', 'payment_doku_payments.id')
      ->join('payment_doku_va_payments', 'payment_doku_payments.reference_id', '=', 'payment_doku_va_payments.id');

    return $this->getResult($query);
  }

  public function get() {
    $filter = $this->getFilters();
    $q = $this->model
      ->join('payment_doku_va_inquiries', 'payment_doku_va_inquiries.id', '=', 'payment_doku_va_payments.inquiry_id')
      ->join('payments', 'payment_doku_va_payments.payment_id', '=', 'payments.id')
      ->select(
        'payment_doku_va_payments.id',
        'payment_doku_va_payments.payment_code',
        'payment_doku_va_payments.payment_date_time',
        'payment_doku_va_inquiries.amount',
        'payment_doku_va_payments.bank',
        'payment_doku_va_payments.approval_code',
        'payment_doku_va_payments.session_id',
        'payment_doku_va_payments.inquiry_id',
        'payment_doku_va_payments.status',
        'payments.source_id',
        'payments.source_type',
        'payment_doku_va_payments.created_at'
      );

    if (isset($filter['search'])) {
      $search = $filter['search'];

      if (isset($search['id'])) {
        $q = $q->where('payment_doku_va_payments.id', $search['id']);
      }
      if (isset($search['pay_code'])) {
        $q = $q->where('pay_code', $search['pay_code']);
      }
      if (isset($search['session_id'])) {
        $q = $q->where('payment_doku_va_payments.session_id', $search['session_id']);
      }
      if (isset($search['inquiry_id'])) {
        $q = $q->where('payment_doku_va_inquiries.id', $search['inquiry_id']);
      }

    }

    return $this->getResult($q->orderBy('payment_doku_va_payments.id', 'DESC'));
  }

  public function getPendingPayment() {
    return $this->model->whereIn('status', [
      DokuPaymentStatus::ACCEPTED,
      DokuPaymentStatus::PENDING,
      DokuPaymentStatus::SUSPECT_FOR_TESTING,
      DokuPaymentStatus::SUSPECT_TIMEOUT,
      DokuPaymentStatus::SUSPECT_NEED_STATUS_RECHECK,
      DokuPaymentStatus::SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK,
    ])->get();
  }

  public function getUnsettled($isPaymentGateway = false) {
    if ($isPaymentGateway) {
      return $this->model
        ->where('status', DokuPaymentStatus::SUCCESS)
        ->where('is_reconciled', false)
        ->where('payment_doku_va_payments.created_at', '>=', '2019-08-09')
        ->orderBy('payment_doku_va_payments.created_at', 'asc')
        ->get();
    } else {
      return $this->model
        ->whereNotNull('payment_date_time')
        ->where('notify_response_msg', 'SUCCESS')
        ->orderBy('created_at', 'asc')
        ->get();
    }
  }
}
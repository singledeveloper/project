<?php

namespace Odeo\Domains\Payment\Doku\Muamalat;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Doku\Helper\DokuInitializer;
use Odeo\Domains\Payment\Doku\Helper\DokuManager;
use Validator;

class Requester extends DokuManager {

  private $tokenCreator;

  public function __construct() {
    parent::__construct();

    $this->tokenCreator = app()->make(\Odeo\Domains\Account\Helper\TokenCreator::class);
  }

  public function request(PipelineListener $listener, $order, $paymentInformation, $payment, $data) {

    $validator = Validator::make($data, [
      "finish_redirect_url" => "required",
      "unfinish_redirect_url" => "required",
    ]);

    if ($validator->fails()) return $listener->response(400, $validator->errors()->all());

    $this->initializer->switchAccount($paymentInformation->info_id);

    if (!isset($data['idt'])) {

      $url = $this->generateUrl([
        'opc' => $paymentInformation->code,
        'order_id' => $order->id,
        'bearer' => (string)$this->tokenCreator->createToken($data['auth']['user_id'], $data['auth']['type'], $data['auth']['platform_id']),
        'finish_redirect_url' => $data['finish_redirect_url'],
        'unfinish_redirect_url' => $data['unfinish_redirect_url']
      ]);

      return $listener->response(200, [
        'type' => \Odeo\Domains\Constant\Payment::REQUEST_PAYMENT_TYPE_VIEW,
        'content' => [
          'link' => $url
        ]
      ]);
    }

    $total = $order->total + 4500;
    $formattedTotal = number_format($total, 2, '.', '');

    $currency = '360';
    $sessionId = $this->library->createSessionId($order->id);

    $words = sha1($formattedTotal . DokuInitializer::$mallId . DokuInitializer::$sharedKey . $order->id);

    $requestDateTime = date('YmdHis');

    if (isset($payment->reference_id) && $payment->reference_id) {
      $dokuPayment = $this->dokuPayments->findById($payment->reference_id);
    } else {
      $dokuPayment = $this->dokuPayments->getNew();
    }

    $dokuPayment->request_date_time = date('Y-m-d H:i:s', $requestDateTime);
    $dokuPayment->session_id = $sessionId;
    $dokuPayment->words = $words;
    $dokuPayment->amount = $total;
    $dokuPayment->currency = $currency;
    $dokuPayment->mall_id = DokuInitializer::$mallId;

    $this->dokuPayments->save($dokuPayment);

    $payment->reference_id = $dokuPayment->id;

    $this->payments->save($payment);

    $response = [
      'URL' => DokuInitializer::$redirectPaymentUrl,
      'MALLID' => DokuInitializer::$mallId,
      'CHAINMERCHANT' => 'NA',
      'AMOUNT' => $formattedTotal,
      'PURCHASEAMOUNT' => $formattedTotal,
      'TRANSIDMERCHANT' => $order->id,
      'WORDS' => $words,
      'REQUESTDATETIME' => $requestDateTime,
      'CURRENCY' => '360',
      'PURCHASECURRENCY' => '360',
      'SESSIONID' => $sessionId,
      'NAME' => 'Your Name',
      'PAYMENTCHANNEL' => DokuInitializer::MUAMALAT_CODE,
      'EMAIL' => 'example@domain.com',
      'BASKET' => $this->library->formatBasket($order)
    ];

    return $listener->response(200, [
        'view_page' => 'payment.doku-v2',
        'view_data' => $response
      ]
    );

  }

}
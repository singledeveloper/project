<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/25/17
 * Time: 10:34 PM
 */

namespace Odeo\Domains\Payment\Doku\Muamalat\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Doku\Muamalat\Model\PaymentDokuMuamalatInternetBankingPayment;

class PaymentDokuMuamalatInternetBankingPaymentRepository extends Repository {

  public function __construct(PaymentDokuMuamalatInternetBankingPayment $paymentDokuMuamalatInternetBanking) {
    $this->model = $paymentDokuMuamalatInternetBanking;
  }

}
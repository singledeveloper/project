<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 08/05/19
 * Time: 15.16
 */

namespace Odeo\Domains\Payment\Prismalink\VaDirect\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Constant\PaymentChannel;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementRepository;
use Odeo\Domains\Payment\Repository\PaymentSettlementDetailRepository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkVaDirectPayment;
use Odeo\Domains\Payment\Prismalink\VaDirect\Model\PaymentPrismalinkVaPayment;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaDirectPaymentRepository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaPaymentRepository;
use Odeo\Domains\PaymentGateway\Jobs\SendPaymentGatewayReconMismatchAlert;
use Odeo\Domains\PaymentGateway\Model\PaymentGatewayInvoicePayment;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayInvoicePaymentRepository;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayPaymentRepository;

class PrismalinkVaDirectReconScheduler {

  private $bcaRepo, $vaStaticPayment, $vaDynamicPayment;

  private $SETTLEMENT_MAP = [
    '10698' => 1,
    '74104' => 2
  ];

  private function init() {
    $this->bcaRepo = app()->make(BankBcaInquiryRepository::class);
    $this->vaStaticPayment = app()->make(PaymentPrismalinkVaDirectPaymentRepository::class);
    $this->newVaStaticPayment = app()->make(PaymentGatewayInvoicePaymentRepository::class);
    $this->vaDynamicPayment = app()->make(PaymentPrismalinkVaPaymentRepository::class);
    $this->paymentSettlement = app()->make(PaymentSettlementRepository::class);
    $this->paymentSettlementDetail = app()->make(PaymentSettlementDetailRepository::class);
    $this->paymentGatewayPayment = app()->make(PaymentGatewayPaymentRepository::class);
  }

  public function run() {
    $this->init();
    
    $this->reconSettlement('10698');
    $this->reconSettlement('74104');
  }

  private function reconSettlement($prefix) {
    $settlements = $this->bcaRepo->getUnknownVaSettlement($this->SETTLEMENT_MAP[$prefix]);
    // $staticPayments = $this->vaStaticPayment->getUnsettled($prefix)->keyBy(function($item) {
    //   return $item['paid_at'] . ':' . $item['id'];
    // });
    $newStaticPayments = $this->newVaStaticPayment->getUnsettled('prismalink_bca', $prefix)->keyBy(function($item) {
      return $item['paid_at'] . ':' . $item['id'];
    });
    $dynamicPayments = $this->vaDynamicPayment->getUnsettled($prefix)->keyBy(function($item) {
      return $item['payment_date'] . ':' . $item['id'];
    });

    // $payments = $staticPayments->union($dynamicPayments)
    //   ->sortBy(function ($p, $key) {
    //     return $key;
    //   });

    $payments = $dynamicPayments->union($newStaticPayments)
      ->sortBy(function ($p, $key) {
        return $key;
      });

    $updatedAt = Carbon::now();

    $paymentSettlementDetails = [];
    // $staticPaymentUpdate = [];
    $newStaticPaymentUpdate = [];
    $dynamicPaymentUpdate = [];
    $paymentGatewayPaymentUpdate = [];
    $settlementUpdates = [];
    $payment = $payments->shift();

    foreach ($settlements as $settlement) {
      $totalAmount = $settlement->credit;
      $settledAmount = 0;

      $referenceType = '';
      $refIds = [];
      $tempPaymentSettlementDetail = [];
      // $tempStaticPaymentUpdate = [];
      $tempNewStaticPaymentUpdate = [];
      $tempDynamicPaymentUpdate = [];
      $tempPaymentGatewayPaymentUpdate = [];
      $isSettled = false;

      !isProduction() && clog('prismalink_recon', 'total settlement: ' . $settlement->credit);
      while (!$isSettled && $settledAmount < $totalAmount) {

        if (!$payment) break;
        
        !isProduction() && clog('prismalink_recon', 'processing id: ' . $payment->id . ' payment: ' . ($settledAmount + $payment->amount) . '/' . $settlement->credit);
        
        if ($payment instanceof PaymentPrismalinkVaPayment) {
          if (Carbon::parse($settlement->date)->diffInHours($payment->payment_date, false) > 22) {
            $isSettled = true;
            !isProduction() && clog('prismalink_recon', 'mismatch date, expected ' . $settlement->date . ' but payment date is '. $payment->payment_date);
            break;
          }
          $tempPaymentSettlementDetail[] = [
            'source_id' => $payment->payment->source_id,
            'source_type' => PaymentChannel::PAYMENT_GATEWAY,
            'vendor_id' => $payment->id,
            'vendor_type' => $this->getType($prefix),
            'amount' => $payment->amount
          ];
          $tempDynamicPaymentUpdate[] = [
            'id' => $payment->id,
            'is_reconciled' => true,
            'updated_at' => $updatedAt
          ];
          $tempPaymentGatewayPaymentUpdate[] = [
            'id' => $payment->payment->source_id,
            'reconciled_at' => Carbon::now()
          ];
        // } else if ($payment instanceof PaymentPrismalinkVaDirectPayment) {
        //   if (Carbon::parse($settlement->date)->diffInHours($payment->paid_at, false) > 22) {
        //     $isSettled = true;
        //     !isProduction() && clog('prismalink_recon', 'mismatch date, expected ' . $settlement->date . ' but payment date is '. $payment->paid_at);
        //     break;
        //   }
        //   $tempPaymentSettlementDetail[] = [
        //     'source_id' => $payment->order_id,
        //     'source_type' => PaymentChannel::ORDER,
        //     'vendor_id' => $payment->id,
        //     'vendor_type' => $this->getType($prefix),
        //     'amount' => $payment->amount
        //   ];
        //   $tempStaticPaymentUpdate[] = [
        //     'id' => $payment->id,
        //     'is_reconciled' => true,
        //     'updated_at' => $updatedAt
        //   ];
        } else if ($payment instanceof PaymentGatewayInvoicePayment) {
          if (Carbon::parse($settlement->date)->diffInHours($payment->paid_at, false) > 22) {
            $isSettled = true;
            !isProduction() && clog('prismalink_recon', 'mismatch date, expected ' . $settlement->date . ' but payment date is '. $payment->paid_at);
            break;
          }
          $tempPaymentSettlementDetail[] = [
            'source_id' => $payment->order_id,
            'source_type' => PaymentChannel::ORDER,
            'vendor_id' => $payment->id,
            'vendor_type' => $this->getType($prefix),
            'amount' => $payment->amount
          ];
          $tempNewStaticPaymentUpdate[] = [
            'id' => $payment->id,
            'is_reconciled' => true,
            'updated_at' => $updatedAt
          ];
        }

        $settledAmount += $payment->amount;
        $isSettled = $settledAmount == $totalAmount;
        
        $payment = $payments->shift();
      }

      if (!$isSettled || $settledAmount > $totalAmount) {
        dispatch(new SendPaymentGatewayReconMismatchAlert([
          'date' => $settlement->date,
          'vendor' => 'Prismalink VA',
          'description' => $settlement->description,
          'settlement_amount' => $settledAmount,
          'total_amount' => $totalAmount
        ]));
        break;
      }

      $paymentSettlement = $this->paymentSettlement->findByAttributes([
        'bank_id' => Bank::BCA,
        'bank_inquiry_id' => $settlement->id
      ]);

      if (!$paymentSettlement) {
        $paymentSettlement = $this->paymentSettlement->getNew();
        $paymentSettlement->bank_id = Bank::BCA;
        $paymentSettlement->bank_inquiry_id = $settlement->id;
        $paymentSettlement->settlement_amount = $totalAmount;
        $paymentSettlement->settlement_date = $settlement->date;
        $paymentSettlement->settlement_description = $settlement->description;
        $paymentSettlement->save();
      }

      array_walk($tempPaymentSettlementDetail, function(&$detail, $key, $paymentSettlement) {
        $detail['payment_settlement_id'] = $paymentSettlement->id;
      }, $paymentSettlement);
      $paymentSettlementDetails = array_merge($tempPaymentSettlementDetail, $paymentSettlementDetails);
      // $staticPaymentUpdate = array_merge($tempStaticPaymentUpdate, $staticPaymentUpdate);
      $newStaticPaymentUpdate = array_merge($tempNewStaticPaymentUpdate, $newStaticPaymentUpdate);
      $dynamicPaymentUpdate = array_merge($tempDynamicPaymentUpdate, $dynamicPaymentUpdate);
      $paymentGatewayPaymentUpdate = array_merge($tempPaymentGatewayPaymentUpdate, $paymentGatewayPaymentUpdate);

      $settlementUpdates[] = [
        'id' => $settlement->id,
        'reference_type' => BankTransferInquiries::REFERENCE_TYPE_SETTLEMENT_ID,
        'reference' => json_encode(["" . $paymentSettlement->id]),
        'updated_at' => $updatedAt
      ];
    }

    count($settlementUpdates) && $this->bcaRepo->updateBulk($settlementUpdates);
    count($paymentSettlementDetails) && $this->paymentSettlementDetail->saveBulk($paymentSettlementDetails);
    // count($staticPaymentUpdate) && $this->vaStaticPayment->updateBulk($staticPaymentUpdate);
    count($newStaticPaymentUpdate) && $this->newVaStaticPayment->updateBulk($newStaticPaymentUpdate);
    count($dynamicPaymentUpdate) && $this->vaDynamicPayment->updateBulk($dynamicPaymentUpdate);
    count($paymentGatewayPaymentUpdate) && $this->paymentGatewayPayment->updateBulk($paymentGatewayPaymentUpdate);
  }

  public function settleFirstPrismalinkVaPayment() {
    $vaPayment = app()->make(PaymentPrismalinkVaDirectPaymentRepository::class);
    $bcaRepo = app()->make(BankBcaInquiryRepository::class);
    $bcaId = 201561;
    $updates = [];

    $ids = $vaPayment->getFirstSettlementPayment()->pluck('order_id')->map(function($val) use (&$updates, $bcaId){
      $updates[] = [
        'id' => $val,
        'bank_reference_id' => $bcaId,
        'updated_at' => Carbon::now()
      ];
      return strval($val);
    });

    $vaPayment->updateBulk($updates);

    $bca = $bcaRepo->findById($bcaId);
    $bca->reference_type = BankTransferInquiries::REFERENCE_TYPE_ORDER_ID;
    $bca->reference = $ids;

    $bcaRepo->save($bca);
    return $updates;
  }

  private function getType($prefix) {
    switch ($prefix) {
      case '10698': 
        return 'prismalink_va_open';
      case '74104':
        return 'prismalink_va_close';
      default:
        return '';
    }
  }

}
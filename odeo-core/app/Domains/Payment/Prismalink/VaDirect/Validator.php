<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/10/19
 * Time: 13.02
 */

namespace Odeo\Domains\Payment\Prismalink\VaDirect;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkManager;

class Validator extends PrismalinkManager {

  public function validate(PipelineListener $listener, $data) {
    if (isset($data['signature']) && strtolower($data['signature']) == $this->library->generateSignature($data)) {
      return $listener->response(200);
    }
    return $listener->response(400, [
      'va_name' => '',
      'response_code' => '01',
      'response_message' => 'Invalid signature',
      'billing_amount' => 0
    ]);
  }

}
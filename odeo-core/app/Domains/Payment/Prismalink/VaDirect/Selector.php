<?php

namespace Odeo\Domains\Payment\Prismalink\VaDirect;

use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Prismalink\Helper\PrismalinkManager;

class Selector extends PrismalinkManager {

  private $prismalinkVaDirectPayments;

  public function __construct() {
    parent::__construct();
    $this->prismalinkVaDirectPayments = app()->make(\Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkVaDirectPaymentRepository::class);
  }

  public function getDetail(PipelineListener $listener, $data) {
    $prismalinkVaDirectPayments = $this->prismalinkVaDirectPayments->findByAttributes('reference_number', $data['traceNo']);

    if (!$prismalinkVaDirectPayments || $prismalinkVaDirectPayments['amount'] != $data['amount']) {
      return $listener->response(400, [
        'response_code' => '01',
        'response_message' => 'Invalid Reference ID',
        'customer_name' => ''   
      ]);
    } else {
      return $listener->response(200, [
        'order_id' => $prismalinkVaDirectPayments->order_id
      ]);
    }
  }

  public function transforms($item) {
    return $item;
  }

  public function getAll(PipelineListener $listener, $data) {
    $this->prismalinkVaDirectPayments->normalizeFilters($data);
    $this->prismalinkVaDirectPayments->setSimplePaginate(true);

    $result = [];
    $payments = $this->prismalinkVaDirectPayments->get();


    foreach ($payments as $item) {
      $result[] = $this->transforms($item);
    }

    return $listener->response(200, array_merge(
      [
        'payments' => $result,
        'settlement_summary' => $this->prismalinkVaDirectPayments->getSettlementSummary()
      ],
      $this->prismalinkVaDirectPayments->getPagination()
    ));

  }

  public function exportPaymentData($data) {
    $this->prismalinkVaDirectPayments->normalizeFilters($data);
    $payments = $this->prismalinkVaDirectPayments->get();

    $writer = WriterFactory::create(Type::CSV);
    $writer->openToBrowser('prismalink_va_payments ' . ($data['search']['start_date'] ?? '0000-00-00') . ' to ' . ($data['search']['end_date'] ?? Carbon::now()->toDateString()) . '.csv');

    $headers = ['id', 'order_id', 'virtual_account_number', 'amount', 'payment_fee', 'payment_net_amount', 'reference_number', 'paid_at', 'bank_reference_id', 'created_at'];
    
    $writer->addRow($headers);

    foreach ($payments as $payment) {
      $row = [];

      foreach ($headers as $h) {
        $row[] = $payment[$h];
      }
      $writer->addRow($row);
    }

    $writer->close();
  }

  public function getPaidTransactionCount(PipelineListener $listener, $data) {
    $count = $this->prismalinkVaDirectPayments->getPaidTransactionCount($data['start_date'], $data['end_date']);
    return $listener->response(200, ['va_direct_count' => $count ?? 0]);
  }

}

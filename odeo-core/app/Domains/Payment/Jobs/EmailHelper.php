<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/13/16
 * Time: 2:23 PM
 */

namespace Odeo\Domains\Payment\Jobs;


use Odeo\Domains\Constant\BillerPundi;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\StoreStatus;
use \Odeo\Domains\Account\Repository\UserRepository;

trait EmailHelper {


  public function getEmail($order, $data) {

    $userRepo = app()->make(UserRepository::class);
    $userId = $order->user_id ?? $data['auth']['user_id'];

    $user = $userRepo->findById($userId);

    $shouldReceivedEmailPlatform = [Platform::WEB_COMPANY, Platform::WEB_STORE];
    if (!$user->is_email_verified && !in_array($order->platform_id, $shouldReceivedEmailPlatform)) {
      return null;
    }
    return $order->email ?? $user->email;
  }

  public function getEmailAndAdditionalEmail($order, $data) {
    $email = $this->getEmail($order, $data);
    if ($order->additional_email) {
      $email = [$email, $order->additional_email];
    }
    return $email;
  }


  public function getEmailInfo($storeId) {

    if ($storeId && $storeId == BillerPundi::ODEO_STORE_ID) {
      return [
        'sender_name' => 'Pundi-Pundi',
        'sender_email' => 'noreply@pundi-pundi.com',
        'email_support' => 'support@pundi-pundi.com',
        'whitelabel' => true
      ];
    }
    return [
      'sender_name' => 'odeo',
      'sender_email' => 'noreply@odeo.co.id',
      'email_support' => 'cs@odeo.co.id',
      'whitelabel' => false
    ];
  }

  public function getStoreData($seller_store_id) {
    $stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $storeParser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);
    $storeManager = app()->make(\Odeo\Domains\Subscription\Helper\StoreManager::class);

    if (!$seller_store_id) return [
      'name' => 'Odeo',
      'domain' => 'https://odeo.co.id',
      'logo' => baseUrl('images/email/logo.png')
    ];


    $store = $stores->findById($seller_store_id);

    if ($seller_store_id === StoreStatus::getStoreDefault()) {
      $response['seller_store_email'] = '';
    } else if ($email = $storeManager->getOwner($seller_store_id)[$seller_store_id]["email"]) {
      $response['seller_store_email'] = $email;
    }


    $response['name'] = $store->name;
    $response['domain'] = "http://" . $storeParser->domainName($store);
    if ($store->logo_path) {
      $response['logo'] = parseFilePath($store->logo_path, "logo")['logo_image_normal'];
    }

    return $response;

  }

}

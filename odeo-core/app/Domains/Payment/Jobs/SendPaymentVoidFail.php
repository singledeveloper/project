<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Payment\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendPaymentVoidFail extends Job  {

  use SerializesModels, EmailHelper;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    Mail::send('emails.fail_void_payment', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo');
      $m->to('pg@odeo.co.id')->subject('Void Payment Fail - ' . time());
    });


  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:08 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;

class PaymentSettlementSelector implements SelectorListener {


  private $paymentInformations, $paymentValidator;

  public function __construct() {
    $this->paymentSettlement = app()->make(\Odeo\Domains\Payment\Repository\PaymentSettlementRepository::class);
    $this->paymentSettlementDetail = app()->make(\Odeo\Domains\Payment\Repository\PaymentSettlementDetailRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $temp = [];

    $temp['id'] = $item->id;
    $temp['bank_name'] = $item->bank->name;
    $temp['settlement_date'] = $item->settlement_date;
    $temp['settlement_description'] = $item->settlement_description;
    $temp['settlement_amount'] = $item->settlement_amount;

    return $temp;
  }

  public function get(PipelineListener $listener, $data) {

    $this->paymentSettlement->normalizeFilters($data);
    $this->paymentSettlement->setSimplePaginate(true);

    $settlements = [];

    foreach ($this->paymentSettlement->gets() as $item) {
      $settlements[] = $this->_transforms($item, $this->paymentSettlement);
    }

    if (count($settlements) > 0) {
      return $listener->response(200, array_merge(['settlements' => $this->_extends($settlements, $this->paymentSettlement)],
        $this->paymentSettlement->getPagination()
      ));
    }
    return $listener->response(204);
  }

  public function getDetail(PipelineListener $listener, $data) {
    
    $this->paymentSettlementDetail->normalizeFilters($data);
    $this->paymentSettlementDetail->setSimplePaginate(true);

    $settlementDetails = [];

    foreach ($this->paymentSettlementDetail->gets() as $item) {
      $settlementDetails[] = $item;
    }

    if (count($settlementDetails) > 0) {
      return $listener->response(200, array_merge(['settlement_details' => $settlementDetails],
        $this->paymentSettlementDetail->getPagination()
      ));
    }
    return $listener->response(204);
  }

}

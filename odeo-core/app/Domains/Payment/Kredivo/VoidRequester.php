<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/3/17
 * Time: 3:54 PM
 */

namespace Odeo\Domains\Payment\Kredivo;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoApi;
use Odeo\Domains\Payment\Kredivo\Helper\KredivoManager;

class VoidRequester extends KredivoManager {

  private $paymentKredivoPaymentVoids;

  public function __construct() {
    parent::__construct();

    $this->paymentKredivoPaymentVoids = app()->make(\Odeo\Domains\Payment\Kredivo\Repository\PaymentKredivoPaymentVoidRepository::class);

  }

  public function void($payment, $data) {

    $kredivoPayment = $this->kredivoPayments->findById($payment->reference_id);

    $data = [
      'server_key' => KredivoApi::$SERVER_KEY,
      'order_id' => $data['order_id'],
      'transaction_id' => $kredivoPayment->transaction_id,
      'cancellation_reason' => 'Out of Stock',
      'cancelled_by' => 'system',
      'cancellation_date' => \Carbon\Carbon::now()->timestamp
    ];

    $client = new \GuzzleHttp\Client();

    $response = $client->post(KredivoApi::$VOID_URL, [
      'json' => $data
    ]);


    $result = json_decode($response->getBody());

    if (!(isset($result->status) && strtolower($result->status) == 'ok' && strtolower($result->transaction_status) == 'cancel')) {

      if ($result->message == 'Transaction is already cancelled.') {
        if (!$this->paymentKredivoPaymentVoids->findByOrderId($data['order_id'])) ;
        else {
          return true;
        }
      } else {
        return false;
      }

    }

    $kredivoVoid = $this->paymentKredivoPaymentVoids->getNew();

    $kredivoVoid->kredivo_payment_id = $kredivoPayment->id;
    $kredivoVoid->status = $result->status;
    $kredivoVoid->fraud_status = $result->fraud_status;
    $kredivoVoid->order_id = $result->order_id ?? $data['order_id'];
    $kredivoVoid->transaction_time = $result->transaction_time ?? null;
    $kredivoVoid->amount = $result->amount ?? null;
    $kredivoVoid->payment_type = $result->payment_type ?? null;
    $kredivoVoid->transaction_status = $result->transaction_status ?? null;
    $kredivoVoid->message = $result->message;
    $kredivoVoid->transaction_id = $result->transaction_id ?? null;

    $this->paymentKredivoPaymentVoids->save($kredivoVoid);

    return true;

  }


  public function withdrawLockedRequestVoid(PipelineListener $listener, $data) {

    $kredivoLockPayments = app()->make(\Odeo\Domains\Payment\Kredivo\Repository\PaymentKredivoLockPaymentRepository::class);
    $payments = app()->make(\Odeo\Domains\Payment\Repository\PaymentRepository::class);
    $cashInserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);

    $kredivoLockedPayments = $kredivoLockPayments->getByUserId($data['auth']['user_id']);
    $willBeDelete = [];

    foreach ($kredivoLockedPayments as $locked) {

      if ($this->void($payments->findByOrderId($locked->order_id), [
        'order_id' => $locked->order_id
      ])
      ) {
        $cashInserter->add([
          'user_id' => $locked->user_id,
          'trx_type' => TransactionType::OCASH_KREDIVO_VOID,
          'cash_type' => CashType::OCASH,
          'amount' => -$locked->locked_amount,
          'data' => json_encode([
            'order_id' => $locked->order_id
          ]),
        ]);
        $willBeDelete[] = $locked->id;
      }
    }


    $cashInserter->run();

    app()->make(\Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoLockPayment::class)
      ->whereIn('id', $willBeDelete)->delete();

    return $listener->response(200);

  }

}
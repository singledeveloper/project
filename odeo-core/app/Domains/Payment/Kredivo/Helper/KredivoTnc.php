<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/6/17
 * Time: 8:22 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Helper;

class KredivoTnc {

  public function getTnc() {

    return [
      [
        '1_text' => trans('payment/kredivo.tnc_kredivo_1'),
      ],
    ];

  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/28/16
 * Time: 10:35 PM
 */

namespace Odeo\Domains\Payment\Kredivo\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Kredivo\Model\PaymentKredivoLockPayment;

class PaymentKredivoLockPaymentRepository extends Repository {

  public function __construct(PaymentKredivoLockPayment $kredivoLockPayment) {
    $this->model = $kredivoLockPayment;
  }

  public function getByUserId($userId) {

    return $this->model
      ->where('user_id', $userId)
      ->get();

  }

}
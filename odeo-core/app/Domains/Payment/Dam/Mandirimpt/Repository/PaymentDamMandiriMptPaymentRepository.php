<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/8/16
 * Time: 12:34 AM
 */

namespace Odeo\Domains\Payment\Dam\Mandirimpt\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Payment\Dam\Mandirimpt\Model\PaymentDamMandiriMptPayment;

class PaymentDamMandiriMptPaymentRepository extends Repository {

  public function __construct(PaymentDamMandiriMptPayment $paymentDamMandiriMpt) {
    $this->model = $paymentDamMandiriMpt;
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/23/16
 * Time: 10:08 PM
 */

namespace Odeo\Domains\Payment;


use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Payment\Helper\directUrlGenerator;
use Odeo\Domains\Payment\Helper\urlGenerator;

class PaymentUserDirectCreator {

  use directUrlGenerator;

  public function create(PipelineListener $listener, $data) {


    if (isset($data['idt'])) {
      $idt = $this->decodeIdt($data['idt']);
      $data = array_merge($data, $idt);
    }

    switch ($data['opc_group']) {

      case Payment::OPC_GROUP_CC_TOKENIZATION:

        return app()->make(\Odeo\Domains\Payment\Doku\Creditcard\AddCardRequester::class)->request($listener, $data);

        break;

    }

    return $listener->response(400);

  }
}
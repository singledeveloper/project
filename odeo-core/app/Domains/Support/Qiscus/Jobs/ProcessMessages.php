<?php

namespace Odeo\Domains\Support\Qiscus\Jobs;

use Odeo\Jobs\Job;
use GuzzleHttp\Client;

class ProcessMessages extends Job{

  private $qiscusBackup, $backupHistory, $qiscusBackupMessage;

  public function __construct($qiscusBackup, $backupHistory) {
    parent::__construct();
    $this->qiscusBackup = $qiscusBackup;
    $this->backupHistory = $backupHistory;
    $this->qiscusBackupMessage = app()->make(\Odeo\Domains\Support\Qiscus\Repository\QiscusBackupMessageRepository::class);
  }

  public function handle() {
    $client = new Client();
    clog('qiscus_backup', 'start getting message for unique_id: ' . $this->qiscusBackup->unique_id);
    $response = $client->request('GET', $this->backupHistory['download_url']);
    $result = json_decode($response->getBody(), true);

    $qiscusBackupMessages = [];
    foreach($result as $message) {
      $qiscusBackupMessages[] = [
        'id' => $message['Id'],
        'qiscus_backup_id' => $this->qiscusBackup->id,
        'user_id'=> $message['CreatorEmail'],
        'message' => $message['Message'],
        'type' => $message['Type'],
        'unique_id' => $message['UniqueId'],
        'room_unique_id' => $message['RoomUniqueId'],
        'created_at' => $message['CreatedAt'],
        'url' => $message['Payload']['url'] ?? null,
        'replied_comment_id' => $message['Payload']['replied_comment_id'] ?? null
      ];
    }
    if (sizeof($qiscusBackupMessages) > 0) {
      $this->qiscusBackupMessage->saveBulk($qiscusBackupMessages);
    }
    clog('qiscus_backup', 'messages get count: ' . sizeof($qiscusBackupMessages));

    $this->qiscusBackup->download_url = $this->backupHistory['download_url'];
    $this->qiscusBackup->total_rows = $this->backupHistory['total_rows'];
    $this->qiscusBackup->save();
  }
}

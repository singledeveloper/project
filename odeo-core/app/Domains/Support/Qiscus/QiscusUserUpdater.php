<?php

namespace Odeo\Domains\Support\Qiscus;

use Carbon\Carbon;
use Odeo\Domains\Constant\QiscusUser;
use Odeo\Domains\Core\PipelineListener;

class QiscusUserUpdater {

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function updateQiscusAccount(PipelineListener $listener, $data) {
    $user = $this->users->findById($data['auth']['user_id']);
    $user->has_qiscus_account = true;
    $this->users->save($user);
    
    return $listener->response(200);

  }

}

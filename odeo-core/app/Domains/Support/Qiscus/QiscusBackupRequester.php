<?php

namespace Odeo\Domains\Support\Qiscus;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Support\Qiscus\Jobs\ProcessMessages;

class QiscusBackupRequester {

  private $qiscusBackup;

  public function __construct() {
    $this->qiscusBackup = app()->make(\Odeo\Domains\Support\Qiscus\Repository\QiscusBackupRepository::class);
  }

  public function getUrl($uniqueId = '') {
    return 'https://'.env('QISCUS_APP_ID').'.qiscus.com/api/v2.1/rest/exports/'.$uniqueId;
  }

  public function create(PipelineListener $listener, $data) {
    $startDate = Carbon::now()->subDays(30)->format('Y-m-d');
    $endDate = Carbon::yesterday()->format('Y-m-d');

    if ($qiscusBackup = $this->qiscusBackup->getLast()) {
      $lastDate = (new Carbon($qiscusBackup->end_date))->addDays(1)->format('Y-m-d');
      if ($lastDate > $startDate) {
        $startDate = $lastDate;
      }
    }
    
    if ($endDate <= $startDate) {
      return;
    }

    clog('qiscus_backup', 'start request backup from '. $startDate . ' to ' . $endDate);

    $client = new Client();
    $response = $client->request('POST', self::getUrl(), [
      'headers' => [
        'QISCUS_SDK_SECRET' => env('QISCUS_SECRET_KEY'),
        'Content-Type' => 'application/x-www-form-urlencoded'
      ],
      'form_params' => [
        'type' => 'comments',
        'start_date' => $startDate,
        'end_date' => $endDate
      ]
    ]);

    $result = json_decode($response->getBody(), true);
    clog('qiscus_backup', 'response from request backup: ' . json_encode($result));
    if($result['status'] === 200) {
      $qiscusBackup = $this->qiscusBackup->getNew();
      $qiscusBackup->unique_id = $result['results']['unique_id'];
      $qiscusBackup->total_rows = 0;
      $qiscusBackup->start_date = $startDate;
      $qiscusBackup->end_date = $endDate;
      $qiscusBackup->save();
    };

    clog('qiscus_backup', 'end request backup');
    return;
  }

  public function processMessages(PipelineListener $listener, $data) {
    clog('qiscus_backup', 'start process messages');
    foreach($qiscusBackups = $this->qiscusBackup->getUnprocessed() as $qiscusBackup) {
      clog('qiscus_backup', 'will process unique_id ' . $qiscusBackup->unique_id);
      $client = new Client();
      $response = $client->request('GET', self::getUrl($qiscusBackup->unique_id), [
        'headers' => [
          'QISCUS_SDK_SECRET' => env('QISCUS_SECRET_KEY'),
        ]
      ]);
      $result = json_decode($response->getBody(), true);
      clog('qiscus_backup', 'response from get backup: ' . json_encode($result));

      if ($result['status'] === 200) {
        $backupHistory = $result['results']['backup_history'];
        if ($backupHistory['status'] === 'finished') {
          dispatch(new ProcessMessages($qiscusBackup, $backupHistory));
        }
      }
    }

    clog('qiscus_backup', 'end process messages');
  }
}

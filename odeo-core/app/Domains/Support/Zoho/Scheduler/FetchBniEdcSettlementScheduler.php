<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/08/19
 * Time: 17.27
 */

namespace Odeo\Domains\Support\Zoho\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Support\Zoho\Helper\ZohoHelper;
use Odeo\Domains\Support\Zoho\ZohoManager;

class FetchBniEdcSettlementScheduler extends ZohoManager {

  public function run() {
    $this->init();
    $messages = collect(json_decode($this->zohoApi->getAccountBniEdcSettlementMails(), true)['data']);
    $existingFetch = $this->zohoFetchRepo->getLastFetchedData()->keyBy('message_id');

    $newFetches = [];
    $now = Carbon::now();

    foreach ($messages as $message) {
      if (!isset($existingFetch[$message['messageId']])) {
        $messageId = $message['messageId'];
        $folderId = $message['folderId'];

        $info = json_decode($this->zohoApi->getAttachmentInfo($messageId, $folderId), true)['data'];
        $trxDate = Carbon::createFromFormat('Ymd', explode('_' , $info['attachments'][0]['attachmentName'])[0]);

        $newFetches[] = [
          'message_id' => $message['messageId'],
          'folder_id' => $message['folderId'],
          'type' => ZohoHelper::TYPE_BNI_EDC_SETTLEMENT,
          'attachment_id' => $info['attachments'][0]['attachmentId'],
          'trx_date' => $trxDate,
          'created_at' => $now,
          'updated_at' => $now
        ];
      }
    }

    count($newFetches) && $this->zohoFetchRepo->saveBulk($newFetches);
  }

}
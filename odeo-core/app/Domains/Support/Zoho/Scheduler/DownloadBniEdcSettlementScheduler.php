<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 01/08/19
 * Time: 18.20
 */

namespace Odeo\Domains\Support\Zoho\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Payment\Pax\Bni\Repository\BniEdcParsedAttachmentRepository;
use Odeo\Domains\Payment\Pax\Helper\ParseHelper;
use Odeo\Domains\Support\Zoho\ZohoManager;

class DownloadBniEdcSettlementScheduler extends ZohoManager {

  private $bniParsedRepo;
  public function init() {
    parent::init();
    $this->bniParsedRepo = app()->make(BniEdcParsedAttachmentRepository::class);
  }

  public function run() {
    $this->init();
    $fetchList = $this->zohoFetchRepo->getFetchDownloadList();

    $path = "app/bni_edc_settlement";
    if (!file_exists(storage_path($path))) {
      mkdir(storage_path($path));
    }

    if (count($fetchList)) {
      $updates = $bniParses = [];
      $now = Carbon::now();
      foreach ($fetchList as $list) {
        $filename = "$list->attachment_id.zip";
        $ptr = fopen(storage_path($path) . "/$filename", 'wb');
        $stream = $this->zohoApi->getAttachmentFile($list->folder_id, $list->message_id, $list->attachment_id);
        fwrite($ptr, $stream);
        fclose($ptr);
        $updates[] = [
          'id' => $list->id,
          'is_downloaded' => true,
          'filename' => $filename,
          'updated_at' => $now
        ];

        $bniParses[] = [
          'filename' => $filename,
          'subject' => ParseHelper::PARSE_SUBJECT_BNI_EDC,
          'date' => $list->trx_date,
          'parsed' => false,
          'created_at' => $now,
          'updated_at' => $now
        ];
      }

      count($updates) && $this->zohoFetchRepo->updateBulk($updates);
      count($bniParses) && $this->bniParsedRepo->saveBulk($bniParses);
    }
  }


}
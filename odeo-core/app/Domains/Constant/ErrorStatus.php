<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 25/07/18
 * Time: 19.38
 */

namespace Odeo\Domains\Constant;

class ErrorStatus {

  const BALANCE_NOT_ENOUGH = 'BALANCE_NOT_ENOUGH';
  const DOUBLE_PURCHASE = 'DOUBLE_PURCHASE';
  const EMPTY_STOCK = 'EMPTY_STOCK';
  const ACCOUNT_BLOCKED = 'ACCOUNT_BLOCKED';
  const ACCOUNT_BANNED = 'ACCOUNT_BANNED';
  const ACCOUNT_BANNED_UPLOADED = 'ACCOUNT_BANNED_UPLOADED';
  const ACCOUNT_BANNED_REJECTED = 'ACCOUNT_BANNED_REJECTED';

}

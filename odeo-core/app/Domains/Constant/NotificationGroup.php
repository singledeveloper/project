<?php

namespace Odeo\Domains\Constant;

class NotificationGroup {

  const ACCOUNT = 1;
  const TRANSACTION = 2;

}

<?php

namespace Odeo\Domains\Constant;
use Odeo\Libraries\ISO8583\JAK8583;

class BillerJatelindo {

  const PROD_HOST = '202.152.22.116:6014';
  const PROD_TERMINAL_ID = '54OTISA1';
  const PROD_ACCEPTOR_ID = '201211111111111';
  const DEV_HOST = '202.152.22.116:6014';
  const DEV_TERMINAL_ID = '54OTISA1';
  const DEV_ACCEPTOR_ID = '201211111111111';
  /*const DEV_HOST = '182.23.53.50:1423';
  const DEV_TERMINAL_ID = 'DEVODEOX';
  const DEV_ACCEPTOR_ID = '200900100800000';*/
  const PURCHASE_MTI = '0200';
  const PURCHASE_PROCESSING_CODE = '171000';
  const ADVICE_MTI = '0220';
  const ADVICE_PROCESSING_CODE = '172000';
  const MERCHANT_TYPE = '6021';
  const AIID = '008';
  const CURRENCY_CODE = '360';
  
  public static function getHost() {
    return app()->environment() == "production" ? self::PROD_HOST : self::DEV_HOST;
  }
  
  public static function getTerminalId() {
    return app()->environment() == "production" ? self::PROD_TERMINAL_ID : self::DEV_TERMINAL_ID;
  }
  
  public static function getAcceptorId() {
    return app()->environment() == "production" ? self::PROD_ACCEPTOR_ID : self::DEV_ACCEPTOR_ID;
  }
  
  public static function encodeISO($mti, $data) {
    $jak = new JAK8583();
    
    $jak->addMTI($mti);
    foreach ($data as $key => $item) {
      $jak->addData($key, $item);
    }
    
    return $jak->getISO();
  }
  
  public static function decodeISO($string) {
    $jak = new JAK8583();

    $jak->addISO($string);
    return $jak->getData();
  }
  
  public static function formatRequest($iso) {
    $header = pack("n*", (strlen($iso) + 1));
    $trailer = pack("H*", '03');
    
    return $header . $iso . $trailer;
  }

  public static function getParts($string, $positions = []) {
    $parts = [];

    foreach ($positions as $position){
      $parts[] = substr($string, 0, $position);
      $string = substr($string, $position);
    }

    return $parts;
  }
}
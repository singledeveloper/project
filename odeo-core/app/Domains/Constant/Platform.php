<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 9:14 PM
 */

namespace Odeo\Domains\Constant;


class Platform {

  const ANDROID = 1;
  const WEB_STORE = 2;
  const WEB_APP = 3;
  const IOS = 4;
  const ADMIN = 5;
  const WEB_COMPANY = 6;
  const AFFILIATE = 7;
  const TELEGRAM = 8;
  const JABBER = 9;
  const SMS_CENTER = 10;
  const AUTO_RECURRING = 11;
  const DISBURSEMENT = 12;
  const VIRTUAL_ACCOUNT = 13;
  const SELLER_CENTER = 14;
  const SYSTEM = 15;
  const ANDROID_OCASH = 16;
  const TERMINAL = 17;
  const SUPPORT = 18;
  const H2H = 100;
  const PAYMENT_GATEWAY = 101;
  
  const ANDROID_SECRET = 'SFuvJTpfXdvVYVHZ1gp4';
  const WEB_STORE_SECRET = '1RgKxNFErftT3igyoURT';
  const WEB_APP_SECRET = 'gvpW7dThBGgEeinKBEsy';
  const IOS_SECRET = 'bCciXHygkmfLh0VO2sKz';

  public static function getConstKeyByValue($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if ($value == $searchVal) return $name;
    }

    return null;
  }

  public static function getConstKeyStringByValue($searchVal) {
    $name = self::getConstKeyByValue($searchVal);
    $name = title_case($name);
    $name = str_replace('_', ' ', $name);
    return $name;
  }

  public static function getAppPlatforms() {
    return [
      self::ANDROID,
      self::IOS,
      self::WEB_APP,
      self::WEB_STORE,
      self::AUTO_RECURRING,
    ];
  }

  public static function getBrowserPlatform() {
    return [
      self::WEB_APP,
      self::WEB_STORE,
    ];
  }

  public static function getH2HPlatforms() {
    return [
      self::AFFILIATE,
      self::JABBER,
    ];
  }

  public static function getOthersPlatforms() {
    return [
      self::SMS_CENTER,
      self::TELEGRAM,
    ];
  }

  public static function generateCartSignature($data) {
    $platformId = isset($data['auth']['platform_id']) ? $data['auth']['platform_id'] : $data['platform_id'];
    switch ($platformId) {
      case self::ANDROID:
        $secret = self::ANDROID_SECRET;
        break;
      case self::WEB_STORE:
        $secret = self::WEB_STORE_SECRET;
        break;
      case self::WEB_APP:
        $secret = self::WEB_APP_SECRET;
        break;
      case self::IOS:
        $secret = self::IOS_SECRET;
        break;
      default:
        return null;
        break;
    }
    return \hash("sha256", ('' . $data["gateway_id"] . $platformId . $data["cart_id"]) . $secret);
  }

}

<?php

namespace Odeo\Domains\Constant;

class RedigDisbursement {

  const PENDING = '10000';
  const SUSPECT = '20000';
  const TIMEOUT = '30000';
  const SUCCESS = '50000';
  const FAIL = '90000';

  const INQUIRY_COST = 0;
  const COST = 3200;

  const STATUS_CODE_INQUIRY_FAILED = 'INQUIRY_FAILED';
  const STATUS_CODE_EXCEPTION = 'EXCEPTION';
  const STATUS_CODE_UNKNOWN = 'UNKNOWN';

  const VENDOR_SWITCHER_ID = 43;

  const STATUS_CODE_TO_MESSAGE = [
    '10000' => 'PENDING',
    '20000' => 'SUSPECT',
    '30000' => 'TIMEOUT',
    '50000' => 'SUCCESS',
    '90000' => 'FAIL',
  ];

  const DISABLE_DISBURSEMENT_REDIS_KEY = 'odeo_core:disable_redig_disbursement';
  const DISABLED_COUNT_DISBURSEMENT_REDIS_KEY = 'odeo_core:disabled_count_redig_disbursement';

  public static function toStatusMessage($status) {
    return self::STATUS_CODE_TO_MESSAGE[$status];
  }

}
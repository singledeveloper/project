<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/3/17
 * Time: 4:12 PM
 */

namespace Odeo\Domains\Constant;


class FlipMutationDisbursement {

  const TYPE_REFUND = 'refund';
  const TYPE_TRANSFER = 'transfer';
  const TYPE_TOPUP = 'topup';

}
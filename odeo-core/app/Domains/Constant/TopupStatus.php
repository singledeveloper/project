<?php

namespace Odeo\Domains\Constant;

class TopupStatus {
  
 const PENDING = 10000;
 const COMPLETED = 50000;
 const CANCELLED = 90000;

 const TOPUP_SUSPECT_LIMIT = 3;
 const TOPUP_OVERLIMIT_MIN_AMOUNT = 10000000;
 
}

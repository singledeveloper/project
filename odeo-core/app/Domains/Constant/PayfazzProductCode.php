<?php

namespace Odeo\Domains\Constant;

class PayfazzProductCode {

  const PAYFAZZ = 'payfazz';

  const OPERATOR_TELKOMSEL = 'telkomsel';
  const OPERATOR_INDOSAT = 'indosat';
  const OPERATOR_XL = 'xl';
  const OPERATOR_THREE = 'three';
  const OPERATOR_BOLT = 'bolt';
  const OPERATOR_SMART = 'smart';
  const OPERATOR_PLN = 'pln';
  const OPERATOR_GARENA = 'garena';
  const OPERATOR_AXIS = 'axis';
  const OPERATOR_TELKOMSEL_DATA = 'telkomsel_data';
  const OPERATOR_XL_DATA = 'xl_data';
  const OPERATOR_INDOSAT_DATA = 'indosat_data';
  const OPERATOR_THREE_DATA = 'three_data';
  const OPERATOR_SMARTFREN_DATA = 'smartfren_data';
  const OPERATOR_BOLT_DATA = 'bolt_data';
  const OPERATOR_AXIS_DATA = 'axis_data';
  const OPERATOR_BPJS = 'bpjs';
  const OPERATOR_PDAM = 'pdam';
  const OPERATOR_TELKOMSEL_POSTPAID = 'telkomsel_postpaid';
  const OPERATOR_SMARTFREN_POSTPAID = 'smartfren_postpaid';
  const OPERATOR_INDOSAT_POSTPAID = 'indosat_postpaid';
  const OPERATOR_PLN_POSTPAID = 'pln_postpaid';
  const OPERATOR_TOPAS_POSTPAID = 'topas_postpaid';
  const OPERATOR_NEX_POSTPAID = 'nex_postpaid';
  const OPERATOR_BIG_POSTPAID = 'big_postpaid';
  const OPERATOR_TELKOM_POSTPAID = 'telkom_postpaid';
  const OPERATOR_ADIRA_POSTPAID = 'adira_postpaid';
  const OPERATOR_BAF_POSTPAID = 'baf_postpaid';
  const OPERATOR_CLMB_POSTPAID = 'clmb_postpaid';
  const OPERATOR_MAF_POSTPAID = 'maf_postpaid';
  const OPERATOR_MEGA_POSTPAID = 'mega_postpaid';
  const OPERATOR_WOM_POSTPAID = 'wom_postpaid';
  const OPERATOR_INDIHOME_POSTPAID = 'indihome_postpaid';
  const OPERATOR_UNIPIN = 'unipin';
  const OPERATOR_INDOVISION_POSTPAID = 'indovision_postpaid';
  const OPERATOR_GOPAY_DRIVER = 'gopay_driver';
  const OPERATOR_GOPAY_CUSTOMER = 'gopay_customer';
  const OPERATOR_GOPAY_MERCHANT = 'gopay_merchant';
  const OPERATOR_PAYFAZZ = 'payfazz';

  const TLKM5 = 'TLKM5';
  const TLKM10 = 'TLKM10';
  const TLKM20 = 'TLKM20';
  const TLKM25 = 'TLKM25';
  const TLKM50 = 'TLKM50';
  const TLKM100 = 'TLKM100';
  const TLKM150 = 'TLKM150';
  const TLKM300 = 'TLKM300';
  const INDST5 = 'INDST5';
  const INDST10 = 'INDST10';
  const INDST30 = 'INDST30';
  const INDST20 = 'INDST20';
  const INDST25 = 'INDST25';
  const INDST50 = 'INDST50';
  const INDST100 = 'INDST100';
  const XL5 = 'XL5';
  const XL10 = 'XL10';
  const XL15 = 'XL15';
  const XL25 = 'XL25';
  const XL30 = 'XL30';
  const XL50 = 'XL50';
  const XL100 = 'XL100';
  const XL200 = 'XL200';
  const XL_SMS20014 = 'XL_SMS20014';
  const XL_SMS50030 = 'XL_SMS50030';
  const THREE1 = 'THREE1';
  const THREE2 = 'THREE2';
  const THREE3 = 'THREE3';
  const THREE4 = 'THREE4';
  const THREE5 = 'THREE5';
  const THREE10 = 'THREE10';
  const THREE20 = 'THREE20';
  const THREE25 = 'THREE25';
  const THREE50 = 'THREE50';
  const THREE100 = 'THREE100';
  const THREE_BICARA40 = 'THREE_BICARA40';
  const THREE_BICARA120 = 'THREE_BICARA120';
  const THREE_BICARA250 = 'THREE_BICARA250';
  const BOLT25 = 'BOLT25';
  const BOLT50 = 'BOLT50';
  const BOLT100 = 'BOLT100';
  const BOLT150 = 'BOLT150';
  const BOLT200 = 'BOLT200';
  const SMART5 = 'SMART5';
  const SMART10 = 'SMART10';
  const SMART20 = 'SMART20';
  const SMART50 = 'SMART50';
  const SMART100 = 'SMART100';
  const PLN20 = 'PLN20';
  const PLN50 = 'PLN50';
  const PLN100 = 'PLN100';
  const PLN200 = 'PLN200';
  const PLN500 = 'PLN500';
  const PLN1000 = 'PLN1000';
  const GARENA10 = 'GARENA10';
  const GARENA20 = 'GARENA20';
  const GARENA50 = 'GARENA50';
  const GARENA100 = 'GARENA100';
  const GARENA150 = 'GARENA150';
  const GARENA300 = 'GARENA300';
  const AXIS5 = 'AXIS5';
  const AXIS10 = 'AXIS10';
  const AXIS15 = 'AXIS15';
  const AXIS25 = 'AXIS25';
  const AXIS30 = 'AXIS30';
  const AXIS50 = 'AXIS50';
  const AXIS100 = 'AXIS100';
  const DATA_TLKM5 = 'DATA_TLKM5';
  const DATA_TLKM10 = 'DATA_TLKM10';
  const DATA_TLKM20 = 'DATA_TLKM20';
  const DATA_TLKM25 = 'DATA_TLKM25';
  const DATA_TLKM50 = 'DATA_TLKM50';
  const DATA_TLKM100 = 'DATA_TLKM100';
  const DATA_XL_XTRACOMBO6 = 'DATA_XL_XTRACOMBO6';
  const DATA_XL_XTRACOMBO12 = 'DATA_XL_XTRACOMBO12';
  const DATA_XL_XTRACOMBO18 = 'DATA_XL_XTRACOMBO18';
  const DATA_XL_XTRACOMBO30 = 'DATA_XL_XTRACOMBO30';
  const DATA_XL_XTRACOMBO42 = 'DATA_XL_XTRACOMBO42';
  const DATA_XL80 = 'DATA_XL80';
  const DATA_XL1_5 = 'DATA_XL1_5';
  const DATA_XL3 = 'DATA_XL3';
  const DATA_XL6 = 'DATA_XL6';
  const DATA_XL8 = 'DATA_XL8';
  const DATA_XL12 = 'DATA_XL12';
  const DATA_XL16 = 'DATA_XL16';
  const DATA_XL_HTRDXTRA1GB = 'DATA_XL_HTRDXTRA1GB';
  const DATA_XL_HTRDXTRA2GB = 'DATA_XL_HTRDXTRA2GB';
  const DATA_XL_HTRDXTRA4GB = 'DATA_XL_HTRDXTRA4GB';
  const DATA_XL_HTRDXTRA6GB = 'DATA_XL_HTRDXTRA6GB';
  const DATA_XL_HTRDXTRA8GB = 'DATA_XL_HTRDXTRA8GB';
  const DATA_XL_HTRDXTRA10GB = 'DATA_XL_HTRDXTRA10GB';
  const DATA_XL_HTRDXTRA16GB = 'DATA_XL_HTRDXTRA16GB';
  const DATA_BBG17 = 'DATA_BBG17';
  const DATA_BBGD45 = 'DATA_BBGD45';
  const DATA_BBS25 = 'DATA_BBS25';
  const DATA_BBS70 = 'DATA_BBS70';
  const DATA_BBD81 = 'DATA_BBD81';
  const DATA_BGU20 = 'DATA_BGU20';
  const DATA_BGUD49 = 'DATA_BGUD49';
  const DATA_BFU30 = 'DATA_BFU30';
  const DATA_BFUD76 = 'DATA_BFUD76';
  const DATA_BBBD49 = 'DATA_BBBD49';
  const DATA_BXGJ045 = 'DATA_BXGJ045';
  const DATA_BXD54 = 'DATA_BXD54';
  const DATA_BXGJ090 = 'DATA_BXGJ090';
  const DATA_BB3IN1D = 'DATA_BB3IN1D';
  const DATA_BXFL075 = 'DATA_BXFL075';
  const DATA_BXFL135 = 'DATA_BXFL135';
  const DATA_BXFS090 = 'DATA_BXFS090';
  const DATA_BXFS180 = 'DATA_BXFS180';
  const INDST_M1 = 'INDST_M1';
  const DATA_INDST1 = 'DATA_INDST1';
  const INDST_M2 = 'INDST_M2';
  const DATA_INDST3 = 'DATA_INDST3';
  const DATA_INDST5 = 'DATA_INDST5';
  const DATA_INDST10 = 'DATA_INDST10';
  const DATA_INDST1GB = 'DATA_INDST1GB';
  const DATA_INDST2GB = 'DATA_INDST2GB';
  const DATA_INDST3GB = 'DATA_INDST3GB';
  const DATA_INDST4GB = 'DATA_INDST4GB';
  const DATA_BBG30 = 'DATA_BBG30';
  const DATA_INDST5GB = 'DATA_INDST5GB';
  const DATA_BBG90 = 'DATA_BBG90';
  const DATA_THREE1 = 'DATA_THREE1';
  const DATA_THREE2 = 'DATA_THREE2';
  const DATA_THREE3 = 'DATA_THREE3';
  const DATA_THREE4 = 'DATA_THREE4';
  const DATA_THREE5 = 'DATA_THREE5';
  const DATA_THREE6 = 'DATA_THREE6';
  const DATA_THREE8 = 'DATA_THREE8';
  const DATA_THREE10 = 'DATA_THREE10';
  const DATA_AON1 = 'DATA_AON1';
  const DATA_THREE20MB = 'DATA_THREE20MB';
  const DATA_THREE80MB = 'DATA_THREE80MB';
  const DATA_AON2 = 'DATA_AON2';
  const DATA_THREE1250MB = 'DATA_THREE1250MB';
  const DATA_AON3 = 'DATA_AON3';
  const DATA_AON4 = 'DATA_AON4';
  const DATA_THREE300MB = 'DATA_THREE300MB';
  const DATA_THREE4250MB = 'DATA_THREE4250MB';
  const DATA_AON5 = 'DATA_AON5';
  const DATA_THREE650MB = 'DATA_THREE650MB';
  const DATA_AON6 = 'DATA_AON6';
  const DATA_AON8 = 'DATA_AON8';
  const DATA_AON10 = 'DATA_AON10';
  const DATA_ANDROMAX25 = 'DATA_ANDROMAX25';
  const DATA_ANDROMAX50 = 'DATA_ANDROMAX50';
  const DATA_ANDROMAX100 = 'DATA_ANDROMAX100';
  const DATA_BOLT1_5 = 'DATA_BOLT1_5';
  const DATA_BOLT3 = 'DATA_BOLT3';
  const DATA_BOLT8 = 'DATA_BOLT8';
  const DATA_BOLT13 = 'DATA_BOLT13';
  const DATA_BOLT17 = 'DATA_BOLT17';
  const DATA_24BRO7 = 'DATA_24BRO7';
  const DATA_BRO1 = 'DATA_BRO1';
  const DATA_BRO2 = 'DATA_BRO2';
  const DATA_BRO3 = 'DATA_BRO3';
  const DATA_BRO5 = 'DATA_BRO5';
  const DATA_IRITP7 = 'DATA_IRITP7';
  const AETRA = 'AETRA';
  const PALYJA = 'PALYJA';
  const PDAMKAB_BALANGAN = 'PDAMKAB_BALANGAN';
  const PDAMKAB_BANDUNG = 'PDAMKAB_BANDUNG';
  const PDAMKAB_BANGKALAN = 'PDAMKAB_BANGKALAN';
  const PDAMKAB_BANJARBARU = 'PDAMKAB_BANJARBARU';
  const PDAMKAB_BOGOR = 'PDAMKAB_BOGOR';
  const PDAMKAB_BOJONEGORO = 'PDAMKAB_BOJONEGORO';
  const PDAMKAB_BONDOWOSO = 'PDAMKAB_BONDOWOSO';
  const PDAMKAB_BOYOLALI = 'PDAMKAB_BOYOLALI';
  const PDAMKAB_BREBES = 'PDAMKAB_BREBES';
  const PDAMKAB_BULELENG = 'PDAMKAB_BULELENG';
  const PDAMKAB_GROBOGAN = 'PDAMKAB_GROBOGAN';
  const PDAMKAB_JEMBER = 'PDAMKAB_JEMBER';
  const PDAMKAB_KUBURAYA = 'PDAMKAB_KUBURAYA';
  const PDAMKAB_MOJOKERTO = 'PDAMKAB_MOJOKERTO';
  const PDAMKAB_PROBOLINGGO = 'PDAMKAB_PROBOLINGGO';
  const PDAMKAB_PURBALINGGA = 'PDAMKAB_PURBALINGGA';
  const PDAMKAB_PURWOREJO = 'PDAMKAB_PURWOREJO';
  const PDAMKAB_REMBANG = 'PDAMKAB_REMBANG';
  const PDAMKAB_SIDOARJO = 'PDAMKAB_SIDOARJO';
  const PDAMKAB_SITUBONDO = 'PDAMKAB_SITUBONDO';
  const PDAMKAB_TAPIN = 'PDAMKAB_TAPIN';
  const PDAMKAB_WONOSOBO = 'PDAMKAB_WONOSOBO';
  const PDAMKOTA_BANDARLAMPUNG = 'PDAMKOTA_BANDARLAMPUNG';
  const PDAMKOTA_BANDUNG = 'PDAMKOTA_BANDUNG';
  const PDAMKOTA_BEKASI = 'PDAMKOTA_BEKASI';
  const PDAMKOTA_BOGOR = 'PDAMKOTA_BOGOR';
  const PDAMKOTA_MANADO = 'PDAMKOTA_MANADO';
  const PDAMKOTA_MATARAM = 'PDAMKOTA_MATARAM';
  const PDAMKOTA_MEDAN = 'PDAMKOTA_MEDAN';
  const PDAMKOTA_PALEMBANG = 'PDAMKOTA_PALEMBANG';
  const PDAMKOTA_PASURUAN = 'PDAMKOTA_PASURUAN';
  const PDAMKOTA_SALATIGA = 'PDAMKOTA_SALATIGA';
  const PDAMKOTA_SURABAYA = 'PDAMKOTA_SURABAYA';
  const PDAMKOTA_TANAHGROGOT = 'PDAMKOTA_TANAHGROGOT';
  const PP_HALO = 'PP_HALO';
  const PP_SMART = 'PP_SMART';
  const PP_MATRIX = 'PP_MATRIX';
  const PP_PLN = 'PP_PLN';
  const PP_TVTOPAS = 'PP_TVTOPAS';
  const PP_TVNEX = 'PP_TVNEX';
  const PP_TVBIG = 'PP_TVBIG';
  const PP_TELKOMPSTN = 'PP_TELKOMPSTN';
  const PP_FNADIRA = 'PP_FNADIRA';
  const PP_FNBAF = 'PP_FNBAF';
  const PP_FNCLMB = 'PP_FNCLMB';
  const PP_FNMAF = 'PP_FNMAF';
  const PP_FNMEGA = 'PP_FNMEGA';
  const PP_FNWOM = 'PP_FNWOM';
  const PP_SPEEDY = 'PP_SPEEDY';
  const UNIPIN10 = 'UNIPIN10';
  const UNIPIN20 = 'UNIPIN20';
  const UNIPIN50 = 'UNIPIN50';
  const UNIPIN100 = 'UNIPIN100';
  const UNIPIN300 = 'UNIPIN300';
  const UNIPIN500 = 'UNIPIN500';
  const PP_TVIDVS = 'PP_TVIDVS';
  const GOPAY_DRIVER = 'GOPAY_DRIVER';
  const GOPAY_CUSTOMER = 'GOPAY_CUSTOMER';
  const GOPAY_MERCHANT = 'GOPAY_MERCHANT';
  const WAL1 = 'WAL1';
  const WAL2 = 'WAL2';
  const WAL3 = 'WAL3';
  const WAL4 = 'WAL4';
  const WAL5 = 'WAL5';
  const WAL10 = 'WAL10';
  const WAL20 = 'WAL20';
  const WAL150 = 'WAL150';
  const WAL50 = 'WAL50';
  const WAL75 = 'WAL75';
  const WAL100 = 'WAL100';
  const WAL200 = 'WAL200';
  const WAL300 = 'WAL300';
  const WAL500 = 'WAL500';
  const WAL1000 = 'WAL1000';
  const WAL25 = 'WAL25';
  const WAL30 = 'WAL30';

  public static function allowedOperator() {
    return [
      self::OPERATOR_TELKOMSEL,
      self::OPERATOR_INDOSAT,
      self::OPERATOR_XL,
      self::OPERATOR_THREE,
      self::OPERATOR_BOLT,
      self::OPERATOR_SMART,
      self::OPERATOR_PLN,
      self::OPERATOR_AXIS,
      self::OPERATOR_TELKOMSEL_DATA,
      self::OPERATOR_XL_DATA,
      self::OPERATOR_INDOSAT_DATA,
      self::OPERATOR_THREE_DATA,
      self::OPERATOR_SMARTFREN_DATA,
      self::OPERATOR_BOLT_DATA,
      self::OPERATOR_AXIS_DATA,
      self::OPERATOR_BPJS,
      self::OPERATOR_TELKOMSEL_POSTPAID,
      self::OPERATOR_SMARTFREN_POSTPAID,
      self::OPERATOR_INDOSAT_POSTPAID,
      self::OPERATOR_PLN_POSTPAID,
      self::OPERATOR_TELKOM_POSTPAID,
    ];
  }

  public static function mapWithOdeo() {
    return [
      self::TLKM5 => 1,
      self::TLKM10 => 2,
      self::TLKM20 => 3,
      self::TLKM25 => 4,
      self::TLKM50 => 5,
      self::TLKM100 => 6,
      self::TLKM150 => 171,
      self::TLKM300 => 173,
      self::INDST5 => 20,
      self::INDST10 => 21,
      self::INDST20 => 23,
      self::INDST30 => 25,
      self::INDST25 => 24,
      self::INDST50 => 26,
      self::INDST100 => 27,
      self::XL5 => 68,
      self::XL10 => 69,
      self::XL15 => 70,
      self::XL25 => 71,
      self::XL30 => 174,
      self::XL50 => 72,
      self::XL100 => 73,
      self::XL200 => 74,
      self::THREE1 => 87,
      self::THREE2 => 214,
      self::THREE3 => 215,
      self::THREE4 => 216,
      self::THREE5 => 88,
      self::THREE10 => 89,
      self::THREE20 => 90,
      self::THREE25 => 91,
      self::THREE50 => 94,
      self::THREE100 => 96,
      self::BOLT25 => 163,
      self::BOLT50 => 164,
      self::BOLT100 => 165,
      self::BOLT150 => 166,
      self::BOLT200 => 167,
      self::SMART5 => 139,
      self::SMART10 => 140,
      self::SMART20 => 141,
      self::SMART50 => 144,
      self::SMART100 => 147,
      self::PLN20 => 157,
      self::PLN50 => 158,
      self::PLN100 => 159,
      self::PLN200 => 160,
      self::PLN500 => 161,
      self::PLN1000 => 162,
      self::AXIS5 => 130,
      self::AXIS10 => 131,
      self::AXIS15 => 132,
      self::AXIS25 => 133,
      self::AXIS30 => 187,
      self::AXIS50 => 134,
      self::AXIS100 => 135,
      self::DATA_TLKM5 => 7,
      self::DATA_TLKM10 => 8,
      self::DATA_TLKM20 => 9,
      self::DATA_TLKM25 => 10,
      self::DATA_TLKM50 => 11,
      self::DATA_TLKM100 => 12,
      self::DATA_XL80 => 206,
      self::DATA_XL3 => 78,
      self::DATA_XL6 => 79,
      self::DATA_XL8 => 80,
      self::DATA_XL12 => 208,
      self::DATA_XL16 => 207,
      self::DATA_INDST1 => 34,
      self::DATA_INDST3 => 35,
      self::DATA_INDST5 => 36,
      self::DATA_INDST10 => 37,
      self::DATA_THREE1 => 103,
      self::DATA_THREE2 => 105,
      self::DATA_THREE3 => 106,
      self::DATA_THREE4 => 107,
      self::DATA_THREE5 => 109,
      self::DATA_THREE6 => 110,
      self::DATA_THREE8 => 111,
      self::DATA_THREE10 => 112,
      self::DATA_BOLT1_5 => 168,
      self::DATA_BOLT3 => 169,
      self::DATA_BOLT8 => 170,
      self::DATA_BRO1 => 137,
      self::DATA_BRO2 => 138,
      self::DATA_BRO3 => 204,
      self::DATA_BRO5 => 205,
    ];
  }
}

<?php

namespace Odeo\Domains\Constant;

class BillerMKM {

  const PROD_SERVER = 'http://192.168.133.100:18010/H2HMKM/interface.php'; // mkmvpn
  const DEV_SERVER = 'http://202.52.50.200/demo/develH2H/interface.php';
  const PROD_CLIENT_ID = 'odeo';
  const DEV_CLIENT_ID = 'test_odeo';
  const MCC = 6021;
  const VOUCHER_ID = 800;
  const ACTION_INQUIRY = 'inquiry';
  const ACTION_PAYMENT = 'payment';
  const ACTION_ADVICE = 'advice';
  const ACTION_BALANCE = 'balance';
  
  const MAX_ADVICE_COUNT = 3;

  public static function getServer() {
    return app()->environment() == 'production' ? self::PROD_SERVER : self::DEV_SERVER;
  }

  public static function getClientId() {
    return app()->environment() == 'production' ? self::PROD_CLIENT_ID : self::DEV_CLIENT_ID;
  }

  public static function isFail($status) {
    return $status != '0068' && $status != '0168' && $status != '0184' && $status != '0000';
    //return intval($status) < 100 || $status == '0163' || $status == '0172' || $status == '0180';
  }

  public static function checkWrongNumberPattern($string) {
    return checkPatterns($string, ['nomor yang Anda masukkan tidak valid', '.Nmr salah.']);
  }
}
<?php


namespace Odeo\Domains\Constant;


class RedigApiRequestReference {

  const PRE_DISBURSEMENT_INQUIRY = 'pre_disbursement_inquiry';
  const DISBURSEMENT = 'disbursement';

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 10/9/16
 * Time: 6:46 PM
 */

namespace Odeo\Domains\Constant;


class Header {

  const ANDROID_HEADER_VER = "X-Odeo-Version-Android";
  const IOS_HEADER_VER = "X-Odeo-Version-IOS";
  const WEB_APP_HEADER_VER = "X-Odeo-Version-Mobile";
  const WEB_STORE_HEADER_VER = "X-Odeo-Version-Web-Store";
  const ANDROID_OCASH_HEADER_VER = "X-Ocash-Version-Android";

  static function getHeaderByPlatform($platformId) {
    switch ($platformId) {
      case Platform::IOS:
        return self::IOS_HEADER_VER;
      case Platform::WEB_APP:
        return self::WEB_APP_HEADER_VER;
      case Platform::ANDROID:
        return self::ANDROID_HEADER_VER;
      case Platform::WEB_COMPANY:
      case Platform::WEB_STORE:
        return self::WEB_STORE_HEADER_VER;
      case Platform::ANDROID_OCASH:
        return self::ANDROID_OCASH_HEADER_VER;
    }
    return false;
  }

  static function getAllHeaderPlatform() {
    return [
      self::ANDROID_HEADER_VER,
      self::WEB_APP_HEADER_VER,
      self::IOS_HEADER_VER,
      self::WEB_STORE_HEADER_VER
    ];
  }

}

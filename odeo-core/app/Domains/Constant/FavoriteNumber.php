<?php

namespace Odeo\Domains\Constant;


class FavoriteNumber {

  const PULSA = 1;
  const PLN = 2;
  const PULSA_POSTPAID = 3;
  const BROADBAND = 4;
  const LANDLINE = 5;
  const BPJS = 6;
  const PDAM = 7;
  const GAME_VOUCHER = 8;
  const EMONEY = 9;
  const BOLT = 12;
  const PGN = 13;
  const OCASH = 14;
  const TRANSPORTATION = 24;

  public static function getCategory($serviceId) {
    switch($serviceId) {
      case Service::PULSA:
      case Service::PAKET_DATA:
        return self::PULSA;
      case Service::PLN:
      case Service::PLN_POSTPAID:
        return self::PLN;
      case Service::PULSA_POSTPAID:
        return self::PULSA_POSTPAID;
      case Service::BROADBAND:
        return self::BROADBAND;
      case Service::LANDLINE:
        return self::LANDLINE;
      case Service::BPJS_KES:
        return self::BPJS;
      case Service::PDAM:
        return self::PDAM;
      case Service::GAME_VOUCHER:
        return self::GAME_VOUCHER;
      case Service::EMONEY:
        return self::EMONEY;
      case Service::TRANSPORTATION:
        return self::TRANSPORTATION;
      case Service::BOLT:
        return self::BOLT;
      case Service::PGN:
        return self::PGN;
      case Service::OCASH:
        return self::OCASH;
      default:
        return 0;
    }
  }
}

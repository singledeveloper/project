<?php

namespace Odeo\Domains\Constant;

class OrderStatus {

  const CREATED = 10000;
  const OPENED = 10001;
  const CONFIRMED = 20001;
  const FAKE_CONFIRMED = 20002;
  const WAITING_FOR_PAYMENT_VENDOR_APPROVAL = 20003;
  const VERIFIED = 30001;
  const PARTIAL_FULFILLED = 30002;
  const WAITING_FOR_UPDATE = 30003;
  const WAITING_SUSPECT = 30005;
  const COMPLETED = 50000;
  const FREE_ORDER_COMPLETED = 50001;
  const CANCELLED = 90000;
  const REFUNDED = 90001;
  const TEMPORARY_FAILED = 90002;
  const MANUAL_FULFILLED = 90003;
  const VOIDED = 90004;
  const REFUNDED_AFTER_SETTLEMENT = 90005;

  const ORDER_REFUNDED = [
    self::REFUNDED,
    self::VOIDED,
    self::REFUNDED_AFTER_SETTLEMENT
  ];

  public static function isRefunded($orderStatus) {
    return in_array($orderStatus, self::ORDER_REFUNDED);
  }

  public static function getConstKeyByValue($searchVal) {

    $constants = (new \ReflectionClass(__CLASS__))->getConstants();

    foreach ($constants as $name => $value) {
      if ($value == $searchVal) {
        return $name;
      }
    }
    return null;
  }
}

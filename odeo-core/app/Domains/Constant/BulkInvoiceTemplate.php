<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 17/12/19
 * Time: 13.03
 */

namespace Odeo\Domains\Constant;


class BulkInvoiceTemplate {

  const VERSION = 'Version 1';
  const SHEET_INFORMATION = 'information';
  const SHEET_TEMPLATE = 'template';

}
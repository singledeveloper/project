<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 10/04/18
 * Time: 13.45
 */

namespace Odeo\Domains\Constant;


class PaymentGatewayAccount {

  const MANDIRI_ECASH = 1;
  const DOKU = 2;

}

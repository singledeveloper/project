<?php

namespace Odeo\Domains\Constant;

class UserType {
  const ADMIN = 'admin';
  const USER = 'user';
  const SELLER = 'seller';
  const GUEST = 'guest';
  const AFFILIATE = 'affiliate';
  const DISBURSEMENT = 'disbursement';
  const INTERNAL = 'internal';
  const TERMINAL = 'terminal';

  const BUSINESS_PREFIX = '62803';

  const GROUP_BYPASS_KYC = 'CashTransferBypassKYC';
  const GROUP_BYPASS_WITHDRAW_LIMIT = 'CashTransferBypassCashOutLimit';
  const GROUP_BYPASS_WITHDRAW_FEE = 'CashBypassWithdrawFee';
  const GROUP_BYPASS_CASH_LIMIT = 'CashBypassCashLimit';
  const GROUP_HAS_PATCH_TRANSACTION_PERMISSION = 'AdminPatchTransaction';
  const GROUP_HAS_VERIFY_ORDER_PERMISSION = 'AdminVerifyOrder';
  const GROUP_BULK_PURCHASE_APPROVER = 'AdminBulkPurchaseApprover';
  const GROUP_NO_REFUND_EMAIL = 'NoRefundEmail';
  const GROUP_CAN_ACCESS_PULSA_BULK = 'PulsaBulk';
}

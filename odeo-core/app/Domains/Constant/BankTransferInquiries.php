<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/6/17
 * Time: 5:54 PM
 */

namespace Odeo\Domains\Constant;


class BankTransferInquiries {

  const REFERENCE_TYPE_KREDIVO_PAYBACK = 'kredivo_payback';
  const REFERENCE_TYPE_INTEREST = 'interest';
  const REFERENCE_TYPE_WITHDRAW_ID = 'withdraw_id';
  const REFERENCE_TYPE_ORDER_ID = 'order_id';
  const REFERENCE_TYPE_UNRECOGNIZED = 'unrecognized';
  const REFERENCE_TYPE_SETTLEMENT_ID = 'settlement_id';

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/9/17
 * Time: 12:10 AM
 */

namespace Odeo\Domains\Constant;


class BankAccount {

  const MANDIRI_ODEO = 1;
  const BCA_ODEO = 2;
  const BRI_ODEO = 3;
  const CIMB_ODEO = 4;
  const PERMATA_ODEO = 6;
  const MANDIRI_GIRO = 7;
  const MAYBANK = 8;

}
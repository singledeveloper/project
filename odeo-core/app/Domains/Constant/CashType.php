<?php

namespace Odeo\Domains\Constant;

class CashType {

  const OCASH = 'cash';
  const ODEPOSIT = 'deposit';
  const OADS = 'ads';
  const ORUSH = 'rush';
  const OCOD = 'cod';
  const OCASHBACK = 'cashback';
  const TEMP_DEPOSIT_LOCK = 'temp_deposit_lock';

  const AMOUNT_WARNING = 250000;

}

<?php

namespace Odeo\Domains\Constant;

class BillerRaja {
  
  const PROD_SERVER = 'https://202.43.173.234/transaksi/';
  const PROD_SERVER_2 = 'https://180.250.248.139/transaksi/';
  const USER_ID = 'SP85073';
  const PASSWORD = '240039';
  
  public static function formatXML($name, $data) {
    $xml = '';
    foreach ($data as $item) {
      $xml .= '<param><value><string>' . $item . '</string></value></param>';
    }
    return '<?xml version="1.0"?><methodCall><methodName>' . $name . '</methodName><params>' . $xml . '</params></methodCall>';
  }

  public static function checkWrongNumberPattern($string) {
    return checkPatterns($string, ['Nomor ponsel tidak benar,']);
  }

  public static function checkFailurePattern($string) {
    return checkPatterns($string, ['sedang dalam gangguan.']);
  }
  
}
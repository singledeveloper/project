<?php

namespace Odeo\Domains\Constant;

class NetworkConfig
{
  const SELF_BONUS = 0.75;
  const PARENT_BONUS = 0.20;
  const GRANDPARENT_BONUS = 0.05;
}

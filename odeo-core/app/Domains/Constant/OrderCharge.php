<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 4:15 PM
 */

namespace Odeo\Domains\Constant;


class OrderCharge {

  const UNIQUE_CODE = 'unique_code';
  const VOUCHER = 'voucher';
  const OCASH = 'ocash';
  const PAYMENT_SERVICE_COST = 'payment_service_cost';
  const DOKU_FEE = 'doku_fee';
  const PUNDI_FEE = 'pundi_fee';
  const BILLER_FEE = 'biller_fee';
  const REFERRAL_CASHBACK = 'referral_cashback';
  const AFFILIATE_FEE = 'affiliate_fee';
  const API_FEE = 'api_fee';

  const GROUP_TYPE_CHARGE_TO_CUSTOMER = 'charge_to_customer';
  const GROUP_TYPE_CHARGE_TO_COMPANY = 'charge_to_company';
  const GROUP_TYPE_COMPANY_PROFIT = 'company_profit';

}
<?php

namespace Odeo\Domains\Constant;

use Telegram\Bot\Api;

class InlineTelegram {

  const CORE_TOKEN = '466237480:AAEP1S7hfv5gWBrKHRw1mAoKNv7XimX_lQM';
  const CHANNEL = '@odeo_id';

  const KEYBOARD_DEFAULT = [
    ['saldo', 'harga.tsel', 'harga.isat'],
    ['harga.xl', 'harga.tri', 'harga.axis'],
    ['harga.smartfren', 'harga.bolt', 'harga.pln']
  ];

  const REDIS_IGNORED_CHAT_ID_KEY = 'telegram_ignored_chat_id';

  public static function setClient($token = '') {
    if ($token == '') $token = self::CORE_TOKEN;
    return new Api($token, true);
  }

  public static function getCommandDescriptions() {
    $descriptions = Inline::COMMAND_DESCRIPTIONS;
    return [
      $descriptions[Inline::CMD_SALDO],
      $descriptions[Inline::CMD_TIKET],
      $descriptions[Inline::CMD_HAPUS_TIKET],
      $descriptions[Inline::CMD_CEK_HARGA],
      $descriptions[Inline::CMD_BELI],
      $descriptions[Inline::CMD_CEK_ORDER]
    ];
  }

  public static function getParserPath($cmd) {
    switch($cmd) {
      case Inline::CMD_HELP:
      case Inline::CMD_HI:
      case Inline::CMD_START:
        return \Odeo\Domains\Subscription\Vendor\Telegram\HelpLauncher::class;
      case Inline::CMD_SALDO:
        return \Odeo\Domains\Transaction\Vendor\Telegram\CashSelector::class;
      case Inline::CMD_TIKET:
        return \Odeo\Domains\Order\Vendor\Telegram\OrderTopupRequester::class;
      case Inline::CMD_HAPUS_TIKET:
        return \Odeo\Domains\Order\Vendor\Telegram\OrderTopupCanceller::class;
      case Inline::CMD_CEK_HARGA:
        return \Odeo\Domains\Inventory\Helper\Vendor\Odeo\TelegramPulsaSearcher::class;
      case Inline::CMD_BELI:
        return \Odeo\Domains\Order\Vendor\Telegram\OrderPurchaser::class;
      case Inline::CMD_CEK_ORDER:
        return \Odeo\Domains\Order\Vendor\Telegram\OrderSelector::class;
      default:
        return \Odeo\Domains\Order\Vendor\Telegram\OrderPurchaser::class;
    }
  }
}

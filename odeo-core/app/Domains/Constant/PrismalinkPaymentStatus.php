<?php

namespace Odeo\Domains\Constant;

use Odeo\Domains\Constant\PaymentGateway;

class PrismalinkPaymentStatus {

  const UNKNOWN = 0;
	const PENDING = 10000;
	const ACCEPTED = 30000;
	const SUCCESS = 50000;
	const SUSPECT = 80000;
	const SUSPECT_NOT_FOUND_ON_PRISMALINK = 80001;
	const SUSPECT_REJECTED_BY_MERCHANT = 80002;
	const SUSPECT_TIMEOUT = 80003;
	const SUSPECT_NEED_STATUS_RECHECK = 80004;
	const SUSPECT_IS_CANCELLED_BUT_NEED_STATUS_RECHECK = 80005;
	const SUSPECT_FOR_TESTING = 89000;
	const FAILED = 90000;
  const FAILED_PAYMENT_IS_CANCELLED = 90001;
  
  public static function getPaymentGatewayStatus($status) {
    $map = [
      self::SUCCESS => PaymentGateway::SUCCESS,
      self::FAILED => PaymentGateway::FAILED,
      self::SUSPECT => PaymentGateway::SUSPECT,
      self::SUSPECT_REJECTED_BY_MERCHANT => PaymentGateway::SUSPECT,
      self::SUSPECT_TIMEOUT => PaymentGateway::SUSPECT,
      self::SUSPECT_FOR_TESTING => PaymentGateway::SUSPECT,
      self::FAILED_PAYMENT_IS_CANCELLED => PaymentGateway::FAILED
    ];
    return $map[$status];
  }
  
}

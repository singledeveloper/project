<?php


namespace Odeo\Domains\Constant;


class EdcTransactionStatus {

  const ON_PROGRESS = '30000';
  const COMPLETED = '50000';

}
<?php

namespace Odeo\Domains\Constant;

class BillerIndosmart {

  const PROD_SERVER = 'http://202.169.46.82:7001/connector/h2hgateway';
  const DEV_SERVER = 'http://125.160.179.235:7003/connector/h2hgateway';
  const PROD_TERMINAL_ID = 89070001;
  const DEV_TERMINAL_ID = 17001009;
  const PROD_PIN = "567789";
  const DEV_PIN = "571675";
  const MERCHANT_TYPE = 6012;
  const MERCHANT_ID = 8017;
  const PROCESSING_CODE = 700000;

  public static function getServer() {
    return app()->environment() == "production" ? self::PROD_SERVER : self::DEV_SERVER;
  }

  public static function getTerminalId() {
    return app()->environment() == "production" ? self::PROD_TERMINAL_ID : self::DEV_TERMINAL_ID;
  }

  public static function getPin() {
    return app()->environment() == "production" ? self::PROD_PIN : self::DEV_PIN;
  }
  
  public static function formatXML($name, $data) {
    $xml = '';
    foreach ($data as $key => $item) {
      $xml .= '<member><name>' . $key . '</name><value>' . $item . '</value></member>';
    }
    return '<?xml version="1.0"?><methodCall><methodName>' . $name . '</methodName><params><param><value><struct>' . $xml . '</struct></value></param></params></methodCall>';
  }
  
  public static function translateResponse($code) {
    $messages = [
      "0" => "SUCCESS. Transaction is success",
      "00" => "SUCCESS. Transaction is success",
      "10" => "Message Invalid. Should be fixed in development phase",
      "11" => "Unrecognized method in request. Should be fixed in development phase",
      "13" => "Invalid Amount. Payment failed since amount is not valid",
      "30" => "Unknown Dealer ID",
      "31" => "Unknown Bank Code",
      "33" => "Unknown Terminal ID",
      "34" => "Invalid/Unknown User",
      "35" => "Invalid Status",
      "40" => "Sign In Error",
      "41" => "Payment Failed",
      "42" => "Unknown Customer ID",
      "43" => "Unknown Payment Service",
      "44" => "Invalid Customer Status",
      "46" => "Reconcile Error",
      "47" => "Payment Pending",
      "48" => "Already Paid",
      "50" => "Invalid transaction ID",
      "51" => "Invalid transaction date time",
      "55" => "Not enough deposit for dealer",
      "56" => "Not enough deposit for terminal",
      "59" => "Transaction rejected",
      "60" => "Transaction reversal",
      "61" => "Request in process",
      "62" => "Reversal denied, top-up is success",
      "63" => "Reversal denied, no transaction request",
      "67" => "Duplicate Transaction",
      "68" => "Transaction timeout",
      "69" => "Reversal Timeout",
      "70" => "Voucher out of stock",
      "71" => "Voucher/Token Already used",
      "72" => "Voucher Unavailable",
      "77" => "Not Prepaid MSISDN. Contact Operator",
      "78" => "Not Postpaid MSISDN. Contact Operator",
      "79" => "Blocked MSISDN. Contact Operator",
      "80" => "Invalid MSISDN. Contact Operator",
      "81" => "MSISDN expired. Contact Operator",
      "90" => "System Time-out. Contact iZone Helpdesk",
      "91" => "Internal System Error. Contact iZone Helpdesk",
      "92" => "Unable to route transaction. Contact iZone Helpdesk",
      "97" => "System Under Maintenance. Contact iZone Helpdesk",
      "99" => "Undefined Error Code"
    ];
    
    if (isset($messages[$code])) {
      if ($code == "00") $status = SwitcherConfig::BILLER_SUCCESS;
      else if ($code == "61" || $code == "62") $status = SwitcherConfig::BILLER_IN_QUEUE;
      else $status = SwitcherConfig::BILLER_FAIL;
      
      return [$status, $messages[$code]];
    }
    
    return [SwitcherConfig::BILLER_FAIL, ""];
  }

}
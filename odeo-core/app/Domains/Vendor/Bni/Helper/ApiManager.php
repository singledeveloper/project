<?php

namespace Odeo\Domains\Vendor\Bni\Helper;

use Illuminate\Support\Facades\Redis;

class ApiManager {

  const TIMEOUT_SEC = 120;

  private $redis;

  static $CONFIG = [
    'production' => [
      'base_url' => 'https://api.bni.co.id:443',
      'client_id' => 'IDBNIT0RFTw==',
      'account_number' => '445383134',
      'user' => null,
      'pwd' => null
    ],
    'staging' => [
      'base_url' => 'https://apidev.bni.co.id:8065',
      'client_id' => "IDBNIT0RFTw==",
      'account_number' => '0115476151',
      'user' => '39591561-8527-4ca6-9366-bc32ef20500e',
      'pwd' => '548937f2-0003-4851-9ea7-c1f210885e71'
    ],
  ];

  public static function init() {
    self::$CONFIG['production']['user'] = env('API_BNI_USER');
    self::$CONFIG['production']['pwd'] = env('API_BNI_PWD');
  }

  public function __construct() {
    $this->redis = Redis::connection();
  }

  public static function getConfig($name) {
    $env = app()->environment();

    if (is_array($name)) {
      return array_map(function ($n) use ($env) {
        return self::$CONFIG[$env][$n];
      }, $name);
    }

    return self::$CONFIG[$env][$name];
  }

  private function getAccessToken($timeout = self::TIMEOUT_SEC) {

    $redisCredentialKeyName = 'odeo_core:bni_disbursement_access_token';

    if ($token = $this->redis->get($redisCredentialKeyName)) {
      return $token;
    }

    list($baseUrl, $user, $pwd) = self::getConfig([
      'base_url', 'user', 'pwd'
    ]);

    $url = $baseUrl . "/api/oauth/token";

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($curl, CURLOPT_USERPWD, $user . ':' . $pwd);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

    $curlResponse = curl_exec($curl);
    $response = json_decode($curlResponse);

    if (!$response || !$response->access_token) {
      $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      clog('bni-disbursement', 'Get Access Token: ' . $statusCode . ' - ' . json_encode($response));
      return null;
    }

    $token = $response->access_token;
    $ex = $response->expires_in;

    $this->redis->setex(
      $redisCredentialKeyName,
      $ex - 60 * 5,
      $token
    );

    return $token;

  }

  private function getSignature($data) {
    $pKeyId = openssl_pkey_get_private("file://" . storage_path("app/bnih2hp12/private.key"));
    openssl_sign($data, $signature, $pKeyId, "sha256WithRSAEncryption");
    openssl_free_key($pKeyId);
    return base64_encode($signature);
  }

  private function request($url, $data = [], $timeout = self::TIMEOUT_SEC) {
    $token = $this->getAccessToken($timeout);

    if (!$token) {
      return null;
    }

    $url = self::getConfig('base_url') . $url . "?access_token=" . $token;

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      "Authorization: Bearer " . $token
    ]);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen(json_encode($data))
    ));
    $curlResponse = curl_exec($curl);
    $response = json_decode($curlResponse);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($statusCode != 200) {
      clog('bni-disbursement', "statusCode=$statusCode url=$url response=" . json_encode($response));
    }

    curl_close($curl);

    return $response;
  }

  public function generateCustomerRefAndValueDate() {
    $parts = explode(".", round(microtime(true), 3));
    if (count($parts) == 1) {
      $parts[] = '000';
    }

    list($sec, $microSec) = $parts;
    $valueDate = date("YmdHis", $sec) . $microSec;
    $customerRef = $valueDate . sprintf("%03d", rand(0, 999));

    $customerRef = substr($customerRef, 0, 19);

    return [$customerRef, $valueDate];
  }


  public function getBalance($accountNumber) {
    $clientId = self::getConfig('client_id');

    $dataSignature = $clientId . $accountNumber;
    $signature = $this->getSignature($dataSignature);

    $response = $this->request('/H2H/getbalance', [
      "clientId" => $clientId,
      "signature" => $signature,
      "accountNo" => $accountNumber,
    ]);

    if ($response && $response->getBalanceResponse && $response->getBalanceResponse->parameters) {
      return $response
        ->getBalanceResponse
        ->parameters;
    }

    return null;

  }

  public function getInHouseInquiry($accountNumber, $timeout = self::TIMEOUT_SEC) {

    $clientId = self::getConfig('client_id');

    $dataSignature = $clientId . $accountNumber;
    $signature = $this->getSignature($dataSignature);

    $response = $this->request('/H2H/getinhouseinquiry', [
      "clientId" => $clientId,
      "signature" => $signature,
      "accountNo" => $accountNumber,
    ], $timeout);

    if (!$response || !isset($response->getInHouseInquiryResponse)) {
      clog('bni-disbursement', json_encode($response));
      return null;
    }

    $result = $response
      ->getInHouseInquiryResponse
      ->parameters;

    if ($result->responseCode != '0110' && $result->responseCode != '0001') {
      clog('bni-disbursement', "unknown inHouseInquiry responseCode json=" . json_encode($result));
    }
    else {
      clog('bni-disbursement-inquiry', json_encode($result));
    }

    return $result;
  }

  public function getInterBankInquiry($refNumber, $bankCode, $destinationAccountNumber, $timeout = self::TIMEOUT_SEC) {
    $clientId = self::getConfig('client_id');
    $sourceAccountNumber = self::getConfig('account_number');

    $dataSignature = $clientId . $bankCode . $destinationAccountNumber . $sourceAccountNumber;
    $signature = $this->getSignature($dataSignature);

    $response = $this->request('/H2H/getinterbankinquiry', [
      "clientId" => $clientId,
      "signature" => $signature,
      "customerReferenceNumber" => $refNumber,
      "accountNum" => $sourceAccountNumber,
      'destinationBankCode' => $bankCode,
      'destinationAccountNum' => $destinationAccountNumber,
    ], $timeout);

    if (!$response || !isset($response->getInterbankInquiryResponse)) {
      clog('bni-disbursement', json_encode($response));
      return null;
    }

    $result = $response
      ->getInterbankInquiryResponse
      ->parameters;

    if ($result->responseCode != '0110' && $result->responseCode != '0001') {
      clog('bni-disbursement', "unknown interBankInquiry responseCode json=" . json_encode($result));
    }
    else {
      clog('bni-disbursement-interbank-inquiry', json_encode($result));
    }

    return $result;
  }

  public function getPaymentStatus($customerReferenceNumber) {

    $clientId = self::getConfig('client_id');

    $dataSignature = $clientId . $customerReferenceNumber;
    $signature = $this->getSignature($dataSignature);

    $response = $this->request('/H2H/getpaymentstatus', [
      "clientId" => $clientId,
      "signature" => $signature,
      "customerReferenceNumber" => $customerReferenceNumber,
    ]);

    return $response
      ->getPaymentStatusResponse
      ->parameters;
  }

  public function doPayment($debitAccountNumber, $creditAccountNumber,
                            $valueAmount, $remark, $customerRef, $valueDate) {

    $clientId = self::getConfig('client_id');

    $paymentMethod = "0";
    $valueCurrency = "IDR";

    $dataSignature = $clientId . $customerRef . $paymentMethod . $debitAccountNumber . $creditAccountNumber . $valueAmount . $valueCurrency;
    $signature = $this->getSignature($dataSignature);

    $response = $this->request('/H2H/dopayment', [
      "clientId" => $clientId,
      "signature" => $signature,
      "customerReferenceNumber" => $customerRef,
      "paymentMethod" => $paymentMethod,
      "debitAccountNo" => $debitAccountNumber,
      "creditAccountNo" => $creditAccountNumber,
      "valueDate" => $valueDate,
      "valueCurrency" => $valueCurrency,
      "valueAmount" => $valueAmount,
      "remark" => $remark,
    ]);

    if ($response && !$response->doPaymentResponse) {
      clog('bni-disbursement', "unknown doPayment response json=" . json_encode($response));
    }

    return $response
      ->doPaymentResponse
      ->parameters;
  }
}

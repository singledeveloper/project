<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/3/17
 * Time: 4:37 PM
 */

namespace Odeo\Domains\Vendor\Flip;

use Carbon\Carbon;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\FlipMutationDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Withdrawal\Flip\FlipNotifyReceiver;

class DisbursementNotifier {

  private $flipDisbursements;

  public function __construct() {
    $this->flipDisbursements = app()->make(\Odeo\Domains\Vendor\Flip\Repository\DisbursementFlipDisbursementRepository::class);
  }

  public function getNotify(PipelineListener $listener, $data) {

    //validate token

    if ($data['token'] != \Odeo\Domains\Vendor\Flip\Helper\FlipApi::$TOKEN) {
      return $listener->response(400);
    }

    $result = $data['data'];

    $this->flipDisbursements->lock();

    if (!($flip = $this->flipDisbursements->findByFlipId($result['id']))) {
      return $listener->response(400);
    }

    if (strtoupper($flip->flip_status) != 'PENDING') {
      return $listener->response(400);
    }

    $flip->flip_timestamp = $result['timestamp'];
    $flip->flip_time_served = $result['time_served'];
    $flip->flip_status = $result['status'];
    $flip->flip_evidence = $result['bukti'];
    $flip->flip_company_id = $result['id_perusahaan'];
    $flip->notify_datetime = Carbon::now()->toDateTimeString();
    $flip->flip_city = $result['kota'];

    if ($result['status'] == 'DONE') {
      $flip->status = FlipDisbursement::SUCCESS;
    } else {
      $flip->status = FlipDisbursement::FAIL;

      app()->make(\Odeo\Domains\Vendor\Flip\Helper\FlipBalanceUpdater::class)
        ->updateBalance([
          'amount' => $flip->amount_to_be_transfer,
          'fee' => (function() use ($flip) {
            if ($flip->flip_receiver_bank == 'mandiri') {
              return FlipDisbursement::FEE_LOCAL;
            }
            return FlipDisbursement::FEE_DOMESTIC;
          })(),
          'type' => FlipMutationDisbursement::TYPE_REFUND,
          'disbursement_flip_disbursement_id' => $flip->id,
        ]);

    }

    $this->flipDisbursements->save($flip);

    switch ($flip->disbursement_type) {

      case Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH:
        $listener->addNext(new Task(FlipNotifyReceiver::class, 'receiveNotify', [
          'withdraw_id' => $flip->disbursement_reference_id
        ]));
        break;

    }

    return $listener->response(200);

  }


}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 5:23 PM
 */

namespace Odeo\Domains\Vendor\Flip\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendFlipImbalanceBalanceAlert extends Job  {

  use SerializesModels;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {

    if (app()->environment() != 'production') {
      return;
    }

    Mail::send('emails.flip_balance_imbalance', [
      'data' => $this->data
    ], function ($m) {

      $m->from('noreply@odeo.co.id', 'odeo');

      $m->to('disbursement@odeo.co.id')
        ->subject('Flip imbalance balance - ' . time());
    });


  }

}

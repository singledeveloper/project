<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/5/17
 * Time: 7:44 PM
 */

namespace Odeo\Domains\Vendor\Flip\Helper;


use Odeo\Domains\Constant\VendorDisbursement;

class FlipBalanceUpdater {

  private $vendorDisbursements, $flipMutations;

  public function __construct() {
    $this->vendorDisbursements = app()->make(\Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository::class);
    $this->flipMutations = app()->make(\Odeo\Domains\Vendor\Flip\Repository\DisbursementFlipMutationDisbursementRepository::class);
  }

  public function updateBalance($data) {

    \DB::beginTransaction();

    $this->vendorDisbursements->lock();

    $vendorDisbursement = $this->vendorDisbursements->findById(VendorDisbursement::FLIP);

    $mutation = $this->flipMutations->getNew();

    $mutation->amount = abs($data['amount']);
    $mutation->fee = abs($data['fee']);
    $mutation->balance_before = $vendorDisbursement->balance;

    $vendorDisbursement->balance += ($data['amount'] + $data['fee']);

    $mutation->balance_after = $vendorDisbursement->balance;

    $mutation->disbursement_flip_disbursement_id = $data['disbursement_flip_disbursement_id'] ?? null;

    $mutation->type = $data['type'];

    $this->vendorDisbursements->save($vendorDisbursement);
    $this->flipMutations->save($mutation);

    \DB::commit();


  }


}
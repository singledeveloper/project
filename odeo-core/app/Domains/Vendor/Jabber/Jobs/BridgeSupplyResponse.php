<?php

namespace Odeo\Domains\Vendor\Jabber\Jobs;

use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Supply\Biller\Responder;
use Odeo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class BridgeSupplyResponse extends Job {

  use InteractsWithQueue, SerializesModels;

  protected $data;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(Responder::class, 'parse'));
    $pipeline->execute($this->data);
  }

}

<?php

namespace Odeo\Domains\Vendor\Jabber\Jobs;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\ServerConfig;
use Odeo\Jobs\Job;

class ReplySocket extends Job {
  use InteractsWithQueue;

  protected $socketPath, $function, $args, $reconId, $jabberStore;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($function, $args, $socketPath = '', $reconId = '') {
    parent::__construct();
    $this->runOnlyOnServer('jabber');
    $this->socketPath = $socketPath;
    $this->function = $function;
    $this->args = $args;
    $this->reconId = $reconId;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle() {

    $jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    if ($this->socketPath == '') $this->socketPath = InlineJabber::getDefaultSocket();
    $message = $jabberManager->reply($this->socketPath, $this->function, $this->args);

    if ($this->reconId != '' && $message != '') {
      $reconRepository = app()->make(\Odeo\Domains\Inventory\Helper\Vendor\Odeo\Repository\VendorSwitcherReconciliationRepository::class);
      $recon = $reconRepository->findById($this->reconId);
      $recon->response = $message;
      $reconRepository->save($recon);
    }
  }

}

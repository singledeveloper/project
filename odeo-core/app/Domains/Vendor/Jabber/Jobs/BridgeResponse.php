<?php

namespace Odeo\Domains\Vendor\Jabber\Jobs;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

class BridgeResponse extends Job {
  use InteractsWithQueue, SerializesModels;

  protected $data, $command;

  public function __construct($command, $data) {
    parent::__construct();
    $this->runOnlyOnServer('jabber');
    $this->data = $data;
    $this->command = $command;
  }

  public function handle() {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(InlineJabber::getParserPath($this->command), 'parse'));
    $pipeline->execute($this->data);
  }

}

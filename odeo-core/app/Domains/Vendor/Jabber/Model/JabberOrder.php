<?php

namespace Odeo\Domains\Vendor\Jabber\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Order\Model\Order;

class JabberOrder extends Entity
{
  public function order() {
    return $this->belongsTo(Order::class);
  }

  public function jabberUser() {
    return $this->belongsTo(JabberUser::class);
  }

  public function jabberStore() {
    return $this->belongsTo(JabberStore::class, 'jabber_store_id');
  }
}

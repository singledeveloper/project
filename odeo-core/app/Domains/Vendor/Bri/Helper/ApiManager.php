<?php

namespace Odeo\Domains\Vendor\Bri\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class ApiManager {

  private $redis;

  static $baseUrl = '';  
  static $consumerKey = '';
  static $consumerSecret = '';
  static $accountNumber = '';

  public function __construct() {
    $this->redis = Redis::connection();
  }

  public static function init() {
    self::$baseUrl = env('API_BRI_BASE_URL');
    self::$consumerKey = env('API_BRI_CONSUMER_KEY');
    self::$consumerSecret = env('API_BRI_CONSUMER_SECRET');
    self::$accountNumber = env('API_BRI_ACCOUNT_NUMBER');
  }

  protected function generateTimestamp() {
    $t = microtime(true);
    $miliseconds = sprintf("%03d",($t - floor($t)) * 100);
    $timestamp = Carbon::now('GMT')->format('Y-m-d\TH:i:s\.'.$miliseconds.'\Z');
    return $timestamp;
  } 

  protected function getAccessToken() {
    $redisCredentialKeyName = 'odeo_core:bri_access_token';

    if ($token = $this->redis->get($redisCredentialKeyName)) {
      return $token;
    }

    $url = self::$baseUrl . "/oauth/client_credential/accesstoken?grant_type=client_credentials";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
      "client_id=" . self::$consumerKey . "&client_secret=" . self::$consumerSecret
    );

    $curlResponse = curl_exec($ch);
    $response = json_decode($curlResponse); 
    curl_close($ch);

    $token = $response->access_token;
    $ex = $response->expires_in;

    $this->redis->setex(
      $redisCredentialKeyName,
      $ex - 60 * 5,
      $token
    );

    return $token;
  }

  public function getRequest($path) {
    $url = self::$baseUrl.$path;
    $timestamp = $this->generateTimestamp();
    $token = self::getAccessToken();

    if ($pos = strpos($path, "?") !== false) {
      $path = substr($path, 0, strpos($path, "?"));
    }

    $payload = "path=$path&verb=GET&token=Bearer $token&timestamp=$timestamp&body=";
    $signature = base64_encode(hash_hmac('sha256', $payload, self::$consumerSecret, true));

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Authorization: Bearer $token",
      "BRI-Signature: $signature",
      "BRI-Timestamp: $timestamp"
    ));
    $response = curl_exec($ch);
    curl_close($ch);

    return json_decode($response);
  }

  public function postRequest($path, $body) {
    $url = self::$baseUrl.$path;
    $timestamp = $this->generateTimestamp();
    $token = self::getAccessToken();

    $payload = "path=$path&verb=POST&token=Bearer $token&timestamp=$timestamp&body=$body";
    $signature = base64_encode(hash_hmac('sha256', $payload, self::$consumerSecret, true));
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Authorization: Bearer ".$token,
      "BRI-Signature: $signature",
      "BRI-Timestamp: $timestamp",
      "Content-Type: application/json"
    ));

    $response = curl_exec($ch);
    curl_close($ch);

    return json_decode($response);
  }

  public function getBalance($accountNumber) {
    // $path = "/v1/inquiry/".$accountNumber;
    $path = "/v2/transfer/internal/accounts?sourceaccount=".$accountNumber."&beneficiaryaccount=".$accountNumber;
    return $this->getRequest($path);
  }

  public function getMutation($accountNumber, $startDate, $endDate) {
    $path = "/v1/statement/".$accountNumber."/".$startDate."/".$endDate;
    return $this->getRequest($path);
  }

  public function transfer($senderAccountNumber, $beneficiaryAccountNumber,
                            $amount, $remark, $referralNumber) {
    $path = "/v2/transfer/internal";

    return $this->postRequest($path, json_encode([
      "NoReferral" => $referralNumber,
      "sourceAccount" => $senderAccountNumber,
      "beneficiaryAccount" => $beneficiaryAccountNumber,
      "Amount" => sprintf("%0.2f",$amount),
      "FeeType" => 'OUR',
      "transactionDateTime" => Carbon::now()->format('d-m-Y H:i:s'),
      "remark" => $remark
    ]));
  }

}

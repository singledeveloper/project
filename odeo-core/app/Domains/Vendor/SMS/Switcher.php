<?php

namespace Odeo\Domains\Vendor\SMS;

use Odeo\Domains\Constant\SMS;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Jabber\Jobs\SendErrorReport;

class Switcher {

  private $switcher;

  public function __construct() {
    $this->switcher = app()->make(\Odeo\Domains\Vendor\SMS\Repository\SmsSwitcherRepository::class);
  }

  const PRIOR = [
    //SMS::OBOX_OUTBOUND,
    // SMS::NEXMO,
    SMS::DARTMEDIA,
    SMS::ZENZIVA,
  ];

  public function setVendor($vendorName) {
    switch($vendorName) {
      case SMS::OBOX_OUTBOUND:
        return app()->make(\Odeo\Domains\Vendor\SMS\Obox\Requester::class);
      case SMS::NEXMO:
        return app()->make(\Odeo\Domains\Vendor\SMS\Nexmo\Requester::class);
      case SMS::DARTMEDIA:
        return app()->make(\Odeo\Domains\Vendor\SMS\Dartmedia\Requester::class);
      default:
        return app()->make(\Odeo\Domains\Vendor\SMS\Zenziva\Requester::class);
    }
  }

  public function request(PipelineListener $listener, $data) {
    // If not production, skip the process of sending SMS
    if (!isProduction()) {
      return;
    }

    if ($this->switcher->findSame($data['sms_text'], $data['sms_destination_number'])) {
      dispatch(new SendErrorReport('Possible spam: ' . $data['sms_text'] . ' to: ' . $data['sms_destination_number']));
    }
    else {
      $prior = isset($data['sms_path']) && in_array($data['sms_path'], self::PRIOR) ? $data['sms_path'] : (self::PRIOR)[0];
      $switcher = $this->switcher->getNew();
      $switcher->number = $data['sms_destination_number'];
      $switcher->sms_text = $data['sms_text'];
      $switcher->vendor_query_dumps = $prior;
      $this->switcher->save($switcher);

      $data['sms_switcher_id'] = $switcher->id;

      $this->setVendor($prior)->request($listener, $data);
    }
  }

  public function changeVendor(PipelineListener $listener, $data) {
    $switcher = $this->switcher->findById($data['sms_switcher_id']);
    $temp = explode(',', $switcher->vendor_query_dumps);
    $diff = array_diff(self::PRIOR, $temp);

    if (count($diff) > 0) {
      $key = array_keys($diff)[0];
      $temp[] = $diff[$key];
      $switcher->vendor_query_dumps = implode(',', $temp);
      $this->switcher->save($switcher);

      $this->setVendor($diff[$key])->request($listener, $data);
    }
  }
}
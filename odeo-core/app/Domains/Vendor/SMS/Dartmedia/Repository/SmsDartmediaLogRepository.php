<?php

namespace Odeo\Domains\Vendor\SMS\Dartmedia\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Dartmedia\Model\SmsDartmediaLog;

class SmsDartmediaLogRepository extends Repository {

  public function __construct(SmsDartmediaLog $smsDartmediaLog) {
    $this->model = $smsDartmediaLog;
  }

  public function getBySwitcherIds($switcherIds) {
    return $this->model->whereIn('sms_switcher_id', $switcherIds)->get();
  }

}
<?php

namespace Odeo\Domains\Vendor\SMS\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\SMS\Model\SmsSwitcher;

class SmsSwitcherRepository extends Repository {

  public function __construct(SmsSwitcher $smsSwitcher) {
    $this->model = $smsSwitcher;
  }

  public function findSame($message, $to) {
    return $this->model->where('sms_text', $message)
      ->where('number', $to)->where('created_at', '>', Carbon::now()->subMinutes(5)->toDateTimeString())
      ->first();
  }

  public function get(){
    $filters = $this->getFilters();

    $query = $this->model;

    if (isset($filters['search'])) $filters = $filters['search'];

    if (isset($filters['telephone'])) $query = $query->whereIn('number', [
      $filters['telephone'], purifyTelephone($filters['telephone'])
    ]);

    return $this->getResult($query->orderBy('id', 'desc'));
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/7/17
 * Time: 3:58 PM
 */

namespace Odeo\Domains\Vendor\SMS\Obox;

use Odeo\Domains\Constant\InlineJabber;
use Odeo\Domains\Constant\Obox;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Jabber\Jobs\ReplySocket;
use Odeo\Domains\Vendor\Jabber\Jobs\SendErrorReport;
use Odeo\Domains\Vendor\SMS\Job\ChangeVendor;

class Requester {

  private $outboundLogs, $pulsaPrefix, $jabberManager, $channelLogs, $smsCenters;

  public function __construct() {
    $this->outboundLogs = app()->make(\Odeo\Domains\Vendor\SMS\Obox\Repository\SmsOutboundLogRepository::class);
    $this->pulsaPrefix = app()->make(\Odeo\Domains\Inventory\Helper\Service\Pulsa\Repository\PulsaPrefixRepository::class);
    $this->jabberManager = app()->make(\Odeo\Domains\Vendor\Jabber\JabberManager::class);
    $this->channelLogs = app()->make(\Odeo\Domains\Affiliate\Repository\ChannelLogRepository::class);
    $this->smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
  }

  public function request(PipelineListener $listener, $data) {
    $channelLog = $this->channelLogs->findLastByFromAndType('jabber_alert', InlineJabber::TYPE_ERROR_REPORT);

    if (time() - strtotime($channelLog->updated_at) > 600 && $pulsaPrefix = $this->pulsaPrefix->getNominal(['prefix' => substr(purifyTelephone($data['sms_destination_number']), 0, 5)])) {
      $destination = '';
      if ($pulsaPrefix->operator_id == Pulsa::OPERATOR_INDOSAT) $destination = Obox::SENDER;

      if ($destination != '') {
        if ($smsCenter = $this->smsCenters->findByActiveJabberAccount($destination)) {
          if ($smsCenter->is_active) {
            $lastOutbound = $this->outboundLogs->findLastId();
            if (!$lastOutbound || ($lastOutbound && time() - strtotime($lastOutbound->updated_at) > 180)) {
              if (!$lastOutbound || ($lastOutbound && $lastOutbound->log_response != null)) {
                try {
                  foreach (explode(';', trans('user.sms_random_pattern')) as $item) {
                    list($pattern, $replacementString) = explode('-', $item);
                    $replacements = explode('|', $replacementString);
                    $data['sms_text'] = str_replace($pattern, $replacements[rand('0', count($replacements) - 1)], $data['sms_text']);
                  }
                } catch (\Exception $e) {
                }

                $outbound = $this->outboundLogs->getNew();
                $outbound->sms_switcher_id = $data['sms_switcher_id'];
                $outbound->sms_center_id = $smsCenter->id;
                $outbound->number = $data['sms_destination_number'];
                $outbound->sms_text = $data['sms_text'];
                $this->outboundLogs->save($outbound);

                $messageLength = strlen($data['sms_text']);
                if ($messageLength > 160) {
                  $count = ceil($messageLength / 155);
                  $messagePart = str_split($data['sms_text'], 155);

                  for ($i = 1; $i <= $count; $i++) {
                    dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
                      'to' => $destination,
                      'message' => '[' . $data['sms_destination_number'] . '][(' . $i . '/' . $count . ')' . $messagePart[$i-1] . ']'
                    ]));
                  }
                } else {
                  dispatch(new ReplySocket(InlineJabber::JAXL_CHAT, [
                    'to' => $destination,
                    'message' => '[' . $data['sms_destination_number'] . '][' . $data['sms_text'] . ']'
                  ]));
                }

                return $listener->response(200);
              } else {
                dispatch(new SendErrorReport("Seems you don't give me the last response, " .
                  "please check the following text: " . $lastOutbound->sms_text . ", to: " . $lastOutbound->number .
                  " at " . $lastOutbound->created_at->format('d/m/Y H:i:s'), true));
              }
            }
          }
        }
        // else dispatch(new SendErrorReport("I didn't get any ping from " . $destination . " for the last 5 minutes. Or maybe your account is in error state? Please check.", true));
      }
    }

    $listener->pushQueue(new ChangeVendor([
      'sms_destination_number' => $data['sms_destination_number'],
      'sms_text' => $data['sms_text'],
      'sms_switcher_id' => $data['sms_switcher_id']
    ]));
    return $listener->response(200);

  }

  public function bypassOutbound(PipelineListener $listener, $data) {
    $lastOutbound = $this->outboundLogs->findLastId();
    $clientIP = getClientIP();

    if ($lastOutbound->log_response == null) {
      $lastOutbound->log_response = 'Bypassed at ' . date('Y-m-d H:i:s') . ' from ' . $clientIP;
      $this->outboundLogs->save($lastOutbound);
    }

    return $listener->response(200);
  }
}

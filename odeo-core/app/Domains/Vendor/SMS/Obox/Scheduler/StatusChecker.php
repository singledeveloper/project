<?php

namespace Odeo\Domains\Vendor\SMS\Obox\Scheduler;

use Odeo\Domains\Vendor\Jabber\Jobs\SendErrorReport;

class StatusChecker {

  private $smsCenters;

  public function __construct() {
    $this->smsCenters = app()->make(\Odeo\Domains\Vendor\SMS\Center\Repository\SmsCenterRepository::class);
  }

  public function run() {

    $lastPingWarnings = [];

    foreach ($this->smsCenters->getActive() as $item) {
      $downtime = time() - strtotime($item->last_pinged_at);
      if ($item->jabber_account != null && $downtime > 300) {
        $lastPingWarnings[] = $item->jabber_account . ' since ' . $item->last_pinged_at;
        if ($downtime > 3600) {
          $item->is_active = false;
          $this->smsCenters->save($item);
        }
      }
    }

    if (count($lastPingWarnings) > 0) dispatch(new SendErrorReport("I didn't get any ping from " . implode(' and ', $lastPingWarnings) . ". Please check.", true));
  }
}
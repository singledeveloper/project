<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/7/17
 * Time: 4:18 PM
 */

namespace Odeo\Domains\Vendor\SMS\Nexmo;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\SMS\Job\ChangeVendor;
use Odeo\Domains\Vendor\SMS\Nexmo\Job\SendNexmoFailAlert;

class Notifier {

  private $nexmoSmsLogs;

  public function __construct() {
    $this->nexmoSmsLogs = app()->make(\Odeo\Domains\Vendor\SMS\Nexmo\Repository\SmsNexmoLogRepository::class);
  }

  public function notify(PipelineListener $listener, $data) {
    
    if (!isset($data['messageId'])) clog('nexmo_error', 'NOTIFY UNKNOWN: ' . json_encode($data));
    else {
      $log = $this->nexmoSmsLogs->findByMessageId($data['messageId']);

      if (!$log) return $listener->response(200);

      if ($data['status'] != 'delivered' && $data['status'] != 'accepted' && ($data['status'] != 'expired' && $log->network_code != '51010')) {
        dispatch(new SendNexmoFailAlert([
          'sms_text' => $log->sms_text,
          'sms_destination_number' => $log->to,
          'status' => $data['status'] ?? null,
          'sms_sender_number' => $log->from,
          'message_id' => $log->message_id ?? '-',
        ]));
        if ($log->request_status == '0') {
          $listener->pushQueue(new ChangeVendor([
            'sms_destination_number' => $log->to,
            'sms_text' => $log->sms_text,
            'sms_switcher_id' => $log->sms_switcher_id
          ]));
        }
      }

      $log->msisdn = $data['msisdn'] ?? null;
      $log->response_status = $data['status'] ?? null;
      $log->scts = $data['scts'] ?? null;
      $log->err_code = $data['err-code'] ?? null;
      $log->message_timestamp = $data['message-timestamp'] ?? null;

      $this->nexmoSmsLogs->save($log);
    }

    return $listener->response(200);
  }

}
<?php

namespace  Odeo\Domains\Vendor\SMS\Center;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Vendor\SMS\CenterManager;
use Odeo\Domains\Vendor\SMS\Registrar;

class PasswordUpdateParser extends CenterManager {

  public function __construct() {
    parent::__construct();
  }

  public function parse(PipelineListener $listener, $data) {

    list($command, $newPin, $pin) = explode('.', $data['sms_message']);

    $data['sms_message'] = $command . '.XXXXXX.' . $pin;
    $this->saveHistory($data);

    $pipeline = new Pipeline();
    $pipeline->add(new Task(Registrar::class, 'changeTransactionPinByTelephone'));
    $pipeline->enableTransaction();
    $pipeline->execute([
      'telephone' => $data['sms_to'],
      'new_transaction_pin' => $newPin,
      'bypass_check_old' => true
    ]);

    if ($pipeline->fail()) $this->reply($pipeline->errorMessage, $data['sms_to']);
    else $this->reply('Pin transaksi ODEO berhasil diubah.', $data['sms_to']);

    return $listener->response(200);
  }

}
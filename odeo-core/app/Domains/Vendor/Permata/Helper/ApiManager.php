<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 29/11/18
 * Time: 15.44
 */

namespace Odeo\Domains\Vendor\Permata\Helper;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class ApiManager {

  static $groupId = '';
  static $apiKey = '';
  static $clientId = '';
  static $clientSecret = '';
  static $clientStaticKey = '';
  static $odeoAccountNo = '';
  static $tokenAuthUrl = '';
  static $balanceInquiryUrl = '';
  static $inquiryOverbookingUrl = '';
  static $overbookingUrl = '';
  static $transactionStatusUrl = '';
  static $accountStatementUrl = '';

  const GRANT_CLIENT_CREDENTIALS = 'client_credentials';
  const PERMATA_TOKEN = 'odeo-core:permata_access_token';
  const ODEO_ACCOUNT_NAME = 'ODEO';
  /**
   * @var Redis
   */
  private $redis;

  /**
   * @var Client
   */
  private $client;

  function __construct() {
    $this->redis = Redis::connection();
    $this->client = new Client([
      'curl' => [
        CURLOPT_RESOLVE => [
          'api.permatabank.com:443:202.191.2.100'
        ],
      ],
      'timeout' => 100,
    ]);
  }

  public static function init() {
    self::$groupId = env("API_PERMATA_GROUP_ID");
    self::$apiKey = env("API_PERMATA_API_KEY");
    self::$clientId = env('API_PERMATA_CLIENT_ID');
    self::$clientSecret = env('API_PERMATA_CLIENT_SECRET');
    self::$clientStaticKey = env('API_PERMATA_CLIENT_STATIC_KEY');
    self::$odeoAccountNo = env('API_PERMATA_ODEO_ACCOUNT_NO');
    self::$tokenAuthUrl = env('API_PERMATA_BASE_URL') . 'oauth/token';
    self::$balanceInquiryUrl = env('API_PERMATA_BASE_URL') . 'InquiryServices/BalanceInfo/inq';
    self::$inquiryOverbookingUrl = env('API_PERMATA_BASE_URL') . 'InquiryServices/AccountInfo/inq';
    self::$overbookingUrl = env('API_PERMATA_BASE_URL') . 'BankingServices/FundsTransfer/add';
    self::$transactionStatusUrl = env('API_PERMATA_BASE_URL') . 'InquiryServices/StatusTransaction/Service/inq';
    self::$accountStatementUrl = env('API_PERMATA_BASE_URL') . 'AccountStatement/inq';
  }

  private function getAccessToken() {
    if ($token = $this->redis->get(self::PERMATA_TOKEN)) {
      return $token;
    }

    $authorization = base64_encode(join(':', [self::$clientId, self::$clientSecret]));
    $timestamp = $this->getTimestamp();
    $signature = $this->generateSignature(self::$apiKey, $timestamp, 'grant_type=' . self::GRANT_CLIENT_CREDENTIALS);
    $res = $this->client->request('POST', self::$tokenAuthUrl, [
      'headers' => [
        'OAUTH-Timestamp' => $timestamp,
        'OAUTH-Signature' => $signature,
        'Authorization' => "Basic {$authorization}",
        'API-Key' => self::$apiKey
      ],
      'form_params' => [
        'grant_type' => self::GRANT_CLIENT_CREDENTIALS
      ],
    ]);

    $res = json_decode($res->getBody()->getContents());
    $this->redis->setex(self::PERMATA_TOKEN, $res->expires_in, $res->access_token);
    return $res->access_token;
  }

  public function generateSignature($key, $timestamp, $bodyMessage) {
    $message = join(':', [
      $key,
      $timestamp,
      str_replace(' ', '', $bodyMessage)
    ]);
    return base64_encode(hash_hmac('sha256', $message, self::$clientStaticKey, true));
  }

  public function getTimestamp() {
    $now = Carbon::now();
    $timestamp = "{$now->format('Y-m-d')}T{$now->format('H:i:s.000P')}";
    return $timestamp;
  }

  public function getRequestHeader($timestamp, $body) {
    $token = $this->getAccessToken();
    return [
      "Content-Type" => "application/json",
      "Authorization" => "Bearer $token",
      "organizationname" => self::$groupId,
      "permata-timestamp" => $timestamp,
      "permata-signature" => $this->generateSignature($token, $timestamp, json_encode($body))
    ];
  }

  public function balanceInquiry() {
    $timestamp = $this->getTimestamp();
    $body = [
      "BalInqRq" => [
        "MsgRqHdr" => [
          "RequestTimestamp" => $timestamp,
          "CustRefID" => time()
        ],
        "InqInfo" => [
          "AccountNumber" => self::$odeoAccountNo
        ]
      ]
    ];

    $res = $this->client->request('POST', self::$balanceInquiryUrl, [
      'headers' => $this->getRequestHeader($timestamp, $body),
      'json' => $body
    ]);

    $content = $res->getBody()->getContents();

    clog('permata_pat', 'balance inquiry');
    clog('permata_pat', $content);

    return json_decode($content, true);
  }

  public function overbookingInquiry($accountNumber) {
    $timestamp = $this->getTimestamp();
    $body = [
      'AcctInqRq' => [
        'MsgRqHdr' => [
          'RequestTimestamp' => $timestamp,
          'CustRefID' => time()
        ],
        'InqInfo' => [
          'AccountNumber' => $accountNumber
        ]
      ]
    ];

    $res = $this->client->request('POST', self::$inquiryOverbookingUrl, [
      'headers' => $this->getRequestHeader($timestamp, $body),
      'json' => $body,
    ]);

    $content = $res->getBody()->getContents();

    clog('permata_pat', 'overbooking inquiry');
    clog('permata_pat', $content);
    return json_decode($content, true);
  }

  public function overbooking($data) {
    $body = [
      'XferAddRq' => [
        'MsgRqHdr' => [
          'RequestTimestamp' => $data['request_timestamp'],
          'CustRefID' => "{$data['cust_ref_id']}",
        ],
        'XferInfo' => [
          'FromAccount' => self::$odeoAccountNo, // odeo account
          'ToAccount' => $data['to_account_no'], // cust account
          'Amount' => intval($data['amount']),
          'CurrencyCode' => 'IDR',
          'ChargeTo' => '0',
          'TrxDesc' => $data['desc'],
          'TrxDesc2' => $data['desc2'],
          'BenefAcctName' => $data['to_account_name'],
          'BenefPhoneNo' => '0811111111',
          'FromAcctName' => self::ODEO_ACCOUNT_NAME,
          'TkiFlag' => 'N'
        ]
      ]
    ];

    $res = $this->client->request('POST', self::$overbookingUrl, [
      'headers' => $this->getRequestHeader($data['request_timestamp'], $body),
      'json' => $body
    ]);

    $content = $res->getBody()->getContents();

    clog('permata_pat', 'overbooking');
    clog('permata_pat', $content);

    return json_decode($content, true);
  }

  public function inquiryTransactionStatus($reffId, $requestTimestamp) {
    $body = [
      'StatusTransactionRq' => [
        'CorpID' => self::$groupId,
        'CustRefID' => $reffId,
      ]
    ];

    $res = $this->client->request('POST', self::$transactionStatusUrl, [
      'headers' => $this->getRequestHeader($requestTimestamp, $body),
      'json' => $body
    ]);

    return [
      $body,
      json_decode($res->getBody()->getContents(), true)
    ];
  }

  public function accountStatement($startDate, $endDate, $sequenceNum = '') {
    $timestamp = $this->getTimestamp();
    $body = [
      'AcctStmtRq' => [
        'MsgRqHdr' => [
          'RequestTimestamp' => $timestamp,
          'CustRefID' => time() . "",
        ],
        'StatementInfo' => [
          'AccountNumber' => self::$odeoAccountNo,
          'AccountCurrency' => 'IDR',
          'StartDate' => $startDate,
          'EndDate' => $endDate
        ],
      ]];

    if ($sequenceNum != '') {
      $body['AcctStmtRq']['PageCtrl'] = [
        'MoreFlag' => 'Y',
        'SequenceNum' => $sequenceNum
      ];
    }

    $res = $this->client->request('POST', self::$accountStatementUrl, [
      'headers' => $this->getRequestHeader($timestamp, $body),
      'json' => $body
    ]);

    return json_decode($res->getBody()->getContents(), true);
  }

}

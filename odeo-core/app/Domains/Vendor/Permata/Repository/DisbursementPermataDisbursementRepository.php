<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/12/18
 * Time: 12.37
 */

namespace Odeo\Domains\Vendor\Permata\Repository;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Permata\Model\DisbursementPermataDisbursement;

class DisbursementPermataDisbursementRepository extends Repository {

  private $lock = false;

  function __construct(DisbursementPermataDisbursement $permataDisbursement) {
    $this->model = $permataDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function get() {
    $filters = $this->getFilters();
    $q = $this->getCloneModel();

    if (isset($filters['search']) && isset($filters['search']['permata_disbursement_id'])) {
      $q = $q->where('id', $filters['search']['permata_disbursement_id']);
    }

    return $this->getResult($q->orderBy('id', 'desc'));
  }

  public function getDisbursementIdByCustRefs($reffIds, $type = Disbursement::API_DISBURSEMENT) {
    return $this->model->whereIn('cust_reff_id', $reffIds)
      ->where('disbursement_type', '=', $type)
      ->where(function ($query) {
        $query->whereNull('error_log_response')
          ->orWhere('error_log_response', '=', '');
      })
      ->select('id', 'disbursement_reference_id', 'cust_reff_id')
      ->get();
  }

  public function getUnreconciledWithdraws() {
    return $this->model
      ->where('disbursement_type', '=', Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH)
      ->where(function ($query) {
        $query->where('is_reconciled', '=', false)
          ->orWhereNull('is_reconciled');
      })
      ->get();
  }

  public function getSuspectDisbursements() {
    return $this->model
      ->join('api_disbursements', 'api_disbursements.id', '=', 'disbursement_permata_disbursements.disbursement_reference_id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_permata_disbursements.*')
      ->get();
  }

}

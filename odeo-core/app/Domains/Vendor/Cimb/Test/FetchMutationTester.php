<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 10/13/17
 * Time: 1:52 PM
 */

namespace Odeo\Domains\Vendor\Cimb\Test;


use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Cimb\Helper\ApiManager;

class FetchMutationTester {

  public function __construct() {
  }

  public function fetch(PipelineListener $listener, $data) {
    $dom = new \DOMDocument();
    $data = [];
    foreach (['C', 'D'] as $direction) {
      $dateTime = Carbon::now();
      $client = ApiManager::getClient();
      $params = [
        'tokenAuth' => ApiManager::generateAuthToken($dateTime->format('YmdHis'), $dateTime->format('YmdHis'), ApiManager::SERVICE_TRX_INQUIRY),
        'txnData' => ApiManager::generateTransactionInquiryData($direction, $dateTime->subDay()->format('Ymd'), $dateTime->addDay()->format('Ymd')),
        'serviceCode' => ApiManager::SERVICE_TRX_INQUIRY,
        'corpID' => ApiManager::getCorpId(),
        'requestID' => $dateTime->format('YmdHis'),
        'txnRequestDateTime' => $dateTime->format('YmdHis')
      ];
      $response = $client->call(ApiManager::$SOAP, [$params]);

      \Log::info(json_encode($response));
      $dom->loadXML($response['txnData']);

      foreach ($dom->getElementsByTagName('data') as $node) {
        foreach ($node->childNodes as $childNode) {
          if($childNode->nodeName == '#text') continue;
          $temp[$childNode->nodeName] = $childNode->nodeValue;
        }

        $data[] = $temp;

      }
    }
    return $listener->response(200, $data);

  }

}

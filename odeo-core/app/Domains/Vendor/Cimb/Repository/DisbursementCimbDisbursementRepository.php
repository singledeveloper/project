<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 9/20/17
 * Time: 2:48 PM
 */

namespace Odeo\Domains\Vendor\Cimb\Repository;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Cimb\Model\DisbursementCimbDisbursement;

class DisbursementCimbDisbursementRepository extends Repository {

  function __construct(DisbursementCimbDisbursement $disbursementCimbDisbursement) {
    $this->setModel($disbursementCimbDisbursement);
  }

  public function lock() {
    $this->lock = true;
  }

  public function get() {
    $filters = $this->getFilters();

    $query = $this->getCloneModel();
    if (isset($filters['search'])) {
      if (isset($filters['search']['cimb_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['cimb_disbursement_id']);
      }
    }
    return $this->getResult($query->orderBy('id', 'desc'));
  }

  public function getSuspectDisbursements() {
    return $this->model
      ->join('api_disbursements', 'api_disbursements.id', '=', 'disbursement_cimb_disbursements.disbursement_reference_id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_cimb_disbursements.*')
      ->get();
  }
}
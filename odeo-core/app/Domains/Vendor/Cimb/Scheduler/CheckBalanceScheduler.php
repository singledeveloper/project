<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/14/17
 * Time: 5:50 PM
 */

namespace Odeo\Domains\Vendor\Cimb\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\CimbApi;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\Cimb\Helper\ApiManager;

class CheckBalanceScheduler {

  private $vendorDisbursementRepo;

  public function __construct() {
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
  }

  public function run() {
    try {

      $this->process();

    } catch (\Exception $e) {
      
    }
  }

  private function process() {
    $vendor = $this->vendorDisbursementRepo->findById(VendorDisbursement::CIMB);

    $dom = new \DOMDocument();
    $dateTime = Carbon::now();
    $client = ApiManager::getClient();

    list($copId) = ApiManager::getConfig(['cop_id']);

    $requestId = CimbApi::generateSeqUniqTrxId(CimbApi::GET_BALANCE_PREFIX);

    $data = [
      'tokenAuth' => ApiManager::generateAuthToken($dateTime->format('YmdHis'), $requestId, ApiManager::SERVICE_ACCOUNT_BALANCE),
      'txnData' => ApiManager::generateAccountBalanceData(),
      'serviceCode' => ApiManager::SERVICE_ACCOUNT_BALANCE,
      'corpID' => $copId,
      'requestID' => $requestId,
      'txnRequestDateTime' => $dateTime->format('YmdHis')
    ];

    $response = $client->call(ApiManager::$SOAP, [$data]);
    $dom->loadXML($response['txnData']);

    $result = [];

    foreach ($dom->getElementsByTagName('balance') as $node) {
      foreach ($node->childNodes as $childNode) {
        if ($childNode->nodeName == '#text') continue;
        $result[$childNode->nodeName] = $childNode->nodeValue;
      }
    }

    $vendor->balance = $result['availableBalance'];

    $this->vendorDisbursementRepo->save($vendor);
  }

}

<?php

namespace Odeo\Domains\Vendor\Telegram;

use Illuminate\Support\Facades\Hash;
use Odeo\Domains\Constant\Inline;
use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Domains\Constant\UserStatus;

class TelegramManager {

  private $chatId = '', $pin = '', $command = '', $token = '';
  protected $telegramUser, $telegramHistories, $telegramStores, $user, $userChecker, $channelLogs;

  public function __construct() {

    app('translator')->setLocale('id');

    $this->telegramUser = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramUserRepository::class);
    $this->telegramHistories = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramHistoryRepository::class);
    $this->telegramStores = app()->make(\Odeo\Domains\Vendor\Telegram\Repository\TelegramStoreRepository::class);
    $this->user = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->userChecker = app()->make(\Odeo\Domains\Account\Helper\UserChecker::class);
    $this->channelLogs = app()->make(\Odeo\Domains\Affiliate\Repository\ChannelLogRepository::class);
  }

  public function initialize($id, $token) {
    $this->chatId = $id;
    $this->token = $token;
  }

  protected function format($data) {
    $temp = explode('.', $data);
    $command = strtolower($temp[0]);

    $data = Inline::COMMAND_FORMATS;

    if (isset($data[$command]) && count($temp) != $data[$command]) return false;

    if ($command == Inline::CMD_SETHP) $temp[1] = purifyTelephone($temp[1]);
    $this->command = strtolower($temp[0]);
    $this->pin = $temp[count($temp) - 1];

    if (!isset($data[$command]) && strtolower($this->command) == strtolower($this->pin)) return false;

    return $temp;
  }

  protected function getCommand() {
    return $this->command;
  }

  protected function getChatId() {
    return $this->chatId;
  }

  public function reply($message = '', $keyboard = []) {

    $telegram = InlineTelegram::setClient($this->token);

    if (!is_string($message)) $message = 'Kami tidak mengenali perintah Anda. Mohon periksa format Anda dan coba lagi.';
    $replyMessage = [
      'chat_id' => $this->chatId,
      'text' => $message
    ];
    if (!is_array($keyboard)) $replyMessage['reply_markup'] = $telegram->replyKeyboardHide();
    else if (count($keyboard) > 0) {
      $replyMessage['reply_markup']= $telegram->replyKeyboardMarkup([
        'keyboard' => $keyboard,
        'resize_keyboard' => true,
        'one_time_keyboard' => false
      ]);
    }

    try {
      $telegram->setAsyncRequest(false);
      $telegram->sendMessage($replyMessage);
    }
    catch (\Telegram\Bot\Exceptions\TelegramResponseException $e) {
      clog('telegram_error', json_encode($e->getResponseData()));
    }
  }

  public function postChannel($message) {
    if (app()->environment() == 'production') {
      $this->initialize(InlineTelegram::CHANNEL, InlineTelegram::CORE_TOKEN);
      $this->reply('Dear Mitra, ' . $message . '. Mohon dikondisikan.');
    }
    else clog('telegram_data', 'CHANNEL: ' . $message);
  }

  protected function authFromId($telegramUser) {
    if (Inline::commandNoAuth($this->command)) return ['user_id' => null];

    $user = $telegramUser->user;
    if ($telegramUser->pin_try_counts == UserStatus::LOGIN_BLOCKED_COUNT ||
      $user->status != UserStatus::OK || $user->login_counts == UserStatus::LOGIN_BLOCKED_COUNT) {
      $this->reply(trans('errors.account_blocked'));
      return false;
    }
    else if (!Hash::check($this->pin, $telegramUser->pin)) {
      $telegramUser->pin_try_counts = $telegramUser->pin_try_counts + 1;
      $this->telegramUser->save($telegramUser);
      if ($telegramUser->pin_try_counts == UserStatus::LOGIN_BLOCKED_COUNT) {
        $this->reply(trans('errors.account_blocked'));
      }
      else $this->reply(trans('errors.invalid_password', ["count" => 3 - $telegramUser->pin_try_counts]));
      return false;
    }
    else {
      if ($telegramUser->pin_try_counts != 0) {
        $telegramUser->pin_try_counts = 0;
        $this->telegramUser->save($telegramUser);
      }
      return ['user_id' => $telegramUser->id];
    }
  }

  protected function authFromNumber($number) {
    if (Inline::commandNoAuth($this->command)) return ['user_id' => null];
    if (!$user = $this->user->findByTelephone($number)) {
      $this->reply(trans('errors.invalid_telephone'));
      return false;
    }
    list($isValid, $user) = $this->userChecker->checkPin($user, $this->pin);
    if (!$isValid) {
      $this->reply($user);
      return false;
    }
    return ['user_id' => $user->id, 'pin' => $this->pin];
  }

}

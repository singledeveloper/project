<?php

namespace Odeo\Domains\Vendor\Telegram\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Telegram\Model\TelegramOrder;

class TelegramOrderRepository extends Repository {

  public function __construct(TelegramOrder $telegramOrder) {
    $this->model = $telegramOrder;
  }

  public function findByOrderId($orderId) {
    return $this->model->where('order_id', $orderId)->first();
  }

}

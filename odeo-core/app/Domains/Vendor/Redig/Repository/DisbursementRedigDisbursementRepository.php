<?php

namespace Odeo\Domains\Vendor\Redig\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Redig\Model\DisbursementRedigDisbursement;

class DisbursementRedigDisbursementRepository extends Repository {

  public function __construct(DisbursementRedigDisbursement $model) {
    $this->setModel($model);
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['redig_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['redig_disbursement_id']);
      }
    }

    if ($this->hasExpand('bank')) $query = $query->with('bank');
    if ($this->hasExpand('inquiry_request')) $query = $query->with('inquiryRequest');
    if ($this->hasExpand('transfer_request')) $query = $query->with('transferRequest');

    return $this->getResult($query->orderBy('id', 'desc'));
  }

}
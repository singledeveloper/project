<?php

namespace Odeo\Domains\Vendor\Redig\Model;


use Odeo\Domains\Account\Model\Bank;
use Odeo\Domains\Core\Entity;

class DisbursementRedigDisbursement extends Entity {

  public function bank() {
    return $this->belongsTo(Bank::class);
  }

  public function inquiryRequest() {
    return $this->belongsTo(RedigApiRequest::class, 'inquiry_request_id');
  }

  public function transferRequest() {
    return $this->belongsTo(RedigApiRequest::class, 'transfer_request_id');
  }

}

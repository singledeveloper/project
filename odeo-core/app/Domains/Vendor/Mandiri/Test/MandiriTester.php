<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 25/03/19
 * Time: 20.08
 */

namespace Odeo\Domains\Vendor\Mandiri\Test;


use Carbon\Carbon;
use Odeo\Domains\Vendor\Mandiri\Helper\ApiManager;

class MandiriTester {

  /**
   * @var ApiManager
   */
  private $apiManager;

  public function __construct() {
    $this->apiManager = app()->make(ApiManager::class);
  }

  public function paymentTransfer() {
    return $this->apiManager->testPaymentTransfer([
      'debitAcc' => '1150009757057',
      'creditAcc' => '567098234',
      'currency' => 'USD',
      'amount' => 107,
      'bankCode' => 'BRINIDJAXXX',
      'trfMethodId' => 3,
      'benefName' => 'PT Maju Mundur',
      'chargeModelId' => 'OUR',
      'remark1' => 'TEST MCP-P07',
      'reffNo' => time(),
      'reserve1' => '',
      'reserve2' => '',
    ]);

  }

  public function transactionStatus($reffno) {
    return $this->apiManager->getTransactionStatus(
      $reffno
    );
  }

  public function accountInquiry() {
    return $this->apiManager->getAccountInquiry(
      '1210001041502'
    );
  }

  public function balanceInquiry() {
    return $this->apiManager->getBalanceInquiry(1150009757057);
  }

}
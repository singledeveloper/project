<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 28/03/19
 * Time: 16.14
 */

namespace Odeo\Domains\Vendor\Mandiri;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Vendor\Mandiri\Repository\DisbursementMandiriDisbursementRepository;

class TransferRecordSelector implements SelectorListener {

  /**
   * @var DisbursementMandiriDisbursementRepository
   */
  private $mandiriDisbursements;

  function __construct() {
    $this->mandiriDisbursements = app()->make(DisbursementMandiriDisbursementRepository::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {
    $response['mandiri_disbursement_id'] = $item->id;
    $response['amount'] = $item->amount;
    $response['disbursement_type'] = $item->disbursement_type;
    $response['status'] = $item->status;
    $response['to_account_number'] = $item->to_account_number;
    $response['to_account_name'] = $item->to_account_name;
    $response['cust_reff_no'] = $item->cust_reff_no;
    $response['remark'] = $item->remark;
    $response['request_time'] = $item->request_time;
    $response['transaction_key'] = $item->transaction_key;
    $response['response_code'] = $item->response_code;
    $response['response_message'] = $item->response_message;
    $response['response_timestamp'] = $item->response_timestamp;
    $response['response_remmittance_no'] = $item->response_remmittance_no;
    $response['is_sign_verified'] = $item->is_sign_verified;
    $response['response_error_message'] = $item->response_error_message;
    $response['response_error_number'] = $item->response_error_number;
    $response['disbursement_reference_id'] = $item->disbursement_reference_id;
    return $response;
  }

  public function get(PipelineListener $listener, $data) {
    $this->mandiriDisbursements->normalizeFilters($data);
    $this->mandiriDisbursements->setSimplePaginate(true);

    $mandiriRecords = [];
    foreach ($this->mandiriDisbursements->get() as $mandiriDisbursement) {
      $mandiriRecords[] = $this->_transforms($mandiriDisbursement, $this->mandiriDisbursements);
    }

    if (count($mandiriRecords)) {
      return $listener->response(200, array_merge(
        ['mandiri_disbursement_record' => $this->_extends($mandiriRecords, $this->mandiriDisbursements)],
        $this->mandiriDisbursements->getPagination()
      ));
    }
    return $listener->response(204, [
      'mandiri_disbursement_record' => []
    ]);
  }

  public function getSuspectDisbursements(PipelineListener $listener, $data) {
    if ($vendorDisbursements = $this->mandiriDisbursements->getSuspectDisbursements()) {
      return $listener->response(200, ['suspect_disbursement_record' => $vendorDisbursements]);
    }
    return $listener->response(204, ['suspect_disbursement_record' => []]);
  }
}
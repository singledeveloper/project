<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Model\DisbursementArtajasaMutation;

class DisbursementArtajasaMutationRepository extends Repository {
  
  public function __construct(DisbursementArtajasaMutation $disbursementArtajasaMutation) {
    $this->model = $disbursementArtajasaMutation;
  }

}
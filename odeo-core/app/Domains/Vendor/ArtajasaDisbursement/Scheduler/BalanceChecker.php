<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use \Carbon\Carbon;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs\SendArtajasaImbalanceBalanceAlert;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs\SendArtajasaTopupDetectedAlert;

class BalanceChecker {

  private $vendorDisbursement, $redis, $artajasaBalanceUpdater;

  public function __construct() {
    $this->vendorDisbursement = app()->make(VendorDisbursementRepository::class);
    $this->artajasaBalanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
    $this->redis = Redis::connection();
  }

  public function run() {
    $remoteBalance = $this->getBalanceFromBersamaku();
    $recordedBalance = $this->vendorDisbursement->findById(VendorDisbursement::ARTAJASA);
    $recordedBalance = floatval($recordedBalance->balance);
    $diffBalance = $remoteBalance - $recordedBalance;

    if ($this->isTopup($diffBalance)) {
      $topupAmount = intval($diffBalance / 1e6) * 1e6;
      $this->artajasaBalanceUpdater->updateBalance([
        'amount' => $topupAmount,
        'fee' => 0,
        'type' => ArtajasaMutationDisbursement::TYPE_TOPUP,
      ]);

      dispatch(new SendArtajasaTopupDetectedAlert([
        'artajasa_balance' => $remoteBalance,
        'recorded_balance' => $recordedBalance,
        'topup_amount' => $topupAmount,
        'final_balance' => $topupAmount + $recordedBalance,
        'datetime' => Carbon::now()->toDateTimeString()
      ]));

      return;
    }

    if (!$this->shouldAlert($diffBalance)) {
      return;
    }

    dispatch(new SendArtajasaImbalanceBalanceAlert([
      'artajasa_balance' => $remoteBalance,
      'recorded_balance' => $recordedBalance,
      'diff_balance' => $diffBalance,
      'datetime' => Carbon::now()->toDateTimeString()
    ]));
  }

  private function getBalanceFromBersamaku() {
    $url = "https://bersamaku.id/disbursement_mitra/action/loginverification2.php";
    $ua = 'Mozilla/5.0 (Windows NT 5.1; rv:16.0) Gecko/20100101 Firefox/16.0 (ROBOT)';
    $cookieFile = "PHPSESSID=5di88hdpf12bbn840gu84eau77";

    $fields = [
      'loginkey' => env('ARTAJASA_DISBURSEMENT_WEB_REPORT_LOGIN_KEY'),
      'username' => env('ARTAJASA_DISBURSEMENT_WEB_REPORT_USERNAME'),
      'password' => env('ARTAJASA_DISBURSEMENT_WEB_REPORT_PASSWORD'),
      'bt_login' => 'login'
    ];

    $coded = [];
    foreach ($fields as $field => $value) {
      $coded[] = $field . '=' . urlencode($value);
    }

    $string = implode('&', $coded);
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, True);
    curl_setopt($ch, CURLOPT_NOBODY, False);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, True);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
    curl_setopt($ch, CURLOPT_POST, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
    $html = curl_exec($ch);

    $pos = strpos($html, '<b>Rp. ');
    $html = substr($html, $pos + 7);

    $pos = strpos($html, ' </b>');
    $html = substr($html, 0, $pos);

    $html = str_replace(".", "", $html);
    $html = str_replace(",", ".", $html);

    return floatval($html);
  }

  private function isTopup($diffBalance) {
    return $diffBalance >= 50e6 && $diffBalance % 1e6 == 0;
  }

  private function shouldAlert($diffBalance) {
    if ($diffBalance == 0) {
      $this->redis->hset('odeo_core:artajasa', 'balance_imbalance_count', 0);
      return false;
    }

    if (!($imbalanceCount = $this->redis->hget('odeo_core:artajasa', 'balance_imbalance_count'))) {
      $imbalanceCount = 0;
    }

    $imbalanceCount = ($imbalanceCount + 1) % 3;
    $this->redis->hset('odeo_core:artajasa', 'balance_imbalance_count', $imbalanceCount);

    return $imbalanceCount == 0;
  }
}

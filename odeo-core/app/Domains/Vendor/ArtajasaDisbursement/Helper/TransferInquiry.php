<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Helper;

use Carbon\Carbon;
use DB;
use Exception;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\Mock\MockApiManager;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaTransferInquiryRepository;

class TransferInquiry {

  private $transferInquiryRepo, $apiManager, $stanGenerator;

  public function __construct() {
    $this->transferInquiryRepo = app()->make(DisbursementArtajasaTransferInquiryRepository::class);
    $this->stanGenerator = app()->make(ArtajasaStanGenerator::class);

    if (app()->environment() == 'production') {
      $this->apiManager = app()->make(ApiManager::class);
    }
    else {
      $this->apiManager = app()->make(MockApiManager::class);
    }
  }

  public function exec($data) {
    $transferInquiry = $this->initTransferInquiry($data, Carbon::now());
    $this->doTransferInquiry($transferInquiry, $data);
    return $transferInquiry;
  }

  private function initTransferInquiry($data, $date) {
    $inquiry = $this->transferInquiryRepo->getNew();
    $inquiry->transaction_datetime = $date;
    $inquiry->channel_type = $data['channel_type'];
    $inquiry->bank_code = $data['bank_code'];
    $inquiry->transfer_to = $data['transfer_to'];
    $inquiry->transfer_to_name = null;
    $inquiry->amount = $data['amount'];
    $inquiry->purpose_desc = $data['purpose_desc'] ?? null;
    $inquiry->terminal_id = $data['terminal_id'];
    $inquiry->reference_type = $data['reference_type'];
    $inquiry->reference_id = $data['reference_id'];
    $inquiry->error_exception_message = null;
    $inquiry->error_exception_code = null;

    DB::transaction(function () use ($inquiry, $date) {
      $this->transferInquiryRepo->lock();
      $inquiry->transaction_id = $this->stanGenerator->generate($date);

      if (!$this->transferInquiryRepo->save($inquiry)) {
        throw new \Exception('Save inquiry failed');
      }
    });

    clog('AJDisbursement', 'Saved inquiry #' . $inquiry->id);
    return $inquiry;
  }

  private function doTransferInquiry($inquiry, $data) {
    try {
      clog('AJDisbursement', "Before send transfer inquiry #{$inquiry->id} ({$data['reference_id']})");
      $result = $this->apiManager->transferInquiry(
        $inquiry->transaction_id,
        $inquiry->transaction_datetime,
        $data['reference_id'],
        $inquiry->bank_code,
        $data['regency_code'],
        $inquiry->transfer_to,
        $inquiry->amount,
        $inquiry->purpose_desc,
        $inquiry->channel_type,
        $data['user_id'],
        $data['user_name'],
        $data['terminal_id'],
        $data['timeout'] ?? 0,
        $data['sender_prefix']
      );
      clog('AJDisbursement', "Sent transfer inquiry code={$result['Response']['Code']}");

      $inquiry->response_code = $result['Response']['Code'];
      $inquiry->response_description = $result['Response']['Description'];

      if ($result['Response']['Code'] == '00') {
        $inquiry->status = ArtajasaDisbursement::SUCCESS;
        $inquiry->transfer_to_name = trim($result['BeneficiaryData']['Name']);
      }
      else {
        $inquiry->status = ArtajasaDisbursement::FAIL;
      }

      $inquiry->response_datetime = Carbon::now();
      $this->transferInquiryRepo->save($inquiry);

      return $inquiry->status == ArtajasaDisbursement::SUCCESS;
    } catch (Exception $e) {
      clog('AJDisbursement', $e);
      \Log::error($e);

      $inquiry->status = ArtajasaDisbursement::FAIL;
      $inquiry->error_exception_message = $e->getMessage();
      $inquiry->error_exception_code = $e->getCode();
      $this->transferInquiryRepo->save($inquiry);

      return false;
    }
  }
}
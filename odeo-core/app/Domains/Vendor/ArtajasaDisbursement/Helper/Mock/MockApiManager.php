<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\Mock;

use DateTime;

class MockApiManager {

  public function transferInquiry($stan, $date, $refNumber, $bankCode, $regencyCode, $accountId, $amount, $purposeDesc,
                                  $channelType, $userId, $userName, $terminalId, $timeout, $senderPrefix) {
    clog('AJDisbursement', 'Mock transferInquiry: ' . json_encode(func_get_args()));
    switch ($accountId) {
      case '100190001':
        return [
          'Response' => [
            'Code' => '76',
            'Description' => 'Invalid to account',
          ]
        ];
        break;

      case '100190003';
        return [
          'Response' => [
            'Code' => '78',
            'Description' => 'Account is closed',
          ]
        ];
        break;

      case '100190004';
        return [
          'Response' => [
            'Code' => '05',
            'Description' => 'Do not honor',
          ]
        ];
        break;

      case '100190005';
        return [
          'Response' => [
            'Code' => '89',
            'Description' => 'Link to Host down',
          ]
        ];
        break;

      case '100180000';
        return [
          'Response' => [
            'Code' => 'TO',
            'Description' => 'Response time-out from ATM Bersama Network',
          ]
        ];
        break;

      default:
        return [
          'BeneficiaryData' => [
            'Name' => 'Ronathan',
          ],
          'Response' => [
            'Code' => '00',
            'Description' => 'Success'
          ]
        ];
        break;
    }
  }

  public function transfer($stan, $date, $refNumber, $bankCode, $regencyCode, $accountId, $name, $amount, $purposeDesc,
                           $channelType, $userId, $userName, $terminalId, $senderPrefix) {
    clog('AJDisbursement', 'Mock transfer: ' . json_encode(func_get_args()));
    switch ($accountId) {
      case '100190001':
        return [
          'Response' => [
            'Code' => '76',
            'Description' => 'Invalid to account',
          ]
        ];
        break;

      case '100190003';
        return [
          'Response' => [
            'Code' => '78',
            'Description' => 'Account is closed',
          ]
        ];
        break;

      case '100190004';
        return [
          'Response' => [
            'Code' => '05',
            'Description' => 'Do not honor',
          ]
        ];
        break;

      case '100190005';
        return [
          'Response' => [
            'Code' => '89',
            'Description' => 'Link to Host down',
          ]
        ];
        break;

      case '100180000';
        return [
          'Response' => [
            'Code' => 'TO',
            'Description' => 'Response time-out from ATM Bersama Network',
          ]
        ];
        break;

      case '100180001';
        throw new \Exception('cURL error 28');
        break;

      case '100180002';
        return [
          'Response' => [
            'Code' => '68',
            'Description' => 'Response received too late',
          ]
        ];
        break;

      default:
        return [
          'Response' => [
            'Code' => '00',
            'Description' => 'Success',
          ]
        ];
    }
  }

  public function statusInquiry($stan, DateTime $date, $targetStan, DateTime $targetDate) {
    clog('AJDisbursement', 'Mock statusInquiry: ' . json_encode(func_get_args()));
    return [
      'TransferData' => [
        'Response' => [
          'Code' => '00',
          'Description' => 'Success',
        ]
      ],
      'Response' => [
        'Code' => '00',
        'Description' => 'Success'
      ]
    ];
  }
}
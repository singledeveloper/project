<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\InternalRefund;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\Helper\ArtajasaResponseMapper;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Repository\BankScrapeAccountNumberRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;

class InternalRefundRequester {
  private $apiDisbursementRepo, $artajasaDisbursementRepo, $artajasaResponseMapper, $bankRepo, $userRepo, $redis,
    $bankScrapeAccountNumberRepo, $balanceUpdater;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->artajasaResponseMapper = app()->make(ArtajasaResponseMapper::class);
    $this->bankScrapeAccountNumberRepo = app()->make(BankScrapeAccountNumberRepository::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->userRepo = app()->make(UserRepository::class);
    $this->balanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
    $this->redis = Redis::connection();
  }

  public function request(PipelineListener $listener, $data) {
    $userId = InternalRefund::INTERNAL_REFUND_USER_ID;
    $amount = $data['amount'];

    $bankScrapeAccountNumber = $this->bankScrapeAccountNumberRepo->findByID($data['account_number_id']);

    if (!$bankScrapeAccountNumber || !$bankScrapeAccountNumber->number) {
      return $listener->response(400, 'Unknown bank account');
    }

    if (!$bankId = InternalRefund::getBankId($bankScrapeAccountNumber)) {
      return $listener->response(400, 'Unknown bank account');
    }

    if ($data['password'] != env('INTERNAL_REFUND_REQUEST_PASSWORD')) {
      return $listener->response(400, 'Invalid password');
    }

    if ($this->isDuplicate($bankId)) {
      return $listener->response(400, 'Dalam waktu 5 menit terakhir, sudah ada yang transfer ke bank ini');
    }

    if (!$this->lock($bankId)) {
      return $listener->response(400, 'Sedang ada transfer yang terjadi');
    }

    $key = 'odeo_core:internal_refund_id';
    $internalRefundId = $this->redis->incr($key);

    $user = $this->userRepo->findById($userId);
    $bank = $this->bankRepo->findById($bankId);

    $artajasa = $this->artajasaDisbursementRepo->getNew();
    $artajasa->disbursement_type = Disbursement::INTERNAL_REFUND;
    $artajasa->disbursement_reference_id = 0;
    $artajasa->status = ArtajasaDisbursement::PENDING;
    $this->artajasaDisbursementRepo->save($artajasa);

    $terminalId = preg_replace('/\\D/', '', $user->telephone);
    $terminalId = substr($terminalId, strlen($terminalId) - 8, 8);
    $terminalId = empty($terminalId) ? '1' : $terminalId;

    $pipeline = new Pipeline();

    $pipeline->add(new Task(BalanceUpdater::class, 'updateBalance', [
      'type' => ArtajasaMutationDisbursement::TYPE_TRANSFER,
      'amount' => $amount,
      'fee' => ArtajasaDisbursement::COST,
      'disbursement_artajasa_disbursement_id' => $artajasa->id,
    ]));

    $pipeline->add(new Task(Transfer::class, 'exec', [
      'artajasa_disbursement_id' => $artajasa->id,
      'bank_code' => explode('-', $bank->aj_bank_code)[0],
      'regency_code' => $bank->aj_regency_code,
      'transfer_to' => $bankScrapeAccountNumber->number,
      'transfer_to_bank' => $bank->name,
      'amount' => $amount,
      'purpose_desc' => 'Artajasa Internal Refund #' . $internalRefundId,
      'channel_type' => ArtajasaDisbursement::CHANNEL_INTERNET,
      'user_id' => $user->id,
      'user_name' => InternalRefund::INTERNAL_REFUND_USER_NAME,
      'terminal_id' => $terminalId,
      'sender_prefix' => ArtajasaDisbursement::ODEO_SENDER_PREFIX,
    ]));

    $pipeline->execute();

    if ($pipeline->success()) {
      $responseCode = '00';
    }
    else {
      $responseCode = $pipeline->errorMessage['response_code'] ?? null;
    }

    $statusCode = $this->artajasaResponseMapper->map($responseCode);

    switch ($statusCode) {
      case ApiDisbursement::COMPLETED:
        $this->redis->hset('odeo_core:internal_refund', $bankId, time());
        $this->unlock($bankId);
        return $listener->response(200);

      case ApiDisbursement::SUSPECT:
      case ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS:
        $this->unlock($bankId);
        return $listener->response(400, 'Transaksi suspect. Silakan cek transaksi ini terlebih dahulu');

      default:

        $this->balanceUpdater->updateBalance([
          'amount' => $amount,
          'fee' => ArtajasaDisbursement::COST,
          'type' => ArtajasaMutationDisbursement::TYPE_REFUND,
          'disbursement_artajasa_disbursement_id' => $artajasa->id,
        ]);

        $statusMessage = ApiDisbursement::getStatusMessage($statusCode);
        $this->unlock($bankId);
        return $listener->response(400, "Internal refund #$internalRefundId failed: $statusMessage");
    }
  }

  private function lock($bankId) {
    return $this->redis->hsetnx('odeo_core:internal_refund_lock', $bankId, 1);
  }

  private function unlock($bankId) {
    $this->redis->hdel('odeo_core:internal_refund_lock', $bankId);
  }

  private function isDuplicate($bankId) {
    $lastTime = $this->redis->hget('odeo_core:internal_refund', $bankId);

    if ($lastTime == null) {
      return false;
    }

    return time() - $lastTime < 5 * 60;
  }

}

<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ApiManager;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaStanGenerator;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\Mock\MockApiManager;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\TransferInquiry;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaTransferRepository;

class Transfer {

  private $artajasaDisbursementRepo, $redis, $stanGenerator, $transferInquiry,
    $transferRepo, $apiManager;

  public function __construct() {
    $this->transferInquiry = app()->make(TransferInquiry::class);
    $this->transferRepo = app()->make(DisbursementArtajasaTransferRepository::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->stanGenerator = app()->make(ArtajasaStanGenerator::class);
    $this->redis = Redis::connection();

    if (app()->environment() == 'production') {
      $this->apiManager = app()->make(ApiManager::class);
    }
    else {
      $this->apiManager = app()->make(MockApiManager::class);
    }
  }

  public function exec(PipelineListener $listener, $data) {
    $id = $data['artajasa_disbursement_id'];

    if (!$this->lockDisbursement($id)) {
      clog('AJDisbursement', "Disbursement #{$id} Response 400 (cant lock)");
      return $listener->response(400);
    }

    if (!($ajDisbursement = $this->artajasaDisbursementRepo->findById($id))) {
      clog('AJDisbursement', "Disbursement #{$id} Response 400 (not found)");
      return $listener->response(400);
    }

    if ($ajDisbursement->status != ArtajasaDisbursement::PENDING) {
      clog('AJDisbursement', "Disbursement #{$id} Response 400 (not pending)");
      return $listener->response(400);
    }

    list($responseCode, $transferred) = $this->handle($ajDisbursement, $data);

    if ($ajDisbursement->status != ArtajasaDisbursement::SUCCESS) {
      clog('AJDisbursement', "Disbursement #{$id} Response 400 (not success): " . json_encode($ajDisbursement));
      return $listener->response(400, [
        'response_code' => $responseCode,
        'transferred' => $transferred,
      ]);
    }

    $this->unlockDisbursement($id);
    clog('AJDisbursement', "Disbursement #{$id} Response 200");

    return $listener->response(200, [
      'response_code' => $responseCode,
      'transferred' => $transferred,
    ]);
  }

  private function handle($ajDisbursement, $data) {
    try {
      $this->init($ajDisbursement, $data);

      $transferInquiry = $this->transferInquiry->exec([
        'user_id' => $data['user_id'],
        'user_name' => $data['user_name'],
        'bank_code' => $data['bank_code'],
        'regency_code' => $data['regency_code'],
        'transfer_to' => $data['transfer_to'],
        'amount' => $data['amount'],
        'purpose_desc' => $data['purpose_desc'],
        'channel_type' => $data['channel_type'],
        'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_DISBURSEMENT_ARTAJASA_DISBURSEMENT,
        'reference_id' => $ajDisbursement->id,
        'terminal_id' => $data['terminal_id'],
        'sender_prefix' => $data['sender_prefix'],
      ]);
      clog('AJDisbursement', 'Executed transfer inquiry: ' . json_encode($transferInquiry));

      $ajDisbursement->disbursement_artajasa_transfer_inquiry_id = $transferInquiry->id;

      if ($transferInquiry->status != ArtajasaDisbursement::SUCCESS) {
        $ajDisbursement->status = $transferInquiry->status;
        $ajDisbursement->error_exception_code = $transferInquiry->error_exception_code;
        $ajDisbursement->error_exception_message = $transferInquiry->error_exception_message;
        $this->artajasaDisbursementRepo->save($ajDisbursement);

        return [$transferInquiry->response_code, false];
      }

      $ajDisbursement->transfer_to_name = $transferInquiry->transfer_to_name;
      $this->artajasaDisbursementRepo->save($ajDisbursement);

      sleep(2);
      clog('AJDisbursement', 'Executing transfer');
      $transfer = $this->initTransfer($data, $transferInquiry->transfer_to_name, Carbon::now());
      $this->doTransfer($transfer, $data);
      clog('AJDisbursement', 'Executed transfer: ' . json_encode($transfer));

      $ajDisbursement->disbursement_artajasa_transfer_id = $transfer->id;
      $ajDisbursement->status = $transfer->status;
      $this->artajasaDisbursementRepo->save($ajDisbursement);

      return [$transfer->response_code, true];
    } catch (Exception $e) {
      clog('AJDisbursement', $e);
      \Log::error($e);
      $ajDisbursement->status = ArtajasaDisbursement::FAIL;
      $ajDisbursement->error_exception_message = $e->getMessage();
      $ajDisbursement->error_exception_code = $e->getCode();
      $this->artajasaDisbursementRepo->save($ajDisbursement);

      return [null, true];
    }
  }

  private function init($ajDisbursement, $data) {
    $ajDisbursement->bank_code = $data['bank_code'];
    $ajDisbursement->regency_code = $data['regency_code'];
    $ajDisbursement->transfer_to = $data['transfer_to'];
    $ajDisbursement->transfer_to_bank = $data['transfer_to_bank'];
    $ajDisbursement->amount = $data['amount'];
    $ajDisbursement->purpose_desc = $data['purpose_desc'];
    $ajDisbursement->terminal_id = $data['terminal_id'];
  }

  private function initTransfer($data, $transferToName, $date) {
    $transfer = $this->transferRepo->getNew();
    $transfer->disbursement_artajasa_disbursement_id = $data['artajasa_disbursement_id'];
    $transfer->transaction_datetime = $date;
    $transfer->channel_type = $data['channel_type'];
    $transfer->bank_code = $data['bank_code'];
    $transfer->amount = $data['amount'];
    $transfer->transfer_to = $data['transfer_to'];
    $transfer->transfer_to_name = $transferToName;
    $transfer->purpose_desc = $data['purpose_desc'] ?? null;
    $transfer->terminal_id = $data['terminal_id'];
    $transfer->error_exception_message = null;
    $transfer->error_exception_code = null;

    DB::transaction(function () use ($transfer, $date) {
      $this->transferRepo->lock();
      $transfer->transaction_id = $this->stanGenerator->generate($date);

      if (!$this->transferRepo->save($transfer)) {
        throw new \Exception('Save transfer failed');
      }

      clog('AJDisbursement', 'Saved transfer #' . $transfer->id);
    });

    return $transfer;
  }

  private function doTransfer($transfer, $data) {
    try {
      clog('AJDisbursement', "Sending transfer #{$transfer->id} request");
      $result = $this->apiManager->transfer(
        $transfer->transaction_id,
        $transfer->transaction_datetime,
        $data['artajasa_disbursement_id'],
        $transfer->bank_code,
        $data['regency_code'],
        $transfer->transfer_to,
        $transfer->transfer_to_name,
        $transfer->amount,
        $transfer->purpose_desc,
        $transfer->channel_type,
        $data['user_id'],
        $data['user_name'],
        $data['terminal_id'],
        $data['sender_prefix']
      );
      clog('AJDisbursement', "Transfer #{$transfer->id} request sent: " . json_encode($result));

      $transfer->response_code = $result['Response']['Code'];
      $transfer->response_description = $result['Response']['Description'];

      if ($result['Response']['Code'] == '00') {
        $transfer->status = ArtajasaDisbursement::SUCCESS;
      }
      else if ($result['Response']['Code'] == '68' || $result['Response']['Code'] == 'TO') {
        $transfer->status = ArtajasaDisbursement::SUSPECT;
      }
      else {
        $transfer->status = ArtajasaDisbursement::FAIL;
      }

      $transfer->response_datetime = Carbon::now();
      $this->transferRepo->save($transfer);
      clog('AJDisbursement', 'Saved transfer #' . $transfer->id);

      return $transfer->status != ArtajasaDisbursement::FAIL;
    } catch (Exception $e) {
      clog('AJDisbursement', 'Transfer Exception: ' . $e->getMessage());
      \Log::error($e);

      if (strpos($e->getMessage(), 'cURL error 28') !== false) {
        clog('AJDisbursement', 'Set as timeout');
        $transfer->status = ArtajasaDisbursement::TIMEOUT;
        $transfer->response_code = ArtajasaDisbursement::RESPONSE_CODE_RTO;
        $transfer->error_exception_message = $e->getMessage();
        $transfer->error_exception_code = $e->getCode();
      }
      else {
        $transfer->status = ArtajasaDisbursement::SUSPECT;
        $transfer->response_code = ArtajasaDisbursement::RESPONSE_CODE_EXCEPTION;
        $transfer->error_exception_message = $e->getMessage();
        $transfer->error_exception_code = $e->getCode();
      }

      $this->transferRepo->save($transfer);
      clog('AJDisbursement', "Saved transfer #{$transfer->id} after exception");

      return false;
    }
  }

  private function lockDisbursement($id) {
    return $this->redis->hsetnx('odeo_core:artajasa_disbursement_lock', 'id_' . $id, 1);
  }

  private function unlockDisbursement($id) {
    return $this->redis->hdel('odeo_core:artajasa_disbursement_lock', 'id_' . $id);
  }

}
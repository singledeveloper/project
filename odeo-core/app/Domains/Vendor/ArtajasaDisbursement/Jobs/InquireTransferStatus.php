<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs;


use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementRequester;
use Odeo\Domains\Disbursement\Withdrawal\Artajasa\HandleArtajasaStatusInquiry as HandleWithdrawal;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\StatusInquiry;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaDisbursementRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Repository\DisbursementArtajasaTransferRepository;
use Odeo\Domains\Vendor\ArtajasaDisbursement\TransferStatusUpdater;
use Odeo\Jobs\Job;

class InquireTransferStatus extends Job implements ShouldQueue {

  private $data, $artajasaDisbursementRepo, $redis, $statusInquiry, $artajasaTransferRepo;

  public function __construct($data = null) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    $this->statusInquiry = app()->make(StatusInquiry::class);
    $this->artajasaDisbursementRepo = app()->make(DisbursementArtajasaDisbursementRepository::class);
    $this->artajasaTransferRepo = app()->make(DisbursementArtajasaTransferRepository::class);
    $this->redis = Redis::connection();

    $id = $this->data['id'];

    if (!$this->lock($id)) {
      return;
    }

    if (!($ajDisbursement = $this->artajasaDisbursementRepo->findById($id))) {
      return;
    }

    if ($ajDisbursement->status != ArtajasaDisbursement::TIMEOUT) {
      return;
    }

    if ($ajDisbursement->disbursement_type != Disbursement::API_DISBURSEMENT &&
      $ajDisbursement->disbursement_type != Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH) {
      return;
    }

    if (!($transfer = $this->artajasaTransferRepo->findById($ajDisbursement->disbursement_artajasa_transfer_id))) {
      return;
    }

    if ($transfer->status != ArtajasaDisbursement::TIMEOUT) {
      return;
    }

    $ajDisbursement->last_inquire_date = Carbon::now();
    $this->artajasaDisbursementRepo->save($ajDisbursement);

    $statusInquiry = $this->statusInquiry
      ->exec($id, $transfer->transaction_id, $transfer->transaction_datetime);

    if ($statusInquiry->status != ArtajasaDisbursement::SUCCESS || $statusInquiry->query_response_code != '00') {
      $this->unlock($id);
      return;
    }

    $transfer->response_code = $statusInquiry->query_response_code;
    $transfer->response_description = $statusInquiry->query_response_description;
    $transfer->status = ArtajasaDisbursement::SUCCESS;
    $ajDisbursement->status = ArtajasaDisbursement::SUCCESS;
    $this->artajasaTransferRepo->save($transfer);
    $this->artajasaDisbursementRepo->save($ajDisbursement);

    $pipeline = new Pipeline();

    switch ($ajDisbursement->disbursement_type) {
      case Disbursement::API_DISBURSEMENT:
        $pipeline->add(new Task(ApiDisbursementRequester::class, 'complete'));
        break;

      case Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH:
        $pipeline->add(new Task(WithdrawRequester::class, 'complete'));
        break;

      default:
        return;
    }

    $pipeline->execute([
      'disbursement_id' => $ajDisbursement->disbursement_reference_id,
      'withdraw_id' => $ajDisbursement->disbursement_reference_id,
    ]);
    $this->unlock($id);
  }


  private function lock($id) {
    return $this->redis->hsetnx('odeo_core:aj_inquire_status_lock', 'id_' . $id, 1);
  }

  private function unlock($id) {
    return $this->redis->hdel('odeo_core:aj_inquire_status_lock', 'id_' . $id);
  }

}

<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Odeo\Jobs\Job;

class SendArtajasaTopupDetectedAlert extends Job {

  use SerializesModels;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  public function handle() {
    if (!isProduction()) {
      return;
    }

    Mail::send('emails.artajasa_topup_detected', [
      'data' => $this->data
    ], function ($m) {
      $m->from('noreply@odeo.co.id', 'odeo')
        ->to('disbursement@odeo.co.id')
        ->subject('Artajasa Topup Detected - ' . time());
    });
  }

}

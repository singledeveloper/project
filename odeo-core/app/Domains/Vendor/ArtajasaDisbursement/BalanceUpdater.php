<?php

namespace Odeo\Domains\Vendor\ArtajasaDisbursement;

use Odeo\Domains\Constant\ArtajasaMutationDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Helper\ArtajasaBalanceUpdater;

class BalanceUpdater {

  private $artajasaBalanceUpdater;

  public function __construct() {
    $this->artajasaBalanceUpdater = app()->make(ArtajasaBalanceUpdater::class);
  }

  public function updateBalance(PipelineListener $listener, $data) {
    $data['fee'] = $data['fee'] ?? 0;
    $data['type'] = $data['type'] ?? ArtajasaMutationDisbursement::TYPE_TOPUP;

    $this->artajasaBalanceUpdater->updateBalance($data);

    return $listener->response(200);
  }

}
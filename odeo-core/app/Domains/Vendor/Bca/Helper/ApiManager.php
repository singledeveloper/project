<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/23/17
 * Time: 7:47 PM
 */

namespace Odeo\Domains\Vendor\Bca\Helper;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Redis;

class ApiManager {

  static $PRODUCTION_BASE_URL = 'https://api.klikbca.com:8065';
  static $STAGING_BASE_URL = 'https://sandbox.bca.co.id:443';
  static $BASE_URL;
  static $CLIENT_ID;
  static $CLIENT_SECRET;
  static $API_SECRET, $API_KEY = '';
  static $CORPORATEID, $ACCOUNTNUMBER;
  static $CHANNELID = '95051';

  private $client, $redis;

  public function __construct() {
    $this->redis = Redis::connection();

    $this->client = new Client();
  }

  public static function init() {
    if (app()->environment() == 'production') {
      self::$BASE_URL = self::$PRODUCTION_BASE_URL;
      self::$CLIENT_ID = env('API_BCA_CLIENT_ID');
      self::$CLIENT_SECRET = env('API_BCA_CLIENT_SECRET');
      self::$API_SECRET = env('API_BCA_API_SECRET');
      self::$API_KEY = env('API_BCA_API_KEY');
      self::$CORPORATEID = 'IBSODEOTEK';
      self::$ACCOUNTNUMBER = [
        '0762299966'
      ];
    }
//    else { // UAT
//      self::$BASE_URL = 'https://devapi.klikbca.com:443';
//      self::$CLIENT_ID = 'b095ac9d-2d21-42a3-a70c-4781f4570704';
//      self::$CLIENT_SECRET = 'bedd1f8d-3bd6-4d4a-8cb4-e61db41691c9';
//      self::$API_SECRET = '5e636b16-df7f-4a53-afbe-497e6fe07edc';
//      self::$API_KEY = 'dcc99ba6-3b2f-479b-9f85-86a09ccaaacf';
//      self::$CORPORATEID = 'h2hauto008';
//      self::$ACCOUNTNUMBER = [
//        ''
//      ];
//    }
    else { // DEV
      self::$BASE_URL = self::$STAGING_BASE_URL;
      self::$CLIENT_ID = '02bbc9cc-b196-4c0a-b225-934bf0c6b58c';
      self::$CLIENT_SECRET = 'b4bfdbfa-4bb4-4dba-86e5-59009279bf5f';
      self::$API_SECRET = 'c3428e60-535e-4f8d-a3e9-1e41b44f819e';
      self::$API_KEY = 'e203da6c-833e-4805-afe9-1df9ccd0580f';
      self::$CORPORATEID = 'BCAAPI2016';
      self::$ACCOUNTNUMBER = [
        '0201245680'
      ];
    }
  }

  public function getAccessToken() {

    $namespace = 'odeo_core:bca_client_credential';

    if ($accessToken = $this->redis->hget($namespace, 'access_token')) {
      return $accessToken;
    }

    $response = $this->client->request('POST', self::$BASE_URL . '/api/oauth/token', [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => 'Basic ' . base64_encode(self::$CLIENT_ID . ':' . self::$CLIENT_SECRET),
      ],
      'form_params' => [
        'grant_type' => 'client_credentials',
      ],
    ]);

    $now = Carbon::now();
    $result = json_decode($response->getBody()->getContents());

    $this->redis->hset($namespace, 'access_token', $result->access_token);
    $this->redis->hset($namespace, 'token_type', $result->token_type);
    $this->redis->hset($namespace, 'scope', $result->scope);
    $this->redis->expireat($namespace, $now->addMinute(50)->timestamp);

    return $result->access_token;

  }

  public function getSignature($method, $url, $accessToken, $data, $iso8061) {

    foreach ($data as &$d) {
      is_string($d) && ($d = str_replace(' ', '', $d));
    }

    $url = str_replace(',', '%2C', $url);

    $sToSign = strtoupper($method) . ':' . $url . ':' . $accessToken;

    $hashed = \hash('sha256', empty($data) ? '' : json_encode($data, JSON_UNESCAPED_SLASHES));
    
    $sToSign .= ':' . $hashed . ':' . $iso8061;

    return hash_hmac('sha256', $sToSign, self::$API_SECRET);
  }

  public function request($method, $url, $data = [], $additionalHeader = []) {


    $token = $this->getAccessToken();
    $now = Carbon::now();
    $payload = [];
    $addZero = function ($text) {
      return sprintf('%02d', $text);
    };

    $iso8061 = '' . $now->year . '-' .
      $addZero($now->month) . '-' .
      $addZero($now->day) . 'T' .
      $addZero($now->hour) . ':' .
      $addZero($now->minute) . ':' .
      $addZero($now->second) . '.000+07:00';

    if (!empty($data)) {
      $method = strtoupper($method);
      if ($method == 'GET') {

        $url .= '?' . http_build_query($data);
        $data = [];

      } else if ($method == 'POST') {
        $payload = [
          'json' => $data
        ];
      }
    }

    try {
      $headers = array_merge([
        'Authorization' => 'Bearer ' . $token,
        'Content-Type' => 'application/json',
        'origin' => 'api.odeo.co.id',
        'X-BCA-Key' => self::$API_KEY,
        'X-BCA-Timestamp' =>  $iso8061,
        'X-BCA-Signature' => $this->getSignature($method, $url, $token, $data, $iso8061),
      ], $additionalHeader);

      $response = $this->client->request($method, self::$BASE_URL . $url, array_merge([
        'headers' => $headers,
        'timeout' => 100,
        'http_errors' => false
      ], $payload));

      if (strpos($url, '/banking/corporates/transfers/domestic') > 0) {
        \Log::info($url);
        \Log::info('header: ');
        \Log::info(json_encode($headers));
        \Log::info('data: ');
        \Log::info(json_encode($data));
      }

    } catch (\Exception $exception) {
      throw  $exception;
    }

    $data = json_decode(preg_replace("!\r?\t?\n!", "", $response->getBody()->getContents()), true);
    
    if (strpos($url, '/banking/corporates/transfers/domestic') > 0) {
      \Log::info('response: ');
      \Log::info(json_encode($data));
    }
    
    return [
      'status_code' => $response->getStatusCode(),
      'data' => $data
    ];

  }


  public function getBalance(array $accountNumber = []) {

    $accountNumber = implode(',', count($accountNumber) ? $accountNumber : self::$ACCOUNTNUMBER );

    return $this->request('GET', '/banking/v3/corporates/' . self::$CORPORATEID . '/accounts/' . $accountNumber);

  }

  public function getMutation($accountNumber, $startDate, $endDate) {

    return $this->request('GET', '/banking/v3/corporates/' . self::$CORPORATEID . '/accounts/' . $accountNumber . '/statements', [
      'EndDate' => $endDate,
      'StartDate' => $startDate,
    ]);

  }

  public function transfer($data) {

    $response = $this->request('POST', '/banking/corporates/transfers', [
      'CorporateID' => $data['corporate_id'],
      'SourceAccountNumber' => $data['source_account_number'],
      'TransactionID' => $data['transaction_id'],
      'TransactionDate' => $data['transaction_date'],
      'ReferenceID' => $data['reference_id'],
      'CurrencyCode' => 'IDR',
      'Amount' => $data['amount'],
      'BeneficiaryAccountNumber' => $data['beneficiary_account_number'],
      'Remark1' => $data['remark_1'],
      'Remark2' => $data['remark_2'],
    ]);

    return $response;
  }

  public function domesticTransfer($data) {

    $response = $this->request('POST', '/banking/corporates/transfers/domestic', [
      'TransactionID' => $data['transaction_id'],
      'TransactionDate' => $data['transaction_date'],
      'ReferenceID' => $data['reference_id'],
      'SourceAccountNumber' => $data['source_account_number'],
      'BeneficiaryAccountNumber' => $data['beneficiary_account_number'],
      'BeneficiaryBankCode' => $data['benificiary_bank_code'],
      'BeneficiaryName' => $data['beneficiary_name'],
      'Amount' => $data['amount'],
      'TransferType' => 'LLG',
      'BeneficiaryCustType' => '1',
      'BeneficiaryCustResidence' => '1',
      'CurrencyCode' => 'IDR',
      'Remark1' => $data['remark_1'],
      'Remark2' => $data['remark_2'],
    ], [
      'ChannelID' => self::$CHANNELID,
      'CredentialID' => self::$CORPORATEID
    ]);

    return $response;
  }

}

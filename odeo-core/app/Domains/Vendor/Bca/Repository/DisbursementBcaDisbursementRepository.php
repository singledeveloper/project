<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/25/17
 * Time: 9:58 PM
 */

namespace Odeo\Domains\Vendor\Bca\Repository;


use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Vendor\Bca\Model\DisbursementBcaDisbursement;

class DisbursementBcaDisbursementRepository extends Repository {

  private $lock = false;

  public function __construct(DisbursementBcaDisbursement $bcaDisbursement) {
    $this->model = $bcaDisbursement;
  }

  public function lock() {
    $this->lock = true;
  }

  public function findById($id, $fields = ['*']) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->find($id, $fields);
  }

  public function getLastInsertedWithinDate($date) {
    $model = $this->getCloneModel();
    if ($this->lock) $model = $this->model->lockForUpdate();
    return $model->whereDate('transaction_date', '=', $date)
      ->orderBy('transaction_id', 'desc')->first();
  }

  public function get() {

    $filters = $this->getFilters();

    $query = $this->getCloneModel();

    if (isset($filters['search'])) {
      if (isset($filters['search']['bca_disbursement_id'])) {
        $query = $query->where('id', $filters['search']['bca_disbursement_id']);
      }
    }

    return $this->getResult($query->orderBy('id', 'desc'));
  }


  public function getSuspectNotRunning() {

    $model = $this->getCloneModel();

    return $model
      ->where('created_at', '<', Carbon::now()->subMinute(10))
      ->whereNotIn('status', [
        BcaDisbursement::SUCCESS, BcaDisbursement::FAIL
      ])
      ->whereNotNull('response_datetime')
      ->get();
  }

  public function getPendingAmount($disbursementType, $date)
  {
    return $this->getCloneModel()
      ->select(\DB::raw('sum(amount) as amount'))
      ->where('disbursement_type', $disbursementType)
      ->where(function ($query) use ($date) {
        return $query
          ->where('created_at', '>=', $date)
          ->orWhere('status', BcaDisbursement::PENDING);
      })
      ->first();
  }

  public function getNthDisbursementReferenceId($period, $disbursementType, $nthRow) {
    $query = "
      WITH data AS (
          SELECT
            id,
            disbursement_type,
            disbursement_reference_id,
            row_number()
            OVER (
              ORDER BY id ) rn
          FROM disbursement_bca_disbursements
          WHERE to_char(created_at, 'YYYY-MM')=:period AND status=:status
          ORDER BY id
      )
      SELECT disbursement_reference_id
      FROM data
      WHERE disbursement_type=:disbursement_type AND rn=:nth_row";

    return \DB::selectOne(\DB::raw($query), [
      'status' => BcaDisbursement::SUCCESS,
      'period' => $period,
      'nth_row' => $nthRow,
      'disbursement_type' => $disbursementType,
    ]);
  }

  public function countSuccessByPeriod($period) {
    return $this->getCloneModel()
      ->where(\DB::raw("to_char(created_at, 'YYYY-MM')"), $period)
      ->where('status', '50000')
      ->count();
  }

  public function getRowNumberOfMonth($id) {
    $query = "
      WITH data AS (
        SELECT
          row_number()
          OVER (
            PARTITION BY to_char(transfer_datetime, 'YYYY-MM')
            ORDER BY transfer_datetime ) AS rn,
          *
        FROM disbursement_bca_disbursements
        WHERE status = '50000'
      )
      SELECT *
      FROM data
      WHERE id = :id";

    return \DB::selectOne(\DB::raw($query), [
      'id' => $id,
    ]);
  }

  public function getSuspectDisbursements() {
    return $this->model
      ->join('api_disbursements', 'api_disbursements.id', '=', 'disbursement_bca_disbursements.disbursement_reference_id')
      ->where('api_disbursements.status', ApiDisbursement::SUSPECT)
      ->select('disbursement_bca_disbursements.*')
      ->get();
  }
}
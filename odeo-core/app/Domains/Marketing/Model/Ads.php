<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Core\Entity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ads extends Entity {
  use SoftDeletes;

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];
  protected $table = 'ads';
  protected $fillable = ['id', 'name'];
}

<?php

namespace Odeo\Domains\Marketing\Model;

use Odeo\Domains\Core\Entity;

class AdsClick extends Entity {
  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['created_at'];

  protected $fillable = ['ads_id', 'store_id', 'ip', 'user_agent'];

  public $timestamps = false;

  public static function boot() {
    parent::boot();

    static::creating(function($model) {
      $timestamp = $model->freshTimestamp();
      $model->setCreatedAt($timestamp);
    });
  }
}

<?php

namespace Odeo\Domains\Marketing\Competitor\Repository;


use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Competitor\Model\CompetitorPrice;

class CompetitorPriceRepository extends Repository {

  public function __construct(CompetitorPrice $competitorPrice) {
    $this->model = $competitorPrice;
  }

  public function findByCompetitor($competitor) {
    return $this->model->where('competitor', $competitor)->get();
  }

  public function getBySKU($inventoryIds = []) {

    return $this->model
      ->selectRaw('
        DISTINCT ON (pulsa_odeo_id) 
        pulsa_odeo_id, 
        pulsa_odeos.name, 
        competitor, 
        competitor_prices.price,
        competitor_prices.created_at
       ')
      ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'competitor_prices.pulsa_odeo_id')
      ->whereIn('pulsa_odeo_id', $inventoryIds)
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->whereNull('pulsa_odeos.owner_store_id')
      ->orderBy('pulsa_odeo_id')
      ->orderBy('price')
      ->orderBy('competitor_prices.created_at', 'DESC')
      ->get();
  }

  public function gets() {
    $filters = $this->getFilters();
    $query = $this->model
    ->selectRaw('
      competitor_prices.id,
      pulsa_odeo_id, 
      pulsa_odeos.name, 
      competitor, 
      competitor_prices.price,
      competitor_prices.created_at,
      competitor_prices.updated_at
    ')
    ->join('pulsa_odeos', 'pulsa_odeos.id', '=', 'competitor_prices.pulsa_odeo_id')
      ->where('pulsa_odeos.status', Pulsa::STATUS_OK)
      ->whereNull('pulsa_odeos.owner_store_id');

    if (isset($filters['search'])) {
      if (isset($filters['search']['competitor_name'])) {
        $query->where('competitor', '=', $filters['search']['competitor_name']);
      }
      if (isset($filters['search']['pulsa_odeo_id'])) {
        $query->where('pulsa_odeo_id', '=', $filters['search']['pulsa_odeo_id']);
      }
    }

    return $this->getResult($query->orderBy('competitor_prices.id', 'desc'));
  }

  public function getCompetitorNames() {
    return $this->model
    ->select('competitor')
    ->distinct()
    ->orderBy('competitor', 'asc')
    ->get();
  }

  public function getAllCompetitorLatestPrice($pulsaOdeoIds = []) {
    return $this->model
    ->selectRaw("DISTINCT ON (pulsa_odeo_id, competitor) *")
    ->whereIn('pulsa_odeo_id', $pulsaOdeoIds)
    ->orderBy('pulsa_odeo_id')
    ->orderBy('competitor')
    ->orderBy('id', 'desc')
    ->get();
  }
}
<?php

namespace Odeo\Domains\Marketing\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Marketing\Scheduler\StoreRevenueRefresher;


class RefreshRevenue extends Command {

  protected $signature = 'marketing:refresh-revenue';

  protected $description = 'Refresh store revenue';


  public function __construct() {
    parent::__construct();
  }


  public function handle() {
    (new StoreRevenueRefresher)->refresh();
  }
}

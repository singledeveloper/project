<?php

namespace Odeo\Domains\Marketing\Helper;

use Odeo\Domains\Constant\CashType;
use Carbon\Carbon;
use DB;

class RevenueManager {

  private $rows = [];

  public function __construct() {
    $this->storeRevenue = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->referralCashbacks = app()->make(\Odeo\Domains\Subscription\MdPlus\Repository\ReferralCashbackRepository::class);
  }

  public function getStoreRevenue($storeIds, $dateStart, $dateEnd, &$profits = [], &$orderCounts = []) {
    $data = $this->storeRevenue->findStoreTotalBetween($storeIds, Carbon::parse($dateStart)->format('Y-m-d'), Carbon::parse($dateEnd)->format('Y-m-d'), $profits, $orderCounts);
    
    $dateRange = dateRange($dateStart, $dateEnd);
    $referralCashbacks = $this->referralCashbacks->getByDateAndStore($dateRange, $storeIds)->keyBy('store_id');
    
    if (!is_array($storeIds)) $storeIds = [$storeIds];
    foreach ($storeIds as $storeId) {
      if (!isset($data[$storeId])) $data[$storeId] = 0;
      if (!isset($profits[$storeId])) $profits[$storeId] = 0;
      if (!isset($orderCounts[$storeId])) $orderCounts[$storeId] = 0;

      $profit = $profits[$storeId] + (isset($referralCashbacks[$storeId]) ? $referralCashbacks[$storeId]->total_claimed : 0);
      $data[$storeId] = $this->currency->formatPrice($data[$storeId]);
      $profits[$storeId] = $this->currency->formatPrice($profit);
    }

    return $data;
  }

  public function getUserRevenue($userId, $dateStart, $dateEnd, &$profits = [], &$orderCounts = []) {

    $stores = app()->make(\Odeo\Domains\Subscription\Repository\UserStoreRepository::class);
    $storeIds = $stores->getCloneModel()->where('user_id', $userId)->pluck('store_id')->toArray();
    $data = $this->storeRevenue->findStoreTotalBetween($storeIds, Carbon::parse($dateStart)->format('Y-m-d'), Carbon::parse($dateEnd)->format('Y-m-d'), $profits, $orderCounts);

    $revenue = 0;
    $profit = 0;
    $orderCount = 0;

    foreach ($storeIds as $item) {
      if(isset($data[$item])) {
        $revenue += $data[$item];
        $profit += $profits[$item];
        $orderCount += $orderCount[$item];
      }
    }
    $profits = [$userId => $this->currency->formatPrice($profit)];
    $orderCounts = [$userId => $orderCount];

    return [$userId => $this->currency->formatPrice($revenue)];
  }
}

<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\InventoryMarginMultiplier;

class MarginMultiplierRepository extends Repository {

  public function __construct(InventoryMarginMultiplier $marginMultiplier) {
    $this->model = $marginMultiplier;
  }

  public function getMultiplier($serviceDetailId, $margin) {
    return $this->findByAttributes(['service_detail_id' => $serviceDetailId, 'margin' => $margin], null, ['multiplier']);
  }

  public function getDefaultMultiplier($serviceDetailId) {
    return $this->findByAttributes(['service_detail_id' => $serviceDetailId], null, ['margin', 'multiplier']);
  }
}

<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Constant\Plan;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Marketing\Model\StoreMarketingBudget;

class StoreMarketingBudgetRepository extends Repository {

  public function __construct(StoreMarketingBudget $storeBudget) {
    $this->model = $storeBudget;
  }

  public function getActive() {
    $query = $this->model->join('stores', 'stores.id' , '=', 'store_marketing_budgets.store_id')
      ->join('user_stores', 'stores.id', '=', 'user_stores.store_id')
      ->whereIn('stores.status', StoreStatus::ACTIVE)
      ->where('stores.plan_id', '<>', Plan::FREE)
      ->select('user_id', 'store_marketing_budgets.*');

    return $query->get();
  }
}

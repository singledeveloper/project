<?php

namespace Odeo\Domains\Marketing\Repository;

use Odeo\Domains\Constant\ForBusiness;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Marketing\Model\BusinessResellerTransaction;

class BusinessResellerTransactionRepository extends Repository {

  public function __construct(BusinessResellerTransaction $businessResellerTransaction) {
    $this->model = $businessResellerTransaction;
  }

  public function getByTransactionDate($date) {
    return $this->getCloneModel()
      ->where('transaction_date', $date)->get();
  }

  public function getUnprocessedTransaction() {
    return $this->getCloneModel()
      ->with(['user'])
      ->where('is_processed', false)
      ->orderBy('id', 'asc')->first();
  }

  public function getWeekTransactionsGroupByDate($userIds, $resellerUserId = '') {
    $query = $this->getCloneModel()
      ->leftJoin('business_reseller_bonuses', 'business_reseller_bonuses.business_reseller_transaction_id', '=', 'business_reseller_transactions.id')
      ->whereIn('business_reseller_transactions.user_id', $userIds)
      ->whereRaw("cast(transaction_date as date) >= date_trunc('day', CURRENT_TIMESTAMP - interval '7 days')")
      ->where(function($query){
        $query->whereNotNull('business_reseller_group_detail_id')
          ->orWhereNull('business_reseller_bonuses.id');
      });

    if ($resellerUserId != '') $query->where('business_reseller_bonuses.reseller_user_id', $resellerUserId);

    return $query
      ->groupBy('business_reseller_transactions.user_id', 'transaction_date')
      ->orderBy('transaction_date', 'asc')
      ->select('business_reseller_transactions.user_id', 'transaction_date',
        \DB::raw('sum(total_fee) as amount'), \DB::raw('sum(reseller_fee) as fee'), \DB::raw('sum(qty) as qty'))
      ->get();
  }

  public function getWeekTransactionsGroupByType($userId, $resellerUserId = '') {
    $query = $this->getCloneModel()
      ->leftJoin('business_reseller_bonuses', 'business_reseller_bonuses.business_reseller_transaction_id', '=', 'business_reseller_transactions.id')
      ->leftJoin('payment_odeo_payment_channel_informations', 'payment_odeo_payment_channel_informations.id', '=', 'business_reseller_transactions.payment_group_id')
      ->where('business_reseller_transactions.user_id', $userId)
      ->whereRaw("cast(transaction_date as date) >= date_trunc('day', CURRENT_TIMESTAMP - interval '7 days')")
      ->where(function($query){
        $query->whereNotNull('business_reseller_group_detail_id')
          ->orWhereNull('business_reseller_bonuses.id');
      });

    if ($resellerUserId != '') $query->where('business_reseller_bonuses.reseller_user_id', $resellerUserId);

    return $query
      ->groupBy('business_reseller_transactions.user_id', 'business_reseller_transactions.type',
        'business_reseller_transactions.payment_group_id', 'payment_odeo_payment_channel_informations.name',
        'business_reseller_bonuses.fee_type', 'business_reseller_bonuses.fee_value')
      ->orderBy('business_reseller_transactions.type', 'asc')
      ->select('business_reseller_transactions.user_id', 'business_reseller_transactions.type',
        'business_reseller_transactions.payment_group_id', 'payment_odeo_payment_channel_informations.name',
        'business_reseller_bonuses.fee_type', 'business_reseller_bonuses.fee_value',
        \DB::raw('sum(total_fee) as amount'), \DB::raw('sum(reseller_fee) as fee'), \DB::raw('sum(qty) as qty'))
      ->get();
  }

  public function getLastTenDaysUsagesByUser() {
    return $this->getCloneModel()
      ->join('users', 'users.id', '=', 'business_reseller_transactions.user_id')
      ->whereIn('business_reseller_transactions.type', [
        ForBusiness::TRANSACTION_PAYMENT_GATEWAY,
        ForBusiness::TRANSACTION_DISBURSEMENT
      ])
      ->whereRaw("cast(business_reseller_transactions.transaction_date as date) >= date_trunc('day', CURRENT_TIMESTAMP - interval '10 days')")
      ->groupBy('users.id', 'users.name', 'business_reseller_transactions.type')
      ->orderBy(\DB::raw('sum(total_amount)'), 'desc')
      ->select('users.id as user_id', 'users.name as user_name', 'business_reseller_transactions.type',
        \DB::raw('sum(business_reseller_transactions.total_amount) as amount'))
      ->get();
  }

  public function getLastTenDaysPaymentGatewayUsage($paymentGroupId) {
    return $this->getCloneModel()
      ->join('users', 'users.id', '=', 'business_reseller_transactions.user_id')
      ->where('business_reseller_transactions.type', ForBusiness::TRANSACTION_PAYMENT_GATEWAY)
      ->whereRaw("cast(business_reseller_transactions.transaction_date as date) >= date_trunc('day', CURRENT_TIMESTAMP - interval '10 days')")
      ->where('payment_group_id', $paymentGroupId)
      ->groupBy('users.name')
      ->orderBy(\DB::raw('sum(business_reseller_transactions.total_amount)'), 'desc')
      ->select(
        'users.name',
        \DB::raw('sum(business_reseller_transactions.total_amount) as amount'),
        \DB::raw('sum(business_reseller_transactions.qty) as qty')
      )->get();
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/2/17
 * Time: 2:59 PM
 */

namespace Odeo\Domains\Marketing\Rateapp;


use Carbon\Carbon;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Core\PipelineListener;

class RateRequester {

  private $userRateApps;

  public function __construct() {
    $this->userRateApps = app()->make(\Odeo\Domains\Marketing\Rateapp\Repository\UserRateAppRepository::class);
  }

  public function request(PipelineListener $listener, $data) {

    if ($this->userRateApps->getExistingRate($data['auth']['user_id'], $data['auth']['platform_id'])) {
      return $listener->response(400, 'You have already rated this app');
    }

    $app = null;


    if ($data['auth']['platform_id'] == Platform::ANDROID) {

      $app = app()
        ->make(\Odeo\Domains\Account\Appidentifier\Repository\UserAndroidIdentifierRepository::class)
        ->findByUserId($data['auth']['user_id']);

      if ($app && count($this->userRateApps->checkIsUsedOnAndroid([
          'ssaid' => $app->ssaid,
          'instance_id' => $app->instance_id
        ])) > 0
      ) {
        return $listener->response(400, 'Your have already rated this app');
      }

    } else {
      return $listener->response(400);
    }

    $userRate = $this->userRateApps->getNew();
    $userRate->user_id = $data['auth']['user_id'];
    $userRate->platform_id = $data['auth']['platform_id'];
    $userRate->confirmation_name = $data['confirmation_name'];
    $userRate->applied_at = Carbon::now()->addMinute(2)->toDateTimeString();
    $userRate->app_identifier_id = $app->id ?? null;
    $this->userRateApps->save($userRate);

    return $listener->response(200, [
      'message' => 'Rate bonus will be given after we have checked your rating'
    ]);
  }

}
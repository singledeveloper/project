<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/2/17
 * Time: 3:46 PM
 */

namespace Odeo\Domains\Marketing\Rateapp\Scheduler;


use Carbon\Carbon;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;

class RateScheduler {

  private $inserter, $userRateApps;

  public function __construct() {
    $this->userRateApps = app()->make(\Odeo\Domains\Marketing\Rateapp\Repository\UserRateAppRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
  }

  public function run() {


    foreach ($this->userRateApps->getMustBeApplied() as $rate) {

      $this->inserter->add([
        'user_id' => $rate->user_id,
        'trx_type' => TransactionType::APP_RATING_BONUS,
        'cash_type' => CashType::OCASH,
        'amount' => 1000,
        'data' => json_encode(['user_rate_app_id' => $rate->id])
      ]);

      $rate->closed_at = Carbon::now();
      $this->userRateApps->save($rate);
    }
    $this->inserter->run();

  }

}
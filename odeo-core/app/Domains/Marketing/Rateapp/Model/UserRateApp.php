<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/2/17
 * Time: 3:16 PM
 */

namespace Odeo\Domains\Marketing\Rateapp\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class UserRateApp extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }
}
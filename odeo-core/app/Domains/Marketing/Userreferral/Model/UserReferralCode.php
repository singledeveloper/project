<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/2/17
 * Time: 11:59 PM
 */

namespace Odeo\Domains\Marketing\Userreferral\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Account\Model\User;

class UserReferralCode extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }

}
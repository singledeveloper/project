<?php

namespace Odeo\Domains\Marketing;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\MarketingConfig;

class IncomeUpdater {

  public function __construct() {
    $this->incomeOutcome = app()->make(\Odeo\Domains\Marketing\Repository\IncomeOutcomeRepository::class);
  }

  public function updateIncome(PipelineListener $listener, $data) {
    $todaysInOut = $this->incomeOutcome->today();
    $todaysInOut->income += $data['amount'];
    $this->incomeOutcome->save($todaysInOut);
    return $listener->response(200);
  }
}

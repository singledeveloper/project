<?php

namespace Odeo\Domains\Marketing;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Marketing\Jobs\PopulateRevenueQueue;

class QueueSelector {

  public function __construct() {
    $this->redis = Redis::connection();
    $this->storeRevenues = app()->make(\Odeo\Domains\Marketing\Repository\StoreRevenueRepository::class);
    $this->storeInventories = app()->make(\Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryRepository::class);
    $this->serviceDetails = app()->make(\Odeo\Domains\Inventory\Repository\ServiceDetailRepository::class);
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
  }

  private function getDefaultStore($serviceId) {
    switch ($serviceId) {
      case Service::PULSA:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::PAKET_DATA:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::PULSA_POSTPAID:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::BOLT:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::PLN:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::PLN_POSTPAID:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::BROADBAND:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::LANDLINE:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::PDAM:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::PGN:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::BPJS_KES:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::TRANSPORTATION:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::FLIGHT:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::HOTEL:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::GAME_VOUCHER:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::GOOGLE_PLAY:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::MULTI_FINANCE:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      case Service::EMONEY:
        $storeId = StoreStatus::getStoreDefault(); // capulsa
        break;
      default:
        $storeId = 0;
        break;
    }
    return $storeId;
  }

  private function _generateQueueId($serviceId) {
    $switcherQueue = 'odeo_core:store_queue_switcher:' . $serviceId;
    $queueId = $this->redis->rpoplpush($switcherQueue, $switcherQueue);
    if (!$queueId) {
      $this->redis->lpush($switcherQueue, 1);
      $queueId = $this->redis->rpoplpush($switcherQueue, $switcherQueue);
    }
    return $queueId;
  }

  public function get(PipelineListener $listener, $data) {
    if (isset($data['use_preferred_store']) && $data['use_preferred_store']) {
      $listener->addNext(new Task(__CLASS__, 'getPreferredStore'));
      return $listener->response(200);
    } else if ($this->_generateQueueId($data['service_id']) == 1) {
      $listener->addNext(new Task(__CLASS__, 'getFree'));
    }

    return $listener->response(200);
  }

  public function getFree(PipelineListener $listener, $data) {
    $profitQueue = 'odeo_core:store_profit_queue:' . $data['service_id'];
    $profitIdx = 'odeo_core:store_profit_idx';
    $failedOrderCount = 'odeo_core:store_failed_order_count';

    if (!$this->redis->exists($profitQueue)) {
      $storeId = $this->getDefaultStore($data['service_id']);
      if (isset($data['skip_pipeline_response'])) {
        return $storeId;
      }
      if ($storeId) {
        return $listener->response(200, ['store_id' => $storeId, 'is_agent' => false]);
      }
      return $listener->response(400, 'Invalid service id');
    }

    if ($this->redis->exists($profitIdx)) {
      $queueId = intval($this->redis->hmget($profitIdx, $data['service_id'])[0]);
    } else {
      $this->redis->hmset($profitIdx, $data['service_id'], 0);
      $queueId = 0;
    }

    $storeId = null;
    $count = $this->redis->zcard($profitQueue);

    if (!$this->redis->get('odeo_core:redispatching_queue')) {
      while ($queueId < $count) {
        $ids = explode(':', $this->redis->zrange($profitQueue, $queueId, $queueId)[0]);
        if (count($ids) != 2) {
          $storeId = null;
          $serviceDetailId = 0;
        } else {
          list($storeId, $serviceDetailId) = $ids;
        }
        if ($storeId == null) {
          break;
        } else if ($serviceDetailId <= 0) {
          continue;
        } else {
          if (intval($this->redis->hmget($failedOrderCount, $storeId)[0]) < 3) {
            break;
          } else {
            $queueId++;
          }
        }
      }
    }

    if ($storeId == null) {
      // queue empty, populate new queue
      if (!$this->redis->get('odeo_core:redispatching_queue')) {
        $this->redis->set('odeo_core:redispatching_queue', 1);
        $listener->pushQueue(new PopulateRevenueQueue());
      }
      $this->redis->hmset($profitIdx, $data['service_id'], 0);
      $storeId = $this->getDefaultStore($data['service_id']);
    } else {
      $this->redis->hmset($profitIdx, $data['service_id'], $queueId);
    }

    if (isset($data['skip_pipeline_response'])) {
      return $storeId;
    }
    return $listener->response(200, ['store_id' => $storeId, 'is_agent' => false]);
  }

  public function getPreferredStore(PipelineListener $listener, $data) {
    if (!isset($data['auth'])) $data['auth'] = app()->make(\Odeo\Domains\Account\TokenManager::class)->parseBearerAndAuth('seller');

    if (!$data['auth']) {
      $this->_generateQueueId($data['service_id']) && $listener->addNext(new Task(__CLASS__, 'getFree'));
      return $listener->response(200);
    }

    $user = $this->users->findById($data['auth']['user_id']);

    if (!$user->purchase_preferred_store_id) {
      $this->_generateQueueId($data['service_id']) && $listener->addNext(new Task(__CLASS__, 'getFree'));
      return $listener->response(200);
    }

    $store = $this->stores->findById($user->purchase_preferred_store_id);

    if (!StoreStatus::isActive($store->status)) {
      if ($store->can_supply) return $listener->response(400, 'Vendor tidak aktif.');
      $this->_generateQueueId($data['service_id']) && $listener->addNext(new Task(__CLASS__, 'getFree'));
      return $listener->response(200);
    }

    $parser = app()->make(\Odeo\Domains\Subscription\Helper\StoreParser::class);

    $currentData = json_decode($parser->currentData($store->plan_id, $store->current_data));
    $serviceName = strtolower(Service::getConstKeyByValue($data['service_id']));

    if (!$store->can_supply && (!isset($currentData) || !isset($currentData->$serviceName) || (isset($currentData->$serviceName) && $currentData->$serviceName <= 0))) {
      $this->_generateQueueId($data['service_id']) && $listener->addNext(new Task(__CLASS__, 'getFree'));
      return $listener->response(200);
    }

    return $listener->response(200, [
      'store_id' => $user->purchase_preferred_store_id,
      'is_agent' => true
    ]);

  }
}

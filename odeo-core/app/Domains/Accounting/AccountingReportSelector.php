<?php

namespace Odeo\Domains\Accounting;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;

class AccountingReportSelector {
  
  public function __construct() {
    $this->accountingReport = app()->make(\Odeo\Domains\Accounting\Repository\AccountingReportRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    $report = $this->accountingReport->findByAttributes('period', $data['period']);
    
    if ($report) {
      return $listener->response(200, json_decode($report->data));
    } else {
      return $listener->response(204);
    }
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/12/17
 * Time: 11.51
 */

namespace Odeo\Domains\Accounting\InquiryInformation\Model;

use Odeo\Domains\Core\Entity;

class BankInquiryAdditionalInformation extends Entity {

  public function fileImages() {
    return $this->hasMany(BankInquiryAdditionalInformationFileImage::class);
  }

}
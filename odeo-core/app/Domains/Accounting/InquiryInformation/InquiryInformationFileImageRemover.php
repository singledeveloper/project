<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 15/12/17
 * Time: 17.25
 */

namespace Odeo\Domains\Accounting\InquiryInformation;

use Odeo\Domains\Core\PipelineListener;

class InquiryInformationFileImageRemover {

  public function __construct() {
    $this->inquiryFileImages = app()->make(\Odeo\Domains\Accounting\InquiryInformation\Repository\BankInquiryAdditionalInformationFileImageRepository::class);
  }

  public function remove(PipelineListener $listener, $data) {

    $deleted = $this->inquiryFileImages->getModel()
      ->where('id', $data['id'])
      ->delete();

    return $listener->response($deleted ? 200 : 400);
  }


}
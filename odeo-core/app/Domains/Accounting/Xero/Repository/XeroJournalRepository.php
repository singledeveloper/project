<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 18/09/18
 * Time: 16.30
 */

namespace Odeo\Domains\Accounting\Xero\Repository;


use Odeo\Domains\Accounting\Xero\Model\XeroJournal;
use Odeo\Domains\Core\Repository;

class XeroJournalRepository extends Repository {

  function __construct(XeroJournal $xeroJournal) {
    $this->setModel($xeroJournal);
  }

}
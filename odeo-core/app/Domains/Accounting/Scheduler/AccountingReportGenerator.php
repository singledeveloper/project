<?php

namespace Odeo\Domains\Accounting\Scheduler;

use Odeo\Domains\Accounting\Jobs\GenerateAccountingReport;

class AccountingReportGenerator {

  public function run() {
    dispatch(new GenerateAccountingReport);
  }
}

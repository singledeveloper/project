<?php

namespace Odeo\Domains\OAuth2\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\OAuth2\Model\OAuth2Token;

class OAuth2TokenRepository extends Repository
{

  public function __construct(OAuth2Token $token)
  {
    $this->model = $token;
  }

  public function findByUserId($userId) {
    return $this->model->where("user_id", $userId)->first();
  }

  public function findByCode($code) {
    return $this->model->where("code", $code)->first();
  }

  public function findByAccess($access) {
    return $this->model->where("access", $access)->first();
  }

  public function findByRefresh($refresh) {
    return $this->model->where("refresh", $refresh)->first();
  }

  public function removeByCode($code) {
    $this->model->where("code", $code)->delete();
  }

  public function removeByAccess($access) {
    $this->model->where("access", $access)->delete();
  }

  public function removeByRefresh($refresh) {
    $this->model->where("refresh", $refresh)->delete();
  }

}

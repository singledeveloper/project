<?php

namespace Odeo\Domains\Network\Commands;

use Illuminate\Console\Command;
use Odeo\Domains\Network\Helper\TreeManager;


class RefreshStoreReferredList extends Command {

  protected $signature = 'network:refresh-referred-list';

  protected $description = 'Refresh store network referred list';


  public function __construct() {
    parent::__construct();
  }


  public function handle() {
    (new TreeManager)->refreshReferredList();
  }
}

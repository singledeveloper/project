<?php

namespace Odeo\Domains\Network\Repository;

use Carbon\Carbon;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkReferralBonusDetail;

class ReferralDetailRepository extends Repository {

  public function __construct(NetworkReferralBonusDetail $ref) {
    $this->model = $ref;
  }
  
  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->first();
  }
  
  public function findByReferredStoreId($referredStoreIds) {
    $query = $this->model;
    if (is_array($referredStoreIds)) return $query->whereIn("referred_store_id", $referredStoreIds)->get();
    return $query->where("referred_store_id", $referredStoreIds)->get();
  }

  public function getReferralCount($day) {
    $date = Carbon::now()->subDays($day)->format('Y-m-d');

    return $this->model->where(\DB::raw('date(created_at)'), $date)
      ->select(\DB::raw('count(store_id) as referred_counts'), 'referred_store_id')
      ->groupBy('referred_store_id')->get();
  }
}

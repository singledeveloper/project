<?php

namespace Odeo\Domains\Network\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Network\Model\NetworkTreeChild;

class TreeChildRepository extends Repository {

  public function __construct(NetworkTreeChild $child) {
    $this->model = $child;
  }
  
  public function findByStoreId($storeId) {
    return $this->model->where("store_id", $storeId)->first();
  }
  
  public function findLike($pid) {
    return $this->model->where("child_data", "like", '%"' . $pid. '|%')
      ->orWhere("child_data", "like", '%,' . $pid. '|%')
      ->orWhere("store_id", $pid)->get();
  }
}

<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Transaction\Helper\Currency;

class TreeSelector implements SelectorListener {

  public function __construct(Currency $currency) {
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->commission = app()->make(\Odeo\Domains\Network\Repository\TeamCommissionRepository::class);
    $this->treeChildManager = app()->make(\Odeo\Domains\Network\Helper\ChildManager::class);
    $this->planManager = app()->make(\Odeo\Domains\Subscription\Helper\PlanManager::class);
    $this->referralManager = app()->make(\Odeo\Domains\Network\Helper\ReferralManager::class);
    $this->currency = $currency;
  }
  
  public function _transforms($item, Repository $repository) {
    return $item;
  }
  
  public function _extends($data, Repository $repository) {
    return $data;
  }
  
  public function getBranch(PipelineListener $listener, $data) {
    if ($network = $this->tree->findByStoreId($data["store_id"])) {
      return $listener->response(200, ["networks" => $this->treeChildManager->get_childs($data["store_id"], isset($data["max_depth"]) ? $data["max_depth"] : 2)]);
    }
    return $listener->response(204);
  }
  
  public function getInfo(PipelineListener $listener, $data) {
    if ($network = $this->tree->findByStoreId($data["store_id"])) {
      $this->tree->normalizeFilters($data);
      
      $store = $network->store;
      $rule = $store->plan;
      $r["store_name"] = $store->name;
      $r["plan_color"] = $rule->color;
      $r["can_assign"] = ($network->is_network_enable ? true : false);
  
      if ($this->tree->hasField("branch")) {
        $child = $this->treeChildManager->get_child_info($data["store_id"]);
        $r["left_branch"]["point"] = formatPoint($child->left_branch->point);
        $r["left_branch"]["count"] = $child->left_branch->count;
        $r["right_branch"]["point"] = formatPoint($child->right_branch->point);
        $r["right_branch"]["count"] = $child->right_branch->count;
      }
  
      if ($this->tree->hasField("team_commission")) {
        $total_tc = 0;
        if ($rule->max_pairing_potential > 0) {
          $team_commission = $this->commission->getTodayClaimed($data['store_id']);
          foreach ($team_commission as $item)
            $total_tc += ($item->first_amount + $item->second_amount);
            
          $tc_over = $total_tc > $rule->max_pairing_potential;
          $r["earning"] = [
            "is_active" => true,
            "current_value" => $this->currency->getFormattedOnly($total_tc, Currency::USD),
            "current_limit" => $this->currency->getFormattedOnly($rule->max_pairing_potential, Currency::USD),
            "overlimit" => $tc_over
          ];
        } else {
          $r["earning"] = ["is_active" => false];
        }
        $r["upgrade_suggestion"] = $this->planManager->getSuggestionPotential($total_tc, $rule->max_pairing_potential);
      }
  
      if ($this->tree->hasField("direct_referral")) {
        $r["direct_referral"] = $this->referralManager->getCurrentReferral($data['store_id'])[$data['store_id']];
      }
      
      return $listener->response(200, $r);
    }
    
    return $listener->response(204);
  }
  
  public function getInvitationInfo(PipelineListener $listener, $data) {
    if ($network = $this->tree->findByStoreId($data["store_id"])) {
      if ($network->referred_store_id != NULL) {
        return $listener->response(200, [
          "store_id" => $data["store_id"],
          "referral_code" => $network->referredStore->subdomain_name,
          "is_parent" => $this->tree->checkUpline($data["store_id"]) != null
        ]);
      }
    }
    return $listener->response(204);
  }
}

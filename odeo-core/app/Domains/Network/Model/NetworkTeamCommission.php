<?php

namespace Odeo\Domains\Network\Model;

use Odeo\Domains\Core\Entity;

class NetworkTeamCommission extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    protected $fillable = ['store_id', 'first_amount', 'second_amount', 'day_limit', 'created_at', 'updated_at', 'first_amount_claimed_at', 'second_amount_claimed_at'];
    
    public $timestamps = true;

}

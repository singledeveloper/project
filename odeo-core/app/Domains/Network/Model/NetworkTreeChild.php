<?php

namespace Odeo\Domains\Network\Model;

use Odeo\Domains\Core\Entity;

class NetworkTreeChild extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    public $timestamps = true;
}

<?php

namespace Odeo\Domains\Network;

use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;

class ReferralRequester {

  public function __construct() {
    $this->referral = app()->make(\Odeo\Domains\Network\Repository\ReferralRepository::class);
    $this->referralManager = app()->make(\Odeo\Domains\Network\Helper\ReferralManager::class);
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->referralDetail = app()->make(\Odeo\Domains\Network\Repository\ReferralDetailRepository::class);
    $this->store = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->inserter = app()->make(\Odeo\Domains\Transaction\Helper\CashInserter::class);
    $this->notification = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->revenue = app()->make(\Odeo\Domains\Transaction\Repository\CashRevenueRepository::class);
  }

  public function create(PipelineListener $listener, $data) {
    $store = $this->store->findById($data["store_id"]);

    if (isset($data["referred_store_id"])) $referredStore = $this->store->findById($data["referred_store_id"]);
    else $referredStore = $store->network->referredStore;

    if ($referredStore) {
      $ref = $this->referralDetail->getNew();
      $ref->referred_store_id = $referredStore->id;
      $ref->store_id = $store->id;
      $ref->plan_id = ($referredStore->plan_id > $store->plan_id ? $store->plan_id : $referredStore->plan_id);
      $this->referralDetail->save($ref);

      $owner = $this->store->findOwner($store->id);

      $this->inserter->add([
        'user_id' => $owner->user_id,
        'store_id' => $store->id,
        'trx_type' => TransactionType::INVITATION,
        'cash_type' => CashType::ODEPOSIT,
        'amount' => 200000,
        'data' => json_encode([
          'recent_desc' => $store->name
        ])
      ]);

      $this->revenue->save([
        'user_id' => $owner->user_id,
        'store_id' => $store->id,
        'order_id' => null,
        'amount' => 200000,
        'trx_type' => TransactionType::INVITATION,
        'data' => json_encode(['recent_desc' => $store->name])
      ]);

      $this->inserter->run();

      $this->notification->setup($owner->user_id, NotificationType::ALPHABET, NotificationGroup::ACCOUNT);
      $this->notification->invitation_new($store->id, $store->name);

      $listener->pushQueue($this->notification->queue());

      if (StoreStatus::isActive($referredStore->status)) {
        return $listener->response(201, ["referred_store_id" => $referredStore->id]);
      } else {
        return $listener->response(201);
      }
    }

    return $listener->response(200);
  }

  public function get(PipelineListener $listener, $data) {
    $this->tree->normalizeFilters($data);

    $networks = [];

    foreach($this->tree->getReferral() as $item) {
      $store["store_id"] = $item->store_id;
      $store["store_name"] = $item->name;

      $plan = $item->plan;

      $store["plan_name"] = $plan->name;
      $store["plan_color"] = $plan->color;
      $store["sales_volume"] = $plan->sales_volume;

      $created = new \DateTime($item->created_at);
      $now = new \DateTime();
      $interval = $now->diff($created);
      $store["age"] = $interval->format("%dd %hh %im");

      $networks[] = $store;
    }

    if (sizeof($networks) > 0)
      return $listener->response(200, array_merge(
        ["referrals" => $networks],
        $this->tree->getPagination()
      ));
    return $listener->response(204, ["referrals" => []]);
  }
}

<?php

namespace Odeo\Domains\Network\Helper;

class ChildManager {
  
  public function __construct() {
    $this->tree = app()->make(\Odeo\Domains\Network\Repository\TreeRepository::class);
    $this->treeChild = app()->make(\Odeo\Domains\Network\Repository\TreeChildRepository::class);
    $this->planManager = app()->make(\Odeo\Domains\Subscription\Helper\PlanManager::class);
  }
  
  private $branch_data = [];
  private $branch_points = 0;
  private $branch_store_counts = 0;
    
  private function recursive_calculate_branch($data, $store_id) {
    if (isset($data->$store_id)) {
      foreach ($data->$store_id as $item) {
        $this->branch_points += $item->sales_volume;
        $this->branch_store_counts++;
        $this->recursive_calculate_branch($data, $item->store_id);
      }
    }
  }
  
  public function get_child_info($store_id) {
    $data = $this->get_childs($store_id, 0);
    
    $info = [
      "left_branch" => ["point" => 0, "count" => 0],
      "right_branch" => ["point" => 0, "count" => 0]
    ];
    
    if (isset($data->$store_id)) {
      foreach ($data->$store_id as $item) {
        $this->recursive_calculate_branch($data, $item->store_id);
        $this->branch_points += $item->sales_volume;
        $this->branch_store_counts ++;
        if ($item->position == "LEFT") {
          $info["left_branch"]["point"] = $this->branch_points;
          $info["left_branch"]["count"] = $this->branch_store_counts;   
        }
        else if ($item->position == "RIGHT") {
          $info["right_branch"]["point"] = $this->branch_points;
          $info["right_branch"]["count"] = $this->branch_store_counts;
        }
        $this->branch_points = 0;
        $this->branch_store_counts = 0;
      }
    }
    
    return json_decode(json_encode($info));
  }
  
  private function recursive_get_child($store_id, $max_depth = 0, $level = 0) {
    if ($max_depth >= $level || $max_depth == 0) {
      $branch = $this->tree->findByParent($store_id, ['store.plan']);
      if (sizeof($branch) > 0) {
        $this->branch_data[$store_id][] = ["depth" => $level];
        foreach ($branch as $item) {
          $branch_data["s"] = $item->store_id;
          $branch_data["sv"] = $item->store->plan->id;
          $branch_data["p"] = !intval($item->position) ? "L" : "R";
          $this->branch_data[$store_id][] = $branch_data;
          $this->recursive_get_child($item->store_id, $max_depth, ($level+1));
        }
      }
    }
  }
  
  public function get_childs($store_id, $max_depth = 2) {
    if ($child = $this->treeChild->findByStoreId($store_id)) {
      $data = json_decode($child->child_data, true);
    }
    else {
      $this->recursive_get_child($store_id, $max_depth);
      $data = $this->branch_data;
      $this->branch_data = [];
      
      $change_data = [];
      foreach ($data as $key => $item) {
        foreach ($item as $output) {
          if (isset($output["depth"])) {
            $change_data[$key][] = "D" . $output["depth"];
          }
          else {
            $string = [];
            $string[] = $output["s"];
            $string[] = $output["sv"];
            $string[] = $output["p"];
            $change_data[$key][] = implode("|", $string);
          }
        }
      }
      
      foreach ($change_data as $key => $item) {
        $change_data[$key] = implode(",", $item);
      }
      $data = $change_data;
      
      if ($max_depth == "0") {
        $child = $this->treeChild->getNew();
        $child->store_id = $store_id;
        $child->child_data = json_encode($data);
        $this->treeChild->save($child);
      }
    }
    
    if ($max_depth != "0") {
      $splicer = [];
      foreach ($data as $key => $item) {
        $temp = explode(",", $item);
        $depth = str_replace("D", "", $temp[0]);
        if ($depth <= $max_depth) {
          $splicer[$key] = $item;
        }
      }
      $data = $splicer;
    }
    
    $retrieve = [];
    
    $plans = $this->planManager->getPlans();
    
    foreach ($data as $key => $item) {
      $temp = explode(",", $item);
      foreach ($temp as $output) {
        $temp_o = explode("|", $output);
        if ($temp_o[0][0] != "D") {
          $r["store_id"] = $temp_o[0];
          $r["plan_color"] = $plans[$temp_o[1]]->color;
          $r["sales_volume"] = $plans[$temp_o[1]]->sales_volume;
          $r["position"] = ($temp_o[2] == "L" ? "LEFT" : "RIGHT");
          $retrieve[$key][] = $r;
        }
      }
    }
    
    return json_decode(json_encode($retrieve));
  }
  
  public function get_deepest_child_id($store_id) {
    $childs = $this->get_childs($store_id, "0");
    if (sizeof($childs) > 0) {
      $last_child_loop = $store_id;
      while(true) {
        if (isset($childs->$last_child_loop)) {
          $current = $childs->$last_child_loop;
          $get_left = false;
          foreach ($current as $item) {
            if ($item->position == "LEFT") {
              $last_child_loop = $item->store_id;
              $get_left = true;
              break;
            }
          }
          if (!$get_left) {
            if (isset($current[0]) && isset($current[0]->store_id)) {
              $last_child_loop = $current[0]->store_id;
            }
            else return $last_child_loop;
          }
        }
        else return $last_child_loop;
      }
    }
    return $store_id;
  }
}

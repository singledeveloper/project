<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Odeo\Domains\Account\Helper\BankAccountInquirer;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementInquiryRepository;
use Odeo\Domains\Transaction\Helper\CashInserter;

class ApiDisbursementInquiryRequester {

  private $cashInserter, $apiDisbursementInquiryRepo, $selector, $bankAccountInquirer;

  public function __construct() {
    $this->selector = app()->make(ApiDisbursementInquirySelector::class);
    $this->cashInserter = app()->make(CashInserter::class);
    $this->apiDisbursementInquiryRepo = app()->make(ApiDisbursementInquiryRepository::class);
    $this->bankAccountInquirer = app()->make(BankAccountInquirer::class);
  }

  public function request(PipelineListener $listener, $data) {
    $this->apiDisbursementInquiryRepo->normalizeFilters($data);

    $userId = $data['auth']['user_id'];
    $apiUser = $data['auth']['api_user'];
    $fee = intval($apiUser->inquiry_fee ?? ApiDisbursement::INQUIRY_FEE);

    if (isset($data['with_validation']) && $data['with_validation']) {
      $fee += intval($apiUser->inquiry_validation_fee ?? ApiDisbursement::INQUIRY_VALIDATION_FEE);
    }

    $inquiry = $this->apiDisbursementInquiryRepo->getNew();
    $inquiry->user_id = $userId;
    $inquiry->bank_id = $data['bank_id'];
    $inquiry->fee = $fee;
    $inquiry->account_number = $data['account_number'];
    $inquiry->customer_name = $data['customer_name'];
    $inquiry->status = ApiDisbursement::FAILED;
    $inquiry->vendor = '';
    $this->apiDisbursementInquiryRepo->save($inquiry);

    list($bankInquiry, $inquiryStatus, $isCached) = $this->bankAccountInquirer->inquire($data['bank_id'], $data['account_number'], $userId, [
      'timeout' => 10,
      'user_telephone' => $apiUser->user->telephone,
      'customer_name' => $data['customer_name'],
      'sender_prefix' => $apiUser->alias,
      'reference_id' => $inquiry->id,
      'reference_type' => ArtajasaDisbursement::INQUIRY_REFERENCE_TYPE_API_DISBURSEMENT_INQUIRY,
    ]);

    if ($bankInquiry) {
      $inquiry->bank_account_inquiry_id = $bankInquiry->id;
      $inquiry->account_name = $bankInquiry->account_name;
      $inquiry->vendor = $isCached ? ApiDisbursement::CACHED_INQUIRY_RESULT : $bankInquiry->disbursement_vendor;
      $inquiry->cost = $isCached ? 0 : $bankInquiry->cost;
    }

    if (!$inquiry->vendor) {
      $inquiry->vendor = '';
    }

    $inquiry->status = BankAccountInquiryStatus::toApiDisbursementStatus($inquiryStatus);
    $costMoney = in_array($inquiry->status, ApiDisbursement::NON_FREE_INQUIRY_STATUS);

    if (!$costMoney) {
      $inquiry->fee = 0;
    }

    $this->apiDisbursementInquiryRepo->save($inquiry);

    if ($inquiry->fee != 0) {
      $this->cashInserter->add([
        'user_id' => $userId,
        'trx_type' => TransactionType::API_DISBURSEMENT_INQUIRY,
        'cash_type' => CashType::OCASH,
        'amount' => -$inquiry->fee,
        'data' => json_encode([
          'id' => $inquiry->id,
        ]),
        'reference_type' => TransactionType::API_DISBURSEMENT_INQUIRY,
        'reference_id' => $inquiry->id
      ]);

      $this->cashInserter->run();
    }

    return $listener->response(200, $this->selector->transforms($inquiry, $this->apiDisbursementInquiryRepo));
  }
}

<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bri;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BriDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Bri\Repository\DisbursementBriDisbursementRepository;

class RetryBriDisbursement {

  private $apiDisbursementRepo, $briDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->briDisbursementRepo = app()->make(DisbursementBriDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED) {
      return $listener->response(400, 'Disbursement status is completed');
    }

    if (!($briDisbursement = $this->briDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'Bri Disbursement not found');
    }

    if ($briDisbursement->status == BriDisbursement::SUCCESS) {
      return $listener->response(400, 'Bri Disbursement status is success');
    }

    $this->redis->hdel('odeo_core:bri_disbursement_lock', 'id_' . $briDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);

    $briDisbursement->error_description = null;
    $briDisbursement->status = null;
    $briDisbursement->response_datetime = null;
    $briDisbursement->response_code = null;
    $briDisbursement->response_description = null;
    $this->briDisbursementRepo->save($briDisbursement);

    if ($disbursement->status != ApiDisbursement::PENDING) {
      $disbursement->status = ApiDisbursement::PENDING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    $listener->pushQueue(new ExecuteDisbursement($disbursement->id));

    return $listener->response(200);
  }

}

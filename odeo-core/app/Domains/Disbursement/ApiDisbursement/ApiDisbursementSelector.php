<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Illuminate\Support\Arr;
use Odeo\Domains\Account\Repository\UserRepository;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementInvoice;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementReport;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Transaction\Helper\CashManager;
use Odeo\Domains\Transaction\Helper\Currency;

class ApiDisbursementSelector {

  private $apiDisbursementRepo;
  private $cashManager;
  private $currency;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->cashManager = app()->make(CashManager::class);
    $this->currency = app()->make(Currency::class);
  }

  public function transforms($item, Repository $repository) {
    $status = $item->status;

    switch ($status) {
      case ApiDisbursement::PENDING_NOT_ENOUGH_DEPOSIT:
        $status = ApiDisbursement::PENDING;
        break;

      case ApiDisbursement::FAIL_PROCESSING:
      case ApiDisbursement::ON_PROGRESS_WAIT_FOR_NOTIFY:
        $status = ApiDisbursement::ON_PROGRESS;
        break;

      case ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS:
        $status = ApiDisbursement::SUSPECT;
        break;
    }

    $result = [
      'id' => +$item->id,
      'bank_id' => +$item->bank_id,
      'account_number' => $item->account_number,
      'account_name' => $item->account_name,
      'customer_name' => $item->customer_name ?? "",
      'customer_email' => $item->customer_email,
      'amount' => +$item->amount,
      'fee' => +$item->fee,
      'description' => $item->description,
      'reference_id' => $item->reference_id,
      'status' => $status,
      'message' => ApiDisbursement::getStatusMessage($status),
      'created_at' => $item->created_at->timestamp,
      'created_at_date' => $item->created_at->format('Y-m-d H:i:s')
    ];

    if ($repository->hasExpand('bank')) {
      $result['bank'] = [
        'name' => $item->bank->name,
      ];
    }

    return $result;
  }

  public function findById(PipelineListener $listener, $data) {
    $this->apiDisbursementRepo->normalizeFilters($data);
    $disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']);

    //if ($disbursement == null || ($data['auth']['type'] == UserType::DISBURSEMENT && $disbursement->user_id != $data['auth']['user_id'])) {
    if ($disbursement == null || (!isAdmin($data) && $disbursement->user_id != $data['auth']['user_id'])) {
      return $listener->response(204, null, true, true);
    }

    return $listener->response(200, $this->transforms($disbursement, $this->apiDisbursementRepo));
  }

  public function findByReferenceId(PipelineListener $listener, $data) {
    $this->apiDisbursementRepo->normalizeFilters($data);
    $disbursement = $this->apiDisbursementRepo->findByUserIdAndReferenceId($data['auth']['user_id'], $data['reference_id']);

    if ($disbursement == null) {
      return $listener->response(204, null, true, true);
    }

    return $listener->response(200, $this->transforms($disbursement, $this->apiDisbursementRepo));
  }

  public function get(PipelineListener $listener, $data) {
    $this->apiDisbursementRepo->normalizeFilters($data);
    $disbursements = $this->apiDisbursementRepo
      ->getAllByUserId($data['auth']['user_id'])
      ->map(function ($item) {
        return $this->transforms($item, $this->apiDisbursementRepo);
      });

    return $listener->response(
      sizeof($disbursements) > 0 ? 200 : 204,
      array_merge(
        ['disbursements' => $disbursements],
        Arr::get($data, 'hide_metadata', false) ? [] : $this->apiDisbursementRepo->getPagination()
      ));
  }

  public function listByBatchDisbursementId(PipelineListener $listener, $data) {
    $this->apiDisbursementRepo->normalizeFilters($data);
    $disbursements = $this->apiDisbursementRepo
      ->listByBatchDisbursementId($data['batch_disbursement_id'])
      ->map(function ($item) {
        return $this->transforms($item, $this->apiDisbursementRepo);
      });

    return $listener->response(
      sizeof($disbursements) > 0 ? 200 : 204,
      array_merge(
        ['disbursements' => $disbursements],
        $this->apiDisbursementRepo->getPagination()
      ));
  }

  public function export(PipelineListener $listener, $data) {
    $filters = [
      'start_date' => $data['start_date'],
      'end_date' => $data['end_date'],
      'file_type' => $data['file_type'] ?? 'csv',
      'user_id' => getUserId()
    ];

    $user = app()->make(UserRepository::class)->findById(getUserId());

    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }

    if (!$user->is_email_verified) {
      return $listener->response(400, 'Your email is not verified');
    }

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue( new SendDisbursementReport($filters, $user->email, $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }

  public function exportInvoice(PipelineListener $listener, $data) {

    $user = app()->make(UserRepository::class)->findById(getUserId());

    if (!$user->email) {
      return $listener->response(400, trans('user.email_not_set'));
    }

    if (!$user->is_email_verified) {
      return $listener->response(400, 'Your email is not verified');
    }

    $exportId = app()->make(\Odeo\Domains\Transaction\Helper\ExportDownloader::class)->create($data);

    $listener->pushQueue( new SendDisbursementInvoice($user->id, $data['periode'], $exportId));

    return $listener->response(200, [
      'export_id' => $exportId
    ]);
  }
}
<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\BatchDisbursementRepository;

class BatchDisbursementDeleter {

  private $batchDisbursementRepo, $apiDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->batchDisbursementRepo = app()->make(BatchDisbursementRepository::class);
  }

  public function deleteDetail(PipelineListener $listener, $data) {
    $apiDisbursement = $data['api_disbursement'];

    $res = $this->apiDisbursementRepo->sumAndCountByBatchDisbursementId($data['batch_disbursement_id']);
    if ($res->count == 1) {
      return $listener->response(400, trans('disbursement.cant_delete_the_only_disbursement'));
    }

    if (in_array($apiDisbursement->status, ApiDisbursement::COMPLETED_STATUS_LIST)) {
      return $listener->response(400, trans('errors.invalid_request'));
    }

    $apiDisbursement->delete();

    $batch = $this->batchDisbursementRepo->findById($data['batch_disbursement_id']);
    $batch->count = $res->count - 1;
    $batch->total_fee = $res->sum_fee - $apiDisbursement->fee;
    $batch->total_amount = $res->sum - $apiDisbursement->amount;
    $batch->save();

    return $listener->response(200);
  }

}
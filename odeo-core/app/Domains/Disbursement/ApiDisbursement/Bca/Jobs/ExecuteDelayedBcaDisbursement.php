<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bca\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;
use Odeo\Domains\Vendor\Bca\Transfer;
use Odeo\Jobs\Job;

class ExecuteDelayedBcaDisbursement extends Job implements ShouldQueue {

  private $disbursementId, $apiDisbursementRepo, $redis, $vendorDisbursementRepo, $bcaDisbursementRepo;

  public function __construct($disbursementId) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle() {
    $this->redis = Redis::connection();
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);

    $disbursement = $this->apiDisbursementRepo->findById($this->disbursementId);

    if (!$disbursement) {
      return;
    }

    if ($disbursement->status != ApiDisbursement::COMPLETED || $disbursement->delayed_at == null) {
      return;
    }

    if (!($bcaDisbursement = $this->bcaDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return;
    }

    if ($bcaDisbursement->status == BcaDisbursement::SUCCESS) {
      return;
    }

    if (!$this->hasEnoughDeposit($disbursement)) {
      return;
    }

    if (!$this->lock($disbursement->id)) {
      return;
    }

    $remark = "WDOCASH 2{$disbursement->id}";

    $pipeline = new Pipeline();
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'bca_disbursement_id' => $disbursement->auto_disbursement_reference_id,
      'corporate_id' => ApiManager::$CORPORATEID,
      'remark_1' => substr($remark, 0, 18),
      'remark_2' => strlen($remark > 18) ? substr($remark, 18, 18) : '',
      'transfer_from' => ApiManager::$ACCOUNTNUMBER[0],
      'transfer_to' => $disbursement->account_number,
      'transaction_key' => 'DISB#' . $disbursement->id,
    ]));

    $pipeline->execute();

    if (!$pipeline->success()) {
      $this->unlock($disbursement->id);
      return;
    }

    $disbursement->delayed_at = null;
    $this->apiDisbursementRepo->save($disbursement);
  }

  private function lock($id) {
    return $this->redis->hsetnx('odeo_core:delayed_api_disbursement_lock', 'id_' . $id, 1);
  }

  private function unlock($id) {
    return $this->redis->hdel('odeo_core:delayed_api_disbursement_lock', 'id_' . $id, 1);
  }

  private function hasEnoughDeposit($disbursement) {
    $lastInquiry = app()
      ->make(BankBcaInquiryRepository::class)
      ->getLastRecordedBalanceByAccountNumber(ApiManager::$ACCOUNTNUMBER[0]);
    $balance = $lastInquiry ? $lastInquiry->balance : 0;
    return $balance > $disbursement->amount;
  }
}

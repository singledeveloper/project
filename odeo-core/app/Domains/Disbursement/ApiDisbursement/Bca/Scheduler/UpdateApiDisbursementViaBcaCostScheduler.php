<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bca\Scheduler;

use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class UpdateApiDisbursementViaBcaCostScheduler {

  private $apiDisbursementRepo, $bcaDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
  }

  public function run() {
    $period = date("Y-m", time() - 12 * 60 * 60);
    $date = date("Y-m-d", time() - 12 * 60 * 60);
    $res = $this->bcaDisbursementRepo
      ->getNthDisbursementReferenceId($period, Disbursement::API_DISBURSEMENT, BcaDisbursement::TRANSFER_API_LIMIT);

    if ($res == null) {
      return;
    }

    $this->apiDisbursementRepo->updateCost(ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT, $date,
      $res->disbursement_reference_id, BcaDisbursement::TRANSFER_EXCEEDED_COST);
  }
}

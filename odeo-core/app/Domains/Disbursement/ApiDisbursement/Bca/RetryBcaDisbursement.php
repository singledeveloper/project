<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Bca;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\Jobs\ExecuteDelayedBcaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\ExecuteDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class RetryBcaDisbursement {

  private $apiDisbursementRepo, $bcaDisbursementRepo, $redis;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function retry(PipelineListener $listener, $data) {
    if (!($disbursement = $this->apiDisbursementRepo->findById($data['disbursement_id']))) {
      return $listener->response(400, 'Disbursement not found');
    }

    if ($disbursement->status == ApiDisbursement::COMPLETED && $disbursement->delayed_at == null) {
      return $listener->response(400, 'Disbursement status is completed');
    }

    if (!($bcaDisbursement = $this->bcaDisbursementRepo->findById($disbursement->auto_disbursement_reference_id))) {
      return $listener->response(400, 'BCA Disbursement not found');
    }

    if ($bcaDisbursement->status == BcaDisbursement::SUCCESS) {
      return $listener->response(400, 'BCA Disbursement status is completed');
    }

    $this->redis->hdel('odeo_core:bca_disbursement_lock', 'id_' . $bcaDisbursement->id);
    $this->redis->hdel('odeo_core:api_disbursement_lock', 'id_' . $data['disbursement_id']);
    $this->redis->hdel('odeo_core:delayed_api_disbursement_lock', 'id_' . $data['disbursement_id']);

    if (strlen($bcaDisbursement->transaction_key) || strlen($bcaDisbursement->bca_status) || strlen($bcaDisbursement->response_datetime)) {
      $bcaDisbursement->transaction_key = null;
      $bcaDisbursement->bca_status = null;
      $bcaDisbursement->response_datetime = null;
      $this->bcaDisbursementRepo->save($bcaDisbursement);
    }

    if ($disbursement->status != ApiDisbursement::PENDING && $disbursement->delayed_at == null) {
      $disbursement->status = ApiDisbursement::PENDING;
      $this->apiDisbursementRepo->save($disbursement);
    }

    if ($disbursement->delayed_at == null || $data['preferred_vendor'] != null) {
      $listener->pushQueue(new ExecuteDisbursement($disbursement->id));
    }
    else {
      $listener->pushQueue(new ExecuteDelayedBcaDisbursement($disbursement->id));
    }

    return $listener->response(200);
  }

}

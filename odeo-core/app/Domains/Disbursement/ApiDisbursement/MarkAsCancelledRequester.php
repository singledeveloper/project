<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;


class MarkAsCancelledRequester {

  public function markAsCancel(PipelineListener $listener, $data) {
    if ($data['password'] != env('API_DISBURSEMENT_MARK_AS_CANCELLED_PASSWORD')) {
      return $listener->response(400, 'Wrong password');
    }

    $listener->addNext(new Task(ApiDisbursementCanceller::class, 'cancel'));

    return $listener->response(200);
  }

}
<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;


class MarkAsSuccessRequester {

  private $apiDisbursementRepo;

  public function __construct() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function markAsSuccess(PipelineListener $listener, $data) {
    if ($data['password'] != env('API_DISBURSEMENT_MARK_AS_SUCCESS_PASSWORD')) {
      return $listener->response(400, trans('errors.invalid_password'));
    }

    $listener->addNext(new Task(ApiDisbursementRequester::class, 'complete'));

    return $listener->response(200);
  }

}
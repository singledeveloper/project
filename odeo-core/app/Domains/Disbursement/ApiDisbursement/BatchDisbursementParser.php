<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Box\Spout\Reader\ReaderFactory;
use Illuminate\Support\Arr;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\BatchDisbursementTemplate;
use Odeo\Domains\Core\PipelineListener;

class BatchDisbursementParser {

  private $bankRepo;

  public function __construct() {
    $this->bankRepo = app()->make(BankRepository::class);
  }

  public function parse(PipelineListener $listener, $data) {
    $reader = ReaderFactory::create($data['file']->getClientOriginalExtension());
    $reader->open($data['file']->path());

    $data = [];
    $templateSheetFound = false;

    foreach ($reader->getSheetIterator() as $sheet) {
      switch (strtolower($sheet->getName())) {
        case BatchDisbursementTemplate::SHEET_INFORMATION:
          if (!$this->correctVersion($sheet)) {
            return $listener->response(400, trans('disbursement.template_outdated', [
              'version' => BatchDisbursementTemplate::VERSION]));
          }
          break;
        case BatchDisbursementTemplate::SHEET_TEMPLATE:
          $templateSheetFound = true;
          $data = $this->parseTemplateSheet($sheet);
          break;
      }
    }

    if (!$templateSheetFound) {
      return $listener->response(400, trans('disbursement.template_outdated', [
        'version' => BatchDisbursementTemplate::VERSION]));
    }

    return $listener->response(200, [
      'rows' => $data,
    ]);
  }

  private function correctVersion($sheet) {
    foreach ($sheet->getRowIterator() as $row) {
      return strtolower($row[0]) == strtolower(BatchDisbursementTemplate::VERSION);
    }
  }

  private function parseTemplateSheet($sheet) {
    $data = [];
    $fields = [
      'bank_code',
      'account_number',
      'customer_name',
      'amount',
      'description',
      'reference_id',
    ];
    $validBankCodes = $this->bankRepo
      ->getATMBersamaBankList()
      ->mapWithKeys(function ($item) {
        return [$item->aj_bank_code => $item->id];
      });

    $firstRow = true;
    foreach ($sheet->getRowIterator() as $row) {
      if ($firstRow) {
        $firstRow = false;
        continue;
      }

      while (count($row) < count($fields)) {
        $row[] = '';
      }

      $row = array_map('trim', $row);
      $item = array_combine($fields, $row);

      if (implode('', $row) == '') {
        continue;
      }

      if ($item['reference_id'] == '') {
        $item['reference_id'] = null;
      }

      if (strlen($item['bank_code']) < 3) {
        $item['bank_code'] = str_pad($item['bank_code'], 3, '0', STR_PAD_LEFT);
      }

      $item['bank_id'] = Arr::get($validBankCodes, $item['bank_code'], null);
      unset($item['bank_code']);
      $data[] = $item;
    }

    return $data;
  }

}
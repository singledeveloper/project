<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 31/10/19
 * Time: 15.37
 */

namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\Artajasa\RetryArtajasaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bca\RetryBcaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bni\RetryBniDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Bri\RetryBriDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Cimb\RetryCimbDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Mandiri\RetryMandiriDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Permata\RetryPermataDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Redig\RetryRedigDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Jobs\Job;

class ExecuteRetryDisbursement extends Job {

  private $disbursementId, $preferredVendor;
  private $apiDisbursementRepo, $redis;

  public function __construct($disbursementId, $preferredVendor = null) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
    $this->preferredVendor = $preferredVendor;
  }

  private function init() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {
    $this->init();
    $disbursement = $this->apiDisbursementRepo->findById($this->disbursementId);

    if (!$disbursement) {
      return;
    }
    if ($disbursement->status == ApiDisbursement::COMPLETED && $disbursement->delayed_at == null) {
      return;
    }

    if ($disbursement->status == ApiDisbursement::SUSPECT_EVENTUALLY_SUCCESS) {
      return;
    }

    if (!$this->lockRetry($disbursement->id)) {
      return;
    }

    if ($this->preferredVendor != null || $this->preferredVendor != '') {
      $this->redis->hset('odeo_core:api_disbursement_preference', $this->disbursementId, $this->preferredVendor);
    } else {
      $this->redis->hdel('odeo_core:api_disbursement_preference', $this->disbursementId);
    }

    $pipeline = new Pipeline;

    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_ARTAJASA_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryArtajasaDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryBcaDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryBniDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryBriDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_CIMB_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryCimbDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_MANDIRI_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryMandiriDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryPermataDisbursement::class, 'retry'));
        break;

      case ApiDisbursement::DISBURSEMENT_VENDOR_REDIG_API_DISBURSEMENT:
        $pipeline->add(new Task(RetryRedigDisbursement::class, 'retry'));
        break;

      default:
        return;
    }

    $pipeline->execute([
      'disbursement_id' => $this->disbursementId,
      'preferred_vendor' => $this->preferredVendor
    ]);
  }

  private function lockRetry($id) {
    $key = 'odeo_core:disbursement_retry_lock_' . $id;
    if (!$this->redis->setnx($key, 1)) {
      return false;
    }
    $this->redis->expire($key, 5 * 60);
    return true;
  }

}
<?php


namespace Odeo\Domains\Disbursement\ApiDisbursement\Jobs;


use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Jobs\Job;

class ValidateAccountNameByBatchDisbursementId extends Job {

  private $batchDisbursementId;
  private $apiDisbursementRepo;

  public function __construct($batchDisbursementId) {
    parent::__construct();
    $this->batchDisbursementId = $batchDisbursementId;
  }

  public function handle() {
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
    $disbs = $this->apiDisbursementRepo->listByBatchDisbursementId($this->batchDisbursementId);

    foreach ($disbs as $disb) {
      dispatch(new ValidateAccountName($disb->id));
    }
  }
}
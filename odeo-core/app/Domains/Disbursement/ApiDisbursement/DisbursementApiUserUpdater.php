<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Helper\DisbursementApiHelper;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class DisbursementApiUserUpdater {

  private $disbursementApiUserRepo;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
  }

  public function createDisbursementApiUser(PipelineListener $listener, $data) {

    $userId = $data['auth']['user_id'];

    if ($disbursementUser = $this->disbursementApiUserRepo->findByUserId($userId)) {
      return $listener->response(409, 'Already Registered');
    }

    $disbursementUser = $this->disbursementApiUserRepo->getNew();
    $disbursementUser->user_id = $userId;
    $disbursementUser->access_key_id = '';
    $disbursementUser->secret_access_key = '';
    $disbursementUser->whitelist = '';
    $disbursementUser->notify_url = '';
    $disbursementUser->whitelist = '';
    $disbursementUser->notify_url = '';
    $disbursementUser->alias = '';
    $disbursementUser->signing_key = $data['signing_key'];
    $disbursementUser->fee = DisbursementApiHelper::DISBURSEMENT_FEE;
    $disbursementUser->inquiry_fee = DisbursementApiHelper::DISB_INQUIRY_FEE;
    $disbursementUser->inquiry_validation_fee = DisbursementApiHelper::DISB_INQUIRY_VALIDATION_FEE;

    $this->disbursementApiUserRepo->save($disbursementUser);

    return $listener->response(201, [
      'fee' => $disbursementUser->fee,
      'notify_url' => $disbursementUser->notify_url,
      'inquiry_fee' => $disbursementUser->inquiry_fee
    ]);
  }

  public function updateWhitelist(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id']);
    if (!$apiUser) {
      return $listener->response(400, 'No access');
    }

    $apiUser->whitelist = json_encode($data['whitelist']);
    $apiUser->save();

    return $listener->response(200);
  }

  public function updateCallback(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id']);
    if (!$apiUser) {
      return $listener->response(400, 'No access');
    }

    $apiUser->notify_url = $data['url'];
    $apiUser->save();

    return $listener->response(200);
  }

  public function updateEmail(PipelineListener $listener, $data) { // TEMPORARY TO TABLE USERS
    $userRepo = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);

    if (!$user = $userRepo->findById($data['auth']['user_id'])) {
      return $listener->response(400, 'No access');
    }

    $user->email = strtolower($data['email']);
    $user->is_email_verified = true;
    $userRepo->save($user);

    return $listener->response(200);
  }

  public function generateSecretKey(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id']);
    if (!$apiUser) {
      return $listener->response(400, 'No access');
    }

    $secretKey = randomStr(64);
    $apiUser->secret_access_key = encrypt($secretKey);
    $apiUser->save();

    return $listener->response(200, [
      'secret_key' => $secretKey,
    ]);
  }

  public function generateSigningKey(PipelineListener $listener, $data) {
    $apiUser = $this->disbursementApiUserRepo->findByUserId($data['auth']['user_id']);
    if (!$apiUser) {
      return $listener->response(400, 'No access');
    }

    $signingKey = randomStr(64);
    $apiUser->signing_key = $signingKey;
    $apiUser->save();

    return $listener->response(200, [
      'signing_key' => $signingKey,
    ]);
  }

  public function updateSigningKey(PipelineListener $listener, $data) {

    if ($apiUser = $this->disbursementApiUserRepo->findByUserId($data['user_id'])) {
      $apiUser->signing_key = $data['signing_key'];
      $this->disbursementApiUserRepo->save($apiUser);
    }

    return $listener->response(200);
  }
}
<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Scheduler;

use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDelayedDisbursementSuspectAlert;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;

class DelayedApiDisbursementNotRunningAlertScheduler {

  private $disbursementRepo;

  public function __construct() {
    $this->disbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function run() {
    $disbursementIdList = $this->disbursementRepo
      ->getDelayedList()
      ->filter(function ($disbursement) {
        return $this->shouldBeRunning($disbursement);
      })
      ->pluck('id')
      ->toArray();

    if (!empty($disbursementIdList)) {
      dispatch(new SendDelayedDisbursementSuspectAlert($disbursementIdList));
    }
  }

  private function shouldBeRunning($disbursement) {
    switch ($disbursement->auto_disbursement_vendor) {
      case ApiDisbursement::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
        return BcaDisbursement::isOperational() && Carbon::now()->minute >= 10;

      default:
        return true;
    }
  }
}
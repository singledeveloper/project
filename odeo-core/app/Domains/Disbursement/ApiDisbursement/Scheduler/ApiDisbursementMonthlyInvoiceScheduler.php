<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement\Scheduler;

use Odeo\Domains\Disbursement\ApiDisbursement\Jobs\SendDisbursementInvoice;
use Odeo\Domains\Disbursement\Repository\DisbursementApiUserRepository;

class ApiDisbursementMonthlyInvoiceScheduler {

  private $disbursementApiUserRepo;

  public function __construct() {
    $this->disbursementApiUserRepo = app()->make(DisbursementApiUserRepository::class);
  }

  public function run() {
    $users = $this->disbursementApiUserRepo->getAll();
    $period = date("Y-m", time() - 15 * 24 * 60 * 60);

    foreach ($users as $user) {
      dispatch(new SendDisbursementInvoice($user->user_id, $period));
    }
  }
}

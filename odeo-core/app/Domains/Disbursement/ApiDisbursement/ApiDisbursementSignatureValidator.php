<?php

namespace Odeo\Domains\Disbursement\ApiDisbursement;


use Illuminate\Http\Request;


use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\Helper\DisbursementApiHelper;

class ApiDisbursementSignatureValidator {

  private $disbursementApiHelper;

  /**
   * @var Request
   */
  private $request;

  public function __construct(Request $request) {
    $this->request = $request;
    $this->disbursementApiHelper = app()->make(DisbursementApiHelper::class);
  }

  public function validate(PipelineListener $listener, $data) {
    $apiUser = $data['auth']['api_user'];

    list($isValid, $errorCode) = $this->disbursementApiHelper->isValid(
      $data['signature_components'],
      $data['api_data']['timestamp'],
      $data['api_data']['signature'],
      decrypt($apiUser->secret_access_key));

    if (!$isValid) {
      return $listener->response(401, [
        'error_code' => $errorCode,
        'message' => ApiDisbursement::getErrorMessage($errorCode),
      ]);
    }

    return $listener->response(200);
  }
}
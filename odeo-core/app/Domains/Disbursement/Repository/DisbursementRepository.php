<?php


namespace Odeo\Domains\Disbursement\Repository;


use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\Model\Disbursement;

class DisbursementRepository extends Repository {


  public function __construct(Disbursement $disbursement) {
    $this->setModel($disbursement);
  }
}
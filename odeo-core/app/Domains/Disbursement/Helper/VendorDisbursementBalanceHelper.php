<?php

namespace Odeo\Domains\Disbursement\Helper;

use Carbon\Carbon;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Repository\ApiDisbursementRepository;
use Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Bca\Repository\BankBcaInquiryRepository;
use Odeo\Domains\Vendor\Bca\Helper\ApiManager;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class VendorDisbursementBalanceHelper {

  private $bankBcaInquiryRepo, $vendorDisbursementRepo, $bcaDisbursementRepo, $apiDisbursementRepo;

  public function __construct() {
    $this->bankBcaInquiryRepo = app()->make(BankBcaInquiryRepository::class);
    $this->vendorDisbursementRepo = app()->make(VendorDisbursementRepository::class);
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
    $this->apiDisbursementRepo = app()->make(ApiDisbursementRepository::class);
  }

  public function getBalance($vendor) {
    if ($vendor == VendorDisbursement::BCA) {
      $lastInquiry = $this->bankBcaInquiryRepo->getLastRecordedBalanceByAccountNumber(ApiManager::$ACCOUNTNUMBER[0]);
      $balance = $lastInquiry ? $lastInquiry->balance : 0;
      $lastInquiryDate = $lastInquiry ? $lastInquiry->last_scrapped_date : Carbon::now();
    }
    else {
      $vendorBalance = $this->vendorDisbursementRepo->findById($vendor);
      $balance = $vendorBalance ? $vendorBalance->balance : 0;
      $lastInquiryDate = $vendorBalance ? $vendorBalance->updated_at : Carbon::now();
    }

    switch ($vendor) {
      case VendorDisbursement::BCA:
        $pendingAmount = $this->bcaDisbursementRepo
          ->getPendingAmount(Disbursement::API_DISBURSEMENT, $lastInquiryDate);
        $amount = $pendingAmount ? $pendingAmount->amount : 0;
        $balance = $balance - $amount;
        break;
      case VendorDisbursement::BNI:
        $pendingAmount = $this->apiDisbursementRepo
          ->getPendingAmount(ApiDisbursement::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT, $lastInquiryDate);
        $amount = $pendingAmount ? $pendingAmount->amount : 0;
        $balance = $balance - $amount;
        break;
    }

    return $balance;
  }

}
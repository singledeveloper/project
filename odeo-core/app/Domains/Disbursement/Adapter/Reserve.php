<?php


namespace Odeo\Domains\Disbursement\Adapter;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Repository\DisbursementRepository;

class Reserve {

  /**
   * @var DisbursementRepository
   */
  private $disbursementRepo;

  public function __construct() {
    $this->disbursementRepo = app()->make(DisbursementRepository::class);
  }

  public function reserve(PipelineListener $listener, $data) {
    $disbursement = $this->disbursementRepo->findById($data['disbursement_id']);
    if (!$disbursement) {
      return $listener->response(400, 'disbursement not found');
    }
    
    switch ($disbursement->disbursement_vendor) {
      case VendorDisbursement::ARTAJASA:
        $listener->addNext(new Task(Artajasa\Reserve::class, 'reserve'));
        break;

      case VendorDisbursement::BCA:
        $listener->addNext(new Task(Bca\Reserve::class, 'reserve'));
        break;

      case VendorDisbursement::BNI:
        $listener->addNext(new Task(Bni\Reserve::class, 'reserve'));
        break;

      case VendorDisbursement::CIMB:
        $listener->addNext(new Task(Cimb\Reserve::class, 'reserve'));
        break;

      case VendorDisbursement::MANDIRI:
        $listener->addNext(new Task(Mandiri\Reserve::class, 'reserve'));
        break;

      default:
        return $listener->response(400, "vendor {$disbursement->disbursement_vendor} not found");
    }

    return $listener->response(200);
  }
}
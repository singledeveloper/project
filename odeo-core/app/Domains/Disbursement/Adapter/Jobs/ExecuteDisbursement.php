<?php

namespace Odeo\Domains\Disbursement\Adapter\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\DisbursementStatus;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Adapater\Bni\Helper\ExecuteBniDisbursement;
use Odeo\Domains\Disbursement\Adapter\Artajasa\Helper\ExecuteArtajasaDisbursement;
use Odeo\Domains\Disbursement\Adapter\Bca\Helper\ExecuteBcaDisbursement;
use Odeo\Domains\Disbursement\Adapter\Cimb\Helper\ExecuteCimbDisbursement;
use Odeo\Domains\Disbursement\Adapter\Mandiri\Helper\ExecuteMandiriDisbursement;
use Odeo\Domains\Disbursement\Repository\DisbursementRepository;
use Odeo\Jobs\Job;

class ExecuteDisbursement extends Job implements ShouldQueue {

  private $disbursementId, $disbursementRepo, $redis;

  public function __construct($disbursementId) {
    parent::__construct();
    $this->disbursementId = $disbursementId;
  }

  public function handle() {
    $this->disbursementRepo = app()->make(DisbursementRepository::class);
    $this->redis = Redis::connection();

    $disbursement = $this->disbursementRepo->findById($this->disbursementId);

    if (!$disbursement) {
      return;
    }

    if (!$this->lock($disbursement->id)) {
      return;
    }

    if ($disbursement->status != DisbursementStatus::ON_PROGRESS) {
      return;
    }

    switch ($disbursement->disbursement_vendor) {
      case VendorDisbursement::ARTAJASA:
        $result = app()->make(ExecuteArtajasaDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::BCA:
        $result = app()->make(ExecuteBcaDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::CIMB:
        $result = app()->make(ExecuteCimbDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::BNI:
        $result = app()->make(ExecuteBniDisbursement::class)->exec($disbursement);
        break;

      case VendorDisbursement::MANDIRI:
        $result = app()->make(ExecuteMandiriDisbursement::class)->exec($disbursement);
        break;

      default:
        return;
    }

    dispatch(new NotifyResult($result));

    $this->unlock($disbursement->id);
  }

  private function lock($id) {
    return $this->redis->hsetnx('odeo_core:disbursement_lock', 'id_' . $id, 1);
  }

  private function unlock($id) {
    return $this->redis->hdel('odeo_core:disbursement_lock', 'id_' . $id);
  }

}

<?php

namespace Odeo\Domains\Disbursement\Adapater\Bni\Helper;

use Odeo\Domains\Constant\BankAccountInquiryStatus;
use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Constant\DisbursementStatus;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Adapter\Helper\DisbursementInquirer;
use Odeo\Domains\Vendor\Bni\Helper\ApiManager;
use Odeo\Domains\Vendor\Bni\Transfer;
use Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository;

class ExecuteBniDisbursement {

  private $bniDisbursementRepo, $disbursementInquirer;

  public function __construct() {
    $this->bniDisbursementRepo = app()->make(DisbursementBniDisbursementRepository::class);
    $this->disbursementInquirer = app()->make(DisbursementInquirer::class);
  }

  public function exec($disbursement) {
    $inquiryStatus = $this->disbursementInquirer->inquire($disbursement);
    if ($inquiryStatus != BankAccountInquiryStatus::COMPLETED) {
      $bni = $this->bniDisbursementRepo->findById($disbursement->disbursement_vendor_reference_id);
      $bni->status = BniDisbursement::FAIL;
      $bni->response_code = BniDisbursement::RESPONSE_CODE_INQUIRY_FAILED;
      $this->bniDisbursementRepo->save($bni);

      return [
        'disbursement_id' => $disbursement->id,
        'status' => BankAccountInquiryStatus::toDisbursementStatus($inquiryStatus),
        'disbursement_vendor' => VendorDisbursement::BNI,
        'disbursement_vendor_reference_id' => $bni->id,
        'disbursement_vendor_updated_at' => strtotime($bni->updated_at),
      ];
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'amount' => $disbursement->amount,
      'bni_disbursement_id' => $disbursement->disbursement_vendor_reference_id,
      'remark' => 'WDOCASH 3' . $disbursement->id,
      'transfer_from' => ApiManager::getConfig('account_number'),
      'transfer_to' => $disbursement->account_number,
    ]));

    $pipeline->execute();
    $bni = $this->bniDisbursementRepo->findById($disbursement->disbursement_vendor_reference_id);

    return [
      'disbursement_id' => $disbursement->id,
      'status' => intval($this->getDisbursementStatus($pipeline)),
      'disbursement_vendor' => VendorDisbursement::BNI,
      'disbursement_vendor_reference_id' => $bni->id,
      'disbursement_vendor_updated_at' => strtotime($bni->updated_at),
    ];
  }

  private function getDisbursementStatus(Pipeline $pipeline) {
    if ($pipeline->success()) {
      return DisbursementStatus::COMPLETED;
    }

    if (isset($pipeline->errorMessage['response_code']) && $pipeline->errorMessage['response_code'] == '0110') {
      return DisbursementStatus::FAILED_CLOSED_BANK_ACCOUNT;
    }

    return DisbursementStatus::SUSPECT;
  }
}
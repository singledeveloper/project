<?php

namespace Odeo\Domains\Disbursement\Adapter\Bca;


use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Vendor\Bca\Repository\DisbursementBcaDisbursementRepository;

class Reserve {

  private $bcaDisbursementRepo;

  public function __construct() {
    $this->bcaDisbursementRepo = app()->make(DisbursementBcaDisbursementRepository::class);
  }

  public function reserve(PipelineListener $listener, $data) {
    $bca = $this->bcaDisbursementRepo->getNew();
    $bca->disbursement_type = Disbursement::DISBURSEMENT;
    $bca->disbursement_reference_id = $data['disbursement_id'];
    $bca->status = BcaDisbursement::PENDING;
    $this->bcaDisbursementRepo->save($bca);

    return $listener->response(200, [
      'disbursement_vendor_reference_id' => $bca->id,
      'disbursement_vendor_updated_at' => strtotime($bca->updated_at),
    ]);
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/12/18
 * Time: 16.43
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Permata\Helper;

use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\PermataDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Disbursement\Withdrawal\Permata\Jobs\TransferWithdraw;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Vendor\Permata\Repository\DisbursementPermataDisbursementRepository;

class ExecuteTransfer {

  /**
   * @var UserWithdrawRepository
   */
  private $userWithdrawRepo;

  /**
   * @var DisbursementPermataDisbursementRepository
   */
  private $permataDisbursementRepo;

  function __construct() {
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
    $this->permataDisbursementRepo = app()->make(DisbursementPermataDisbursementRepository::class);
  }

  public function execute($withdraw) {
    if ($withdraw->auto_disbursement_reference_id) {
      $permataId = $withdraw->auto_disbursement_reference_id;

    } else {
      $permata = $this->permataDisbursementRepo->getNew();
      $permata->amount = $withdraw->amount - $withdraw->fee;
      $permata->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $permata->disbursement_reference_id = $withdraw->id;
      $permata->status = PermataDisbursement::STATUS_PENDING;

      $this->permataDisbursementRepo->save($permata);
      $permataId = $permata->id;

      $withdraw->auto_disbursement_reference_id = $permataId;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_PERMATA_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;
    $this->userWithdrawRepo->save($withdraw);

    dispatch(new TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'permata_disbursement_id' => $permataId
    ]));
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 04/12/18
 * Time: 16.57
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Permata\Jobs;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Permata\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job {

  /**
   * @var UserWithdrawRepository
   */
  private $userWithdrawRepo;

  /**
   * @var Redis
   */
  private $redis;

  private $data;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
  }

  private function initialize() {
    $this->redis = Redis::connection();
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
  }

  public function handle() {
    $this->initialize();

    $data = $this->data;

    if ($withdraw = $this->userWithdrawRepo->findById($data['withdraw_id'])) {
      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $pipeline = new Pipeline;
    $namespace = 'odeo_core:auto_wd_lock';
    $withdrawId = $data['withdraw_id'];

    if (!$this->redis->hsetnx($namespace, 'wd_id_' . $withdrawId, 1)) {
      throw new \Exception('trying to execute duplicate withdraw');
    }

    $pipeline->add(new Task(Transfer::class, 'exec', [
      'desc' => $withdraw->is_bank_transfer ? $withdraw->description ?? '' : 'WDOCASH ' . $withdrawId,
      'transaction_key' => 'WD-OCASH#' . $withdrawId,
      'account_number' => $withdraw->account_number,
      'account_name' => $withdraw->account_name,
      'permata_disbursement_id' => $data['permata_disbursement_id']
    ]));

    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));
    $pipeline->execute($data);

    if ($pipeline->success()) {
      $this->redis->hdel($namespace, 'wd_id_' . $withdrawId);
    } else {
      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);

      $errorCode = $pipeline->errorMessage['error_code'] ?? null;
      if (!$errorCode) return;

      $pipeline = $pipeline->clear();

      switch ($errorCode) {
//        case '01': // decline
//          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
//            'withdraw_id' => $withdrawId,
//            'withdraw_status' => Withdraw::CANCELLED_FOR_BANK_REJECTION
//          ]));
//          break;
        case '14': // account not found
        case '90': // invalid beneficiary/currency
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdrawId,
            'withdraw_status' => Withdraw::CANCELLED_FOR_INVALID_ACCOUNT_INFORMATION
          ]));
          break;
      }
      $pipeline->countTask() && $pipeline->execute([
        'user_id' => $withdraw->user_id
      ]);
    }
  }
}

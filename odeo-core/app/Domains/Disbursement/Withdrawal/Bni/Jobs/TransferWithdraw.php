<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Bni\Jobs;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Transaction\WithdrawCanceller;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\Bni\Helper\ApiManager;
use Odeo\Domains\Vendor\Bni\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job {

  private $data;

  private $userWithdraws, $redis;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;
    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->redis = Redis::connection();
  }

  public function handle() {

    if ($withdraw = $this->userWithdraws->findById($this->data['withdraw_id'])) {

      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $namespace = 'odeo_core:auto_wd_lock';

    if (!$this->redis->hsetnx($namespace, 'wd_id_' . $this->data['withdraw_id'], 1)) {
      return;
    }

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'remark' => $withdraw->is_bank_transfer ? $withdraw->description ?? '' : 'WDOCASH 1' . $withdraw->id,
      'transfer_from' => ApiManager::getConfig('account_number'),
      'transfer_to' => $withdraw->account_number
    ]));
    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));
    $pipeline->execute($this->data);

    if ($pipeline->success()) {
      $this->redis->hdel($namespace, 'wd_id_' . $withdraw->id);
      return;
    } else {

      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdraws->save($withdraw);

      $errorCode = $pipeline->errorMessage['response_code'] ?? null;

      if (!$errorCode) return;

      $pipeline = $pipeline->clear();

      switch ($errorCode) {
        case '0007':
          $pipeline->add(new Task(WithdrawCanceller::class, 'cancel', [
            'withdraw_id' => $withdraw->id,
            'withdraw_status' => Withdraw::CANCELLED_FOR_WRONG_ACCOUNT_NUMBER
          ]));
          break;
      }

      $pipeline->countTask() && $pipeline->execute([
        'user_id' => $withdraw->user_id
      ]);

    }
  }

}

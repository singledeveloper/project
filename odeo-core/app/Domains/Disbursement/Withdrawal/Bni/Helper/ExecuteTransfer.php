<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Bni\Helper;

use Odeo\Domains\Constant\BniDisbursement;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\Withdraw;

class ExecuteTransfer {

  private $userWithdraws, $bniDisbursements;

  public function __construct() {
    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
    $this->bniDisbursements = app()->make(\Odeo\Domains\Vendor\Bni\Repository\DisbursementBniDisbursementRepository::class);
  }

  public function execute($withdraw) {

    $bniId = null;

    if ($withdraw->auto_disbursement_reference_id) {

      $bniId = $withdraw->auto_disbursement_reference_id;

    } else {

      $bni = $this->bniDisbursements->getNew();

      $bni->amount = $withdraw->amount - $withdraw->fee;
      $bni->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $bni->disbursement_reference_id = $withdraw->id;
      $bni->status = BniDisbursement::PENDING;

      $this->bniDisbursements->save($bni);

      $bniId = $bni->id;

      $withdraw->auto_disbursement_reference_id = $bni->id;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_BNI_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;

    $this->userWithdraws->save($withdraw);

    dispatch(new \Odeo\Domains\Disbursement\Withdrawal\Bni\Jobs\TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'bni_disbursement_id' => $bniId
    ]));
  }

}
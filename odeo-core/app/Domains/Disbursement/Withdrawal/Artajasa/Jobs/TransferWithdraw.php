<?php

namespace Odeo\Domains\Disbursement\Withdrawal\Artajasa\Jobs;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Account\Repository\BankRepository;
use Odeo\Domains\Constant\ArtajasaDisbursement;
use Odeo\Domains\Constant\Withdraw;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\Withdrawal\Artajasa\Helper\ArtajasaResponseHandler;
use Odeo\Domains\Transaction\Repository\UserWithdrawRepository;
use Odeo\Domains\Transaction\WithdrawRequester;
use Odeo\Domains\Vendor\ArtajasaDisbursement\Transfer;
use Odeo\Jobs\Job;

class TransferWithdraw extends Job implements ShouldQueue {

  private $data, $artajasaResponseHandler, $bankRepo, $redis;
  private $userWithdrawRepo;

  public function __construct($data = null) {
    parent::__construct();
    $this->data = $data;
    $this->artajasaResponseHandler = app()->make(ArtajasaResponseHandler::class);
    $this->bankRepo = app()->make(BankRepository::class);
    $this->redis = Redis::connection();
    $this->userWithdrawRepo = app()->make(UserWithdrawRepository::class);
  }

  public function handle() {
    if ($withdraw = $this->userWithdrawRepo->findById($this->data['withdraw_id'])) {

      if ($withdraw->status != Withdraw::ON_PROGRESS_AUTO_TRANSFER) {
        return;
      }
    }

    $pipeline = new Pipeline;

    if (!$this->lock($this->data['withdraw_id'])) {
      return;
    }

    $bank = $withdraw->bank;
    $transactionKey = 'WDOCASH' . $withdraw->id;
    $purposeDesc = $withdraw->is_bank_transfer ? $withdraw->description ?? '' :  $transactionKey;

    $terminalId = preg_replace('/\\D/', '', $withdraw->user->telephone);
    $terminalId = substr($terminalId, strlen($terminalId) - 8, 8);
    $terminalId = empty($terminalId) ? '1' : $terminalId;

    $pipeline->add(new Task(Transfer::class, 'exec', [
      'artajasa_disbursement_id' => $this->data['artajasa_disbursement_id'],
      'bank_code' => explode('-', $bank->aj_bank_code)[0],
      'regency_code' => $bank->aj_regency_code,
      'transfer_to' => $withdraw->account_number,
      'transfer_to_name' => $withdraw->account_name,
      'transfer_to_bank' => $bank->name,
      'amount' => $this->data['amount'],
      'purpose_desc' => $purposeDesc,
      'transaction_key' => $transactionKey,
      'channel_type' => ArtajasaDisbursement::CHANNEL_MOBILE,
      'user_id' => $withdraw->user_id,
      'user_name' => $withdraw->user->name,
      'terminal_id' => $terminalId,
      'sender_prefix' => ArtajasaDisbursement::ODEO_SENDER_PREFIX,
    ]));

    $pipeline->add(new Task(WithdrawRequester::class, 'complete'));

    $pipeline->execute($this->data);

    if ($pipeline->success()) {
      $this->unlock($withdraw->id);
      return;
    } else {
      $responseCode = $pipeline->errorMessage['response_code'] ?? null;
      $withdraw->status = Withdraw::FAIL_PROCESS_AUTO_TRANSFER;
      $this->userWithdrawRepo->save($withdraw);

      if($responseCode) {
        $this->artajasaResponseHandler->handle($responseCode, $withdraw->id, $withdraw->user_id);
      }
    }
  }

  private function lock($id) {
    return $this->redis->hsetnx('odeo_core:auto_wd_lock', 'wd_id_' . $id, 1);
  }

  private function unlock($id) {
    return $this->redis->hdel('odeo_core:auto_wd_lock', 'wd_id_' . $id);
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/10/17
 * Time: 10:41 PM
 */

namespace Odeo\Domains\Disbursement\Withdrawal\Flip\Helper;

use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\Disbursement;
use Odeo\Domains\Constant\FlipDisbursement;
use Odeo\Domains\Constant\Withdraw;

class ExecuteTransfer {


  private $flipDisbursements, $userWithdraws;

  public function __construct() {
    $this->flipDisbursements = app()->make(\Odeo\Domains\Vendor\Flip\Repository\DisbursementFlipDisbursementRepository::class);
    $this->userWithdraws = app()->make(\Odeo\Domains\Transaction\Repository\UserWithdrawRepository::class);
  }

  public function execute($withdraw, $bankId) {

    $flipId = null;

    if ($withdraw->auto_disbursement_reference_id) {

      $flipId = $withdraw->auto_disbursement_reference_id;

    } else {

      $flip = $this->flipDisbursements->getNew();

      $flip->amount_to_be_transfer = $withdraw->amount - $withdraw->fee;
      $flip->disbursement_type = Disbursement::DISBURSEMENT_FOR_WITHDRAW_OCASH;
      $flip->disbursement_reference_id = $withdraw->id;
      $flip->status = FlipDisbursement::PENDING;

      $this->flipDisbursements->save($flip);

      $flipId = $flip->id;

      $withdraw->auto_disbursement_reference_id = $flip->id;
      $withdraw->auto_disbursement_vendor = Withdraw::DISBURSEMENT_VENDOR_FLIP_API_DISBURSEMENT;
    }

    $withdraw->status = Withdraw::ON_PROGRESS_AUTO_TRANSFER;

    $this->userWithdraws->save($withdraw);

    $fee = FlipDisbursement::FEE_LOCAL;
    if ($bankId != Bank::MANDIRI) {
      $fee = FlipDisbursement::FEE_DOMESTIC;
    }

    dispatch((new \Odeo\Domains\Disbursement\Withdrawal\Flip\Jobs\TransferWithdraw([
      'withdraw_id' => $withdraw->id,
      'flip_disbursement_id' => $flipId,
      'bank_id' => $bankId,
      'fee' => $fee
    ])));



  }

}
<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Bri\Jobs;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\SendAlertReplenishmentFail;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentRequester;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;
use Odeo\Domains\Vendor\Bri\Helper\ApiManager;
use Odeo\Domains\Vendor\Bri\Transfer;
use Odeo\Jobs\Job;

class TransferReplenishment extends Job  {

  private $data;

  private $billerReplenishmentRepo;

  private $redis;

  public function __construct($data) {
    parent::__construct();
    $this->data = $data;

    $this->redis = Redis::connection();
    $this->billerReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
  }

  public function handle() {

    $data = $this->data;

    if ($this->redis->hsetnx(BillerReplenishment::REDIS_NS, $this->getRedisKey(), 1)) {
      return;
    }

    $replenishment = $this->billerReplenishmentRepo->findById($data['replenishment_id']);
    $vendorSwitcher = $replenishment->vendorSwitcher;

    if ($replenishment->status != BillerReplenishment::ON_PROGRESS_AUTO_TRANSFER) {
      return;
    }

    $accountNumber = ApiManager::$accountNumber;

    $pipeline = new Pipeline;
    $pipeline->add(new Task(Transfer::class, 'exec', [
      'remark' => 'VSREPL#' . $replenishment->id,
      'transfer_from' => $accountNumber,
      'transfer_to' => $vendorSwitcher->bri_account_number,
    ]));

    $pipeline->add(new Task(ReplenishmentRequester::class, 'complete'));

    $pipeline->execute($this->data);

    if ($pipeline->fail()) {

      $errorCode = $pipeline->errorMessage['error_code'] ?? '';

      $replenishment->status = BillerReplenishment::FAILED;
      $replenishment->error_message = 'Error code: ' . $errorCode;
      $this->billerReplenishmentRepo->save($replenishment);

      dispatch(new SendAlertReplenishmentFail($vendorSwitcher->name, $replenishment->amount));
    }

    $this->deleteRedisKey();

  }


  private function deleteRedisKey() {
    $this->redis->hdel(BillerReplenishment::REDIS_NS, $this->getRedisKey());
  }

  private function getRedisKey() {
    $data = $this->data;
    return BillerReplenishment::REDIS_KEY_ID . $data['replenishment_id'];
  }

}

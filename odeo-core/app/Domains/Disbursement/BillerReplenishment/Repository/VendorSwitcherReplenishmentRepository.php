<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Repository;

use Carbon\Carbon;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Disbursement\BillerReplenishment\Model\VendorSwitcherReplenishment;

class VendorSwitcherReplenishmentRepository extends Repository {

  public function __construct(VendorSwitcherReplenishment $vendorSwitcherReplenishment) {
    $this->model = $vendorSwitcherReplenishment;
  }

  public function findLastReplenishment($vendorSwitcherId) {
    return $this->model->where("vendor_switcher_id", $vendorSwitcherId)
      ->where('status', '<', BillerReplenishment::COMPLETED)
      ->where('status', '<>', BillerReplenishment::FAILED)
      ->orderBy("id", "desc")->first();
  }

  public function getSameDayCompletedReplenishmentCount($vendorSwitcherId) {
    return $this->model->where("vendor_switcher_id", $vendorSwitcherId)
      ->where('status', BillerReplenishment::COMPLETED)
      ->where(\DB::raw('date(created_at)'), date("Y-m-d"))->count();
  }

  public function getCompletedReplenishmentCount($vendorSwitcherId) {
    return $this->model->where("vendor_switcher_id", $vendorSwitcherId)
      ->where('status', BillerReplenishment::COMPLETED)
      ->where('created_at', '>', Carbon::now()->subHours(24)->toDateTimeString())->count();
  }

  public function findDuplicatedReplenishment($vendorSwitcherId, $minutes = 3) {
    return $this->model->where("vendor_switcher_id", $vendorSwitcherId)
      ->whereIn('status', [
        BillerReplenishment::COMPLETED,
        BillerReplenishment::ON_PROGRESS_AUTO_TRANSFER,
        BillerReplenishment::WAIT_FOR_NOTIFY
      ])
      ->where('created_at', '>', Carbon::now()->subMinutes($minutes)->toDateTimeString())->first();
  }

  public function findUnprocessedAutoReplenishment() {
    return $this->model->where('status', BillerReplenishment::PENDING)->first();
  }

  public function getPendingTransactions() {
    return $this->model->with('vendorSwitcher')->where(function($query){
      $query->whereNotNull('verified_at')->where('status', BillerReplenishment::WAIT_FOR_NOTIFY);
    })->orWhereIn('status', [
      BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER,
      BillerReplenishment::WAIT_FOR_BILLER_UPDATE
    ])->get();
  }

  public function findByAmount($vendorSwitcherId, $amount, $vendor) {
    return $this->model->where('amount', $amount)
      ->where("vendor_switcher_id", $vendorSwitcherId)
      ->where('auto_disbursement_vendor', $vendor)
      ->whereNotIn('status', [
        BillerReplenishment::FAILED,
        BillerReplenishment::CANCELLED,
        BillerReplenishment::WAIT_FOR_BILLER_UPDATE
      ])
      ->whereNull('reconciled_at')->first();
  }

  public function gets() {
    $query = $this->getCloneModel();
    $filters = $this->getFilters();

    $query = $query->with(['vendorSwitcher']);

    if (!isset($filters['sort_by'])) $filters['sort_by'] = 'updated_at';
    if (!isset($filters['sort_type'])) $filters['sort_type'] = 'desc';

    return $this->getResult($query->orderBy($filters['sort_by'], $filters['sort_type']));
  }

}
<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\SendAlertReplenishmentFail;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;
use Odeo\Domains\Inventory\Repository\VendorSwitcherRepository;
use Odeo\Domains\Transaction\Helper\Currency;

class ReplenishmentCreator {

  private $redis, $billerReplenishmentRepo, $vendorSwitcherRepo, $settingExecutor, $currency;

  public function __construct() {

    $this->redis = Redis::connection();

    $this->billerReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
    $this->vendorSwitcherRepo = app()->make(VendorSwitcherRepository::class);
    $this->settingExecutor = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Helper\SettingExecutor::class);
    $this->currency = app()->make(Currency::class);
  }


  public function request(PipelineListener $listener, $data) {

    $lastReplenishment = $this->billerReplenishmentRepo->findLastReplenishment($data['vendor_switcher_id']);

    $biller = $this->vendorSwitcherRepo->findById($data['vendor_switcher_id']);
    $multiplier = isset($data['multiplier']) && $data['multiplier'] > 1 ? $data['multiplier'] : 1;

    $autoReplenishVendor = $data['bank'] ?? $this->getAutoDisbursementAvailableVendor();

    if ($multiplier > 1 && ($biller->status == SwitcherConfig::BILLER_STATUS_OK || $autoReplenishVendor != BillerReplenishment::DISBURSEMENT_VENDOR_BCA_MANUAL))
      return $listener->response(400, 'Biller ini tidak dapat melakukan pengulangan. Mohon cek kembali input Anda.');
    else if ($biller->status != SwitcherConfig::BILLER_STATUS_OK) $data['is_biller_update_ignored'] = true;

    if ($lastReplenishment && $lastReplenishment->status != BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER) {
      $this->touchLastReplenishment($lastReplenishment);
      return $listener->response(200);
    }

    if ($lastReplenishment) $data['amount'] = $lastReplenishment->amount;

    if (!(isset($data['auth']['user_id']) && $data['auth']['user_id'] == 4350)) {
      if ($autoReplenishVendor == BillerReplenishment::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT
        && $data['amount'] > BillerReplenishment::DEFAULT_MAXIMAL_DEPOSIT )
        return $listener->response(400, 'Tidak dapat melakukan replenishment lebih dari ' .
          $this->currency->getFormattedOnly(BillerReplenishment::DEFAULT_MAXIMAL_DEPOSIT));
    }

    $beginReplenishment = !isset($data['is_biller_update_ignored']) ? $this->settingExecutor->beforeSend($listener, $data) : [];

    if (isset($beginReplenishment['is_error'])) {
      $this->sendAlertReplenishmentFail($data);
      return $listener->response(200);
    }

    if (!$lastReplenishment) {
      $lastReplenishment = $this->billerReplenishmentRepo->getNew();
      $lastReplenishment->vendor_switcher_id = $data['vendor_switcher_id'];
    }

    $lastReplenishment->amount = $this->getAmount($beginReplenishment, $data);
    $lastReplenishment->status = $this->getStatus($beginReplenishment, $autoReplenishVendor);

    $lastReplenishment->auto_disbursement_vendor = $autoReplenishVendor;
    $lastReplenishment->balance_before = $lastReplenishment->vendorSwitcher->current_balance;

    $this->billerReplenishmentRepo->save($lastReplenishment);

    if ($biller->status != SwitcherConfig::BILLER_STATUS_OK) {
      for($x=0; $x<$multiplier; $x++) {
        if ($x>0) $replenishment = $lastReplenishment->replicate();
        else $replenishment = $lastReplenishment;
        $replenishment->verified_at = date('Y-m-d H:i:s');
        $replenishment->status = BillerReplenishment::COMPLETED;
        $this->billerReplenishmentRepo->save($replenishment);

        dispatch(new InsertMutation([
          'vendor_switcher_id' => $biller->id,
          'amount' => $replenishment->amount,
          'mutation_route' => SwitcherConfig::ROUTE_REPLENISHER
        ]));
      }
    }

    return $listener->response(200);

  }

  private function sendAlertReplenishmentFail($data) {
    $this->deleteRedisKey($data);
    $biller = $this->vendorSwitcherRepo->findById($data['vendor_switcher_id']);
    dispatch(new SendAlertReplenishmentFail($biller->name, $data['amount']));
  }

  private function getStatus($beginReplenishment, $autoReplenishVendor) {

    if (isset($beginReplenishment['on_hold'])) {
      return BillerReplenishment::WAIT_FOR_BILLER_UPDATE;
    }

    if (isset($beginReplenishment['status'])) {
      return $beginReplenishment['status'];
    }

    if (BillerReplenishment::isAutoDisbursement($autoReplenishVendor)) {
      return BillerReplenishment::PENDING;
    } else {
      return BillerReplenishment::WAIT_FOR_MANUAL_VERIFY;
    }
  }

  private function getAmount($beginReplenishment, $data) {
    if (isset($beginReplenishment['on_hold'])) return $data['amount'];
    return $beginReplenishment['amount'] ?? $data['amount'];
  }

  private function deleteRedisKey($data) {
    $this->redis->hdel(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_BILLER . $data['vendor_switcher_id']);
  }

  private function getAutoDisbursementAvailableVendor() {
    return BillerReplenishment::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT;
  }

  private function touchLastReplenishment($lastReplenishment) {
    $this->billerReplenishmentRepo->save($lastReplenishment);
  }

}

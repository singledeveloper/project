<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Scheduler;

use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\Bca\Helper\ExecuteTransfer as ExecuteTransferBca;
use Odeo\Domains\Disbursement\BillerReplenishment\Bri\Helper\ExecuteTransfer as ExecuteTransferBri;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentRequester;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;

class AutoReplenishmentScheduler {

  private $billerReplenishmentRepo;

  public function __construct() {
    $this->billerReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
  }

  public function run() {

    if ($replenishment = $this->billerReplenishmentRepo->findUnprocessedAutoReplenishment()) {

      if ($this->billerReplenishmentRepo->findDuplicatedReplenishment($replenishment->vendor_switcher_id)) {
        $this->cancelReplenishment($replenishment);

      } else {

        switch ($replenishment->auto_disbursement_vendor) {

          case BillerReplenishment::DISBURSEMENT_VENDOR_BCA_API_DISBURSEMENT:
            BcaDisbursement::isOperational() &&
            app()->make(ExecuteTransferBca::class)->execute($replenishment);
            break;

          case BillerReplenishment::DISBURSEMENT_VENDOR_BRI_API_DISBURSEMENT:
            app()->make(ExecuteTransferBri::class)->execute($replenishment);
            break;

          default:
            $this->failReplenishment($replenishment);
            break;

        }
      }
    }
  }


  private function cancelReplenishment($replenishment) {
    $pipeline = new Pipeline();
    $pipeline->add(new Task(ReplenishmentRequester::class, 'cancel'));
    $pipeline->execute(['replenishment_id' => $replenishment->id]);
  }

  private function failReplenishment($replenishment) {
    $replenishment->status = BillerReplenishment::FAILED;
    $this->billerReplenishmentRepo->save($replenishment);
  }

}

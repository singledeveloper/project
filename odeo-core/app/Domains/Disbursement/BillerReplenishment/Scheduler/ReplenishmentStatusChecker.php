<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Scheduler;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\Jobs\InsertMutation;
use Odeo\Domains\Constant\BcaDisbursement;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\SwitcherConfig;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\ResumeReplenishment;
use Odeo\Domains\Disbursement\BillerReplenishment\Jobs\SendAlertReplenishmentFail;
use Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository;

class ReplenishmentStatusChecker {

  private $billerReplenishmentRepo, $redis;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->billerReplenishmentRepo = app()->make(VendorSwitcherReplenishmentRepository::class);
  }

  public function run() {

    foreach ($this->billerReplenishmentRepo->getPendingTransactions() as $replenishment) {

      switch ($replenishment->status) {
        case  BillerReplenishment::PENDING_PROGRESS_AUTO_TRANSFER:
          $this->resumeReplenishment($replenishment);
          break;

        case BillerReplenishment::WAIT_FOR_BILLER_UPDATE:
          $this->checkBillerUpdate($replenishment);
          break;

        default:
          $this->updateIfTopUpHasSuccess($replenishment);
          break;
      }
    }
  }

  private function updateIfTopUpHasSuccess($replenishment) {
    $biller = $replenishment->vendorSwitcher;
    if ($biller->current_balance > $replenishment->balance_before) {
      $replenishment->status = BillerReplenishment::COMPLETED;
      $this->billerReplenishmentRepo->save($replenishment);

      $this->deleteRedisKey($replenishment);

      if ($biller->minimum_topup != null) {
        dispatch(new InsertMutation([
          'vendor_switcher_id' => $biller->id,
          'amount' => $replenishment->amount,
          'mutation_route' => SwitcherConfig::ROUTE_REPLENISHER
        ]));
      }
    }
  }

  private function resumeReplenishment($replenishment) {
    if (BcaDisbursement::isOperational()) {
      $replenishment->balance_before = $replenishment->vendorSwitcher->current_balance;
      $this->billerReplenishmentRepo->save($replenishment);

      dispatch(new ResumeReplenishment(['vendor_switcher_id' => $replenishment->vendor_switcher_id]));
    }
  }


  private function checkBillerUpdate($replenishment) {
    if (time() - strtotime($replenishment->created_at) >= 600) {
      $replenishment->status = BillerReplenishment::FAILED;
      $replenishment->error_message = 'No response from biller';
      $this->billerReplenishmentRepo->save($replenishment);

      dispatch(new SendAlertReplenishmentFail($replenishment->vendorSwitcher->name, $replenishment->amount));
    }
  }

  private function deleteRedisKey($replenishment) {
    $this->redis->hdel(BillerReplenishment::REDIS_NS, BillerReplenishment::REDIS_KEY_BILLER . $replenishment->vendor_switcher_id);
  }


}

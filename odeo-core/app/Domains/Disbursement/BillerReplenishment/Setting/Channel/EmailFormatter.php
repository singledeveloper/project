<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel;

use Illuminate\Support\Facades\Mail;
use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Contract\ChannelContract;

class EmailFormatter implements ChannelContract {

  public function beforeSend($data, $setting) {
    return ['amount' => $data['amount']];
  }

  public function onComplete($data, $setting) {

    $settingData = json_decode($setting->data, true);

    Mail::send([], [], function ($m) use ($data, $settingData) {

      $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

      $replyMessage = str_replace(Supplier::addPattern(Supplier::SF_PARAM_DEPOSIT),
        $currencyHelper->formatPrice($data['amount'])['formatted_amount'], $settingData[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY]);
      $replyMessage = str_replace(Supplier::addPattern(Supplier::SF_PARAM_BANK), strtoupper(explode('_', $data['bank'])[0]), $replyMessage);

      $m->from('cs@odeo.co.id', 'Customer Service ODEO');
      $m->to(explode(';', $settingData[BillerReplenishment::SETTING_CHANNEL_EMAIL_TO]))
        ->subject('Odeo Konfirmasi Deposit '  . date('Y-m-d'));
      $m->setBody($replyMessage, 'text/html');

    });

    return [];
  }

  public function onSetup($data) {
    return;
  }

}

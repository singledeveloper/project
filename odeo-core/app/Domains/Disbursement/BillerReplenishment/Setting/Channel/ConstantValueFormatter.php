<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Contract\ChannelContract;

class ConstantValueFormatter implements ChannelContract {

  private $billerReplenishments;

  public function __construct() {
    $this->billerReplenishments = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Repository\VendorSwitcherReplenishmentRepository::class);
  }

  public function beforeSend($data, $setting) {
    $count = $this->billerReplenishments->getSameDayCompletedReplenishmentCount($data['vendor_switcher_id']);
    $data['amount']+= ($count * BillerReplenishment::SAME_DAY_MULTIPLIER);
    return ['amount' => $data['amount'] + intval($setting->data)];
  }

  public function onComplete($data, $setting) {
    return [];
  }

  public function onSetup($data) {
    return;
  }

}

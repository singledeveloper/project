<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Channel;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Constant\InlineTelegram;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\Contract\ChannelContract;

class TelegramFormatter implements ChannelContract {

  public function beforeSend($data, $setting) {
    return ['amount' => $data['amount']];
  }

  public function onComplete($data, $setting) {
    $telegramManager = app()->make(\Odeo\Domains\Vendor\Telegram\TelegramManager::class);
    $currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);

    $settingData = json_decode($setting->data, true);

    $replyMessage = str_replace(Supplier::addPattern(Supplier::SF_PARAM_DEPOSIT),
      $currencyHelper->formatPrice($data['amount'])['formatted_amount'], $settingData[BillerReplenishment::SETTING_CHANNEL_TELEGRAM_MESSAGE]);
    $replyMessage = str_replace(Supplier::addPattern(Supplier::SF_PARAM_BANK), strtoupper(explode('_', $data['bank'])[0]), $replyMessage);

    $telegramManager->initialize($settingData[BillerReplenishment::SETTING_CHANNEL_TELEGRAM_CHAT_ID], InlineTelegram::CORE_TOKEN);
    $telegramManager->reply($replyMessage);
    return [];
  }

  public function onSetup($data) {
    $settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
    $cache = app()->make(\Illuminate\Contracts\Cache\Repository::class);
    $table = $settings->getModel()->getTable();
    $cache->tags($table)->forget($table . '.' . InlineTelegram::REDIS_IGNORED_CHAT_ID_KEY);
    return;
  }

}

<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting;

use Odeo\Domains\Constant\BillerReplenishment;
use Odeo\Domains\Core\PipelineListener;

class SettingRequester {

  private $settings, $vendorSwitchers, $channelManager;

  public function __construct() {
    $this->settings = app()->make(\Odeo\Domains\Disbursement\BillerReplenishment\Setting\Repository\VendorSwitcherReplenishmentSettingRepository::class);
    $this->vendorSwitchers = app()->make(\Odeo\Domains\Inventory\Repository\VendorSwitcherRepository::class);
    $this->channelManager = app()->make(ChannelManager::class);
  }

  public function editBiller(PipelineListener $listener, $data) {

    if ($biller = $this->vendorSwitchers->findById($data['vendor_switcher_id'])) {
      $biller->minimum_topup = $data['minimum_topup'];
      $biller->maximum_topup = $data['maximum_topup'];
      if ($data['topup_started_at'] != '' && $data['topup_ended_at'] != '') {
        $biller->topup_started_at = $data['topup_started_at'];
        $biller->topup_ended_at = $data['topup_ended_at'];
      }
      $biller->is_auto_replenish = isset($data['is_auto_replenish']);

      $this->vendorSwitchers->save($biller);

      return $listener->response(200);
    }
    return $listener->response(400, 'Data is invalid');

  }

  public function setup(PipelineListener $listener, $data) {

    if ($data['route_type'] == BillerReplenishment::SETTING_ROUTE_BEFORE && $this->settings->findByVendorSwitcherId($data['vendor_switcher_id'], BillerReplenishment::SETTING_ROUTE_BEFORE))
      return $listener->response(400, 'Beforesend already set. You need to remove the previous one.');

    $setting = $this->settings->getNew();
    $setting->vendor_switcher_id = $data['vendor_switcher_id'];
    $setting->route_type = $data['route_type'];
    $setting->channel_type = $data['channel_type'];

    $variables = [];
    foreach (BillerReplenishment::getRequiredChannelParameters($data['channel_type']) as $item) {
      $variables[$item] = is_string($data[$item]) ? trim($data[$item]) : $data[$item];
    }
    if (isset($variables[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY]))
      $variables[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY] = str_replace(["\r\n", "\r", "\n"], '<br>', $variables[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY]);
    $setting->data = json_encode($variables);
    $this->settings->save($setting);

    $this->channelManager->setPath($setting->channel_type)->onSetup($variables);

    return $listener->response(200);

  }

  public function edit(PipelineListener $listener, $data) {

    if ($setting = $this->settings->findById($data['biller_replenishment_setting_id'])) {
      $setting->route_type = $data['route_type'];
      $setting->channel_type = $data['channel_type'];

      $variables = [];
      foreach (BillerReplenishment::getRequiredChannelParameters($data['channel_type']) as $item) {
        $variables[$item] = is_string($data[$item]) ? trim($data[$item]) : $data[$item];
      }
      if (isset($variables[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY]))
        $variables[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY] = str_replace(["\r\n", "\r", "\n"], '<br>', $variables[BillerReplenishment::SETTING_CHANNEL_EMAIL_BODY]);
      $setting->data = json_encode($variables);
      $this->settings->save($setting);

      $this->channelManager->setPath($setting->channel_type)->onSetup($variables);

      return $listener->response(200);
    }
    return $listener->response(400, 'Data is invalid');

  }

  public function remove(PipelineListener $listener, $data) {

    if ($setting = $this->settings->findById($data['biller_replenishment_setting_id'])) {
      $setting->is_active = false;
      $this->settings->save($setting);

      return $listener->response(200);
    }
    return $listener->response(400, 'Data is invalid');

  }

}

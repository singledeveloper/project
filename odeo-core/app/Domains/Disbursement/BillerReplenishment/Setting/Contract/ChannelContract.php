<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/20/16
 * Time: 11:10 PM
 */

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Contract;

interface ChannelContract {

  public function beforeSend($data, $setting);

  public function onComplete($data, $setting);

  public function onSetup($data);

}
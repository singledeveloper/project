<?php

namespace Odeo\Domains\Disbursement\BillerReplenishment\Setting\Model;

use Odeo\Domains\Core\Entity;
use Odeo\Domains\Inventory\Model\VendorSwitcher;

class VendorSwitcherReplenishmentSetting extends Entity {

  public function vendorSwitcher() {
    return $this->belongsTo(VendorSwitcher::class);
  }

}
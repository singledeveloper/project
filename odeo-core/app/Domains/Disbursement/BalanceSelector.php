<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 5/3/17
 * Time: 10:48 PM
 */

namespace Odeo\Domains\Disbursement;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;
use Odeo\Domains\Disbursement\Helper\VendorDisbursementBalanceHelper;

class BalanceSelector implements SelectorListener {

  private $vendorDisbursements, $currencyHelpers, $vendorDisbursementBalanceHelper;

  public function __construct() {
    $this->vendorDisbursements = app()->make(\Odeo\Domains\Disbursement\Repository\VendorDisbursementRepository::class);
    $this->currencyHelpers = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->vendorDisbursementBalanceHelper = app()->make(VendorDisbursementBalanceHelper::class);
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function _transforms($item, Repository $repository) {

    $temp = [];

    $temp['name'] = $item->name;
    $temp['balance'] = $this->currencyHelpers->formatPrice($item->balance);

    return $temp;
  }

  public function getBalance(PipelineListener $listener, $data) {

    $vendorDisbursement = $this->vendorDisbursements->findById($data['id']);

    if (!$vendorDisbursement || !count($vendorDisbursement)) {
      return $listener->response(400, 'cannot find balance', true);
    }

    $vendorDisbursement->balance = $this->vendorDisbursementBalanceHelper->getBalance($data['id']);
    $vendorDisbursement = $this->_transforms($vendorDisbursement, $this->vendorDisbursements);

    return $listener->response(200, [
      'vendor_disbursement_information' => $vendorDisbursement
    ]);


  }

}
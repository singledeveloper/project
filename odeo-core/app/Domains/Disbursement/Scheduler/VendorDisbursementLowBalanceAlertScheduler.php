<?php

namespace Odeo\Domains\Disbursement\Scheduler;


use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Disbursement\Helper\VendorDisbursementBalanceHelper;
use Odeo\Domains\Disbursement\Jobs\SendVendorDisbursementLowBalanceAlert;

class VendorDisbursementLowBalanceAlertScheduler {

  private $redis, $vendorDisbursementBalanceHelper;

  public function __construct() {
    $this->redis = Redis::connection();
    $this->vendorDisbursementBalanceHelper = app()->make(VendorDisbursementBalanceHelper::class);
  }

  public function run() {
    $key = 'odeo_core:vendor_disbursement_low_balance_last_alert_time2';
    $alerted = true;
    $lowBalances = [];
    $idMap = [
      'aj' => VendorDisbursement::ARTAJASA,
      'bca' => VendorDisbursement::BCA,
      'bni' => VendorDisbursement::BNI,
      'cimb' => VendorDisbursement::CIMB,
      //'redig' => VendorDisbursement::REDIG,
    ];

    foreach ($idMap as $vendor => $id) {
      $balance = $this->vendorDisbursementBalanceHelper->getBalance($id);

      if ($balance > 20e6) {
        continue;
      }

      $lowBalances[$vendor] = $balance;
      $time = $this->redis->hget($key, $vendor);

      if (time() - $time < 3600) {
        continue;
      }

      $alerted = false;
      $this->redis->hset($key, $vendor, time());
    }

    if ($alerted) {
      return;
    }

    dispatch(new SendVendorDisbursementLowBalanceAlert($lowBalances));
  }
}
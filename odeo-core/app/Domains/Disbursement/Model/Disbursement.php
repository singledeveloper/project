<?php


namespace Odeo\Domains\Disbursement\Model;


use Odeo\Domains\Account\Model\User;
use Odeo\Domains\Core\Entity;

class Disbursement extends Entity {

  public function user() {
    return $this->belongsTo(User::class);
  }

}
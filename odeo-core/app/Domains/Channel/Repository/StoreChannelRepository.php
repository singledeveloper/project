<?php

namespace Odeo\Domains\Channel\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Channel\Model\StoreChannel;

class StoreChannelRepository extends Repository {

  public function __construct(StoreChannel $channel) {
    $this->model = $channel;
  }

  public function get() {
    $filters = $this->getFilters();
    $query = $this->model->orderBy('id', 'desc');

    if ($this->hasExpand('store')) $query = $query->with('store');
    if ($this->hasExpand('mdr')) $query = $query->with('store.mdr');
    if ($this->hasExpand('user')) $query = $query->with('user');

    if (isset($filters['store_id'])) $query = $query->where('store_id', $filters['store_id']);

    return $this->getResult($query);
  }
}

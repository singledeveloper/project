<?php

namespace Odeo\Domains\Channel\Repository;

use Odeo\Domains\Core\Repository;
use Odeo\Domains\Channel\Model\StoreMdr;

class StoreMdrRepository extends Repository {

  public function __construct(StoreMdr $mdr) {
    $this->model = $mdr;
  }
}

<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;

class StoreMdrRequester {

  public function __construct() {
    $this->mdr = app()->make(\Odeo\Domains\Channel\Repository\StoreMdrRepository::class);
  }

  public function upsert(PipelineListener $listener, $data) {
    $storeMdr = $this->mdr->findByAttributes('store_id', $data['store_id']);
    if(!$storeMdr) {
      $storeMdr = $this->mdr->getNew();
      $storeMdr->store_id = $data['store_id'];
    }
    $storeMdr->base_mdr = $data['mdr'];
    $this->mdr->save($storeMdr);
    
    return $listener->response(200);
  }
}

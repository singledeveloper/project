<?php

namespace Odeo\Domains\Channel\Model;

use Odeo\Domains\Core\Entity;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreMdr extends Entity
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['store_id', 'mdr'];

    public $timestamps = true;
}

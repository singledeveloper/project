<?php

namespace Odeo\Domains\Channel;

use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Constant\ChannelConfig;
use Odeo\Domains\Constant\ChannelStatus;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Core\Task;

class ChannelRequester {

  public function __construct() {
    $this->channel = app()->make(\Odeo\Domains\Channel\Repository\StoreChannelRepository::class);
    $this->channelCodes = app()->make(\Odeo\Domains\Channel\Repository\ChannelCodeRepository::class);
    $this->userCash = app()->make(\Odeo\Domains\Transaction\Repository\UserCashRepository::class);
    $this->cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $this->currency = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->selector = app()->make(\Odeo\Domains\Channel\ChannelSelector::class);
  }

  public function create(PipelineListener $listener, $data) {
    $channel = $this->channel->getNew();

    $storeDeposit = $this->cashManager->getStoreDepositBalance($data['store_id']);
    $userCash = $this->cashManager->getCashBalance($data['auth']['user_id']);

    // if(!isset($storeDeposit[$data['store_id']]) || $storeDeposit[$data['store_id']] < ChannelConfig::CHANNEL_MINIMUM_DEPOSIT) {
    //   if(!$userCash || $userCash[$data['auth']['user_id']] < ChannelConfig::CHANNEL_MINIMUM_DEPOSIT) {
    //     return $listener->response(400, 'A minimum oDeposit or oCash of ' . $this->currency->formatPrice(ChannelConfig::CHANNEL_MINIMUM_DEPOSIT)['formatted_amount'] . ' is required to create payment channel');
    //   }
    // }

    $channel->user_id = isAdmin($data) && isset($data['user_id']) ? $data['user_id'] : $data['auth']['user_id'];
    $channel->store_id = $data['store_id'];
    $channel->channel_data = json_encode($data['channel_data']);
    $channel->channel_settings = json_encode($data['channel_settings']);
    $channel->status = ChannelStatus::ACTIVE;

    $this->channel->save($channel);

    return $listener->response(201, $this->selector->_transforms($channel, $this->channel));
  }

  public function update(PipelineListener $listener, $data) {
    $channel = $this->channel->findById($data['channel_id']);
    if(!$channel) return $listener->response(400);

    if(isset($data['channel_settings'])) {
      $channel->channel_settings = json_encode($data['channel_settings']);
    }
    $this->channel->save($channel);

    return $listener->response(200);
  }

  public function updateData(PipelineListener $listener, $data) {
    $channel = $this->channel->findById($data['channel_id']);

    if(isset($data['code'])) {
      $channelCode = $this->channelCodes->findByAttributes('code', $data['code']);
      $this->channelCodes->getModel()->withTrashed()->where('code', $channel->code)->restore();

      $channel->code = $channelCode->code;
      $channel->hash = $channelCode->hash;
      $this->channelCodes->delete($channelCode);
    } else if(isset($data['device_id'])) {
      $channelData = json_decode($channel->channel_data);
      $channelData->device_id = $data['device_id'];
      $channel->channel_data = json_encode($channelData);
    }
    $this->channel->save($channel);

    return $listener->response(200);
  }
}

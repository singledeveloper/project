<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/11/19
 * Time: 12.26
 */

namespace Odeo\Domains\Approval\Job;


use Carbon\Carbon;
use Odeo\Domains\Account\Registrar;
use Odeo\Domains\Accounting\CompanyTransactionInformation\Repository\CompanyTransactionAdditionalInformationRepository;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Approval\Repository\ApprovalPendingRequestRepository;
use Odeo\Domains\Constant\BankAccount;
use Odeo\Domains\Constant\BankTransferInquiries;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\MandiriVer2\Repository\BankMandiriGiroInquiryRepository;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\Maybank\Repository\BankMaybankInquiryRepository;
use Odeo\Domains\Transaction\CashTransfer;
use Odeo\Jobs\Job;

class ApproveOCashTransfer extends Job {

  private $approvalPendingId, $approvalPendingRepo, $additionalInformationRepo;

  public function __construct($approvalPendingId) {
    parent::__construct();
    $this->approvalPendingId = $approvalPendingId;
  }

  public function init() {
    $this->approvalPendingRepo = app()->make(ApprovalPendingRequestRepository::class);
    $this->additionalInformationRepo = app()->make(CompanyTransactionAdditionalInformationRepository::class);
  }

  public function handle() {
    $this->init();
    $pending = $this->approvalPendingRepo->findById($this->approvalPendingId);
    $data = json_decode($pending->raw_data, true);
    $pipeline = new Pipeline();
    $pipeline->add(new Task(Registrar::class, 'autoSign', ['telephone' => purifyTelephone($data['to_phone'])]));
    $pipeline->add(new Task(CashTransfer::class, 'transfer'));
    $pipeline->enableTransaction();
    $pipeline->execute($data);

    $pending->processed_at = Carbon::now();
    if ($pipeline->fail()) $pending->status = Approval::FAILED;
    else {
      $pending->status = Approval::PROCESSED;
      if (isset($pipeline->data['id'])) {
        $this->additionalInformationRepo->updateInformationByReference(
          $pipeline->data['id'], 'approval', $pending->id
        );
      }
    }

    $this->approvalPendingRepo->save($pending);
  }

}
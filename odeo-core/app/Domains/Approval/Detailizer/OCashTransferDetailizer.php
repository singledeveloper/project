<?php

namespace Odeo\Domains\Approval\Detailizer;

use Odeo\Domains\Constant\Recurring;
use Odeo\Domains\Transaction\Helper\Currency;

class OCashTransferDetailizer implements DetailizerContract {

  private $currencyHelper;

  public function __construct() {
    $this->currencyHelper = app()->make(Currency::class);
  }

  private function formatRecurring($data) {
    return isset($data->transaction_type) && $data->transaction_type != 'immediate' ?
      Recurring::getTypeName($data->recurring_type .
        ($data->recurring_type == Recurring::TYPE_MONTH ? ('_' . $data->recurring_month) : '') .
        '_' . $data->recurring_time) : 'No Recurring';
  }

  public function format($data) {
    return [[
      'name' => 'Recurring',
      'value' => $this->formatRecurring($data)
    ], [
      'name' => 'Amount',
      'value' => $this->currencyHelper->formatPrice($data->amount)['formatted_amount']
    ], [
      'name' => 'Destination',
      'value' => $data->to_phone
    ]];
  }

  public function description($data) {
    return $this->currencyHelper->formatPrice($data->amount)['formatted_amount']
      . ' - ' . $data->to_phone . ' - ' . $this->formatRecurring($data);
  }

  public function cancel($data) {
    // TODO: Implement cancel() method.
  }


}
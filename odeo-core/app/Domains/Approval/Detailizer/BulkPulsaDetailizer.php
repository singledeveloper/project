<?php

namespace Odeo\Domains\Approval\Detailizer;

use Odeo\Domains\Constant\BulkPurchase;
use Odeo\Domains\Order\Repository\PulsaBulkRepository;

class BulkPulsaDetailizer implements DetailizerContract {

  public function format($data) {
    return [];
  }

  public function description($data) {
    return $data->description;
  }

  public function cancel($data) {
    $pulsaBulkRepo = app()->make(PulsaBulkRepository::class);
    if ($bulk = $pulsaBulkRepo->findById($data->reference_id)) {
      $bulk->status = BulkPurchase::REJECTED;
      $pulsaBulkRepo->save($bulk);
    }
  }

}
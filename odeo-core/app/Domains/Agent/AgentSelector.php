<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/6/17
 * Time: 8:30 PM
 */

namespace Odeo\Domains\Agent;


use Carbon\Carbon;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Subscription\ManageInventory\Repository\StoreInventoryVendorRepository;

class AgentSelector {

  private $agents, $purchaseInformations, $currencyHelper, $stores;

  public function __construct() {
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->purchaseInformations = app()->make(\Odeo\Domains\Order\Helper\AgentMasterSalesManager::class);
    $this->currencyHelper = app()->make(\Odeo\Domains\Transaction\Helper\Currency::class);
    $this->inventoryVendors = app()->make(StoreInventoryVendorRepository::class);
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
  }

  public function extendSupplyGroup($data, Repository $repository) {
    if ($agentIds = $repository->beginExtend($data, 'agent_id')) {

      $agentGroups = [];
      foreach ($this->agents->getAgentSupplyGroup($agentIds) as $item) {
        $agentGroups[$item->id] = $item->name . ($item->name != $item->new_name && $item->new_name != null ? (' > ' . $item->new_name) : '');
      }

      foreach ($agentIds as $item) {
        if (!isset($agentGroups[$item])) $agentGroups[$item] = '';
      }

      $repository->addExtend('supply_group', $agentGroups);
    }

    return $repository->finalizeExtend($data);
  }

  public function extendPurchaseInformation($items) {

    $today = Carbon::now();

    $dateRange = [
      [
        'date' => $today->toDateString(),
        'name' => 'today'
      ],
      [
        'date' => $today->subDay(1)->toDateString(),
        'name' => 'yesterday'
      ]
    ];

    $purchaseInformations = $this->purchaseInformations->getPurchaseInformationByDate(
      array_pluck($items, 'user.id'),
      array_pluck($dateRange, 'date')
    );

    $iterator = 0;

    foreach ($items as $key => $item) {

      foreach ($dateRange as $date) {
        $items[$key]['purchase_information'][$date['name']] = [
          'total_sales_quantity' => isset($item['agent_id']) && isset($purchaseInformations[$iterator][1]) ? $purchaseInformations[$iterator][1] : 0,
          'total_sales_amount' => $this->currencyHelper->formatPrice(isset($item['agent_id']) && isset($purchaseInformations[$iterator][0]) ? $purchaseInformations[$iterator][0] : 0)
        ];
        ++$iterator;
      }

    }

    return $items;
  }


  public function parseRoleData($data) {

    if (isset($data->agent_joined_at) && isset($data->server_joined_at)) {

      return [
        min($data->server_joined_at, $data->agent_joined_at),
        AgentConfig::ROLE_AGENT_SERVER
      ];

    } else if (isset($data->server_joined_at)) {
      return [
        $data->server_joined_at,
        AgentConfig::ROLE_SERVER
      ];
    } else {
      return [
        $data->agent_joined_at,
        AgentConfig::ROLE_AGENT
      ];
    }
  }


  public function get(PipelineListener $listener, $data) {

    $agentRows = [];

    $this->agents->normalizeFilters($data);
    $agentUsers = $this->agents->getMyAgents();

    $incrementAgent = 9000000 + $data['offset']; #temp android

    foreach ($agentUsers['result'] as $user) {

      $temp = [];

      $temp['agent_id'] = isset($user->agent_id) ? $user->agent_id : $incrementAgent++; #temp android
      $temp['user'] = [
        'id' => $user->user_id,
        'phone_number' => $user->telephone,
        'name' => $user->name
      ];

      list($joined, $role) = $this->parseRoleData($user);
      $temp['requested_at'] = $temp['joined_at'] = Carbon::parse($joined)->toDateString();
      $temp['role'] = $role;

      $agentRows[] = $temp;

    }

    if (count($agentRows) > 0) {
      if (isset($data['extend_supply'])) {
        $agentRows = $this->extendSupplyGroup($agentRows, $this->agents);
      }
      return $listener->response(200, array_merge(
        ['agents' => $this->extendPurchaseInformation($agentRows)],
        [
          "metadata" => [
            "resultset" => $agentUsers['pagination']
          ]
        ]
      ));
    }
    return $listener->response(204, [], false, true);
  }

  public function getPendingAgent(PipelineListener $listener, $data) {

    $agentOwner = $this->stores->findOwner($data['store_id']);

    $this->agents->normalizeFilters(array_merge($data, [
      'user_id' => $agentOwner->user_id
    ]));

    $agents = [];

    foreach ($this->agents->getRequestedAgents() as $agent) {
      $agents[] = [
        'agent_id' => $agent->id,
        'requested_at' => $agent->requested_at,
        'user' => [
          'id' => $agent->user->id,
          'name' => $agent->user->name,
          'phone_number' => $agent->user->telephone,
        ],
      ];
    }

    if (count($agents) > 0) {
      return $listener->response(200, array_merge(
        ['pending_agents' => $agents],
        $this->agents->getPagination()
      ));
    }

    return $listener->response(204);


  }

}
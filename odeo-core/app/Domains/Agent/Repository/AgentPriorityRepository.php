<?php

namespace Odeo\Domains\Agent\Repository;

use Carbon\Carbon;
use Odeo\Domains\Agent\Model\AgentPriority;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\AgentConfig;

class AgentPriorityRepository extends Repository {

  public function __construct(AgentPriority $agentPriority) {
    $this->model = $agentPriority;
  }

  public function getLatestPrioritiesFromUserId($userId) {
    return $this->model
      ->select(\DB::raw('service_id, max(priority) as priority'))
      ->join('agents', 'agents.id', '=', 'agent_id')
      ->where('user_id', $userId)
      ->groupBy('service_id')
      ->orderBy('service_id')
      ->get();
  }

  public function getPreferredPrioritiesByUserId($userId) {
    return $this->model
      ->select('*', 'agent_priorities.id')
      ->join('agents', 'agents.id', '=', 'agent_id')
      ->where('user_id', $userId)
      ->where('priority', '>', 0)
      ->get();
  }

  public function getByUserIdAndStoreId($userId, $storeId) {
    return $this->model
      ->select('*', 'agent_priorities.id')
      ->join('agents', 'agents.id', '=', 'agent_id')
      ->where('user_id', $userId)
      ->where('store_referred_id', $storeId)
      ->get();
  }

  public function getPrioritiesByUserIdAndServiceId($userId, $serviceId) {
    return $this->model
      ->select('agent_priorities.id' ,'store_referred_id', 'name', 'stores.status', 'priority', 'is_selected')
      ->join('agents', 'agents.id', '=', 'agent_id')
      ->join('stores', 'stores.id', '=', 'store_referred_id')
      ->where('user_id', $userId)
      ->where('service_id', $serviceId)
      ->whereIn('agents.status', [AgentConfig::PENDING, AgentConfig::ACCEPTED])
      ->orderBy('priority')
      ->orderBy('name')
      ->get();
  }

  public function getPreferredPrioritesExceptIds($userId, $serviceId, $ids) {
    return $this->model
      ->select('*', 'agent_priorities.id')
      ->join('agents', 'agents.id', '=', 'agent_id')
      ->where('user_id', $userId)
      ->where('service_id', $serviceId)
      ->where('priority', '>', 0)
      ->whereNotIn('agent_priorities.id', $ids)
      ->get();
  }

}
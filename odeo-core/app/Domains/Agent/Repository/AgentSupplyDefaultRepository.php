<?php

namespace Odeo\Domains\Agent\Repository;

use Odeo\Domains\Agent\Model\AgentSupplyDefault;
use Odeo\Domains\Core\Repository;

class AgentSupplyDefaultRepository extends Repository {

  public function __construct(AgentSupplyDefault $agentSupplyDefault) {
    $this->model = $agentSupplyDefault;
  }

  public function findByStoreId($storeId) {
    return $this->model->where('store_id', $storeId)->first();
  }

}
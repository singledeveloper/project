<?php

namespace Odeo\Domains\Agent;


use Odeo\Domains\Agent\Repository\AgentPriorityRepository;
use Odeo\Domains\Constant\AssetAws;
use Odeo\Domains\Core\PipelineListener;

class AgentPrioritySelector {

  private $agentPriorities;

  public function __construct() {
    $this->agentPriorities = app()->make(AgentPriorityRepository::class);
  }

  public function getPriorities(PipelineListener $listener, $data) {
    $agentPrioritiesRemoved = [];

    $agentPrioritiesAdded[] = [
      'id' => 0,
      'store_id' => 0,
      'store_name' => 'Top',
      'store_logo' => AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::TOP_STORE_LOGO),
      'store_status' => '50000'
    ];

    $agentPriorities = $this->agentPriorities->getPrioritiesByUserIdAndServiceId(
      $data['auth']['user_id'],
      $data['service_id']
    );
    
    foreach ($agentPriorities as $agentPriority) {
      $id = $agentPriority->id;
      $storeId = $agentPriority->store_referred_id;
      $name = $agentPriority->name;
      $priority = $agentPriority->priority;

      $agentPriority = [
        'id' => $id,
        'store_id' => $storeId,
        'store_name' => $name,
        'store_logo' => AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::DEFAULT_STORE_LOGO),
        'store_status' => $agentPriority->status
      ];

      if ($priority === 0) {
        $agentPrioritiesRemoved[] = $agentPriority;
      } else {
        $agentPrioritiesAdded[] = $agentPriority;
      }
    }

    $selectedAgentPriorities = $agentPriorities->where('is_selected', true)->first();
    $selectedPriorityId = $selectedAgentPriorities ? $selectedAgentPriorities->id : 0;

    return $listener->response(200, array_merge(
      [
        'added' => $agentPrioritiesAdded,
        'removed' => $agentPrioritiesRemoved,
        'selected_priority_id' => $selectedPriorityId
      ]
    ));
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 3/8/17
 * Time: 11:21 PM
 */

namespace Odeo\Domains\Agent;

use Carbon\Carbon;
use Odeo\Domains\Constant\AgentConfig;
use Odeo\Domains\Constant\NotificationType;
use Odeo\Domains\Constant\NotificationGroup;
use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Constant\AssetAws;

class AgentRequester {

  private $users, $agents, $agentPriorities, $stores, $notifications, $masterSettingRetriever, $userReferral;

  public function __construct() {
    $this->users = app()->make(\Odeo\Domains\Account\Repository\UserRepository::class);
    $this->agents = app()->make(\Odeo\Domains\Agent\Repository\AgentRepository::class);
    $this->agentPriorities = app()->make(\Odeo\Domains\Agent\Repository\AgentPriorityRepository::class);
    $this->stores = app()->make(\Odeo\Domains\Subscription\Repository\StoreRepository::class);
    $this->notifications = app()->make(\Odeo\Domains\Notification\Helper\Builder::class);
    $this->masterSettingRetriever = app()->make(\Odeo\Domains\Agent\Helper\MasterSettingRetriever::class);
    $this->userReferral = app()->make(\Odeo\Domains\Marketing\Userreferral\Repository\UserReferralCodeRepository::class);
  }

  public function _transforms($item, Repository $repository) {
    $agent=[];
    $agent['store_id'] = $item->store_referred_id;
    $agent['store_name'] = $item->name;
    $agent['is_active'] = StoreStatus::isActive($item->store_status);
    $agent['logo'] = AssetAws::getAssetPath(AssetAws::BUSINESS_LOGO, AssetAws::DEFAULT_STORE_LOGO);
    $agent['master_code'] = $item->inputted_master_code;
    $agent['status_code'] = $item->status;

    return $agent;
  }

  public function masterInformation(PipelineListener $listener, $data) {

    $agent = $this->agents->findByUserId($data['auth']['user_id']);
    $userReferral = $this->userReferral->findByUserId($data['auth']['user_id']);

    $isAvailable = !$agent || ($agent && $agent->status == AgentConfig::AVAILABLE);

    $contactPerson = [];
    $description = '';

    if ($agent) {

      $master = $this->users->findById($agent->user_referred_id);

      $contactPerson = [
        'name' => maskMessage($master->name),
        'phone_number' => '-',
        'email' => '-'
      ];

      $description = (function () use ($agent) {

        switch ($agent->status) {
          case AgentConfig::ACCEPTED:
            return trans('agent.description_for_agent_when_accepted');
          case AgentConfig::PENDING:
            return trans('agent.description_for_agent_when_pending');
          default:
            return trans('agent.description_for_agent_when_available');
        }
      })();

    }


    return $listener->response(200, [
      'inputted_master_code' => $isAvailable ? '-' : strtoupper($agent->inputted_master_code),
      'inputted_referral_code' => $userReferral ? strtoupper($userReferral->referral_code) : '-',
      'can_input' => $isAvailable,
      'contact_person' => !$isAvailable ? $contactPerson : [],
      'description' => $description
    ]);

  }

  public function masterList(PipelineListener $listener, $data) {

    $userReferral = $this->userReferral->findByUserId($data['auth']['user_id']);

    $agents = [];
    foreach ($this->agents->getMastersByUserId($data['auth']['user_id']) as $item) {
      $agents[] = $this->_transforms($item, $this->agents);
    }

    return $listener->response(200, [
      'inputted_referral_code' => $userReferral ? strtoupper($userReferral->referral_code) : '-',
      'masters' => $agents
    ]);

  }

  public function addMaster(PipelineListener $listener, $data) {
    $masterCode = $data['master_code'];
    if (strlen($masterCode) < 4) {
      return $listener->response(400, 'Invalid master code');
    }

    $userId = isset($data['auth']) ? $data['auth']['user_id'] : $data['user_id'];

    if (
      ($agent = $this->agents->findByUserIdAndMasterCode($userId, $masterCode))
      && in_array($agent->status, [AgentConfig::PENDING, AgentConfig::ACCEPTED])
    ) {
      return $listener->response(400, 'cannot request the same agency twice');
    }

    $referredStore = $this->stores->findBySubdomain(strtolower($data["master_code"]));
    if (!$referredStore || !StoreStatus::isActive($referredStore->status)) {
      return $listener->response(400, 'Invalid master code');
    }

    $user = $referredStore->owner->first();
    $me = $this->users->findById($userId);

    $isNewAgent = !$agent;
    if ($isNewAgent){
      $agent = $this->agents->getNew();
    }

    $agent->user_id = $userId;
    $agent->user_referred_id = $user->id;
    $agent->store_referred_id = $referredStore->id;
    $agent->status = AgentConfig::PENDING;
    $agent->requested_at = datenow();
    $agent->joined_at = null;
    $agent->inputted_master_code = $data['master_code'];

    $this->agents->save($agent);

    if ($isNewAgent) {
      $this->addAgentPriorities($agent);
    } else {
      $this->updateAgentPriorities($userId, $referredStore->id);
    }

    $setting = $this->masterSettingRetriever->retrieve($referredStore->id);

    if ($setting->auto_approve_agent) {

      $this->notifications->setup($user->id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
      $this->notifications->autoAcceptAgency($me->name ?? 'someone');

      $listener->addNext(new Task(AgentAcceptor::class, 'accept', [
        'agent_id' => $agent->id,
        'store_id' => $referredStore->id,
        'is_auto' => true
      ]));

    } else {
      $this->notifications->setup($user->id, NotificationType::CHECK, NotificationGroup::ACCOUNT);
      $this->notifications->requestAgency($me->name ?? 'someone');
    }

    $listener->pushQueue($this->notifications->queue());

    return $listener->response(200);
  }

  private function addAgentPriorities($agent) {
    $newAgentPriorities = array();
    $now = Carbon::now();
    $userPriorities = $this->agentPriorities->getLatestPrioritiesFromUserId($agent->user_id);
    if (count($userPriorities) === 0) {
      $userPriorities = [];
      foreach(Service::getServicesForAgents() as $serviceId) {
        $userPriorities[] = (object) [
          'service_id' => $serviceId, 
          'priority' => 0
        ];
      }
      $userPriorities = collect($userPriorities);
    }
    foreach($userPriorities as $userPriority) {
      $priority = ($userPriority->priority + 1) % (AgentConfig::MAX_PRIORITY + 1);
      $newAgentPriorities[] = [
        'agent_id' => $agent->id,
        'service_id' => $userPriority->service_id,
        'priority' => $priority,
        'created_at' => $now,
        'updated_at' => $now
      ];
    }
    $this->agentPriorities->saveBulk($newAgentPriorities);
  }

  private function updateAgentPriorities($userId, $storeId) {
    $agentPriorities = $this->agentPriorities->getByUserIdAndStoreId($userId, $storeId);
    if ($agentPriorities->isEmpty()) {
      return;
    }

    $updatedAgentPriorities = array();
    $now = Carbon::now();
    $userPriorities = $this->agentPriorities->getLatestPrioritiesFromUserId($userId)->keyBy('service_id');
    
    foreach($agentPriorities as $agentPriority) {
      $priority = ($userPriorities[$agentPriority->service_id]->priority + 1) % (AgentConfig::MAX_PRIORITY + 1);
      $updatedAgentPriorities[] = [
        'id' => $agentPriority->id,
        'priority' => $priority,
        'updated_at' => $now
      ];
    }
    $this->agentPriorities->updateBulk($updatedAgentPriorities);
  }

}

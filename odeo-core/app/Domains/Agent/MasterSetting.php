<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/5/17
 * Time: 16:21
 */

namespace Odeo\Domains\Agent;

use Odeo\Domains\Core\PipelineListener;

class MasterSetting {

  private $masterSettings;

  public function __construct() {
    $this->masterSettings = app()->make(\Odeo\Domains\Agent\Repository\StoreAgentMasterSettingRepository::class);
  }

  public function toggleAutoApproveAgent(PipelineListener $listener, $data) {
    $this->masterSettings->getModel()->where('store_id', $data['store_id'])->update([
      'auto_approve_agent' => \DB::raw('not auto_approve_agent')
    ]);
    return $listener->response(200);
  }

}
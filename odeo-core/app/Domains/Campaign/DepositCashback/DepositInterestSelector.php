<?php

namespace Odeo\Domains\Campaign\DepositCashback;

use Carbon\Carbon;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Transaction\Repository\CashTransactionRepository;

class DepositInterestSelector {

  private $transactions;

  public function __construct() {
    $this->transactions = app()->make(CashTransactionRepository::class);
  }

  public function summary(PipelineListener $listener, $data) {

    $now = Carbon::now();

    $startDate = $data['start_date'] ?? $now->subWeek(1)->toDateString();
    $endDate = $data['end_date'] ?? $now->toDateString();

    $result = $this->transactions->depositInterestSummary($startDate, $endDate);
    return $listener->response(200, [
      'data' => $result
    ]);
  }
}


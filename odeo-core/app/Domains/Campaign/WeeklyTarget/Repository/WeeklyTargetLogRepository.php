<?php

namespace Odeo\Domains\Campaign\WeeklyTarget\Repository;


use Odeo\Domains\Campaign\WeeklyTarget\Model\WeeklyTargetLog;
use Odeo\Domains\Core\Repository;

class WeeklyTargetLogRepository extends Repository {


  public function __construct(WeeklyTargetLog $log) {
    $this->model = $log;
  }

}
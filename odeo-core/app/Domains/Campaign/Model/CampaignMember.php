<?php

namespace Odeo\Domains\Campaign\Model;

use Odeo\Domains\Core\Entity;

class CampaignMember extends Entity {
  protected $hidden = ['created_at', 'updated_at'];

}

<?php

namespace Odeo\Domains\Faq;


use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Faq\Repository\FaqRepository;

class FaqSelector {

  private $faqRepo;

  public function __construct() {
    $this->faqRepo = app()->make(FaqRepository::class);
  }

  public function get(PipelineListener $listener, $data) {
    $this->faqRepo->normalizeFilters($data);
    if ($faqs = $this->getFaq()) {
      return $listener->response(200, ["faqs" => $faqs]);
    }
    return $listener->response(204, ["faqs" => []]);
  }

  // this function exists for BI compliance purpose, currently used on "ocash" app
  private function getFaq() {
    return isOcashApp() ?
      $this->faqRepo->getOcashFaq() :
      $this->faqRepo->get();
  }

}

<?php

namespace Odeo\Domains\Faq;

use Odeo\Domains\Core\PipelineListener;

class FaqInserter {

  private $faqs;

  public function __construct() {
    $this->faqs = app()->make(\Odeo\Domains\Faq\Repository\FaqRepository::class);
  }

  public function insert(PipelineListener $listener, $data) {
    $faq = $this->faqs->getNew();

    $faq->question = $data['question'];
    $faq->answer = $data['answer'];
    $faq->priority = $data['priority'];
    $faq->slug = strtolower(trim($data['slug']));
    $faq->group = isset($data['group']) ? $data['group'] : '';
    $this->faqs->save($faq);

    return $listener->response(200);
  }
}
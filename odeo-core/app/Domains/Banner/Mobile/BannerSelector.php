<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/6/17
 * Time: 4:51 PM
 */

namespace Odeo\Domains\Banner\Mobile;

use Odeo\Domains\Constant\AwsConfig;
use Odeo\Domains\Core\PipelineListener;
use Odeo\Domains\Core\Repository;
use Odeo\Domains\Core\SelectorListener;

class BannerSelector implements SelectorListener {

  private $banners;

  public function __construct() {
    $this->banners = app()->make(\Odeo\Domains\Banner\Mobile\Repository\MobileBannerRepository::class);
  }

  public function _transforms($item, Repository $repository) {


    $response['image'] = AwsConfig::S3_BASE_URL . $item['url'];
    $response['trigger'] = $item['trigger'];

    if (count($item['additional']) > 0) {
      $response['additional'] = json_decode($item['additional']);
    }

    return $response;
  }

  public function _extends($data, Repository $repository) {
    return $data;
  }

  public function get(PipelineListener $listener, $data) {

    $this->banners->normalizeFilters($data);

    $banners = [];
    foreach ($this->banners->getActive() as $banner) {
      $banners[] = $this->_transforms($banner, $this->banners);
    }
    if (sizeof($banners) > 0) {
      return $listener->response(200, $banners);
    }
    return $listener->response(204);
  }

}

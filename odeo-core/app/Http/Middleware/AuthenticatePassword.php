<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\Loginner;
use Odeo\Domains\Account\DeviceChecker;

class AuthenticatePassword {

  use \Odeo\Domains\Core\IO;

  public function handle($request, Closure $next) {
    list($isValid, $data) = $this->validateData([
      'auth.user_id' => 'required',
      'pin' => 'required'
    ]);
    if (!$isValid) return $data;

    $pipeline = new Pipeline;
    $pipeline->add(new Task(DeviceChecker::class, 'check'));
    $pipeline->add(new Task(Loginner::class, 'checkPin'));
    $pipeline->execute($data);

    if ($pipeline->fail()) return $this->buildErrorsResponse($pipeline->errorMessage, $pipeline->statusCode, "", $pipeline->errorStatus);

    return $next($request);
  }
}

<?php

namespace Odeo\Http\Middleware;

use Closure;
use Odeo\Domains\Account\Jobs\SaveAccessLogs;

class AdminLogger {

  public function handle($request, Closure $next) {
    $url = $request->url();
    if (strpos($url, '/v1/pulsa/balance-information') === false &&
      strpos($url, '/v1/api-disbursement/suspect-count') === false) {
      $data = $request->all();

      dispatch(new SaveAccessLogs([
        'user_id' => $data['auth']['user_id'],
        'path' => $url,
        'description' => json_encode($data)
      ]));
    }

    return $next($request);
  }
}

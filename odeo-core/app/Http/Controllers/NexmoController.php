<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 2/7/17
 * Time: 4:17 PM
 */
namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Vendor\SMS\Nexmo\Notifier;
use Odeo\Domains\Vendor\SMS\Nexmo\Receiver;

class NexmoController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function notify() {
    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function inbound() {
    $this->pipeline->add(new Task(Receiver::class, 'receive'));
    return $this->executeAndResponse($this->getRequestData());
  }

}
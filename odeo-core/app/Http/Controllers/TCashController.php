<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Biller\TCash\AccountRequester;
use Odeo\Domains\Biller\TCash\AccountSelector;
use Odeo\Domains\Biller\TCash\DepositDistributor;
use Odeo\Domains\Core\Task;

class TCashController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function requestOtp() {

    list($isValid, $data) = $this->validateData([
      'telephone' => 'required',
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'requestOtp'));
    return $this->executeAndResponse($data);
  }

  public function verifyOtp() {

    list($isValid, $data) = $this->validateData([
      'tcash_user_id' => 'required',
      'otp' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'verifyOtp'));
    return $this->executeAndResponse($data);
  }

  public function login() {

    list($isValid, $data) = $this->validateData([
      'tcash_user_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'login'));
    return $this->executeAndResponse($data);
  }

  public function distributeDeposit() {

    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(DepositDistributor::class, 'distribute'));
    return $this->executeAndResponse($data);
  }

  public function getHistories() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AccountRequester::class, 'getHistories'));
    return $this->executeAndResponse($data);
  }

  public function get() {

    $this->pipeline->add(new Task(AccountSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function test() {

    list($isValid, $data) = $this->validateData([
      'tcash_user_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AccountRequester::class, 'test'));
    return $this->executeAndResponse($data);
  }

}

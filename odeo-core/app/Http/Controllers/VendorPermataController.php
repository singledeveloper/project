<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 22/02/19
 * Time: 12.57
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BalanceSelector;
use Odeo\Domains\Vendor\Permata\TransferRecordSelector;

class VendorPermataController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getPermataRecord() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getInformation() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BalanceSelector::class,'getBalance', [
      'id' => VendorDisbursement::PERMATA
    ]));
    return $this->executeAndResponse($data);
  }

  public function getSuspectDisbursements() {
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'getSuspectDisbursements'));
    return $this->executeAndResponse();
  }

}
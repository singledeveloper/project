<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PostpaidViewer;
use Odeo\Domains\Supply\Biller\Inquirer;

class PostpaidSwitcherController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function plnReport() {
    list($isValid, $data) = $this->validateData([
      'odps' => 'required|integer',
      'ref' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PostpaidViewer::class, 'getPlnReport'));
    $this->pipeline->execute($data);

    if (isset($this->pipeline->data['view_page'])) {
      return view($this->pipeline->data['view_page'], $this->pipeline->data['view_data']);
    }
    else abort(404);
  }

  public function bpjsReport() {
    list($isValid, $data) = $this->validateData([
      'odps' => 'required|integer',
      'ref' => 'required'
    ]);

    if(!$isValid) return $data;

    $this->pipeline->add(new Task(PostpaidViewer::class, 'getBpjsReport'));
    $this->pipeline->execute($data);

    if (isset($this->pipeline->data['view_page'])) {
      return view($this->pipeline->data['view_page'], $this->pipeline->data['view_data']);
    }
    else abort(404);
  }

  public function getPostpaidList() {
    $this->pipeline->add(new Task(PostpaidViewer::class, 'getPostpaidInventories'));
    return $this->executeAndResponse($this->getRequestData());
  }
  
}

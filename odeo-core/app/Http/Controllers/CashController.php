<?php

namespace Odeo\Http\Controllers;

use Carbon\Carbon;
use Odeo\Domains\Approval\ApprovalSelector;
use Odeo\Domains\Approval\Helper\Approval;
use Odeo\Domains\Constant\UserKycStatus;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Account\Loginner;
use Odeo\Domains\Account\Registrar;
use Odeo\Domains\Channel\ChannelSelector;
use Odeo\Domains\Channel\ChannelPaymentProcessor;
use Odeo\Domains\Channel\ChannelTransformer;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\CashSelector;
use Odeo\Domains\Transaction\CashTransfer;
use Odeo\Domains\Transaction\Helper\Currency;
use Odeo\Domains\Transaction\TempCashSelector;
use Odeo\Domains\Transaction\DepositRequester;
use Odeo\Domains\Transaction\CashUpdater;
use Odeo\Domains\Constant\CashType;
use Odeo\Domains\Constant\CashConfig;

class CashController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function cash() {
    $data = $this->getRequestData();
    if (!isset($data["currency"])) $currency = Currency::IDR;
    else $currency = $data["currency"];

    $userId = $data["auth"]["user_id"];
    $lockedCash = [];
    $cashManager = app()->make(\Odeo\Domains\Transaction\Helper\CashManager::class);
    $cash = $cashManager->getCashBalance($userId, $currency, $lockedCash);

    $kycRepo = app()->make(\Odeo\Domains\Account\Repository\UserBusinessKycRepository::class);
    $kyc = $kycRepo->checkCurrentKyc($userId);

    return $this->buildSuccessResponse([
      CashType::OCASH => $cash[$userId],
      'locked_cash' => $lockedCash[$userId],
      'last_transaction' => "" . Carbon::now('Asia/Jakarta'),
      'kyc_status' => $kyc ? $kyc->status : UserKycStatus::PENDING
    ]);

  }

  public function cashDepositRequest() {
    list($isValid, $data) = $this->validateData([
      'created_at' => 'required_without:store_id|date|before:now',
      'store_id' => 'required_without:created_at'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TempCashSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function convert() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'cash_type' => 'required|in:deposit',
      'cash.amount' => 'required|min:0',
      'cash.currency' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(DepositRequester::class, 'request'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function transferOCash() {
    list($isValid, $data) = $this->validateData([
      'to_phone' => 'required',
      //'amount' => 'required|integer|min:1000|max:' . CashConfig::MAXIMUM_CASH_OUT_PER_DAY,
      'amount' => 'required|integer|min:1000',
      'currency' => 'required'
    ]);

    if (!$isValid) return $data;

    $firstPipelineData = [];
    if ($data['amount'] > CashConfig::MIN_TRANSFER_REQUIRE_PIN) {
      $this->pipeline->add(new Task(Loginner::class, 'checkPin'));
      $this->pipeline->execute($data);
      if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
      $firstPipelineData = $this->pipeline->data;
      $this->pipeline = $this->pipeline->clear();
    }

    $this->pipeline->add(new Task(ApprovalSelector::class, 'guardNonMakerEndpoint', ['path' => Approval::PATH_OCASH_TRANSFER]));
    $this->pipeline->add(new Task(Registrar::class, 'autoSign', ['telephone' => purifyTelephone($data['to_phone'])]));
    $this->pipeline->add(new Task(CashTransfer::class, 'transfer'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute(array_merge($data, $firstPipelineData));
    if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    return $this->buildSuccessResponse(array_diff_key($this->pipeline->data, $data), 200);
  }

  public function transferOCashMaker() {
    list($isValid, $data) = $this->validateData([
      'to_phone' => 'required',
      'amount' => 'required|integer|min:1000',
      'currency' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(Registrar::class, 'autoSign', ['telephone' => purifyTelephone($data['to_phone'])]));
    $this->pipeline->add(new Task(CashTransfer::class, 'transfer'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function payOCash() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required',
      'amount.amount' => 'required|integer',
      'amount.currency' => 'required'
    ]);

    if (!$isValid) return $data;

    $requirePin = $data['amount']['amount'] > CashConfig::MIN_PAYMENT_REQUIRE_PIN;
    $firstPipelineData = [];
    if ($requirePin) {
      $this->pipeline->add(new Task(Loginner::class, 'checkPin'));
      $this->pipeline->execute($data);
      if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
      $firstPipelineData = $this->pipeline->data;
      $this->pipeline = $this->pipeline->clear();
    }

    $this->pipeline->add(new Task(ChannelSelector::class, 'getDetail', [
      'expand' => 'store,user'
    ]));
    $this->pipeline->add(new Task(CashUpdater::class, 'pay', [
      'amount' => $data['amount']['amount'],
      'currency' => $data['amount']['currency'],
    ]));
    $this->pipeline->add(new Task(ChannelPaymentProcessor::class, 'newUserCashback'));
    $this->pipeline->add(new Task(ChannelPaymentProcessor::class, 'MDR'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute(array_merge($data, $firstPipelineData));
    if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    return $this->buildSuccessResponse([], 200);
  }
}

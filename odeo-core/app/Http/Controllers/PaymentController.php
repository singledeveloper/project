<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/27/16
 * Time: 2:16 PM
 */

namespace Odeo\Http\Controllers;

use Illuminate\Http\Request;
use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Account\Loginner;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Domains\Order\OrderSelector;
use Odeo\Domains\Payment\Akulaku\Director;
use Odeo\Domains\Payment\Bni\BniNotifier;
use Odeo\Domains\Payment\Odeo\Terminal\TerminalGuard;
use Odeo\Domains\Payment\PaymentConfigurator;
use Odeo\Domains\Payment\PaymentConfirmationSelector;
use Odeo\Domains\Payment\PaymentConfirmator;
use Odeo\Domains\Payment\PaymentInformationSelector;
use Odeo\Domains\Payment\PaymentListSelector;
use Odeo\Domains\Payment\PaymentNotificator;
use Odeo\Domains\Payment\PaymentOpenRequester;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\Transaction\TempDepositRequester;

class PaymentController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  //for user
  public function getList() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required|integer',
    ]);
    if (!$isValid) return $data;

    if (isNewDesign()) {
      $this->pipeline->add(new Task(PaymentListSelector::class, 'getGroupedEnabled'));
    } else {
      $this->pipeline->add(new Task(PaymentListSelector::class, 'getEnabled'));
    }

    return $this->executeAndResponse($data);
  }

  public function terminalGetList() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required|integer',
      'phone_number' => 'required',
    ]);
    if (!$isValid) return $data;
    $this->pipeline->add(new Task(TerminalGuard::class, 'validateOrderUser'));
    $this->pipeline->add(new Task(PaymentListSelector::class, 'getGroupedEnabled'));
    return $this->executeAndResponse($data);
  }

  public function terminalGetListWoOrder() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PaymentListSelector::class, 'getEnabled'));
    return $this->executeAndResponse($data);
  }

  //for admin
  public function getAll() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PaymentInformationSelector::class, 'getAll'));

    return $this->executeAndResponse($data);
  }

  public function requestPayment() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required_without:idt',
      'opc' => 'required_without:idt'
    ]);

    if (!$isValid) return $data;

    $data['client_ip_address'] = getClientIP();

    if (!isset($data['idt'])) {
      $this->pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
      $this->pipeline->enableTransaction();
      $this->pipeline->enableAntiSpam($data['order_id']);
      $this->pipeline->execute($data);
      if ($this->pipeline->fail()) {
        return $this->buildErrorsResponse($this->pipeline->errorMessage);
      }
    }

    $this->pipeline = $this->pipeline->clear();
    $this->pipeline->add(new Task(PaymentRequester::class, 'request'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    if (isset($this->pipeline->data['view_page'])) {

      if ($this->pipeline->data['view_page'] == 'payment.doku-v1') {

        if (!isset($data['view_except']) || !in_array('order', $data['view_except'])) {
          $pipeline2 = new Pipeline;
          $pipeline2->add(new Task(OrderSelector::class, 'find', [
          ]));
          $pipeline2->execute([
            'order_id' => $this->pipeline->data['view_data']['orderID'],
            'expand' => 'payment,payment_logo,payment_fee'
          ]);
          $this->pipeline->data['view_data']['order'] = $pipeline2->data;
        }
      }

      return view($this->pipeline->data['view_page'], $this->pipeline->data['view_data']);
    }

    return $this->buildResponse(200, $this->pipeline->data);
  }

  public function openPayment() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
    ]);

    if (!$isValid) return $data;

    $firstPipelineData = [];
    if ((isset($_GET['with_password']) && $_GET['with_password']) || (isset($data['with_password']) && $data['with_password'])) {
      $data['with_password'] = true;
      $this->pipeline->add(new Task(Loginner::class, 'checkPin'));
      $this->pipeline->execute($data);
      if ($this->pipeline->fail()) return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
      $firstPipelineData = $this->pipeline->data;
      $this->pipeline = $this->pipeline->clear();
    }

    $this->pipeline->add(new Task(PaymentOpenRequester::class, 'open'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse(array_merge($data, $firstPipelineData));

  }

  public function confirmPayment() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'confirmation_name' => 'required_if:is_auto_confirm,false|min:2|max:50',
      'confirmation_number' => 'required_if:is_auto_confirm,false|min:2|max:50',
      'confirmation_amount' => 'required|numeric'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PaymentConfirmator::class, 'confirm'));

    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    return $this->buildSuccessResponse($data);

  }

  public function cancelConfirmPayment() {

    list($isValid, $data) = $this->validateData([
      'order_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(OrderCanceller::class, 'cancelConfirm'));

    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function getConfirmationDetail($orderId) {

    $data['order_id'] = $orderId;

    $this->pipeline->add(new Task(PaymentConfirmationSelector::class, 'getDetail'));

    return $this->executeAndResponse($data);

  }

  public function dokuSubmit() {

    list($isValid, $data) = $this->validateData([
      'doku_amount' => 'required',
      'doku_currency' => 'required',
      'opc' => 'required'
    ]);

    if (!$isValid) return $data;


    $data['order_id'] = isset($data['doku_invoice_no']) ? $data['doku_invoice_no'] : $data['invoice_no'];

    $this->pipeline->add(new Task(PaymentOpenRequester::class, 'open'));

    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json($this->pipeline->errorMessage['additional'] ?? [], 200);
    }
    return response()->json($this->pipeline->data['additional'], $this->pipeline->statusCode);
  }


  public function dokuNotify(Request $request) {
    $data = $request->all();

    $data['order_id'] = $data['TRANSIDMERCHANT'];

    $this->pipeline->add(new Task(PaymentNotificator::class, 'notify'));

    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response('ERROR', 400);
    }

    return response('CONTINUE', 200);

  }

  public function bniNotify(Request $request) {
    $data = $request->all();

    $this->pipeline->add(new Task(BniNotifier::class, 'notify'));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([
        'status' => '100'
      ], 400);
    }
    return response()->json([
      'status' => '000'
    ]);
  }

  public function dokuBackToMerchant(Request $request) {
    $data = $request->all();

    $data['order_id'] = $data['TRANSIDMERCHANT'];

    $data['status'] = $data['STATUSCODE'] == '0000' ? 'success' : 'fail';

    $this->pipeline->add(new Task(PaymentNotificator::class, 'backToMerchant'));

    $this->pipeline->execute($data);

    $redirectUrl = $this->pipeline->data['redirect_to'] . ($data['STATUSCODE'] != '0000' ? '?error=true' : '');

    return redirect($redirectUrl);

  }

  public function dokuInquiry(Request $request) {
    $data = $request->all();
    $data['virtual_account_number'] = $data['PAYMENTCODE'];
    $data['vendor_code'] = VirtualAccount::getVendorByPrefix($data['PAYMENTCODE']);

    $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $errorMessage = $this->pipeline->errorMessage['error_code'] ?? json_encode($this->pipeline->errorMessage);
      dispatch(new \Odeo\Domains\Payment\Jobs\SendVaInquiryFailAlert($data, $errorMessage));
      $response = '<?xml version="1.0"?>' . PHP_EOL;
      $response .= '<INQUIRY_RESPONSE>' . PHP_EOL;
      $response .= '<RESPONSECODE>' . $errorMessage . '</RESPONSECODE>' . PHP_EOL;
      $response .= '</INQUIRY_RESPONSE>';
      return response($response)->header('Content-Type', 'application/xml');
    }

    $result = $this->pipeline->data;

    return response($result['xml_response'])->header('Content-Type', 'application/xml');
  }

  public function kredivoNotify(Request $request) {
    $data = $request->all();

    $this->pipeline->add(new Task(PaymentNotificator::class, 'notify'));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([
        "status" => "FAIL",
        "message" => "Notification cannot be processed"
      ], 400);
    }

    return response()->json([
      "status" => "OK",
      "message" => "Notification has been received"
    ]);

  }

  public function akulakuNotify(Request $request) {
    $data = $request->all();
    $data['order_id'] = $data['refNo'];
    $this->pipeline->add(new Task(PaymentNotificator::class, 'notify'));
    $this->pipeline->execute($data);
    return response()->json([
      'code' => (int)$this->pipeline->fail()
    ], $this->pipeline->fail() ? 400 : 200);
  }

  public function akulakuRedirect(Request $request) {
    $data = $request->all();
    $data['order_id'] = $data['refNo'];
    $this->pipeline->add(new Task(Director::class, 'redirect'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);
    return redirect($this->pipeline->data['redirect_to']);
  }

  public function togglePaymentChannel() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(PaymentConfigurator::class, 'togglePaymentChannel'));

    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function damNotify() {
    $data['response'] = file_get_contents('php://input');
    $arr = explode(",", $data['response']);

    if ($arr[4] != 'SUCCESS') {
      return response()->json([
        "status" => "FAIL",
        "message" => "Notification cannot be processed"
      ]);
    }
    $data['order_id'] = $arr[3];

    $this->pipeline->add(new Task(PaymentNotificator::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([
        "status" => "FAIL",
        "message" => "Notification cannot be processed"
      ], 400);
    }

    return response()->json([
      "status" => "OK",
      "message" => "Notification has been received"
    ]);

  }

  public function damMandiriMptNotify(Request $request) {
    $data['order_id'] = $request->merchant_order_id;

    $this->pipeline->add(new Task(PaymentNotificator::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([
        "status" => "FAIL",
        "message" => "Notification cannot be processed"
      ], 400);
    }

    return response()->json([
      "status" => "OK",
      "message" => "Notification has been received"
    ]);

  }

  public function damRedirectUrl() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(\Odeo\Domains\Payment\Dam\Mandiriecash\Director::class, 'translate'));
    $this->pipeline->add(new Task(\Odeo\Domains\Payment\PaymentDirector::class, 'redirect'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    return redirect($this->pipeline->data['redirect_to']);
  }

  public function showPaymentStatus() {
    return view('payment.payment-status-redirect');
  }

  public function requestOpenPayment(Request $request) {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'opc' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['client_ip_address'] = $request->getClientIp();
    $data['with_password'] = true;

    $this->pipeline->add(new Task(TempDepositRequester::class, 'tempDeposit'));
    $this->pipeline->add(new Task(PaymentRequester::class, 'request'));
    $this->pipeline->add(new Task(PaymentOpenRequester::class, 'open'));
    $this->pipeline->enableTransaction();
    $this->pipeline->enableAntiSpam($data['order_id']);
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, 400, '', $this->pipeline->errorStatus);
    }

    if (isset($this->pipeline->data['view_page'])) {

      if ($this->pipeline->data['view_page'] == 'payment.doku-v1') {

        if (!isset($data['view_except']) || !in_array('order', $data['view_except'])) {
          $pipeline2 = new Pipeline;
          $pipeline2->add(new Task(OrderSelector::class, 'find', [
          ]));
          $pipeline2->execute([
            'order_id' => $this->pipeline->data['view_data']['orderID'],
            'expand' => 'payment,payment_logo,payment_fee'
          ]);
          $this->pipeline->data['view_data']['order'] = $pipeline2->data;
        }
      }

      return view($this->pipeline->data['view_page'], $this->pipeline->data['view_data']);
    }

    return $this->buildResponse(200, [
      "order_id" => $this->pipeline->data['order_id']
    ]);
  }

  public function terminalRequestOpenPayment(Request $request) {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'opc' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['client_ip_address'] = $request->getClientIp();

    $this->pipeline->add(new Task(PaymentRequester::class, 'request'));
    $this->pipeline->add(new Task(PaymentOpenRequester::class, 'open'));
    $this->pipeline->enableTransaction();
    $this->pipeline->enableAntiSpam($data['order_id']);
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, 400, '', $this->pipeline->errorStatus);
    }

    return $this->buildResponse(200, [
      "order_id" => $this->pipeline->data['order_id']
    ]);
  }

  public function logBniSaleResult(Request $request) {
    $data = $request->all();
    clog('pax-result', 'logBniSaleResult: data=' . json_encode($data));
    return $this->buildResponse(200);
  }

  public function prismalinkReversal(Request $request) {
    $data = $request->all();
    $data['virtual_account_number'] = $data['accountNo'];

    return response()->json([
      'responseCode' => '01',
      'responseMessage' => 'Connection error'
    ], 200);

    $this->pipeline->add(new Task(VirtualAccountSelector::class, 'getDetail'));
    $this->pipeline->add(new Task(PaymentVoidRequester::class, 'void'));

    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([
        'responseCode' => '01',
        'responseMessage' => 'Connection error'
      ], 200);
    }

    $result = $this->pipeline->data;
    $response = [
      'responseCode' => $result['response_code'],
      'responseMessage' => $result['response_message'],
    ];

    return response()->json($response);
  }
}

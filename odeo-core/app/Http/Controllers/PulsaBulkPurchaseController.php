<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\BulkPurchaseSelector;
use Odeo\Domains\Order\BulkPurchaseUpdater;

class PulsaBulkPurchaseController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function bulkCreateGroup() {
    list($isValid, $data) = $this->validateData([
      'bulk_name' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BulkPurchaseUpdater::class, 'createGroup'));
    return $this->executeAndResponse($data);
  }

  public function removeGroup() {
    list($isValid, $data) = $this->validateData([
      'pulsa_bulk_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BulkPurchaseUpdater::class, 'removeGroup'));
    return $this->executeAndResponse($data);
  }

  public function bulkChangeApproval() {
    list($isValid, $data) = $this->validateData([
      'pulsa_bulk_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BulkPurchaseUpdater::class, 'approveOrRejectGroup'));
    return $this->executeAndResponse($data);
  }

  public function bulkPurchase() {
    list($isValid, $data) = $this->validateData([
      'bulks' => 'required',
      'pulsa_bulk_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BulkPurchaseUpdater::class, 'insert'));
    return $this->executeAndResponse($data);
  }

  public function getGroups() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(BulkPurchaseSelector::class, 'getGroups'));
    return $this->executeAndResponse($data);
  }

  public function getGroupSummary($pulsaBulkId) {
    $data = $this->getRequestData();
    $data['pulsa_bulk_id'] = $pulsaBulkId;
    $this->pipeline->add(new Task(BulkPurchaseSelector::class, 'getGroupSummary'));
    return $this->executeAndResponse($data);
  }

  public function getGroupDetails($pulsaBulkId) {
    $data = $this->getRequestData();
    $data['pulsa_bulk_id'] = $pulsaBulkId;
    $this->pipeline->add(new Task(BulkPurchaseSelector::class, 'getGroupDetails'));
    return $this->executeAndResponse($data);
  }

  public function export($pulsaBulkId) {
    $data = $this->getRequestData();
    $data['pulsa_bulk_id'] = $pulsaBulkId;
    $this->pipeline->add(new Task(BulkPurchaseSelector::class, 'export'));
    return $this->executeAndResponse($data);
  }

  public function toggleDetail() {
    list($isValid, $data) = $this->validateData([
      'pulsa_bulk_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BulkPurchaseUpdater::class, 'toggleDetail'));
    return $this->executeAndResponse($data);
  }
}

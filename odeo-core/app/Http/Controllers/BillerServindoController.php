<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Biller\Servindo\Notifier;

class BillerServindoController extends Controller {

  public function __construct() {
    parent::__construct();
  }
  
  public function notify() {
    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    $this->pipeline->disableResponse();
    return $this->executeAndResponse($this->getRequestData());
  }

}

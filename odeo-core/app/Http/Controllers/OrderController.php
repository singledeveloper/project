<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 9/24/16
 * Time: 4:45 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Account\Helper\CustomerEmailManager;
use Odeo\Domains\Activity\Scheduler\AutoSendReportScheduler;
use Odeo\Domains\Constant\OrderStatus;
use Odeo\Domains\Constant\Service;
use Odeo\Domains\Constant\TransactionType;
use Odeo\Domains\Constant\UserType;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Jobs\BeginVoid;
use Odeo\Domains\Order\AdminOrderSelector;
use Odeo\Domains\Order\DoublePurchaseSelector;
use Odeo\Domains\Order\FinancialOrderSelector;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Order\OrderAccumulator;
use Odeo\Domains\Order\OrderCanceller;
use Odeo\Domains\Order\OrderGuard;
use Odeo\Domains\Order\OrderRefunder;
use Odeo\Domains\Order\OrderSalesProcessor;
use Odeo\Domains\Order\OrderSelector;
use Odeo\Domains\Order\OrderUpdater;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Domains\Order\Receipt\SheetReceipt\SheetReceiptSelector;
use Odeo\Domains\Order\StoreOrderSelector;
use Odeo\Domains\Order\UserOrderSelector;
use Odeo\Domains\Order\Reconciliation\OrderReconciliationSelector;
use Odeo\Domains\Order\Reconciliation\OrderReconciler;
use Odeo\Domains\Payment\Odeo\Terminal\TerminalGuard;
use Odeo\Domains\Payment\PaymentInformationSelector;
use Odeo\Domains\Subscription\StoreVerificator;
use Odeo\Domains\Transaction\CashRequester;

class OrderController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function testDailyReport() {
    $this->pipeline->add(new Task(AutoSendReportScheduler::class, 'sendEmail'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getAll() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(AdminOrderSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getByStore($storeId) {
    setQueryTimeout(10);

    $data = $this->getRequestData();

    $data["expand"] = "user,seller,payment,payment_detail";
    $data['search']['store_id'] = $storeId;
    $data['search']['not_status'] = OrderStatus::CREATED;
    $data['sort_by'] = 'order_id';
    $data['sort_type'] = 'desc';
    $data['limit'] = 10;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_id' => $storeId,
      'allow_mentor' => true
    ]));
    $this->pipeline->add(new Task(StoreOrderSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function exportByStore($storeId) {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data['search']['store_id'] = $storeId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'store_id' => $storeId,
      'allow_mentor' => true
    ]));

    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(StoreOrderSelector::class, 'export'));

    return $this->executeAndResponse($data);

  }

  public function getByUser() {
    setQueryTimeout(10);

    $data = $this->getRequestData();

    $data['sort_by'] = 'orders.id';
    $data['sort_type'] = 'desc';

    $this->pipeline->add(new Task(UserOrderSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function exportOrder() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $data = $this->getRequestData();

    $data['sort_by'] = 'orders.id';
    $data['sort_type'] = 'desc';

    $this->pipeline->add(new Task(CustomerEmailManager::class, 'checkIsEmailVerified'));
    $this->pipeline->add(new Task(UserOrderSelector::class, 'exportOrder'));

    return $this->executeAndResponse($data);
  }

  public function getPaginatedFinancialOrder() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(FinancialOrderSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function getAllFinancialOrder() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse();
    }

    return (new FinancialOrderSelector)->getExportedFinancialOrder($data);
  }


  public function findOrder($orderId) {
    $data = $this->getRequestData();
    $data['order_id'] = $orderId;
    $data['expand'] = 'user,payment,payment_logo,seller';

    $this->pipeline->add(new Task(OrderGuard::class, 'validateUser'));
    $this->pipeline->add(new Task(OrderSelector::class, 'find'));

    return $this->executeAndResponse($data);
  }

  public function terminalFindOrder($orderId) {
    list($isValid, $data) = $this->validateData([
      'phone_number' => 'required'
    ]);
    if (!$isValid) {
      return $data;
    }

    $data['order_id'] = $orderId;
    $data['expand'] = 'user,payment,payment_logo,seller';

    $this->pipeline->add(new Task(TerminalGuard::class, 'validateOrderUser'));
    $this->pipeline->add(new Task(OrderSelector::class, 'find'));

    return $this->executeAndResponse($data);
  }

  public function verify($orderId) {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(AdminChecker::class, 'checkHasPermission', [
      'task' => UserType::GROUP_HAS_VERIFY_ORDER_PERMISSION
    ]));
    $this->pipeline->add(new Task(OrderVerificator::class, 'updateOrderToVerifyStatus'));

    $this->pipeline->execute([
      'order_id' => $orderId,
      'verified_by' => $data['auth']['user_id'],
      'skip_pipeline_response' => false
    ]);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
    }

    dispatch(new VerifyOrder($orderId));

    return $this->buildSuccessResponse();

  }

  public function complete($orderId) {
    $this->pipeline->add(new Task(OrderVerificator::class, 'completeOrder'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse([
      'order_id' => $orderId
    ]);

  }

  public function cancel($orderId) {
    $data = $this->getRequestData();
    $data['order_id'] = $orderId;

    $this->pipeline->add(new Task(OrderGuard::class, 'validateUser'));
    $this->pipeline->add(new Task(OrderCanceller::class, 'cancel'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function refund($orderId) {
    $data = $this->getRequestData();

    $data['order_id'] = $orderId;

    $this->pipeline->add(new Task(OrderRefunder::class, 'refund'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function void($orderId) {
    $data = $this->getRequestData();
    $data['order_id'] = $orderId;
    dispatch(new BeginVoid($data));

    return $this->buildSuccessResponse();
  }

  public function faultRefund($orderId) {
    list($isValid, $data) = $this->validateData([
      'amount' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['order_id'] = $orderId;

    $this->pipeline->add(new Task(OrderRefunder::class, 'faultRefund'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function sendEmailComplete($orderId) {
    $data['order_id'] = $orderId;

    $this->pipeline->add(new Task(OrderSalesProcessor::class, 'sendEmailComplete'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function confirmReceipt() {

    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'rating' => 'integer|between:1,5',
      'feedback' => 'string',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(OrderGuard::class, 'validateUser'));
    $this->pipeline->add(new Task(OrderUpdater::class, 'confirmReceipt'));
    $this->pipeline->add(new Task(CashRequester::class, 'settleOrder'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updateRating() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'rating' => 'required|integer|between:1,5',
      'feedback' => 'string',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(OrderGuard::class, 'validateUser'));
    $this->pipeline->add(new Task(OrderUpdater::class, 'updateRating'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updateStatus() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'status' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(OrderUpdater::class, 'updateStatus'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);

  }

  public function accumulateSalesPerStoreFromDayOne() {
    $this->pipeline->add(new Task(OrderAccumulator::class, 'accumulateSalesPerStoreFromDayOne'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($this->getRequestData());
    if (isset($this->pipeline->data['view_page']))
      return view($this->pipeline->data['view_page'], $this->pipeline->data['view_data']);
    else
      return $this->buildErrorsResponse($this->pipeline->errorMessage);
  }

  public function getSheetReceipt($orderId) {
    $data = $this->getRequestData();
    $data['order_id'] = $orderId;

    $this->pipeline->add(new Task(OrderGuard::class, 'validateUser'));
    $this->pipeline->add(new Task(SheetReceiptSelector::class, 'find'));

    return $this->executeAndResponse($data);
  }

  public function getFilterContents() {
    $data = $this->getRequestData();
    $data['products'] = [
      ['id' => Service::PULSA, 'name' => trans('service.service_' . Service::PULSA)],
      ['id' => Service::PLN, 'name' => trans('service.service_' . Service::PLN)],
      ['id' => Service::PAKET_DATA, 'name' => trans('service.service_' . Service::PAKET_DATA)],
      ['id' => Service::BOLT, 'name' => trans('service.service_' . Service::BOLT)],
      ['id' => Service::PULSA_POSTPAID, 'name' => trans('service.service_' . Service::PULSA_POSTPAID)],
      ['id' => Service::PLN_POSTPAID, 'name' => trans('service.service_' . Service::PLN_POSTPAID)],
      ['id' => Service::BPJS_KES, 'name' => trans('service.service_' . Service::BPJS_KES)],
      ['id' => Service::PDAM, 'name' => trans('service.service_' . Service::PDAM)],
      ['id' => Service::BROADBAND, 'name' => trans('service.service_' . Service::BROADBAND)],
      ['id' => Service::LANDLINE, 'name' => trans('service.service_' . Service::LANDLINE)],
      ['id' => Service::PGN, 'name' => trans('service.service_' . Service::PGN)],
      ['id' => Service::GAME_VOUCHER, 'name' => trans('service.service_' . Service::GAME_VOUCHER)],
      ['id' => Service::TRANSPORTATION, 'name' => trans('service.service_' . Service::TRANSPORTATION)],
      ['id' => Service::EMONEY, 'name' => trans('service.service_' . Service::EMONEY)],
      ['id' => Service::GOOGLE_PLAY, 'name' => trans('service.service_' . Service::GOOGLE_PLAY)]
    ];
    $data['status'] = [
      ['code' => OrderStatus::OPENED, 'message' => trans('order.order_status_' . OrderStatus::OPENED)],
      ['code' => OrderStatus::WAITING_FOR_UPDATE, 'message' => trans('order.order_status_' . OrderStatus::WAITING_FOR_UPDATE)],
      ['code' => OrderStatus::WAITING_SUSPECT, 'message' => trans('order.order_status_' . OrderStatus::WAITING_SUSPECT)],
      ['code' => OrderStatus::PARTIAL_FULFILLED, 'message' => trans('order.order_status_' . OrderStatus::PARTIAL_FULFILLED)],
      ['code' => OrderStatus::COMPLETED, 'message' => trans('order.order_status_' . OrderStatus::COMPLETED)],
      ['code' => OrderStatus::REFUNDED, 'message' => trans('order.order_status_' . OrderStatus::REFUNDED)],
    ];
    $data['trx_types'] = [
      ['code' => TransactionType::TOPUP, 'text' => trans('transaction.trx_type_' . TransactionType::TOPUP)],
      ['code' => TransactionType::WITHDRAW, 'text' => trans('transaction.trx_type_' . TransactionType::WITHDRAW)],
      ['code' => TransactionType::ORDER_CONVERT, 'text' => trans('transaction.trx_type_' . TransactionType::ORDER_CONVERT)],
      ['code' => TransactionType::REFUND, 'text' => trans('transaction.trx_type_' . TransactionType::REFUND)],
//      ['code' => TransactionType::RUSH_BONUS, 'text' => 'Hustler Rush Bonus'],
//      ['code' => TransactionType::MENTOR_RUSH_BONUS, 'text' => 'Mentor Rush Bonus'],
//      ['code' => TransactionType::LEADER_RUSH_BONUS, 'text' => 'Leader Rush Bonus'],
      ['code' => TransactionType::SPONSOR_BONUS, 'text' => trans('transaction.trx_type_' . TransactionType::SPONSOR_BONUS)],
      ['code' => TransactionType::PAYMENT, 'text' => trans('transaction.trx_type_' . TransactionType::PAYMENT)],
      ['code' => TransactionType::TRANSFER, 'text' => trans('transaction.trx_type_' . TransactionType::TRANSFER)],
      ['code' => TransactionType::PRESET_CASHBACK, 'text' => trans('transaction.trx_type_' . TransactionType::PRESET_CASHBACK)],
    ];
    $this->pipeline->add(new Task(PaymentInformationSelector::class, 'getFilter'));
    return $this->executeAndResponse($data);
  }

  public function reconciliation() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['sort_by'] = $data['sort_by'] ?? 'id';
    $data['sort_type'] = $data['sort_type'] ?? 'desc';

    $this->pipeline->add(new Task(OrderReconciliationSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function reconcileOrder() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required',
      'diff_reason' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(OrderReconciler::class, 'manualRecon'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function reconcileOrderBatch() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(OrderReconciler::class, 'dispatchRecon'));

    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function recalculateReconciliation() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(OrderReconciler::class, 'recalculateData'));

    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getDetectedDoublePurchaseOrder() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(DoublePurchaseSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }
}

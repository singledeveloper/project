<?php

namespace Odeo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Odeo\Domains\Constant\ApiDisbursement;
use Odeo\Domains\Constant\Bank;
use Odeo\Domains\Constant\BatchDisbursementStatus;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementSummarySelector;
use Odeo\Domains\Disbursement\ApiDisbursement\ApiDisbursementUpdater;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementApprover;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementCanceller;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementDeleter;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementGuard;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementParser;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementRequester;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementSelector;
use Odeo\Domains\Disbursement\ApiDisbursement\BatchDisbursementValidator;

class BatchDisbursementController extends Controller {

  public function getBatchDisbursements() {
    list($valid, $data) = $this->validateData([
      'start_date' => 'date',
      'end_date' => 'date',
      'name' => 'max:60',
      'status' => Rule::in(BatchDisbursementStatus::FILTERS),
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(BatchDisbursementSelector::class, 'listByUserId'));
    return $this->executeAndResponse($data);
  }

  public function getBatchDisbursement($batchDisbursementId) {
    list ($valid, $data) = $this->validateData([
      'bank_id' => 'exists:banks,id',
      'account_number' => 'integer',
      'customer_name' => 'max:64',
      'reference_id' => 'max:32',
      'status' => Rule::in(ApiDisbursement::STATUS_FILTERS),
    ], [
      'batch_disbursement_id' => $batchDisbursementId,
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guard'));
    $this->pipeline->add(new Task(BatchDisbursementSelector::class, 'findById'));
    $this->pipeline->add(new Task(ApiDisbursementSummarySelector::class, 'getSummaryByBatchDisbursementId'));
    return $this->executeAndResponse($data);
  }

  public function getBatchDisbursementDetail($batchDisbursementId) {
    list ($valid, $data) = $this->validateData([
      'bank_id' => 'exists:banks,id',
      'account_number' => 'integer',
      'customer_name' => 'max:64',
      'reference_id' => 'max:32',
      'status' => Rule::in(ApiDisbursement::STATUS_FILTERS),
    ], [
      'batch_disbursement_id' => $batchDisbursementId,
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guard'));
    $this->pipeline->add(new Task(ApiDisbursementSelector::class, 'listByBatchDisbursementId', [
      'expand' => 'bank',
    ]));
    return $this->executeAndResponse($data);
  }

  public function createBatchDisbursement(Request $request) {
    list($valid, $data) = $this->validateData([
      'name' => 'required|min:3|max:60|unique:batch_disbursements,name',
      'file' => 'required|file',
      'extension' => 'nullable|in:xlsx',
    ], [
      'extension' => $request->hasFile('file') ? $request->file('file')->getClientOriginalExtension() : null,
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(BatchDisbursementParser::class, 'parse'));
    $this->pipeline->add(new Task(BatchDisbursementValidator::class, 'validate'));
    $this->pipeline->add(new Task(BatchDisbursementRequester::class, 'request'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

  public function approve($id) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guard'));
    $this->pipeline->add(new Task(BatchDisbursementApprover::class, 'approve'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $id,
    ]));
  }

  public function cancel($id) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guard'));
    $this->pipeline->add(new Task(BatchDisbursementCanceller::class, 'cancel'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $id,
    ]));
  }

  public function updateDetail($batchDisbursementId, $disbursementId) {
    $data = $this->getRequestData();
    $min = Arr::get($data, 'bank_id') == Bank::ODEO ? ApiDisbursement::MIN_AMOUNT_ODEO : ApiDisbursement::MIN_AMOUNT;
    list($valid, $data) = $this->validateData([
      'bank_id' => 'required|exists:banks,id',
      'account_number' => 'required|digits_between:8,18',
      'amount' => 'required|integer|min:' . $min . '|max:' . ApiDisbursement::MAX_AMOUNT,
      'customer_name' => 'required|max:64',
      'reference_id' => 'nullable|max:32|alpha_dash',
    ], [
      'disbursement_id' => $disbursementId,
      'batch_disbursement_id' => $batchDisbursementId,
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guardDetail'));
    $this->pipeline->add(new Task(ApiDisbursementUpdater::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function deleteDetail($batchDisbursementId, $disbursementId) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guardDetail'));
    $this->pipeline->add(new Task(BatchDisbursementDeleter::class, 'deleteDetail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $batchDisbursementId,
      'disbursement_id' => $disbursementId,
    ]));
  }

  public function revalidateDetail($batchDisbursementId, $disbursementId) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guardDetail'));
    $this->pipeline->add(new Task(BatchDisbursementValidator::class, 'validateAccountName'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $batchDisbursementId,
      'disbursement_id' => $disbursementId,
    ]));
  }

  public function markDetailAsValidationSuccess($batchDisbursementId, $disbursementId) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guardDetail'));
    $this->pipeline->add(new Task(ApiDisbursementUpdater::class, 'markAsValidationSuccess'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $batchDisbursementId,
      'disbursement_id' => $disbursementId,
    ]));
  }

  public function export($batchDisbursementId) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guard'));
    $this->pipeline->add(new Task(BatchDisbursementSelector::class, 'export'));
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $batchDisbursementId
    ]));
  }

  public function exportFailed($batchDisbursementId) {
    $this->pipeline->add(new Task(BatchDisbursementGuard::class, 'guard'));
    $this->pipeline->add(new Task(BatchDisbursementSelector::class, 'exportFailed'));
    return $this->executeAndResponse($this->getRequestData([
      'batch_disbursement_id' => $batchDisbursementId
    ]));
  }
}

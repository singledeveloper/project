<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Notification\NotificationSelector;
use Odeo\Domains\Notification\NotificationUpdater;

class NotificationController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function readInternal($notificationId) {
    $data = $this->getRequestData();
    $data['notification_id'] = $notificationId;
    $this->pipeline->add(new Task(NotificationUpdater::class, 'readInternal'));

    return $this->executeAndResponse($data);
  }

  public function getBalanceNotification() {
    $this->pipeline->add(new Task(NotificationSelector::class, 'getBalanceNotification'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function saveBalanceNotification() {
    list($isValid, $data) = $this->validateData([
      'amount' => 'required',
      'email' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(NotificationUpdater::class, 'saveBalanceNotification'));
    return $this->executeAndResponse($data);
  }

}

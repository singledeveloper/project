<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Activity\RealtimeDetailizer;
use Odeo\Domains\Biller\BillerMutationSelector;
use Odeo\Domains\Biller\BillerReconciliationSelector;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentCreator;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentSelector;
use Odeo\Domains\Biller\BillerSelector;
use Odeo\Domains\Disbursement\BillerReplenishment\ReplenishmentRequester;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\ChannelValidator;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\SettingDetailizer;
use Odeo\Domains\Disbursement\BillerReplenishment\Setting\SettingRequester;

class BillerController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function recon() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(BillerReconciliationSelector::class, 'getRecon'));
    return $this->executeAndResponse($data);
  }

  public function replenish() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(ReplenishmentSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function replenishCreate() {
    list($isValid, $data) = $this->validateData([
      'vendor_switcher_id' => 'required',
      'amount' => 'required|integer',
      'bank' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ReplenishmentCreator::class, 'request'));
    return $this->executeAndResponse($data);
  }

  public function replenishVerify() {
    list($isValid, $data) = $this->validateData([
      'replenishment_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ReplenishmentRequester::class, 'verify'));
    return $this->executeAndResponse($data);
  }

  public function replenishFail() {
    list($isValid, $data) = $this->validateData([
      'replenishment_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ReplenishmentRequester::class, 'fail'));
    return $this->executeAndResponse($data);
  }

  public function replenishCancel() {
    list($isValid, $data) = $this->validateData([
      'replenishment_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ReplenishmentRequester::class, 'cancel'));
    return $this->executeAndResponse($data);
  }

  public function replenishComplete() {
    list($isValid, $data) = $this->validateData([
      'replenishment_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ReplenishmentRequester::class, 'complete'));
    return $this->executeAndResponse($data);
  }

  public function lastMutation() {
    $this->pipeline->add(new Task(BillerMutationSelector::class, 'getLastMutation'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getMutations($billerId) {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data['vendor_switcher_id'] = $billerId;

    $this->pipeline->add(new Task(BillerMutationSelector::class, 'getMutationDetails'));
    return $this->executeAndResponse($data);
  }

  public function get() {
    $data = $this->getRequestData();
    $data['is_paginate'] = false;
    $this->pipeline->add(new Task(BillerSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getReplenishmentSetting($billerId) {
    $data = $this->getRequestData();
    $data['vendor_switcher_id'] = $billerId;

    $this->pipeline->add(new Task(SettingDetailizer::class, 'getDetail'));
    return $this->executeAndResponse($data);
  }

  public function editReplenishmentSetting() {
    list($isValid, $data) = $this->validateData([
      'vendor_switcher_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(SettingRequester::class, 'editBiller'));
    return $this->executeAndResponse($data);
  }

  public function getReplenishmentConditionDetail($settingId) {
    $data = $this->getRequestData();
    $data['biller_replenishment_setting_id'] = $settingId;

    $this->pipeline->add(new Task(SettingDetailizer::class, 'getConditionDetail'));
    return $this->executeAndResponse($data);
  }

  public function addReplenishmentCondition() {
    list($isValid, $data) = $this->validateData([
      'vendor_switcher_id' => 'required',
      'route_type' => 'required',
      'channel_type' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ChannelValidator::class, 'checkData'));
    $this->pipeline->add(new Task(SettingRequester::class, 'setup'));
    return $this->executeAndResponse($data);
  }

  public function editReplenishmentCondition() {
    list($isValid, $data) = $this->validateData([
      'biller_replenishment_setting_id' => 'required',
      'route_type' => 'required',
      'channel_type' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ChannelValidator::class, 'checkData'));
    $this->pipeline->add(new Task(SettingRequester::class, 'edit'));
    return $this->executeAndResponse($data);
  }

  public function removeReplenishmentCondition() {
    list($isValid, $data) = $this->validateData([
      'biller_replenishment_setting_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(SettingRequester::class, 'remove'));
    return $this->executeAndResponse($data);
  }

  public function getBalanceInformations() {
    $this->pipeline->add(new Task(RealtimeDetailizer::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

}

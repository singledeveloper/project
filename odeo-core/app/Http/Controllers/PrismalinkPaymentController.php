<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 28/11/18
 * Time: 14.56
 */

namespace Odeo\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Task;
use Odeo\Domains\OAuth2\Helper\ResponseHelper;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\Payment\Prismalink\VaDirect\Inquirer;
use Odeo\Domains\Payment\Prismalink\VaDirect\Payer;
use Odeo\Domains\Payment\Prismalink\VaDirect\Repository\PaymentPrismalinkCallbackLogRepository;
use Odeo\Domains\Payment\Prismalink\VaDirect\Validator;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelValidator;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Domains\Payment\Prismalink\VaDirect\Selector;
use Odeo\Domains\Payment\Prismalink\VaDirect\InquirySelector;
use Odeo\Domains\Payment\Prismalink\VaDirect\PaymentSelector;
use Odeo\Domains\PaymentGateway\Repository\PaymentGatewayUserPaymentChannelRepository;

class PrismalinkPaymentController extends PaymentGatewayInvoiceController {

  private $prismalinkCallbackLogRepo;
  protected $pgUserChannelRepo;

  public function __construct() {
    parent::__construct();
    $this->prismalinkCallbackLogRepo = app()->make(PaymentPrismalinkCallbackLogRepository::class);
    $this->pgUserChannelRepo = app()->make(PaymentGatewayUserPaymentChannelRepository::class);
  }

  private function isV2($vaCode) {
    $prefixes = $this->pgUserChannelRepo->getActiveChannels()->pluck('prefix');

    $match = false;
    foreach ($prefixes as $prefix) {
      if (strpos($vaCode, $prefix) === 0) {
        $match = true;
        break;
      }
    }
    return $match;
  }

  private function logCallback($type, $body) {
    $log = $this->prismalinkCallbackLogRepo->getNew();
    $log->type = $type;
    $log->body = json_encode($body);
    $this->prismalinkCallbackLogRepo->save($log);
  }

  private function isValidV2Response($content) {
    try {
      return isset($content['responseCode']);
    } catch (\Exception $e) {
      clog('prismalink_error_response', $e->getMessage());
      clog('prismalink_error_response', $e->getTraceAsString());
      return false;
    }
  }

  private function routeToV2($data, $url, $errorResponse) {
    if (!isset($data['traceNo'])) {
      $data['traceNo'] = 'PG' . time();
    }

    $client = new Client();
    $res = $client->request('post', $url, [
      'json' => $data,
      'timeout' => 60,
      'http_errors' => false,
    ]);

    $content = json_decode($res->getBody()->getContents(), true); //beware, its pointer

    if ($this->isValidV2Response($content)) {
      $content['traceNo'] = $data['traceNo'];
      return response()->json($content, 200);
    }
    return response()->json($errorResponse, 200);
  }

  public function prismalinkInquiry(Request $request) {
    $data = $request->all();
    $errorResponse = [
      'vaName' => '',
      'vaDesc' => '',
      'responseCode' => '01',
      'responseMessage' => 'Connection error',
      'billingAmount' => 0
    ];
    
    if ($this->isV2($data['accountNo'])) {
      $this->logCallback('va_inquiry', $data);
      return $this->vaInquiry($request);
    }

    $invoiceLog = $this->logInvoiceRequest('invoice_inquiry', VirtualAccount::VENDOR_PRISMALINK_BCA, json_encode($data));

    $data['virtual_account_number'] = $data['accountNo'];
    $data['vendor_code'] = VirtualAccount::VENDOR_PRISMALINK_BCA;
    $data['reference'] = $data['traceNo'];

    $this->pipeline->add(new Task(Validator::class, 'validate'));
    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));

    $this->pipeline->execute($data);

    $result = $this->pipeline->data;

    if ($this->pipeline->fail()) {
      $errorCode = $this->pipeline->errorMessage['error_code'] ?? 0;
      
      switch ($errorCode) {
        case PaymentInvoiceHelper::INVOICE_INVALID:
          $errorMessage = 'Not found';
          break;
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
          $errorMessage = 'Invalid number';
          break;
        default:
          $errorMessage = 'Connection error';
          break;
      }
      $errorResponse['responseMessage'] = $this->pipeline->errorMessage['error_message'] ?? $errorMessage;
      dispatch(new \Odeo\Domains\Payment\Jobs\SendVaInquiryFailAlert($data, $errorResponse['responseMessage']));
      return response()->json($errorResponse, 200);
    }

    $this->updateInvoiceLog($invoiceLog, $result['order_id']);

    $response = [
      'vaName' => $result['customer_name'],
      'vaDesc' => $result['item_name'] ?? '',
      'responseCode' => '00',
      'responseMessage' => 'Success',
      'billingAmount' => $result['total']
    ];

    return response()->json($response);
  }

  public function prismalinkNotify(Request $request) {
    $data = $request->all();
    $notifyType = '';

    if (isset($data['code']) && $data['code'] == '02') {
      $notifyType = 'va_payment';
    } else if (isset($data['code']) && $data['code'] == '04') {
      $notifyType = 'va_payment_retry';
    }
    $errorResponse = [
      'responseCode' => '01',
      'responseMessage' => 'Connection error',
      'customerUsername' => ''
    ];

    if ($this->isV2($data['accountNo'])) {
      $this->logCallback($notifyType, $data);
      return $this->vaPayment($request);
    }

    $invoiceLog = $this->logInvoiceRequest('invoice_payment', VirtualAccount::VENDOR_PRISMALINK_BCA, json_encode($data));

    $data['virtual_account_number'] = $data['accountNo'];
    $data['vendor_code'] = VirtualAccount::VENDOR_PRISMALINK_BCA;
    $data['reference'] = $data['traceNo'];

    $this->pipeline->add(new Task(Validator::class, 'validate'));
    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getDetailByVaNoAndReference'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceNotifier::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $errorResponse['responseMessage'] = $this->pipeline->errorMessage['response_message'] ?? 'Connection error';
      return response()->json($errorResponse, 200);
    }

    $result = $this->pipeline->data;
    $this->updateInvoiceLog($invoiceLog, $result['order_id']);
    $response = [
      'responseCode' => '00',
      'responseMessage' => 'Success',
      'customerUsername' => $result['customer_name'] ?? ''
    ];

    return response()->json($response);
  }

  public function getAllVaPayment() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(PaymentSelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getAllVaInquiry() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(InquirySelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getOrderPayments() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(Selector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getVaTransactionCount() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(Selector::class, 'getPaidTransactionCount'));
    $this->pipeline->add(new Task(PaymentSelector::class, 'getPaidTransactionCount'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getPaidTransactionCount'));
    return $this->executeAndResponse($data);
  }

  public function exportData() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse();
    }

    return (new Selector())->exportPaymentData($data);
  }

  public function vaInquiry(Request $request) {
    $data = $request->all();

    $data['virtual_account_number'] = $data['accountNo'];
    $data['vendor_code'] = VirtualAccount::VENDOR_PRISMALINK_BCA;

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Inquirer::class, 'vaInquiry'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
    $this->pipeline->add(new Task(Inquirer::class, 'createInquiry'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $response = [
        'vaName' => '',
        'responseCode' => '01',
        'responseMessage' => $this->pipeline->errorMessage,
        'billingAmount' => 0
      ];
    } else {
      $data = $this->pipeline->data;
      $response = [
        'vaName' => $data['customer_name'],
        'vaDesc' => $data['display_text'],
        'responseCode' => '00',
        'responseMessage' => 'Success',
        'billingAmount' => $data['amount']
      ];
    }

    return response()->json($response, ResponseHelper::parseResponseStatus($response));
  }

  public function vaPayment(Request $request) {
    $data = $request->all();

    $data['virtual_account_number'] = $data['accountNo'];
    $data['vendor_code'] = VirtualAccount::VENDOR_PRISMALINK_BCA;

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Payer::class, 'vaPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->add(new Task(Payer::class, 'updatePayment'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $response = [
        'responseCode' => '01',
        'responseMessage' => $this->pipeline->errorMessage,
        'customerUsername' => ''
      ];
    } else {
      $response = [
        'responseCode' => '00',
        'responseMessage' => 'Success',
        'customerUsername' => $this->pipeline->data['customer_name']
      ];
    }

    return response()->json($response, ResponseHelper::parseResponseStatus($response));
  }

}

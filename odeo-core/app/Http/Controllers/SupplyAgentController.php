<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Supply\SupplyInventoryGroupDetailSelector;
use Odeo\Domains\Supply\SupplyInventoryGroupEditor;
use Odeo\Domains\Supply\SupplyInventoryGroupSelector;

class SupplyAgentController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getInventoryGroups() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(SupplyInventoryGroupSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function addInventoryGroup() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'name' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'add'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editInventoryGroup() {
    list($isValid, $data) = $this->validateData([
      'supply_group_id' => 'required',
      'name' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'edit'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getInventoryGroupDetails($groupId) {
    $data = $this->getRequestData();
    $data['supply_group_id'] = $groupId;

    $this->pipeline->add(new Task(SupplyInventoryGroupDetailSelector::class, 'get'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function exportPriceList($groupId) {
    list($isValid, $data) = $this->validateData([
      'email' => 'required'
    ]);
    if (!$isValid) return $data;
    $data['supply_group_id'] = $groupId;

    $this->pipeline->add(new Task(SupplyInventoryGroupDetailSelector::class, 'exportPriceList'));
    return $this->executeAndResponse($data);
  }

  public function addInventoryGroupPriceDetails($groupId) {
    list($isValid, $data) = $this->validateData([
      'price_details' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['supply_group_id'] = $groupId;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'addPriceDetails'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editInventoryGroupPriceDetail($groupDetailId) {
    list($isValid, $data) = $this->validateData([
      'sell_price' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['store_inventory_group_price_detail_id'] = $groupDetailId;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'editPriceDetail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeInventoryGroupPriceDetail($groupDetailId) {
    $data = $this->getRequestData();
    $data['store_inventory_group_price_detail_id'] = $groupDetailId;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'removePriceDetail'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editAgentGroup() {
    list($isValid, $data) = $this->validateData([
      'supply_group_id' => 'required',
      'user_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'editAgentGroup'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function setInventoryGroupAsDefault($groupId) {
    $data = $this->getRequestData();
    $data['supply_group_id'] = $groupId;

    $this->pipeline->add(new Task(SupplyInventoryGroupEditor::class, 'setGroupAsDefault'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }
}

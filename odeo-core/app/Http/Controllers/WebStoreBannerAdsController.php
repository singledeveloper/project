<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 1/6/17
 * Time: 4:44 PM
 */
namespace Odeo\Http\Controllers;



use Odeo\Domains\Banner\Webstore\BannerAdsInserter;
use Odeo\Domains\Banner\Webstore\BannerAdsRemover;
use Odeo\Domains\Banner\Webstore\BannerAdsSelector;
use Odeo\Domains\Banner\Webstore\BannerAdsUpdater;
use Odeo\Domains\Core\Task;

class WebStoreBannerAdsController extends Controller {
    
  public function __construct() {
    parent::__construct();
  }

  public function get() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(BannerAdsSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function insert() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(BannerAdsInserter::class, 'insert'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function remove() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(BannerAdsRemover::class, 'remove'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function activate($bannerAdsId) {

    $data = $this->getRequestData();
    $data['banner_ads_id'] = $bannerAdsId;

    $this->pipeline->add(new Task(BannerAdsUpdater::class, 'activate'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }
  
}
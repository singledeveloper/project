<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Network\ReferralRequester;
use Odeo\Domains\Network\ReferralClaimRequester;
use Odeo\Domains\Network\CommissionRequester;
use Odeo\Domains\Network\TreeSelector;
use Odeo\Domains\Network\TreeRequester;
use Odeo\Domains\Notification\NotificationUpdater;

class NetworkController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function referred($storeId) {
    $data = $this->getRequestData();
    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(ReferralRequester::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getBranch($storeId) {
    list($isValid, $data) = $this->validateData([
      'max_depth' => 'integer|min:1|max:5'
    ]);

    if (!$isValid) return $data;

    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(TreeSelector::class, 'getBranch'));
    return $this->executeAndResponse($data);
  }

  public function getInfo($storeId) {
    $data = $this->getRequestData();
    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(TreeSelector::class, 'getInfo'));
    return $this->executeAndResponse($data);
  }

  public function claimReferral() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ReferralClaimRequester::class, 'claim'));
    $this->pipeline->add(new Task(NotificationUpdater::class, 'toClaimed'));
    return $this->executeAndResponse($data);
  }

  public function checkInvitation($storeId) {
    $data = $this->getRequestData();
    $data["store_id"] = $storeId;
    $this->pipeline->add(new Task(TreeSelector::class, 'getInvitationInfo'));
    return $this->executeAndResponse($data);
  }

  public function assign() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:network_trees,store_id',
      'assigned_store_id' => 'required',
      'parent_store_id' => 'required',
      'position' => 'required_without:position_auto_assign|in:0,1',
      'position_auto_assign' => 'required_without:position|in:1'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TreeRequester::class, 'assign'));
    $this->pipeline->add(new Task(CommissionRequester::class, 'create'));
    return $this->executeAndResponse($data);
  }
}

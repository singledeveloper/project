<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Accounting\InquiryInformation\InquiryInformationUpdater;
use Odeo\Domains\Constant\Payment;
use Odeo\Domains\Constant\Platform;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Core\Pipeline;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\CartCheckouter;
use Odeo\Domains\Order\CartInserter;
use Odeo\Domains\Order\CartRemover;
use Odeo\Domains\Order\Jobs\VerifyOrder;
use Odeo\Domains\Order\OrderVerificator;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BankAccountNumberSelector;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\BankScrapeAccountNumberUpdater;
use Odeo\Domains\Payment\Odeo\BankTransfer\Scraper\InquirySelector;
use Odeo\Domains\Payment\PaymentRequester;
use Odeo\Domains\Transaction\TopupRequester;

class BankTransferController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getAccountNumber() {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(BankAccountNumberSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function getInquiry($bank, $id) {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $data['bank'] = $bank;
    $data['id'] = $id;

    $this->pipeline->add(new Task(InquirySelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function exportInquiry($bank, $id) {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['bank'] = $bank;
    $data['id'] = $id;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse();
    }

    return (new InquirySelector())->getExportedInquiry($data);
  }


  public function updateInquiry($bank, $id) {

    $data = $this->getRequestData();
    $data['bank'] = $bank;
    $data['id'] = $id;

    $this->pipeline->add(new Task(BankScrapeAccountNumberUpdater::class, 'updateInquiry'));

    return $this->executeAndResponse($data);

  }

  public function manualCreateTopupFromBankTransfer($bank, $id) {

    $payload = $this->getRequestData();

    $adminAuth = $payload['auth'];

    //$this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(InquirySelector::class, 'getAmountById', [
      'bank' => $bank,
      'inquiry_id' => (int)$payload['inquiry_id']
    ]));

    $this->pipeline->execute($payload);

    $topupAmount = isset($this->pipeline->data['credit_amount']) ? $this->pipeline->data['credit_amount'] : 0;

    if ($this->pipeline->fail() || $topupAmount === 0) {
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    $this->pipeline = new Pipeline;

    $data = [
      'auth' => [
        'user_id' => $payload['user_id'],
        'platform_id' => Platform::ADMIN
      ]
    ];

    \DB::beginTransaction();

    $this->pipeline->add(new Task(CartRemover::class, 'clear'));
    $this->pipeline->add(new Task(TopupRequester::class, 'request'));
    $this->pipeline->add(new Task(CartInserter::class, 'addToCart', ['service_detail_id' => ServiceDetail::TOPUP_ODEO]));
    $this->pipeline->add(new Task(CartCheckouter::class, 'checkout', [
      'gateway_id' => Payment::ADMIN_OCASH,
    ]));

    $this->pipeline->add(new Task(PaymentRequester::class, 'request', [
      'opc' => (function () use ($bank) {
        switch (strtoupper($bank)) {
          case 'BCA':
            return 368;
          case 'MANDIRI':
          case 'MANDIRI_GIRO':
            return 369;
          case 'BNI':
            return 370;
          case 'BRI':
            return 371;
          case 'CIMB':
            return 562;
          case 'PERMATA':
            return 563;
        }
        return null;
      })()
    ]));


    $this->pipeline->execute(array_merge($data, [
      'cash' => [
        'amount' => $topupAmount,
        'currency' => 'IDR'
      ],
    ]));

    if ($this->pipeline->fail()) {
      \DB::rollback();
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    $orderId = $this->pipeline->data['order_id'];

    $this->pipeline = new Pipeline;

    $this->pipeline->add(new Task(BankScrapeAccountNumberUpdater::class, 'updateInquiry', [
      'bank' => $bank,
      'inquiry_id' => $payload['inquiry_id'],
      'reference_type' => 'order_id',
      'reference' => [
        (string)$orderId
      ]
    ]));

    $this->pipeline->add(new Task(OrderVerificator::class, 'updateOrderToVerifyStatus', [
      'order_id' => $orderId,
      'verified_by' => $adminAuth['user_id'],
      'skip_pipeline_response' => false
    ]));

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      \DB::rollback();
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    \DB::commit();

    dispatch(new VerifyOrder($orderId));

    return $this->buildSuccessResponse([
      'order_id' => $orderId
    ]);

  }

  public function createInquiryInformation($bank, $id) {
    $payload = $this->getRequestData();
    $payload['bank'] = $bank;
    $this->pipeline->add(new Task(InquiryInformationUpdater::class, 'updateInquiryInformation'));
    return $this->executeAndResponse($payload);
  }


}

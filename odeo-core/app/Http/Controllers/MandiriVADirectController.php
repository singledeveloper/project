<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 11/04/19
 * Time: 12.43
 */

namespace Odeo\Http\Controllers;


use Illuminate\Http\Request;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Payment\Mandiri\Helper\MandiriHelper;
use Odeo\Domains\Payment\Mandiri\MandiriDirectInquirer;
use Odeo\Domains\Payment\Mandiri\MandiriDirectPayment;
use Odeo\Domains\Payment\Mandiri\MandiriDirectReverser;
use Odeo\Domains\Payment\Mandiri\MandiriTester;
use Odeo\Domains\Payment\Mandiri\Repository\PaymentMandiriCallbackLogRepository;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelValidator;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class MandiriVADirectController extends PaymentGatewayInvoiceController {

  private $mandiriCallbackLogs;

  function __construct() {
    parent::__construct();
    $this->mandiriCallbackLogs = app()->make(PaymentMandiriCallbackLogRepository::class);
  }

  public function getInstance(Request $request) {
    $xml = $request->getContent();
    list($callable, $data) = $this->parseRequestBody($xml);
    $data['virtual_account_number'] = $data['billKey1'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_MANDIRI;

    if ($this->isInvoiceRequest($data['virtual_account_number'])) {
      return $this->processInvoice($callable, $xml, $data);
    }

    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));

    switch ($callable) {
      case 'inquiry':
        $this->logCallback($xml, 'va_inquiry');
        $this->pipeline->add(new Task(MandiriDirectInquirer::class, 'vaInquiry'));
        $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
        $this->pipeline->add(new Task(MandiriDirectInquirer::class, 'createInquiry'));
        break;
      case 'payment':
        $this->logCallback($xml, 'va_payment');
        $this->pipeline->add(new Task(MandiriDirectPayment::class, 'vaPayment'));
        $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
        $this->pipeline->add(new Task(MandiriDirectPayment::class, 'updatePayment'));
        break;
      case 'reverse':
        $this->logCallback($xml, 'va_reverse');
        $this->pipeline->add(new Task(MandiriDirectReverser::class, 'vaReverse'));
        break;
    }

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $code = 'B5';
      if (isset($this->pipeline->errorMessage['status_code'])) {
        $code = $this->pipeline->errorMessage['status_code'];
      }
      $result = $this->errorResponse($callable, $code);
      clog('mandiri_va', 'error ' . $callable);
      clog('mandiri_va', $result);
      clog('mandiri_va', json_encode($this->pipeline->errorMessage));

      if ($callable != 'payment') {
        return response($result, 200, ['Content-Type' => 'application/xml']);
      }
    }

    $data = $this->pipeline->data;
    $result = $this->buildXmlResponse($callable, $data);
    clog('mandiri_va', 'success ' . $callable);
    clog('mandiri_va', $result);
    return response($result, 200, ['Content-Type' => 'application/xml']);

  }

  private function processInvoice($callable, $xml, $data) {
    switch ($callable) {
      case 'inquiry':
        $invoiceLog = $this->logInvoiceRequest('invoice_inquiry', $data['vendor_code'], $xml);
        $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));
        break;
      case 'payment':
        $invoiceLog = $this->logInvoiceRequest('invoice_payment', $data['vendor_code'], $xml);
        $data['amount'] = $data['paymentAmount'];

        $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getDetailByVaNo'));
        $this->pipeline->add(new Task(PaymentGatewayInvoiceNotifier::class, 'notify'));
        break;
      case 'reverse':
        $invoiceLog = $this->logInvoiceRequest('invoice_reversal', $data['vendor_code'], $xml);
        $this->pipeline->add(new Task(MandiriDirectReverser::class, 'vaReverse'));
        break;
    }

    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $code = 'B5';
      if (isset($this->pipeline->errorMessage['status_code'])) {
        $code = $this->pipeline->errorMessage['status_code'];
      }
      $result = $this->errorResponse($callable, $code);
      clog('mandiri_va', 'error ' . $callable);
      clog('mandiri_va', $result);
      clog('mandiri_va', json_encode($this->pipeline->errorMessage));

      return response($result, 200, ['Content-Type' => 'application/xml']);
    }

    $result = $this->pipeline->data;
    $this->updateInvoiceLog($invoiceLog, $result['order_id']);
    switch ($callable) {
      case 'inquiry':
        $response = [
          'bill_info_1' => $result['name'],
          'bill_info_2' => $result['customer_name'],
          'bill_name' => $result['item_name'],
          'bill_code' => '01',
          'bill_amount' => $result['total']
        ];
        break;
      case 'payment':
        $response = [
          'bill_info_1' => $result['customer_name'],
          'bill_info_2' => $result['description'],
          'status_code' => '00'
        ];
        break;
      case 'reverse':
        $response = [];
        break;
    }

    $responseXml = $this->buildXmlResponse($callable, $response);
    clog('mandiri_va', 'success ' . $callable);
    clog('mandiri_va', $responseXml);
    return response($responseXml, 200, ['Content-Type' => 'application/xml']);

  }

  private function parseRequestBody($xml) {
    $xml = simplexml_load_string(str_replace(['m:', 'SOAP-ENV:', 'soapenv:'], '', $xml), 'SimpleXMLElement', LIBXML_NOWARNING);
    $resultData = json_decode(json_encode($xml), true)['Body'];

    $callable = key($resultData);

    return [$callable, $resultData[$callable]['request']];
  }

  private function buildXmlResponse($fn, $data) {
    $callable = $fn . 'Response';
    return $this->$callable($data);
  }

  private function logCallback($body, $type) {
    $log = $this->mandiriCallbackLogs->getNew();
    $log->body = $body;
    $log->type = $type;
    $this->mandiriCallbackLogs->save($log);
  }

  private function inquiryResponse($data) {
    return
      '<?xml version="1.0" encoding="UTF-8"?>
       <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <soapenv:Body>
            <inquiryResponse xmlns="bankmandiri.h2h.billpayment.ws">
              <inquiryResult>
                <currency>360</currency>
                <billInfo1>' . $data['bill_info_1'] . '</billInfo1>
                <billInfo2>' . $data['bill_info_2'] . '</billInfo2>
                <billInfo3>' . $data['bill_name'] . '</billInfo3>
                <billDetails>
                  <BillDetail>
                    <billCode>' . $data['bill_code'] . '</billCode>
                    <billName>' . $data['bill_name'] . '</billName>
                    <billShortName>OCA</billShortName>
                    <billAmount>' . $data['bill_amount'] . '</billAmount>
                  </BillDetail>
                </billDetails>
                <status>
                  <isError>false</isError>
                  <errorCode>00</errorCode>
                  <statusDescription>Sukses</statusDescription>
                </status>
              </inquiryResult>
            </inquiryResponse>
          </soapenv:Body>
       </soapenv:Envelope>';
  }

  private function paymentResponse($data) {
    return
      '<?xml version="1.0" encoding="UTF-8"?>
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soapenv:Body>
          <paymentResponse xmlns="bankmandiri.h2h.billpayment.ws">
            <paymentResult>
              <billInfo1>' . $data['bill_info_1'] . '</billInfo1>
              <billInfo2>' . $data['bill_info_2'] . '</billInfo2>
              <status>
                <isError>false</isError>
                <errorCode>' . $data['status_code'] . '</errorCode>
                <statusDescription>Sukses</statusDescription>
              </status>
            </paymentResult>
          </paymentResponse>
        </soapenv:Body>
      </soapenv:Envelope>';
  }

  private function reverseResponse() {
    return
      '<?xml version="1.0" encoding="utf-8"?>
      <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <soap:Body>
          <reverseResponse xmlns="bankmandiri.h2h.billpayment.ws">
            <reverseResult>
              <status>
                <isError>false</isError>
                <errorCode>00</errorCode>
                <statusDescription>Sukses</statusDescription>
              </status>
            </reverseResult>
          </reverseResponse>
        </soap:Body>
      </soap:Envelope>';
  }

  private function errorResponse($callable, $code) {
    $errorData = MandiriHelper::STATUS_CODES[$code];
    return
    '<?xml version="1.0" encoding="UTF-8"?>
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soapenv:Body>
          <' . $callable . 'Response xmlns="bankmandiri.h2h.billpayment.ws">
            <' . $callable . 'Result>
              <status>
                <isError>true</isError>
                <errorCode>' . $errorData['errorCode'] . '</errorCode>
                <statusDescription>' . $errorData['statusDescription'] . '</statusDescription>
              </status>
            </' . $callable . 'Result>
          </' . $callable . 'Response>
        </soapenv:Body>
      </soapenv:Envelope>';
  }
}

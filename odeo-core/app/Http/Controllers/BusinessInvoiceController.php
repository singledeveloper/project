<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 07/11/19
 * Time: 18.39
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Core\Task;
use Odeo\Domains\Invoice\BulkInvoiceParser;
use Odeo\Domains\Invoice\BulkInvoiceRequester;
use Odeo\Domains\Invoice\BulkInvoiceValidator;
use Odeo\Domains\Invoice\BusinessInvoiceUserRequester;
use Odeo\Domains\Invoice\BusinessInvoiceUserSelector;
use Odeo\Domains\Invoice\BusinessInvoiceUserSettingRequester;
use Odeo\Domains\Invoice\InvoiceSelector;
use Odeo\Domains\Invoice\InvoiceRequester;
use Odeo\Domains\Invoice\InvoiceUserRequester;
use Odeo\Domains\Invoice\InvoiceUserSelector;
use Odeo\Domains\Invoice\InvoiceUserUpdater;
use Odeo\Domains\VirtualAccount\UserVirtualAccountRequester;

class BusinessInvoiceController extends Controller {

  public function addInvoiceUser() {
    list($isValid, $data) = $this->validateData([
      'billed_user_id' => 'required_without:billed_user_name',
      'billed_user_name' => 'required_without:billed_user_id',
      'billed_user_email' => 'email',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(InvoiceUserRequester::class, 'addInvoiceUser'));
    $this->pipeline->add(new Task(UserVirtualAccountRequester::class, 'requestVa'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function addInvoiceItems() {
    list($isValid, $data) = $this->validateData([
      'billed_user_id' => 'required_without:billed_user_name',
      'billed_user_name' => 'required_without:billed_user_id',
      'invoice_name' => 'required',
      'invoice_items' => 'required|min:1',
      'invoice_items.*.name' => 'required',
      'invoice_items.*.amount' => 'required|numeric|min:0',
      'invoice_items.*.price' => 'required|numeric|min:0'
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(InvoiceRequester::class, 'addInvoiceItems'));
    $this->pipeline->add(new task(UserVirtualAccountRequester::class, 'requestVa'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getUserInvoices() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(InvoiceSelector::class, 'getAllByUser'));
    return $this->executeAndResponse($data);
  }

  public function getUserInvoiceUsers() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(InvoiceUserSelector::class, 'getAllByUser'));
    return $this->executeAndResponse($data);
  }

  public function findInvoiceUser($id) {
    $data = $this->getRequestData();
    $data['id'] = $id;
    $this->pipeline->add(new Task(InvoiceUserSelector::class, 'findInvoiceUser'));
    $this->pipeline->add(new Task(UserVirtualAccountRequester::class, 'requestVa'));
    return $this->executeAndResponse($data);
  }

  public function getInvoiceByBilledUserId($id) {
    $data = $this->getRequestData();
    $data['id'] = $id;
    $this->pipeline->add(new Task(InvoiceSelector::class, 'getInvoiceByBilledUserId'));
    return $this->executeAndResponse($data);
  }

  public function updateInvoiceUser($id) {
    list($isValid, $data) = $this->validateData([
      'billed_user_email' => 'email',
    ]);

    if (!$isValid) {
      return $data;
    }

    $data['id'] = $id;
    $this->pipeline->add(new Task(InvoiceUserUpdater::class, 'updateInvoiceUser'));
    $this->pipeline->add(new Task(UserVirtualAccountRequester::class, 'requestVa'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getBilledUserVaCode($id) {
    $data = $this->getRequestData();
    $data['id'] = $id;
    $this->pipeline->add(new Task(UserVirtualAccountRequester::class, 'requestVa'));
    return $this->executeAndResponse($data);
  }

  public function insertBulkInvoice() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BulkInvoiceParser::class, 'parse'));
    $this->pipeline->add(new Task(BulkInvoiceValidator::class, 'validate'));
    $this->pipeline->add(new Task(BulkInvoiceRequester::class, 'request'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function exportInvoiceUsers() {
    $data = $this->getRequestData();
    $data['billed_user_id'] = '';
    $this->pipeline->add(new Task(InvoiceUserSelector::class, 'exportInvoiceUsers'));
    return $this->executeAndResponse($data);
  }

  public function getInvoiceVaCode($id) {
    $data = $this->getRequestData();
    $data['id'] = $id;
    $this->pipeline->add(new Task(UserVirtualAccountRequester::class, 'requestInvoiceVa'));
    return $this->executeAndResponse($data);
  }

  public function findInvoice($id) {
    $data = $this->getRequestData();
    $data['id'] = $id;
    $this->pipeline->add(new Task(InvoiceSelector::class, 'findInvoiceById'));
    return $this->executeAndResponse($data);
  }

  public function getInvoiceUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BusinessInvoiceUserSelector::class, 'getInvoiceUser'));
    $this->pipeline->add(new Task(BusinessInvoiceUserSettingRequester::class, 'getInvoicePaymentChannels'));
    return $this->executeAndResponse($data);
  }

  public function requestInvoiceUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BusinessInvoiceUserRequester::class, 'request'));
    return $this->executeAndResponse($data);
  }

  public function toggleChargeToCustomer() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BusinessInvoiceUserRequester::class, 'toggleChargeToCustomer'));
    return $this->executeAndResponse($data);
  }

  public function getInvoicePaymentChannel() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BusinessInvoiceUserSettingRequester::class, 'getInvoicePaymentChannels'));
    return $this->executeAndResponse($data);
  }

  public function toggleInvoicePaymentChannel() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required|numeric'
    ]);
    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(BusinessInvoiceUserSettingRequester::class, 'toggleInvoicePaymentChannel'));
    return $this->executeAndResponse($data);
  }

  public function updateSettlement() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required|numeric',
      'settlement_type' => 'required|numeric',
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BusinessInvoiceUserSettingRequester::class, 'updateSettlement'));
    return $this->executeAndResponse($data);
  }

}
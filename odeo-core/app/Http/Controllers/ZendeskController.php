<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\ZendeskAuthRequester;
use Odeo\Domains\Core\Task;

class ZendeskController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function ZendeskJwtAuthentication() {

    $data = $this->getRequestData();
    $this->pipeline->add(new Task(ZendeskAuthRequester::class, 'zendeskJwtAuthentication'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([], 401);
    }
    return response()->json($this->pipeline->data, 200);
  }

}

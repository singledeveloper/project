<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 1/19/17
 * Time: 9:53 PM
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Account\Userreport\Helper\Initializer;
use Odeo\Domains\Account\Userreport\Registered\RegisteredUserReportSelector;
use Odeo\Domains\Core\Task;

class RegisteredUserController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getRegisteredUserReport() {

    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $data['is_paginate'] = false;

    $this->pipeline->add(new Task(RegisteredUserReportSelector::class, 'getRegisteredUserReport', [
      'day_num' => Initializer::DAY_NUM
    ]));

    return $this->executeAndResponse($data);

  }

}
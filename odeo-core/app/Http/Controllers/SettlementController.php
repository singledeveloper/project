<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Payment\PaymentSettlementSelector;
use Odeo\Domains\Core\Task;

class SettlementController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function get() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['sort_by'] = $data['sort_by'] ?? 'settlement_date';
    $data['sort_type'] = $data['sort_type'] ?? 'desc';

    $this->pipeline->add(new Task(PaymentSettlementSelector::class, 'get'));

    return $this->executeAndResponse($data);
  }

  public function getDetail() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $data['sort_by'] = $data['sort_by'] ?? 'id';
    $data['sort_type'] = $data['sort_type'] ?? 'asc';

    $this->pipeline->add(new Task(PaymentSettlementSelector::class, 'getDetail'));

    return $this->executeAndResponse($data);
  }
}

<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 4/8/17
 * Time: 5:35 PM
 */

namespace Odeo\Http\Controllers;


use Illuminate\Http\Request;
use Odeo\Domains\Constant\VendorDisbursement;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Disbursement\BalanceSelector;
use Odeo\Domains\Vendor\Flip\BalanceUpdater;
use Odeo\Domains\Vendor\Flip\DisbursementNotifier;
use Odeo\Domains\Vendor\Flip\TransferRecordSelector;

class VendorFlipController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getFlipRecord() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(TransferRecordSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getInformation() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BalanceSelector::class, 'getBalance', [
      'id' => VendorDisbursement::FLIP
    ]));
    return $this->executeAndResponse($data);
  }

  public function notify(Request $request) {

    $data = $request->all();
    $data['data'] = json_decode($data['data'], true);

    $this->pipeline->add(new Task(DisbursementNotifier::class, 'getNotify'));
    $this->pipeline->enableTransaction();

    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return 'ERROR';
    }
    return 'SUCCESS';

  }

  public function updateBalance() {

    $data = $this->getRequestData();

    $this->pipeline->add(new Task(BalanceUpdater::class, 'updateBalance'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }


}
<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 06/08/19
 * Time: 17.33
 */

namespace Odeo\Http\Controllers;


use Illuminate\Http\Request;
use Odeo\Domains\Constant\VirtualAccount;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\UserInvoice\Odeo\Biller\Odeo\Requester;
use Odeo\Domains\Payment\Artajasa\Helper\ArtajasaApi;
use Odeo\Domains\Payment\Artajasa\Va\Inquirer;
use Odeo\Domains\Payment\Artajasa\Va\InquirySelector;
use Odeo\Domains\Payment\Artajasa\Va\Payer;
use Odeo\Domains\Payment\Artajasa\Va\PaymentSelector;
use Odeo\Domains\Payment\Artajasa\Va\Repository\PaymentArtajasaCallbackLogRepository;
use Odeo\Domains\Payment\Artajasa\Va\Validator;
use Odeo\Domains\Payment\Helper\PaymentInvoiceHelper;
use Odeo\Domains\Payment\PaymentNotificator;
use Odeo\Domains\Payment\PaymentVaDirectInquirer;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelValidator;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayInvoiceSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;

class ArtajasaPaymentController extends PaymentGatewayInvoiceController {

  private $artajasaCallbackLogRepo;

  public function __construct() {
    parent::__construct();
    $this->artajasaCallbackLogRepo = app()->make(PaymentArtajasaCallbackLogRepository::class);
  }

  private function logCallback($type, $body) {
    $log = $this->artajasaCallbackLogRepo->getNew();
    $log->type = $type;
    $log->body = $body;
    $this->artajasaCallbackLogRepo->save($log);
  }

  public function vaInquiry(Request $request) {
    $requestData = $request->getContent();
    
    $data = simplexml_load_string($requestData);
    $data = json_decode(json_encode($data), true);
    
    if ($this->isInvoiceRequest($data['vaid'])) {
      return $this->invoiceInquiry($data);
    }
    
    $this->logCallback('va_inquiry', $requestData);
    
    $data['virtual_account_number'] = $data['vaid'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_ARTAJASA;
    
    $this->pipeline->add(new Task(PaymentGatewayChannelValidator::class, 'validatePaymentVendor'));
    $this->pipeline->add(new Task(Inquirer::class, 'vaInquiry'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAInquiry'));
    $this->pipeline->add(new Task(Inquirer::class, 'createInquiry'));
    $this->pipeline->execute($data);

    $xml = new \SimpleXMLElement('<data/>');

    if ($this->pipeline->fail()) {
      clog('artajasa_va', serialize($this->pipeline->errorMessage));
      $response = [
        'type' => ArtajasaApi::RES_INQUIRY,
        'ack' => $this->pipeline->errorMessage['code'] ?? '05',
        'bookingid' => 'NULL',
        'customer_name' => 'NULL',
        'min_amount' => 0,
        'max_amount' => 0,
        'productid' => 'NULL',
        'signature' => $data['signature']
      ];
      $this->parseXml($response, $xml);
      $res = $xml->asXML();

      return response($res, 200, ['Content-Type' => 'application/xml']);
    }

    $data = $this->pipeline->data;
    $response = [
      'type' => $data['type'],
      'ack' => $data['ack'],
      'bookingid' => $data['bookingid'],
      'customer_name' => $data['customer_name'],
      'min_amount' => $data['min_amount'],
      'max_amount' => $data['max_amount'],
      'productid' => $data['productid'],
      'signature' => $data['signature']
    ];
    $this->parseXml($response, $xml);
    return response($xml->asXML(), 200, ['Content-Type' => 'application/xml']);
  }

  private function invoiceInquiry($data) {
    $invoiceLog = $this->logInvoiceRequest('invoice_inquiry', VirtualAccount::VENDOR_DIRECT_ARTAJASA, json_encode($data));
    $data['virtual_account_number'] = $data['vaid'];
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_ARTAJASA;

    $this->pipeline->add(new Task(Validator::class, 'validateInquiry'));
    $this->pipeline->add(new Task(PaymentVaDirectInquirer::class, 'inquiry'));
    $this->pipeline->execute($data);

    $xml = new \SimpleXMLElement('<data/>');
    if ($this->pipeline->fail()) {

      $errorCode = $this->pipeline->errorMessage['error_code'] ?? '91';
      switch ($errorCode) {
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
        case PaymentInvoiceHelper::INVOICE_INVALID:
          $errorCode = '76';
          break;
        default:
          $errorCode = '91';
          break;
      }
      $response = $this->buildInquiryErrorResponse($errorCode, $data['signature']);
      $this->parseXml($response, $xml);
      $res = $xml->asXML();

      return response($res, 200, ['Content-Type' => 'application/xml']);
    }

    $result = $this->pipeline->data;

    $this->updateInvoiceLog($invoiceLog, $result['order_id']);
    $response = [
      'type' => ArtajasaApi::RES_INQUIRY,
      'ack' => '00',
      'bookingid' => $result['reference'],
      'customer_name' => $result['customer_name'],
      'min_amount' => $result['total'],
      'max_amount' => $result['total'],
      'productid' => $result['item_name'],
      'signature' => $data['signature']
    ];
    $this->parseXml($response, $xml);

    return response($xml->asXML(), 200, ['Content-Type' => 'application/xml']);
  }

  public function vaPayment(Request $request) {
    $requestData = $request->getContent();
    
    $data = simplexml_load_string($requestData);
    $data = json_decode(json_encode($data), true);
    
    if ($invoice = $this->findInvoiceByReferenceAndAmount(VirtualAccount::VENDOR_DIRECT_ARTAJASA, $data['bookingid'], $data['amount'])) {
      return $this->invoicePayment($data, $invoice);
    }
    
    $this->logCallback('va_payment', $requestData);
    
    $this->pipeline->add(new Task(Payer::class, 'vaPayment'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->add(new Task(Payer::class, 'updatePayment'));
    $this->pipeline->execute($data);

    $xml = new \SimpleXMLElement('<return/>', LIBXML_NOEMPTYTAG);

    if ($this->pipeline->fail()) {
      clog('artajasa_va', serialize($this->pipeline->errorMessage));
      $response = [
        'type' => ArtajasaApi::RES_PAYMENT,
        'ack' => $this->pipeline->errorMessage['code'] ?? '05',
        'bookingid' => $data['bookingid'],
        'signature' => $data['signature']
      ];
      $this->parseXml($response, $xml);

      return response($xml->asXML(), 200, ['Content-Type' => 'application/xml']);
    }
    $data = $this->pipeline->data;
    $response = [
      'type' => $data['type'],
      'ack' => $data['ack'],
      'bookingid' => $data['bookingid'],
      'signature' => $data['signature']
    ];

    $this->parseXml($response, $xml);
    return response($xml->asXML(), 200, ['Content-Type' => 'application/xml']);
  }

  private function invoicePayment($data, $invoice) {
    $invoiceLog = $this->logInvoiceRequest('invoice_payment', VirtualAccount::VENDOR_DIRECT_ARTAJASA, json_encode($data));

    $data['virtual_account_number'] = $invoice->virtual_account_number;
    $data['vendor_code'] = VirtualAccount::VENDOR_DIRECT_ARTAJASA;

    $this->pipeline->add(new Task(Validator::class, 'validatePayment'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceSelector::class, 'getDetailByVaNo'));
    $this->pipeline->add(new Task(PaymentGatewayInvoiceNotifier::class, 'notify'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    $xml = new \SimpleXMLElement('<return/>', LIBXML_NOEMPTYTAG);

    if ($this->pipeline->fail()) {
      $errorCode = $this->pipeline->errorMessage['error_code'] ?? '01';

      switch ($errorCode) {
        case PaymentInvoiceHelper::INVALID_SIGNATURE:
          $errorCode = '101';
          break;
        case PaymentInvoiceHelper::INVOICE_INVALID_AMOUNT:
          $errorCode = '13';
          break;
        case PaymentInvoiceHelper::INVOICE_ALREADY_PAID:
          $errorCode = '78';
          break;
        case PaymentInvoiceHelper::INVOICE_NOT_FOUND:
        case PaymentInvoiceHelper::INVOICE_INVALID:
        default:
          $errorCode = '91';
          break;
      }

      $response = $this->buildPaymentResponse($errorCode, $data);
      $this->parseXml($response, $xml);
      return response($xml->asXml(), 200, ['Content-Type' => 'application/xml']);
    }

    $response = $this->buildPaymentResponse('00', $data);
    $this->parseXml($response, $xml);
    $this->updateInvoiceLog($invoiceLog, $invoice->order_id);

    return response($xml->asXml(), 200, ['Content-Type' => 'application/xml']);
  }

  public function getAllVaPayment() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(PaymentSelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getAllVaInquiry() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);
    $this->pipeline->add(new Task(InquirySelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  private function buildInquiryErrorResponse($code, $signature) {
    return [
      'type' => ArtajasaApi::RES_INQUIRY,
      'ack' => $code,
      'bookingid' => 'NULL',
      'customer_name' => 'NULL',
      'min_amount' => 0,
      'max_amount' => 0,
      'productid' => 'NULL',
      'signature' => $signature
    ];
  }

  private function buildPaymentResponse($code, $data) {
    return [
      'type' => ArtajasaApi::RES_PAYMENT,
      'ack' => $code,
      'bookingid' => $data['bookingid'],
      'signature' => $data['signature']
    ];
  }

  private function parseXml($data, &$xml) {
    foreach ($data as $key => $val) {
      $xml->$key = $val;
    }
  }

}

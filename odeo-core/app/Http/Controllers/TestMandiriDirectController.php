<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 07/05/19
 * Time: 14.22
 */

namespace Odeo\Http\Controllers;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Request;

class TestMandiriDirectController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function createRequest() {
    $request = Request::instance();
    $xml = $request->getContent();
    $client = new Client();
    $response = $client->request('POST', 'http://api.staging.odeo.co.id/v1/mandiri-va', [
      'headers' => [
        'Content-Type' => 'text/xml; charset=UTF8',
      ],
      'body' => $xml
    ]);
    return $response->getBody();
  }

}
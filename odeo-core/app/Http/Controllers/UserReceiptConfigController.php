<?php
/**
 * Created by PhpStorm.
 * User: febrianjiuwira
 * Date: 5/9/17
 * Time: 1:58 PM
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Core\Task;
use Odeo\Domains\Order\Receipt\ReceiptConfigSelector;
use Odeo\Domains\Order\Receipt\ReceiptConfigUpdater;

class UserReceiptConfigController extends Controller {
  public function __construct() {
    parent::__construct();
  }

  public function getConfig() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(ReceiptConfigSelector::class, 'getReceiptConfig'));
    return $this->executeAndResponse($data);
  }

  public function updateConfig() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(ReceiptConfigUpdater::class, 'updateReceiptConfig'));
    return $this->executeAndResponse($data);
  }
}
<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Hotel\HotelManager;


class HotelController extends Controller {

  public function __construct() {

    parent::__construct();

  }

  public function searchAutoComplete() {
    
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(HotelManager::class, 'searchAutoComplete'));

    return $this->executeAndResponse($data);
    
  }


  public function searchHotel() {
    
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'store_id' => 'required|exists:stores,id',
      'startdate' => 'required',
      'enddate' => 'required',
      'night' => 'required',
      'room' => 'required',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(HotelManager::class, 'searchHotel'));

    return $this->executeAndResponse($data);

  }


  public function getRoomList() {
    
    list($isValid, $data) = $this->validateData([
      'service_detail_id' => 'required',
      'store_id' => 'required|exists:stores,id',
      'business_uri' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(HotelManager::class, 'getRoomList'));

    return $this->executeAndResponse($data);

  }
  

}

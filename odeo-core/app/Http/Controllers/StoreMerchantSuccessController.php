<?php
/**
 * Created by PhpStorm.
 * User: Romano
 * Date: 12/21/16
 * Time: 11:36 PM
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\StoreStatus;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\MerchantSuccessSelector;

class StoreMerchantSuccessController extends Controller {

  public function index () {
    $data = $this->getRequestData();

    $this->parseQuerySearch($data);

    $data["expand"] = "plan,owner,deposit,invitator,provider,store_inventories,invited_by";
    $data['search']['store_status'] = StoreStatus::OK;

    $this->pipeline->add(new Task(MerchantSuccessSelector::class, 'getMerchantStore'));
    return $this->executeAndResponse($data);
  }
}
<?php

namespace Odeo\Http\Controllers;

use Illuminate\Validation\Rule;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Constant\Transportation;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Transportation\TransportationManager;

class TransportationController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getTnC() {
    $this->pipeline->add(new Task(TransportationManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function searchNominal() {
    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id'
    ]);

    if (!$isValid) return $data;

    if (isset($data['default_price']) && $data['default_price']
      || (isset($data['service_detail_id']) && $data['service_detail_id'] == 'undefined')) {
      $data['service_detail_id'] = ServiceDetail::TRANSPORTATION_ODEO;
    }

    $this->pipeline->add(new Task(TransportationManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);

  }

  public function getCategories() {
    list($isValid, $data) = $this->validateData([
      'category' => Rule::in([Transportation::ETOLL, Transportation::GRAB, Transportation::GOJEK]),
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::TRANSPORTATION_ODEO;
    if (isset($data['category'])) {
      $data['only'] = Transportation::getCategoriesByKey($data['category']);
    }

    if (isset($data['except'])) {
      $excepts = json_decode($data['except']);
      // for new version, which $data['except'] is array of key, instead of direct categories
      $altExcepts = array_reduce($excepts, function($res, $key) {
        return array_merge($res, Transportation::getCategoriesByKey($key));
      }, []);
      $data['except'] = count($altExcepts) > 0 ? $altExcepts : $excepts;
    }

    $this->pipeline->add(new Task(TransportationManager::class, 'getAllCategoryByServiceDetailId'));
    return $this->executeAndResponse($data);
  }

  public function getAmounts() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required|exists:stores,id',
      'category' => 'required'
    ]);

    if (!$isValid) return $data;

    $data['service_detail_id'] = ServiceDetail::TRANSPORTATION_ODEO;

    $this->pipeline->add(new Task(TransportationManager::class, 'searchNominal'));

    return $this->executeAndResponse($data);
  }

}

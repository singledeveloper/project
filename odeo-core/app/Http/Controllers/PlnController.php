<?php

namespace Odeo\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Odeo\Domains\Biller\Bakoel\Inquirer\PlnInquirer;
use Odeo\Domains\Constant\Pulsa;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Constant\ServiceDetail;
use Odeo\Domains\Inventory\Pln\PlnBukalapakScrapper;
use Odeo\Domains\Inventory\Pln\PlnManager;
use Odeo\Domains\Inventory\Pln\PlnScrapper;
use Odeo\Domains\Inventory\Pulsa\PulsaManager;

class PlnController extends Controller {

  public function getTnC() {
    $this->pipeline->add(new Task(PulsaManager::class, 'getTermAndCon'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function get() {

    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id',
    ]);

    if (!$isValid) return $data;

    if(isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::PLN_ODEO;
    }

    if (isset($data['number'])) {
      $redis = Redis::connection();
      $switchFlag = $redis->get(Pulsa::REDIS_PLN_SWITCH_LOCK);
      if ($switchFlag && $switchFlag >= Pulsa::PLN_SWITCH_FLAG)
        $this->pipeline->add(new Task(PlnInquirer::class, 'inquiry', ['bypass' => true]));
      //else $this->pipeline->add(new Task(PlnScrapper::class, 'getPrepaidInquiry'));
      else $this->pipeline->add(new Task(PlnBukalapakScrapper::class, 'getPrepaidInquiry'));
    }
    $this->pipeline->add(new Task(PlnManager::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getPostpaid() {

    list($isValid, $data) = $this->validateData([
      'default_price' => 'required_without_all:service_detail_id,store_id',
      'service_detail_id' => 'required_unless:default_price,true',
      'store_id' => 'required_unless:default_price,true|exists:stores,id',
      'number' => 'required'
    ]);

    if (!$isValid) return $data;

    if(isset($data['default_price']) && $data['default_price']) {
      $data['service_detail_id'] = ServiceDetail::PLN_POSTPAID_ODEO;
    }

    $this->pipeline->add(new Task(PlnManager::class, 'validatePostpaidInventory', ["for_bill" => true]));
    return $this->executeAndResponse($data);
  }

}

<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Core\Task;
use Odeo\Domains\Supply\TemplateRequestMaker;
use Odeo\Domains\Supply\TemplateResponseGroupMaker;
use Odeo\Domains\Supply\TemplateResponseMaker;
use Odeo\Domains\Supply\TemplateSelector;

class SupplyTemplateController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getTemplates() {
    $this->pipeline->add(new Task(TemplateSelector::class, 'get'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getTemplateDetail($templateId) {
    $data = $this->getRequestData();
    $data['template_id'] = $templateId;
    $this->pipeline->add(new Task(TemplateSelector::class, 'getDetail'));
    return $this->executeAndResponse($data);
  }

  public function createTemplate() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'template_name' => 'required',
      'format_type' => 'required',
      'body_details' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TemplateRequestMaker::class, 'setRequest'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editTemplate($templateId) {
    list($isValid, $data) = $this->validateData([
      'template_name' => 'required',
      'format_type' => 'required',
      'body_details' => 'required'
    ]);
    if (!$isValid) return $data;
    $data['template_id'] = $templateId;

    $this->pipeline->add(new Task(TemplateRequestMaker::class, 'edit'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getTemplatePreviewExample() {
    list($isValid, $data) = $this->validateData([
      'format_type' => 'required',
      'body_details' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TemplateRequestMaker::class, 'preview'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getTemplateResponses($templateId) {
    $data = $this->getRequestData();
    $data['template_id'] = $templateId;
    $this->pipeline->add(new Task(TemplateSelector::class, 'getResponses'));
    return $this->executeAndResponse($data);
  }

  public function getTemplateBasicResponses($templateId) {
    $data = $this->getRequestData();
    $data['template_id'] = $templateId;
    $this->pipeline->add(new Task(TemplateSelector::class, 'getBasicResponses'));
    return $this->executeAndResponse($data);
  }

  public function setResponseGroup() {
    list($isValid, $data) = $this->validateData([
      'template_id' => 'required',
      'vendor_switcher_id' => 'required',
      'format_type' => 'required',
      'route_type' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TemplateResponseGroupMaker::class, 'setResponseGroup'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeResponseGroup($templateResponseId) {
    $data = $this->getRequestData();
    $data['template_response_id'] = $templateResponseId;

    $this->pipeline->add(new Task(TemplateResponseGroupMaker::class, 'removeGroup'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function registerResponse() {
    list($isValid, $data) = $this->validateData([
      'template_response_id' => 'required_without:template_id',
      'template_id' => 'required_without:template_response_id',
      'vendor_switcher_id' => 'required',
      'response_status_type' => 'required',
      'response_details' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(TemplateResponseMaker::class, 'setResponse'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeResponse($templateResponseDetailId) {
    $data = $this->getRequestData();
    $data['template_response_detail_id'] = $templateResponseDetailId;

    $this->pipeline->add(new Task(TemplateResponseMaker::class, 'remove'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

}

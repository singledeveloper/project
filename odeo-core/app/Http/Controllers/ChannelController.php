<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Channel\StoreMdrRequester;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Channel\ChannelRequester;
use Odeo\Domains\Channel\ChannelSelector;
use Odeo\Domains\Channel\ChannelTypeSelector;
use Odeo\Domains\Channel\ChannelPaymentConfirmator;
use Odeo\Domains\Channel\ChannelCodeRequester;
use Odeo\Domains\Subscription\StoreSelector;
use Odeo\Domains\Subscription\StoreVerificator;

class ChannelController extends Controller {

  public function index() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ChannelSelector::class, 'getDetail', [
      'expand' => 'store,mdr,channel_data,channel_settings'
    ]));
    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function create() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'channel_data.name' => 'required',
      'channel_data.phone_number' => 'required',
      'channel_data.address' => 'required',
      'channel_data.city' => 'required',
      'channel_data.province' => 'required',
      'channel_data.zip_code' => 'required',
      'channel_settings.type' => 'required|in:static,variable,pulsa',
      'channel_settings.amount' => 'required_if:channel_settings.type,static,pulsa',
      'channel_settings.currency' => 'required_if:channel_settings.type,static,pulsa',
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(ChannelRequester::class, 'create'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updateSettings() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required',
      'channel_settings.type' => 'required',
      'channel_settings.amount' => 'required_if:channel_settings.type,static,pulsa',
      'channel_settings.currency' => 'required_if:channel_settings.type,static,pulsa'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ChannelRequester::class, 'update'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function type() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(ChannelTypeSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function pay() {
    list($isValid, $data) = $this->validateData([
      'channel_code' => 'required|exists:store_channels,code',
      'skip_confirmation' => 'boolean'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(ChannelSelector::class, 'getDetail', [
      'expand' => 'user,store,channel_settings'
    ]));
    $this->pipeline->add(new Task(StoreSelector::class, 'getStoreDetail', [
      'expand' => 'inventory',
      'fields' => 'id,opay_logo_path'
    ]));
    $this->pipeline->add(new Task(ChannelPaymentConfirmator::class, 'confirm'));
    $this->pipeline->enableTransaction();
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      if($this->pipeline->statusCode == 403) return $this->buildResponse(200, $this->pipeline->errorMessage);
      return $this->buildErrorsResponse($this->pipeline->errorMessage, $this->pipeline->statusCode);
    }

    $response = $this->pipeline->data;
    if($response['skip_confirmation']) {
      $response = [];
      $response['skip_confirmation'] = true;
    } else {
      if(isset($response['auth'])) unset($response['auth']);
      if(isset($response['channel_code'])) unset($response['channel_code']);
      if(isset($response['user_id'])) unset($response['user_id']);
      if(isset($response['user_name'])) unset($response['user_name']);
      if(isset($response['daily_payment_limit'])) unset($response['daily_payment_limit']);
      if(isset($response['service_id'])) unset($response['service_id']);
      if(isset($response['operator'])) unset($response['operator']);
      if(isset($response['price'])) unset($response['price']);
      if(isset($response['inventory'])) unset($response['inventory']);
    }

    return $this->buildSuccessResponse($response, $this->pipeline->statusCode);
  }

  public function code() {
    $data = $this->getRequestData();

    $this->pipeline->add(new Task(ChannelCodeRequester::class, 'stat'));
    return $this->executeAndResponse($data);
  }

  public function listChannel() {
    $data = $this->getRequestData();
    $data["expand"] = 'channel_code,channel_data,channel_settings,store,mdr';

    $this->pipeline->add(new Task(ChannelSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function generateCode() {
    list($isValid, $data) = $this->validateData([
      'amount' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(ChannelCodeRequester::class, 'create'));
    return $this->executeAndResponse($data);
  }

  public function assignCode() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required|exists:store_channels,id',
      'code' => 'required|exists:channel_codes,code'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(ChannelRequester::class, 'updateData'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function updateDeviceId() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required|exists:store_channels,id',
      'device_id' => 'required'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(ChannelRequester::class, 'updateData'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function setMDR() {
    list($isValid, $data) = $this->validateData([
      'channel_id' => 'required|exists:store_channels,id',
      'mdr' => 'required'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(ChannelSelector::class, 'getDetail', [
      'expand' => 'store',
      'fields' => 'id'
    ]));
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(StoreMdrRequester::class, 'upsert'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function printCode() {
    list($isValid, $data) = $this->validateData([
      'amount' => 'required',
      'id' => 'exists:channel_codes,id'
    ]);

    if (!$isValid) return $data;

    $channel = app()->make(\Odeo\Domains\Channel\ChannelCodeRequester::class);
    return $channel->printCode($data);
  }
}

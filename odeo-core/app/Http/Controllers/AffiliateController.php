<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Biller\Bakoel\Inquirer\PlnInquirer;
use Odeo\Domains\Constant\Affiliate;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\AffiliatePulsaSearcher;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaSearcher;
use Odeo\Domains\Inventory\Pln\PlnBukalapakScrapper;
use Odeo\Domains\Inventory\Pln\PlnScrapper;
use Odeo\Domains\Order\Affiliate\OrderPurchaser;
use Odeo\Domains\Order\Affiliate\PostpaidInquirer;

class AffiliateController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function purchasePrepaid() {
    list($isValid, $data) = $this->validateData([
      'denom' => 'required',
      'number' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(OrderPurchaser::class, 'parse'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse(Affiliate::finalizeErrorMessage($data['number'], $this->pipeline->errorMessage));
    }

    return $this->buildResponse(200, $this->pipeline->data);
  }

  public function checkOrderStatus($orderId) {
    $data = $this->getRequestData();
    $data['order_id'] = $orderId;
    $this->pipeline->add(new Task(OrderPurchaser::class, 'checkOrder'));
    return $this->executeAndResponse($data);
  }

  public function inquiryPrepaidPln() {
    list($isValid, $data) = $this->validateData([
      'number' => 'required',
    ]);

    if (!$isValid) return $data;

    //$this->pipeline->add(new Task(PlnInquirer::class, 'inquiry', ['is_error' => true]));
    //$this->pipeline->add(new Task(PlnScrapper::class, 'getPrepaidInquiry', ['is_error' => true]));
    $this->pipeline->add(new Task(PlnBukalapakScrapper::class, 'getPrepaidInquiry', ['is_error' => true]));
    return $this->executeAndResponse($data);
  }

  public function inquiryPrepaid() {
    list($isValid, $data) = $this->validateData([
      'number' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaSearcher::class, 'getPrepaidInquiry'));
    if (isset($data['list'])) {
      $this->pipeline->add(new Task(PulsaSearcher::class, 'searchNominal'));
    }
    return $this->executeAndResponse($data);
  }

  public function inquiryPostpaid() {
    list($isValid, $data) = $this->validateData([
      'denom' => 'required',
      'number' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PostpaidInquirer::class, 'parse'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      $number = revertTelephone($data['number']);
      $number = explode('/', $number)[0];
      $number = explode('#', $number)[0];
      $errorMessage = is_array($this->pipeline->errorMessage) ? implode(', ', $this->pipeline->errorMessage) : $this->pipeline->errorMessage;
      if (strpos($errorMessage, 'Trx sdh pernah') === false)
        $errorMessage = 'GAGAL Trx ke ' . $number . '. ' . $errorMessage;
      return $this->buildErrorsResponse($errorMessage);
    }

    return $this->buildResponse(200, $this->pipeline->data);
  }

  public function getProductCategories() {
    $this->pipeline->add(new Task(PulsaSearcher::class, 'getAllCategory', ['for_api' => true]));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getProductList() {
    list($isValid, $data) = $this->validateData([
      'category_code' => 'required'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(AffiliatePulsaSearcher::class, 'getByCategory'));
    return $this->executeAndResponse($data);
  }

  public function getProductPrice() {
    list($isValid, $data) = $this->validateData([
      'product_code' => 'required'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(AffiliatePulsaSearcher::class, 'getCodePrice'));
    return $this->executeAndResponse($data);
  }

  public function getProductDescription() {
    list($isValid, $data) = $this->validateData([
      'product_code' => 'required'
    ]);

    if (!$isValid) return $data;
    $this->pipeline->add(new Task(AffiliatePulsaSearcher::class, 'getCodeDescription'));
    return $this->executeAndResponse($data);
  }

}

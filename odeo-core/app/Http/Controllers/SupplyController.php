<?php

namespace Odeo\Http\Controllers;

use Odeo\Domains\Biller\BillerReconciliationSelector;
use Odeo\Domains\Biller\BillerSelector;
use Odeo\Domains\Constant\Supplier;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Inventory\Helper\Vendor\Odeo\PulsaEditor;
use Odeo\Domains\Supply\BillerConnector;
use Odeo\Domains\Supply\Biller\Notifier;
use Odeo\Domains\Supply\BillerDetailizer;
use Odeo\Domains\Supply\BillerInventoryEditor;
use Odeo\Domains\Supply\BillerInventorySelector;

class SupplyController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getConnectionList() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['is_paginate'] = false;
    $data['fields'] = 'id,name,is_active,current_balance,notify_slug';
    $data['status'] = '50000';
    $data['expand'] = 'connection';

    $this->pipeline->add(new Task(BillerSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getBillerList() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['is_paginate'] = false;
    $data['fields'] = 'id,name,status,current_balance';

    $this->pipeline->add(new Task(BillerSelector::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function getDetail($vendorSwitcherId) {
    $data = $this->getRequestData();
    $data['vendor_switcher_id'] = $vendorSwitcherId;

    $this->pipeline->add(new Task(BillerDetailizer::class, 'get'));
    return $this->executeAndResponse($data);
  }

  public function registerConnection() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'name' => 'required_without:vendor_switcher_id',
      'biller_template_id' => 'required',
      'server_destination' => 'required',
      //'queue_limit' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillerConnector::class, 'setup'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editConnection($vendorSwitcherId) {
    list($isValid, $data) = $this->validateData([
      'name' => 'required',
      'biller_template_id' => 'required',
      'server_destination' => 'required',
      //'queue_limit' => 'required'
    ]);
    if (!$isValid) return $data;
    $data['vendor_switcher_id'] = $vendorSwitcherId;

    $this->pipeline->add(new Task(BillerConnector::class, 'edit'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editConnectionPassword($vendorSwitcherId) {
    $data = $this->getRequestData();
    $data['vendor_switcher_id'] = $vendorSwitcherId;

    $this->pipeline->add(new Task(BillerConnector::class, 'editPassword'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editConnectionPin($vendorSwitcherId) {
    $data = $this->getRequestData();
    $data['vendor_switcher_id'] = $vendorSwitcherId;

    $this->pipeline->add(new Task(BillerConnector::class, 'editPin'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editConnectionToken($vendorSwitcherId) {
    $data = $this->getRequestData();
    $data['vendor_switcher_id'] = $vendorSwitcherId;

    $this->pipeline->add(new Task(BillerConnector::class, 'editToken'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function toggleBiller($vendorSwitcherId) {
    $data = $this->getRequestData();
    $data['vendor_switcher_id'] = $vendorSwitcherId;

    $this->pipeline->add(new Task(BillerConnector::class, 'toggle'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function addInventory() {
    list($isValid, $data) = $this->validateData([
      'vendor_switcher_id' => 'required|exists:vendor_switchers,id',
      'pulsa_odeo_id' => 'required_without:pulsa_category|exists:pulsa_odeos,id',
      'pulsa_category' => 'required_without:pulsa_odeo_id',
      'pulsa_name' => 'required_without:pulsa_odeo_id'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillerInventoryEditor::class, 'add'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editInventory($inventoryId) {
    list($isValid, $data) = $this->validateData([
      'store_base_price' => 'required'
    ]);
    if (!$isValid) return $data;
    $data['pulsa_odeo_id'] = $inventoryId;

    $this->pipeline->add(new Task(BillerInventoryEditor::class, 'edit'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function toggleInventory($inventoryId) {
    $data = $this->getRequestData();
    $data['pulsa_odeo_id'] = $inventoryId;

    $this->pipeline->add(new Task(BillerInventoryEditor::class, 'toggle'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getMarketplaceInventories() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillerInventorySelector::class, 'getMarketplaceInventories'));
    return $this->executeAndResponse($data);
  }

  public function sellToMarketPlace() {
    list($isValid, $data) = $this->validateData([
      'price_details' => 'required',
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillerInventoryEditor::class, 'sellToMarketPlace'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function editMarketplace($inventoryId) {
    list($isValid, $data) = $this->validateData([
      'sell_price' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['inventory_id'] = $inventoryId;
    $this->pipeline->add(new Task(BillerInventoryEditor::class, 'editMarketplace'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function removeMarketplace($inventoryId) {
    $data = $this->getRequestData();
    $data['inventory_id'] = $inventoryId;
    $this->pipeline->add(new Task(BillerInventoryEditor::class, 'removeMarketplace'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function notify($slug) {
    $data = $this->getRequestData();
    $data['slug'] = $slug;

    $this->pipeline->add(new Task(Notifier::class, 'notify'));
    $this->pipeline->enableTransaction();
    return $this->executeAndResponse($data);
  }

  public function getOptions() {
    $data = $this->getRequestData();
    if (isset($data['type'])) {
      if ($data['type'] == 'format') return $this->buildResponse(200, Supplier::getFormats());
      else if ($data['type'] == 'status') return $this->buildResponse(200, Supplier::getStatuses());
    }
    return $this->buildResponse(204);
  }

  public function testConnection() {
    $this->pipeline->add(new Task(BillerConnector::class, 'testConnection'));
    return $this->executeAndResponse($this->getRequestData());
  }

  public function getBaseInventories() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillerInventorySelector::class, 'getBaseInventories'));
    return $this->executeAndResponse($data);
  }

  public function getUnpopulateInventories() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'supply_group_id' => 'required'
    ]);
    if (!$isValid) return $data;

    $data['is_populated'] = true;
    $this->pipeline->add(new Task(BillerInventorySelector::class, 'getUnpopulateInventories'));
    return $this->executeAndResponse($data);
  }

  public function reconSupply() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(BillerReconciliationSelector::class, 'getReconForSupply'));
    return $this->executeAndResponse($data);
  }

  public function exportOrder() {
    list($isValid, $data) = $this->validateData([
      'store_id' => 'required',
      'date' => 'required',
      'email' => 'required'
    ]);
    if (!$isValid) return $data;

    $this->pipeline->add(new Task(BillerReconciliationSelector::class, 'exportOrder'));
    return $this->executeAndResponse($data);
  }

  public function editSupplyOrder() {
    list($isValid, $data) = $this->validateData([
      'order_id' => 'required|exists:orders,id',
      'vendor_switcher_id' => 'required',
      'status' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PulsaEditor::class, 'editSwitcherSupply'));
    return $this->executeAndResponse($data);
  }

}

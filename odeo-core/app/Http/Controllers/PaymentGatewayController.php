<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 04/12/18
 * Time: 20.32
 */

namespace Odeo\Http\Controllers;


use Odeo\Domains\Account\Helper\AdminChecker;
use Odeo\Domains\Core\Task;
use Odeo\Domains\OAuth2\Helper\ResponseHelper;
use Odeo\Domains\PaymentGateway\PaymentGatewayChannelSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayNotifier;
use Odeo\Domains\PaymentGateway\PaymentGatewayPaymentSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayPaymentUpdater;
use Odeo\Domains\PaymentGateway\PaymentGatewayUserSelector;
use Odeo\Domains\PaymentGateway\PaymentGatewayUserUpdater;

class PaymentGatewayController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getPaymentByUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getByUser'));
    return $this->executeAndResponse($data);
  }

  public function exportPaymentByUser() {
    list($isValid, $data) = $this->validateData([
      'start_date' => 'required|date',
      'end_date' => 'required|date'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'export'));

    return $this->executeAndResponse($data);
  }

  public function exportInvoiceByUser() {
    list($isValid, $data) = $this->validateData([
      'periode' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'exportInvoice'));

    return $this->executeAndResponse($data);
  }

  public function getFilterPaymentByUser() {
    $data['status'] = [
      ['code' => 'PENDING', 'message' => '10001 - PENDING'],
      ['code' => 'ON_PROGRESS_PAYMENT_APPROVAL', 'message' => '30000 - ON PROGRESS PAYMENT APPROVAL'],
      ['code' => 'SUCCESS', 'message' => '50000 - SUCCESS'],
      ['code' => 'SUSPECT', 'message' => '80000 - SUSPECT'],
      ['code' => 'FAILED', 'message' => '90000 - FAILED'],
    ];

    return $this->buildResponse(200, $data);
  }

  public function getAllPayment() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getAll'));
    return $this->executeAndResponse($data);
  }

  public function getPayment($paymentId) {
    $data = $this->getRequestData();
    $data['payment_gateway_payment_id'] = $paymentId;

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getPayment'));
    $this->pipeline->execute($data);
    if ($this->pipeline->fail()) {
      return response()->json([
        'message' => $this->pipeline->errorMessage,
        'status_code' => ResponseHelper::STATUS_ERROR,
        'error_code' => ResponseHelper::CODE_ERROR
      ], 400);
    }
    return response()->json($this->pipeline->data);
  }

  public function getPaymentByReferenceId($referenceId) {
    $data = $this->getRequestData();
    $data['external_reference_id'] = $referenceId;

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getPaymentByReferenceId'));
    $this->pipeline->execute($data);
    if ($this->pipeline->fail()) {
      return response()->json($this->pipeline->errorMessage, 200);
    }
    return response()->json($this->pipeline->data);
  }
  
  public function getPgNotifyLog() {
    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getNotifyLog'));
    return $this->executeAndResponse($data);
  }

  public function getPaymentLog($paymentId) {
    $data = $this->getRequestData();
    $data['payment_id'] = $paymentId;

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getLogByPayment'));
    return $this->executeAndResponse($data);
  }

  public function getPaymentGatewayUser() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(PaymentGatewayUserSelector::class, 'getPaymentGatewayUser'));
    return $this->executeAndResponse($data);
  }

  public function updateCallback() {
    list($valid, $data) = $this->validateData([
      'url' => 'required|url',
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayUserUpdater::class, 'updateCallback'));
    return $this->executeAndResponse($data);
  }

  public function updateEmailReport() {
    list($valid, $data) = $this->validateData([
      'email' => 'required',
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayUserUpdater::class, 'updateEmailReport'));
    return $this->executeAndResponse($data);
  }

  public function togglePaymentChannel() {
    list($valid, $data) = $this->validateData([
      'channel_id' => 'required'
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayUserUpdater::class, 'togglePaymentChannel'));
    $this->pipeline->enableTransaction();
    
    return $this->executeAndResponse($data);
  }

  public function paymentChannelGroupList() {
    $data = $this->getRequestData();
    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->add(new Task(PaymentGatewayChannelSelector::class, 'getActivePaymentGroups'));
    return $this->executeAndResponse($data);
  }

  public function updateSettlement() {
    list($valid, $data) = $this->validateData([
      'channel_id' => 'required',
      'settlement_type' => 'required'
    ]);

    if (!$valid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayUserUpdater::class, 'updateSettlement'));
    $this->pipeline->enableTransaction();
    
    return $this->executeAndResponse($data);
  }

  public function export() {

    $data = $this->getRequestData();
    $this->parseQuerySearch($data);

    $this->pipeline->add(new Task(AdminChecker::class, 'check'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return $this->buildErrorsResponse();
    }

    return (new PaymentGatewayPaymentSelector())->getExported($data);
  }

  public function resendPaymentNotification($paymentId) {
    $data = $this->getRequestData();
    $data['payment_gateway_payment_id'] = $paymentId;

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getPaymentNotifyDetail'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));
    $this->pipeline->execute($data);

    if ($this->pipeline->fail()) {
      return response()->json([
        'message' => $this->pipeline->errorMessage,
      ], 400);
    }
    return response()->json($this->pipeline->data);
  }

  public function markAsAccepted() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'payment_id' => 'required',
    ]);

    if (!$isValid) {
      return $data;
    }

    $this->pipeline->add(new Task(PaymentGatewayPaymentUpdater::class, 'markPaymentAsAccepted'));

    return $this->executeAndResponse($data);
  }

  public function notifyPayment() {
    list($isValid, $data) = $this->validateData([
      'password' => 'required',
      'payment_id' => 'required'
    ]);

    if (!$isValid) return $data;
    $data['payment_gateway_payment_id'] = $data['payment_id'];

    $this->pipeline->add(new Task(PaymentGatewayPaymentSelector::class, 'getPaymentNotifyDetail'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'validateNotify'));
    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'notifyVAPayment'));

    return $this->executeAndResponse($data);
  }

  public function renotifyPayment() {
    list($isValid, $data) = $this->validateData([
      'pg_payment_id' => 'required'
    ]);

    if (!$isValid) return $data;

    $this->pipeline->add(new Task(PaymentGatewayNotifier::class, 'renotifyPayment'));

    return $this->executeAndResponse($data);
  }
}

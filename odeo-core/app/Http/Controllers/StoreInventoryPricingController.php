<?php
/**
 * Created by PhpStorm.
 * User: vete
 * Date: 7/7/17
 * Time: 19:23
 */

namespace Odeo\Http\Controllers;

use Odeo\Domains\Constant\InventoryPricing;
use Odeo\Domains\Core\Task;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceCreator;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceRemover;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceSelector;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryGroupPriceToggleAutoUpdate;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryPricingSelector;
use Odeo\Domains\Subscription\ManageInventory\StoreInventoryPricingUpdater;
use Odeo\Domains\Subscription\StoreVerificator;

class StoreInventoryPricingController extends Controller {

  public function __construct() {
    parent::__construct();
  }

  public function getPricing($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreInventoryPricingSelector::class, 'get'));

    return $this->executeAndResponse($data);

  }

  public function addGroupPrice($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'response_with_store_data' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryGroupPriceCreator::class, 'create'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data, null, [
      'store'
    ]);
  }

  public function removeGroupPrice($storeId, $serviceDetailId) {

    $data = $this->getRequestData();


    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(StoreInventoryGroupPriceRemover::class, 'remove'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function getUpdateDuration() {
    return $this->buildSuccessResponse(InventoryPricing::getUpdateTime());
  }

  public function updatePricing($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'response_with_store_data' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryPricingUpdater::class, 'updateInventoryPrice'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data, null, [
      'store'
    ]);

  }

  public function compareGroupPrice($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard'));
    $this->pipeline->add(new Task(StoreInventoryGroupPriceSelector::class, 'compareGroupPrice'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);

  }

  public function toggleGroupPriceAutoUpdateProfit($storeId, $serviceDetailId) {

    $data = $this->getRequestData();

    $data['store_id'] = $storeId;
    $data['service_detail_id'] = $serviceDetailId;

    $this->pipeline->add(new Task(StoreVerificator::class, 'guard', [
      'not_available_for_free_store' => true
    ]));
    $this->pipeline->add(new Task(StoreInventoryGroupPriceToggleAutoUpdate::class, 'toggle'));
    $this->pipeline->enableTransaction();

    return $this->executeAndResponse($data);
  }

}
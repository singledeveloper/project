<?php

class resetPasswordTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testResetPasswordByTelephone() {
    
    $telephone = '628997170052';
    
    $api = $this->json('POST', 'user/forgotPassword', [
      'data' => [
        'phone_number' => $telephone
      ]
    ]);

    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'ttl'
      ]
    ]);
    
    $api = $this->json('POST', 'user/resetPassword', [
      'data' => [
        'telephone' => $telephone,
        'type' => 'sms',
        'token' => '1234',
        'password' => '1234567890',
        'confirm_password' => '1234567890'
      ]
    ]);
    
    $api->seeStatusCode(200);
  }
}

<?php

class requestWithdrawTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testRequestWithdraw() {
    
    $this->loginAsSeller();
    
    $api = $this->get('user/bankAccount', IntegrationTestCase::$storage['header']);
    
    $user = IntegrationTestCase::$storage['user'];

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'bank_accounts' => [
          '*' => [
            "id"
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    
    $api = $this->json('POST', 'user/withdraw/create', [
      'data' => [
        'bank_account_id' => $data['bank_accounts'][0]['id'],
        'cash' => [
          'amount' => 1000000,
          'currency' => 'IDR'
        ],
        'pin' => '12345678'
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'withdraw_id'
      ]
    ]);
    
    $this->loginAsAdmin();
    
    $api = $this->json('POST', 'user/withdraw/complete', [
      'data' => [
        'user_id' => $user["user_id"]
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(200);
  }
  
}

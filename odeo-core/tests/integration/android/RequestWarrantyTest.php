<?php

class requestWarrantyTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testRequestWarranty() {
    
    $this->loginAsSeller();
    
    $api = $this->get('user/stores', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'stores' => [
          "*" => [
            'store_id'
          ]
        ]
      ]
    ]);
    
    $data = json_decode($api->response->getContent(), true)['data'];
    
    $api = $this->get('warranty/' . $data['stores'][0]['store_id'], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'type', 'description', 'expired_at'
      ]
    ]);
    
    $api = $this->json('POST', 'warranty/create', [
      'data' => [
        'store_id' => $data['stores'][0]['store_id'],
        'info' => [
          "name" => "Brian Japutra",
          "phone_number" => "6285267878088",
          "address" => "Grand Slipi Tower",
          "province" => "DKI Jakarta",
          "city" => "Jakarta Barat",
          "zip_code" => "11280"
        ]
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'warranty_id', 'cart_id'
      ]
    ]);
    
    $this->checkoutAndPay(\Odeo\Domains\Constant\Payment::ANDROID_WARRANTY, 143);
    
    $api = $this->get('warranty/' . $data['stores'][0]['store_id'], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'warranty_id', 'expired_at'
      ]
    ]);
  }
  
}

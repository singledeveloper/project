<?php

class requestTopupTest extends AndroidTestCase {

  use \Laravel\Lumen\Testing\DatabaseTransactions;

  public function setUp()
  {
    parent::setUp();

    $this->beginDatabaseTransaction();
  }

  public function testRequestTopup() {
    
    $this->loginAsSeller();
    
    $api = $this->get('user/cash', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'cash' => [
          "amount", "currency", "formatted_amount"
        ]
      ]
    ]);
    
    $api = $this->json('POST', 'user/topup/create', [
      'data' => [
        'cash' => [
          "amount" => 100000,
          "currency" => "IDR"
        ]
      ]
    ], IntegrationTestCase::$storage['header']);
    
    $api->seeStatusCode(201);
    
    $api->seeJsonStructure([
      'data' => [
        'topup_id', 'cart_id'
      ]
    ]);
    
    $api = $this->get('user/topup', IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);
    
    $api->seeJsonStructure([
      'data' => [
        'topups' => [
          "*" => [
            'topup_id'
          ]
        ]
      ]
    ]);
    
    $this->checkoutAndPay(\Odeo\Domains\Constant\Payment::ANDROID_OCASH, 66);
  }
  
}

<?php

abstract class AndroidTestCase extends IntegrationTestCase {

  public function __construct() {
    parent::__construct();
  }


  public function checkoutAndPay($gatewayId, $opc) {

    $response_checkout = $this->checkout([
      'platform_id' => \Odeo\Domains\Constant\Platform::ANDROID,
      'gateway_id' => $gatewayId
    ]);

    $this->payWithBankTransfer($response_checkout['order_id'], $opc);
  }


}
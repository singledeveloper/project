<?php

trait Authorization {

  public function loginAsAdmin() {

    $api = $this->json('POST', 'user/login', [
      'email' => 'admin@odeo.co.id',
      'password' => '12345678'
    ]);

    $api->seeStatusCode(201);

    IntegrationTestCase::$storage['user'] = json_decode($api->response->content(), true)['data'];

    IntegrationTestCase::$storage['header'] = [
      'Authorization' => 'Bearer ' . IntegrationTestCase::$storage['user']['token'],
    ];

  }
  
  public function loginAsSeller() {

    $api = $this->json('POST', 'user/login', [
      'phone_number' => '628997170052',
      'password' => '12345678'
    ]);

    $api->seeStatusCode(201);

    IntegrationTestCase::$storage['user'] = json_decode($api->response->content(), true)['data'];

    IntegrationTestCase::$storage['header'] = [
      'Authorization' => 'Bearer ' . IntegrationTestCase::$storage['user']['token'],
    ];

  }

  public function loginAsGuest() {

    $api = $this->json('POST', 'user/guestLogin');

    $api->seeStatusCode(201);

    IntegrationTestCase::$storage['user'] = json_decode($api->response->content(), true)['data'];

    IntegrationTestCase::$storage['header'] =  [
      'Authorization' => 'Bearer ' . IntegrationTestCase::$storage['user']['token'],
      'Accept-Language' => 'id'
    ];

  }

}
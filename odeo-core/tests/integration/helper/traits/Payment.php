<?php

trait Payment {

  public function payWithBankTransfer($orderId, $opc) {

    $api = $this->get('order/' . $orderId, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJson([
      'status' => "" . \Odeo\Domains\Constant\OrderStatus::CREATED
    ]);

    $response_order = json_decode($api->response->getContent(), true)['data'];

    $api = $this->get('payment/list?order_id=' . $orderId, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJson([
      'opc' => $opc
    ]);

    $api = $this->json('POST', 'payment/request', [
      'data' => [
        'opc' => $opc,
        'order_id' => $orderId,

      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api = $this->json('POST', 'payment/open', [
      'data' => [
        'order_id' => $orderId,
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api = $this->get('order/' . $orderId, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJson([
      'status' => "" . \Odeo\Domains\Constant\OrderStatus::OPENED
    ]);

    $api = $this->json('POST', 'payment/confirm', [
      'data' => [
        'order_id' => $orderId,
        'confirmation_amount' => $response_order['cart_data']['total']['amount'],
        'confirmation_name' => 'vincent',
        'confirmation_number' => "123456789"
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api = $this->get('order/' . $orderId, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJson([
      'status' => "" . \Odeo\Domains\Constant\OrderStatus::CONFIRMED
    ]);

    $this->loginAsAdmin();

    $api = $this->json('POST', 'order/' . $orderId . '/verify/', [], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);


    $api = $this->get('order/' . $orderId, IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(200);

    $api->seeJson([
      'status' => "" . \Odeo\Domains\Constant\OrderStatus::COMPLETED
    ]);

  }

}
<?php

trait Cart {

  public function addCart($data) {

    $api = $this->json('POST', 'cart/add', [
      'data' => [
        'item_id' => $data['item_id'],
        'item_detail' => $data['item_detail'],
        'store_id' => $data['store_id'],
        'service_detail_id' => $data['service_detail_id']
      ]
    ], IntegrationTestCase::$storage['header']);

    $api->seeStatusCode(201);
  }
  
}
<?php

abstract class IntegrationTestCase extends TestCase {

  use Authorization, Payment, Checkout, Cart;

  static $storage = null;

  public function __construct() {

    parent::__construct();

    $this->baseUrl = $this->baseUrl . '/v1';

  }


}
